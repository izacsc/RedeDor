#Include 'Protheus.ch'
/*
{Protheus.doc} MT094END()
Ponto de Entrada para Gera��o de "pr� nota de entrada" 
@Author     Mick William da Silva
@Since      06/05/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Param      
@Return     
*/

User Function MT094END()

	Local aRet := {}

	aRet:= U_F0100404()
	
Return aRet

