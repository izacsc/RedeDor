#Include 'Protheus.ch'

/*{Protheus.doc} MTA110MNU()
Ponto de Entrada para adicionar as opções do Banco de Conhecimento no aRotina
@Author		Nairan Alves Silva
@Since			27/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
User Function MTA110MNU()

U_F0400103()

Return aRotina