#Include 'Protheus.ch'

/*
{Protheus.doc} MT131MNU()
Ponto de Entrada para Adicionar a Tela de Log e Mendi��o Automatizada na Rotina de Gera��o Cota��o" 
@Author     Mick William da Silva
@Since      11/07/2016
@Version    P12.7
@Project    MAN00000462901_EF_001
@Return		aNewRot, Retorna as Novas Rotinas a serem inclusas no A��es Relacionadas Browse.  
*/

User Function MT131MNU()
	IF FindFunction('U_F0100108')
		U_F0100108()
	EndIF
Return (aRotina)
 
