#Include 'Protheus.ch'

/*
{Protheus.doc} FA750BRW()
PE para adicionar o bot�o recusa nas rotinas FINA750 
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Return		aRet, Vetor de Bot�es    
*/
User Function FA750BRW()

	Local aRet := {}

	aRet:= U_F0101005() 

Return aRet

