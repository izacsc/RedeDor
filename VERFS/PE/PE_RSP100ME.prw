#Include 'Protheus.ch'

/*/{Protheus.doc} RSP100ME
(long_description)
@author Fernando
@since 14/12/2016
@version 1.0
@project MAN0000007423039_EF_002
/*/
User Function RSP100ME()
	Local aArea := GetArea()
	aAdd( aRotina, {'Reabre a Vaga', 'u_VagaReab()', 0, Nil} )
	aAdd( aRotina, {'Historico', 'U_HistMotiv()', 0, Nil} )
	RestArea(aArea)
Return



/*/{Protheus.doc} F0500206
Vaga Reaberta
@author  Fernando Carvalho 
@since 14/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
User Function VagaReab()	
	Local lMotivo		:= .F.
	Local cMotivo		:= ""
	Local cStatus		:= "Vaga Reaberta"
	
	If !Empty(SQS->QS_XSUSPEN)
			Alert("A vaga ser� reaberta para o status em que estava antes de ser 'Suspensa'!")
			//dar um seek
			RecLock("SQS",.F.)
				SQS->QS_XMOTIVO := cMotivo
				SQS->QS_XSTATUS := SQS->QS_XSUSPEN
				SQS->QS_XSUSPEN := ''
			SQS->(MsUnLock())	
		
	Else
			Alert("A vaga n�o foi 'Suspensa' para que seja 'Reaberta'!")	
	EndIf			
Return
	
/*/{Protheus.doc} HistMotiv
Historico
@author Fernando
@since 14/12/2016
@version 1.0
@project MAN0000007423039_EF_002
/*/
User Function HistMotiv()
	Local oDlg
	Local cTexto1 := SQS->QS_XMOTIVO
	
	If !Empty(cTexto1)
		DEFINE DIALOG oDlg TITLE "Motivo da Altera��o" FROM 180, 180 TO 420, 700 PIXEL
		TMultiGet():new( 01, 01, {| u | if( pCount() > 0, cTexto1 := u, cTexto1 ) },oDlg, 260, 92, , , , , , .T. )
		TButton():New( 100, 100, "OK",oDlg,{||oDlg:End()}, 40,10,,,.F.,.T.,.F.,,.F.,,,.F. )
		ACTIVATE DIALOG oDlg CENTERED
	Else
		Alert("N�o existe hist�rico de altera��es!")
	EndIf
Return	