#Include "PROTHEUS.CH"
/*{Protheus.doc} PE_FINA050()
Ponto de entrada no final do processamento da T�tulo a Pagar.
@Author	Paulo Kr�ger
@Since		27/10/2016
@Version	P12.7
@Project   MAN00000463801_EF_001
@Return	l�gico	 */

User Function FINA050()

Local	lRet	:=	.T.
Local	aArea	:=	GetArea()
/*===================================================================|
|Exclui documentos anexos quando excluida a T�tulo a Pagar.          |
|===================================================================*/
If ISINCALLSTACK('FA050Delet')
	lRet := U_F0400105('FINA050',SE2->E2_FILIAL, SE2->(E2_NUM+E2_PREFIXO+E2_FORNECE+E2_LOJA))
EndIf
RestArea(aArea)
Return(lRet)