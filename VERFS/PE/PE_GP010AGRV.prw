#Include 'Protheus.ch'

/*
{Protheus.doc} GP010AGRV()
Apos a grava��o dos registros
@Author     Bruno de Oliveira
@Since      23/11/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
*/
User Function GP010AGRV()

	U_F0100326(SRA->RA_XVAGA,SRA->RA_CIC)

Return