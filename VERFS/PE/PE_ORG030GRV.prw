#Include 'Protheus.ch'

/*
{Protheus.doc} ORG030GRV()
Apos a grava��o do Posto
@Author     Bruno de Oliveira
@Since      04/07/2016
@Version    P12.1.07
@Project    MAN00000463701_EF_001
*/
User Function ORG030GRV()
	
	Local nOpc := PARAMIXB[1]
	Local cPosto := PARAMIXB[2]
	Local cDepto := PARAMIXB[3]
	
	U_F0300101(nOpc,cPosto,cDepto)
	
Return
