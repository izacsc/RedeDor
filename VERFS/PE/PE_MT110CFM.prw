#Include 'Protheus.ch'

/*
{Protheus.doc} MT110CFM()
Ponto de Entrada para executar a Medi��o Automatizada ap�s a Libera��o
Da Solicita��o de Compras. 
@Author		Mick William da Silva
@Since		18/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001
@Return		NIL
*/

User Function MT110CFM()
	Local cNumSol  := PARAMIXB[1]     
	Local nOpc  	:= PARAMIXB[2]     
	  
	If FindFunction("U_F0100101") .And. nOpc == 1
		U_F0100101(.T.,cNumSol)
	EndIf
	
Return Nil

