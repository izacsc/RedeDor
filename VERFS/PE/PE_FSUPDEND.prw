#Include 'Protheus.ch'
/*---------------------------------------------------------------------------------------------------------------------------
{Protheus.doc} FSUPDEND
Ponto de  Entrada do FSTools
@Author     queizy.nascimento
@Since      02/12/2016
@Version    P12.7
@Project    MAN0000007423039_EF_002
@Return
/*///---------------------------------------------------------------------------------------------------------------------------
#Include 'Protheus.ch'

/*
{Protheus.doc} FSUPDEND()

@Author     Henrique Madureira
@Since      20/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_002
@Return	 
*/
User Function FSUPDEND()

If FindFunction("U_F0100202")
	U_F0100202()
EndIf
If FindFunction("U_F0300601")
	U_F0300601()
EndIf
If FindFunction("U_F0500107")
	U_F0500107()
EndIf
If FindFunction("U_F0500108")
	U_F0500108()
EndIf
If FindFunction("U_F0500207")
	U_F0500207()
EndIf

If FindFunction("U_F0500111")
	U_F0500111()
EndIf
If FindFunction("U_F0500406")
	U_F0500406()
Endif

PutMv ("MV_PROXNUM", "U009")

Return .T.













