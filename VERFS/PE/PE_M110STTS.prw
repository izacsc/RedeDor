#Include 'Protheus.ch'
/*
{Protheus.doc} M110STTS()
Ponto de Entrada para executar a Medi��o Automatizada ap�s a Confirma��o
Da Solicita��o de Compras. 
@Author		Mick William da Silva
@Since		18/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001	
*/

User Function M110STTS()
	Local cNumSol	:= Paramixb[1]
	
	If FindFunction("U_F0100101")
		U_F0100101(.F.,cNumSol)
	EndIf
	
Return

