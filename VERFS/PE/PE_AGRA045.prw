#Include 'Protheus.ch'
/*
{Protheus.doc} AGRA045()
Ponto de Entrada para que possa ser somente ser utilizadao 
a funcionalidade de Visuaiza��o da rotina padr�o "Locais de Estoque" 
@Author     Mick William da Silva
@Since      13/05/2016
@Project    MAN00000463301_EF_016
*/

User Function AGRA045()
	
	Local aRet := {}

	aRet := U_F0201601()

Return aRet