#Include 'Protheus.ch'
/*
{Protheus.doc} MT120COR()
Ponto de Entrada para altera��o do Status (legenda de Pedidos de Compra)
PE no fonte MATA120
@Author     Mick William da Silva
@Since      05/05/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Return		aRet, vetor de cores      
*/

User Function MT120COR()

	Local aRet := {}

	aRet := U_F0100402()
	
Return aRet

