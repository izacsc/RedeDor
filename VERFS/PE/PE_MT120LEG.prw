#Include 'Protheus.ch'
/*
{Protheus.doc} MT120LEG()
Ponto de Entrada para altera��o da Legenda dos Pedidos de Compra
PE no fonte MATA120
@Author     Mick William da Silva
@Since      05/05/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Return		aRet, Vetor de Cores     
*/
User Function MT120LEG()

	Local aRet := {}

	aRet := U_F0100403()

Return aRet

