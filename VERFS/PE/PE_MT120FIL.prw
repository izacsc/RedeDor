#Include 'Protheus.ch'

/*/{Protheus.doc} RSP100ME
(long_description)
@author Nairan
@since 26/12/2016
@version 1.0
@project MAN00000462901_EF_004
/*/
User Function MT120FIL()
Local cRet	:= ""
cRet := "C7_XSOLPAG != '1'"
Return cRet
