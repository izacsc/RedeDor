#define STR0001 "Confirma"
#define STR0002 "Redigita"
#define STR0003 If( cPaisLoc $ "ANG|PTG", "Abandonar", "Abandona" )
#define STR0004 "Pesquisar"
#define STR0005 "Visualizar"
#define STR0006 If( cPaisLoc $ "ANG|PTG", "Manuten��o", "Manuten��o" )
#define STR0007 "Excluir"
#define STR0008 "Transfer�ncias"
#define STR0009 "Origem"
#define STR0010 "Destino"
#define STR0011 "Empresa:"
#define STR0012 "Filial :"
#define STR0013 If( cPaisLoc $ "ANG|PTG", "Registo: ", "Matricula :" )
#define STR0014 If( cPaisLoc $ "ANG|PTG", "C.custo :", "C.Custo :" )
#define STR0015 If( cPaisLoc $ "ANG|PTG", "Data de refer�ncia : ", "Data de Referencia : " )
#define STR0016 If( cPaisLoc $ "ANG|PTG", "O registo digitada pertence ao empregado: ", "A Matricula Digitada Pertence ao Funcion�rio: " )
#define STR0017 If( cPaisLoc $ "ANG|PTG", "Confirma a transfer�ncia ?            ", "Confirma a Transfer�ncia ?            " )
#define STR0018 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia entre filiais", "Transfer�ncia entre Filiais" )
#define STR0019 If( cPaisLoc $ "ANG|PTG", "A transferir ficheiro: ", "Transferindo Arquivo: " )
#define STR0020 "Aguarde..."
#define STR0021 "Visualizar - Transfer�ncias"
#define STR0022 If( cPaisLoc $ "ANG|PTG", "Registo:", "Matricula:" )
#define STR0023 "Nome:"
#define STR0024 If( cPaisLoc $ "ANG|PTG", "Quanto � Exclus�o?", "Quanto a Exclus�o?" )
#define STR0025 "Exclus�o -Transfer�ncias"
#define STR0026 If( cPaisLoc $ "ANG|PTG", "** n�o registado **", "** Nao Cadastrado **" )
#define STR0027 "Transfer�ncia n�o pode ser anterior `a Admiss�o"
#define STR0028 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia Fora Do Per�odo Da Data Base", "Transfer�ncia fora do periodo da Data Base" )
#define STR0029 "Transfer�ncia n�o pode ser anterior `a ultima Transfer�ncia"
#define STR0030 If( cPaisLoc $ "ANG|PTG", "N�o � a permitida a transfer�ncia de registo para este empregado.", "N�o e' Permitida Transferencia de Matricula para este Funcion�rio" )
#define STR0031 If( cPaisLoc $ "ANG|PTG", "J� Existe Empregado Com O Registo Digitada Na Empresa Destino. Introduza Novo Registo.", "J� existe Funcion�rio com a Matricula Digitada na Empresa Destino. Informe nova Matricula." )
#define STR0032 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia Entre Empresas", "Transfer�ncia entre Empresas" )
#define STR0033 If( cPaisLoc $ "ANG|PTG", "J� existe empregado com o registo digitada. indique novo registo. ", "J� existe Funcion�rio com a Matricula Digitada. Informe nova Matricula. " )
#define STR0034 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia De Registo", "Transfer�ncia de Matricula" )
#define STR0038 If( cPaisLoc $ "ANG|PTG", "Caso n�o seja o mesmo empregado RECOMENDAMOS que seja informado o novo Registo.", "Caso n�o seja o mesmo Funcion�rio RECOMENDAMOS seja informada nova Matricula." )
#define STR0039 If( cPaisLoc $ "ANG|PTG", "N�o foi poss�vel abrir o ficheiro da empresa de destino: ", "N�o foi possivel abrir o arquivo da Empresa Destino: " )
#define STR0040 If( cPaisLoc $ "ANG|PTG", "A preparar ficheiros para a transfer�ncia. ", "Preparando Arquivos para a Transfer�ncia. " )
#define STR0041 If( cPaisLoc $ "ANG|PTG", "Admiss�o:", "Admiss�o:" )
#define STR0042 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia Em Lote", "Transfer�ncia em Lote" )
#define STR0043 If( cPaisLoc $ "ANG|PTG", "Op��es", "Opcoes" )
#define STR0044 "Filtro      "
#define STR0045 If( cPaisLoc $ "ANG|PTG", "Marca/desm.todos", "Marca/Desm.Todos" )
#define STR0046 If( cPaisLoc $ "ANG|PTG", "A actualizar relacionamentos do: ", "Atualizando Relacionamentos do: " )
#define STR0047 If( cPaisLoc $ "ANG|PTG", "Aten��o!", "Atencao!" )
#define STR0048 If( cPaisLoc $ "ANG|PTG", "Filiais com mesmo n�mero de contribuinte. a transfer�ncia ser� feita como se os n�mero de contribuintes fossem diferentes", "Filiais com mesmo CNPJ. A transf�rencia ser� feita como se os CNPJs fossem diferentes" )
#define STR0049 If( cPaisLoc $ "ANG|PTG", "A transfer�ncia de registos s� � permitida para empregados admitidos dentro do m�s de compet�ncia.", "Transfer�ncia de matriculas s� e permitida para funcion�rios admitidos dentro do m�s de compet�ncia." )
#define STR0050 If( cPaisLoc $ "ANG|PTG", "Em &lote", "Em Lote" )
#define STR0051 If( cPaisLoc $ "ANG|PTG", "O registo ", "O Registro " )
#define STR0052 If( cPaisLoc $ "ANG|PTG", " est� reservado para outro utilizador.", " est� reservado para outro usu�rio." )
#define STR0053 If( cPaisLoc $ "ANG|PTG", "Existe outro utilizador a efectuar esta opera��o", "Existe outro usu�rio efetuando esta opera��o" )
#define STR0054 If( cPaisLoc $ "ANG|PTG", "A Filtrar empregados", "Filtrando Funcion�rios" )
#define STR0055 If( cPaisLoc $ "ANG|PTG", "A Seleccionar Empregados", "Selecionando Funcion�rios" )
#define STR0056 If( cPaisLoc $ "ANG|PTG", "A Eliminar A Selec��o", "Retirando a Sele��o" )
#define STR0057 If( cPaisLoc $ "ANG|PTG", "N�o Foi Poss�vel A Reserva Dos Registos Para O Processo De Transfer�ncia", "N�o foi possivel a reserva dos registros para o processo de Transfer�ncia" )
#define STR0058 If( cPaisLoc $ "ANG|PTG", "N�o Foi Poss�vel A Reserva De Alguns Registos Para O Processo De Transfer�ncia", "N�o foi possivel a reserva de alguns registros para o processo de Transfer�ncia" )
#define STR0059 If( cPaisLoc $ "ANG|PTG", "Estes registos est�o reservados para outro utilizador.", "Estes registros est�o reservados para outro usuario." )
#define STR0060 If( cPaisLoc $ "ANG|PTG", "O processo de transfer�ncia, para estes registos, n�o ser� efectuado.", "O Processo de Transfer�ncia, para estes registros, n�o sera efetuado." )
#define STR0061 If( cPaisLoc $ "ANG|PTG", "A Reservar Os Registos Para Transfer�ncia", "Reservando os Registros para Transfer�ncia" )
#define STR0062 If( cPaisLoc $ "ANG|PTG", "A Desbloquear Os Registos", "Liberando os Registros" )
#define STR0063 If( cPaisLoc $ "ANG|PTG", "N�o foi poss�vel reservar todos os registos para transfer�ncia", "N�o foi possivel reservar todos os registros para transferencia" )
#define STR0064 "Tentar novamente?"
#define STR0065 If( cPaisLoc $ "ANG|PTG", "A tentar reservar o(s) registo(s).", "Tentando reservar o(s) registro(s)." )
#define STR0066 If( cPaisLoc $ "ANG|PTG", "O registo ", "O Registro " )
#define STR0067 If( cPaisLoc $ "ANG|PTG", " est� reservado para outro utilizador.", " esta reservado para outro usuario." )
#define STR0068 If( cPaisLoc $ "ANG|PTG", "N�o existem empregados a serem transferidos", "N�o existem funcionarios a serem transferidos" )
#define STR0069 "Pesquisar"
#define STR0070 If( cPaisLoc $ "ANG|PTG", "Excedeu o n�mero de registos a serem reservados.", "Excedeu o numero de registros a serem reservados." )
#define STR0071 If( cPaisLoc $ "ANG|PTG", "Matr�cula Inv�lida", "Matricula Invalida" )
#define STR0072 If( cPaisLoc $ "ANG|PTG", "Ocorreram Inconsist�ncias Durante O Processo De Transfer�ncia. Deseja Consultar O Log", "Ocorreram Inconsistencias durante o Processo de Transferencia. Deseja consultar o LOG" )
#define STR0073 If( cPaisLoc $ "ANG|PTG", "Di�rio De Ocorr�ncias", "Log de Ocorrencias" )
#define STR0074 If( cPaisLoc $ "ANG|PTG", "Transfer�ncia De Registos N�o � Permitida Para Quem Utiliza Integridade Referencial", "Transferencia de matriculas nao e permitida para quem utiliza Integridade Referencial" )
#define STR0075 If( cPaisLoc $ "ANG|PTG", "N�o Foi Poss�vel Efectuar A Tranfer�ncia Para O Registo:", "Nao foi possivel Efetuar a Tranferencia para a Matricula:" )
#define STR0076 If( cPaisLoc $ "ANG|PTG", "N�o Existe O Empregado Na Filial De Origem:", "Nao existe o Funcionario na Filial de Origem:" )
#define STR0077 "Filial:"
#define STR0078 If( cPaisLoc $ "ANG|PTG", "Registo:", "Matricula:" )
#define STR0079 If( cPaisLoc $ "ANG|PTG", "N�o Foi Poss�vel Actualizar O Registo De Empregados Na Origem:", "Nao foi possivel atualizar o Cadastro de Funcionarios na Origem:" )
#define STR0080 If( cPaisLoc $ "ANG|PTG", "N�o � Recomendada A Elimina��o De Transfer�ncias.", "N�o e recomendada a Exclus�o de Transferencias." )
#define STR0081 If( cPaisLoc $ "ANG|PTG", "Se For Confirmada A Elimina��o, Apenas Os Registos De Transfer�ncias", "Se for confirmada a Exclus�o, apenas os Registros de Transferencias" )
#define STR0082 If( cPaisLoc $ "ANG|PTG", "Do sre ser�o eliminados. os dados do empregado permanecer�o inalterados.", "do SRE ser�o eliminados. Os Dados do Funcionario permanecer�o inalterados." )
#define STR0083 If( cPaisLoc $ "ANG|PTG", "Confirmar A Exclus�o?", "Confirma a Exclus�o?" )
#define STR0084 If( cPaisLoc $ "ANG|PTG", "Empregado j� transferido anteriormente!", "Funcionario ja transferido anteriormente!" )
#define STR0085 If( cPaisLoc $ "ANG|PTG", "Impossibilidade de transfer�ncia", "Impossibilidade de transferencia" )
#define STR0086 If( cPaisLoc $ "ANG|PTG", "Empregado tem demiss�o prevista para o pr�ximo m�s.", "Funcionario tem demissao calculada para o proximo mes" )
#define STR0087 If( cPaisLoc $ "ANG|PTG", "Empregado ", "Funcionario " )
#define STR0088 If( cPaisLoc $ "ANG|PTG", " tem a valor ", " tem a verba " )
#define STR0089 If( cPaisLoc $ "ANG|PTG", " dividida para um centro de custo que n�o existe na filial destino, por este motivo n�o foi transferido", " rateada para um centro de custo que nao existe na filial destino, por e sse motivo nao foi transferido" )
#define STR0090 If( cPaisLoc $ "ANG|PTG", "Elemento:", "Item :" )
#define STR0091 "Classe :"
#define STR0092 "Legenda"
#define STR0093 If( cPaisLoc $ "ANG|PTG", "Alias inv�lido: ", "Alias Inv�lido: " )
#define STR0159 "� necess�rio que os seguintes campos estejam presentes na configura��o: "
#define STR0160 If( cPaisLoc $ "ANG|PTG", "Refira o posto para carregar seus dados!", "Informe o posto para carregar seus dados!" )
#define STR0161 If( cPaisLoc $ "ANG|PTG", "Posto n�o encontrado!", "Posto nao encontrado!" )
#define STR0162 If( cPaisLoc $ "ANG|PTG", "Empregado tem demiss�o calculada no m�s e por este motivo n�o foi transferido.", "Funcionario tem demissao calculada no mes, e por este motivo nao foi transferido." )
#define STR0163 If( cPaisLoc $ "ANG|PTG", "Transfer�ncias n�o s�o permitidas em meses Posteriores ou Anteriores ao m�s de c�lculo da Folha de Pagamento ", "Transferencias n�o s�o permitidas em meses Posteriores ou Anteriores ao atual m�s de c�lculo da Folha de Pagamento " )
#define STR0164 "Grupo Empresas:"
#define STR0165 "Empr./Unid Negoc./Filial: "
#define STR0166 If( cPaisLoc $ "ANG|PTG", "Tabela TM0 - Ficha M�dica, configurada como EXCLUSIVA.                   ", "Tabela TM0 - Ficha Medica, configurada como EXCLUSIVA.                   " )
#define STR0167 If( cPaisLoc $ "ANG|PTG", "Se optar por continuar com o processo de transfer�ncia, APENAS as tabelas do GEST�O DE PESSOAL ser�o transferidas.", "Se optar por continuar com o processo de transferencia, APENAS as tabelas do GESTAO DE PESSOAL serao transferidas." )
#define STR0168 If( cPaisLoc $ "ANG|PTG", "As tabelas BAU, REP, TM0, TM5 e TMT, do Medicina e Seguran�a do Trabalho, N�O ser�o transferidas e dever�o ser actualizadas manualmente.", "As tabelas BAU, REP, TM0, TM5 e TMT do Medicia e Seguranca do trabalho NAO serao transferidas e deverao ser atualizadas manualmente." )
#define STR0169 If( cPaisLoc $ "ANG|PTG", "Continuar com o processo de TRANSFER�NCIA ? ", "Continuar com o processo de TRANSFERENCIA ? " )
#define STR0170 If( cPaisLoc $ "ANG|PTG", "A tabela TM0 - Ficha M�dica, est� configurada como Exclusiva. APENAS as tabelas do Gest�o de Pessoal foram transferidas!", "A tabela TM0 - Ficha Medica, esta configurada como Exclusiva. APENAS as tabelas do Gestao de Pessoal foram transferidas!" )
#define STR0171 If( cPaisLoc $ "ANG|PTG", "ATEN��O! As tabelas BAU, REP, TM0, TM5 e TMT, do Medicina e Seguran�a do Trabalho, N�O foram transferidas, e dever�o ser ", "ATENCAO! As tabelas BAU, REP, TM0, TM5 e TMT do Medicia e Seguranca do Trabalho NAO foran transferidas, e deverao ser " )
#define STR0172 If( cPaisLoc $ "ANG|PTG", "actualizadas\inclu�das manualmente.", "atualizadas\incluidas manualmente." )
#define STR0173 If( cPaisLoc $ "ANG|PTG", "A data de refer�ncia � obrigat�ria.", "Data de referencia e obrigatoria!" )
#define STR0174 If( cPaisLoc $ "ANG|PTG", "O colaborador � respons�vel por um departamento. Deseja desvincul�-lo?", "O funcion�rio � responsavel por um departamento, deseja desassoci�-lo?" )
#define STR0175 "Departamento"
#define STR0176 If( cPaisLoc $ "ANG|PTG", "O registo n�o ser� integrado ao RM - N�o existe ambiente instalado", "O registro n�o ser� integrado ao RM - N�o existe ambiente instalado" )
#define STR0177 If( cPaisLoc $ "ANG|PTG", "Existem colaborador(es) com Situa��o(�es) de Demitidos na transfer�ncia. Deseja continuar?", "Existem funcionario(s) com Situa��o(��es) de Demitidos na transfer�ncia, Deseja Continuar ?" )
#define STR0178 "Posto:"
#define STR0179 "O Posto esta com  Status 'Congelado' ou 'Cancelado'."
#define STR0180 "Consulte o administrador do sistema."
#define STR0181 "Excedeu a quantidade maxima de ocupantes permitida para o posto."
#define STR0182 "Dados para Altera��o"
#define STR0183 "C�d. Fun��o De:"
#define STR0184 "C�d. Fun��o Para:"
#define STR0185 "Sal�rio De:"
#define STR0186 "Sal�rio Para:"
#define STR0187 "Tabela De:"
#define STR0188 "Tabela Para:"
#define STR0189 "N�vel De:"
#define STR0190 "N�vel Para:"
#define STR0191 "Faixa De:"
#define STR0192 "Faixa Para:"
#define STR0193 "Tipo Alt. Salarial:"
#define STR0194 "Dt. Alt. Salarial:"
#define STR0195 "A data da altera��o salarial n�o pode ser menor que a data de admiss�o."
#define STR0196 "Ok"
#define STR0197 "Nova fun��o n�o foi informada."
#define STR0198 "Novo salario n�o foi informado."
#define STR0199 "Nova tabela  n�o foi informada."
#define STR0200 "Novo n�vel n�o foi informado."
#define STR0201 "Nova faixa n�o foi informada."
#define STR0202 "Tipo de Altera��o salarial n�o foi informado"
#define STR0203 "Data de altera��o salarial n�o foi informada."
#define STR0204 "Departamento est� bloqueado para uso."
#define STR0205 "Departamento possui centro de custo diferente do centro de custos do funcion�rio"
#define STR0206 "Funcion�rio com status de demitido."
#define STR0207 "A matr�cula digitada j� existe e pertence ao mesmo funcion�rio."
#define STR0208 " ainda nao tem o registro de Admissao ou Carga Inicial enviado ao RET, por esse motivo nao foi transferido. Verifique no TAF os dados desse funcionario."
#define STR0209 "eSocial: Registro de admiss�o do funcion�rio em tr�nsito TAF x RET. A transferencia n�o ser� efetivada."
#define STR0210 "ATEN��O: Ap�s a confirma��o N�O SER� POSS�VEL DESFAZER ESSA OPERA��O!"
