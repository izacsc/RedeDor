#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
/*
{Protheus.doc} F0500108()
Atualiza��o da tabela RCB
@Author     Roberto Souza
@Since      08/11/2016
@Version    P12.7
@Project    MAN0000007423039_EF_00101
@Return
*/
User Function F0500108()
	
	Local nFil
	Local nTamFil    := Len(cFilAnt)
	Local nTamFilRCB := Len(AllTrim(xFilial("RCB")))
	Local aTodasFil  := {}
	Local aFilRCB    := {xFilial("RCB")}
	Local cFilRCB    := cFilAnt
	
	aTodasFil := FWAllFilial(cEmpAnt,,,.F.)
	For nFil := 1 To Len(aTodasFil)
		cFilRCB := PadR(Left(aTodasFil[nFil],nTamFilRCB),nTamFil)
		If ( AScan(aFilRCB,{|x| x == cFilRCB }) == 0 )
			AAdd(aFilRCB,cFilRCB)
		EndIf
	Next
	
	For nFil := 1 To Len(aFilRCB)
		RCB->(DbSetOrder(1))
		If !RCB->(DbSeek(IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil]) + "U006"))
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U006'
			RCB->RCB_DESC   := 'CODIGO RESCISAO'
			RCB->RCB_ORDEM  := '01'
			RCB->RCB_CAMPOS := 'CODIGO'
			RCB->RCB_DESCPO := 'C�digo Rescis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 2// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '99'
			RCB->RCB_PESQ   := '1'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U006'
			RCB->RCB_DESC   := 'CODIGO RESCISAO'
			RCB->RCB_ORDEM  := '02'
			RCB->RCB_CAMPOS := 'DESCRICAO '
			RCB->RCB_DESCPO := 'Descri��o Rescis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 30// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_PADRAO := ''
			RCB->RCB_PESQ   := '1'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
			
			
		EndIf
	Next
	
Return

