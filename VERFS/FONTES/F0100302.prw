#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH" 
/*
{Protheus.doc} F0100302()
Busca o departamento da matricula referente a vis�o 
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100302()

	Local oOrg
	Local cHtml   := ""
	
	Private nPTotal
	Private nCPage

	WEB EXTENDED INIT cHtml START "InSite"

	 	Default HttpGet->Page        := "1"
		Default HttpGet->FilterField := ""
		Default HttpGet->FilterValue := ""
	 	nCPage:= Val(HttpGet->Page)

		oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
		                
		fGetInfRotina("U_F0100301.APW") //Verificar
		GetMat()							//Pega a Matricula e a filial do participante logado
	     
		oOrg:cVISION      	:= HttpSession->aInfRotina:cVisao
		oOrg:cPARTICIPANTID	:= HttpSession->cParticipantID 		
		oOrg:nPAGE		 	:= nCPage
		oOrg:cFilterField   := HttpGet->FilterField
		oOrg:cFilterValue   := HttpGet->FilterValue
	
		If oOrg:GetDepartment()
			HttpSession->Department := oOrg:oWSGETDEPARTMENTRESULT:oWSLISTOFDEPARTMENT:OWSDATADEPARTMENT
			nPTotal 		         := oOrg:oWSGETDEPARTMENTRESULT:nPagesTotal
		Else
			HttpSession->Department := {}
			nPTotal 		         := 1
		EndIf
			    
		HttpCTType("text/html; charset=ISO-8859-1")
		cHtml := ExecInPage( "F0100302" )	
		
	WEB EXTENDED END
	
Return cHtml