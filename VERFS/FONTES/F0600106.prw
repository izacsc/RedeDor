#include "protheus.ch"
/*/F0600106
Fun��o respons�vel pelas integra��es de RH
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cNomAlias, character, Nome da Tabela
@project	MAN0000007423040_EF_001,MAN0000007423040_EF_002,
@project	MAN0000007423040_EF_003,MAN0000007423040_EF_005,MAN0000007423040_EF_007
@return Nil
/*/
User Function F0600106(cNomAlias)
Local cFuncF001	:= "GPEA010|TRMA100|RSPM001"
Local cFuncF002	:= "GPEA240|MDTA685"
Local cFuncF003	:= "GPEA010|TRMA100|PONA160|GPER200|GPEM690|CSAA080"
Local cFuncF005	:= "GPEM060|GPEM030"
Local cFuncF007	:= "GPEA180"

If AllTrim(cNomAlias) == "SRA" 
	If AllTrim(FunName()) $ cFuncF001 //Admiss�o
	//	FldSettrigger(RA_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600100(x,y)}) -- Nairan: O PE CheckFile parou de funcionar na integra��o da SRA. Foi desenvolvido o PE 
	EndIf
Endif


If AllTrim(cNomAlias) == "SR8" //Afastamento
	If AllTrim(FunName()) $ cFuncF002
		FldSettrigger(R8_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600200(x,y)})
	Endif
Endif

If AllTrim(cNomAlias) == "SR9" //Carga Hor�ria/Sindicato
	If AllTrim(FunName()) $ cFuncF003
		FldSettrigger(R9_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600300(x,y,"SR9")})
	EndIf
Endif

If AllTrim(cNomAlias) == "SR7"//Fun��o
	If AllTrim(FunName()) $ cFuncF003
		FldSettrigger(R7_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600300(x,y,"SR7")})
	EndIf
Endif
/*
If AllTrim(cNomAlias) == "SR3"//Sal�rio
	If AllTrim(FunName()) $ cFuncF003
		FldSettrigger(R3_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600300(x,y,"SR3")})
	Endif
Endif
*/
If AllTrim(cNomAlias) == "SPF"//Turno
	If AllTrim(FunName()) $ cFuncF003
		FldSettrigger(PF_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600300(x,y,"SPF")})
	Endif
Endif

If AllTrim(cNomAlias) == "SRH"//F�rias
	If AllTrim(FunName()) $ cFuncF005
		FldSettrigger(RH_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600500(x,y)})
	Endif
Endif

If AllTrim(cNomAlias) == "SRE"//Transfer�ncia
	If AllTrim(FunName()) $ cFuncF007
		FldSettrigger(RE_FILIAL,8192 + 16384 + 32768,{|X,Y| U_F0600700(x,y)})
	Endif
Endif

Return .T.


/*/F0600100
Integra��o com o Cadastro de Funcion�rio
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cCampo, character, Nome do Campo
@param nModo, num�rico, Tipo de Opera��o
@project	MAN0000007423040_EF_001
@return Nil
/*/
User Function F0600100(cCampo,nModo)
Local nCodInsert 	:= 8192
Local aArea      	:= GetArea()
Local lRet			:= .T.
Local cOper			:= "UPSERT"

If nModo == nCodInsert
	
	U_F0600901("F0600101",; // cFunc 
				SRA->(RECNO()),; // nRecno 
				"SRA",; // cAliasTrb 
				SRA->RA_FILIAL + SRA->RA_MAT,; // cChave 
				"",; // cObs
				CTOD(""),; // Data de envio
				cOper) // Operacao
Endif

Return lRet

/*/F0600200
Integra��o com o Cadastro de Afastamento
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cCampo, character, Nome do Campo
@param nModo, num�rico, Tipo de Opera��o
@project	MAN0000007423040_EF_002
@return Nil
/*/
User Function F0600200(cCampo,nModo)
Local nCodDelete 	:= 32768
Local aArea      	:= GetArea()
Local lRet			:= .T.
Local cOper			:= Iif(nModo == nCodDelete, "DELETE", "UPSERT")

//If nModo != nCodDelete
	
	U_F0600901("F0600201",; // cFunc 
				SR8->(RECNO()),; // nRecno 
				"SR8",; // cAliasTrb 
				SR8->R8_FILIAL + SR8->R8_MAT + DTOS(SR8->R8_DATAINI) + SR8->R8_TIPO,; // cChave 
				"",; // cObs
				CTOD(""),; // Data de envio
				cOper) // Operacao
//Endif

Return lRet

/*/F0600300
Cadastro de Altera��o de Dados Contratuais
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cCampo, character, Nome do Campo
@param nModo, num�rico, Tipo de Opera��o
@param cAliasTrb, character, Alias utilizado
@project	MAN0000007423040_EF_003
@return Nil
/*/
User Function F0600300(cCampo,nModo,cAliasTrb)
Local nCodDelete 	:= 32768
Local aArea      	:= GetArea()
Local lRet			:= .T.
Local cOper			:= Iif(nModo == nCodDelete, "DELETE", "UPSERT")

//If nModo != nCodDelete
	//lRet := U_F0600301(cAliasTrb)//Realiza a Chamada da fun��o de integra��o F06003.
	
	// Troca de Turno.
	If cAliasTrb == "SPF"
	
		U_F0600901("F0600301",; // cFunc 
					SPF->(RECNO()),; // nRecno 
					"SPF",; // cAliasTrb 
					SPF->PF_FILIAL + SPF->PF_MAT + DTOS(SPF->PF_DATA),; // cChave 
					"",; // cObs
					CTOD(""),; // Data de envio
					cOper) // Operacao
								
	// Troca de Fun��o.
	ElseIf cAliasTrb == "SR7"
	
		U_F0600901("F0600301",; // cFunc 
					SR7->(RECNO()),; // nRecno 
					"SR7",; // cAliasTrb 
					SR7->R7_FILIAL + SR7->R7_MAT + DTOS(SR7->R7_DATA) + SR7->R7_TIPO,; // cChave 
					"",; // cObs
					CTOD(""),; // Data de envio
					cOper) // Operacao
					
	// Troca de Sindicato/Carga Hor�ria.
	ElseIf cAliasTrb == "SR9"
	
		U_F0600901("F0600301",; // cFunc 
					SR9->(RECNO()),; // nRecno 
					"SR9",; // cAliasTrb 
					SR9->R9_FILIAL + SR9->R9_MAT + SR9->R9_CAMPO + DTOS(SR9->R9_DATA),; // cChave 
					"",; // cObs
					CTOD(""),; // Data de envio
					cOper) // Operacao
					
	EndIf
						
//Endif

Return lRet

/*/F0600500
Cadastro de F�rias
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cCampo, character, Nome do Campo
@param nModo, num�rico, Tipo de Opera��o
@project	MAN0000007423040_EF_005
@return Nil
/*/
User Function F0600500(cCampo,nModo)
Local nCodInsert := 8192
Local nCodDelete := 32768
Local aArea      := GetArea()
Local lRet		:= .T.
Local cOper		:= Iif(nModo == nCodDelete, "DELETE", "UPSERT")

If nModo == nCodInsert .OR. nModo == nCodDelete
	
	U_F0600901("F0600501",; // cFunc 
				SRH->(RECNO()),; // nRecno 
				"SRH",; // cAliasTrb 
				SRH->RH_FILIAL + SRH->RH_MAT + DTOS(SRH->RH_DATAINI),; // cChave 
				"",; // cObs
				CTOD(""),; // Data de envio
				cOper) // Operacao
Endif

Return lRet

/*/F0600700
Integra��o com o Cadastro de Funcion�rio
@type User function
@author nairan.silva
@since 01/11/2016
@version 12.7
@param cCampo, character, Nome do Campo
@param nModo, num�rico, Tipo de Opera��o
@project	MAN0000007423040_EF_007
@return Nil
/*/
User Function F0600700(cCampo,nModo)
Local nCodInsert := 8192
Local aArea      := GetArea()
Local lRet			:= .T.
Local cOper			:= "UPSERT"

If nModo == nCodInsert

	If SRE->RE_CCD <> SRE->RE_CCP
		
		U_F0600901("F0600301",; // cFunc 
					SRE->(RECNO()),; // nRecno 
					"SRE",; // cAliasTrb 
					SRE->RE_FILIALD + SRE->RE_MATD,; // cChave 
					"",; // cObs
					CTOD(""),; // Data de envio
					cOper) // Operacao
		
	EndIf
	
	If SRE->RE_FILIALD <> SRE->RE_FILIALP .OR.;
		SRE->RE_DEPTOD <> SRE->RE_DEPTOP .OR.; 
		SRE->RE_PROCESD <> SRE->RE_PROCESP

		U_F0600901("F0600701",; // cFunc 
					SRE->(RECNO()),; // nRecno 
					"SRE",; // cAliasTrb 
					SRE->RE_FILIALD + SRE->RE_MATD,; // cChave 
					"",; // cObs
					CTOD(""),; // Data de envio
					cOper) // Operacao
					
	EndIf
				
Endif

Return lRet
