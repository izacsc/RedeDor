#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0700902
Valida��o do CNPJ do fabricante
@author Fernando Carvalho
@since 20/01/2017
@Project MAN0000007423041_EF_007
@param cCGC, caracter, C�digo CGC
/*/
User Function F0700902(cCGC)
	Local lRet 	:= .T.
	Local aArea	:= GetArea()
	
	P13->(DbSetOrder(2))//P13_FILIAL+P13_CGC
	If P13->(DbSeek(xFilial("P13")+SubStr(cCGC,1,8)))
		
		Help("",1, "CNPJ J� EXISTE!", "O CNPJ informado j� foi cadastrado para o fabricante:" ,;
				"O CNPJ informado j� foi cadastrado para o fabricante:"+ CHR(13) + CHR(10)+;
				 "C�digo:   " + P13->P13_COD + CHR(13) + CHR(10)+;
				"Descri��o:" + P13->P13_DESCR , 3, 0)
		
		lRet := .F.		
	EndIf			
	RestArea(aArea)
Return lRet






