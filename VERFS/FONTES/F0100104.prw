#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#Include 'TopConn.ch'

/*
{Protheus.doc} F0100104
Valida��o e Execu��o da Rotina de Medi��o Automatizada - Automatica
@Author		Mick William da Silva
@Since		30/06/2016
@Version	P12.7
@Project    MAN00000462901_EF_001
@Param		cNumSol, N�mero da solicita��o de compra.
@Param		cItemSol, Item da solicita��o de compra.
@Param		lFunOri, indica se veio da rotina de aprova��o.
@Return		lRet, Retorna se houve erro e gerou Log, para n�o Setar o Status de Contrato na Solicita��o de Compras(Destino F0100101) 
*/

User Function F0100104(cNumSol,cItemSol,lFunOri,cTipoSC,cCodMSC)
	
	Local aArea		:= GetArea()
	Local aAreaCND	:= CND->(GetArea())
	Local aAreaCNE	:= CNE->(GetArea())
	Local cQuery	:= ""
	Local nI		:= 0
	Local aCabec 	:= {}
	Local aAllCab	:= {}
	Local aItem  	:= {}
	Local aItens	:= {}
	Local lDtIVl	:= .T.
	Local cCodUser 	:= Alltrim(RetCodUsr()) 		 //Retorna o Codigo do Usuario
	Local cNamUser 	:= Alltrim(UsrRetName( cCodUser ))//Retorna o nome do usuario
	Local lDatInv	:= .F.
	Local QRYAIA 	:= GetNextAlias()
	Local QRYCNF 	:= GetNextAlias()
	Local QRYCNE 	:= GetNextAlias()
	Local dData
	Local dComp		:= Substr(dToS(dDataBase),5,2) + "/"  + Substr(dToS(dDataBase),1,4)
	Local lRet		:= .T.
	Local cItem		:= "001"
	Local nValTab	:= 0
	Local aProds		:= {}
	Local aProdGCT	:= {}
	Local aCabCont	:= {}
	Local nx			:= 0
	Local cChaveS		:= ""
	Local cChaveW		:= ""
	Local cTpAP		:= SuperGetMV("FS_XTPSCCA")	//Tipos de solicita��o de compras que N�O ter�o aprova��o de Al�adas
	Local cEstoq	:= ""
	
	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.
	
	If lFunOri //Caso venha de uma solicita��o aprovada
		cChaveS	:= cNumSol+cItemSol
		cChaveW	:= "SC1->(!Eof()) .And. SC1->C1_NUM == '"+cNumSol+"' .And. SC1->C1_ITEM == '"+cItemSol+"' "
	Else
		cChaveS	:= cNumSol
		cChaveW	:= "SC1->(!Eof()) .And. SC1->C1_NUM == '"+cNumSol+"' "
	Endif	
	
	//SC1->( dbGoTop() )
	SC1->(DbSetOrder(1))
	SC1->(DbSeek(xFilial("SC1") + cChaveS ))
	
	While &(cChaveW)
	
		cEstoq := Posicione("P17", 1, xFilial("P17") + SC1->C1_PRODUTO, "P17_ESTOQ")

		aProds := {	xFilial("SC1")	,;	// 01 - Filial
			SC1->C1_NUM				,;		// 02 - N�mero da Solicita��o
			SC1->C1_PRODUTO			,;		// 03 - Produto
			SC1->C1_LOCAL			,;			// 04 - Armaz�m
			SC1->C1_CC				,;			// 05 - Centro de Custo
			cNamUser				,;			// 06 - Nome do Usu�rio
			SC1->C1_XTPSC 			,;		// 07 - C�digo do Tipo de contrato (Tabela SX5)
			SC1->C1_FORNECE			,;		// 08 - Fornecedor
			SC1->C1_LOJA			,;			// 09 - Loja  
			""						,;			// 10 - Contrato
			""						,;			// 11 - Data Inicial Contrato
			""						,;			// 12 - Data Final do Contrato
			SC1->C1_QUANT			,;			// 13 - Quantidade
			SC1->C1_ITEM			,;			// 14 - Item de Compra
			""						,;			// 15 - Pre�o de Tabela
			""						,;			// 16 - Item do Contrato
			SC1->C1_XCODSET			,;			// 17 - Codigo do Setor
			cEstoq}								// 18 - Produto estoc�vel
	
			IF !lFunOri
				RECLOCK("SC1", .F.)
				SC1->C1_XTPSC 	:= cTipoSC
				IF !(Alltrim(cTipoSC) $ cTpAP)
					SC1->C1_APROV 	:= 'B'
				EndIf
				SC1->C1_XMOTIVO := cCodMSC
				SC1->( MSUNLOCK() )
			EndIf	
		//Consulta o produto e devolve a o c�digo da Tabela de Pre�os
		cQuery := "SELECT AIA_FILIAL,AIA_CODTAB,AIA_CODFOR ,AIA_LOJFOR ,AIB_CODPRO,AIA_DATATE,AIB_PRCCOM "
		cQuery += "FROM "+ RetSqlName("AIB") +" AIB "
		cQuery += "LEFT JOIN "+ RetSqlName("AIA") +" AIA ON AIB_FILIAL = AIA_FILIAL AND AIB_CODTAB = AIA_CODTAB "
		cQuery += "WHERE AIA_FILIAL ='"+ SC1->C1_FILIAL +"' AND AIB_CODPRO='"+ SC1->C1_PRODUTO +"' AND AIB.D_E_L_E_T_=' ' AND AIA.D_E_L_E_T_=' ' "
		cQuery += "AND (AIA_DATATE >= '" + DtoS(dDataBase) + "' OR AIA_DATATE = ' ')"
		
		TCQUERY cQuery NEW ALIAS (QRYAIA)
		
		If (QRYAIA)->(EOF())
			U_F0100105("01",aProds) //Produto n�o encontrado em nenhuma Tabela de Pre�os.
			lRet:= .F.
			Exit
		EndIf
		
		lDtIVl := .T.
		dData := IIF(Empty((QRYAIA)->AIA_DATATE),DtoS(dDataBase),(QRYAIA)->AIA_DATATE)
		
		IF dData < DtoS(dDataBase) //verifica data vigente
			lDtIVl := .F.
		EndIF
		
		IF lDtIVl
			cQuery := " SELECT CNA.CNA_CONTRA,CNA.CNA_NUMERO,CN9.CN9_FILIAL,CN9.CN9_DTINIC,CN9.CN9_DTFIM,CNA.CNA_FORNEC, CNA.CNA_LJFORN,CNA.CNA_REVISA,CNA.CNA_XTABPC, CNB.CNB_ITEM  "
			cQuery += " FROM " + RetSQLName("CNA") + " CNA, "
			cQuery += " INNER JOIN " + RetSQLName("CN9") + " CN9 ON CN9.CN9_NUMERO = CNA.CNA_CONTRA AND CN9.CN9_REVISA = CNA.CNA_REVISA "
			cQuery += " INNER JOIN " + RetSQLName("CNB") + " CNB ON CNB.CNB_CONTRA = CN9.CN9_NUMERO AND CN9.CN9_REVISA = CNB.CNB_REVISA AND CNA.CNA_NUMERO = CNB.CNB_NUMERO "
			cQuery += " WHERE "
			cQuery += " CNA.CNA_FILIAL = '"+ (QRYAIA)->AIA_FILIAL +"' AND "
			cQuery += " CN9.CN9_FILIAL = '"+ (QRYAIA)->AIA_FILIAL +"' AND "
			cQuery += " CN9.CN9_SITUAC =  '05' AND "
			cQuery += " CNA.CNA_SALDO  > 0 AND "
			cQuery += " CNA.D_E_L_E_T_ = ' ' AND "
			cQuery += " CN9.D_E_L_E_T_ = ' ' AND "
			cQuery += " CNB.D_E_L_E_T_ = ' ' AND "
			cQuery += " CNA.CNA_XTABPC = '"+ (QRYAIA)->AIA_CODTAB +"' AND "
			cQuery += " CNB.CNB_PRODUT = '"+ (QRYAIA)->AIB_CODPRO +"' AND "
			cQuery += " CNA.CNA_FORNEC =  '"+ (QRYAIA)->AIA_CODFOR +"' AND   "
			cQuery += " CNA.CNA_LJFORN = '"+ (QRYAIA)->AIA_LOJFOR  +"'   "
			
			
			cQuery := ChangeQuery( cQuery )
				
			//TCQUERY cQuery NEW ALIAS (QRYCNF)
			dbUseArea(.T.,"TOPCONN", TCGenQry(,,cQuery),QRYCNF, .F., .T.)
			Count To nRec
			(QRYCNF)->(DbGoTop())
			
			If (QRYCNF)->(EOF())
				lRet:= .F.
				Exit
			EndIf
			
			If nRec > 1
				U_F0100105("08",aProds) //"Produto Encontrado em Mais de Um Contrato Vigente"
				lRet:= .F.
				Exit			
			Endif
			
			IF (QRYCNF)->CN9_DTFIM <= DtoS(dDataBase) .And. !Empty((QRYCNF)->CN9_DTFIM)
				aProds[8]  := (QRYCNF)->CNA_FORNEC
				aProds[9]  := (QRYCNF)->CNA_LJFORN
				aProds[10] := (QRYCNF)->CNA_CONTRA
				aProds[11] := StoD((QRYCNF)->CN9_DTINIC)
				aProds[12] := StoD((QRYCNF)->CN9_DTFIM)
				
				lDatInv:= .T.
			EndIf
			
			IF !lDatInv
				aProds[10] := (QRYCNF)->CNA_CONTRA
				aProds[15] := (QRYAIA)->AIB_PRCCOM
				aProds[16] := (QRYCNF)->CNB_ITEM
			Else
				U_F0100105("04",aProds) //Contrato com data de Vig�ncia fora da Validade.
				lRet:= .F.
				Exit
			Endif
			
			If Len(aCabCont) == 0
				
				aAdd(aCabec,{"CND_CONTRA",(QRYCNF)->CNA_CONTRA,NIL})
				aAdd(aCabec,{"CND_REVISA",(QRYCNF)->CNA_REVISA,NIL})
				aAdd(aCabec,{"CND_COMPET",dComp				  ,NIL})
				aAdd(aCabec,{"CND_NUMERO",(QRYCNF)->CNA_NUMERO,NIL})
				aAdd(aCabec,{"CND_FORNEC",(QRYCNF)->CNA_FORNEC,NIL})
				aAdd(aCabec,{"CND_LJFORN",(QRYCNF)->CNA_LJFORN,NIL})
				aAdd(aCabec,{"CND_FILIAL",(QRYCNF)->CN9_FILIAL,NIL})
				aAdd(aCabec,{"CND_NUMMED","",NIL})
				aAdd(aCabec,{"CND_PARCEL","",NIL})
				
				Aadd(aCabCont,aCabec)
			Else
				For nx:=1 to len(aCabCont)		
					If aScan(aCabCont[nx],{|x| AllTrim(x[2])==(QRYCNF)->CNA_CONTRA}) == 0 
						
						aAdd(aCabec,{"CND_CONTRA",(QRYCNF)->CNA_CONTRA,NIL})
						aAdd(aCabec,{"CND_REVISA",(QRYCNF)->CNA_REVISA,NIL})
						aAdd(aCabec,{"CND_COMPET",dComp				  ,NIL})
						aAdd(aCabec,{"CND_NUMERO",(QRYCNF)->CNA_NUMERO,NIL})
						aAdd(aCabec,{"CND_FORNEC",(QRYCNF)->CNA_FORNEC,NIL})
						aAdd(aCabec,{"CND_LJFORN",(QRYCNF)->CNA_LJFORN,NIL})
						aAdd(aCabec,{"CND_FILIAL",(QRYCNF)->CN9_FILIAL,NIL})
						aAdd(aCabec,{"CND_NUMMED","",NIL})
						aAdd(aCabec,{"CND_PARCEL","",NIL})
						
						Aadd(aCabCont,aCabec)
					Endif
				Next
			EndIf
				
			Aadd(aProdGCT,aProds)
		Else
			U_F0100105("02",aProds) //Produto com a Data Vigente vencida.
			lRet:= .F.
			Exit
		Endif		
		 
		aCabec := {} 
		(QRYAIA)->(DbCloseArea())
		(QRYCNF)->(dbCloseArea())
		SC1-> (DbSkip())
	EndDo
	
	If Empty(aCabCont)
		U_F0100105("03",aProds) //"N�o foi possivel realizar a Medi��o, pois a Tabela de Pre�o n�o foi Encontrado em nenhum Contrato !"
		lRet:= .F.		
	EndIf
	
	If lRet
		lRet := U_F0100111(aCabCont,aProdGCT)
	EndIf
	
	
	//Atualiza solicita��es de Compra.
	SC1->(DbSetOrder(1))
	SC1->(DbSeek(xFilial("SC1") + cChaveS ))
	
	While &(cChaveW)
		IF lRet
			RECLOCK("SC1", .F.)
			SC1->C1_FLAGGCT := '1'
			SC1->C1_XITMED	:= "Utilizado pelo GCT"
			SC1->( MSUNLOCK() )
		ELSE
			RECLOCK("SC1", .F.)
			SC1->C1_XITMED	:=	"Medi��o n�o Realizada"
			SC1->( MSUNLOCK() )
		EndIF
		SC1->(DbSkip())	
	EndDo
	
	RestArea(aAreaCNE)
	RestArea(aAreaCND)
	RestArea(aArea)

Return lRet

/*/{Protheus.doc} F0100111
Realiza a rotina autom�tica para gera��o de contratos.
@Author	Nairan Alves Silva
@Since		25/08/2016
@Version	P12.7
@Project	MAN00000462901_EF_001
@aCabCont	Cabe�alho dos contratos encontrados
@aProdGCT	Produtos encontrados na solicita��o de Compra	
@Return	lRet, Retorna se houve erro e gerou Log 
/*/
User Function F0100111(aCabCont,aProdGCT)

Local aItens 	:= aClone(aProdGCT)
Local aCabGCT	:= aClone(aCabCont)
Local aItGCT	:= {}
Local aItsGCT	:= {}
Local aItensCNB	:= {}
Local nPos		:= 0
Local nX		:= 1
Local nY		:= 1
Local nZ		:= 1
Local nI		:= 0
Local cCont		:= ""
Local cKey		:= ""
Local lRet		:= .T.
Local lAtu		:= .T.
Local cKeyBkp	:= ""
Local cDoc   	:= ""

If Len(aItens) < 0
	Alert("O conte�do dos itens est� incorreto.")
	lRet := .F.
EndIf

Begin Transaction

If lRet

	// Setor + Produto estoc�vel + Contrato
	aSort(aItens, , , {|x, y| x[17]+x[18]+x[10] < y[17]+y[18]+y[10]})
	cCont := aItens[01][10]
	cKey := aItens[01][17]+aItens[01][18]+aItens[01][10]
	
	For nX := 1 To Len(aItens)
	
		lAtu	:= .T.
		If cKey == aItens[nX][17]+aItens[nX][18]+aItens[nX][10]
		
			For nZ := 1 To Len(aItsGCT)
				nPos := aScan(aItsGCT[nZ],{|x| AllTrim(x[1])+AllTrim(x[2])=="CNE_ITEM"+aItens[nX][16]})
				If  nPos > 0
					lAtu	:= .F.
					aItsGCT[nZ][04][02] += aItens[nX][13]
					aItsGCT[nZ][06][02] += aItens[nX][15] * aItens[nX][13]
					Exit
				Endif
			Next
			If lAtu
				AADD(aItGCT,{"CNE_FILIAL"	, aItens[nX][01] , NIL }	)
				AADD(aItGCT,{"CNE_ITEM"		, aItens[nX][16]		, NIL }	)
				AADD(aItGCT,{"CNE_PRODUT"	, aItens[nX][03] , NIL }	)
				AADD(aItGCT,{"CNE_QUANT"		, aItens[nX][13], NIL }	)
				AADD(aItGCT,{"CNE_VLUNIT"	, aItens[nX][15]	, NIL }	)
				AADD(aItGCT,{"CNE_VLTOT"		, aItens[nX][15] * aItens[nX][13] ,NIL	 }	)
				AADD(aItGCT,{"CNE_DTENT"		, dDataBase, NIL }	)

				AADD(aItsGCT,aItGCT)
			EndIf
			
			aItGCT := {}	
				
		Endif
		
		nPos == 0
		If cKey != aItens[nX][17]+aItens[nX][18]+aItens[nX][10] .Or. nX == Len(aItens)
			For nY := 1 To Len(aCabGCT)
				nPos := aScan(aCabGCT[nY],{|x| AllTrim(x[2])== cCont})
				If nPos > 0
					nPos := nY
					Exit
				Endif
			Next
				
			If nPos > 0
				
				// Preenche o numero da medi��o.
				cDoc := CriaVar("CND_NUMMED")
				aCabGCT[nPos][8][2] := cDoc
				
				// Pega todos os itens da planilha do contrato.
				aItensCNB := GetItemCNB(cCont)
				
				// Preenche os valores dos itens que ser� feita a medi��o.
				For nY := 1 To Len(aItsGCT)
					For nZ := 1 To Len(aItensCNB)
						If aItensCNB[nZ][3][2] == aItsGCT[nY][3][2]		// Produto
							aItensCNB[nZ][4][2] := aItsGCT[nY][4][2]	// Quantidade
							aItensCNB[nZ][5][2] := aItsGCT[nY][5][2]	// Vl Unitario
							aItensCNB[nZ][6][2] := aItsGCT[nY][6][2]	// Vl Total
						EndIf
					Next
				Next
				
				//��������������������������������������������������Ŀ	
				//� Executa rotina automatica para gerar as medicoes �	
				//����������������������������������������������������	
				CNTA120(aCabGCT[nPos], aItensCNB, 3, .F.)
					
				//�����������������������������������������������������Ŀ	
				//� Executa rotina automatica para encerrar as medicoes �	
				//�������������������������������������������������������	
				CNTA120(aCabGCT[nPos], aItensCNB, 6, .F.)

				If lMsErroAuto
					MostraErro()
					For nX := 1 To Len(aItens)
						U_F0100105("05", aItens[nX]) //"Erro na inclusao da Medi��o"
					Next
					lRet:= .F.
					DisarmTransaction()
					Exit
				EndIf			
			
			Endif		
			
			cKeyBkp		:= cKey
			cKey 		:= aItens[nX][17]+aItens[nX][18]+aItens[nX][10]
			aItGCT 	:= {}
			aItsGCT	:= {}
			
			If nX != Len(aItens) .Or. (nX == Len(aItens) .And. cKeyBkp != cKey)
				nX -= 1
			EndIF
		EndIf

		nPos := 0
	Next
	
Endif
End Transaction
Return lRet

/*/{Protheus.doc} GetItemCNB
(long_description)
@type function
@author alexandre.arume
@since 19/01/2017
@version 1.0
@param cCont, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function GetItemCNB(cCont)
	
	Local aAreaCNB 	:= CNB->(GetArea())
	Local aItem		:= {}
	Local aItens	:= {}
	
	dbSelectArea("CNB")
	CNB->(dbSetOrder(1))
	CNB->(dbSeek(xFilial("CNB") + cCont))
	
	While !CNB->(Eof()) .AND. CNB->CNB_CONTRA == cCont
		
		aAdd(aItem, {"CNE_FILIAL"	, CNB->CNB_FILIAL	, NIL })
		aAdd(aItem, {"CNE_ITEM"		, CNB->CNB_ITEM		, NIL })
		aAdd(aItem, {"CNE_PRODUT"	, CNB->CNB_PRODUTO	, NIL })
		aAdd(aItem, {"CNE_QUANT"	, 0					, NIL })
		aAdd(aItem, {"CNE_VLUNIT"	, 0					, NIL })
		aAdd(aItem, {"CNE_VLTOT"	, 0					, NIL })
		aAdd(aItem, {"CNE_DTENT"	, dDataBase			, NIL })

		aAdd(aItens, aItem)
		aItem := {}
		CNB->(dbSkip())
	End
	
	CNB->(dbCloseArea())
	
	RestArea(aAreaCNB)
	
Return aItens
