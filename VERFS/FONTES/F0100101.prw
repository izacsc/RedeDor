#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RWMAKE.CH"

/*
{Protheus.doc} F0100101()
Rotina de medi��o automatica

@Author			Jose Marcio da Silva Santos / Mick William da Silva
@Since			09/06/2016					/ 07/07/2016
@Version		P12.7
@Project    	MAN00000462901_EF_001
@Param			lOri, Parametro que verifica se a fun��o est� sendo chamada da Aprova��o de Solicita��o de Compras.
@Param			cNumSol, N�mero da Solicita��o de Compras.		
*/

User Function F0100101(lOri,cNumSol)
	Local aArea		:= GetArea()
	Local aAreaSC1	:= SC1->(GetArea())
	Local aAreaSC1x
	Local lMedPar	:= .T.
	Local lProc		:= .T.
	Local lCont		:= .F.
	Local lAlter	:= .F.
	Local cTpSC		:= SuperGetMV("FS_XTPSCME")	//Tipos que N�O ser�o considerados pela rotina de Medi��o Automatizada
	Local cTpAP		:= SuperGetMV("FS_XTPSCCA")	//Tipos de solicita��o de compras que N�O ter�o aprova��o de Al�adas
	Local cAlsQry	:= GetNextAlias()
	Local aTpSC		:= {}
	Local cQuery	:= ""
	Local aProds	:= {}
	Local nI		:= 0
	Local cCodUser 	:= Alltrim(RetCodUsr()) 		  //Retorna o Codigo do Usuario
	Local cNamUser 	:= Alltrim(UsrRetName( cCodUser ))//Retorna o nome do usuario
	Local lGrvGCT	:= .T.
	Local cChaveS	:= ""
	Local cChaveW	:= ""

	
	Private cTipoSC	:= CriaVar("C1_XTPSC")
	Private cDescSC	:= CriaVar("C1_XDESCTP")
	Private cCodMSC	:= CriaVar("C1_XMOTIVO")
	Private cDesMSC	:= CriaVar("C1_XDESMOT")
	
	Default lOri 	:= .F.
	Private lFunOri := IIF(lOri == .T.,.T., .F. )//Verifico se vem da Aprova��o de Solicita��o de Compras.
	
	If ALTERA .And. !lFunOri
		cTipoSC := SC1->C1_XTPSC
		cDescSC := Posicione("SX5",1,xFilial("SX5") + "ZX" + SC1->C1_XTPSC,"X5_DESCRI")
		
		cCodMSC := SC1->C1_XMOTIVO
		cDesMSC := Posicione("SX5",1,xFilial("SX5") + "ZZ" + SC1->C1_XTPSC,"X5_DESCRI")
	
		If MsgYesNo("Deseja alterar o Tipo da Solicita��o de Compras?")
			lAlter := .T.
		EndIf
	EndIf

	If ( INCLUI .Or. lAlter .OR. lFunOri )
		
		While !lCont .And. !lFunOri
			lCont:= F001TPSC()
		EndDo

		If  !lFunOri
			//Verifica Tipos de solicita��o de compras que N�O ter�o aprova��o de Al�adas
			If !(Alltrim(cTipoSC) $ cTpAP)
				//SC1->( dbGoTop() )
				SC1->(DbSetOrder(1))
				SC1->(DbSeek(xFilial("SC1") + cNumSol ))
				aProds := {	xFilial("SC1")	,;	// 01 - Filial
					SC1->C1_NUM				,;		// 02 - N�mero da Solicita��o
					SC1->C1_PRODUTO			,;		// 03 - Produto
					SC1->C1_LOCAL			,;			// 04 - Armaz�m
					SC1->C1_CC				,;			// 05 - Centro de Custo
					cNamUser				,;			// 06 - Nome do Usu�rio
					SC1->C1_XTPSC 			,;		// 07 - C�digo do Tipo de contrato (Tabela SX5)
					SC1->C1_FORNECE			,;		// 08 - Fornecedor
					SC1->C1_LOJA			,;			// 09 - Loja  
					""						,;			// 10 - Contrato
					""						,;			// 11 - Data Inicial Contrato
					""						,;			// 12 - Data Final do Contrato
					SC1->C1_QUANT			,;			// 13 - Quantidade
					SC1->C1_ITEM			,;			// 14 - Item de Compra
					""						,;			// 15 - Pre�o de Tabela
					""}									// 16 - Item do Contrato
				//Grava LOG
				U_F0100105("07",aProds)
				If lMedPar
					lMedPar := .F.
				EndIf
			EndIf
			
		EndIf
			
		//Verifica Tipos que N�O ser�o considerados pela rotina de Medi��o Automatizada
		If Alltrim(cTipoSC) $ cTpSC
			//SC1->( dbGoTop() )
			SC1->(DbSetOrder(1))
			SC1->(DbSeek(xFilial("SC1") + cNumSol ))
			aProds := {	xFilial("SC1")	,;	// 01 - Filial
				SC1->C1_NUM				,;		// 02 - N�mero da Solicita��o
				SC1->C1_PRODUTO			,;		// 03 - Produto
				SC1->C1_LOCAL			,;			// 04 - Armaz�m
				SC1->C1_CC				,;			// 05 - Centro de Custo
				cNamUser				,;			// 06 - Nome do Usu�rio
				SC1->C1_XTPSC 			,;		// 07 - C�digo do Tipo de contrato (Tabela SX5)
				SC1->C1_FORNECE			,;		// 08 - Fornecedor
				SC1->C1_LOJA			,;			// 09 - Loja  
				""						,;			// 10 - Contrato
				""						,;			// 11 - Data Inicial Contrato
				""						,;			// 12 - Data Final do Contrato
				SC1->C1_QUANT			,;			// 13 - Quantidade
				SC1->C1_ITEM			,;			// 14 - Item de Compra
				""						,;			// 15 - Pre�o de Tabela
				""}									// 16 - Item do Contrato			
			//Grava LOG
			U_F0100105("06",aProds)
			lProc := .F.
			If lMedPar 
				lMedPar := .F.
			EndIf
		EndIf
		
		If lMedPar
			lMedPar:= U_F0100104(cNumSol,SC1->C1_ITEM,lFunOri,cTipoSC,cCodMSC)
		Else
			cChaveS	:= cNumSol
			cChaveW	:= "SC1->(!Eof()) .And. SC1->C1_NUM == '"+cNumSol+"' "
			
			//SC1->( dbGoTop() )
			SC1->(DbSetOrder(1))
			SC1->(DbSeek(xFilial("SC1") + cChaveS ))
			
			While &(cChaveW)
				RECLOCK("SC1", .F.)
				IF !lFunOri
					SC1->C1_XTPSC 	:= cTipoSC
					IF !(Alltrim(cTipoSC) $ cTpAP)
						SC1->C1_APROV 	:= 'B'
					EndIf
					SC1->C1_XMOTIVO := cCodMSC
				EndIf				
				SC1->C1_XITMED	:=	"Medi��o n�o Realizada"
				SC1->( MSUNLOCK() )
				SC1->(DbSkip())	
			EndDo
		EndIf
		
		IF !lMedPar
			MsgAlert("Medi��o N�o Gerada ou Gera��o parcialmente, verifique a Tela de Log!","Medi��o Parcial")
		EndIf
	EndIf
	RestArea(aAreaSC1)
	RestArea(aArea)
	
Return

/*
{Protheus.doc} F001TPSC()
Rotina para montar a Tela de Tipo de Solicita��o de Compras
@Author		Mick William da Silva
@Since		18/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001
@Return		lRet, Retorna True caso cTipoSC tenha sido preenchido, caso contr�rio False.
*/

Static Function F001TPSC()

	Local lRet	:= .T. 
	Local lOk	:= .F.

	DEFINE MSDIALOG oDlg FROM 0,0  TO 180,370 TITLE OemToAnsi("Tipo Solicita��o de Compras") Of oMainWnd PIXEL
			
	@ 012, 012 SAY "Tipo" PIXEL OF oDlg
	@ 011, 050 MSGET cTipoSC F3 "ZX" SIZE 030, 8 VALID VALIDSX5("ZX",cTipoSC)PIXEL OF oDlg
	@ 022, 050 MSGET cDescSC:= Posicione("SX5",1,xFilial("SX5") + "ZX" + cTipoSC,"X5_DESCRI") SIZE 120, 8 PIXEL OF oDlg WHEN .F.
			
	@ 039, 012 SAY "Motivo" PIXEL OF oDlg
	@ 037, 050 MSGET cCodMSC F3 "ZZ" SIZE 030, 8 VALID VALIDSX5("ZZ",cCodMSC) PIXEL OF oDlg
	@ 048, 050 MSGET cDesMSC:= Posicione("SX5",1,xFilial("SX5") + "ZZ" + cCodMSC,"X5_DESCRI") SIZE 120, 8 PIXEL OF oDlg WHEN .F.
			
	@ 070, 110 BMPBUTTON TYPE 01 ACTION (lOk:=.T., IIF( Empty(cTipoSC),MsgAlert("Informe o Tipo da Solicita��o de Compras !"), Close(oDlg)) )
	@ 070, 142 BMPBUTTON TYPE 02 ACTION (lOk:=.F., IIF( Empty(cTipoSC),MsgAlert("Informe o Tipo da Solicita��o de Compras !"), Close(oDlg)) )
			
	ACTIVATE MSDIALOG oDlg
		
	IF !lOk .OR. Empty(cTipoSC)
		MsgAlert("Informe o Tipo da Solicita��o de Compras !")
		lRet	:= .F.
	EndIf
		
Return( lRet )
/////////////////////////////////////////
// Valida as informa��es na tabela SX5 //
/////////////////////////////////////////
Static Function VALIDSX5(cTab,cInfo)
Local lRet := .T.
Local aAreaSX5	:= SX5->(GetArea())

If !SX5->(DbSeek(FWXFilial("SX5")+cTab+cInfo))
	Aviso("Registro n�o Encontrado", "O registro "+cInfo+" n�o foi encontrado na tabela "+cTab+"." )
	lRet := .F.
Endif

RestArea(aAreaSX5)
Return lRet