#include "protheus.ch"
#include "fwmvcdef.ch"

#DEFINE STAT_APROVADO "4"

/*/{Protheus.doc} F0500101

@type function
@author 		alexandre.arume
@since 			10/10/2016
@version 		1.0
@Project    	MAN0000007423039_EF_001
@return 		${return}, ${return_description}

/*/
User Function F0500101()
	
	Local oBrowse
	
	oBrowse := FWmBrowse():New()
	oBrowse:SetAlias("P10")
	oBrowse:SetMenuDef("F0500101")
	oBrowse:SetDescription("Solicitacao de Desligamento")
	oBrowse:SetSeeAll(.F.)
	
	oBrowse:AddLegend('P10->P10_STATUS == "1"',  "YELLOW"		, "Solicitado")
	oBrowse:AddLegend('P10->P10_STATUS == "2"',  "GREEN"		, "Aprovado")
	oBrowse:AddLegend('P10->P10_STATUS == "3"',  "RED"		, "Rejeitado")
	oBrowse:AddLegend('P10->P10_STATUS == "4"',  "BLUE"		, "Aguardando Efetivacao RH")
	
	oBrowse:SetFilterDefault('P10_MATSOL == "'+__cUserId+'" .OR. "'+__cUserId+'" == "000000"')
	
	oBrowse:Activate()
	
Return

/*/{Protheus.doc} MenuDef

@type function
@author alexandre.arume
@since 10/10/2016
@version 1.0
@return ${return}, ${return_description}

/*/
Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina Title 'Pesquisar'                     Action 'PesqBrw'          	OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'                    Action 'VIEWDEF.F0500101' 	OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Incluir'                       Action 'VIEWDEF.F0500101' 	OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Imprimir'                      Action 'VIEWDEF.F0500101' 	OPERATION 8 ACCESS 0
	ADD OPTION aRotina Title 'Banco Espec�fico - Visualizar' Action 'U_F0400101(1)'    	OPERATION 0 ACCESS 0
	ADD OPTION aRotina Title 'Banco Espec�fico - Incluir'    Action 'U_F0400101(3)'    	OPERATION 0 ACCESS 0
	ADD OPTION aRotina Title 'Banco Espec�fico - Excluir'    Action 'U_F0400101(5)'    	OPERATION 0 ACCESS 0
	ADD OPTION aRotina Title "Legenda" 							Action 'U_F0500109' 		 	OPERATION 6 ACCESS 0
	
Return aRotina

/*/{Protheus.doc} ModelDef

@type function
@author alexandre.arume
@since 10/10/2016
@version 1.0
@return ${return}, ${return_description}

/*/
Static Function ModelDef()
	
	Local oModel	:= Nil							// Modelo de dados
	Local oStruP10 	:= FWFormStruct(1, "P10")		// Estrutura da tabela P10
	Local bPosVal	:= {|oModel| F05001Val(oModel)}	// Bloco de valida��o da P10
	
	oStruP10:RemoveField("P10_FILIAL")
	
	oModel := MPFormModel():New("F05001M", /*{|oModel|F0500199(oModel)}*/, bPosVal /*bPosValidacao*/, /*bCommit*/, /*bCancel*/)
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	oModel:AddFields("P10MASTER", /*cOwner*/, oStruP10, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/)
	
	// Adiciona a descricao do Modelo de Dados
	oModel:SetDescription("Modelo de Dados de Solicitacao de Desligamento")
	
	// Adiciona a descricao do Componente do Modelo de Dados
	oModel:GetModel("P10MASTER"):SetDescription("Dados de Solicitacao de Desligamento")
	
	oModel:SetPrimaryKey({"P10_FILIAL", "P10_COD"})
	
	oStruP10:SetProperty('P10_MATRIC', MODEL_FIELD_VALID, {|| U_F0500102(oModel)})
	oStruP10:SetProperty('P10_CODRES', MODEL_FIELD_VALID, {|| U_F0500103(oModel)})
	
Return oModel

/*/{Protheus.doc} ViewDef

@type function
@author alexandre.arume
@since 10/10/2016
@version 1.0
@return ${return}, ${return_description}

/*/
Static Function ViewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	Local oModel   := FWLoadModel("F0500101")
	Local oStruP10 := FWFormStruct(2, "P10")
	Local oView
	
	oStruP10:RemoveField("P10_MATSOL")
	
	// Cria o objeto de View
	oView := FWFormView():New()
	
	// Define qual o Modelo de dados ser� utilizado
	oView:SetModel(oModel)
	
	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField("VIEW_P10", oStruP10, "P10MASTER")
	
	// Criar um "box" horizontal para receber algum elemento da view
	oView:CreateHorizontalBox("TELA" , 100)
	
	// Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView("VIEW_P10", "TELA")
	
Return oView

/*/{Protheus.doc} F05001Val
Valida se o tipo de rescis�o esta cadastrada na tabela TP Rescis�es Vs. Vis�es.
@type function
@author alexandre.arume
@since 14/10/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function F05001Val(oModel)
	
	Local lRet 		:= .F.							// Variavel de retorno
	Local aAreaRCC 	:= RCC->(GetArea())				// Armazena area RCC
	Local oMdl 		:= oModel:GetModel("P10MASTER")	// Modelo de dados da rotina
	Local cCodRes		:= oMdl:GetValue("P10_CODRES")	// Codigo da rescis�o
	Local cCodTab		:= "U005"						// Codigo da tabela RCC
	Local cMatric		:= oMdl:GetValue("P10_MATRIC")	// Matricula do funcionario
	Local cFilOri		:= xFilial("RCC")				// Filial de Origem
	Local cTipo		:= "5"
	Local cOrigem		:= "U_F0500101"
	Local cAssunto		:= ""
	Local nOpera 		:= oMdl:GetOperation()
	Local cBody   		:= ""
	Local cEmail  		:= ""
	Local cNumSolic   := ""
	Local nNivel	:= 0
	Local cCod		:= ""
	Local cFilResp	:= ""
	Local cMatResp	:= ""
	Local cCodVis	:= ""
	Local aMatInc	:= {}
	Local aCampos	:= {"P10_FILIAL", "P10_MATRIC", "P10_DTSOLI", "P10_CODRES", "TMP_DESRES", "P10_MOTIVO","P10_DTDEMI"}
	Local aDados	:= {xFilial("P10"),;
						cMatric,;
						DTos(oMdl:GetValue("P10_DTSOLI")),;
						cCodRes,;
						FDESCRCC("S043", IF( !EMPTY(cCodRes), cCodRes,""), 1, 2, 3, 30),;
						oMdl:GetValue("P10_MOTIVO"),;
						DTos(oMdl:GetValue("P10_DTDEMI"))}
	
	If ! Empty(cCodRes)
		
		
		dbSelectArea("RCC")
		RCC->(dbSetOrder(1))
		RCC->(dbSeek(xFilial("RCC") + cCodTab))
		
		While !RCC->(Eof()) .AND. RCC->RCC_FILIAL + RCC->RCC_CODIGO == xFilial("RCC") + cCodTab
			
			If RCC->RCC_FILIAL + RCC->RCC_CODIGO == xFilial("RCC") + cCodTab .AND. Alltrim(Substr(RCC->RCC_CONTEUDO, 1, 2)) == AllTrim(cCodRes)
				cTipo := Right(AllTrim(RCC->RCC_CONTEUDO),1)
				cCodVis:= Substr(RCC->RCC_CONTEUDO,33,6)
				lRet := .T.
				Exit
				
			EndIf
			
			RCC->(dBSkip())
			
		EndDo
		
	EndIf
	
	RestArea(aAreaRCC)
	If nOpera != 5
		If !lRet
			MsgAlert("Rescis�o n�o cadastrada na tabela TP Rescis�es vs. Vis�es.", "Aten��o")
		Else
			aMatInc := U_GetInfMat()
		    nNivel := SelectVisao( cCod, @cFilResp, @cMatResp, cMatric, cTipo, @cCodVis, aDados, aMatInc )
		    lRet := U_F0500199( cMatric, cCodVis, 1 )
		    If lRet
		    
					// Salva nas tabelas RH3 e RH4 para utiliza��o do fluxo padr�o do Portal.
					cNumSolic :=  U_F0500105(cMatric, cFilOri, cCodRes, cTipo, cOrigem, aCampos, aDados, @cEmail , cCodVis)
					
					If !Empty( cNumSolic )
						
						// Indicadores
						If FindFunction("U_F0500201")
							U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"041") //Monitoramento de Desligamento de Funciton�rios
							U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"042") //Solicita��o de Desligamento Aberta
						EndIf
						
						// Atualiza a Solicita��o
						oMdl:SetValue("P10_CODRH3",cNumSolic)
						// Envia o primeiro e-mail .
						cAssunto := "Solicita��o de Desligamento de Funcion�rio: "+cNumSolic
						
						cBody := '<html><body><pre>'+CRLF
						cBody += '<b>Prezado,</b>'+CRLF
						cBody += "Solicita��o de desligamento de funcion�rio N� <b>"+cNumSolic+"</b> aguardando sua aprova��o/reprova��o no portal de Rh."+CRLF
						cBody += '</pre></body></html>'
						
						If U_F0200304( cAssunto, cBody, cEmail )
							MsgAlert("O aviso da solicita��o  N� <b>"+cNumSolic+"</b> foi enviado com sucesso.","Aten��o")
						Else
							//				MsgAlert("Ocorreu um erro no envio do aviso da solicita��o.","Aten��o")
						EndIf
						
					Else
						lRet := .F.
					EndIf
			Endif
		EndIf
	EndIf
	
	
	
	
Return( lRet )

/*/{Protheus.doc} F0500102
Valida��o do funcion�rio.
@type function
@author 	alexandre.arume
@since 		14/10/2016
@version 	1.0
@Project    	MAN0000007423039_EF_001
@Param		oModel, objeto, Modelo de dados
@return 	${return}, ${return_description}

/*/
User Function F0500102(oModel)
	
	Local lRet			:= .T.							// Variavel de retorno
	Local oMld 		:= oModel:GetModel("P10MASTER")	// Modelo de dados da rotina
	Local cCodFunc		:= oMld:GetValue("P10_MATRIC")	// Codigo do funcionario
	Local aAreaSRA 	:= SRA->(GetArea())				// Armazena area SRA
	Local aAreaP10 	:= P10->(GetArea())				// Armazena area P10
	
	If ! Empty(cCodFunc)
		
		dbSelectArea("SRA")
		SRA->(dbSetOrder(1))
		If !(lRet := SRA->(dbSeek(xFilial("SRA") + cCodFunc)))
			MsgAlert("Funcion�rio inexistente.", "Aten��o")
		EndIf
		
		dbSelectArea("P10")
		P10->(dbSetOrder(2))
		If P10->(dbSeek(xFilial("P10") + cCodFunc))
			If P10->P10_STATUS <> "3"
				MsgAlert("Funcion�rio j� possui solicita��o de desligamento.", "Aten��o")
				lRet := .F.
			EndIf
		EndIf
		
	EndIf
	
	RestArea(aAreaSRA)
	RestArea(aAreaP10)
	
Return lRet

/*/{Protheus.doc} F0500103
Fun��o acionada na valida��o do campo Codigo de Rescis�o para preenchimento do campo Descri��o da Rescis�o.
@type function
@author alexandre.arume
@since 14/10/2016
@version 1.0
@Project    	MAN0000007423039_EF_001
@Param		oModel, objeto, Modelo de dados
@return ${return}, ${return_description}

/*/
User Function F0500103(oModel)
	
	Local lRet 		:= .F.							// Variavel de retorno
	Local aAreaRCC 	:= RCC->(GetArea())				// Armazena area RCC
	Local oMld 		:= oModel:GetModel("P10MASTER")	// Modelo de dados da rotina
	Local cTpRes		:= oMld:GetValue("P10_CODRES")	// Codigo da 	rescis�o
	Local cCodTab		:= "S043"						// Codigo da tabela RCC
	Local cDesc		:= ""							// Descri��o da rescis�o
	
	If ! Empty(cTpRes)
		
		dbSelectArea("RCC")
		RCC->(dbSetOrder(1))
		RCC->(dbSeek(xFilial("RCC") + cCodTab))
		
		While !RCC->(Eof()) .AND. RCC->RCC_FILIAL + RCC->RCC_CODIGO == xFilial("RCC") + cCodTab
			
			If RCC->RCC_FILIAL + RCC->RCC_CODIGO == xFilial("RCC") + cCodTab .AND. Alltrim(Substr(RCC->RCC_CONTEUDO, 1, 2)) == AllTrim(cTpRes)
				
				cDesc := Alltrim(Substr(RCC->RCC_CONTEUDO, 3, 30))
				oMld:LoadValue("P10_DESRES", cDesc)
				lRet := .T.
				Exit
				
			EndIf
			
			RCC->(dBSkip())
			
		EndDo
		
	EndIf
	
	RestArea(aAreaRCC)
	If !lRet
		MsgAlert("Rescis�o n�o cadastrada na tabela TP Rescis�es vs. Vis�es.", "Aten��o")
	EndIf
	
Return( lRet )

/*/{Protheus.doc} F0500104
Processo de aprova��o.
@type function
@author alexandre.arume
@since 21/10/2016
@version 1.0
@Project    	MAN0000007423039_EF_001
@Param		oModel, objeto, Modelo de dados
@return ${return}, ${return_description}

/*/
User Function F0500104( oModel )
	
	Local lRet 		:= .F.
	Local aArea       := GetArea()
	Local oModelRH3	:= oModel:GetModel("RH3MASTER")
	Local cMatric		:= oModelRH3:GetValue("RH3_MAT",4)
	Local cSolic		:= oModelRH3:GetValue("RH3_CODIGO",4)
	Local cVisao		:= oModelRH3:GetValue("RH3_VISAO",4)
	Local dDtSol		:= oModelRH3:GetValue("RH3_DTSOLI",4)
	Local cFilP10		:= oModelRH3:GetValue("RH3_FILIAL",4)
	Local cTpRes 		:= FDESCRCC("U005", cVisao, 33, 6, 69, 1)
	Local aInfoP10    := GetP10( cSolic )
	
	Private cTipResPortal 	:= AllTrim(aInfoP10[02])
	
	// Valida se o tipo de rescis�o precisa sde anexo
	If HasAttach( cMatric, cTpRes )
		
		
		DbSelectArea("SRA")
		SRA->(DbSetOrder(1))
		If SRA->(DbSeek( xFilial("SRA") + cMatric ))
			Set Filter to SRA->RA_FILIAL == xFilial("SRA") .And. SRA->RA_MAT == cMatric
			
			// Chama o calculo da rescis�o
			FWMsgRun(,{|| GPEM040() },"Processando...","Iniciando c�lculo de rescis�o..." )
			DbSelectArea("SRG")
			SRG->(DbSetOrder(1))
			
			lRet :=  SRG->( DbSeek( xFilial("SRG") + cMatric  ) )
			
			// Atualiza a P10
			If lRet
				U_F0500110( cSolic, "2" )
			EndIf
			
			SET FILTER TO
			
			If FindFunction("U_F0500201")
				U_F0500201(cFilP10,cSolic,"047")//Atendido Posto
			EndIf
			
		EndIf
		
	EndIf
	
	RestArea( aArea )
Return( lRet )

/*/{Protheus.doc} F0500105
Salva nas tabelas RH3 e RH4.
@type function
@author 		alexandre.arume
@since 			17/10/2016
@version 		1.0
@Project    	MAN0000007423039_EF_001
@param 			cMatric, character, (Descri��o do par�metro)
@param 			cFilOri, character, (Descri��o do par�metro)
@param 			cCod, character, (Descri��o do par�metro)
@param 			cTipo, character, (Descri��o do par�metro)
@param 			cOrigem, character, (Descri��o do par�metro)
@param 			aCampos, array, (Descri��o do par�metro)
@param 			aDados, array, (Descri��o do par�metro)
@param 			cMailDesr, character, (Descri��o do par�metro)
@param 			cCodVis, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
User Function F0500105(cMatric, cFilOri, cCod, cTipo, cOrigem, aCampos, aDados, cMailDest, cCodVis )
	
	Local cValAux	:= ""
	Local nX		:= 0
	Local cMatResp	:= ""
	Local cFilResp	:= ""
	Local cCodDep	:= ""
	Local nNivel	:= 0
	Local aMatInc := {}
	Local aRH3    := {}
	Local cNumRH3 := ""
	
	Default cEmp := cEmpAnt
	Default cCod := ""
	
	aMatInc := U_GetInfMat()
	If !Empty( aMatInc ) .And. !Empty( aMatInc[01] ) .And. !Empty( aMatInc[02] )
		
		nNivel := SelectVisao( cCod, @cFilResp, @cMatResp, cMatric, cTipo, @cCodVis, aDados, aMatInc )
		
		If !Empty(cFilResp) .And. !Empty(cMatResp)
			
			AADD( aRH3 , {"RH3_MAT"		, cMatric })
			AADD( aRH3 , {"RH3_TIPO"	, " " })//cTipo //"6"	//cTipo
			AADD( aRH3 , {"RH3_ORIGEM"	, cOrigem }) //"U_F0500101" //cOrigem
			AADD( aRH3 , {"RH3_DTSOLI"	, DATE() })
			AADD( aRH3 , {"RH3_VISAO"	, cCodVis })
			AADD( aRH3 , {"RH3_FILINI"	, aMatInc[01] })
			AADD( aRH3 , {"RH3_MATINI"	, aMatInc[02] })
			AADD( aRH3 , {"RH3_EMPAPR"	, cEmp })
			AADD( aRH3 , {"RH3_EMPINI"	, cEmp })
			AADD( aRH3 , {"RH3_EMP"		, cEmp })
			AADD( aRH3 , {"RH3_STATUS"	, "1" })
			AADD( aRH3 , {"RH3_NVLINI"	, nNivel })
			AADD( aRH3 , {"RH3_NVLAPR"	, 0})//IIf(nNivel > 0, nNivel, 99) })
			AADD( aRH3 , {"RH3_FILAPR"	, cFilResp })
			AADD( aRH3 , {"RH3_MATAPR"	, cMatResp })
			AADD( aRH3 , {"RH3_KEYINI"	, "002" })
			AADD( aRH3 , {"RH3_FLUIG"	, 0 })
			AADD( aRH3 , {"RH3_XTPCTM"	, "005" })
			
			// Adiciona a vis�o no historico do RH4
			AADD( aCampos	, "RH3_VISAO" )
			AADD( aDados	, cCodVis )
			
			cNumRH3 := U_F0500106( 3, aRH3 , aCampos, aDados )
			
			cMailDest := Posicione("SRA",1,aMatInc[01] + aMatInc[02],"RA_EMAIL")
			
		Else
			
			Help("",1, "Help", "Vis�o X Respons�vel(F0500105_01)", "N�o foi localizado o aprovador associado a vis�o "+cCodVis+"!" , 3, 0)
			
		EndIf
	Else
		
	EndIf
	
Return( cNumRH3 )



/*/{Protheus.doc} F0500106
Salva nas tabelas RH3 e RH4.
@type function
@author 		Roberto Souza
@since 			10/11/2016
@version 		1.0
@Project    	MAN0000007423039_EF_001
@param 			nOpc, numeric, (Descri��o do par�metro)
@param 			aDadosRH3, array,	aDadosRH3[??][01] - Nome do campo
aDadosRH3[??][02] - Conteudo do campo
@param 			aCposRH4, array, Campos da RH4
@param 			aDadosRH4, array, Dados da RH4

@return ${return}, ${return_description}

/*/
User Function F0500106( nOpc, aDadosRH3 , aCposRH4, aDadosRH4, cFilSRA )
	Local aArea   		:= GetArea()
	Local cNumRH3 		:= ""
	Local aStruRH3		:= GetStru( "RH3" )
	Local nLenDados	:= Len( aDadosRH3 )
	Local nLenStru    := Len( aStruRH3 )
	Local nDados      := 1
	Local nRH4   		:= 1
	Local nStru       := 1
	Local aRH3        := {}
	Local aRH3Aux     := {}
	Local cFilBck		:= cFilAnt
	Default nOpc      := 3
	Default cFilSRA	:= xFilial("RH3")
	
	Private INCLUI    := ( nOpc == 3 )
	
	If !(cFilSRA == xFilial("RH3"))
		cFilAnt := cFilSRA
	EndIf
		
	If nOpc == 3
		
		// Varre a estrutura pra validar os campos
		For nStru := 1 To nLenStru
			// Ignora os campos filial e c�digo
			If !( AllTrim(aStruRH3[nStru][01]) $ "RH3_FILIAL|RH3_CODIGO" )
				// Busca o Campo na estrutura
				nScan := aScan(aDadosRH3, {|x| AllTrim(x[01]) == Alltrim( aStruRH3[nStru][01] )  })
				If nScan > 0
					AADD( aRH3, { aStruRH3[nStru][01] , aDadosRH3[nScan][02] })
				Else
					// Tratamento para incluir valor default caso ternha iniciar padr�o e n�o seja campo virtual
					If !Empty( aStruRH3[nStru][11] ) .And. AllTrim(aStruRH3[nStru][09]) <> "V"
						AADD( aRH3Aux, { aStruRH3[nStru][01] ,aStruRH3[nStru][11] } )
					EndIf
				EndIf
			EndIf
		Next
		
		If !Empty( aRH3 )
			
			// Faz a persistencia pra proteger o contador
			While .T.
				cNumRH3 := GetSX8Num("RH3","RH3_CODIGO")
				DbSelectArea("RH3")
				RH3->( DbSetOrder(1) )
				If RH3->(dbSeek(xFilial("RH3")+cNumRH3 ))
					ConfirmSX8()
					Conout("[RH3] - Realocando contador -> "+cNumRH3)
					Loop
				EndIf
				Exit
			EndDo
			
			DbSelectArea("RH3")
			
			RecLock("RH3", .T.)
			
			// Campos que s�o incluidos obrigatoriamente aqui
			RH3->RH3_FILIAL := xFilial("RH3")
			RH3->RH3_CODIGO := cNumRH3
			
			// Grava os Campos passados
			For nDados := 1 To Len( aRH3 )
				nField := RH3->(FieldPos(aRH3[nDados][01]))
				RH3->(FieldPut( nField, aRH3[nDados][02] ))
			Next
			
			// Grava os Campos n�o passados
			For nDados := 1 To Len( aRH3Aux )
				nField := RH3->(FieldPos(aRH3Aux[nDados][01]))
				RH3->(FieldPut( nField, &(aRH3Aux[nDados][02]) ))
			Next
			
			RH3->(MsUnLock())
			ConfirmSX8()
			
			If !Empty( aCposRH4 ) .And.  !Empty( aDadosRH4 )
				
				DbSelectArea("RH4")
				
				For nRH4 := 1 to Len(aCposRH4)
					
					RecLock("RH4",.T.)
					
					RH4_FILIAL	:= xFilial("RH4")
					RH4_CODIGO := cNumRH3
					RH4_ITEM	:= nRH4
					RH4_CAMPO	:= aCposRH4[nRH4]
					RH4_DESCAM := Posicione("SX3", 2, aCposRH4[nRH4], "X3Descric()")
					RH4_VALANT	:= ""
					RH4_VALNOV	:= aDadosRH4[nRH4]
					
					RH4->(MsUnLock())
					
				Next
			EndIf
			
		EndIf
		
	EndIf
	
	RestArea( aArea )
	cFilAnt	:= cFilBck//retorna a variavel
Return( cNumRH3 )


/*/{Protheus.doc} F0500109
Legenda do Browse
@type function
@author 		Roberto Souza
@since 			10/11/2016
@version 		1.0
@Project    	MAN0000007423039_EF_001
@return ${return}, ${return_description}

/*/
User Function F0500109()
	Local aLegenda := {}
	
	//Monta as cores
	AADD(aLegenda,{"BR_AMARELO"		, "Solicitado"})
	AADD(aLegenda,{"BR_VERDE"		, "Aprovado"})
	AADD(aLegenda,{"BR_VERMELHO"	, "Rejeitado"})
	AADD(aLegenda,{"BR_AZUL"			, "Aguardando Efetivacao RH"})
	
	BrwLegenda("Status da Solicita��o", "Status", aLegenda)
Return( .T. )


/*/{Protheus.doc} F0500110
Atualiza o status da P10
@type function
@author 		Roberto Souza
@since 			10/11/2016
@version 		1.0
@Project    	MAN0000007423039_EF_001
@param 			cSolic, char, Codigo da solicita��o
@param 			cStatus, char ,	Status a ser alterado
@return ${return}, ${return_description}

/*/
User Function F0500110( cSolic, cStatus )
	Local lRet 		:= .F.
	Local aArea 	 	:= GetArea()
	Local aInfoP10    := {}
	
	Default cSolic 	:= ""
	Default cStatus	:= ""
	
	If !Empty(cSolic) .And. !Empty(cStatus)
		aInfoP10    := GetP10( cSolic )
		
		DbSelectArea("P10")
		P10->(DbSetOrder(1))
		If P10->(DbSeek(xFilial("P10")+aInfoP10[01]))
			RecLock("P10",.F.)
			P10->P10_STATUS := cStatus
			P10->(MsUnlock())
			lRet := .T.
		EndIf
	EndIf
	RestArea( aArea )
	
Return( lRet )

/*/{Protheus.doc} SelectVisao

@type function
@author alexandre.arume
@since 20/10/2016 
@version 1.0
@param cCod, character, (Descri��o do par�metro)
@param cFilResp, character, (Descri��o do par�metro)
@param cMatResp, character, (Descri��o do par�metro)
@param cMatric, character, (Descri��o do par�metro)
@param cTipo, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function SelectVisao( cCod, cFilResp, cMatResp, cMatric, cTipo, cCodVis , aDados, aMatInc )
	
	Local nI 			:= 0
	Local cCodDep		:= ""
	Local cTpRSJC 		:= AllTrim( GetNewPar("FS_TPRSJC","5") )
	Local cAliasVIS	:= GetNextAlias()
	Local lGerente		:= .F.
	Local lContinua   := .T.
	Local cVISR001    := AllTrim( GetNewPar("FS_VISR001","") ) // Demiss�o de gerente
	Local cVISR002    := AllTrim( GetNewPar("FS_VISR002","") ) // Demiss�o de funcionario com estabilidade
	Local aAprov		:= {}
	
	// Verifica se muda a vis�o
	If AllTrim( cTipo ) $ cTpRSJC
		If IsGer( cMatric )
			cCodVis := cVISR001
		ElseIf IsOver10( cMatric, Stod(aDados[03]) ) .Or. HasRFX(cMatric, Stod(aDados[03]))
			cCodVis := cVISR002
		EndIf
	EndIf
	
		aAprov := U_F0100327(aMatInc[01],aMatInc[02],POSICIONE("SRA",1,aMatInc[01]+aMatInc[02],"RA_DEPTO"),cCodVis,cEmpAnt)
		If !Empty(aAprov[01][01])
			cFilResp 	:= aAprov[01][01]
			cMatResp 	:= aAprov[01][02]
			nI			:= aAprov[01][04]
		EndIf
	
Return( nI )

/*/{Protheus.doc} HasAttach

@type function
@author alexandre.arume
@since 20/10/2016
@version 1.0
@param cMatric, character, (Descri��o do par�metro)
@param cTpRes, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function HasAttach(cMatric, cTpRes)
	
	Local lRet		:= .T.
	Local cQuery	:= ""
	Local cMyAlias	:= GetNextAlias()
	
	cTpRes := AllTrim(cTpRes)
	
	Do Case
		
	Case cTpRes $ "1,2,4"		// �bito, Pedido de Demiss�o, Abandono de Emprego
		
		cQuery := "SELECT P09_CODDOC "
		cQuery += "FROM " + RetSqlName("P09") + " "
		cQuery += "WHERE P09_CODORI = '" + cMatric + "' "
		cQuery += "AND P09_FILIAL = '" + xFilial("P09") + "' "
		cQuery += "AND P09_ROTINA LIKE '%F0500101%' "
		cQuery += "AND D_E_L_E_T_ = ' '"
		
		cQuery := ChangeQuery(cQuery)
		
		dbUseArea( .T., "TOPCONN", TCGenQry( ,,cQuery ), cMyAlias, .F., .T. )
		
		If (cMyAlias)->(Eof())
			MsgAlert("Favor inserir o anexo para prosseguir o processo de aprova��o.", "Aten��o")
			lRet := .F.
		EndIf
		
		(cMyAlias)->(dbCloseArea())
		
	End Case
	
Return lRet

/*/{Protheus.doc} HasRFX
Verifica se o funcionario tem estabilidade.
@type function
@author alexandre.arume
@since 24/10/2016
@version 1.0
@param cMatric, character, (Descri��o do par�metro)
@param dDtSol, data, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function HasRFX(cMatric, dDtSol)
	
	Local lRet	:= .F.
	
	dbSelectArea("RFX")
	RFX->(dbSetOrder(1))
	RFX->(dbSeek(xFilial("RFX") + cMatric))
	
	While !RFX->(Eof()) .And. RFX->RFX_FILIAL+RFX->RFX_MAT  == xFilial("RFX") + cMatric   
		
		If RFX->RFX_DATAF > DToS(dDtSol)
			lRet := .T.
			Exit
		EndIf
		
		RFX->(dbSkip())
	EndDo
	
Return lRet


/*/{Protheus.doc} IsGer
Verifica se o funcionario � gerente.
@type function
@author Roberto Souza
@since 24/10/2016
@version 1.0
@param cMatric, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function IsGer( cMatric )
	
	Local cFunc := Posicione("SRA",1,xFilial("SRA")+cMatric,"RA_CODFUNC")
	Local lRet  := Posicione("SRJ",1,xFilial("SRJ")+cFunc,"RJ_XGEREN") == "S"
	
Return( lRet )


/*/{Protheus.doc} IsGer
Retorna a rela��o usuario x funcionario.
@type function
@author		Roberto Souza
@since		08/11/2016
@version	1.0
@param		cMatric, character, (Descri��o do par�metro)
@return		aInfo, Filial e Matricula do Funcionario Logado
/*/
User Function GetInfMat(lExib)
	
	Local aInfo 	:= {"",""}
	Local aAllUser	:= {}
	Local nTamEmp 	:= Len(Alltrim(cEmpAnt))
	Local nTamFil 	:= Len(Alltrim(cFilAnt))
	
	Default lExib := .T.
	//Busca vinculo funcional
	PswOrder(1)
	If ( PswSeek(__cUserId,.T.) )
		aAllUser := PswRet()
		
		cFilUsr := SUBSTR(aAllUser[1,22], nTamEmp+1, nTamFil)
		cMatUsr := SUBSTR(aAllUser[1,22], nTamEmp+nTamFil+1, 6)
		
		If !Empty(cFilUsr) .And. !Empty(cMatUsr)
			aInfo[01] :=  cFilUsr
			aInfo[02] :=  cMatUsr
		Else
			If lExib
				Help("",1, "Help", ,"O usuario solicitante n�o possui vinculo funcional. � necess�rio efetuar a amarra��o com a matricula do funcion�rio atrav�s do m�dulo Configurador", 3, 0)
			EndIf
		EndIf
	EndIf
	
Return( aInfo )


/*/{Protheus.doc} IsGer
Retorna as informa��es da P10 a partir da solicita��o
@type function
@author Roberto Souza
@since 08/11/2016
@version 1.0
@param cMatric, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function GetP10( cSolic )
	
	Local aInfo 	:= Array(10)
	Local cP10		:= GetNextAlias()
	
	Default cSolic:= ""
	
	BeginSql alias cP10
		SELECT P10_COD,P10_CODRES, P10_MATSOL, P10_DTSOLI, P10_MOTIVO, P10_STATUS FROM %table:P10%
		WHERE P10_CODRH3 = %exp:cSolic% AND
		P10_FILIAL = %exp:xFilial("P10")% AND
		%notdel%
	EndSql
	
	If (cP10)->(!Eof())
		aInfo[01] := (cP10)->P10_COD
		aInfo[02] := (cP10)->P10_CODRES
		aInfo[03] := (cP10)->P10_MATSOL
		aInfo[04] := (cP10)->P10_DTSOLI
		aInfo[05] := (cP10)->P10_MOTIVO
		aInfo[06] := (cP10)->P10_STATUS
	EndIf
	
Return( aInfo )


/*/{Protheus.doc} IsOver10
Verifica se o funcionario tem estabilidade de mais de 10 anos.
@type function
@author Roberto Souza
@since 09/11/2016
@version 1.0
@param cMatric, character, (Descri��o do par�metro)
@param dDtSol, data, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function IsOver10(cMatric, dDtSol)
	
	Local lRet		:= .F.
	Local nAnosE  := 10 // Anos Estabilidade
	Local nDiasE  := nAnosE * 365
	
	DbSelectArea("SRA")
	SRA->(DbSetOrder(1))
	If SRA->(DbSeek(xFilial("SRA") + cMatric))
		dDtAdmiss := SRA->RA_ADMISSA
		nDiasAtivo:= dDtSol - dDtAdmiss
		lRet := nDiasE <= nDiasAtivo
	EndIf
Return( lRet )



/*/{Protheus.doc} GetStru
Cria array com estrutura de uma tabela.

@author Roberto Souza
@since 08/11/2016
@version 1
/*/
Static Function GetStru( cTable )
	Local nUsado  := 0
	Local aHeader := {}
	
	DbSelectArea("SX3")
	SX3->(DbSetOrder(1))
	SX3->(DbSeek( cTable ))
	While  SX3->(!Eof()) .And. SX3->X3_ARQUIVO == cTable
		AADD(aHeader,{;
			AllTrim(X3_CAMPO)	,; // 01
		X3_TIPO		,; // 02
		X3_TAMANHO	,; // 03
		X3_DECIMAL	,; // 04
		X3_PICTURE	,; // 05
		X3_VALID	,; // 06
		X3_USADO	,; // 07
		X3_F3		,; // 08
		X3_CONTEXT	,; // 09
		X3_CBOX		,; // 10
		X3_RELACAO	}) // 11
		SX3->(DbSkip())
	EndDo
	
Return( aHeader )


/*/{Protheus.doc} F0500198
Fun�ao para buscar todas as visoes do usuario logado e montar um array com os integrantes da equipe dele
@type function
@author 		Ademar Fernandes
@since 			13/01/2017
@version 		1.0
@Project    	MAN0000007423039_EF_001
@param			cFilGes	- filial do "gestor"
@param			cMatGes	- matricula do "gestor"
@param			cCodVis	- codigo da visao principal
@return 		aRet => estrutura com todos os integrantes das visoes do usuario logado
/*/
User Function F0500198(cFilGes, cMatGes, cCodVis)
	
	Local aArea 	:= GetArea()
	Local aPostos	:= {}
	Local aRet		:= {}
	Local aMatInc	:= {}
	Local cQuery	:= ""
	Local cMy1Alias	:= GetNextAlias()
	Local cMy2Alias	:= GetNextAlias()
	Local cTypeOrg	:= ""
	Local nX
	Local cPostAte
	
	Default cFilGes	:= ""
	Default cMatGes	:= ""
	Default cCodVis	:= ""
	
	If Empty(cMatGes)
		aMatInc	:= U_GetInfMat()
	EndIf
	cFilGes	:= Iif(Empty(cFilGes), aMatInc[01], cFilGes)
	cMatGes	:= Iif(Empty(cMatGes), aMatInc[02], cMatGes)
	
	TipoOrg(@cTypeOrg, cCodVis)
	
	//-Verifica as visoes que o Usuario participa
	IF !Empty(cMatGes) .And. (cTypeOrg = '1' .Or. Empty(cCodVis))
		
		/*
		SELECT * FROM DB2.RCX990 A
		INNER JOIN DB2.RD4990 B ON B.D_E_L_E_T_ = ' ' 
		AND RD4_FILIDE=RCX_FILIAL AND RD4_CODIDE=RCX_POSTO
		WHERE A.D_E_L_E_T_ = ' ' 
		AND RCX_FILFUN = '01' AND RCX_MATFUN = '320712'
		ORDER BY RCX_FILIAL,RCX_FILFUN,RCX_MATFUN,RCX_POSTO
		*/
		DbSelectArea("RCX")	//-Ocupantes do posto
		DbSetOrder(4)	//-RCX_FILIAL+RCX_FILFUN+RCX_MATFUN+RCX_POSTO
	
		cQuery := "SELECT * "
		cQuery += "FROM " + RetSqlName("RCX") + " RCX "
		cQuery += "INNER JOIN " + RetSqlName("RD4") + " RD4 "
		cQuery += "ON RD4.D_E_L_E_T_ = ' ' "
		cQuery += "AND RD4_FILIDE=RCX_FILIAL AND RD4_CODIDE=RCX_POSTO "
		If !Empty(cCodVis)
			cQuery += "AND RD4_CODIGO = '" + cCodVis + "' "
		EndIf
		cQuery += "WHERE RCX.D_E_L_E_T_ = ' ' "
		cQuery += "AND RCX_FILFUN = '" + cFilGes + "' "
		cQuery += "AND RCX_MATFUN = '" + cMatGes + "' "
		cQuery += "ORDER BY RCX_FILIAL,RCX_FILFUN,RCX_MATFUN,RCX_POSTO "
		
		cQuery := ChangeQuery(cQuery)
		dbUseArea( .T., "TOPCONN", TCGenQry( ,,cQuery ), cMy1Alias, .F., .T. )
		
		If (cMy1Alias)->(Eof())
			MsgAlert("Usu�rio logado n�o pertence a nenhum Posto Ativo!", "Aten��o")
		Else
			While !Eof()
				
				aAdd(aPostos, {	RCX_FILIAL,;	//-01
								RCX_POSTO,;		//-02
								RCX_FILFUN,;	//-03
								RCX_MATFUN,;	//-04
								RCX_DTINI,;		//-05
								RCX_DTFIM,;		//-06
								RD4_CODIGO,;	//-07-Cod.Visao
								RD4_CHAVE })	//-08
				dbSkip()
			EndDo
			
			//-Monta o Array da(s) equipe(s) 
			DbSelectArea("RD4")	//-Postos
			DbSetOrder(4)	//-RD4_FILIAL+RD4_CODIGO+RD4_CHAVE
			
			For nX := 1 To Len(aPostos)
				
				cPostAte := SubStr((Alltrim(aPostos[nX,08])+"ZZZZZZZZZZZZZZZZZZZZ"),1,Len(RD4->RD4_CHAVE))
				
				/*
				SELECT * FROM DB2.RD4990 RD4
				INNER JOIN DB2.RCX990 RCX ON RCX.D_E_L_E_T_ = ' ' 
				AND RCX_FILIAL=RD4_FILIDE AND RCX_POSTO=RD4_CODIDE
				WHERE RD4.D_E_L_E_T_ = ' ' 
				AND RD4_CODIGO='000010' 
				AND (RD4_CHAVE BETWEEN '001001              ' AND '001001ZZZZZZZZZZZZZZ' )
				ORDER BY RCX_FILIAL,RCX_FILFUN,RCX_MATFUN,RCX_POSTO
				*/
				cQuery := "SELECT * "
				cQuery += "FROM " + RetSqlName("RD4") + " RD4 "
				cQuery += "INNER JOIN " + RetSqlName("RCX") + " RCX "
				cQuery += "ON RCX.D_E_L_E_T_ = ' ' "
				cQuery += "AND RCX_FILIAL=RD4_FILIDE AND RCX_POSTO=RD4_CODIDE "
				cQuery += "WHERE RD4.D_E_L_E_T_ = ' ' "
				cQuery += "AND RD4_CODIGO = '" + aPostos[nX,07] + "' "
				cQuery += "AND (RD4_CHAVE BETWEEN '" + aPostos[nX,08] + "' AND '"+cPostAte+"') "
				cQuery += "ORDER BY RD4_FILIAL,RD4_CODIGO,RD4_CHAVE "
				
				cQuery := ChangeQuery(cQuery)
				dbUseArea( .T., "TOPCONN", TCGenQry( ,,cQuery ), cMy2Alias, .F., .T. )
				
				While !Eof()
				
					aAdd(aRet, {	RCX_FILIAL,;	//-01
									RCX_POSTO,;		//-02
									RCX_FILFUN,;	//-03
									RCX_MATFUN,;	//-04
									RCX_DTINI,;		//-05
									RCX_DTFIM,;		//-06
									RD4_CODIGO,;	//-07-Cod.Visao
									RD4_CHAVE })	//-08
					dbSkip()
				EndDo
				(cMy2Alias)->(dbCloseArea())
				
			Next nX
		EndIf
	
		(cMy1Alias)->(dbCloseArea())
	EndIf
	
	RestArea(aArea)
Return(aRet)


/*/{Protheus.doc} F0500199
Fun�ao para buscar a visao do usuario logado e verificar se ele pode incluir uma Solicita�ao para o funcionario que deseja
@type function
@author 		Luis Trombini
@since 			06/01/2017
@version 		1.0
@Project    	MAN0000007423039_EF_001
@return 		lRet => (.T.) Pode incluir a solici�ao ### (.F.) N�o pode incluir a solicita�ao
/*/
User Function F0500199(cMatric, cCodVis, nOp)  // nop = 1-Desligamento ### 2-Movimento Pessoal
	
	Local aArea := GetArea()
	Local aRet := {}
	Local aMatInc := U_GetInfMat()
	local c1Posto := " "
	local c2Posto := " "
	local c1Tree  := " "
	local c2Tree  := " "
	Local c1Chave := " "
	Local c2Chave := " "
	local c1Depto := " "
	local c2Depto := " "
	Local cTypeOrg := ""
	Local cFilTrp := ""
	Local lRet     := .T.
	
	Default nOp	:= 1
		
	//-Verifica se o Usuario logado tem Vinculo Funcional
	If Empty(aMatInc[01]) .And. Empty(aMatInc[02])	//-cFilUsr###cMatUsr
		Return( .f. )
	Else
		TipoOrg(@cTypeOrg, cCodVis)
		
		//-Verifica as visoes que o Usuario participa
		IF cTypeOrg = '1'
			
			// Verifica solicitante para ver o nivel e pares dentro da vis�o
			DbSelectArea("SRA")
			IF SRA->(DbSeek(aMatInc[01]+aMatInc[02]))
				c1Posto:= RA_POSTO
			Endif
			
			DbSelectArea("RD4")
			DbSetOrder(7)
			If !RD4->(dbSeek(xFilial("RD4")+cCodVis+cEmpAnt+aMatInc[01]+c1Posto))
				if nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "1 - Usu�rio sem Permiss�o para solicita��o de desligamento.  Aten��o!!!", 3, 0)
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Usu�rio sem Permiss�o para solicita��o de Movimento de Pessoal.  Aten��o!!!", 3, 0)
				EndIf				
				lRet := .F.
				Return(lRet)
			else
				c1Tree := RD4_TREE
				c1Chave := RD4_CHAVE
			Endif
			
			// Verifica funcionario Selecionado
			DbSelectArea("SRA")
			If !SRA->(dbSeek(xFilial("SRA") + cMatric))
				If nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "Funcion�rio inexistente nesta Filial.  Aten��o!!!", 3, 0)				
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Funcion�rio inexistente nesta Filial.  Aten��o!!!", 3, 0)				
				EndIf				
				lRet := .F.
				Return(lRet)
			Else
				cFilTrp:= RA_Filial
				c2Posto:= RA_POSTO
				If Empty(c2Posto)
					If nop = 1
						Help("",1, "Help", "Solicita��o de Desligamento", "Funcion�rio Solicitado n�o esta Locado em um Posto.   Aten��o!!!", 3, 0)
					Else
						Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Funcion�rio Solicitado n�o esta Locado em um Posto.   Aten��o!!!", 3, 0)
					Endif					
					lRet := .F.
					Return(lRet)
				EndIf
				
			Endif
			
			DbSelectArea("RD4")
			DbSetOrder(7)
			If RD4->(dbSeek(xFilial("RD4")+cCodVis+cEmpAnt+cFilTrp+c2Posto))
				c2Tree := RD4_TREE
				c2Chave := RD4_CHAVE
			Endif
			
			If (c1Chave>=c2Chave  )
				If nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "2 - Usu�rio sem Permiss�o para solicita��o de desligamento.  Aten��o!!!", 3, 0)
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Usu�rio sem Permiss�o para solicita��o de Movimento de Pessoal.  Aten��o!!!", 3, 0)
                Endif					
				lRet := .F.
				Return(lRet)
			Endif
			
		Else
			// Verifica solicitante para ver o nivel e pares dentro da vis�o
			
			DbSelectArea("SRA")
			IF SRA->(DbSeek(aMatInc[01]+aMatInc[02]))
				c1Depto:= RA_DEPTO
			Endif
			
			DbSelectArea("RD4")
			DbSetOrder(7)
			If !RD4->(dbSeek(xFilial("RD4")+cCodVis+cEmpAnt+aMatInc[01]+c1Depto))
				if nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "Departamento n�o Consta da Vis�o para solicita��o de desligamento.  Aten��o !!!", 3, 0)
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Departamento n�o Consta da Vis�o para solicita��o de Movimento de Pessoal.  Aten��o !!!", 3, 0)
				EndIf
				lRet := .F.
				
				Return(lRet)
			else
				c1Tree := RD4_TREE
				c1Chave := RD4_CHAVE
				
				DbSelectArea("SQB")
				DbSetorder(1)
				
				If !SQB->(dbSeek(xFilial("SQB")+c1Depto))
				    If nop = 1  
						Help("",1, "Help", "Solicita��o de Desligamento", "Departamento n�o Consta no Cadastro de Departamento. Aten��o!!!" , 3, 0)
					Else
						Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Departamento n�o Consta no Cadastro de Departamento. Aten��o!!!" , 3, 0)
					EndIf
					lRet := .F.
					Return(lRet)
				Else
					IF (aMatInc[01]+aMatInc[02]) <> (SQB->QB_FILRESP+SQB->QB_MATRESP)
						If nop = 1
							Help("",1, "Help", "Solicita��o de Desligamento", "Usu�rio sem Permiss�o para solicita��o de desligamento.  Aten��o!!!", 3, 0)
						Else
							Help("",1, "Solicita��o de Movimento de Pessoal", "Usu�rio sem Permiss�o para solicita��o de movimenta��o pessoal.  Aten��o!!!", 3, 0)
						Endif
						lRet := .F.
						Return(lRet)
					EndIf
				EndIf
			Endif
			
			DbSelectArea("SRA")
			If !SRA->(dbSeek(xFilial("SRA") + cMatric))
				If nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "Funcion�rio inexistente.  Aten��o!!!", 3, 0)
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Funcion�rio inexistente.  Aten��o!!!", 3, 0)
				EndIf				
				lRet := .F.
				Return(lRet)
			Else
				c2Depto:= RA_DEPTO
				cFilTrp := ra_filial
			Endif
			
			DbSelectArea("SQB")
			DbSetorder(1)
			
			If !SQB->(dbSeek(xFilial("SQB")+c2Depto))
				If nop = 1
					Help("",1, "Help", "Solicita��o de Desligamento", "Departamento n�o Consta no Cadastro de Departamento. Aten��o!!!" , 3, 0)
				Else
					Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Departamento n�o Consta no Cadastro de Departamento. Aten��o!!!" , 3, 0)
				EndIf				
				lRet := .F.
				Return(lRet)
			Else
				IF (xFilial("SRA") + cMatric) = (SQB->QB_FILRESP+SQB->QB_MATRESP)
					
					DbSelectArea("RD4")
					DbSetOrder(7)
					If  RD4->(dbSeek(xFilial("RD4")+cCodVis+cEmpAnt+cFilTrp+c2Depto))
						c2Tree := RD4_TREE
						c2Chave := RD4_CHAVE
						
						If (c1Chave >= c2Chave)
							If nop = 1
								Help("",1, "Help", "Solicita��o de Desligamento", "Usu�rio sem Permiss�o para solicita��o de desligamento do Funcion�rio.  Aten��o!!!" , 3, 0)
							Else
								Help("",1, "Help", "Solicita��o de Movimento de Pessoal", "Usu�rio sem Permiss�o para solicita��o de Movimento de Pessoal.  Aten��o!!!" , 3, 0)
							Endif
							lRet := .F.
							Return(lRet)
						Endif
					EndIf
				EndIf
			EndIf
		Endif
	Endif
	RestArea(aArea)
	
Return (lRet)
