#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH" 

/*
{Protheus.doc} F0100315()
Redireciona a p�gina respons�vel pela op��o escolhida.
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100315()

	Local cHtml := ""
	Local nIndiceDepto := Val(HttpGet->nIndDept)
	
	Private cMsg
	
	cOpc := HttpGet->cOpc
	cNPost := HttpGet->cNPost
	
	WEB EXTENDED INIT cHtml START "InSite"
	
		HttpCTType("text/html; charset=ISO-8859-1")
		
		cCodDepto := HttpSession->Department[nIndiceDepto]:cDepartment
		cDescrDepto := HttpSession->Department[nIndiceDepto]:cDescrDepartment
		
		If cOpc == "2" //Or�amento
			cHtml += ExecInPage( "F0100315" )
		Else
			If cNPost == "1" //Novo Posto Sim
				cNPosto := "1"
				cHtml += ExecInPage( "F0100319" )
			Else
				cHtml += ExecInPage( "F0100315" )
			EndIf
		EndIf	 
	
	WEB EXTENDED END
	
Return cHtml