#INCLUDE 'TOTVS.CH'
#INCLUDE 'APWEBSRV.CH'

//-----------------------------------------------------------------------
/*/{Protheus.doc} F0600801
Estrutura do Web-Service do Calculo Rescicao
 
@author Eduardo Fernandes 
@since  07/11/2016
@return Nil  

@project MAN0000007423040_EF_008
@cliente Rededor
@version P12.1.7
             
/*/
//-----------------------------------------------------------------------
User Function F0608001()
Return

WSSERVICE Rescisao_ApDat Description "Integracao Calculo de Rescisao ApData"	
	WSDATA cRET					AS STRING
	WSDATA EF06008_FILIAL	 	AS STRING
	WSDATA EF06008_MATRIC		AS STRING
	WSDATA EF06008_CODVER		AS STRING
	WSDATA EF06008_REFVER		AS STRING
	WSDATA EF06008_QTDVER 		AS STRING OPTIONAL
	WSDATA EF06008_VALVER 		AS STRING 
	WSDATA EF06008_ANOMESREF	AS STRING OPTIONAL
	WSDATA EF06008_DTTRANSAC 	AS STRING OPTIONAL	
	WSDATA EF06008_HRTRANSAC 	AS STRING OPTIONAL	
	WSDATA EF06008_OPERACAO 	AS STRING
	
	WSMETHOD LoadRGB 	DESCRIPTION "Carrerga os dados da Integracao"   
EndWsService

WSMETHOD LoadRGB WsReceive EF06008_FILIAL, EF06008_MATRIC, EF06008_CODVER, EF06008_REFVER, EF06008_QTDVER, EF06008_VALVER, EF06008_ANOMESREF, EF06008_DTTRANSAC, EF06008_HRTRANSAC, EF06008_OPERACAO  WsSend cRET WsService Rescisao_ApDat
Local cVar01  := ::cRET
Local lReturn := U_F0600802(::EF06008_FILIAL, ::EF06008_MATRIC, ::EF06008_CODVER, ::EF06008_REFVER, ::EF06008_QTDVER, ::EF06008_VALVER, ::EF06008_ANOMESREF, ::EF06008_DTTRANSAC, ::EF06008_HRTRANSAC, ::EF06008_OPERACAO, @cVar01)
::cRET := cVar01 
Return lReturn