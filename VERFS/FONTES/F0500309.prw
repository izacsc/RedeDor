#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*/{Protheus.doc} F0500309
(long_description)
@type    function
@author  henrique.toyada
@since   31/10/2016
@version 1.0
@return  cHtml, Retorno pagina para ser acessada
@project MAN00000463301_EF_003
/*/
User Function F0500309()
	
	Local cHtml   	:= ""
	
	Local cHierarquia 	:= ""
	Local nPos        	:= 0
	Local nCnt        := 0
	Local aAux        	:= {}
	Local nAux        	:= 0
	Local nNivel      	:= 0
	Local nReg       := 0
	Local oOrg
	
	Private cMsg
	Private oParam
	Private oLista
	Private oSol
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	HttpCTType("text/html; charset=ISO-8859-1")
	
	cFililS := HttpGet->cFilRh3
	cCodigo := HttpGet->cCodigo
	cTipo   := HttpGet->cTipo
	cOperac := HttpGet->cStatus
	cVisao  := HttpGet->cVisao
	cMatric := HttpSession->RHMat
	nOperac := HttpGet->cStatus
	cCodMat := HttpGet->cCodMat
	cFilRh3 := HttpGet->cFilRh3
	cFilials  := HttpGet->cFilRh3
	cFilis  := HttpGet->cFilRh3

	
	oInf   := WSW0500307():new()
	WsChgURL(@oInf,"W0500307.APW")
	If oInf:ChkRh3(cCodigo)
		lRet := oInf:lCHKRH3RESULT
	EndIf
	
	Do Case
	Case cTipo = '001'
		cStatus   := "1"
		lBtAprova := .F.
		lRet := .T.
		cMATRI    := cCodMat
		cSolic    := cCodigo
		oParam := WSW0200301():new()
		WsChgURL(@oParam,"W0200301.APW")
		
		If oInf:BuscaTrm(cCodigo,cFilRh3)
			CCalend := oInf:oWSBUSCATRMRESULT:CCalend
			CCurso  := oInf:oWSBUSCATRMRESULT:CCurso
			cNOME   := oInf:oWSBUSCATRMRESULT:CNome
		EndIf
		
		If oParam:InfTrm(CCalend,CCurso,cFilRh3)
			cCalendario 	:=  oParam:oWSInfTrmRESULT:cCalendario
			cCurso 		:=  oParam:oWSInfTrmRESULT:cCurso
			cHorario 		:=  oParam:oWSInfTrmRESULT:cHorario
			cInicio 		:=  oParam:oWSInfTrmRESULT:cInicio
			cTermino 		:=  oParam:oWSInfTrmRESULT:cTermino
			cTreinamento 	:=  oParam:oWSInfTrmRESULT:cTreinamento
			cVagas 		:=  oParam:oWSInfTrmRESULT:cVagas
			cTurma 		:=  oParam:oWSInfTrmRESULT:cTurma
		EndIf
		
		cHtml := ExecInPage("F0200309")//Treinamento/Evento
		
	Case cTipo = '002'
		
		fGetInfRotina("U_F0100301.APW") //Verificar
		GetMat()							//Pega a Matricula e a filial do participante logado
		
		cCodSol := cCodigo
		cMatric := HttpSession->RHMat
		cVisPad := HttpSession->aInfRotina:cVisao
		nOperac := '9'
		cTVaga  := ""
		cLugar  := "1"
	    
		oSol := WSW0500308():New()
		WsChgURL(@oSol,"W0500308.APW")
		
		oSol:BscSolVags(cCodSol,cMatric,cVisPad,cFililS)
		If oSol:oWSBscSolVagsRESULT:cRet == ".T."
			cFVaga   := oSol:oWSBscSolVagsRESULT:cFilVag
			cTVaga   := IIF(oSol:oWSBscSolVagsRESULT:cVAGA == "1","Aumento de Quadro","Substitui��o")
			cCdDept  := oSol:oWSBscSolVagsRESULT:cDepto
			cCdPost  := oSol:oWSBscSolVagsRESULT:cPosto
			cFPost   := oSol:oWSBscSolVagsRESULT:cFlPost
			cCdCC    := oSol:oWSBscSolVagsRESULT:cCodCC
			cDESCRCC := oSol:oWSBscSolVagsRESULT:cDescCc
			cCdFun   := oSol:oWSBscSolVagsRESULT:cFuncao
			CDESCRFC := oSol:oWSBscSolVagsRESULT:cDescFu
			cNSolic  := oSol:oWSBscSolVagsRESULT:cNomeSol
			cPrzVg   := oSol:oWSBscSolVagsRESULT:cPrazo
			cDtAbt   := oSol:oWSBscSolVagsRESULT:cDtAbt
			cDtFch   := oSol:oWSBscSolVagsRESULT:cDtFch
			cCustV   := oSol:oWSBscSolVagsRESULT:cCustVg
			cPrSel   := oSol:oWSBscSolVagsRESULT:cProcS
			cTpVg    := IIF(oSol:oWSBscSolVagsRESULT:cTpVaga == "1","Int./Ext.",IIF(oSol:oWSBscSolVagsRESULT:cTpVaga == "2","Interna","Externa"))
			cJornTb  := oSol:oWSBscSolVagsRESULT:cHorMes
			cObservacao   := ALLTRIM(oSol:oWSBscSolVagsRESULT:cXObs)
			cDscDetal   := ALLTRIM(oSol:oWSBscSolVagsRESULT:cObserDet)	
			cCODCARGO  := oSol:oWSBscSolVagsRESULT:cCODCARGO
			cDESCRCARGO  := oSol:oWSBscSolVagsRESULT:cDESCRCARGO
			//Campo observa��o		
		EndIf
		
		cHtml := ExecInPage("F0100310") //Vaga
	Case cTipo = '003'
		
		fGetInfRotina("U_F0100312.APW") //Verificar
		GetMat()							//Pega a Matricula e a filial do participante logado
		//Ajustando solicita��o
		cCodSol := cCodigo
		cMatric := HttpSession->RHMat
		cVisPad := HttpSession->aInfRotina:cVisao
		nOperac := "9"
		cLugar  := "1"
	    //cFililS := httpget->cFilil
		oSol := WSW0500308():New()
		WsChgURL(@oSol,"W0500308.APW")
	
		If oSol:BscSolQdro(cCodSol,cMatric,cVisPad,cFililS)
			cFilQd := oSol:oWSBscSolQdroRESULT:cFilQdr
			cCDept := oSol:oWSBscSolQdroRESULT:cCdDept
			cDDept := oSol:oWSBscSolQdroRESULT:cDsDept
			cCFunc := oSol:oWSBscSolQdroRESULT:cCdFunc
			cDFunc := oSol:oWSBscSolQdroRESULT:cDsFunc
			cCCarg := oSol:oWSBscSolQdroRESULT:cCdCarg
			cDCarg := oSol:oWSBscSolQdroRESULT:cDsCarg
			cCCtt  := oSol:oWSBscSolQdroRESULT:cCdCCtt
			cDCtt  := oSol:oWSBscSolQdroRESULT:cDscCtt
			cSalar := oSol:oWSBscSolQdroRESULT:cSalari
			cTpCnt := oSol:oWSBscSolQdroRESULT:cTpCont
			cQntQd := oSol:oWSBscSolQdroRESULT:cQntQdr
			cTpPst := oSol:oWSBscSolQdroRESULT:cTpPost
			cObs   := oSol:oWSBscSolQdroRESULT:cObserv
			cCdPst := oSol:oWSBscSolQdroRESULT:cCodPst
			cOrgSl := oSol:oWSBscSolQdroRESULT:cOrigSl
		EndIf
		
		cHtml := ExecInPage("F0100321")//Aumento de quadro/Or�amento
		
	Case cTipo = '004'
		lRet := .T.
		oParam := WSW0500307():new()
		WsChgURL(@oParam,"W0500307.APW")
		If oParam:FunRh3(cCodigo,cFilRh3)
			cNomeFun := oParam:oWSFUNRH3RESULT:cNOMMAFUN
			cMatFun  := oParam:oWSFUNRH3RESULT:cCODMAFUN
		EndIf
		If oParam:InfRh4F(cCodigo,cFilRh3)
			oLista :=  oParam:oWSINFRH4FRESULT
			cFili    := ALLTRIM(oLista:CPA5FILIAL)
			cCodVg   := ALLTRIM(oLista:CPA5CDVAGA)
			cNomVg   := ALLTRIM(oLista:CTMPNMVAGA)
			cValVg   := ALLTRIM(oLista:CPA5VLVAGA)
			cSalFe   := ALLTRIM(oLista:CPA5SLFECH)
			cCodCand := ALLTRIM(oLista:CPA5CDCAND)
			cNomCand := ALLTRIM(oLista:CPA5NMCAND)
			cCpf     := ALLTRIM(oLista:CPA5CPFCAN)
			cDtApro  := ALLTRIM(oLista:CPA5DTAPRV)
			cLogTor  := ALLTRIM(oLista:CPA5LOGTOR)
			cNomTor  := ALLTRIM(oLista:CTMPNOMTOR)
		EndIf
		cHtml := ExecInPage("F0500309")//FAP
		
	Case cTipo = '005'
		lRet := .T.
		oParam := WSW0500307():new()
		WsChgURL(@oParam,"W0500307.APW")
		If oParam:FunRh3(cCodigo,cFilRh3)
			cNomeFun := oParam:oWSFUNRH3RESULT:cNOMMAFUN
			cMatFun  := oParam:oWSFUNRH3RESULT:cCODMAFUN
		EndIf
		If oParam:InfRh4D(cCodigo,cFilRh3)
			oLista :=  oParam:oWSINFRH4DRESULT
			cFili    := ALLTRIM(oLista:CP10FILIAL)
			cCodMat  := ALLTRIM(oLista:CP10MATRIC)
			cNomMat  := ALLTRIM(oLista:CTMPMATRIC)
			cDtSoli  := STOD(ALLTRIM(oLista:CP10DTSOLI))
			cCodRes  := ALLTRIM(oLista:CP10CODRES)
			cDesRes  := ALLTRIM(oLista:CTMPDESRES)
			cMotivo  := ALLTRIM(oLista:CP10MOTIVO)
			cDtDesl  := STOD(ALLTRIM(oLista:CP10DTDEMI))
		EndIf
		
		cHtml := ExecInPage("F0500310")//DESLIGAMENTO
		
	Case cTipo = '006'
		lRet := .T.
		oParam := WSW0500307():new()
		WsChgURL(@oParam,"W0500307.APW")
		If oParam:FunRh3(cCodigo,cFilRh3)
			cNomeFun := oParam:oWSFUNRH3RESULT:cNOMMAFUN
			cMatFun  := oParam:oWSFUNRH3RESULT:cCODMAFUN
		EndIf
		If oParam:InfRh4C(cCodigo,cFilRh3)
			oLista :=  oParam:oWSINFRH4CRESULT
			cTMPFUNCAO := ALLTRIM(oLista:cTMPFUNCAO) //Codigo da Fun��o Atual
			cTMPD_FUNC := ALLTRIM(oLista:cTMPD_FUNC) //Nome da Fun��o Atual
			cTMPSALATU := ALLTRIM(oLista:cTMPSALATU) //Sal�rio Atual
			cTMPTABELA := ALLTRIM(oLista:cTMPTABELA) //C�dio da Tabela Atual
			cTMPNIVELT := ALLTRIM(oLista:cTMPNIVELT) //Nivel da Tabela Atual
			cTMPFAIXAT := ALLTRIM(oLista:cTMPFAIXAT) //Faixa Atual
			cRASALARIO := ALLTRIM(oLista:cRASALARIO) //Sal�rio Proposto
			cRATIPOALT := ALLTRIM(oLista:cRATIPOALT) //Motivo da Altera��o Salarial
			cTMPD_MOT  := ALLTRIM(oLista:cTMPD_MOT)  //Descri��o do� Motivo
			cTMPVLSALA := ALLTRIM(oLista:cTMPVLSALA) //Valor do aumento proposto
			cTMPPERCSA := ALLTRIM(oLista:cTMPPERCSA) //Percentual do Aumento Proposto
			cRACODFUNC := ALLTRIM(oLista:cRACODFUNC) //Codigo da Fun��o Proposta
			cTMPD_F_PP := ALLTRIM(oLista:cTMPD_F_PP) //Nome da Fun��o Proposta
			cTMPH_M_AT := ALLTRIM(oLista:cTMPH_M_AT) //Horas M�s Atual
			cTMPH_S_AT := ALLTRIM(oLista:cTMPH_S_AT) //Horas Semana Atual
			cTMPH_D_AT := ALLTRIM(oLista:cTMPH_D_AT) //Horas Dia Atual
			cRAHRSMES  := ALLTRIM(oLista:cRAHRSMES)  //Horas M�s Proposto
			cRAHRSEMAN := ALLTRIM(oLista:cRAHRSEMAN) //Horas Semana Proposto
			cRAHRSDIA  := ALLTRIM(oLista:cRAHRSDIA)  //Horas Dia Proposto
			cTMPTURNAT := ALLTRIM(oLista:cTMPTURNAT) //Turno de Trabalho Atual
			cTMPD_TURN := ALLTRIM(oLista:cTMPD_TURN) //Nome do Turno Atual
			cTMPS_TURN := ALLTRIM(oLista:cTMPS_TURN) //Seq.Ini.Turno Atual
			cTMPREGRA  := ALLTRIM(oLista:cTMPREGRA)  //Regra Atual
			cRATNOTRAB := ALLTRIM(oLista:cRATNOTRAB) //Turno de trabalho Proposto
			cTMPDESCTU := ALLTRIM(oLista:cTMPDESCTU) //Nome do Turno Proposto
			cRASEQTURN := ALLTRIM(oLista:cRASEQTURN) //Seq.Ini. Turno Proposto
			cRAREGRA   := ALLTRIM(oLista:cRAREGRA)   //Regra Proposta
			cTMPFILIAL := ALLTRIM(oLista:cTMPFILIAL) //Filial Atual
			cTMPD_FILI := ALLTRIM(oLista:cTMPD_FILI) //Nome da Filial Atual
			cTMPCCATU  := ALLTRIM(oLista:cTMPCCATU)  //Centro de Custo Atual
			cTMPD_CCAT := ALLTRIM(oLista:cTMPD_CCAT) //Descri��o do Centro de Custo Atual
			cTMPDEPTOA := ALLTRIM(oLista:cTMPDEPTOA) //Departamento Atual
			cTMPD_DEPT := ALLTRIM(oLista:cTMPD_DEPT) //Nome do Departamento Atual
			cRAFILIAL  := ALLTRIM(oLista:cRAFILIAL)  //Filial/Empresa proposta
			cTMPN_F_D  := ALLTRIM(oLista:cTMPN_F_D)  //Nome da Filial/Empresa proposta
			cRACC      := ALLTRIM(oLista:cRACC)      //Centro de Custo proposto
			cTMPD_CC_D := ALLTRIM(oLista:cTMPD_CC_D) //Descri��o do Centro de Custo Proposto
			cRADEPTO   := ALLTRIM(oLista:cRADEPTO)   //Departamento Proposto
			cTMPD_DEPD := ALLTRIM(oLista:cTMPD_DEPD) //Nome do Departamento Proposto
			cRAPROCES  := ALLTRIM(oLista:cRAPROCES)  //Nome do Departamento Proposto
			cTMPPROCES := ALLTRIM(oLista:cTMPPROCES) //Processo Atual
			cTMPTIPO   := ALLTRIM(oLista:cTMPTIPO)   //Processo Proposto
			cTMPPOSTO  := ALLTRIM(oLista:cTMPPOSTO)   //Posto Atual
			cRAPOSTO   := ALLTRIM(oLista:cRAPOSTO)   //Posto Proposto
			cTMPCLVL   := ALLTRIM(oLista:cTMPCLVL)   //Classe Valor Atual
			cRACLVL    := ALLTRIM(oLista:cRACLVL)  	 //Classe Valor Proposto
			cTMPITEM   := ALLTRIM(oLista:cTMPITEM)   //Item Contabil Atual
			cRAITEM	   := ALLTRIM(oLista:cRAITEM)    //Item Cont�bil Proposto
			
		EndIf
		
		cHtml := ExecInPage("F0500311")//CARGOS E SALARIOS
		
	Case cTipo = '007'
		//Ajustando solicita��o
		oParam    := WSW0200301():new()
		WsChgURL(@oParam,"W0200301.APW")
		cSolic    := cCodigo
		cMat      := cMatric
		lBtAprova := .F.
		lRet := .T.

		fGetInfRotina("U_F0200301.APW")
		GetMat()								//Pega a Matricula e a filial do participante logado
	
		oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg,"ORGSTRUCTURE.APW",,,HttpGet->EmployeeEmp)
		
		If Empty(HttpGet->EmployeeFilial) .And. Empty(HttpGet->Registration)
			oOrg:cParticipantID 	    := HttpSession->cParticipantID
		
			If ValType(HttpSession->RHMat) != "U" .And. !Empty(HttpSession->RHMat)
				oOrg:cRegistration	 := HttpSession->RHMat
			EndIf
		Else
			oOrg:cEmployeeFil  	    := HttpGet->EmployeeFilial
			oOrg:cRegistration 	    := HttpGet->Registration
		EndIf
	
		oOrg:cKeyVision				:=  Alltrim(HttpGet->cKeyVision)
	
		oOrg:cVision     		    := HttpSession->aInfRotina:cVisao
		oOrg:cFilterValue 		    := HttpGet->FilterValue
		oOrg:cFilterField   		:= HttpGet->FilterField
		oOrg:cRequestType 		    := ""
	
		IF oOrg:GetStructure()
			HttpSession->aStructure  := aClone(oOrg:oWSGetStructureResult:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE)
			nPageTotal 		       := oOrg:oWSGetStructureResult:nPagesTotal
		
		// *****************************************************************
		// Inicio - Monta Hierarquia
		// *****************************************************************
			cHierarquia := '<ul style="list-style-type: none;"><li><a href="#" class="links" onclick="javascript:GoToPage(null,1,null,null,null,null,' +;
				"'" + HttpSession->aStructure[1]:cEmployeeFilial + "'," +;
				"'" + HttpSession->aStructure[1]:cRegistration + "'," +;
				"'" + HttpSession->aStructure[1]:cEmployeeEmp + "'," +;
				"'" + oOrg:cKeyVision + "'" + ')">'
		
			If Empty(HttpSession->cHierarquia) .or. (HttpSession->cParticipantID == HttpSession->aStructure[1]:cParticipantID)
				nNivel                 := 1
				HttpSession->cHierarquia := ""
			Else
				aAux := Str2Arr(HttpSession->cHierarquia, "</ul>")
				If (nPos := aScan(aAux, {|x| cHierarquia $ x })) > 0
					For nAux := len(aAux) to nPos step -1
						aDel(aAux,nAux)
						aSize(aAux,Len(aAux)-1)
					Next nAux
				EndIf
				HttpSession->cHierarquia := ""
				For nPos := 1 to Len(aAux)
					HttpSession->cHierarquia += aAux[nPos] + "</ul>"
				Next nPos
			
				nNivel := Iif(Len(aAux) > 0,Len(aAux)+1,1)
			EndIf
		
			For nPos := 1 to nNivel
				cHierarquia += '&nbsp;&nbsp;&nbsp;'
			Next nPos
			cHierarquia += Alltrim(str(nNivel)) + " . " + HttpSession->aStructure[1]:cName + '</a></li></ul>'
		
			HttpSession->cHierarquia += cHierarquia
		// Fim - Monta Hierarquia
		Else
			HttpSession->aStructure := {}
			nPageTotal 		      := 1
		EndIf
	
		If oParam:RetInvFu(cCodMat,CFILIALS)
			cMatricu1  := oParam:oWSRetInvFuRESULT:cMatricula
			cNome1     := oParam:oWSRetInvFuRESULT:cNome
			cAdmissao1 := oParam:oWSRetInvFuRESULT:cAdmissao
			cDepartam1 := oParam:oWSRetInvFuRESULT:cDepartame
			cSituacao1 := ""
			cCenCusto1 := oParam:oWSRetInvFuRESULT:cCenCusto
			cCargo1    := oParam:oWSRetInvFuRESULT:cCargo
			cFilial1   := CFILIALS
			cNmCarg1   := oParam:oWSRetInvFuRESULT:cNMCARGO
			cNmCeCu1   := oParam:oWSRetInvFuRESULT:cNMCENTRO
			cNmDepa1   := oParam:oWSRetInvFuRESULT:cNMDEPART
			Superior1  := cMat
		Else
			cMatricu1  := ''
			cNome1     := ''
			cAdmissao1 := ''
			cDepartam1 := ''
			cSituacao1 := ''
			cCenCusto1 := ''
			cCargo1    := ''
			cFilial1   := ''
			cNmCarg1   := ''
			cNmCeCu1   := ''
			cNmDepa1   := ''
			Superior1  := ''
		EndIf
		If oInf:InfRh4I(cSolic,cFilRh3)
			cCod  := oInf:cINFRH4IRESULT
		EndIf
		
		If oParam:InfPa3(cCod,"      ")
			ccurseName         := oParam:oWSINFPA3RESULT:ccurseName
			cinstituteName     := oParam:oWSINFPA3RESULT:cinstituteName
			ccontact           := oParam:oWSINFPA3RESULT:ccontact
			cphone             := oParam:oWSINFPA3RESULT:cphone
			cramal             := oParam:oWSINFPA3RESULT:cramal
			cstartDate         := oParam:oWSINFPA3RESULT:cstartDate
			cendDate           := oParam:oWSINFPA3RESULT:cendDate
			cmonthlyPayment    := oParam:oWSINFPA3RESULT:cmonthlyPayment
			cinstallmentAmount := oParam:oWSINFPA3RESULT:cinstallmentAmount
			cbenefconcedido    := oParam:oWSINFPA3RESULT:cbenefconcedido
			cvltotcurso        := oParam:oWSINFPA3RESULT:cvltotcurso
			cvlinvesanovig     := oParam:oWSINFPA3RESULT:cvlinvesanovig
			cduracao           := oParam:oWSINFPA3RESULT:cduracao
			cTipo              := oParam:oWSINFPA3RESULT:cTipo
			cObservacao        := oParam:oWSINFPA3RESULT:cobservation
		EndIf
		
		cHtml := ExecInPage("F0200304")//Incentivo academico
	EndCase
	
	WEB EXTENDED END
	
Return cHtml
