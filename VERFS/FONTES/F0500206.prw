#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0500206
Fun��o g�nerica para execu��o das fun��es dos processos
@author  Fernando Carvalho 
@since 11/11/2016
@version 12.7
@param cProcesso, character, (Processo executado)
@param oMdGrid, objeto, (Grid da rotina	 MVC)
@param oView, objeto, (View da rotina MVC)
@project 2015GGS0758_MAN00000060101_EF_001
/*/
User Function F0500206(cProcesso, oMdGrid, oView)

	If (cProcesso == "Encaminhamento")
		If !MsgYesNo("Deseja Realmente Confirmar 'Encaminhamento C�lula de Cadastro'?")
			return
		EndIf			
		CelCad(oMdGrid)
		
	ElseIf (cProcesso == "DesDocsExame")
		If !MsgYesNo("Deseja Realmente Confirmar 'Candidato Desistente Docs. / Exame'?")
			return
		EndIf
		DesisDocEx(oMdGrid)
		
	ElseIf (cProcesso == "DocsInconsist")
		If !MsgYesNo("Deseja Realmente Confirmar 'Candidato Doc. Inconsistente'?")
			return
		EndIf		
		DocIncon()
	
	ElseIf (cProcesso == "Inapto")
		If !MsgYesNo("Deseja Realmente Confirmar 'Candidato Inapto Exame'?")
			return
		EndIf		
		Inapto()
			
	ElseIf (cProcesso == "AssinaturaContra")
		If !MsgYesNo("Deseja Realmente Confirmar 'Assinatura de Contrato'?")
			return
		EndIf		
		AssContra()
			
	ElseIf (cProcesso == "DesistContrato")
		If !MsgYesNo("Deseja Realmente Confirmar 'Candidato Desistente Contrato'?")
			return
		EndIf
		DesisContra()
		
	ElseIf (cProcesso == "VagaSuspensa")
		If !MsgYesNo("Deseja Realmente Confirmar 'Vaga Suspensa'?")
			return
		EndIf
		VagaSus()
		
	ElseIf (cProcesso == "VagaCancelada")
		If !MsgYesNo("Deseja Realmente Confirmar 'Vaga Cancelada'?")
			return
		EndIf
		VagaCan()
		
	ElseIf (cProcesso == "VagaReaberta")
		If !MsgYesNo("Deseja Realmente Confirmar 'Vaga Reaberta'?")
			return
		EndIf
		VagaReab()
		
	Endif
Return .T.



/*/{Protheus.doc} F0500206
Enc. C�lula de Cadastro
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function CelCad()
	Local aArea	:= GetArea()
	Local lRet		:= .F. 
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"030")
	
	cAssunto    := "Em admiss�o"
	cBody       := "O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+PA2->PA2_SOL+" foi encaminhado para c�lula de admiss�o."
						
	cEmail      := GetMv("FS_CELCAD",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif	
	RestArea(aArea)
Return


/*/{Protheus.doc} F0500206
Candidato Desistente Docs./Exame
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function DesisDocEx()
	Local lRet 	:= .F.
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"031")
	cAssunto    := "Candidato Desistente"
	cBody       := 	"O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+PA2->PA2_SOL+;
						" desistiu da vaga de c�digo"+PA2->PA2_CDVAGA+"."
	
	cEmail      := GetMv("FS_RECSEL",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"085")
	ReprovaPA2()
Return


/*/{Protheus.doc} F0500206
Candidato Doc. Inconsistente'
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function DocIncon()
	Local lRet		:= .F.
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"032")
	cAssunto    := "Candidato Doc. Inconsistente"
	cBody       := 	"O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+;
						PA2->PA2_SOL+" foi Reprovado por documentos inconsistentes."
	cEmail      := GetMv("FS_RECSEL",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"085")
	ReprovaPA2()
	Sair()
Return


/*/{Protheus.doc} F0500206
Candidato Inapto Exame
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function Inapto()
	Local lRet		:= .F.
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"033")
	cAssunto    := "Candidato Inapto Exame"
	cBody       := 	"O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+;
						PA2->PA2_SOL+" foi Reprovado por ser considerado Inapto no Exame."
	
	cEmail      := GetMv("FS_RECSEL",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"085")
	ReprovaPA2()
	Sair()
	
Return


/*/{Protheus.doc} F0500206
Assinatura de Contrato
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function AssContra()
	Local lRet 	:= .F. 
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"034")
	cAssunto    := "Em Assinatura de Contrato"
	cBody       := 	"O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+;
						PA2->PA2_SOL+" foi encaminhado para Assinatura de Contrato."
	
	cEmail      := GetMv("FS_CELADM",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif
	Sair()
Return


/*/{Protheus.doc} F0500206
Candidato Desistente Contrato
@author  queizy.nascimento 
@since 17/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function DesisContra()
	Local lRet		:= .F.
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"035")
	cAssunto    := "Candidato Desistente"
	cBody       := 	"O Candidato "+POSICIONE("SQG",1,PA2->PA2_FILCAN+PA2->PA2_CDCAND,"QG_NOME")+"  referente a solicita��o "+;
						PA2->PA2_SOL+" desistiu do Contrato."
	
	cEmail      := GetMv("FS_RECSEL",,"")
	lRet        := u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif
	U_F0500201(PA2->PA2_FILIAL, PA2->PA2_SOL,"085")
	ReprovaPA2()
	Sair()
Return


/*/{Protheus.doc} F0500206
Vaga Suspensa
@author  Fernando Carvalho 
@since 14/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function VagaSus()
	Local lMotivo		:= .F.
	Local aMotivo		:= ""
	Local cStatus		:= "Vaga Suspensa"
	Local aArea		:= GetArea()
	Local lRet			:= .F.
	cFILsQG := StaticCall( F0600401, RetFilSX2, PA2->PA2_FILIAL, "SQS")
	DbSelectArea("SQS")
	SQS->(DbSetOrder(1))
	If SQS->(dbSeek(cFILsQG + PA2->PA2_CDVAGA))
		If Empty(SQS->QS_XSUSPEN)
			While !lMotivo
				aMotivo := Motivo()
				lMotivo := aMotivo[1]
				cMotivo := aMotivo[2]
				If !lMotivo .and. !Empty(aMotivo[2])
					lMotivo := .T.
				Elseif !lMotivo .and. Empty(aMotivo[2])
					Alert("� obrigat�rio o preenchimento de uma justificativa para altera��o do Status!")	
				EndIf	
			EndDo
			If lMotivo .AND. !Empty(aMotivo[2]) .AND. !aMotivo[1] 
				RecLock("SQS",.F.)
					SQS->QS_XMOTIVO := cMotivo	
					SQS->QS_XSUSPEN := SQS->QS_XSTATUS		
					SQS->QS_XSTATUS := '6'		
				SQS->(MsUnLock())
				U_F0500201(SQS->QS_XSOLFIL,SQS->QS_XSOLPTL,"012")	
				U_F0500201(PA2->PA2_FILIAL,PA2->PA2_SOL,"012")	
				EnviaEmail(cStatus)
			EndIf	
		Else
			Alert("Vaga j� se encontra 'Suspensa'!")
		EndIf
	Else	
		Alert("Vaga n�o encontra!")
	EndIf	
	SQS->(dbcloseArea())
	ReprovaPA2()
	RestArea(aArea)
	Sair()
Return	


/*/{Protheus.doc} F0500206
Vaga Cancelada
@author  Fernando Carvalho 
@since 14/11/2016
@version 12.7
@project 2015GGS0758_MAN00000060101_EF_001
/*/
Static Function VagaCan()
	Local lMotivo		:= .F.
	Local cMotivo		:= ""
	Local cStatus		:= "Vaga Cancelada"
	Local aArea		:= GetArea()
	cFILsQG := StaticCall( F0600401, RetFilSX2, PA2->PA2_FILIAL, "SQS")
	DbSelectArea("SQS")
	SQS->(DbSetOrder(1))
	If SQS->(dbSeek(cFILsQG + PA2->PA2_CDVAGA))
		While !lMotivo
			aMotivo := Motivo()
			lMotivo := aMotivo[1]
			cMotivo := aMotivo[2]
			If !lMotivo .and. !Empty(aMotivo[2])
				lMotivo := .T.
			Elseif !lMotivo .and. Empty(aMotivo[2])
				Alert("� obrigat�rio o preenchimento de uma justificativa para altera��o do Status!")	
			EndIf	
		EndDo
		If lMotivo .AND. !Empty(aMotivo[2]).AND. !aMotivo[1]
			RecLock("SQS",.F.)
				SQS->QS_XMOTIVO := cMotivo	
				SQS->QS_XSTATUS := '5'
			SQS->(MsUnLock())
			U_F0500201(PA2->PA2_FILIAL,SQS->QS_XSOLPTL,"011")
			U_F0500201(PA2->PA2_FILIAL,PA2->PA2_SOL,"011")	
			EnviaEmail(cStatus)
		EndIf	
	Else
		Alert("Vaga n�o encontra!")
	EndIf
	SQS->(dbcloseArea())
	ReprovaPA2()
	RestArea(aArea)	
	Sair()	
Return

Static Function Motivo()
	Local oDlg
	Local lOk		
	Local cTexto1 := ""	
	
 	DEFINE DIALOG oDlg TITLE "Motivo da Altera��o" FROM 180, 180 TO 420, 700 PIXEL
 		TMultiGet():new( 01, 01, {| u | if( pCount() > 0, cTexto1 := u, cTexto1 ) },oDlg, 260, 92, , , , , , .T. )
  		TButton():New( 100, 080, "OK"		,oDlg,{||oDlg:End(),lOk := .F.}, 40,10,,,.F.,.T.,.F.,,.F.,,,.F. )
  		TButton():New( 100, 130, "Cancel"	,oDlg,{||oDlg:End(),lOk := .T.}, 40,10,,,.F.,.T.,.F.,,.F.,,,.F. )   
  	ACTIVATE DIALOG oDlg CENTERED
return {lOk,cTexto1}


Static Function EnviaEmail(cStatus)	
	Local lTrue    := .T.
	Local cAssunto := cStatus
	Local cBody    := "A vaga referente ao n�mero "+ PA2->PA2_SOL +" teve mudan�a de Status."+ CRLF+;
					  "Novo Status: " + cStatus
	Local cEmail   := ""							
	Local lRet     := .F.
	Local cTypeOrg := ""
		
	dbSelectArea("RH3")
	RH3->(dbSetOrder(1))
	RH3->(dbSeek(PA2->PA2_FILIAL+PA2->PA2_SOL))	
	
	dbSelectArea("SRA")
	SRA->(dbSetOrder(1))
	SRA->(dbSeek(RH3->RH3_FILINI+RH3->RH3_MATINI))	
	
	cEmail 	:= SRA->RA_EMAIL		
	lRet	:= u_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail	
	
	While lTrue
	
		TipoOrg(@cTypeOrg, RH3->RH3_VISAO)
		aRet	:= fBuscaSuperior(SRA->RA_FILIAL,SRA->RA_MAT,SRA->RA_DEPTO,{},cTypeOrg,RH3->RH3_VISAO)
		If Len(aRet) > 0
			
			If !Empty(aRet[1,1]) .AND. !Empty(aRet[1,2])
				dbSelectArea("SRA")
				SRA->(dbSetOrder(1))			
				If SRA->(dbSeek(aRet[1,1]+aRet[1,2]))		
					cEmail 	:= SRA->RA_EMAIL		
					lRet	:= U_F0200304(cAssunto, cBody, cEmail) //Rotina de Envio de E-mail
				EndIf
			Else
				lTrue 	:= .F.
			EndIf
			
		Else
			lTrue 	:= .F.
		EndIf	
	EndDo
	
	If !lRet
		Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
	Endif

Return 

Static Function ReprovaPA2()
	Local lRet	:= .T.
	Local aArea	:= GetArea()
	Reclock("PA2",.f.)
		PA2->PA2_SIT := "RE"
	PA2->(MsUnLock())
	
	SQG->(dbcloseArea())
	DbSelectARea("SQG")
	SQG->(dbSetOrder(1))
	If SQG->(dbSeek(PA2->PA2_FILCAN + PA2->PA2_CDCAND))
		Reclock("SQG",.f.)
		SQG->QG_XFILFAP := ""
		SQG->QG_XCODFAP := ""		
		SQG->(MsUnLock())
	EndIf		
	SQG->(dbcloseArea())
	RestArea(aArea)
Return lRet

Static Function Sair()
	Local oView := FwViewActive()
	Alert("Processo efetuado com sucesso!")	
	oView:ButtonCancelAction()	//Necess�rio para encerrar a View
Return