#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#Include 'TopConn.ch'

/*
{Protheus.doc} F0100110()
Execu��o da Rotina Manual de Medi��o Automatizada
@Author		Mick William da Silva
@Since		13/07/2016
@Version	P12.7s
@Project    MAN00000462901_EF_001 
@Param		aProds, Recebe o Produto selecionado na Tela de Browse Manual de Medi��o Automatizada
@Return		lRet, Retorna True sen�o houver diverg�ncia, caso contrario False.
*/

User Function F0100110(aProds)
	Local aArea		:= GetArea()
	Local aAreaSC1	:= SC1->(GetArea())
	Local aAreaSC1x
	Local QRYAIA 	:= GetNextAlias()
	Local QRYCNF 	:= GetNextAlias()
	Local cQuery	:= ""
	Local nI		:= 0
	Local aCabec 	:= {}
	Local aAllCab	:= {}
	Local aItem  	:= {}
	Local aItens	:= {}
	Local cDoc   	:= ""
	Local lDtIVl	:= .T.
	Local lCtNao	:= .T.
	Local aListAux	:= {}
	Local aButtons1 := {}
	Local dData
	Local aSize     := {}
	Local aObjects	:= {}
	Local aPosObjH  := {}
	Local dComp		:= Substr(dToS(dDataBase),5,2) + "/"  + Substr(dToS(dDataBase),1,4)
	Local cItem		:= "001"
	Local nValTab	:= 0

	
	Private oNoMarked   := LoadBitmap(GetResources(),'LBNO')
	Private oMarked     := LoadBitmap(GetResources(),'LBOK')
	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.
	Private aList 		:= {}
	Private oList
	Private lRet		:= .T.
	
	//Consulta o produto e devolve a o c�digo da Tabela de Pre�os
	If !Empty(aProds)
		
		//Consulta o produto e devolve a o c�digo da Tabela de Pre�os
		cQuery:= "SELECT AIA_FILIAL,AIA_CODTAB,AIA_CODFOR ,AIA_LOJFOR ,AIB_CODPRO,AIA_DATATE,AIB_PRCCOM "
		cQuery+= "FROM "+ RetSqlName("AIB") +" AIB "
		cQuery+= "LEFT JOIN "+ RetSqlName("AIA") +" AIA ON AIB_FILIAL = AIA_FILIAL AND AIB_CODTAB = AIA_CODTAB "
		cQuery+= "WHERE AIA_FILIAL ='"+ aProds[01] +"' AND AIB_CODPRO='"+ aProds[03] +"' AND AIB.D_E_L_E_T_=' ' AND AIA.D_E_L_E_T_=' ' "
		cQuery += "AND (AIA_DATATE >= '" + DtoS(dDataBase) + "' OR AIA_DATATE = ' ')"
		
		TCQUERY cQuery NEW ALIAS (QRYAIA)

		IF (QRYAIA)->(EOF())
			U_F0100105("01",aProds) //Produto n�o encontrado em nenhuma Tabela de Pre�os.
			lRet := .F.
		EndIf
			
		While !(QRYAIA)->(EOF())
		
			lDtIVl := .T.
			dData := IIF(Empty((QRYAIA)->AIA_DATATE),DtoS(dDataBase),(QRYAIA)->AIA_DATATE)
			
			IF dData < DtoS(dDataBase) //verifica data vigente
				lDtIVl := .F.
			EndIF
			
			IF lDtIVl
				cQuery := " SELECT CNA.CNA_CONTRA,CNA.CNA_NUMERO,CN9.CN9_FILIAL,CN9.CN9_DTINIC,CN9.CN9_DTFIM,CNA.CNA_FORNEC, CNA.CNA_LJFORN,CNA.CNA_REVISA,CNA.CNA_XTABPC, CNB.CNB_ITEM  "
				cQuery += " FROM " + RetSQLName("CNA") + " CNA, "
				cQuery += " INNER JOIN " + RetSQLName("CN9") + " CN9 ON CN9.CN9_NUMERO = CNA.CNA_CONTRA AND CN9.CN9_REVISA = CNA.CNA_REVISA "
				cQuery += " INNER JOIN " + RetSQLName("CNB") + " CNB ON CNB.CNB_CONTRA = CN9.CN9_NUMERO AND CN9.CN9_REVISA = CNB.CNB_REVISA AND CNA.CNA_NUMERO = CNB.CNB_NUMERO "
				cQuery += " WHERE "
				cQuery += " CNA.CNA_FILIAL = '"+ (QRYAIA)->AIA_FILIAL +"' AND "
				cQuery += " CN9.CN9_FILIAL = '"+ (QRYAIA)->AIA_FILIAL +"' AND "
				cQuery += " CN9.CN9_SITUAC =  '05' AND "
				cQuery += " CNA.CNA_SALDO  > 0 AND "
				cQuery += " CNA.D_E_L_E_T_ = ' ' AND "
				cQuery += " CN9.D_E_L_E_T_ = ' ' AND "
				cQuery += " CNB.D_E_L_E_T_ = ' ' AND "
				cQuery += " CNA.CNA_XTABPC = '"+ (QRYAIA)->AIA_CODTAB +"' AND "
				cQuery += " CNB.CNB_PRODUT = '"+ (QRYAIA)->AIB_CODPRO +"' AND "
				cQuery += " CNA.CNA_FORNEC =  '"+ (QRYAIA)->AIA_CODFOR +"' AND   "
				cQuery += " CNA.CNA_LJFORN = '"+ (QRYAIA)->AIA_LOJFOR  +"'   "
				
				cQuery := ChangeQuery( cQuery )
					
				TCQUERY cQuery NEW ALIAS (QRYCNF)
					
				IF !(QRYCNF)->(EOF())
					lDatInv:= .F.
					aCabec:= {}
					aItem := {}
					nValTab := Posicione("AIB",2,(QRYCNF)->CN9_FILIAL +(QRYCNF)->CNA_FORNEC + (QRYCNF)->CNA_LJFORN + (QRYCNF)->CNA_XTABPC + (QRYAIA)->AIB_CODPRO,"AIB_PRCCOM" )
					
					aAdd(aCabec,{"CND_CONTRA"	,(QRYCNF)->CNA_CONTRA,NIL})
					aAdd(aCabec,{"CND_REVISA"	,(QRYCNF)->CNA_REVISA,NIL})
					aAdd(aCabec,{"CND_COMPET"	,dComp				  ,NIL})
					aAdd(aCabec,{"CND_NUMERO"	,(QRYCNF)->CNA_NUMERO,NIL})
					aAdd(aCabec,{"CND_FORNEC"	,(QRYCNF)->CNA_FORNEC,NIL})
					aAdd(aCabec,{"CND_LJFORN"	,(QRYCNF)->CNA_LJFORN,NIL})
					aAdd(aCabec,{"CND_FILIAL"	,(QRYCNF)->CN9_FILIAL,NIL})
					aAdd(aCabec,{"CND_NUMMED"	,"",NIL})
					aAdd(aCabec,{"CND_PARCEL"	,"",NIL})
					
					AADD(aItem,{"CNE_FILIAL"	, aProds[1] , NIL }	)
					AADD(aItem,{"CNE_ITEM"		, cItem		, NIL }	)
					AADD(aItem,{"CNE_PRODUT"	, aProds[3] , NIL }	)
					AADD(aItem,{"CNE_QUANT"		, aProds[13], NIL }	)
					AADD(aItem,{"CNE_VLUNIT"	, nValTab	, NIL }	)
					AADD(aItem,{"CNE_VLTOT"		, nValTab * aProds[13] ,NIL	 }	)
					AADD(aItem,{"CNE_DTENT"		, dDataBase, NIL }	)
		
					AADD(aItens,aItem)
					
					IF (QRYCNF)->CN9_DTFIM <= DtoS(dDataBase) .And. !Empty((QRYCNF)->CN9_DTFIM)
						aProds[8]  := (QRYCNF)->CNA_FORNEC
						aProds[9]  := (QRYCNF)->CNA_LJFORN
						aProds[10] := (QRYCNF)->CNA_CONTRA
						aProds[11] := StoD((QRYCNF)->CN9_DTINIC)
						aProds[12] := StoD((QRYCNF)->CN9_DTFIM)
						
						lDatInv:= .T.
					EndIf
					IF !lDatInv
						aProds[8]  := (QRYCNF)->CNA_FORNEC
						aProds[9]  := (QRYCNF)->CNA_LJFORN
						aProds[10] := (QRYCNF)->CNA_CONTRA
						aProds[11] := StoD((QRYCNF)->CN9_DTINIC)
						aProds[12] := StoD((QRYCNF)->CN9_DTFIM)
						aProds[15] := (QRYAIA)->AIB_PRCCOM
						aProds[16] := (QRYCNF)->CNB_ITEM					
						aAdd(aAllCab,aCabec)
					EndIF
				
				EndIF
					
				(QRYCNF)->(dbCloseArea())
			EndIf
			(QRYAIA)->(dbSkip())
		EndDo
		
		(QRYAIA)->(DbCloseArea())
		DbSelectArea("AIA")
		
		IF Len(aAllCab) == 1
											
			IF lDatInv
				U_F0100105("04",aProds) //Contrato com data de Vig�ncia fora da Validade.
				lRet := .F.
			ELSE
				lRet:= F0110Med('1',aAllCab[1],aProds,aItens)
				
				If !lMsErroAuto
					ConOut("Incluido com sucesso! "+cDoc)
				Else
					U_F0100105("05",aProds) //"Erro na inclusao da Medi��o"
					lRet := .F.
				EndIf
			EndIf
			
		ElseIF  Len(aAllCab) >= 2
		
			For nI := 1 To Len( aAllCab )
				If aScan(aList,{|x| AllTrim(x[2])== aAllCab[nI][1][2] }) == 0
					aListAux:={.F.,aAllCab[nI][1][2],aAllCab[nI][2][2],aAllCab[nI][3][2],aAllCab[nI][4][2],aAllCab[nI][5][2],aAllCab[nI][6][2],aAllCab[nI][7][2],aAllCab[nI][8][2],aAllCab[nI][9][2],aItens[nI][2][2],aItens[nI][5][2],aItens[nI][6][2]}
					aadd(aList, aListAux)
				Endif
			Next nI
					
			aSize    := MsAdvSize(.T.)
			aObjects := {}
			AAdd(aObjects,{100,50,.T.,.T.,.T.})
			aInfo    := {aSize[1],aSize[2],aSize[3],aSize[4],5,5}
			aPosObjH := MsObjSize(aInfo,aObjects,.T.,.F.)
			oTFont := TFont():New('Courier new',,-16,.T.,.T.)
			
			DEFINE MSDIALOG oDlg TITLE "Selecione o Contrato Desejado"  FROM aSize[7],00 TO 400,1200 OF oMainWnd PIXEL
			oPanelTop:= tPanel():New(0,0,"Solicita��o: "+aProds[2]+" Produto: "+aProds[3]+".",oDlg,oTFont,.T.,,,CLR_HGRAY,00,020)
			oPanelTop:align:= CONTROL_ALIGN_TOP
			@ 30, 10 LISTBOX oList VAR cLbx1 FIELDS HEADER '','Nr. Contrato','Num.Revisao','Competencia', ;
				'Nr Planilha','Fornecedor','Loja','Filial','Nr Medicao','Parcela','Item','Prc Final','Prc Total' SIZE 500, 400 OF oDlg ;
				ON DBLCLICK (F0110Mark()) PIXEL

			oList:align:= CONTROL_ALIGN_ALLCLIENT
			oList:SetArray(aList)
			
			// Monta a linha a ser exibina no Browse
			oList:bLine := {||{ Iif(aList[oList:nAt,01],oMarked,oNoMarked),;
				aList[oList:nAt,02],;
				aList[oList:nAt,03],;
				aList[oList:nAt,04],;
				aList[oList:nAt,05],;
				aList[oList:nAt,06],;
				aList[oList:nAt,07],;
				aList[oList:nAt,08],;
				aList[oList:nAt,09],;
				aList[oList:nAt,10],;
				aList[oList:nAt,11],;
				aList[oList:nAt,12],;
				aList[oList:nAt,13]	} }
	
			ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| lRet:= F0110Med('2',aList,aProds,aItens),oDlg:End() },{||lRet:= .F.,oDlg:End()},,aButtons1)

	
		Elseif !lDtIVl
			U_F0100105("02",aProds) //Produto com a Data Vigente vencida.
			lRet := .F.
		Else
			U_F0100105("03",aProds) //"N�o foi possivel realizar a Medi��o, pois a Tabela de Pre�o n�o foi Encontrado em nenhum Contrato !"
			lRet := .F.
		EndIf
		
	Endif
	
	RestArea(aAreaSC1)
	RestArea(aArea)
Return ( lRet )

/*
{Protheus.doc} F0110Mark()
Marca/Desmarca registro
@Author		Mick William da Silva
@Since		14/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001	
*/

Static Function F0110Mark()
	Local nX:= 0

	For nX:= 1 to Len(aList)
		aList[Nx][1] := .F.
	Next nX
	
	aList[oList:nAT,1] := !aList[oList:nAT,1]
	oList:Refresh()
Return


/*
{Protheus.doc} F0110Med()
Executa a Medica��o
@Author		Mick William da Silva
@Since		14/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001 
@Param		cTip, 1- Quando vem direto, sem a necessidade de selecionar o CONTRATO; 2 -  Vem da Tela "Selecione o Contrato Desejado".
@Param		aCabs , Cabe�alho do Contrato.	
@Param		aProds, Produtos.
@Param		aItens Itens para Medi��o
@Return		lRet, Retorna Verdadeiro caso n�o exista diverg�ncia, caso contr�rio False. 
*/
Static Function F0110Med(cTip,aCabs,aProds,aItens)
	Local nY:= 0
	Local aAux := aCabs
	Local aCab := {}
	Local cPos := 0
	Local aItem:= aItens
	Local aAuxIt:={}
	Local aRet	  := {}

	IF cTip == '2'
		aCabs 	:= {}
		aItens	:= {}
		cPos 	:= AScan(aAux,{|x| x[1] == .T. })

		IF !Empty(cPos)
			aAdd(aCab,{"CND_CONTRA",aAux[cPos][2],NIL})
			aAdd(aCab,{"CND_REVISA",aAux[cPos][3],NIL})
			aAdd(aCab,{"CND_COMPET",aAux[cPos][4],NIL})
			aAdd(aCab,{"CND_NUMERO",aAux[cPos][5],NIL})
			aAdd(aCab,{"CND_FORNEC",aAux[cPos][6],NIL})
			aAdd(aCab,{"CND_LJFORN",aAux[cPos][7],NIL})
			aAdd(aCab,{"CND_FILIAL",aAux[cPos][8],NIL})
			aAdd(aCab,{"CND_NUMMED",aAux[cPos][9],NIL})
			aAdd(aCab,{"CND_PARCEL",aAux[cPos][10],NIL})
			
			aProds[8]  := aAux[cPos][6]
			aProds[9]  := aAux[cPos][7]
			aProds[10] := aAux[cPos][2]
			aProds[15] := aAux[cPos][12]
			aProds[16] := aAux[cPos][11]			
			
			//AADD(aCabs,aCab)
			aCabs := aClone(aCab)
		EndIf
		aAuxIt := aItem[cPos]
		AADD(aItens,aAuxIt)
	EndIf
 
	
	aAdd(aRet,{aCabs,aProds})
Return aRet