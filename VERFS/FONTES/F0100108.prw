#Include 'Protheus.ch'

/*
{Protheus.doc} F0100108()
Inclus�o de Log de Eventos e Medi��o Automatizada na rotina de "Gera��o de Cota��o"
@Author     Mick William da Silva
@Since      11/07/2016
@Version    P12.7
@Project    MAN00000462901_EF_001    
@Return		aNewRot, Retorna as Novas Rotinas a serem inclusas no A��es Relacionadas Browse.     
*/

User Function F0100108() 

	AAdd( aRotina, { "Logs de Eventos", "U_F0100106()",0, 2, 0 } )
	AAdd( aRotina, { "Medi��o Auto"	  , "U_F0100109()",0, 2, 0 } )

Return( )
