#include 'protheus.ch'
#include 'apwebsrv.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0700801
Valida��o para incluir e alterar
@author Fernando Carvalho
@since 23/01/2017
@Project MAN0000007423041_EF_008
@param oRegFabricante, objeto, (Descri��o do par�metro)
/*/
User Function F0700801(oRegFabricante)
	Local cRetorno := ""
    Local aCampos  := {}
    
    aCampos := {;
                    {"P13_FILIAL"	, oRegFabricante:cFilial    },;
                    {"P13_COD"   	, oRegFabricante:cCOD       },;
                    {"P13_TIPO"		, oRegFabricante:cTIPO   	},; 
                    {"P13_DESCR"	, oRegFabricante:cDESCR    	},;
                    {"P13_CGC"		, oRegFabricante:cCGC    	},;
                    {"P13_MSBLQL"	, oRegFabricante:cMSBLQL    },;
                    {"P13_ID"    	, U_GetIntegID()            };//-- fun��o pra pegar o ID          	
                }
    
    cChave   := u_RetChave("P13", aCampos,{1,2})
    cRetorno := u_UpsMVCRg("P13", cChave, "F0700701", "MASTER", aCampos) //-- chamar fun��o de log

    aCampos := ASize(aCampos,0)
    aCampos := Nil

Return cRetorno