#Include 'Protheus.ch'
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} F0500411
Utilizado para filtrar consultas padroes 
@type function
@author Ademar Fernandes
@since 26/01/2017
@version 1.0
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500411(nMyOpc)

	Local oModAtu	:= FWModelActive()
	Local cBkpFil	:= cEmpAnt+cFilAnt
	Local cFilProp	:= oModAtu:GetModel("TRFDETAIL"):GEtValue("FILIPROP")
	Local cCustoProp:= oModAtu:GetModel("TRFDETAIL"):GEtValue("CCUSTOPROP")
	Local cDepProp	:= oModAtu:GetModel("TRFDETAIL"):GEtValue("DEPTOPROP")
	Local cRetorno	:= ""
	
	Default nMyOpc := 0
	
	If Empty(cFilProp)
		cFilProp := oModAtu:GetModel("TRFDETAIL"):GEtValue("TMP_FILIAL")  
	EndIf
	
	If nMyOpc = 0	//-Busca a "Filial Para" 
		dbSelectArea("SM0")
		SM0->(dbSetOrder(1))
		if SM0->(dbSeek(cEmpAnt+cFilProp))
			cRetorno := SM0->(Alltrim(M0_NOME)+" - "+Alltrim(M0_FILIAL))
		EndIf
		SM0->(dbSeek(cEmpAnt+cBkpFil))
	
	ElseIf nMyOpc = 2	//-Filtro do C.Custo (CTT) para usar no SXB
		cRetorno += "CTT->CTT_FILIAL == '"+cFilProp+"' .OR. Empty(CTT->CTT_FILIAL) "
		
	ElseIf nMyOpc = 1	//-Filtro do Depto.(SQB) para usar no SXB
		cRetorno += "(SQB->QB_FILIAL == '"+cFilProp+"' .OR. Empty(SQB->QB_FILIAL)) "
		cRetorno += ".AND. (SQB->QB_CC == '"+cCustoProp+"' .OR. Empty(SQB->QB_CC)) "
		
	ElseIf nMyOpc = 3	//-Filtro do Posto (RCL) para usar no SXB
		If Empty(cDepProp)
			cDepProp := oModAtu:GetModel("TRFDETAIL"):GEtValue("TMP_DEPTOA")  
		EndIf
		cRetorno += "(RCL->RCL_FILIAL == '"+cFilProp+"' .OR. Empty(RCL->RCL_FILIAL)) "
		cRetorno += ".AND. (RCL->RCL_CC == '"+cCustoProp+"' .OR. Empty(RCL->RCL_CC)) "
		cRetorno += ".AND. (RCL->RCL_DEPTO == '"+cDepProp+"' .OR. Empty(RCL->RCL_DEPTO)) "
		
	EndIf

	If nMyOpc <> 0
		cRetorno := "@#" + cRetorno + "@#"
	EndIf
	
Return(cRetorno)
