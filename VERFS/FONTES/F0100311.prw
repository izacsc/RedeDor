#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100311()
Realiza o processo de aprova��o ou reprova��o
@Author     Bruno de Oliveira, Henrique Madureira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100311()
	
	Local cHtml   := ""
	Local oSolic  := Nil
	
	Private cMsg
	
	WEB EXTENDED INIT cHtml START "InSite"

	   	oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
		
		oSolic := WSW0500308():New()
		WsChgURL(@oSolic,"W0500308.apw")
		
		cFililS  := httpGet->cFilil
		cCodSol  := HttpGet->cSolic
		cVisPad  := HttpSession->aInfRotina:cVisao
		cConfirm := httpget->cAceita
		cLugar   := HttpPost->cLugar
		
		If oSolic:InfSolVg(cFililS,cCodSol)
		
			oOrg:cParticipantID  := ""
			oOrg:cVision         := oSolic:oWSInfSolVgResult:cRH3VISAO
			oOrg:cEmployeeFil    := oSolic:oWSInfSolVgResult:cRH3FILINI
			oOrg:cRegistration   := oSolic:oWSInfSolVgResult:cRH3MATINI
			oOrg:cEmployeeSolFil := oSolic:oWSInfSolVgResult:cRH3FILAPR
			oOrg:cRegistSolic    := oSolic:oWSInfSolVgResult:cRH3MATAPR
		
		EndIf
		
		If oOrg:GetStructure()
		
			cFilSol := oOrg:cEmployeeSolFil
			cMatSol := oOrg:cRegistSolic
			cDepto  := ""
			cVOrg   := HttpSession->aInfRotina:cVisao
			cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
			
			oSolic:VerAprvSit(cFilSol,cMatSol,cDepto,cVOrg,cEmpFunc,"2")
			
			oSolic:oWSSolicitVag:cFilialVg  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitVag:cCodMatric := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitVag:cFilSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitVag:cMatSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitVag:cVisaoOrg  := cVOrg
			oSolic:oWSSolicitVag:cFilAprov  := oSolic:oWSVERAPRVSITRESULT:cFilAprov
			oSolic:oWSSolicitVag:cMatAprov  := oSolic:oWSVERAPRVSITRESULT:cMatAprov
			oSolic:oWSSolicitVag:nNvlAprov  := oSolic:oWSVERAPRVSITRESULT:nNvlAprov
			oSolic:oWSSolicitVag:cEmpAprov  := oSolic:oWSVERAPRVSITRESULT:cEmpAprov
			oSolic:oWSSolicitVag:cEmpresa   := GetEmpFun()
			oSolic:oWSSolicitVag:cCODSOLVG  := cCodSol
			
			If cConfirm == "1" //Aprovar
			
				oSolic:AprSolVaga()
				cMsg := "Solicita��o aprovada!"
			
			ElseIf cConfirm == "2" //Reprovar
			
				oSolic:oWSSolicitVag:cTpRj  := "1" //Cancelar
				oSolic:oWSSolicitVag:cXObs := HttpPost->txtobs
				
				oSolic:RepSolVaga()
				cMsg := "Solicita��o reprovada!"
			
			EndIf
	
		EndIf
				
		If cLugar != '1'
			cHtml := ExecInPage("F0100301")
		Else
			cHtml := ExecInPage("F0500305")
		EndIf
	WEB EXTENDED END

Return cHtml