#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200314()
Aprova/Reprova
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return	 cHtml
*/
User Function F0200314()
	
	Local cHtml   := ""
	Local aAux    := {}
	Local nCnt    := 1
	Local oParam  := Nil
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	cMat     := HTTPSession->RHMat
	cMsg     := "Registro n�o alterado."
	cSolic   := HttpGet->cSolic
	cStatus  := HttpGet->cAprova
	cNome    := HttpPost->cNome
	cCalend  := HttpPost->txtCalendario
	cCurso   := HttpPost->txtCurso
	cTurma   := HttpPost->txtTurma
	cFilis   := HttpGet->cFilis
	
	oParam := WSW0200301():new()
	WsChgURL(@oParam,"W0200301.APW")
	
	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
		
	fGetInfRotina("U_F0200306.APW")
	GetMat()								//Pega a Matricula e a filial do participante logado
	
	oOrg:cParticipantID   := ""
	oOrg:cVision          := HttpSession->aInfRotina:cVisao
	oOrg:cEmployeeFil     := HttpSession->aUser[2]
	oOrg:cRegistration    := HttpSession->RhMat
	oOrg:cEmployeeSolFil  := HttpSession->aUser[2]
	oOrg:cRegistSolic     := HttpSession->RhMat
	
	If oOrg:GetStructure()
		
		oSolic := WSW0500308():New()
		WsChgURL(@oSolic,"W0500308.apw")
		
		cFilSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
		cMatSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
		cVOrg    := HttpSession->aInfRotina:cVisao
		cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
		cDepto   := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cDepartment
		
		////cEmployeeFilial, Registration, Department,Visao  ,EmployeeEmp, Registration
		If oSolic:VerAprvSit(cFilSol,cMatSol,cDepto,cVOrg,cEmpFunc,'2')
			
			cFilAprov  := oSolic:oWSVERAPRVSITRESULT:cFilAprov
			cMatAprov  := oSolic:oWSVERAPRVSITRESULT:cMatAprov
			
			If cStatus == "1"
				If oParam:PegSuper(cMat, cSolic, '1',cFilAprov,cMatAprov,cFilis)
					If Empty(cMatAprov)
						U_F0200302(2, cMatSol,cNome,cCalend,cCurso,cTurma,cFilSol)
					Else
						U_F0200302(1, cMatAprov,cNome,cCalend,cCurso,cTurma,cFilAprov)
					EndIf
					cMsg := "Aprovado com sucesso."
				EndIf
			Else
				If oParam:PegViInv(cMat,cSolic,'1', cFilis)
					cMsg := "Reprovado com sucesso."
					For nCnt := 1 To Len(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL)
						cMat := IIF(EMPTY(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL[nCnt]:CQBMATRESP),cMat,OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL[nCnt]:CQBMATRESP)
						U_F0200302(3, cMat,cNome,cCalend,cCurso,cTurma,cFilSol)
					Next
					
				EndIf
			EndIf
		Else
			cMsg := "Erro no cadastro! Erro no cadastro de vis�o."
		EndIf
	EndIf
	
	cHtml := ExecInPage("F0500305")
	
	WEB EXTENDED END
	
Return cHtml

