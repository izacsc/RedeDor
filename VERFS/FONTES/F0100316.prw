#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100316()
Exibi��o dos postos referentes ao departamento selecionado
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100316()

	Local cHtml   	:= ""
	Local oOrg      
	Local nIndiceDepto	:= Val(HttpGet->nIDept)
	
	WEB EXTENDED INIT cHtml START "InSite"
	                     
	 	Default HttpSession->Postos  := {}
	 	cOpc := HttpGet->cOpc	 	
		oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg, "ORGSTRUCTURE.APW",,,HttpSession->cEDepto)
		
		cCodDepto := HttpSession->Department[nIndiceDepto]:cDepartment
		cDescrDepto := HttpSession->Department[nIndiceDepto]:cDescrDepartment
		                     
		//LISTA DE POSTOS	                
		oOrg:cCompanyID		:= HttpSession->Department[nIndiceDepto]:cDepartmentEmp
		oOrg:cDepartmentID	:= HttpSession->Department[nIndiceDepto]:cDepartment
		oOrg:cRequestType   := HttpSession->cTypeRequest
		HttpSession->DadosFunc := WsClassNew('ORGSTRUCTURE_DATAEMPLOYEE')
		HttpSession->DadosFunc:CEMPLOYEEEMP := HttpSession->Department[nIndiceDepto]:cDepartmentEmp
		
		If oOrg:GetPostos()
			HttpSession->Postos := oOrg:oWSGETPOSTOSRESULT:oWSLISTOFPOSTS:oWSDATAPOSTOS
			nPTotal	        := oOrg:oWSGETPOSTOSRESULT:nPagesTotal
		EndIf
		
		HttpCTType("text/html; charset=ISO-8859-1")		
		cHtml := ExecInPage( "F0100316" )
			
	WEB EXTENDED END
	
Return cHtml