#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#include 'FILEIO.ch'
/*
{Protheus.doc} F0300603()
Exporta��o de Dependentes
@Author     Rogerio Candisani
@Since      18/10/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300603()

Local cctmpTit:= ""
Local cctmpDep :=""
Local montaTxt:= ""
Local cPerg:= "FSW0300603"	// Gp. perguntas especifico
Local cGrauPar:=""
Local lRet:= .F. // confirma a opera��o
Local lGerou := .f. 
//exportar os dados do dependenter em .CSV
//criar pergunta F0300603

/*	Exporta��o Dependente:
MV_PAR01 Filial De
MV_PAR02 Filial At�
MV_PAR03 Matr�cula De
MV_PAR04 Matr�cula At�
MV_PAR05 Admiss�o De
MV_PAR06 Admiss�o ate
MV_PAR07 Vig�ncia De
MV_PAR08 Vig�ncia At�
MV_PAR09 Tipo de Arquivo (M�dico / Odontol�gico)
MV_PAR10 Caminho de Grava��o
*/

//Observa��es para exporta��es dos Dependentes:
//1)	As Colunas 1, 2, 3, 5, 6 e 15 - Informar somente a coluna, o dado ser� vazio;
//2)	A Coluna 14 � Informar o grau de parentesco de acordo com o conte�do do campo RB_GRAUPAR;
//3)	As Colunas que foram indicadas para serem geradas �em branco� ter�o dados informados manualmente pela equipe de benef�cios;
//4)	Os tamanhos das colunas s�o livres, e dever�o ser separados por �; �.
	
Pergunte(cPerg,.T.)

If (MsgYesNo(OemToAnsi("Confirma a exporta��o do dependente ?"),OemToAnsi("Atencao")))
	lRet:= .T.
Else
	lRet:= .F.
Endif
 
 
If lRet 
	//montar query do titular
	ctmpTit:=GetNextAlias()
	BeginSql Alias ctmpTit
		SELECT RA_ADMISSA, RA_FILIAL, RA_MAT, RA_NOME   
		FROM %table:SRA% SRA
		Inner join %table:RHK% RHK ON 
			RHK.RHK_MAT = SRA.RA_MAT AND
			RHK.%NotDel%
		WHERE SRA.RA_FILIAL BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
			AND SRA.RA_MAT BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04%
			AND SRA.RA_ADMISSA BETWEEN %Exp:MV_PAR05% AND %Exp:MV_PAR06%
			AND RHK.RHK_PERINI >= %Exp:MV_PAR07%
			AND ( RHK.RHK_PERFIM <= %Exp:MV_PAR08% OR RHK.RHK_PERFIM = '' ) // a pedido do Trombini mudado para RHK_PERINI  
			AND RHK.RHK_TPFORN = %Exp:MV_PAR09%
			AND SRA.%NotDel%
		ORDER BY %Order:SRA%
	EndSql	
		
	//gera dados dos dependentes de cada titular com plano ativo
	DbSelectArea(ctmpTit)
	(ctmpTit)->(DbGoTop())
	While !(ctmpTit)->(EOF()) 
	
			lgerou := .t. 
			//montar query do dependente
			ctmpDep:=GetNextAlias()
			BeginSql Alias ctmpDep
				SELECT RB_MAT, RB_NOME,RB_SEXO, RB_DTNASC, RB_GRAUPAR,RB_CIC,
					RB_MAE       
				FROM %table:SRB% SRB
				Inner join %table:RHL% RHL ON 
					%xFilial:RHL% = RHL.RHL_FILIAL AND 
					RHL.RHL_MAT = SRB.RB_MAT AND
					RHL.RHL_CODIGO = SRB.RB_COD AND
					RHL.%NotDel%
				WHERE SRB.RB_FILIAL = %xFilial:SRB%
					AND SRB.RB_MAT = %Exp:(ctmpTit)->RA_MAT%
					AND RHL.RHL_PERINI >= %Exp:MV_PAR07% 
					AND RHL.RHL_PERINI <= %Exp:MV_PAR08% // a pedido do Trombini mudado para RHK_PERINI
					AND RHL.RHL_TPFORN = %Exp:MV_PAR09% 
					AND SRB.%NotDel%
				ORDER BY %Order:SRB%
			EndSql	
			
			
			DbSelectArea(ctmpDep)
			(ctmpDep)->(DbGoTop())
			cMontaTxt := ""
			While !(ctmpDep)->(EOF())
			    
				//monta o txt dos dependentes]
				//ajustar o montaTxt
				//colunas 4, 7,8,9,10,11,12,13,14,16,17,18,19
				cMontaTxt += ";" // 1 - brancos
				cMontaTxt += ";" // 2 - brancos
				cMontaTxt += ";" // 3 - brancos
				cMontaTxt += (ctmpDep)->RB_MAT + ";" // 4
				cMontaTxt += ";" // 5 - brancos
				cMontaTxt += ";" // 6 - brancos
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(ctmpDep)->RB_MAT,"RA_NOME") + ";" // 7 nome do titular
				cMontaTxt += (ctmpDep)->RB_CIC + ";" // 8 N. CPF , Informar o CPF beneficiario, se menos de 18 anos CPF nao deve ser preenchido
				cMontaTxt += (ctmpDep)->RB_NOME + ";"// 9 Nome do segurado
				cMontaTxt += (ctmpDep)->RB_CIC + ";" // 10 N. CPF , Informar o CPF beneficiario, se menos de 18 anos CPF nao deve ser preenchido
				cMontaTxt += subst((ctmpDep)->RB_DTNASC,7,2) + "/" + subst((ctmpDep)->RB_DTNASC,5,2) + "/" + subst((ctmpDep)->RB_DTNASC,1,4) + ";" // 11 Data nascimento , Informar a data DD/MM/AAAA
				cMontaTxt += IIF((ctmpDep)->RB_SEXO == "M","Masculino","Feminino") + ";" // 12 Sexo , Masculino ou Feminino
				cMontaTxt += ";" // 13 estado civil
				Do Case
					 Case (ctmpDep)->RB_GRAUPAR == "C"
					 		cGrauPar:= "C�njuge/Companheiro"
					 Case (ctmpDep)->RB_GRAUPAR == "F"
					 		cGrauPar:= "Filho(a)"
					 Case (ctmpDep)->RB_GRAUPAR == "E"
					 		cGrauPar:= "Enteado(a)"		
					 Case (ctmpDep)->RB_GRAUPAR == "P"
					 		cGrauPar:= "Pai/M�e"
					 Case (ctmpDep)->RB_GRAUPAR == "O"
					 		cGrauPar:= "Agregado/Outros"		
				EndCase
				cMontaTxt += cGrauPar + ";" // 14 Grau de parentesco ,Quando for informa��es do Titular, informar fixo "Titular"
				cMontaTxt += ";" // 15 data de casamento
				cMontaTxt += ";" // 16 data de inclusao no plano
				cMontaTxt += (ctmpDep)->RB_MAE + ";" // 17 Nome da m�e , informa��o nome da m�e completo
				cMontaTxt += ";" // 18 numero de nascido vivo, opcional
				cMontaTxt += ";" // 19 numero de CNS, opcional
				cMontaTxt += CHR(13)+ CHR(10)
				(ctmpDep)->(DbSkip())
			Enddo
		(ctmpDep)->(DbCloseArea()) 	
		DbSelectArea(ctmpTit)
		(ctmpTit)->(DbSkip())
	Enddo
	
	//fechar arquivos temporarios
	(ctmpTit)->(DbCloseArea())
	
	//gerar o arquivo
	If lgerou
		criaCSV()
	Else
		MsgAlert("N�o existem dados a serem gerados, verifique os parametros utilizados")
	Endif 
	
Endif	

Return lRet

///////////////////////////////////////////////////////////////////////
// Exportando dados para planilha 
////////////////////////////////////////////////////////////////////////
Static Function criaCSV()

// Nome do arquivo criado, o nome � composto por umam descri��o 
//a data e a hora da cria��o, para que n�o existam nomes iguais
cNomeArq := alltrim(MV_PAR10) + ".csv"

// criar arquivo texto vazio a partir do root path no servidor
nHandle := FCREATE(cNomeArq)

FWrite(nHandle,cMontaTxt)

// encerra grava��o no arquivo
FClose(nHandle)

FOPEN(cNomeArq, FO_READWRITE)

//MsgAlert("Relatorio salvo em:" + cNomeArq )

Return	
