#include "protheus.ch"
#include "TBICONN.CH"

/*/{Protheus.doc} TsJ00103

@project	MAN0000007423040_EF_001
@type 		function
@author 	alexandre.arume
@since 		09/11/2016
@version 	1.0
@return 	${return}, ${return_description}

/*/
User Function TsJ00103()
	Local aVet := {'01', '01010002'}
	U_F0600103(aVet)      
Return

/*/{Protheus.doc} F0600103
Fun��o chamada no Job, que seleciona os registros da PA6 para serem integrados para a tabela intermedi�ria.

@project	MAN0000007423040_EF_001
@type 		function
@author 	alexandre.arume
@since 		09/11/2016
@version 	1.0
@return 	${return}, ${return_description}

/*/
User Function F0600103(aParam)
	
	Local cQuery 	:= ""
	Local cAlias	:= GetNextAlias()
	Local dDtIni    := Date()
	Local cHora     := Time()
	Local cEmpIni   := IIF(ValType(aParam) == "A", aParam[1], cEmpAnt)
	Local cFilIni   := IIF(ValType(aParam) == "A", aParam[2], cFilAnt)
	Local cId		:= ""
	Local lRet		:= .T.
	
	// Verifica se j� existe um Job em execu��o com o mesmo nome.
	If !LockByName("F0600103_" + cEmpIni, .F. ,.F.)
		Conout("F0600103 - Ja existe um Job com mesmo nome em execucao!")
	Else
	
		ConOut("**************************************************************************")
		ConOut("* F0600103: Integracao dos Cadastro						 				 *")
		ConOut("* Inicio: " + Dtos(dDtIni) + " - " + cHora + "                   		 	 *")
		ConOut("* Montagem do ambiente na empresa " + cEmpIni + " - " + cFilIni + " 		 *")
		Conout("* F0600103 - Inicio Thread: '" + cValToChar(ThreadID()) 				)
		ConOut("**************************************************************************")
		
		RpcSetType(3)
		If !RpcSetEnv( cEmpIni, cFilIni,,,"FAT",, /*aFiles*/ )
			Conout("F0600103 - Nao foi possivel inicializar o ambiente.")
		Else	
	
			cQuery := " SELECT PA6_ID, PA6_ALIAS, PA6_CHALIA, PA6_OPERAC, PA6_RECNOT, PA6_FUNC, "
			cQuery += "  R_E_C_N_O_ RECPA6 "
			cQuery += " FROM " + RetSqlName("PA6") + " "
			cQuery += " WHERE PA6_FILIAL = '" + xFilial("PA6") + "' "
			cQuery += " AND D_E_L_E_T_ = ' ' "
			cQuery += " AND PA6_DATENV = ' ' "
			cQuery += " ORDER BY PA6_DATA ASC, PA6_HORA ASC, PA6_RECNOT ASC "
			cQuery := ChangeQuery(cQuery)
			
			dbUseArea(.T., "TOPCONN", TCGenQry( ,,cQuery ), cAlias, .F., .T.)
			
			While !(cAlias)->(Eof())
					
				Begin Transaction
				
					cId := AllTrim((cAlias)->PA6_ID)
					
					// EF 001 - Cadastro de Funcion�rios.
					If (cAlias)->PA6_ALIAS == "SRA"
												
							If U_F0600101(cId,(cAlias)->PA6_RECNOT)
							
								UpdatePA6(cId)
							Else							
								DisarmTran()
							EndIf
						
						SRA->(dbCloseArea())
					
					// EF 002 - Cadastro de Afastamentos.
					ElseIf (cAlias)->PA6_ALIAS == "SR8"
						
						If U_F0600201(cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT)
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
						SR8->(dbCloseArea())
						
					// EF 003 - Atualiza��o de Dados Contratuais.
					ElseIf (cAlias)->PA6_ALIAS == "SPF"
						
						If U_F0600301((cAlias)->PA6_ALIAS, cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT, "1")
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
						SPF->(dbCloseArea())
						
					// EF 003 - Atualiza��o de Dados Contratuais.
					ElseIf (cAlias)->PA6_ALIAS == "SR3"
												
						If U_F0600301((cAlias)->PA6_ALIAS, cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT, "2")
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
						SR3->(dbCloseArea())
						
					// EF 003 - Atualiza��o de Dados Contratuais.
					ElseIf (cAlias)->PA6_ALIAS == "SR7"
						
						If U_F0600301((cAlias)->PA6_ALIAS, cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT, "3")
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
						SR7->(dbCloseArea())
						
					// EF 003 - Atualiza��o de Dados Contratuais.
					ElseIf (cAlias)->PA6_ALIAS == "SR9"
						
						If U_F0600301((cAlias)->PA6_ALIAS, cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT)
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
						SR9->(dbCloseArea())
						
					// EF 005 - Cadastro de F�rias.
					ElseIf (cAlias)->PA6_ALIAS == "SRH"
						
						If U_F0600501(cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT)
						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
					// EF 007 - Transferencias.
					ElseIf (cAlias)->PA6_ALIAS == "SRE"
						
						If AllTrim((cAlias)->PA6_FUNC) == "F0600301"
							lRet := U_F0600301((cAlias)->PA6_ALIAS, cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT, "6")
						Else
							lRet := U_F0600701(cId, (cAlias)->PA6_OPERAC, (cAlias)->PA6_RECNOT)
						EndIf
						
						If lRet 
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
					// EF 006 - Aprovacao Recisao
					ElseIf (cAlias)->PA6_ALIAS == "RH3"
												
						If U_F0600602(cId, "RH3", (cAlias)->PA6_RECNOT, (cAlias)->PA6_OPERAC)						
							UpdatePA6(cId)
						Else
							DisarmTran()
						EndIf
						
					// EF 006 - Exclusao Calculo Rescisao
					ElseIf (cAlias)->PA6_ALIAS == "SRG"
												
						If AllTrim((cAlias)->PA6_FUNC) == "F0601101" 
							If U_F0601101(cId, "SRG", (cAlias)->RECPA6, (cAlias)->PA6_OPERAC)	
								UpdatePA6(cId)
							Else
								DisarmTran()
							EndIf
						Else

							If U_F0600602(cId, "SRG", (cAlias)->PA6_RECNOT, (cAlias)->PA6_OPERAC)						
								UpdatePA6(cId)
							Else
								DisarmTran()
							Endif
						Endif		
					EndIf
					
					(cAlias)->(dbSkip())
					
				End Transaction
				
			End
			
			(cAlias)->(dbCloseArea())
			
			RpcClearEnv() // Desconecta ambiente.
			
		EndIf
		
		Conout("F0600103 - Final Thread: " + cValToChar(ThreadID()))
		
		UnLockByName("F0600103_" + cEmpIni, .F., .F.)
		
	EndIf
	
Return

/*/{Protheus.doc} UpdatePA6
Atualiza data de envio.

@project	MAN0000007423040_EF_001
@type 		function
@author 	alexandre.arume
@since 		09/11/2016
@version 	1.0
@return 	${return}, ${return_description}

/*/
Static Function UpdatePA6(cId)
	
	dbSelectArea("PA6")
	PA6->(dbSetOrder(1))
	If PA6->(dbSeek(FWXFILIAL("PA6")+cId))
		
		RecLock("PA6", .F.)
		PA6->PA6_DATENV := Date()
		PA6->(MsUnlock())
		
	EndIf
	
	PA6->(dbCloseArea())
	
Return
