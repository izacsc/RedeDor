#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100321()
Busca as informa��es da solicita��o escolhida.
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100321()
	
	Local cHtml := ""
	Local oSol  := Nil
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	    cFililS := httpget->cFilil
	    cCodSol := httpget->cSolic
	    cMatric := HttpSession->RHMat
	    cVisPad := HttpSession->aInfRotina:cVisao
	    nOperac := httpget->nGrid
	    
	  	oSol := WSW0500308():New()
		WsChgURL(@oSol,"W0500308.APW")            
	
		If oSol:BscSolQdro(cCodSol,cMatric,cVisPad,cFililS)
			cFilQd := oSol:oWSBscSolQdroRESULT:cFilQdr
			cCDept := oSol:oWSBscSolQdroRESULT:cCdDept
			cDDept := oSol:oWSBscSolQdroRESULT:cDsDept
			cCFunc := oSol:oWSBscSolQdroRESULT:cCdFunc
			cDFunc := oSol:oWSBscSolQdroRESULT:cDsFunc
			cCCarg := oSol:oWSBscSolQdroRESULT:cCdCarg
			cDCarg := oSol:oWSBscSolQdroRESULT:cDsCarg
			cCCtt  := oSol:oWSBscSolQdroRESULT:cCdCCtt
			cDCtt  := oSol:oWSBscSolQdroRESULT:cDscCtt
			cSalar := oSol:oWSBscSolQdroRESULT:cSalari
			cTpCnt := oSol:oWSBscSolQdroRESULT:cTpCont
			cQntQd := oSol:oWSBscSolQdroRESULT:cQntQdr
			cTpPst := oSol:oWSBscSolQdroRESULT:cTpPost
			cObs   := oSol:oWSBscSolQdroRESULT:cObserv
			cCdPst := oSol:oWSBscSolQdroRESULT:cCodPst
			cOrgSl := oSol:oWSBscSolQdroRESULT:cOrigSl
		EndIf	
			
		HttpCTType("text/html; charset=ISO-8859-1")		
		cHtml := ExecInPage( "F0100321" )
		
	WEB EXTENDED END
		
Return cHtml