#include 'Protheus.ch'
#include "FWMVCDEF.CH"

#define cHdSC7 ""
#define cItemSC7 ""

// Indica se a fun��o SumDesc ou SumDesp foi chamada.
Static lFunSum := .F.

/*
{Protheus.doc} F0100401()
Gera��o Solicita��o de Pagamentos - Req. ID_635
@Author     Mick William da Silva
@Since      29/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Menu		Compras\Atualiza��es\Pedidos\solicita��es de Pagamento
*/
User Function F0100401()

	Local oBrw 		:= FwMBrowse():New()
	Local aColunas	:= {} 
	
	oBrw:SetDescription("Solicita��o de Pagamentos")
	oBrw:SetAlias("SC7")
	oBrw:SetMenuDef("F0100401")
	oBrw:SetFilterDefault("C7_XSOLPAG == '1'")
	
	//Incrementa coluna no browse
	aColunas	:=	{{'Numero'		,'C7_NUM'		,'C',6,0,'@!'}}
	oBrw:SetFields(aColunas)
	
	oBrw:AddLegend("C7_QTDACLA > 0"														, "BR_LARANJA"	, "Em recebimento (Pr�-nota)")
	oBrw:AddLegend("C7_QUJE >= C7_QUANT"												, "DISABLE"		, "Recebido")
	oBrw:AddLegend("C7_QUJE <> 0 .AND. C7_QUJE < C7_QUANT"							, "BR_AMARELO"	, "Recebido parcialmente")
	oBrw:AddLegend("C7_CONAPRO == 'B' .AND. C7_QUJE < C7_QUANT"						, "BR_AZUL"		, "Em aprova��o")
	oBrw:AddLegend("C7_QUJE == 0 .AND. C7_QTDACLA == 0"								, "ENABLE"			, "Pendente")
	oBrw:AddLegend("C7_ACCPROC == '1'"													, "PMSEDT2"		, "Aguardando aceite do fornecedor (Marketplace)")
	oBrw:AddLegend("!Empty(C7_CONTRA) .AND. Empty(C7_RESIDUO)"						, "BR_BRANCO"		, "Pedido de Contrato (GCT)")
	oBrw:AddLegend("!Empty(C7_RESIDUO)"												, "BR_CINZA"		, "Com res�duo eliminado")
	oBrw:AddLegend("C7_TIPO != 1"														, "BR_PRETO"		, "Autoriza��o de Entrega")
	
	oBrw:Activate()
	
Return

/*{Protheus.doc} F0100409
Bot�o de Legenda
@author 	Mick William da Silva
@since 		29/04/2016
@version 	P12.7
@Project    MAN00000462901_EF_004
*/
Static Function Legenda()
      
	Local aLegenda := {}

	aAdd(aLegenda, {"ENABLE" 		,"Pendente"})
	aAdd(aLegenda, {"BR_AMARELO"	,"Recebido parcialmente"})
	aAdd(aLegenda, {"DISABLE"		,"Recebido"})
	aAdd(aLegenda, {"BR_AZUL"		,"Em aprova��o"})
	aAdd(aLegenda, {"BR_CINZA"		,"Com res�duo eliminado"})
	aAdd(aLegenda, {"BR_LARANJA"	,"Em recebimento (Pr�-nota)"})
	aAdd(aLegenda, {"BR_PRETO"		,"Autoriza��o de Entrega"})
	aAdd(aLegenda, {"BR_BRANCO"	    ,"Pedido de Contrato (GCT)"})
	aAdd(aLegenda, {"PMSEDT2"	    ,"Aguardando aceite do fornecedor (Marketplace)"})

	BrwLegenda("Status Sol. Pagamento", "Legenda", aLegenda)

Return Nil

/*{Protheus.doc} MenuDEF
MenuDef para o Objeto
@author     Mick William da Silva
@since      29/04/2016
@version    P12.7
@Project    MAN00000462901_EF_004
*/
Static Function MenuDef()

	Local aRotina := {}

	ADD OPTION aRotina TITLE "Pesquisar"		ACTION "VIEWDEF.F0100401" 	OPERATION 1  ACCESS 0 // Pesquisar
	ADD OPTION aRotina TITLE "Visualizar"		ACTION "VIEWDEF.F0100401" 	OPERATION 2  ACCESS 0 // Visualizar
	ADD OPTION aRotina TITLE "Incluir"			ACTION "VIEWDEF.F0100401" 	OPERATION 3  ACCESS 0 // Incluir
	ADD OPTION aRotina TITLE "Alterar"			ACTION "VIEWDEF.F0100401" 	OPERATION 4  ACCESS 0 // Alterar
	ADD OPTION aRotina TITLE "Excluir"			ACTION "VIEWDEF.F0100401" 	OPERATION 5  ACCESS 0 // Excluir
//	ADD�OPTION�aRotina�TITLE�"Conhecimento"�	ACTION�"StaticCall(F0100401, BancoCon)" OPERATION�MODEL_OPERATION_UPDATE�ACCESS�0	// Conhecimento
//	ADD OPTION aRotina TITLE "Legenda" 		ACTION "StaticCall(F0100401, Legenda)" OPERATION 9 ACCESS 0	// Legenda
	
	//Adiciona Op��es do Banco de Conhecimento.
	If FindFunction("U_F0400104")
		aRotina := U_F0400104(aRotina)
	Endif
Return aRotina

/*{Protheus.doc} ModelDef
Defini��o do modelo de Dados
@author		Mick William da Silva
@since		29/04/2016
@version	P12.7
@Project	MAN00000462901_EF_004
*/
Static Function ModelDef()

	// Cria as estruturas a serem utilizadas no Modelo de Dados.
	Local oStruSC7		:= FwFormStruct(1, "SC7", {|x| AllTrim(x) + "|" $ cItemSC7})
	Local oStruSC7p		:= FWFormStruct(1, "SC7", {|x| AllTrim(x) + "|" $ cHdSC7})
	Local oStruSC7r		:= FWFormStruct(1, "SC7", {|x| AllTrim(x) + "|" $ cHdSC7})

	// Cria o objeto do Modelo de Dados
//	Local oModel 		:= MpFormModel():New("M0100401",,,{|oModel| SaveModel(oModel)})
//	Local oModel 		:= MpFormModel():New("M0100401",, {|oModel| U_F0400105('F0100401',SC7->C7_FILIAL, SC7->C7_NUM)},{|oModel| SaveModel(oModel)})
	Local oModel 		:= MpFormModel():New("M0100401",, {|oModel| fTudoOk(oModel)} ,{|oModel| SaveModel(oModel)})
	Local dInitEmi	:= dDataBase
	Local cVldForn	:= "IIf(!Empty(M->C7_FORNECE), ExistCpo('SA2', M->C7_FORNECE), .T.)"
	Local cVldLoja	:= "IIf(!Empty(M->C7_LOJA), ExistCpo('SA2', M->C7_FORNECE+M->C7_LOJA), .T.)"
	Local cVldCond	:= "IIf(!Empty(M->C7_COND), ExistCpo('SE4', M->C7_COND), .T.)"
	Local cVldXNat	:= "IIf(!Empty(M->C7_XNATURE), ExistCpo('SED', M->C7_XNATURE), .T.)"
	Local cVldXTipo	:= "IIf(!Empty(M->C7_XTIPO), ExistCpo('P02', M->C7_XTIPO), .T.)"
	Local cVldXEsp	:= "IIf(!Empty(M->C7_XESPECI), ExistCpo('SX5', '42'+M->C7_XESPECI), .T.)"
	Local cVldXDtEmi	:= "IIf(!Empty(M->C7_XDTEMI), M->C7_XDTEMI <= DDATABASE, .T.)"
	Local cVldCC		:= "IIf(!Empty(M->C7_CC), ExistCpo('CTT', M->C7_CC), .T.)"
	Local cVldProd	:= "IIf(!Empty(M->C7_PRODUTO), ExistCpo('SB1', M->C7_PRODUTO), .T.)"
	Local cVldUM		:= "IIf(!Empty(M->C7_UM), ExistCpo('SAH', M->C7_UM), .T.)"
	Local cVldLocal	:= "IIf(!Empty(M->C7_LOCAL), ExistCpo('NNR', M->C7_LOCAL), .T.)"
	Local aTrigCond	:=	FwStruTrigger ( 'C7_COND'    /*cDom*/, 'C7_CONDD'   /*cCDom*/, 'SE4->E4_DESCRI'	/*cRegra*/, .T. /*lSeek*/, 'SE4' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SE4') + M->C7_COND"    /*cChave*/, /*cCondic*/ )
	Local aTrigVenc:=	FwStruTrigger ( 'C7_XDTEMI'    /*cDom*/, 'C7_XDTVEN'   /*cCDom*/, 'U_F0100411(M->C7_XDTEMI,M->C7_COND)'	/*cRegra*/, .T. /*lSeek*/, 'SE4' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SE4') + M->C7_COND"    /*cChave*/, /*cCondic*/ )	
	Local aTrigNatu	:=	FwStruTrigger ( 'C7_XNATURE' /*cDom*/, 'C7_XNATD'   /*cCDom*/, 'SED->ED_DESCRIC'	/*cRegra*/, .T. /*lSeek*/, 'SED' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SED') + M->C7_XNATURE" /*cChave*/, /*cCondic*/ )
	Local aTrigTipo	:=	FwStruTrigger ( 'C7_XTIPO'   /*cDom*/, 'C7_XTIPOD'  /*cCDom*/, 'P02->P02_DESC'	/*cRegra*/, .T. /*lSeek*/, 'P02' /*cAlias*/,  01 /*nOrdem*/, "xFilial('P02') + M->C7_XTIPO"   /*cChave*/, /*cCondic*/ )
	Local aTrigTSer	:=	FwStruTrigger ( 'C7_XTIPO'   /*cDom*/, 'C7_XPREF'  /*cCDom*/, 'P02->P02_PREF'	/*cRegra*/, .T. /*lSeek*/, 'P02' /*cAlias*/,  01 /*nOrdem*/, "xFilial('P02') + M->C7_XTIPO"   /*cChave*/, /*cCondic*/ )
	Local aTrigProd1	:=	FwStruTrigger ( 'C7_PRODUTO' /*cDom*/, 'C7_UM'      /*cCDom*/, 'SB1->B1_UM'		/*cRegra*/, .T. /*lSeek*/, 'SB1' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SB1') + M->C7_PRODUTO" /*cChave*/, /*cCondic*/ )
	Local aTrigProd2	:=	FwStruTrigger ( 'C7_PRODUTO' /*cDom*/, 'C7_DESCRI'  /*cCDom*/, 'SB1->B1_DESC'	/*cRegra*/, .T. /*lSeek*/, 'SB1' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SB1') + M->C7_PRODUTO" /*cChave*/, /*cCondic*/ )
	Local aTrigProd3	:=	FwStruTrigger ( 'C7_PRODUTO' /*cDom*/, 'C7_LOCAL'   /*cCDom*/, 'SB1->B1_LOCPAD'	/*cRegra*/, .T. /*lSeek*/, 'SB1' /*cAlias*/,  01 /*nOrdem*/, "xFilial('SB1') + M->C7_PRODUTO" /*cChave*/, /*cCondic*/ )
	Local nOper		:= oModel:GetOperation()
	
	// Adiciona os campos no Cabe�alho.
	oStruSC7p:AddField("C7_NUM", "C7_NUM", "C7_NUM", "C", 6, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| GetSX8Num("SC7", "C7_NUM")}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_EMISSAO", "C7_EMISSAO", "C7_EMISSAO", "D", 8, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| dDataBase}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_FORNECE", "C7_FORNECE", "C7_FORNECE", "C", 6, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldForn), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_LOJA", "C7_LOJA", "C7_LOJA", "C", 2, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldLoja), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_COND", "C7_COND", "C7_COND", "C", 3, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldCond), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_CONDD", "C7_CONDD", "C7_CONDD", "C", 40, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| GetDescrip(oModel, cFilAnt, "C7_COND", "SE4", "E4_DESCRI")}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7p:AddField("C7_XNATURE", "C7_XNATURE", "C7_XNATURE", "C", 10, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldXNat), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XNATD", "C7_XNATD", "C7_XNATD", "C", 30, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| GetDescrip(oModel, PadR(Left(cFilAnt, 4), 8), "C7_XNATURE", "SED", "ED_DESCRIC")}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7p:AddField("C7_XTIPO", "C7_XTIPO", "C7_XTIPO", "C", 2, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldXTipo), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XTIPOD", "C7_XTIPOD", "C7_XTIPOD", "C", 220, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| GetDescrip(oModel, cFilAnt, "C7_XTIPO", "P02", "P02_DESC")}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7p:AddField("C7_XPREF", "C7_XPREF", "C7_XPREF", "C", 3, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| GetDescrip(oModel, cFilAnt, "C7_XTIPO", "P02", "P02_PREF")}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7p:AddField("C7_XESPECI", "C7_XESPECI", "C7_XESPECI", "C", 6, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldXEsp), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XDOC", "C7_XDOC", "C7_XDOC", "C", 9, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XSERIE", "C7_XSERIE", "C7_XSERIE", "C", 3, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XDTEMI", "C7_XDTEMI", "C7_XDTEMI", "D", 8, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldXDtEmi), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_XDTVEN", "C7_XDTVEN", "C7_XDTVEN", "D", 8, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7p:AddField("C7_CC", "C7_CC", "C7_CC", "C", 11, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldCC), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	
	// Adiciona gatilhos ao cabe�alho.
	oStruSC7p:AddTrigger( aTrigCond[1] /*cIdField*/, aTrigCond[2] /*cTargetIdField*/, aTrigCond[3] /*bPre*/, aTrigCond[4] /*bSetValue*/ )
	oStruSC7p:AddTrigger( aTrigVenc[1] /*cIdField*/, aTrigVenc[2] /*cTargetIdField*/, aTrigVenc[3] /*bPre*/, aTrigVenc[4] /*bSetValue*/ )
	oStruSC7p:AddTrigger( aTrigNatu[1] /*cIdField*/, aTrigNatu[2] /*cTargetIdField*/, aTrigNatu[3] /*bPre*/, aTrigNatu[4] /*bSetValue*/ )
	oStruSC7p:AddTrigger( aTrigTipo[1] /*cIdField*/, aTrigTipo[2] /*cTargetIdField*/, aTrigTipo[3] /*bPre*/, aTrigTipo[4] /*bSetValue*/ )
	oStruSC7p:AddTrigger( aTrigTSer[1] /*cIdField*/, aTrigTSer[2] /*cTargetIdField*/, aTrigTSer[3] /*bPre*/, aTrigTSer[4] /*bSetValue*/ )
	
	// Adiciona os campos ao grid.
	oStruSC7:AddField("C7_ITEM", "C7_ITEM", "C7_ITEM", "C", 4, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_PRODUTO", "C7_PRODUTO", "C7_PRODUTO", "C", 15, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldProd), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_DESCRI", "C7_DESCRI", "C7_DESCRI", "C", 30, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7:AddField("C7_UM", "C7_UM", "C7_UM", "C", 2, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldUM), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_QUANT", "C7_QUANT", "C7_QUANT", "N", 12, 2, /*bValid*/{|| SumProd(oModel)}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_PRECO", "C7_PRECO", "C7_PRECO", "N", 14, 2, /*bValid*/{|| SumProd(oModel)}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_TOTAL", "C7_TOTAL", "C7_TOTAL", "N", 14, 2, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| TotValue(oModel)}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_VLDESC", "", "C7_VLDESC", "N", 14, 2, /*bValid*/{|| IIf(!lFunSum, SumDesc(oModel, "Valid"), .T.)}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_DESPESA", "", "C7_DESPESA", "N", 14, 2, /*bValid*/{|| IIf(!lFunSum, SumDesp(oModel, "Valid"), .T.)}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_LOCAL", "C7_LOCAL", "C7_LOCAL", "C", 2, /*nDecimal*/, /*bValid*/FWBuildFeature(STRUCT_FEATURE_VALID, cVldLocal), /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)
	oStruSC7:AddField("C7_OBS", "C7_OBS", "C7_OBS", "C", 30, /*nDecimal*/, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.F., /*cValid*/)

	// Adiciona gatilhos ao grid.
	oStruSC7:AddTrigger( aTrigProd1[1] /*cIdField*/, aTrigProd1[2] /*cTargetIdField*/, aTrigProd1[3] /*bPre*/, aTrigProd1[4] /*bSetValue*/ )
	oStruSC7:AddTrigger( aTrigProd2[1] /*cIdField*/, aTrigProd2[2] /*cTargetIdField*/, aTrigProd2[3] /*bPre*/, aTrigProd2[4] /*bSetValue*/ )
	oStruSC7:AddTrigger( aTrigProd3[1] /*cIdField*/, aTrigProd3[2] /*cTargetIdField*/, aTrigProd3[3] /*bPre*/, aTrigProd3[4] /*bSetValue*/ )
	
	// Adiciona os campos do Rodap�.
	oStruSC7r:AddField("C7_TOTMERC", "C7_TOTMERC", "C7_TOTMERC", "N", 14, 2, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| SumMerc(oModel)}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7r:AddField("C7_TOTDESC", "C7_TOTDESC", "C7_TOTDESC", "N", 14, 2, /*bValid*/{|| PropValue(oModel, "C7_TOTDESC", "C7_VLDESC", "Valid")}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| IIf(!lFunSum, SumDesc(oModel, "Init"), )}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7r:AddField("C7_TOTDESP", "C7_TOTDESP", "C7_TOTDESP", "N", 14, 2, /*bValid*/{|| PropValue(oModel, "C7_TOTDESP", "C7_DESPESA", "Valid")}, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| IIf(!lFunSum, SumDesp(oModel, "Init"), )}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)
	oStruSC7r:AddField("C7_TOTSOL", "C7_TOTSOL", "C7_TOTSOL", "N", 14, 2, /*bValid*/, /*bWhen*/, /*aValues*/, /*lObrigat*/, /*bInit*/{|| TotSolPagt(oModel)}, /*lKey*/.F., /*lNoUpd*/.F., /*lVirtual*/.T., /*cValid*/)	
	
	// Adiciona ao modelo um componente de formul�rio
	oModel:AddFields("MODEL_SC7p", /*cOwner*/, oStruSC7p)
	
	// Adiciona ao modelo uma grid
	oModel:AddGrid("MODEL_SC7", "MODEL_SC7p", oStruSC7)
	
	oModel:GetModel("MODEL_SC7"):SetOptional(.T.)
	oModel:GetModel("MODEL_SC7"):SetUniqueLine({"C7_ITEM", "C7_PRODUTO"})
	oModel:SetRelation("MODEL_SC7", { {"C7_FILIAL", "xFilial('SC7')"}, {"C7_NUM", "C7_NUM"} }, SC7->(IndexKey(1)))
	
	oModel:GetModel("MODEL_SC7p"):SetPrimaryKey({"C7_FILIAL", "C7_NUM", "C7_ITEM"})
	
	// Adiciona o campo "Total Mercadoria" no Rodap�.
	oModel:AddCalc("MODEL_TOT", "MODEL_SC7p", "MODEL_SC7", "C7_VLDESC", "TOTDESC", "SUM", , , "")
	oModel:AddCalc("MODEL_TOT", "MODEL_SC7p", "MODEL_SC7", "C7_DESPESA", "TOTDESP", "SUM", , , "")
	
	// Adiciona a estrutura do rodap� ao modelo
	oModel:AddFields("MODEL_SC7r", "MODEL_SC7p", oStruSC7r)
	oModel:SetVldActivate({|oModel| U_F0100410(oModel)})
//	oModel:SetActivate({|oModel| FWFLDPUT("C7_FORNECE",'000001',,oModel)})
Return oModel

/*{Protheus.doc} F0100410
Valida��o na altera��o/exclus�o da solicita��o.
@Author Nairan Alves Silva
@Since 17/12/2016
@Version P12.7
@Project	MAN00000462901_EF_004
@Return lREt = True or False
*/
User Function F0100410(oModel)
Local nOper		:= oModel:GetOperation()
Local aAreaP02	:= P02->(GetArea())
Local aAreaSY1	:= SY1->(GetArea())
Local cComprador := ''
Local lRet := .T.
Local cUsrBackup	:= ""
Local lSolic   := .T.
Local lRestPed := If(SuperGetMv("MV_RESTPED")=="S", .T. , .F. )
Local aGrupo   := {}
Local cGrupo   := ""
Local nLoop    := 0

If nOper == 4 .Or. nOper == 5
	cComprador := Posicione("P02", 1, xFilial("P02") + SC7->C7_XTIPO, "P02_COMPRA")
	If Empty(cComprador)
		Help('',1,'F0100401',,"N�o foi encontrado comprador vinculado a esse Tipo de Pagamento.",1,0,,,,,,{"Atualize a tabela Tipos de Despesas."})	
		lRet := .F.
	Else
		cUserTemp := Posicione("SY1", 1, xFilial("SY1") + cComprador, "Y1_USER")  
		If Empty(cUserTemp)
			Help('',1,'F0100401',,"Inconsist�ncia no cadastro de compradores.",1,0,,,,,,{"Verifique o cadastro de compradores."})	
			lRet := .F.
		Else	
			aGrupo := UsrGrComp(cUserTemp)
			If aScan(aGrupo,"*") != 0
			    lSolic := .F.
			Else
			    For nLoop := 1 to Len(aGrupo)
			        cGrupo += aGrupo[nLoop]
			    Next nLoop
			EndIf
			If ( lSolic .And. lRestPed .And. !(SC7->C7_GRUPCOM $ cGrupo) .And. SC7->C7_USER != cUserTemp .And. !Empty(SC7->C7_USER) )
			    If Empty(SC7->C7_GRUPCOM)
			        Help("  ",1,"A120RSPED",,UsrRetName(SC7->C7_USER),4,11)
			    Else
			        Help("  ",1,"USUNAOAUT",,SC7->C7_GRUPCOM,3,25)
			    EndIf
			    lRet := .F.
			EndIf
		EndIf
	EndIf
Endif

Return lRet

/*{Protheus.doc} ViewDef
Funcao generica MVC do View
@Author Mick William da Silva
@Since 29/04/2016
@Version P12.7
@Project	MAN00000462901_EF_004
@Return oView - Objeto da View MVC
*/
Static Function ViewDef()

	Local oModel	:= FWLoadModel("F0100401")
	Local oView 	:= FWFormView():New()
	Local oStruSC7  := Nil
	Local oStruSC7p	:= Nil
	Local oStruSC7r := Nil

	oView:SetModel(oModel)

	oStruSC7p := FwFormStruct(2, "SC7", {|x| AllTrim(x) + "|" $ cHdSC7}) 	// Campos do Cabe�alho
	oStruSC7 := FwFormStruct(2, "SC7", {|x| AllTrim(x) + "|" $ cItemSC7}) 	// Campos dos Itens
	oStruSC7r := FwFormStruct(2, "SC7", {|x| AllTrim(x) + "|" $ cHdSC7}) 	// Campos do Rodap�

	oView:AddField("VIEW_SC7p", oStruSC7p, "MODEL_SC7p")
	oView:AddGrid ("VIEW_SC7" , oStruSC7, "MODEL_SC7")
	oView:AddField("VIEW_TOT", oStruSC7r, "MODEL_SC7r")
	
	oStruSC7p:AddField("C7_NUM", "01", "Num. Solic. Pagto", "Num. Solic. Pagto", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_EMISSAO", "02", "Dt Emiss�o", "Dt Emiss�o", /*aHelp*/, "D", /*cPicture*/, /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_FORNECE", "03", "Cod. Fornecedor", "Cod. Fornecedor", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"FOR", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_LOJA", "04", "Loja Fornecedor", "Loja Fornecedor", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_COND", "05", "Cond. Pagto", "Cond. Pagto", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"FSSE4", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_CONDD", "06", "Desc. Cond. Pagto", "Desc. Cond. Pagto", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.T., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XNATURE", "07", "Natureza Financeira", "Natureza Financeira", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"FSSED", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XNATD", "08", "Desc. Natureza", "Desc. Natureza", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.T., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XTIPO", "09", "Tipo do Pagamento", "Tipo do Pagamento", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"FSWP02", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XTIPOD", "10", "Desc. Tipo do Pagamento", "Desc. Tipo do Pagamento", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.T., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )	
	oStruSC7p:AddField("C7_XPREF", "11", "Prefixo", "Prefixo", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.T., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XESPECI", "12", "Especie do Documento", "Especie do Documento", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"42", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XDOC", "13", "Num. da Nota Fiscal", "Num. da Nota Fiscal", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XSERIE", "14", "Serie da Nota Fiscal", "Serie da Nota Fiscal", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XDTEMI", "15", "Dt Emissao Nota Fiscal", "Dt Emissao Nota Fiscal", /*aHelp*/, "D", '', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_XDTVEN", "16", "Data 1� Vencimento", "Data 1� Vencimento", /*aHelp*/, "D", '', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7p:AddField("C7_CC", "17", "Centro de Custo", "Centro de Custo", /*aHelp*/, "C", '', /*bPictVar*/, /*cLookUp*/"CTT", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	
	oStruSC7:AddField("C7_ITEM", "01", "Item", "Item", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_PRODUTO", "02", "Codigo do Produto", "Codigo do Produto", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"FSSB1", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_DESCRI", "03", "Descricao do Produto", "Descricao do Produto", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.T., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_UM", "04", "Unidade de medida", "Unidade de medida", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"SAH", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_QUANT", "05", "Quantidade", "Quantidade", /*aHelp*/, "N", "@E 999999999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_PRECO", "06", "Preco", "Preco", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_TOTAL", "07", "Total", "Total", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_VLDESC", "08", "Vl. Desconto", "Vl. Desconto", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_DESPESA", "09", "Vlr.Despesas", "Vlr.Despesas", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_LOCAL", "10", "Armazem", "Armazem", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/"NNR", /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7:AddField("C7_OBS", "11", "Observacoes", "Observacoes", /*aHelp*/, "C", '@!', /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	
	oStruSC7r:AddField("C7_TOTMERC", "01", "Total de Mercadoria", "Total de Mercadoria", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7r:AddField("C7_TOTDESC", "02", "Desconto Total", "Desconto Total", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7r:AddField("C7_TOTDESP", "03", "Juros Total", "Juros Total", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.T., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	oStruSC7r:AddField("C7_TOTSOL", "04", "Total da Solicita��o de Pagamento", "Total da Solicita��o de Pagamento", /*aHelp*/, "N", "@E 99,999,999,999.99", /*bPictVar*/, /*cLookUp*/, /*lCanChange*/.F., /*cFolder*/"01", /*cGroup*/, /*aComboValues*/, /*nMaxLenCombo*/, /*cIniBrow*/, /*lVirtual*/.F., /*PictVar*/, /*lInsertLine*/, /*nWidth*/ )
	
	oView:CreateHorizontalBox("SUPERIOR", 40)
	oView:CreateHorizontalBox("GRID", 50)
	oView:CreateHorizontalBox("RODAPE", 10)

	oView:SetOwnerView("VIEW_SC7p", "SUPERIOR")
	oView:SetOwnerView("VIEW_SC7", "GRID")
	oView:SetOwnerView("VIEW_TOT", "RODAPE")
	
	oView:AddIncrementField("VIEW_SC7", "C7_ITEM")
	
Return oView


/*/{Protheus.doc} SaveModel
Grava��o do modelo de dados.
	
@author 	alexandre.arume
@since 		24/08/2016
@version 	1.0
@param 		oModel, Modelo de dados.
@Project	MAN00000462901_EF_004

@return ${lRet}, Status da opera��o.

/*/
Static Function SaveModel(oModel)

	Local nOperation	:= oModel:GetOperation()
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local oMdlMain	:= oModel:GetModel("MODEL_SC7p")
	Local oMdlFooter	:= oModel:GetModel("MODEL_SC7r")
	Local cCond		:= GetMv("FS_CONDPAG",,"001")
	Local aAreas     	:= {SC7->(GetArea()), P02->(GetArea()), GetArea()}
	Local aCabec		:= {}
	Local aItens		:= {}
	Local aItem		:= {}
	Local nI			:= 0
	Local lRet			:= .T.
	Local cComprador	:= ''
	Local cUserIdBk	:= __cUserId
	Local cUserTemp	:= ''
	Private lMsErroAuto	:= .F.
	
	If nOperation <> MODEL_OPERATION_INSERT
		aAdd(aCabec, {"C7_NUM", oMdlMain:getValue("C7_NUM"), Nil})
	EndIf
	
	cComprador := Posicione("P02", 1, xFilial("P02") + oMdlMain:GetValue("C7_XTIPO"), "P02_COMPRA")
	
	If Empty(cComprador)
		Help('',1,'F0100401',,"N�o foi encontrado comprador vinculado a esse Tipo de Pagamento.",1,0,,,,,,{"Atualize a tabela Tipos de Despesas."})	
		aEval(aAreas, {|x| RestArea(x)})
		Return .F.
	Else
 		cUserTemp := Posicione("SY1", 1, xFilial("SY1") + cComprador, "Y1_USER")  
		If Empty(cUserTemp)
			Help('',1,'F0100401',,"Inconsist�ncia no cadastro de compradores.",1,0,,,,,,{"Verifique o cadastro de compradores."})	
			Return .F.
		Else	
			aAdd(aCabec, {"C7_COMPRA" 	, cComprador							, Nil})
		EndIf
	EndIf
	
	aAdd(aCabec, {"C7_EMISSAO"	, oMdlMain:getValue("C7_EMISSAO"	), Nil})
	aAdd(aCabec, {"C7_FORNECE"	, oMdlMain:getValue("C7_FORNECE"	), Nil})
	aAdd(aCabec, {"C7_LOJA"   	, oMdlMain:getValue("C7_LOJA"		), Nil})
	aAdd(aCabec, {"C7_COND"   	, oMdlMain:getValue("C7_COND"		), Nil})
	aAdd(aCabec, {"C7_CONTATO"	, ""								, Nil})
	aAdd(aCabec, {"C7_FILENT" 	, cFilAnt							, Nil})
	aAdd(aCabec, {"C7_MOEDA"  	, 1									, Nil})
	aAdd(aCabec, {"C7_TXMOEDA"	, 0									, Nil})
	aAdd(aCabec, {"C7_DESPESA"	, oMdlFooter:getValue("C7_TOTDESP")	, Nil})
	aAdd(aCabec, {"C7_SOLICIT"	, AllTrim(UsrRetName(__CUSERID))	, Nil})
	aAdd(aCabec, {"C7_APROV" 	, Posicione("P02", 1, xFilial("P02") + oMdlMain:GetValue("C7_XTIPO"), "P02_COMPRA"), Nil})
	
	For nI := 1 To oMdlDetail:Length()
	
		oMdlDetail:GoLine(nI)
		
		If oMdlDetail:IsDeleted() .And. nOperation == MODEL_OPERATION_INSERT
			Loop
		Endif
		aItem := {}
		
		aAdd(aItem, {"C7_ITEM"		, oMdlDetail:getValue("C7_ITEM"		), Nil})
		aAdd(aItem, {"C7_PRODUTO" 	, oMdlDetail:getValue("C7_PRODUTO"	), Nil})
		aAdd(aItem, {"C7_UM" 		, oMdlDetail:getValue("C7_UM"		), Nil})
		aAdd(aItem, {"C7_QUANT" 	, oMdlDetail:getValue("C7_QUANT"	), Nil})
		aAdd(aItem, {"C7_PRECO" 	, oMdlDetail:getValue("C7_PRECO"	), Nil})
		aAdd(aItem, {"C7_TOTAL" 	, oMdlDetail:getValue("C7_TOTAL"	), Nil})
		aAdd(aItem, {"C7_VLDESC" 	, oMdlDetail:getValue("C7_VLDESC"	), Nil})
		aAdd(aItem, {"C7_LOCAL" 	, oMdlDetail:getValue("C7_LOCAL"	), Nil})
		aAdd(aItem, {"C7_OBS" 		, oMdlDetail:getValue("C7_OBS"		), Nil})
		aAdd(aItem, {"C7_XSOLPAG"	, "1"								, Nil})
		aAdd(aItem, {"C7_XUSR"		, __CUSERID                          , Nil})
		aAdd(aItem, {"C7_XNATURE"	, oMdlMain:getValue("C7_XNATURE"	), Nil})
		aAdd(aItem, {"C7_XTIPO"		, oMdlMain:getValue("C7_XTIPO"		), Nil})
		aAdd(aItem, {"C7_XPREF"   	, oMdlMain:getValue("C7_XPREF"		), Nil})		
		aAdd(aItem, {"C7_XESPECI"	, oMdlMain:getValue("C7_XESPECI"	), Nil})
		aAdd(aItem, {"C7_XDOC"		, oMdlMain:getValue("C7_XDOC"		), Nil})
		aAdd(aItem, {"C7_XSERIE"	, oMdlMain:getValue("C7_XSERIE"		), Nil})
		aAdd(aItem, {"C7_XDTEMI"	, oMdlMain:getValue("C7_XDTEMI"		), Nil})
		aAdd(aItem, {"C7_XDTVEN"	, oMdlMain:getValue("C7_XDTVEN"		), Nil})
		aAdd(aItem, {"C7_CC"		, oMdlMain:getValue("C7_CC"			), Nil})
		aAdd(aItem, {"C7_REC_WT"	, GetRecno(oModel)					, Nil})
		
		If oMdlDetail:IsDeleted()
			aAdd(aItem, {"AUTDELETA", "S", Nil})	// Linha deletada.
		EndIf
						
		aAdd(aItens, aItem)
			
	Next nI
 	
	If oModel:VldData()
 	
		Begin Transaction
			__cUserId := cUserTemp
			//lAutoErrNoFile := .T. 
			// Rotina autom�tica do Pedido de Compra.
			MsgRun("Aguarde...", , {|| MATA120(1, aCabec, aItens, nOperation)})
			//MsgRun("Aguarde...", , {|| MSExecAuto({|v,x,y,z| MATA120(v,x,y,z)},1,aCabec,aItens,nOperation)})
			IF lMsErroAuto
			
				MostraErro()
				lRet := .F.
				DisarmTransaction()
				oModel:SetErrorMessage(,,�oModel:GetId(),,�"HELP",�"Erro na rotina autom�tica",�"Verifique os dados informados.",,�)
			EndIf
			__cUserId := cUserIdBk

		End Transaction
			
	Else
		JurShowErro(oModel:GetModel():GetErrormessage())
	EndIf
	
	aEval(aAreas, {|x| RestArea(x)})

Return lRet

/*{Protheus.doc} F0100408
Acessa o banco de conhecimento

@author 	Mick William da Silva
@since 		29/04/2016
@version 	P12.7
@Project	MAN00000462901_EF_004
*/
Static Function BancoCon()
	
	MsgAlert("Em desenvolvimento.", "Aviso")

Return

/*/{Protheus.doc} GetRecno
Retorna o valor do Recno do registro.
	
@author 	alexandre.arume
@since 		24/08/2016
@version 	1.0
@Project	MAN00000462901_EF_004
		
@param oModel, Modelo de dados.

@return ${nRet}, Valor do Recno.

/*/
Static Function GetRecno(oModel)
	
	Local nOperation	:= oModel:GetOperation()
	Local oMdlMain	:= oModel:GetModel("MODEL_SC7p")
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local cNum 		:= oMdlMain:GetValue("C7_NUM")
	Local cItem 		:= oMdlDetail:GetValue("C7_ITEM")
	Local aSC7Area	:= {}
	Local nRet			:= 0
	
	If nOperation <> MODEL_OPERATION_INSERT
		
		aSC7Area := SC7->(GetArea())
		
		SC7->(dbSetOrder(1))
		If SC7->(dbSeek(xFilial("SC7") + cNum + cItem))
			nRet := SC7->(RECNO())
		EndIf
		
		RestArea(aSC7Area)
		
	EndIf

Return nRet

/*/{Protheus.doc} GetDescrip
Retorna a descri��o do campo.
	
@author 	alexandre.arume
@since 		22/08/2016
@version 	1.0
		
@param 		oModel, (Objeto do modelo)
@param 		cFil, (Codigo da filial)
@param 		cSC7Fld, (Campo da tabela SC7)
@param 		cTable, (Nome da tabela a ser pesquisada)
@param 		cField, (Nome do campo a ser pesquisado)

@Project	MAN00000462901_EF_004

@return 	cRet, Descri��o.

/*/
Static Function GetDescrip(oModel, cFil, cSC7Fld, cTable, cField)
	
	Local oMdlMain:= oModel:GetModel("MODEL_SC7p")
	Local nOper 	:= oModel:GetOperation()
	Local cRet 	:= ""
	
	If nOper <> 3 .AND. ! Empty(oMdlMain:GetValue(cSC7Fld))
		cRet := Posicione(cTable, 1, cFil + oMdlMain:GetValue(cSC7Fld), cField)
	EndIf
	
Return cRet

/*/{Protheus.doc} SumProd
Multiplica��o da quantidade pelo pre�o de cada item.
	
@author 	alexandre.arume
@since 		19/08/2016
@version 	1.0		
@Project	MAN00000462901_EF_004

@return ${.T.}

/*/
Static Function SumProd(oModel)
	
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local nI			:= 0
	Local nTot			:= 0
	
	// Calcula o valor total do produto.
	nTot := oMdlDetail:GetValue("C7_QUANT") * oMdlDetail:GetValue("C7_PRECO")
	
	// Atualiza o valor do campo Total.
//	oMdlDetail:SetValue("C7_TOTAL", nTot)
	If oMdlDetail:GetOperation() <> 5
		oMdlDetail:LoadValue("C7_TOTAL", nTot)
	EndIf	
	// Atualiza o valor do campo Total de Mercadoria.
	SumMerc(oModel)
	
Return .T.

/*/{Protheus.doc} TotValue
Valor total do produto.
	
@author 	alexandre.arume
@since 		22/08/2016
@version 	1.0
		
@param 		oModel, objeto, (Objeto do modelo)
@Project	MAN00000462901_EF_004

@return ${nRet}, Valor total.

/*/
Static Function TotValue(oModel)

	Local oMdlDetail 	:= oModel:GetModel("MODEL_SC7")
	Local nRet			:= 0
	
	If oModel:GetOperation() <> 3
		If ! Empty(oMdlDetail:GetValue("C7_QUANT")) .AND. ! Empty(oMdlDetail:GetValue("C7_PRECO"))
			nRet := oMdlDetail:GetValue("C7_QUANT") * oMdlDetail:GetValue("C7_PRECO")
		EndIf
	EndIf
	
Return nRet

/*/{Protheus.doc} SumDesc
Somat�ria dos descontos de cada item.
	
@author 	alexandre.arume
@since 		19/08/2016
@version 	1.0
@param 		oModel, objeto, (Modelo do objeto)
@Project	MAN00000462901_EF_004

@return ${.T}
/*/
Static Function SumDesc(oModel, cBlock)
	
	Local oMdlMain 	:= oModel:GetModel("MODEL_SC7p")
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local oMdlFooter 	:= oModel:GetModel("MODEL_SC7r")
	Local oMdlTot 	:= oModel:GetModel("MODEL_TOT")
	Local nTot			:= oMdlTot:GetValue("TOTDESC")
	
	// Seta a variavel com True, para informar que esta vindo da fun��o SumDesc.
	// Essa verifica��o ser� utilizada na fun��o PropValue.
	// N�o ser�o atualizados os valores dos descontos.
	lFunSum := .T.
	
	// Atualiza o valor do Total de Desconto com o valor do campo TOTDESC, que faz a somat�ria dos valores de descontos dos produto.
	//oMdlFooter:SetValue("C7_TOTDESC", nTot)
	If oMdlFooter:GetOperation() <> 5
		oMdlFooter:LoadValue("C7_TOTDESC", nTot)
	EndIf
	// Retorna ao valor original.
	lFunSum := .F.
	
	// Atualiza o campo do valor do Total da Solicita��o de Pagamento.
	TotSolPagt(oModel)
	
	// Caso esteja vindo do inicializador padr�o, ou seja, na abertura da tela.
	If cBlock == "Init"
		PropValue(oModel, "C7_TOTDESC", "C7_VLDESC", cBlock)
	EndIf
	
// Se a fun��o for chamada em valida��o retorna True, sen�o retorna o valor total de desconto.
Return IIf(cBlock == "Valid", .T., nTot)

/*/{Protheus.doc} SumMerc
Somat�ria dos valores totais de cada item.
	
@author 	alexandre.arume
@since 		19/08/2016
@version 	1.0
@param 		oModel, objeto, (Modelo do objeto)
@Project	MAN00000462901_EF_004

@return ${nTot}, Valor total das Mercadoria.
/*/
Static Function SumMerc(oModel)
	
	Local oMdlDetail 	:= oModel:GetModel("MODEL_SC7")
	Local oMdlFooter 	:= oModel:GetModel("MODEL_SC7r")
	Local oView		:= FWViewActive()
	Local nI			:= 0
	Local nTot			:= 0
	Local�aSaveLines	:=�{}
	
	aSaveLines	:= FWSaveRows()
	// Percorre os itens do Grid.
	For nI := 1  To oMdlDetail:Length()
	
		oMdlDetail:GoLine(nI)
	
		nTot += oMdlDetail:GetValue("C7_TOTAL")
		
	Next
	FWRestRows(aSaveLines)
	
	// Atualiza o valor do campo Total de Mercadoria.
	//oMdlFooter:SetValue("C7_TOTMERC", nTot)
	If oMdlFooter:GetOperation() <> 5
		oMdlFooter:LoadValue("C7_TOTMERC", nTot)
	EndIf	
	// Atualiza o campo do valor do Total da Solicita��o de Pagamento.
	TotSolPagt(oModel)
		
Return nTot

/*/{Protheus.doc} SumDesp
Somat�ria dos valores das despesas de cada item.
	
@author 	alexandre.arume
@since 		19/08/2016
@version 	1.0	
@param 		oModel, objeto, (Modelo do objeto)
@Project	MAN00000462901_EF_004

@return ${nTot}, Valor total das despesas.
/*/
Static Function SumDesp(oModel, cBlock)
	
	Local oMdlMain 	:= oModel:GetModel("MODEL_SC7p")
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local oMdlFooter 	:= oModel:GetModel("MODEL_SC7r")
	Local oMdlTot 	:= oModel:GetModel("MODEL_TOT")
	Local nTot			:= oMdlTot:GetValue("TOTDESP")
	
	// Seta a variavel com True, para informar que esta vindo da fun��o SumDesp.
	// Essa verifica��o ser� utilizada na fun��o PropValue.
	// N�o ser�o atualizados os valores dos descontos.
	lFunSum := .T.
	
	// Atualiza o valor do Total de Despesa com o valor do campo TOTDESP, que faz a somat�ria dos valores de descontos dos produto.
	//oMdlFooter:SetValue("C7_TOTDESP", nTot)
	If oMdlFooter:GetOperation() <> 5
		oMdlFooter:LoadValue("C7_TOTDESP", nTot)
	EndIf
	// Retorna ao valor original.
	lFunSum := .F.
	
	// Atualiza o campo do valor do Total da Solicita��o de Pagamento.
	TotSolPagt(oModel)
	
	// Caso esteja vindo do inicializador padr�o, ou seja, na abertura da tela.
	If cBlock == "Init"
		PropValue(oModel, "C7_TOTDESP", "C7_DESPESA", cBlock)
	EndIf
	
// Se a fun��o for chamada em valida��o retorna True, sen�o retorna o valor total de desconto.	
Return IIf(cBlock == "Valid", .T., nTot)

/*/{Protheus.doc} PropValue
C�lculo do valor proporcional por item.
	
@author alexandre.arume
@since 22/08/2016
@version 1.0
		
@param oModel, (Descri��o do par�metro)
@param cFooTot, (Campo total do footer)
@param cFld, (Campo proporcional)

@return ${return}, ${return_description}

/*/
Static Function PropValue(oModel, cFooTot, cFld, cBlock)
	
	Local oMdlMain 	:= oModel:GetModel("MODEL_SC7p")
	Local oMdlDetail	:= oModel:GetModel("MODEL_SC7")
	Local oMdlFooter 	:= oModel:GetModel("MODEL_SC7r")
	Local oView		:= FWViewActive()
	Local nI			:= 0
	Local nTotMerc	:= IIf(oMdlFooter:GetValue("C7_TOTMERC") <> Nil, oMdlFooter:GetValue("C7_TOTMERC"), 0)
	Local nTot			:= IIf(oMdlFooter:GetValue(cFooTot) <> Nil, oMdlFooter:GetValue(cFooTot), 0)
	Local nTotItem	:= 0
	Local nValItem	:= 0
	
	// Caso n�o esteja vindo da fun��o SumDesc ou SumDesp.
	If !lFunSum
		
		// Percorre os itens do Grid.
		For nI := 1 To oMdlDetail:Length()
		
			oMdlDetail:GoLine(nI)
			
			// Pega o valor do campo total do produto.
			nTotItem := IIf(oMdlDetail:GetValue("C7_TOTAL") <> Nil, oMdlDetail:GetValue("C7_TOTAL"), 0)
			
			If nTotMerc = 0 .OR. nTot = 0 .OR. nTotItem = 0
				
				nValItem := 0
				
			Else
		
				nValItem := nTotItem * 100 / nTotMerc	// Porcentagem equivalente
				nValItem := ((nValItem / 100) * nTot)	// Valor proporcional
			
			EndIf
			
			// Atualiza o valor do campo.
			//oMdlDetail:SetValue(cFld, nValItem)
			If oMdlDetail:GetOperation() <> 5
				oMdlDetail:LoadValue(cFld, nValItem)
			EndIf			
		Next
		
		// Se for edi��o do campo Desconto Total ou Juros Total.
		If cBlock == "Valid"
		
			oMdlDetail:GoLine(1)
			oView:Refresh()
			
		EndIf
		
	EndIf
	
Return .T.


/*/{Protheus.doc} TotSolPagt
C�lculo da Solicita��o de Pagamento.
	
@author 	alexandre.arume
@since 		19/08/2016
@version 	1.0
@param 		oModel, objeto, (Modelo do objeto)
@Project	MAN00000462901_EF_004

@return ${nTot}, Valor total da Solicita��o de Pagamento
/*/
Static Function TotSolPagt(oModel)
	
	Local oMdlFooter 	:= oModel:GetModel("MODEL_SC7r")
	Local nTotMerc	:= oMdlFooter:GetValue("C7_TOTMERC")
	Local nTotDesp	:= oMdlFooter:GetValue("C7_TOTDESP")
	Local nTotDesc	:= oMdlFooter:GetValue("C7_TOTDESC")
	Local nTot			:= 0
	
	nTot := IIf(! Empty(nTotMerc), nTotMerc, 0) + IIf(! Empty(nTotDesp), nTotDesp, 0) - IIf(! Empty(nTotDesc), nTotDesc, 0)
	
	If oMdlFooter:GetValue("C7_TOTSOL") <> Nil
//		oMdlFooter:SetValue("C7_TOTSOL", nTot)
		If oMdlFooter:GetOperation() <> 5
			oMdlFooter:LoadValue("C7_TOTSOL", nTot)
		EndIf
	EndIf
	
Return nTot

/*/{Protheus.doc} fTrigger
Restaura informa��es do Cadastro de Produtos
@author 	Totvs
@since 		19/08/2016
@version 	1.0
@param 		cProduto
@Project	MAN00000462901_EF_004
@return	l�gico
/*/

User Function fTrigger(cProduto)
	M->C7_UM		:= Posicione('SB1',01,xFilial('SB1') + cProduto,'B1_UM')
	M->C7_LOCAL	:= Posicione('SB1',01,xFilial('SB1') + cProduto,'B1_LOCPAD')
	M->C7_DESCRI	:= Posicione('SB1',01,xFilial('SB1') + cProduto,'B1_DESC')
Return(.T.)

/*/{Protheus.doc} fTudoOk
Chamada de fun��es de valida��es na confirma��o do cadastro.
@author 	Paulo Kr�ger
@since 		13/12/2016
@version 	1.0
@param 		oModelo
@Project	MAN00000462901_EF_004
@return	l�gico
/*/

Static Function fTudoOk(oModelo)

Local nOpca := 0
Local lRet  := .T.
Local oMdlMain	:= oModelo:GetModel("MODEL_SC7p")
nOpca := oModelo:GetOperation()//FWModelActive():noperation

If nOpca == 5
	U_F0400105('F0100401',SC7->C7_FILIAL, SC7->C7_NUM)
EndIf

If Empty(oMdlMain:getValue("C7_FORNECE")) .AND. oMdlMain:getValue("C7_FORNECE") != NIL
	Help( ,, 'HELP',, "PREENCHA O CAMPO FONECEDOR", 1, 0)
	lRet  := .F.
EndIf

Return lRet

/*/{Protheus.doc} F0100411
Calcula a data de vencimento da parcela
@author 	Nairan Silva
@since 		17/02/2017
@version 	1.0
@param 		oModelo
@Project	MAN00000462901_EF_004
@return	l�gico
/*/
User Function F0100411(dDataEmis, cCond)
Local dRet := Date()
Local aDatas := CONDICAO(100, cCond ,, dDataEmis ,,,, )

Default cCond := '001'
Default dDataEmis := Date()

If Len(aDatas) > 0
	If !Empty(aDatas[01][01])
		dRet := aDatas[01][01]
	EndIf
EndIf

Return dRet