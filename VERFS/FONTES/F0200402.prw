#Include 'TOTVS.CH'
//-----------------------------------------------------------------------------
/*/{Protheus.doc} F0200402
Valida setor(es) informados no parametros
@type User function
@author Cristiane Thomaz Polli
@since 07/07/2016
@version P12.1.7
@Project MAN00000463301_EF_004
@return ${lValSet}, ${.T. o cont�udo informado � v�lido .F. n�o � v�lido.}
/*/
//-----------------------------------------------------------------------------
User Function F0200402()
	
	Local	lValSet		:= .T.
	Local	aAreaAtu	:= GetArea()
	Local	aAreaNNR	:= NNR->(GetArea())
	Local	nSetor		:= 0
	Local	nTamCpo		:= TamSX3('NNR_CODIGO')[1]
	Local 	cSetor		:= Alltrim(MV_PAR02)
	Local 	nTamCont	:= Len(cSetor)
	Local 	aSetAtu		:= StrTokArr(cSetor,";")
	Local 	nPosSet		:= 0
	Local   nPosAux		:= 0
	
	//Valida 
	If !Empty(cSetor) 
	
	//verifica se o �ltimo caracter � ponto e v�rgula.
		If Substring(Alltrim(cSetor),len(cSetor),1) <> ';' 
				
			Aviso('Obrigat�rio Separador (;)','Informe o ponto  e virgula (;) ap�s cada c�digo. Obrigat�rio!',{'OK'})
			lValSet		:= .F.
								
		EndIf
		
		//Varre o range para certificar-se que todos os valores existem na tabela referida  (NNR)	
		For nSetor := 1 to Len(aSetAtu)
			
			If nTamCpo < len(aSetAtu[nSetor])
			
					Aviso('N�o v�lido tamanho','O c�digo informe '+aSetAtu[nSetor]+' ultrapassa o tamanho do campo Setor. N�o � V�lido!',{'OK'})
					lValSet		:= .F.
				
			Else
			
				dbSelectArea('NNR')
				
				NNR->(dbSetOrder(1))
								
				if !NNR->(dbSeek(xFilial('NNR')+Padr(aSetAtu[nSetor],nTamCpo,' ')))
				
					Aviso('Inv�lido Setor','O Setor '+Padr(aSetAtu[nSetor],nTamCpo,' ')+' n�o existe. Informe um setor v�lido!',{'OK'})
					lValSet		:= .F.
				
				Elseif NNR->(FieldPos("NNR_MSBLQL")) > 0 .And. NNR->NNR_MSBLQL == '1'
				
					Aviso('N�o permitido Setor','O Setor '+Padr(aSetAtu[nSetor],nTamCpo,' ')+' esta bloqueado. Informe um setor ativo!',{'OK'})
					lValSet		:= .F.
									
				EndIf

				nPosSet	:= 	nSetor+1
				//Verifica se a informa��o j� foi digitada no parametro
				If lValSet	.AND. nPosSet <= len(aSetAtu) .AND. (nPosAux	:= Ascan(aSetAtu,{|x| Alltrim(x) == aSetAtu[nSetor]},nPosSet)) > 0
					
					Aviso('Duplicidade','O Setor '+aSetAtu[nSetor]+' foi informada novamente na posi��o '+StrZero(nPosAux,3)+'. N�o duplique o filtro!',{'OK'})
					lValSet		:= .F.	
								
				EndIf
			
			EndIf
					
		Next nSetor	
		
	Else
	
		Aviso('Obrigat�rio Preenchimento','Informe pelo menos um setor. Campo  obrigat�rio!',{'OK'})
		lValSet	:= .F.
		
	EndIf

	RestArea(aAreaAtu)
	RestArea(aAreaNNR)
	
Return lValSet