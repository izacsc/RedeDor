#Include "PROTHEUS.CH"
#DEFINE DF_SENHA  "R&dD0r_12!"
/*Fun��o respons�vel pelo banco de conhecimento espec�fico.
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param	    	nOpc - Op��o selecionada pelo usu�rio. 1 - Visualizar, 3 - Inclus�o, 5 - Exclus�o, 6 - Download
@Return		Nil	 */
User Function F0400101(nOpc)                        
Local oButton1
Local oButton2
Local oButton3
Local oButton4
Local oGet1
Local cGet1 := Space(TAMSX3("P09_NOMDOC")[1])
Local oGroup1
Local oGroup2
Local oGroup3
Local oGroup4
Local oSay1
Local oWBrowse1
Local aWBrowse1 := {}
Local oGetDad1
Local aHeader1	:= {}
Local aCols1		:= {}	
Local nEditGetD	:= ""
Local bDelOk := {|| VldDel(oGetDad1,nOpc)}

Default nOpc := 3

Static oDlg

Private cFile := ""

If nOpc == 5
	If !VldExc()
		Return
	Endif
Endif

//TITULO,X3_CAMPO,X3_PICTURE,X3_TAMANHO,X3_DECIMAL,X3_VALID,X3_USADO,X3_TIPO,X3_F3,X3_CONTEXT,X3_CBOX,X3_RELACAO
Aadd(aHeader1,	{ 	'Documento'	,'P09_NOMDOC'	,'@!'						,TAMSX3("P09_NOMDOC")[1],00,'','���������������','C','','R','',''})
Aadd(aHeader1,	{ 	'Rotina'		,'P09_ROTINA'	,'@!'						,10,00,'','���������������','C','','R','',''})
Aadd(aHeader1,	{ 	'C�digo'	,'P09_CODDOC'	,'@!'						,09,00,'','���������������','C','','R','',''})


If nOpc == 1
	nEditGetD	:= GD_UPDATE
	FilDados(@aCols1)
Elseif nOpc == 3
	nEditGetD	:= GD_UPDATE+GD_DELETE
	FilDados(@aCols1)
	If Len(aCols1) == 0
		Aadd(aCols1,{"","","",.F.})
	EndIf
Else
	nEditGetD	:= GD_DELETE
	FilDados(@aCols1)
Endif

If Empty(aCols1)
	Aviso("Visualizar Arquivo","N�o existe documentos anexado.",{"Ok"},1,)
	Return
Endif

  DEFINE MSDIALOG oDlg TITLE "Banco de Conhecimento Rede D'or" FROM 000, 000  TO 400, 800 COLORS 0, 16777215 PIXEL

    @ 002, 003 GROUP oGroup1 TO 195, 395 PROMPT "Banco de Conhecimento Espec�fico" OF oDlg COLOR 0, 16777215 PIXEL
    @ 036, 007 GROUP oGroup3 TO 192, 390 OF oDlg COLOR 0, 16777215 PIXEL
    @ 013, 007 GROUP oGroup2 TO 035, 390 OF oDlg COLOR 0, 16777215 PIXEL
    @ 021, 010 SAY oSay1 PROMPT "Selecione o documento:" SIZE 061, 007 OF oDlg COLORS 0, 16777215 PIXEL
    
     oGetDad1 := MsNewGetDados():New(042, 015 ,140 , 377, nEditGetD,  , , , , , , , ,bDelOk , oDlg, aHeader1, aCols1)
    
    
    @ 019, 071 MSGET oGet1 VAR cGet1 SIZE 250, 010 OF oDlg COLORS 0, 16777215 Valid (AddGrid(@oGetDad1,@aCols1,@cGet1,@oGet1)) WHEN nOpc == 3 PIXEL
    @ 019, 330 BUTTON oButton1 PROMPT "Buscar Arquivo" SIZE 050, 012 OF oDlg Action U_F0400102(@oGet1,@cGet1) WHEN nOpc == 3 PIXEL

	//fWBrowse1(@oWBrowse1,@aWBrowse1)
    @ 158, 010 GROUP oGroup4 TO 188, 388 OF oDlg COLOR 0, 16777215 PIXEL

	If nOpc == 1
		@ 166, 226 BUTTON oButton2 PROMPT "Download" SIZE 049, 017 	OF oDlg  Action AbriArq(oGetDad1,2) WHEN nOpc == 1 PIXEL
		@ 166, 280 BUTTON oButton2 PROMPT "Visualiza" SIZE 049, 017 	OF oDlg Action AbriArq(oGetDad1,1) 	WHEN nOpc == 1 PIXEL
		@ 166, 335 BUTTON oButton3 PROMPT "Cancela" SIZE 049, 017 		OF oDlg Action oDlg:End() PIXEL
	ElseIf nOpc == 3
		@ 166, 172 BUTTON oButton2 PROMPT "Download" SIZE 049, 017 	OF oDlg  Action AbriArq(oGetDad1,2) WHEN nOpc == 1 PIXEL
		@ 166, 226 BUTTON oButton2 PROMPT "Visualiza" SIZE 049, 017 	OF oDlg Action AbriArq(oGetDad1,1) 	WHEN nOpc == 1 PIXEL
		@ 166, 280 BUTTON oButton3 PROMPT "Cancela" SIZE 049, 017 		OF oDlg Action oDlg:End() PIXEL
		@ 166, 335 BUTTON oButton4 PROMPT "Confirma" SIZE 049, 017 	OF oDlg Action (IIF(GrvArq(oGetDad1,nOpc),oDlg:End(),)) /*WHEN(nOpc == 3 .Or. nOpc == 5)*/ PIXEL
	ElseIf nOpc == 5
		@ 166, 172 BUTTON oButton2 PROMPT "Download" SIZE 049, 017 	OF oDlg  Action AbriArq(oGetDad1,2) WHEN nOpc == 1 PIXEL
		@ 166, 226 BUTTON oButton2 PROMPT "Visualiza" SIZE 049, 017 	OF oDlg Action AbriArq(oGetDad1,1) 	WHEN nOpc == 1 PIXEL
		@ 166, 280 BUTTON oButton3 PROMPT "Sair" SIZE 049, 017 		OF oDlg Action oDlg:End() PIXEL
		@ 166, 335 BUTTON oButton4 PROMPT "Excluir" SIZE 049, 017 	OF oDlg Action ExcArq(oGetDad1,nOpc) /*WHEN(nOpc == 3 .Or. nOpc == 5)*/ PIXEL	
	EndIF
	If nOpc == 1
		oGetDad1:oBrowse:bLDblClick := {|| Processa( {|| AbriArq(oGetDad1,1) }, "Aguarde", "Chamando funcao..." )} 
	EndIf
		
  ACTIVATE MSDIALOG oDlg CENTERED

Return

/*{Protheus.doc} F0400102()
Executa a fun��o cGetFile e atualiza o Objeto
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param	    	oGet - Objeto em tela
@Param	    	cGet - Diret�rio do arquivo
@Return		Nil	 */
User Function F0400102(oGet1,cGet1)

cGet1 := cGetFile('*.*','Selecione arquivo',0,'C:\',.T.,,.F.)
oGet1:Refresh()
oGet1:SetFocus()

Return 

Static Function VldDel(oGetDad1,nOpc)
Local nX		:= 0
Local aCols1	:= aClone(oGetDad1:aCols)
Local nAt		:= oGetDad1:nAt
Local lRet		:= .T.

If nOpc != 5
	If !Empty(aCols1[nAt][3])
		lRet := .F.
		Aviso("Erro na linha!","N�o � poss�vel deletar itens j� inclu�dos. Para esse processo � necess�rio selecionar a op��o Banco de Conhecimento - Excluir",{"Ok"},1,)
	EndIf
Endif

Return lRet

/*{Protheus.doc} AbriArq()
Fun��o executada para visualizar ou baixar arquivos
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	oGetDad1 - Diret�rio do arquivo
@Param 	   	nOpc - 1 - Visualizar, 2 - Download
@Return		Nil	 */
Static Function AbriArq(oGetDad1,nOpc)

Local aDados	:= aClone(oGetDad1:aCols)
Local nLin		:= oGetDad1:nAt
Local cArqRed	:= "\RDANEXOS\"
Local cArqLoc	:= GetTempPath() 
Local cFile	:= aDados[nLin][03]
Local lRet		:= .T.
Local nHdl 	:= 0
Local cNArq	:= AllTrim(aDados[nLin][01])

If nOpc != 1
	cArqLoc	:= cGetFile('*.*','Selecione o Diret�rio Destino',0,'C:\',.F.,nOR( GETF_LOCALHARD, GETF_LOCALFLOPPY, GETF_RETDIRECTORY ),.F.)
Endif

If Empty(cArqLoc)
	Return
Endif

CpyS2T( cArqRed+cFile+".MZP", cArqLoc, ,  )

If !(File(cArqLoc+cFile+".MZP"))
    lRet := .F.
Endif

If lRet			
	lRet := MsDecomp( cArqLoc+cFile+".MZP" , cArqLoc,DF_SENHA  )
Endif

If lRet .And. nOpc == 1
	nHdl := shellExecute("Open", cArqLoc+cNArq,"Null" , "C:\", 1 )
Endif

nHdl := FErase( cArqLoc+cFile+".MZP", ,)

If !lRet .Or. nHdl < 0
    Alert("N�o foi poss�vel abrir o arquivo.")    
Else
	Aviso("Sucesso!","Processo realizado com Sucesso.",{"Ok"},1,)
Endif

Return

/*{Protheus.doc} AddGrid()
Fun��o para adicionar arquivos no Grid
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	oGetDad1 - Objeto GetDados
@Param 	   	aCols1 - Acols do Objeto
@Param 	   	cGet1 - Conte�do do Get
@Param 	   	oGet1 - Objeto do Get
@Return		Nil	 */
Static Function AddGrid(oGetDad1,aCols1,cGet1,oGet1)
Local nX := 0
Local aDados	:= aClone(oGetDad1:aCols)

If !Empty(cGet1)

	If aScan(aDados,{|x| AllTrim(x[1])== cGet1}) > 0
		Alert("O arquivo "+AllTrim(cGet1)+" j� foi selecionado.")
	Elseif !(File(cGet1))
		Alert("O arquivo n�o existe no diret�rio especificado.")
	Else
		If Empty(aDados[01][01])
			aDados := {}
		Endif
		Aadd(aDados,{cGet1,FunName(),'',.F.})	
		//Aadd(aCols1,{(cAliasTrb)->P09_NOMDOC,(cAliasTrb)->P09_ROTINA, (cAliasTrb)->P09_CODDOC,.F.})
	Endif
Endif
oGetDad1:aCols := aClone(aDados)
oGetDad1:Refresh()
cGet1 := Space(50)
oGet1:Refresh()
Return


/*{Protheus.doc} GrvArq()
Fun��o para gravar os arquivos selecionados
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	oGetDad1 - Objeto GetDados
@Param 	   	nOpc - 3 - Inclus�o, 5 - Exclus�o
@Return		Nil	 */
Static Function GrvArq(oGetDad1,nOpc)
Local aDados	:= aClone(oGetDad1:aCols)
Local aHeader	:= aClone(oGetDad1:aHeader)
Local cArqRed	:= "\RDANEXOS\"
Local cArqLoc	:= GetTempPath()
Local cRet		:= ""
Local nRet		:= 0
Local lRet		:= .T.
Local nHdl 	:= 0
Local cNArq	:= ""//AllTrim(SubStr(cFile,rAt( '\', cFile)+1,50))
Local nX		:= 0
Local cCodigo	:= ""
Local cNivel	:= RetNvlTor()
Local cMsg		:= 'N�o foi poss�vel realizar a opera��o.'

If nOpc <> 1

	If !MsgYesNo("Confirma Opera��o?","Banco de Conhecimento")
		Return
	EndIf
	
	If !(ExistDir( "\RDANEXOS" ))
		nRet := MakeDir( "\RDANEXOS" )
		If nRet != 0
		    Help('',1,'GERAZIP',,"N�o foi poss�vel criar o diret�rio na rede.")
			Return
		Endif   
	EndIf
	
	If !(ExistDir( cArqLoc ))
		nRet := MakeDir( cArqLoc )
		If nRet != 0
		    Help('',1,'GERAZIP',,"N�o foi poss�vel criar o diret�rio Local.")
			Return
		Endif   
	EndIf
	
	If lRet := ValidArq(aDados, nOpc)
	
		Begin Transaction
			If nOpc == 3
				For nX := 1 To Len(aDados)
					If aDados[nX][Len(aHeader)+1] .Or. !Empty(aDados[nX][3])
						Loop
					Endif
					cNArq	:= RetNomeArq(aDados[nX][01],2)
					cCodigo := GETSXENUM("P09","P09_CODDOC")
					RecLock("P09",.T.)
					P09->P09_FILIAL	:= FWxFilial("P09")
					P09->P09_CODDOC	:= cCodigo
					P09->P09_NOMDOC	:= cNArq
					P09->P09_DTHORA	:= DTOS(Date())+StrTran(Time(),":","")
					P09->P09_NIVEL 	:= cNivel
					P09->P09_CODORI	:= RetCodOri(aDados[nX][02])[01]
					P09->P09_ROTINA	:= aDados[nX][02]
					P09->(MsUnLock())
					
					/*=============================================================|
					|Verifica se h� t�tulo a pagar recusado.                       |
					|Se houver, altera o status de R (recusado) para C (corrigido).|
					|Par�metros:                                                   |
					|01 - Filial                                                   |
					|02 - Rotina                                                   |
					|03 - Origem                                                   |
					|=============================================================*/
					
					U_F010101I(FWxFilial('P09'),aDados[nX][02],RetCodOri(aDados[nX][02])[02])
					
					cRet := MsCompress( aDados[nX][01] , cArqLoc+cCodigo+".MZP",DF_SENHA )
					
					nHdl := fOpen(cRet)
					
					If nHdl == -1
					    DisarmTransaction()
					    rollbacksx8()
					    lRet := .F.
					    Exit
					Else
						fClose(nHdl)
						If !(CpyT2S( cArqLoc+cCodigo+".MZP", cArqRed, ,  ))
						    rollbacksx8()
						    DisarmTransaction()
						    lRet := .F.
						    Exit
						Else
							FErase( cRet, ,)			
						Endif
					Endif
					ConfirmSX8()
				Next
			Else
				For nX := 1 To Len(aDados)
					If aDados[nX][Len(aHeader)+1]
						cCodigo	:= aDados[nX][03]
						nHdl := fErase(cArqRed+cCodigo+".MZP")
						If P09->(DbSeek(FWxFilial("P09")+cCodigo))
							RecLock("P09",.F.)
							P09->(dbDelete())
							P09->(MsUnLock())
						Endif
					EndIf
				Next	
			Endif
		End Transaction
		
		If lRet
			Aviso("Sucesso!","Processo realizado com Sucesso.",{"Ok"},1,)
		Else
		    Help('',1,'GERAZIP',,cMsg)    
		Endif
		
	EndIf
	
EndIf

Return lRet

/*{Protheus.doc} ExcArq()
Fun��o para Excluir os arquivos selecionados
@Author			Nairan Alves Silva
@Since			04/01/2017
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	oGetDad1 - Objeto GetDados
@Param 	   	nOpc - 3 - Inclus�o, 5 - Exclus�o
@Return		Nil	 */
Static Function ExcArq(oGetDad1,nOpc)
Local aDados	:= aClone(oGetDad1:aCols)
Local aHeader	:= aClone(oGetDad1:aHeader)
Local aCols	:= {}
Local nAt		:= oGetDad1:nAt
Local cArqRed	:= "\RDANEXOS\"
Local cArqLoc	:= GetTempPath()
Local cRet		:= ""
Local nRet		:= 0
Local lRet		:= .T.
Local nHdl 	:= 0
Local cNArq	:= ""//AllTrim(SubStr(cFile,rAt( '\', cFile)+1,50))
Local nX		:= 0
Local cCodigo	:= ""
Local cNivel	:= RetNvlTor()
Local cMsg		:= 'N�o foi poss�vel realizar a opera��o.'

If nOpc <> 1

	If !MsgYesNo("Confirma Exclus�o?","Banco de Conhecimento")
		Return
	EndIf
	
	If lRet := ValidArq(aDados, nOpc)
	
		Begin Transaction
			cCodigo	:= aDados[nAt][03]
			nHdl := fErase(cArqRed+cCodigo+".MZP")
			If P09->(DbSeek(FWxFilial("P09")+cCodigo))
				RecLock("P09",.F.)
				P09->(dbDelete())
				P09->(MsUnLock())
			Endif
		End Transaction
		
		If lRet
			FilDados(@aCols)
			oGetDad1:aCols := aClone(aCols)
			oGetDad1:Refresh()
			Aviso("Sucesso!","Arquivo Exclu�do com Sucesso.",{"Ok"},1,)
		Else
		    Help('',1,'GERAZIP',,cMsg)    
		Endif
		
	EndIf
	
EndIf

Return lRet

/*{Protheus.doc} RetNomeArq()
Fun��o para retornar o nome do arquivo
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	cDir - Diret�rio do Arquivo
@Param 	   	nTipo - 1 - Sem Extens�o, 2 - Com extens�o
@Return		Nil	 */
Static Function RetNomeArq(cDir,nTipo)
Local cRet	:= ""
Local nArq	:= ""

Default nTipo := 1

cRet	:= AllTrim(SubStr(cDir ,rAt( '\', cDir )+1,TAMSX3("P09_NOMDOC")[1]))
If nTipo == 1
	cRet	:= SubStr(cRet,1,rAt( '.', cRet)-1)
Endif

Return cRet

/*{Protheus.doc} RetNvlTor()
Retorna o N�vel do Arquivo
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function RetNvlTor()
Local cFunc	:= FunName()
Local cRet		:= "00"

If cFunc == "MATA110"
	cRet := "01"
Elseif AllTrim(cFunc) $ "MATA120|MATA121"
	cRet := "02"
Elseif AllTrim(cFunc) == "MATA103"
	cRet := "03"
Elseif AllTrim(cFunc) == "MATA140"
	cRet := "04"
Elseif AllTrim(cFunc) == "FINA050"
	cRet := "05"
Elseif AllTrim(cFunc) == "FINA550"
	cRet := "06"
Elseif AllTrim(cFunc) == "GPEM660"
	cRet := "07"
Elseif AllTrim(cFunc) == "F0100401"
	cRet := "08"
Elseif AllTrim(cFunc) == "AE_DESPV"
	cRet := "09"
Elseif AllTrim(cFunc) == "F0500101"
	cRet := "10"	
EndIf

Return cRet

/*{Protheus.doc} RetCodOri()
Retorna o Campo Chave da Tabela
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	cFunc - Fun��o respons�vel por acionar a rotina de banco de conhecimento
@Return		Nil	 */
Static Function RetCodOri(cFunc)
Local cRet		:= ""
Local cIntFin	:= ""
Local aRet		:= {}

If cFunc == "MATA110"
	cRet := SC1->C1_NUM
	cIntFin := cRet
Elseif AllTrim(cFunc) $ "MATA120|MATA121"
	cRet := SC7->C7_NUM
	cIntFin := cRet
Elseif AllTrim(cFunc) $ "MATA103"
	cRet := SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
	cIntFin := cRet
Elseif AllTrim(cFunc) $ "MATA140"
	cRet := SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
	cIntFin := cRet
Elseif AllTrim(cFunc) $ "FINA050"
	cRet := SE2->(E2_NUM+E2_PREFIXO+E2_FORNECE+E2_LOJA)
	cIntFin := cRet
Elseif AllTrim(cFunc) $ "FINA550"
	cRet := SET->ET_CODIGO
	cIntFin := cRet
ElseIf AllTrim(cFunc) $ "GPEM660"
	cRet := RC1->(RC1_FILTIT+RC1_CODTIT+RC1_PREFIX+RC1_NUMTIT)
	cIntFin := RC1->(RC1_FILTIT+RC1_PREFIX+RC1_NUMTIT+RC1_TIPO+RC1_FORNEC)
ElseIf AllTrim(cFunc) $ "F0100401"
	cRet := SC7->C7_NUM
	cIntFin := cRet
ElseIf AllTrim(cFunc) $ "AE_DESPV"
	cRet := LHQ->LHQ_CODIGO
	cIntFin := cRet
ElseIf AllTrim(cFunc) $ "F0500101"
	cRet := P10->P10_MATRIC
	cIntFin := cRet
Endif

AADD(aRet,cRet)
AADD(aRet,cIntFin)

Return aRet

/*{Protheus.doc} FilDados()
Valida se os arquivos poder�o ou n�o ser exclu�dos
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function VldExc()
Local lRet	:= .T.
Local cFunc	:= FunName()
Local cMsg		:= ""

If cFunc == "MATA110"
	lRet := !(AllTrim(SC1->C1_APROV) != 'B')
	If !lRet
		cMsg := "A solicita��o de compra j� foi aprovada."
	Endif
Elseif AllTrim(cFunc) $ "MATA120|MATA121"
	lRet := !(AllTrim(SC7->C7_CONAPRO) != 'B')
	If !lRet
		cMsg := "O pedido de compra j� foi aprovado."
	Endif
Elseif AllTrim(cFunc) == "MATA103"
	lRet := Empty(SF1->F1_DUPL)
	If !lRet
		cMsg := "A nota fiscal j� gerou um t�tulo a pagar."
	Endif
Elseif AllTrim(cFunc) == "MATA140"
	lRet := Empty(SF1->F1_STATUS)
	If !lRet
		cMsg := "A Pr�-Nota j� foi classificada."
	Endif
Elseif AllTrim(cFunc) == "FINA050"
	lRet := Empty(SE2->E2_BAIXA)
	If !lRet
		cMsg := "O t�tulo j� foi baixado."
	Endif
Elseif AllTrim(cFunc) == "FINA550"
	lRet := AllTrim(SET->ET_SITUAC) == "0"
	If !lRet
		cMsg := "O caixinha n�o est� mais ativo."
	Endif
Elseif AllTrim(cFunc) == "GPEM660"
	lRet := Empty(RC1->RC1_NUMTIT)
	If !lRet
		cMsg := "T�tulo gerado. N�o � poss�vel excluir anexos."
	Endif
Elseif AllTrim(cFunc) == "F0100401"
	//lRet := !(AllTrim(SC7->C7_CONAPRO) != 'B')
	lRet := Empty(SC7->C7_APROV)
	If !lRet
		cMsg := "A solicita��o de pagamento j� foi aprovada."
	Endif
Elseif AllTrim(cFunc) == "AE_DESPV"
	lRet := Empty(Posicione("LHP",1,xFilial("LHQ") + LHQ->LHQ_CODIGO,"LHP_DOCUME"))
	If !lRet
		cMsg := "A presta��o de conta j� possui um t�tulo gerado."
	Endif	
EndIf

If !lRet
	Alert(cMsg)
Endif

Return lRet

/*{Protheus.doc} FilDados()
Filtra informa��es do GetDados
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Param     	aCols1 - Acols do GetDados
@Return		Nil	 */
Static Function FilDados(aCols1)
Local cQuery	:= ""
Local aAreaSC1	:= SC1->(GetArea())
Local aAreaSC7	:= SC7->(GetArea())
Local aAreaSD1	:= SD1->(GetArea())
Local aAreaSF1	:= SF1->(GetArea())
Local aAreaSE2	:= SE3->(GetArea())
Local cAliasTrb  := GetNextAlias()
Local cFunc	:= FunName()

If AllTrim(cFunc) == "MATA110"
	cQuery += QuerySC1()
Elseif AllTrim(cFunc) $ "MATA120|MATA121"
	cQuery += QuerySC7()	
	SC1->(DbSetOrder(6))
	If SC1->(DbSeek(FWxFilial("SC1")+SC7->C7_NUM))
		cQuery += " UNION "
		cQuery += QuerySC1()
		lSC1	:= .T.
	Endif
ElseIf AllTrim(cFunc) $ "MATA103|MATA140"
	
	If cFunc == "MATA103"
		cQuery += QuerySF1("'MATA103','MATA140'")
	Else
		cQuery += QuerySF1("'MATA140'")
	Endif
	SC7->(DbSetOrder(1))
	SD1->(DbSetOrder(1))
	If SD1->(DbSeek(xFilial("SD1")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)))
		While SD1->(D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA) == SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			If SC7->(DbSeek(FWxFilial("SC7")+SD1->D1_PEDIDO))
				cQuery += " UNION "
				cQuery += QuerySC7()					
			Endif
			SC1->(DbSetOrder(6))
			If SC1->(DbSeek(FWxFilial("SC1")+SC7->C7_NUM))
				cQuery += " UNION "
				cQuery += QuerySC1()
			Endif
			SD1->(DbSkip())
		EndDo
	Endif
ElseIf AllTrim(cFunc) $ "FINA050"
	cQuery += QuerySE2()
	SC7->(DbSetOrder(1))
	SD1->(DbSetOrder(1))
	SF1->(DbSetOrder(1))
	If SD1->(DbSeek(xFilial("SD1")+SE2->(E2_NUM+E2_PREFIXO+E2_FORNECE+E2_LOJA))) .And. SF1->(DbSeek(xFilial("SF1")+SE2->(E2_NUM+E2_PREFIXO+E2_FORNECE+E2_LOJA)))
		cQuery += " UNION "
		cQuery += QuerySF1("'MATA103','MATA140'")							
		While SD1->(D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA) == SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			If SC7->(DbSeek(FWxFilial("SC7")+SD1->D1_PEDIDO))
				cQuery += " UNION "
				cQuery += QuerySC7()					
			Endif
			SC1->(DbSetOrder(6))
			If SC1->(DbSeek(FWxFilial("SC1")+SC7->C7_NUM))
				cQuery += " UNION "
				cQuery += QuerySC1()
			Endif
			SD1->(DbSkip())
		EndDo
	Endif	
ElseIf AllTrim(cFunc) $ "FINA550"
	cQuery += QuerySET()
ElseIf AllTrim(cFunc) $ "GPEM660"
	cQuery += QueryRC1()
ElseIf "F0100401" $ AllTrim(cFunc)
	cQuery += QuerySolPg()
Elseif AllTrim(cFunc) == "AE_DESPV"
	cQuery += QueryLHC()
Elseif AllTrim(cFunc) == "F0500101"
	cQuery += QueryP10()
Endif

cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTrb,.T.,.T.)

While (cAliasTrb)->(!EOF())
	Aadd(aCols1,{(cAliasTrb)->P09_NOMDOC,(cAliasTrb)->P09_ROTINA, (cAliasTrb)->P09_CODDOC,.F.})
	(cAliasTrb)->(DbSkip())
EndDo

RestArea(aAreaSC1)
RestArea(aAreaSC7)
RestArea(aAreaSD1)
RestArea(aAreaSF1)
RestArea(aAreaSE2)
(cAliasTrb)->(DbCloseArea())
Return

/*{Protheus.doc} QuerySC1()
Query para retornar dados anexados da rotina Solicita��o de Compra
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySC1()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SC1->C1_NUM+"' "
cQuery += " AND P09_FILIAL = '"+SC1->C1_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'MATA110' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*{Protheus.doc} QuerySC7()
Query para retornar dados anexados da rotina Pedido de Compra
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySC7()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SC7->C7_NUM+"' "
cQuery += " AND P09_FILIAL = '"+SC7->C7_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'MATA121' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*{Protheus.doc} QuerySF1()
Query para retornar dados anexados da rotina Documento de Entrada
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySF1(cRotina)
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)+"' "
cQuery += " AND P09_FILIAL = '"+SF1->F1_FILIAL+" ' "
cQuery += " AND P09_ROTINA IN ("+cRotina+") "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*{Protheus.doc} QuerySE2()
Query para retornar dados anexados da rotina Contas a Pagar
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySE2()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SE2->(E2_NUM+E2_PREFIXO+E2_FORNECE+E2_LOJA)+"' "
cQuery += " AND P09_FILIAL = '"+SE2->E2_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'FINA050' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery


/*{Protheus.doc} QuerySET()
Query para retornar dados anexados da rotina Caixinha
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySET()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SET->ET_CODIGO+"' "
cQuery += " AND P09_FILIAL = '"+SET->ET_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'FINA550' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*{Protheus.doc} QueryRC1()
Query para retornar dados anexados da rotina Manuten��o de Benef�cios
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QueryRC1()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+RC1->(RC1_FILTIT+RC1_CODTIT+RC1_PREFIX+RC1_NUMTIT)+"' "
cQuery += " AND P09_FILIAL = '"+RC1->RC1_FILTIT+" ' "
cQuery += " AND P09_ROTINA = 'GPEM660' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery



/*{Protheus.doc} QuerySolPg()
Query para retornar dados anexados da rotina Solicita��o de Pagamento
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QuerySolPg()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+SC7->C7_NUM+"' "
cQuery += " AND P09_FILIAL = '"+SC7->C7_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'F0100401' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*{Protheus.doc} QueryLHC()
Query para retornar dados anexados da rotina Presta��o de Contas
@Author			Nairan Alves Silva
@Since			20/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
Static Function QueryLHC()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA  FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+LHQ->LHQ_CODIGO+"' "
cQuery += " AND P09_FILIAL = '"+LHQ->LHQ_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'AE_DESPV' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*/{Protheus.doc} QueryP10
Query para retornar dados anexados da rotina Solicita��o de Desligamento
@type function
@author alexandre.arume
@since 14/10/2016
@version 1.0
@return cQuery

/*/
Static Function QueryP10()
Local cQuery := ""
cQuery += " SELECT P09_CODDOC, P09_NOMDOC, P09_NIVEL, P09_ROTINA FROM "+RetSqlName("P09")+" "	
cQuery += " WHERE P09_CODORI = '"+P10->P10_MATRIC+"' "
cQuery += " AND P09_FILIAL = '"+P10->P10_FILIAL+" ' "
cQuery += " AND P09_ROTINA = 'F0500101' "
cQuery += " AND D_E_L_E_T_ = ' ' "
Return cQuery

/*/{Protheus.doc} ValidArq
Verifica se ja existe o arquivo.
@type function
@author alexandre.arume
@since 16/12/2016
@version 1.0
@param cRotina, character, (Descri��o do par�metro)
@param cNomDoc, character, (Descri��o do par�metro)
@return ${return}, ${return_description}

/*/
Static Function ValidArq(aDados, nOpc)
	
	Local lRet 		:= .T.
	Local cQuery 	:= ""
	Local cAlias 	:= GetNextAlias()
	Local cDocs		:= ""
	Local nX		:= 0
	Local lDel		:= .F.
	Local lProc	:= .F.
	
	If Len(aDados) > 0 .AND. nOpc == 3
	
		cQuery := "SELECT DISTINCT P09_NOMDOC "
		cQuery += "FROM " + RetSqlName("P09") + " "
		cQuery += "WHERE P09_FILIAL = '" + FWxFilial("P09") + "' "
		cQuery += "AND P09_ROTINA = '" + aDados[01][02] + "' "
		cQuery += "AND P09_CODORI = '" +RetCodOri(aDados[01][02])[01]+ "' "
		cQuery += "AND P09_NOMDOC IN ("
		
		For nX := 1 To Len(aDados)
			If aDados[nX][4]
				lDel := .T.
			Else
				If !Empty(aDados[nX][3])
					lDel := .T.
				Else
					lDel := .F.
				EndIf	
			EndIf
			If !lDel
				cQuery += "'" + RetNomeArq(aDados[nX][1], 2) + "', "
				lProc := .T.
			EndIf	
		Next
		
		cQuery := Left(cQuery, Len(cQuery)-2) + ")"
		cQuery += " AND D_E_L_E_T_ = ' ' "
		
		If lProc
			cQuery := ChangeQuery(cQuery)
					
			dbUseArea(.T., "TOPCONN", TCGenQry( ,,cQuery ), cAlias, .F., .T.)
			
			While !(cAlias)->(Eof())
				cDocs += AllTrim((cAlias)->P09_NOMDOC) + ", "
				lRet := .F.
				
				(cAlias)->(dbSkip())
			End
			
			If ! Empty(cDocs) 
				Alert("Os arquivos " + Left(cDocs, Len(cDocs)-2) + " j� foram inseridos.")
			EndIf
		EndIf
		
	EndIf
	
Return lRet
