#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE 'FWMVCDEF.CH'
/*/{Protheus.doc} F0500307
Web Service referente as solicita��es para aprova��o e reprova��o
@type function
@author henrique.toyada
@since 26/10/2016
@project MAN00000463301_EF_003
/*/
User Function F0500307()
	
Return

//===========================================================================================================

WSSTRUCT AcompaSol
	WSDATA Codigo     As String
	WSDATA SolData    As String
	WSDATA CodSoli    AS String
	WSDATA TpSolic    As String
	WSDATA Status     As String
	WSDATA Solicita   As String
	WSDATA Visao      As String
	WSDATA Matric     As String
	WSDATA FilRh3     AS String
ENDWSSTRUCT

WSSTRUCT _AcompaSol
	WSDATA Acompanha  As ARRAY OF AcompaSol
ENDWSSTRUCT

//===========================================================================================================

WSSTRUCT Infh4
	WSDATA PA5FILIAL As String
	WSDATA PA5CDVAGA As String
	WSDATA PA5VLVAGA As String
	WSDATA PA5SLFECH As String
	WSDATA PA5CDCAND As String
	WSDATA PA5NMCAND As String
	WSDATA PA5CPFCAN As String
	WSDATA PA5DTAPRV As String
	WSDATA PA5LOGTOR As String
	WSDATA TMPNMVAGA As String
	WSDATA TMPNOMTOR As String
ENDWSSTRUCT

WSSTRUCT _Infh4
	WSDATA Registro  As ARRAY OF Infh4
ENDWSSTRUCT

//===========================================================================================================

WSSTRUCT SolDes
	WSDATA P10FILIAL  As String
	WSDATA P10MATRIC  As String
	WSDATA TMPMATRIC  As String
	WSDATA P10DTSOLI  As String
	WSDATA P10CODRES  As String
	WSDATA TMPDESRES  As String
	WSDATA P10MOTIVO  As String
	WSDATA P10DTDEMI  AS String
ENDWSSTRUCT

WSSTRUCT _SolDes
	WSDATA Registro  As ARRAY OF SolDes
ENDWSSTRUCT

//===========================================================================================================

WSSTRUCT SlCarSal
	WSDATA TMPFUNCAO As String //Codigo da Fun��o Atual
	WSDATA TMPD_FUNC As String //Nome da Fun��o Atual
	WSDATA TMPSALATU As String //Sal�rio Atual
	WSDATA TMPTABELA As String //C�dio da Tabela Atual
	WSDATA TMPNIVELT As String //Nivel da Tabela Atual
	WSDATA TMPFAIXAT As String //Faixa Atual
	WSDATA RASALARIO As String //Sal�rio Proposto
	WSDATA RATIPOALT As String //Motivo da Altera��o Salarial
	WSDATA TMPD_MOT  As String //Descri��o do� Motivo
	WSDATA TMPVLSALA As String //Valor do aumento proposto
	WSDATA TMPPERCSA As String //Percentual do Aumento Proposto
	WSDATA RACODFUNC As String //Codigo da Fun��o Proposta
	WSDATA TMPD_F_PP As String //Nome da Fun��o Proposta
	WSDATA TMPH_M_AT As String //Horas M�s Atual
	WSDATA TMPH_S_AT As String //Horas Semana Atual
	WSDATA TMPH_D_AT As String //Horas Dia Atual
	WSDATA RAHRSMES  As String //Horas M�s Proposto
	WSDATA RAHRSEMAN As String //Horas Semana Proposto
	WSDATA RAHRSDIA  As String //Horas Dia Proposto
	WSDATA TMPTURNAT As String //Turno de Trabalho Atual
	WSDATA TMPD_TURN As String //Nome do Turno Atual
	WSDATA TMPS_TURN As String //Seq.Ini.Turno Atual
	WSDATA TMPREGRA  As String //Regra Atual
	WSDATA RATNOTRAB As String //Turno de trabalho Proposto
	WSDATA TMPDESCTU As String //Nome do Turno Proposto
	WSDATA RASEQTURN As String //Seq.Ini. Turno Proposto
	WSDATA RAREGRA   As String //Regra Proposta
	WSDATA TMPFILIAL As String //Filial Atual
	WSDATA TMPD_FILI As String //Nome da Filial Atual
	WSDATA TMPCCATU  As String //Centro de Custo Atual
	WSDATA TMPD_CCAT As String //Descri��o do Centro de Custo Atual
	WSDATA TMPDEPTOA As String //Departamento Atual
	WSDATA TMPD_DEPT As String //Nome do Departamento Atual
	WSDATA RAFILIAL  As String //Filial/Empresa proposta
	WSDATA TMPN_F_D  As String //Nome da Filial/Empresa proposta
	WSDATA RACC      As String //Centro de Custo proposto
	WSDATA TMPD_CC_D As String //Descri��o do Centro de Custo Proposto
	WSDATA RADEPTO   As String //Departamento Proposto
	WSDATA TMPD_DEPD As String //Nome do Departamento Proposto
	WSDATA TMPTIPO   As String //Tipo da� Solicita��o
	WSDATA TMPPROCES As String //Processo Atual
	WSDATA RAPROCES  As String //Processo Proposto
	WSDATA TMPPOSTO	 As String //Codigo do posto Atual
	WSDATA RAPOSTO	 As String //Proposta do novo Posto
	WSDATA TMPCLVL	 As String //Classe Valor Atual
	WSDATA RACLVL	 As String //Proposta da nova Classe Valor
	WSDATA TMPITEM	 As String //Item contabil atual
	WSDATA RAITEM	 As String //Item cont�bil proposto		
ENDWSSTRUCT

WSSTRUCT _SlCarSal
	WSDATA Registro  As ARRAY OF SlCarSal
ENDWSSTRUCT

//===========================================================================================================

WSSTRUCT Treina
	WSDATA CALEND   As String
	WSDATA CURSO    As String
	WSDATA NOME     As String
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Funcio
	WSDATA CodMaFun   As String
	WSDATA NomMaFun   As String
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT SuperInf
	WSDATA CodMaSup   As String
	WSDATA FilMaSup   As String
	WSDATA NvlAprov   As String
	WSDATA EmpAprov   As String
	WSDATA Solici     As String
	WSDATA FilSolic   AS String
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT InfRh3Su
	WSDATA RH3VISAO   AS String
	WSDATA RH3FILINI  AS String
	WSDATA RH3MATINI  AS String
	WSDATA RH3FILAPR  AS String
	WSDATA RH3MATAPR  AS String
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT SupEml
	WSDATA QBMATRESP AS String
	WSDATA RANOME    As String
	WSDATA RAEMAIL   As String
ENDWSSTRUCT
//===========================================================================================================
WSSERVICE W0500307 DESCRIPTION "WebService Server responsavel pelas rotinas de Solicita��es feitas"
	
	WSDATA Matricula   AS String
	WSDATA Filtro      AS String
	WSDATA Campo       AS String
	WSDATA Valor       AS String
	WSDATA CodRh4      AS String
	WSDATA CodRh3      AS String
	WSDATA PA3XCOD     AS String
	WSDATA FILRH3     AS String
	WSDATA lRet        AS Boolean
	WSDATA _Acom       AS _AcompaSol // Informa��o da RH3
	WSDATA _Func       AS Funcio     // Informa��o Funcion�rio
	WSDATA _Inf4       AS Infh4      // Informa��o da RH4 FAP
	WSDATA _SlCrSl     AS SlCarSal   // Informa��o da RH4 CARGOS E SALARIOS
	WSDATA _SolDes     AS SolDes     // Informa��o da RH4 DESLIGAMENTO
	WSDATA _Treina     AS Treina     // Informa��o sobre o treinamento
	WSDATA _InfRh3     AS InfRh3Su
	WSDATA _SupInfP    AS SupEml
	WSDATA _SuperInf   AS SuperInf
	
	WSMETHOD InfRh3    DESCRIPTION "Pega Solicita��es da RH3"
	WSMETHOD FunRh3    DESCRIPTION "Pega Informa��o do funcionario da RH3"
	WSMETHOD InfRh4F   DESCRIPTION "Pega Solicita��es da RH4 FAP"
	WSMETHOD InfRh4D   DESCRIPTION "Pega Solicita��es da RH4 DESLIGAMENTO"
	WSMETHOD InfRh4C   DESCRIPTION "Pega Solicita��es da RH4 CARGOS E SALARIOS"
	WSMETHOD InfRh4I   DESCRIPTION "Pega codigo da tabela PA3"
	WSMETHOD BuscaTrm  DESCRIPTION "Informa��o do treinamento"
	WSMETHOD ChkRh3    DESCRIPTION "Verifica status da solicita��o"
	WSMETHOD InfSolVg  DESCRIPTION "Informa��es da RH3"
	WSMETHOD InfPegSup DESCRIPTION "Pega informa��o do superior"
	
ENDWSSERVICE
//===========================================================================================================
// Metodo que envia o emailpega as informa��es da RH3
WSMETHOD InfRh3 WSRECEIVE Matricula,Filtro,Campo,Valor WSSEND _Acom WSSERVICE W0500307
	
	Local nCnt	:= 1
	Local aAux := {}
	Local oSolicita
	
	aAux := RetRh3(::Matricula,::Filtro,::Campo,::Valor)
	
	If Len(aAux) > 0
		::_Acom := WSClassNew( "_AcompaSol" )
		
		::_Acom:Acompanha := {}
		oSolicita :=  WSClassNew( "AcompaSol" )
		For nCnt := 1 To Len(aAux)
			oSolicita:Codigo     := aAux[nCnt][1]
			oSolicita:SolData    := aAux[nCnt][2]
			oSolicita:CodSoli    := aAux[nCnt][3]
			oSolicita:TpSolic    := aAux[nCnt][4]
			oSolicita:Status     := aAux[nCnt][5]
			oSolicita:Solicita   := aAux[nCnt][6]
			oSolicita:Visao      := aAux[nCnt][7]
			oSolicita:Matric     := aAux[nCnt][8]
			oSolicita:FilRh3     := aAux[nCnt][9]
			AAdd( ::_Acom:Acompanha, oSolicita )
			oSolicita :=  WSClassNew( "AcompaSol" )
		Next
	Else
		::_Acom := WSClassNew( "_AcompaSol" )
		::_Acom:Acompanha := {}
	EndIf
Return .T.
//===========================================================================================================
// Metodo que envia o emailpega as informa��es da RH3
WSMETHOD FunRh3 WSRECEIVE CodRh3,FILRH3 WSSEND _Func WSSERVICE W0500307
	
	Local nCnt	:= 1
	Local aAux := {}
	Local oSolicita
	
	aAux := InfoRh3(::CodRh3,::FILRH3)
	
	If Len(aAux) > 0
		_Func:CodMaFun  := aAux[1]
		_Func:NomMaFun  := aAux[2]
	EndIf
Return .T.
//===========================================================================================================
Static Function InfoRh3(CodRh3,FILRH3)
	Local cQuery 		:= ''
	Local cAliasPa3	:= 'RETPA3'
	Local aAux			:= {}
	
	cQuery := "SELECT RH3_CODIGO,RH3_MAT,RA_NOME, QG_NOME "
	cQuery += "FROM " + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("SRA") + " SRA "
	cQuery += "ON 	SRA.RA_MAT = RH3.RH3_MAT "
	cQuery += "AND SRA.RA_FILIAL = RH3.RH3_FILINI "
	cQuery += "	AND SRA.D_E_L_E_T_ = ' ' "
	cQuery += "LEFT JOIN " + RetSqlName("SQG") + " SQG "
	cQuery += "ON 	SQG.QG_CURRIC = RH3.RH3_MAT "
	cQuery += "	AND SQG.D_E_L_E_T_ = ' ' "
	cQuery += "WHERE 	RH3.RH3_CODIGO = '" + CodRh3 + "' "
	cQuery += "		AND RH3.RH3_FILIAL = '" + FILRH3 + "' "
	cQuery += "		AND RH3.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasPa3)
	
	DbSelectArea(cAliasPa3)
	While ! (cAliasPa3)->(EOF())
		AADD(aAux, (cAliasPa3)->RH3_MAT)
		AADD(aAux, IIF(EMPTY((cAliasPa3)->RA_NOME),(cAliasPa3)->QG_NOME,(cAliasPa3)->RA_NOME))
		(cAliasPa3)->(DbSkip())
	End
	(cAliasPa3)->(DbCloseArea())
	
Return aAux

//===========================================================================================================
// Metodo que envia o emailpega as informa��es da RH3
WSMETHOD ChkRh3 WSRECEIVE CodRh3,FILRH3 WSSEND lRet WSSERVICE W0500307
	
	//RH3_FILIAL+RH3_CODIGO
	RH3->(DbSetOrder(1))
	If RH3->(DBSEEK(::FILRH3 + ::CodRh3))
		If RH3->RH3_STATUS = '1'
			::lRet := .T.
		Else
			::lRet := .F.
		EndIf
	EndIf
	
Return .T.
//===========================================================================================================
/*{Protheus.doc} RetRh3
(long_description)
@type function
@author henrique.toyada
@since 26/10/2016
@project MAN00000463301_EF_003
*/
Static Function RetRh3(cMatri, cFiltro, cCampo, cValor)
	
	Local cQuery 		:= ''
	Local cAliasPa3	:= 'RETPA3'
	Local nCnt			:= 1
	Local aAux			:= {}
	
	cQuery := "SELECT	RH3_CODIGO, RH3_DTSOLI , RH3_XTPCTM, PA7_DESCR, RH3_STATUS, RH3_MATINI, SRA.RA_NOME, RH3_VISAO, RH3_FILIAL "
	cQuery += "FROM	" + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("SRA") + " SRA "
	cQuery += "ON SRA.RA_MAT = RH3.RH3_MATINI "
	cQuery += "AND SRA.RA_FILIAL = RH3.RH3_FILINI "
	cQuery += "	AND SRA.D_E_L_E_T_ = ' ' "
	cQuery += "INNER JOIN " + RetSqlName("PA7") + " PA7 "
	cQuery += "ON PA7.PA7_CODIGO = RH3.RH3_XTPCTM "
	cQuery += "	AND PA7.D_E_L_E_T_ = ' ' "
	cQuery += "WHERE	RH3_MATAPR = '"+ cMatri +"' "
	cQuery += "		AND RH3_STATUS = '1' "
	cQuery += "		AND RH3_TIPO = ' ' "
	cQuery += "		AND RH3_XTPCTM != ' ' "
	If cFiltro == "2"
		cQuery += "		AND " + cCampo + " like '%" + cValor + "%' "
	EndIf
	cQuery += "		AND RH3.D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH3_CODIGO"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasPa3)
	
	DbSelectArea(cAliasPa3)
	While ! (cAliasPa3)->(EOF())
		AADD(aAux, {(cAliasPa3)->RH3_CODIGO,;
			(cAliasPa3)->RH3_DTSOLI,;
			(cAliasPa3)->RH3_XTPCTM,;
			(cAliasPa3)->PA7_DESCR,;
			(cAliasPa3)->RH3_STATUS,;
			(cAliasPa3)->RA_NOME,;
			(cAliasPa3)->RH3_VISAO,;
			(cAliasPa3)->RH3_MATINI,;
			(cAliasPa3)->RH3_FILIAL })
		(cAliasPa3)->(DbSkip())
	End
	(cAliasPa3)->(DbCloseArea())
	
Return aAux

//===========================================================================================================
// Metodo que envia o email
WSMETHOD InfRh4F WSRECEIVE CodRh4,FILRH3 WSSEND _Inf4 WSSERVICE W0500307
	
	Local aAuxH4 := {}
	Local nCnt   := 0
	Local oSolicita
	
	aAuxH4 := Rh4Inf(::CodRh4,::FILRH3)
	
	If Len(aAuxH4) > 0
		::_Inf4:PA5FILIAL  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_FILIAL"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5CDVAGA  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_CDVAGA"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5VLVAGA  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_VLVAGA"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5SLFECH  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_SLFECH"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5CDCAND  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_CDCAND"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5NMCAND  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_NMCAND"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5CPFCAN  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_CPFCAN"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5DTAPRV  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_DTAPRV"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:PA5LOGTOR  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_LOGTOR"}))�= 0,'',aAuxH4[var][2])
		::_Inf4:TMPNMVAGA  := POSICIONE("SQS",1,XFILIAL("SQS") + ALLTRIM(IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_CDVAGA"}))�= 0,'',aAuxH4[var][2])),'QS_DESCRIC')
		::_Inf4:TMPNOMTOR  := POSICIONE("SRA",1,IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_FILIAL"}))�= 0,'',aAuxH4[var][2]) + ALLTRIM(IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "PA2_LOGTOR"}))�= 0,'',aAuxH4[var][2])),'RA_NOME')
	EndIf
	
Return .T.
//===========================================================================================================
// Metodo que envia o email
WSMETHOD InfRh4I WSRECEIVE CodRh4,FILRH3 WSSEND PA3XCOD WSSERVICE W0500307
	
	Local aAuxH4 := {}
	Local nCnt   := 0
	Local oSolicita
	
	aAuxH4 := Rh4Inf(::CodRh4,::FILRH3)
	
	If Len(aAuxH4) > 0
		::PA3XCOD  := ALLTRIM(aAuxH4[2][2])
	EndIf
	
Return .T.
//===========================================================================================================
// Metodo que envia o email
WSMETHOD InfRh4D WSRECEIVE CodRh4,FILRH3 WSSEND _SolDes WSSERVICE W0500307
	
	Local aAuxH4 := {}
	Local nCnt   := 0
	Local oSolicita
	
	aAuxH4 := Rh4Inf(::CodRh4,::FILRH3)
	
	If Len(aAuxH4) > 0
		::_SolDes:P10FILIAL  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_FILIAL"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:P10MATRIC  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_MATRIC"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:P10DTSOLI  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_DTSOLI"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:P10CODRES  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_CODRES"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:TMPDESRES  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_DESRES"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:P10MOTIVO  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_MOTIVO"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:P10DTDEMI  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_DTDEMI"}))�= 0,'',aAuxH4[var][2])
		::_SolDes:TMPMATRIC  := POSICIONE("SRA",1,IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_FILIAL"}))�= 0,'',aAuxH4[var][2]) + ALLTRIM(IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "P10_MATRIC"}))�= 0,'',aAuxH4[var][2])),'RA_NOME')
	EndIf
	
Return .T.
//===========================================================================================================
// Metodo que envia o email
WSMETHOD InfRh4C WSRECEIVE CodRh4,FILRH3 WSSEND _SlCrSl WSSERVICE W0500307
	
	Local aAuxH4 := {}
	Local aAux   := {}
	Local nCnt   := 0
	Local nPos   := 0
	Local var    := 0
	Local cTipo  := ""
	Local oSolicita
	
	aAuxH4 := Rh4Inf(::CodRh4,::FILRH3)
	aAux   := Rh4InfR(::CodRh4,::FILRH3)
	
	If Len(aAuxH4) > 0
		::_SlCrSl:TMPFUNCAO := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_FUNCAO"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_FUNC := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_FUNC"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPSALATU := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_SALATU"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPTABELA := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_TABELA"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPNIVELT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_NIVELT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPFAIXAT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_FAIXAT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RASALARIO := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_SALARIO"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RATIPOALT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_TIPOALT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_MOT  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_MOT"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPVLSALA := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_VLSALA"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPPERCSA := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_PERCSA"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RACODFUNC := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_CODFUNC"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_F_PP := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_F_PP"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPH_M_AT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_H_M_AT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPH_S_AT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_H_S_AT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPH_D_AT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_H_D_AT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAHRSMES  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_HRSMES"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAHRSEMAN := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_HRSEMAN"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAHRSDIA  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_HRSDIA"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPTURNAT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_TURNAT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_TURN := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_TURN"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPS_TURN := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_S_TURN"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPREGRA  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_REGRA"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:RATNOTRAB := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_TNOTRAB"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPDESCTU := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_DESCTU"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RASEQTURN := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_SEQTURN"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAREGRA   := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_REGRA"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPFILIAL := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_FILIAL"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_FILI := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_FILI"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPCCATU  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_CCATU"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_CCAT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_CCAT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPDEPTOA := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_DEPTOA"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_DEPT := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_DEPT"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAFILIAL  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_FILIAL"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPN_F_D  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_N_F_D"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:RACC      := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_CC"}) ) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_CC_D := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_CC_D"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RADEPTO   := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_DEPTO"})) = 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPD_DEPD := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_D_DEPD"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPPROCES := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_PROCES"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAPROCES  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_PROCES"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPPOSTO  := IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_POSTO"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAPOSTO 	:= IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_POSTO"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPCLVL  	:= IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_CLVL"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RACLVL  	:= IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_CLVL"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:TMPITEM 	:= IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "TMP_ITEM"}))�= 0,'',aAuxH4[var][2])
		::_SlCrSl:RAITEM  	:= IIF((var := ASCAN(aAuxH4,{|x,y|  Alltrim(x[1]) == "RA_ITEM"}))�= 0,'',aAuxH4[var][2])		
	EndIf
	
	If Len(aAux) > 0
		For var:= 1 To Len(aAux)
			cTipo += ALLTRIM(aAux[var][1][2])
		Next
		::_SlCrSl:TMPTIPO   := cTipo
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} Rh4Inf()

@Author     Henrique Madureira
@Since
@Version    P12.7
@project    MAN00000463301_EF_003
@Param      CodSol, Codigo da solicita��o
@Return     aAux
*/
Static Function Rh4Inf(CodSol,FILRH3)
	
	Local cQuery 	:= ''
	Local cAliRh4	:= 'RH4INF'
	Local aAux		:= {}
	
	cQuery := "SELECT RH4_CAMPO, RH4_VALNOV "
	cQuery += "FROM	" + RetSqlName("RH4") + " "
	cQuery += "WHERE RH4_CODIGO = '" + CodSol + "' "
	cQuery += "		AND RH4_FILIAL = '" + FILRH3 + "' "
	cQuery += "      AND D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH4_ITEM "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRh4)
	
	DbSelectArea(cAliRh4)
	While ! (cAliRh4)->(EOF())
		AADD(aAux, {(cAliRh4)->RH4_CAMPO,(cAliRh4)->RH4_VALNOV})
		(cAliRh4)->(DbSkip())
	End
	(cAliRh4)->(DbCloseArea())
	
Return aAux

/*
{Protheus.doc} Rh4InfR()

@Author     Henrique Madureira
@Since
@Version    P12.7
@project    MAN00000463301_EF_003
@Param      CodSol, Codigo da solicita��o
@Return     aAux
*/
Static Function Rh4InfR(CodSol,FILRH3)
	
	Local cQuery 	:= ''
	Local cAliRh4	:= 'RH4INFH'
	Local aAux		:= {}
	Local aReg    := {}
	
	cQuery := "SELECT RH4_CAMPO, RH4_VALNOV "
	cQuery += "FROM	" + RetSqlName("RH4") + " "
	cQuery += "WHERE RH4_CODIGO = '" + CodSol + "' "
	cQuery += "		AND RH4_FILIAL = '" + FILRH3 + "' "
	cQuery += "      AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRh4)
	
	DbSelectArea(cAliRh4)
	While ! (cAliRh4)->(EOF())
		
		If ALLTRIM((cAliRh4)->RH4_CAMPO) = "TMP_TIPO" .AND. LEN(aAux) > 1
			AADD(aReg,aAux)
			aAux := {}
		EndIf
		
		AADD(aAux, {(cAliRh4)->RH4_CAMPO,(cAliRh4)->RH4_VALNOV})
		
		(cAliRh4)->(DbSkip())
	End
	AADD(aReg,aAux)
	(cAliRh4)->(DbCloseArea())
	
Return aReg

//===========================================================================================================
// Metodo que envia o email
WSMETHOD BuscaTrm WSRECEIVE CodRh4,FILRH3 WSSEND _Treina WSSERVICE W0500307
	
	Local aAuxH4 := {}
	Local nCnt   := 0
	Local var := 0
	Local oSolicita
	
	aAuxH4 := Rh4Inf(::CodRh4,::FILRH3)
	
	If Len(aAuxH4) > 0
		for var:= 1 to Len(aAuxH4)
			If ASCAN(aAuxH4[var], "RA3_CALEND")�!= 0
				::_Treina:CALEND  := aAuxH4[var][2]
			EndIf
			If ASCAN(aAuxH4[var], "RA3_CURSO")�!= 0
				::_Treina:CURSO   := aAuxH4[var][2]
			EndIf
			If ASCAN(aAuxH4[var], "TMP_NOME")�!= 0
				::_Treina:NOME    := aAuxH4[var][2]
			EndIf
		next
		
	EndIf
	
Return .T.
//===========================================================================================================
WsMethod InfSolVg WsReceive CodRh3,FILRH3 WsSend _InfRh3 WsService W0500307
	
	Local aAux := {}
	
	aAux := RetInfRh3(::CodRh3,::FILRH3)
	
	If Len(aAux) > 0
		
		::_InfRh3:RH3VISAO   := Alltrim(aAux[1])
		::_InfRh3:RH3FILINI  := Alltrim(aAux[2])
		::_InfRh3:RH3MATINI  := Alltrim(aAux[3])
		::_InfRh3:RH3FILAPR  := Alltrim(aAux[4])
		::_InfRh3:RH3MATAPR  := Alltrim(aAux[5])
		
	EndIf
	
Return .T.
//===========================================================================================================
Static Function RetInfRh3(cCodSol,FILRH3)
	
	Local cQuery 	:= ''
	Local cAliRh3	:= 'RHPA3INF'
	Local aAux		:= {}
	
	cQuery := "SELECT RH3_VISAO, RH3_FILINI, RH3_MATINI,RH3_FILAPR, RH3_MATAPR "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_CODIGO = '" + cCodSol + "' "
	cQuery += "		AND RH3_FILIAL = '" + FILRH3 + "' " 
	cQuery += "		AND D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRh3)
	
	DbSelectArea(cAliRh3)
	While !(cAliRh3)->(EOF())
		AADD(aAux, (cAliRh3)->RH3_VISAO)
		AADD(aAux, (cAliRh3)->RH3_FILINI)
		AADD(aAux, (cAliRh3)->RH3_MATINI)
		AADD(aAux, (cAliRh3)->RH3_FILAPR)
		AADD(aAux, (cAliRh3)->RH3_MATAPR)
		(cAliRh3)->(DbSkip())
	End
	(cAliRh3)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
WsMethod InfPegSup WsReceive _SuperInf WsSend _SupInfP WsService W0500307
	
	Local aAux := {}
	
	aAux := PegSuper(Self:_SuperInf)
	
	If Len(aAux) > 0
		::_SupInfP:QBMATRESP := aAux[1]
		::_SupInfP:RANOME    := aAux[2]
		::_SupInfP:RAEMAIL   := aAux[3]
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} PegSuper()
Pega o superior do funcionario
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, matricula do aprovador
@Param      cSolicit, codigo da solicita��o
@Param      cLocal, tipo da solicita��o
@Return     aAux
*/
Static Function PegSuper(oSolicVag)
	
	Local nCont     := 1
	Local cQuery    := ''
	Local cAliasAi8 := 'RETSRA'
	Local aAux      := {}
	Local cMatApr   := ""
	Local cFilApr   := ""
	Local cEmlApr   := ""
	
	cMatApr := oSolicVag:CodMaSup
	cFilApr := oSolicVag:FilMaSup
	cNomApr := POSICIONE("SRA",1,cFilApr + cMatApr,"RA_NOME")
	cEmlApr := POSICIONE("SRA",1,cFilApr + cMatApr,"RA_EMAIL")
	cSolicit:= oSolicVag:Solici
	cFilRh3 := oSolicVag:FilSolic
	
	AADD(aAux,cMatApr)
	AADD(aAux,cNomApr)
	AADD(aAux,cEmlApr)
	
	If !(EMPTY(cMatApr))
		RH3->(DbSetOrder(1))
		If RH3->(DbSeek(cFilRh3 + cSolicit))
			Reclock("RH3", .F.)
			RH3->RH3_FILAPR := cFilApr
			RH3->RH3_MATAPR := cMatApr
			RH3->(MsUnlock())
		EndIf
		
		cTpSol := POSICIONE("RH3", 1, cFilRh3 + cSolicit,"RH3_XTPCTM")
		
		DO CASE
		CASE cTpSol = '004'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"006")
			EndIf
		CASE cTpSol = '005'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"044")
			EndIf
		CASE cTpSol = '006'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"064")
			EndIf
		ENDCASE
	Else
		RH3->(DbSetOrder(1))
		If RH3->(DbSeek(cFilRh3 + cSolicit))
			Reclock("RH3", .F.)
			RH3->RH3_STATUS	:= "4"
			RH3->(MsUnlock())
		EndIf
				
		cTpSol := POSICIONE("RH3", 1, cFilRh3 + cSolicit,"RH3_XTPCTM")
		
		DO CASE
		CASE cTpSol = '004'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"027")
			EndIf
		CASE cTpSol = '005'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"046")
			EndIf
						
			//----------------------------------------------
			// Grava Log PA6 - Adic. por Edu em 16/12/16	 //
			//----------------------------------------------
			If Len(GetFuncArray('U_F0600601')) > 0
				U_F0600601(RH3->RH3_FILIAL,RH3->RH3_CODIGO,RH3->(Recno()))
			EndIf
			
		CASE cTpSol = '006'
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"063")
			EndIf
		ENDCASE
	EndIf
	
Return aAux
