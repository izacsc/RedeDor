#Include 'Protheus.ch'

/*
{Protheus.doc} F0100404()
Libera��o de documento
@Author     Mick William da Silva
@Since      05/05/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
*/
User Function F0100404()

	Local aAreas     := {SCR->(GetArea()),SF1->(GetArea()), SD1->(GetArea()), SC7->(GetArea()), GetArea()}
	
	Local cNumPedido 	:= Alltrim(PARAMIXB[1])
	Local cFilDoc    	:= SCR->CR_FILIAL
	Local cTipo		:= Alltrim(PARAMIXB[2])
	Local cOperac		:= Alltrim(PARAMIXB[3])	
	Local cChave     	:= ""
	Local cUFOri    	:= ""
	Local nOpc			:= 0
	Local lRet			:= .T.
	
	Private aCabec      := {}
	Private aItens      := {}
	Private aLinha      := {}
	Private lMsErroAuto := .F.

	If SCR->(DbSeek(FWxFilial("SCR")+"PC"+cNumPedido))
		While AllTrim(SCR->(CR_TIPO+CR_NUM)) == AllTrim("PC"+cNumPedido)
			If SCR->CR_STATUS == '01' .Or. SCR->CR_STATUS == '02'
				lRet := .F.
			Endif
			SCR->(DbSkip())
		EndDo
	Else
		lRet := .F.
	Endif
	
	SC7->(DBSetOrder(1))

	If lRet .And. SC7->(MSSeek(cFilDoc + cNumPedido))
	
		cChave := cFilDoc + cNumPedido
		cUFOri := Posicione("SA2", 1, xFilial("SA2") + SC7->C7_FORNECE + SC7->C7_LOJA, "A2_EST")

		aCabec := {{'F1_TIPO', 'N'				, NIL},;
			{'F1_FORMUL'	, ''				, NIL},;
			{'F1_DOC'		, SC7->C7_XDOC		, NIL},;
			{'F1_SERIE'		, SC7->C7_XPREF		, NIL},;
			{'F1_EMISSAO'	, SC7->C7_XDTEMI	, NIL},;
			{'F1_FORNECE'	, SC7->C7_FORNECE	, NIL},;
			{'F1_LOJA'		, SC7->C7_LOJA		, NIL},;
			{'F1_EST'		, cUFOri			, NIL},;
			{'F1_COND'		, SC7->C7_COND		, NIL},;
			{'F1_XUSR'		, SC7->C7_XUSR		, NIL},;
			{'F1_ORIGEM'	, SC7->C7_ORIGEM	, NIL},;
			{'F1_ESPECIE'	, SC7->C7_XESPECI	, NIL}}
				
		While SC7->(!Eof()) .AND. ( SC7->(C7_FILIAL + C7_NUM) == cChave )
	
			aLinha := {{'D1_COD', SC7->C7_PRODUTO	, NIL},;
				{'D1_UM'		, SC7->C7_UM		, NIL},;
				{'D1_QUANT'		, SC7->C7_QUANT		, NIL},;
				{'D1_VUNIT'		, SC7->C7_PRECO		, NIL},;
				{'D1_TOTAL'		, SC7->C7_TOTAL		, NIL},;
				{'D1_VALDESC'	, SC7->C7_VLDESC	, NIL},;
				{'D1_DESPESA'	, SC7->C7_DESPESA	, NIL},;
				{'D1_LOCAL'		, SC7->C7_LOCAL		, NIL},;
				{'D1_CC'		, SC7->C7_CC		, NIL},;
				{'D1_PEDIDO'	, SC7->C7_NUM		, NIL},;
				{'D1_ITEMPC'	, SC7->C7_ITEM		, NIL},;
				{'D1_QTDPEDI'	, SC7->C7_QUANT		, NIL},;
				{'D1_XNATURE'	, SC7->C7_XNATURE	, NIL},;
				{'D1_XPRIVEN'	, SC7->C7_XDTVEN	, NIL}}
	
			AAdd(aItens, AClone(aLinha))
			
			SC7->(DbSkip())
			
		EndDo
	
		MsgRun("Aguarde, gerando Pr�-Nota de Entrada...", , {|| MSExecAuto( {|x,y,z| MATA140(x,y,z) }, aCabec, aItens, 3 )})

		If lMsErroAuto
			nOpc := Aviso("Aten��o","Ocorreu um erro nomento da gera��o da Pr�-Nota de entrada. Voc� pode estornar essa libera��o ou continuar o processo, tornando-se necess�rio a inclus�o manual da Nota Fiscal.",{"Estornar Lib","Confirmar"},2)
			If nOpc == 1
				A097Estorna()
			Else
				MostraErro()
			Endif
		EndIf
	
	Else
		Help("", 1, "Erro", , "Pr�-Nota de Entrada n�o gerado.", 1, 0)
	EndIf


	AEval(aAreas, {|x| RestArea(x)})
	
Return
