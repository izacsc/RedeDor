#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'

Static lBtApRp := .T.

/*
{Protheus.doc} F0100323()
Atendimento das solicita��es customizadas
@Author     Bruno de Oliveira
@Since      25/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
*/
User Function F0100323()
	
	oMBrowse := FWMBrowse():New()
	oMBrowse:SetAlias("RH3")
	oMBrowse:SetOnlyFields({'RH3_FILIAL','RH3_CODIGO','RH3_NOME','RH3_XDSCTM','RH3_DTSOLI','RH3_DTATEN'} )
	oMBrowse:AddLegend('RH3->RH3_STATUS=="1"',  "YELLOW", "Solicitado")
	oMBrowse:AddLegend('RH3->RH3_STATUS=="2"',  "GREEN"	, "Aprovado")
	oMBrowse:AddLegend('RH3->RH3_STATUS=="4"',  "BLUE"	, "Aguardando Efetivacao RH")
	oMBrowse:AddLegend('RH3->RH3_STATUS=="3"',  "RED"	, "Rejeitado")
	
	oMBrowse:SetFilterDefault('!EMPTY(RH3_XTPCTM)')
	oMBrowse:SetCacheView( .F. )
	oMBrowse:Activate()
	
Return

/*
{Protheus.doc} MenuDef()
Cria��o do Menu de atendimento de solicita��es
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return		aRotina, op��o de menu
*/
Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina TITLE "Atender" ACTION "VIEWDEF.F0100323" OPERATION 4 ACCESS 0
	
Return aRotina

/*
{Protheus.doc} ModelDef()
Modelo de dados do atendimento de solicita��es
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return		oModel, Modelo de dados
*/
Static Function ModelDef()
	
	Local oModel
	Local oStrRH3 := FwFormStruct(1,"RH3")
	Local oStrRH4 := FWFormStruct(1, "RH4")
	Local bCancel := {|oModel| FCancAtend(oModel) }
	Local bPreMd  := {|| lBtApRp := .T.  }
	
	oModel := MPFormModel():New("M01003",  bPreMd , /*bPosMd*/ , /*bCommitMd*/, bCancel )
	oModel:SetDescription("Atendimento das solicita��es")
	
	oStrRH3:SetProperty( "RH3_MAT", MODEL_FIELD_OBRIGAT, .F. )
	oStrRH3:SetProperty( "RH3_TIPO", MODEL_FIELD_OBRIGAT, .F. )
	
	oModel:AddFields("RH3MASTER", /*cOwner*/, oStrRH3)
	
	oModel:AddGrid(	"RH4DETAIL","RH3MASTER",oStrRH4)
	
	oModel:SetPrimaryKey({"RH3_CODIGO"})
	
	oModel:GetModel("RH4DETAIL"):SetUniqueLine({"RH4_ITEM"}) //Diz ao model que o campo deve ser validado quanto a repeticao
	
	oModel:SetRelation("RH4DETAIL",{{"RH4_FILIAL","xFilial('RH4')"},{"RH4_CODIGO", "RH3_CODIGO"}}, "RH4_FILIAL+RH4_CODIGO+STR(RH4_ITEM,3)")
	
Return oModel

/*
{Protheus.doc} ViewDef()
Interface do atendimento de solicita��es
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param		oView, interface
*/
Static Function ViewDef()
	
	Local oModel := FWLoadModel('F0100323')
	Local oStrRH3 := FWFormStruct(2, "RH3")
	Local oStrRH4 := FWFormStruct(2, "RH4")
	Local oView	:= FWFormView():New()
	
	If RH3->RH3_STATUS == '4'
		oView:AddUserButton("Aprovar", "", {|oModel| If(FsVldStat(),FsAprovSol(oModel),.F.) })
		oView:AddUserButton("Reprovar","", {|oModel| If(FsVldStat(),FsReproSol(oModel),.F.) })
	EndIf
	
	oStrRH3:RemoveField("RH3_FILIAL")
	oStrRH3:RemoveField("RH3_ORIGEM")
	oStrRh3:RemoveField("RH3_TIPO")
	oStrRH3:RemoveField("RH3_TPDESC")
	oStrRH3:RemoveField("RH3_VISAO")
	oStrRH3:RemoveField("RH3_NVLINI")
	oStrRH3:RemoveField("RH3_FILINI")
	oStrRH3:RemoveField("RH3_MATINI")
	oStrRH3:RemoveField("RH3_NVLAPR")
	oStrRH3:RemoveField("RH3_FILAPR")
	oStrRH3:RemoveField("RH3_MATAPR")
	oStrRh3:RemoveField("RH3_FLUIG")
	oStrRH3:RemoveField("RH3_WFID")
	oStrRH3:RemoveField("RH3_IDENT")
	oStrRH3:RemoveField("RH3_KEYINI")
	oStrRH3:RemoveField("RH3_EMP")
	oStrRH3:RemoveField("RH3_EMPINI")
	oStrRH3:RemoveField("RH3_EMPAPR")
	oStrRH3:RemoveField("RH3_XSUSPE")
	
	oStrRH4:RemoveField("RH4_FILIAL")
	oStrRH4:RemoveField("RH4_CAMPO")
	oStrRH4:RemoveField("RH4_CODIGO")
	oStrRH4:RemoveField("RH4_VALANT")
	oStrRH4:RemoveField("RH4_XOBS")
	
	oView:SetModel(oModel)
	oView:AddField("VIEW_RH3", oStrRH3, 'RH3MASTER')
	
	oView:AddGrid("VIEW_RH4", oStrRH4,"RH4DETAIL")
	oView:CreateHorizontalBox("TOP", 40)
	oView:CreateHorizontalBox("BOTTOM", 60)
	
	oView:SetOwnerView("VIEW_RH3", "TOP")
	oView:SetOwnerView("VIEW_RH4", "BOTTOM")
	
Return oView

/*
{Protheus.doc} FsVldStat()
Valida��o do campo Status
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return		lRet, .T. Permite continuar e .F. n�o permite
*/
Static Function FsVldStat
	
	Local lRet := .T.
	
	If RH3->RH3_STATUS == "2" .OR. RH3->RH3_STATUS == "3"
		MsgAlert("A solicita��o ja foi atendida!","Aten��o!")
		lRet := .F.
	EndIf
	
	If RH3->RH3_STATUS == "1"
		MsgAlert("A solicita��o ainda esta em processo de aprova��o!", "Aten��o!")
		lRet := .F.
	EndIf
	
Return lRet

/*
{Protheus.doc} FsAprovSol()
Aprova��o da solicita��o
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param		oModel, objeto, Modelo de dados da solicita��o
*/
Static Function FsAprovSol(oModel)
	
	Local oModel := FWModelActive()
	Local cTipo := oModel:GetModel("RH3MASTER"):GetValue("RH3_XTPCTM")
	Local cFilSolic	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_FILIAL")
	Local cCodSolic := oModel:GetModel("RH3MASTER"):GetValue("RH3_CODIGO")
	Local cFilMatri := oModel:GetModel("RH3MASTER"):GetValue("RH3_FILINI")
	Local cCodMatri := oModel:GetModel("RH3MASTER"):GetValue("RH3_MATINI")
	Local lPosto := .T.
	Local cDirExec := GetMv("FS_DIREXE")
	Local cRhDedic := GetMv("FS_RHDED")
	Local cDescVg := ""
	
	If MsgYesNo("Confirma a aprovacao da solicitacao?","Aten��o")
		
		If cTipo == "001" //Solicita��o de Treinamento/Evento
			
			cTFilial := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",1)) //RA3_FILIAL
			cTMat    := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",2)) //RA3_MAT
			cTCalend := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",4)) //RA3_CALEND
			cTCurso  := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",5)) //RA3_CURSO
			cTTurma  := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",6)) //RA3_TURMA
			cTData   := CTOD(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",7)) //RA3_DATA
			
			//Verifica disponibilidade de vagas
			cAliasQry := "F0200306"
			BeginSql alias cAliasQry
				SELECT *
				FROM %table:RA2% RA2
				WHERE RA2.RA2_CALEND = %exp:cTCalend% AND
				RA2.RA2_CURSO  = %exp:cTCurso%  AND
				RA2.RA2_TURMA  = %exp:cTTurma%  AND
				RA2.%notDel%
			EndSql
			
			If !(cAliasQry)->(EOF())
				If !((cAliasQry)->RA2_VAGAS > (cAliasQry)->RA2_RESERV)
					(cAliasQry)->( DbCloseArea() )
					
					MsgAlert("Nao existem mais vagas disponiveis para esse treinamento!","Aten��o!")
					Return .F.
				EndIf
			Else
				(cAliasQry)->( DbCloseArea() )
				
				MsgAlert("Treinamento da solicitacao nao localizado!" ,"Aten��o!")
				Return .F.
			EndIf
			(cAliasQry)->( DbCloseArea() )
			
			//Atualiza treinamento RA3
			DbSelectArea("RA3")
			RecLock("RA3", .T.)
			RA3->RA3_FILIAL := xFilial("RA3", cTFilial)
			RA3->RA3_MAT    := cTMat
			RA3->RA3_CALEND := cTCalend
			RA3->RA3_CURSO  := cTCurso
			RA3->RA3_TURMA  := cTTurma
			RA3->RA3_DATA   := cTData
			RA3->RA3_RESERV := "S"
			RA3->(MsUnlock())
			
			// Atualiza reservas calendario RA2
			dbSelectArea("RA2")
			RA2->(DbSetOrder(1))
			IF RA2->(DbSeek((xFilial("RA2",cTFilial)+ RTRIM(cTCalend) + RTRIM(cTCurso) + RTRIM(cTTurma))))
				RecLock("RA2", .F.)
				RA2->RA2_RESERV := (RA2->RA2_RESERV + 1)
				RA2->(MsUnlock())
			EndIf
			
			//Atualiza status solicitacao
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
			//Alterado 24/01/2017
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"072")//Atendido Treinamento Externo
			EndIf
            	
		ElseIf cTipo == "002" //Solicita��o de Vagas
			
			cDepto := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",4))
			cPosto := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",5))
			cFilPost := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",6))
			
			DbSelectArea("RCL")
			RCL->(DbSetOrder(1)) //RCL_FILIAL+RCL_DEPTO+RCL_POSTO
			If RCL->(DbSeek(cFilPost+cDepto+cPosto))
				If (RCL->RCL_NPOSTO - RCL->RCL_OPOSTO) <= 0
					lPosto := .F.
				EndIf
			EndIf
			
			If lPosto
				
				cVgDisp := GetSX8Num("SQS","QS_VAGA")
				
				cValSal := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",13))
				
				cValSal := StrTran( cValSal, ".", "" )
				cValSal := StrTran( cValSal, ",", "." )
				
				cDescVg := Posicione("SRJ",1,xFilial("SRJ") + Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",8)),"RJ_DESC")
				
				DbSelectArea("SQS")
				RecLock("SQS",.T.)
				SQS->QS_FILIAL  := xFilial("SQS")
				SQS->QS_VAGA    := cVgDisp
				SQS->QS_DESCRIC := cDescVg
				SQS->QS_POSTO   := cPosto
				SQS->QS_FILPOST := cFilPost
				SQS->QS_CC      := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",7))
				SQS->QS_FUNCAO  := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",8))
				SQS->QS_SOLICIT := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",9))
				SQS->QS_VCUSTO  := VAL(cValSal)
				SQS->QS_TIPO    := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",15))
				SQS->QS_NRVAGA  := 1
				SQS->QS_DTABERT := DATE()
				SQS->QS_FILRESP := cFilMatri
				SQS->QS_MATRESP := cCodMatri
				SQS->QS_XSTATUS := "1"
				SQS->QS_XJORN   := VAL(Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",18)))
				SQS->QS_XSOLFIL := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",1))
				SQS->QS_XSOLPTL := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",16))
				SQS->QS_XOBSERV := Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_XOBS",23))
				SQS->QS_XPUBLIC := "2"
				MSMM(,,,Alltrim(oModel:GetModel("RH4DETAIL"):GetValue("RH4_XOBS",23)),1,,,"SQS","QS_CODPERF")
				SQS->(MsUnLock())
								
				//Atualiza status solicitacao
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
				
				If !Empty(cDirExec) .AND. !Empty(cRhDedic)
					cEmail := cDirExec+";"+cRhDedic
					cBody := "Solicita��o de Vaga aprovada, Vaga " + cVgDisp + " dispon�vel para recrutamento"
					U_F0200304("Solicita��o Vagas", cBody,cEmail)
				EndIf
				//Alterado 24/01/2017
				If FindFunction("U_F0500201")
					U_F0500201(cFilSolic,cCodSolic,"030")//EM ADMISSAO
				EndIf
				
			Else
				
				MsgAlert("Nao existe mais posto disponivel para essa vaga!","Aten��o!")
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "003" //Solicita��o de Aumento de Quadro\Or�amento
			
			// Atualiza RBT - // Busca RH4
			
			cPCC := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",4)
			cPDepto := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",2)
			cPFunc := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",6)
			cPCarg := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",8)
			nPSalar := Val(oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",10))
			cPTpost := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",11)
			cPTcont := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",12)
			cPQtdM := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",13)
			cCodPost := oModel:GetModel("RH4DETAIL"):GetValue("RH4_VALNOV",15)
			
			Begin Transaction				
				
				DbSelectArea("RBT")
				RecLock("RBT", .T.)
				RBT->RBT_FILIAL := xFilial("RBT")
				RBT->RBT_CODMOV	:= GetSx8Num("RBT", "RBT_CODMOV")
				RBT->RBT_DTAMOV := dDataBase
				RBT->RBT_CC     := cPCC
				RBT->RBT_DEPTO  := cPDepto
				RBT->RBT_FUNCAO := cPFunc
				RBT->RBT_CARGO  := cPCarg
				RBT->RBT_REMUNE := nPSalar
				RBT->RBT_TPOSTO := cPTpost
				RBT->RBT_TPCONT := cPTcont
				RBT->RBT_QTDMOV := VAL(cPQtdM)
				RBT->RBT_CODPOS := cCodPost
				RBT->RBT_JUSTIF := "02"
				RBT->RBT_TIPOR  := "2"
				RBT->RBT_STATUS := "1"
				RBT->(MsUnLock())
				
				
			End Transaction
			
			// Atualizando RBX - Registro de movimentacao do departamento
			DbSelectArea("RBX")
			RecLock("RBX",.T.)
			RBX->RBX_FILIAL	:=	xFilial("RBX")
			RBX->RBX_DEPTO 	:=	cPDepto
			RBX->RBX_CODMOV :=	RBT->RBT_CODMOV
			RBX->RBX_DTAMOV :=	RBT->RBT_DTAMOV
			RBX->RBX_CODOPE :=	"1"
			RBX->RBX_TPOSTO :=	cPTpost
			RBX->RBX_QTDOPE	:=	VAL(cPQtdM)
			RBX->RBX_RESOPE	:=	cUsername
			RBX->RBX_JUSTIF :=  "01"
			RBX->(MsUnLock())
			
			//Valida tipo de posto se a solicitacao for de atualizacao de posto que ja existe
            If !Empty(RBT->RBT_CODPOS) .And. AllTrim(RBT->RBT_CODPOS) != "Novo"
                If !( FsVldTPos( RBT->RBT_FILIAL, RBT->RBT_CODPOS ) )
                    Return .F.          
                EndIf           
            EndIf
			
			// Atualiza Status da Solicitacao
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
			//Efetua gravacao de postos/historico
			OrgXAprova( RBT->RBT_FILIAL, RBT->RBT_CODMOV, RBX->RBX_CODOPE, RBX->RBX_QTDOPE, RBX->RBX_JUSTIF, .F., .T. )
			
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"024")//Atendido Posto
			EndIf
			
			//Else se for or�amento
			
			//EndIf
			
		ElseIf cTipo == "004" //Solicita��o da FAP
			
			If U_F0500302(cFilSolic,cCodSolic,"AP")
				
				//Atualiza status solicitacao
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			Else
				
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "005" //Solicita��o de Desligamento
			
			If U_F0500104(oModel)
				
				//Atualiza status solicitacao
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			Else
				
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "006" //Solicita��o Cargos e Salarios
			
			if U_F0500402(oModel)
				
				//Atualiza status solicitacao
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			Else
				
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "007" //Incentivo Academico
			
			//Atualiza status solicitacao
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "2")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			//Alterado 24/01/2017
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"080")//Atendido Incentivo Academico
			EndIf
		EndIf
		
		If oModel:VldData()
			oModel:CommitData()
			MsgInfo("Solicitacao aprovada","Aten��o")
			lBtApRp := .F.
		Else
			MsgAlert(oModel:GetErrorMessage()[6],"Atencao!")
		EndIf
		
		
		
	EndIf
	
Return

/*
{Protheus.doc} FsReproSol()
Reprova��o da solicita��o
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param		oModel, objeto, Modelo de dados da solicita��o
*/
Static Function FsReproSol(oModel)
	
	Local oModel := FWModelActive()
	Local cTipo 	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_XTPCTM")
	Local cFilSolic := oModel:GetModel("RH3MASTER"):GetValue("RH3_FILIAL")
	Local cCodSolic := oModel:GetModel("RH3MASTER"):GetValue("RH3_CODIGO")
	Local cVisSolic	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_VISAO")
	Local cMatSolic	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_MAT")
	
	If MsgNoYes("Confirma a Reprova��o da solicita��o?","Aten��o")
	
		If cTipo == "001" //Treinamento/Evento
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"076")//Reprovado Posto
			EndIf
			
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
		ElseIf cTipo == "002" //Solicita��o de Vagas
		
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"014")//Reprovado Vaga
			EndIf
			
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
		ElseIf cTipo == "003" //Solicita��o de Aumento de Quadro
			
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"025")//Reprovado Posto
			EndIf
			
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
		ElseIf cTipo == "004" //Solicita��o da FAP
			
			If U_F0500302(cFilSolic,cCodSolic,"RE")
				
				//Atualiza status solicitacao
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
				
			Else
				
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "005" //Solicita��o de Desligamento
			
			If U_F0500110(cCodSolic,"3")
				
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
				//Alterado 26/01/2017
				If FindFunction("U_F0500201")
					U_F0500201(cFilSolic,cCodSolic,"045")//Reprovado solicita��o de desligamento
				EndIf
			Else
				
				Return .F.
				
			EndIf
			
		ElseIf cTipo == "006" //Solicita��o de Cargos e Salarios
			
			If U_F0500407(cFilSolic,cMatSolic,cCodSolic,cVisSolic)
				
				oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
				oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
				
			Else
				
				Return .F.
				
			EndIf
		
		ElseIf cTipo == "007" //Incentivo Academico
			If FindFunction("U_F0500201")
				U_F0500201(cFilSolic,cCodSolic,"081")//Reprovado Incentivo Academico
			EndIf
			
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
		Else
			
			oModel:GetModel("RH3MASTER"):SetValue("RH3_STATUS", "3")
			oModel:GetModel("RH3MASTER"):SetValue("RH3_DTATEN", dDataBase)
			
		EndIf
		
		
		If oModel:VldData()
			oModel:CommitData()
			//--------------------------------------------
			// Gravar PA6 da Reprovacao do Desligamento //
			//--------------------------------------------
			If cTipo == "005" //Solicita��o de Desligamento
				U_F0600601(RH3->RH3_FILIAL, "RH3", RH3->RH3_CODIGO, RH3->(Recno()), "DELETE")
			Endif
			MsgInfo("Solicita��o reprovada","Aten��o")
			lBtApRp := .F.
		Else
			MsgAlert(oModel:GetErrorMessage()[6],"Atencao!")
		EndIf
		
	EndIf
	
Return

/*
{Protheus.doc} FCancAtend()
Valida��o do cancelamento do modelo
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param		oModel, objeto, Modelo de dados da solicita��o
*/
Static Function FCancAtend(oModel)
	
	If !lBtApRp
		MsgAlert("Foi realizado opera��o de aprova��o ou reprova��o da solicita��o, n�o � permitido cancelar","Aten��o!")
	EndIf
	
Return lBtApRp

/*
{Protheus.doc} FsVldTPos()
Valida��o do tipo de posto
@Author     Bruno de Oliveira
@Since      26/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param		cFilRCL, caracter, Filial do posto
@Param		cCodPos, caracter, C�digo do posto
@Return		lRet
*/
Static Function FsVldTPos(cFilRCL,cCodPos)

	Local aArea := GetArea()
	Local lRet  := .T.
	
	DbSelectArea("RCL")
	DbSetOrder(RetOrder("RCL", "RCL_FILIAL+RCL_POSTO"))
	RCL->(DbSeek(cFilRCL + cCodPos))
	
	If RCL->RCL_TPOSTO == "1"
	    If !MsgYesNo( "O Posto Origem � Individual, ap�s a aprova��o ele se tornar� Gen�rico!" + CRLF + "Deseja continuar?" , "Aten��o" )
	        lRet := .F.
	    EndIf
	EndIf
	
	Restarea(aArea)

Return lRet
