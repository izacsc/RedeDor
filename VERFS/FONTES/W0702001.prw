#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"

/*
{Protheus.doc} W0702001()
Webservice responsavel pela inclus�o/altera��o no Cadastro de Clientes
@Author     Bruno de Oliveira
@Since		20/01/2017
@Version    P12.1.7
@Project    MAN0000007423041_EF_020
*/
User Function W0702001()
Return
//==================================================================================================================
WSSTRUCT RegClient
	WSDATA cFILREG  AS String	//Filial
	WSDATA cCOD     As String	//C�digo Cliente
	WSDATA cLOJA    AS String	//Loja do Cliente
	WSDATA cNOME    AS String	//Nome
	WSDATA cNREDUZ  AS String   //Nome Fantasia
	WSDATA cPESSOA  AS String	//Tipo Pessoa Fisica/Juridica
	WSDATA cEND     AS String	//Endere�o
	WSDATA cCOMPLEM AS String	//Complemento do Endere�o
	WSDATA cBAIRRO  AS String	//Bairro
	WSDATA cTIPO    AS String	//Tipo do Cliente
	WSDATA cEST     AS String	//Estado
	WSDATA cCEP     AS String	//Cep
	WSDATA cCOD_MUN AS String	//C�digo Municipio
	WSDATA cCGC     AS String	//CNPJ/CPF do cliente
	WSDATA cPFISICA AS String	//RG ou Passaporte
	WSDATA cPAIS    AS String	//Pais
	WSDATA cCODPAIS AS String	//Codigo Bacen
	WSDATA cEMAIL   AS String	//E-mail
	WSDATA cXOPCM   AS String	//Operadora (Sim/N�o)
ENDWSSTRUCT

WSSERVICE W0702001 DESCRIPTION "WebService Server para inclus�o/altera��o no cadastro de clientes"

	WSDATA Cliente  AS RegClient
	WSDATA cRetorno AS String
	
	WSMETHOD UPSERTCLIENTE DESCRIPTION "Inclus�o ou altera��o no Cadastro de Clientes"

ENDWSSERVICE

WSMETHOD UPSERTCLIENTE WSRECEIVE Cliente WSSEND cRetorno WSSERVICE W0702001

	::cRetorno := U_F0702001(Self:Cliente)

RETURN(.T.)