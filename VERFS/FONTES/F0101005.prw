#Include 'Protheus.ch'

/*
{Protheus.doc} F0101005()
PE FA750BRW Para adicionar o bot�o recusa nas rotinas FINA750 
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Return		aRet, Vetor de Botoes    
*/
User Function F0101005()

Local aRet := {}
	
aAdd(aRet, {'Recusar'     ,"U_F010101A",0,3})
aAdd(aRet, {'Hist. Recusa','U_F010101G',0,3})

Return aRet