#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0700003
Monitor Integra��o Log de Sa�da
@author Alex Sandro 
@since 27/12/2016
@Project MAN0000007423041_EF_000 - Log de Sa�da
/*/
User Function F0700003()
	Local oBrowse := FWMBrowse():New()
	
	oBrowse:SetAlias('P20')
	oBrowse:SetDescription('Monitor Integra��o Log de Sa�da')
	oBrowse:Activate()
Return

Static Function MenuDef()
	Local aRotina := {}
	
	ADD OPTION aRotina Title 'Visualizar' 	Action 'VIEWDEF.F0700003' OPERATION 2 ACCESS 0

Return aRotina

Static Function ModelDef()
	Local oStruMod 	:= FWFormStruct(1,'P20')
	Local oModel	:= MPFormModel():New('M0700003') //Model com 7 caracteres

	oModel:AddFields('MASTER',, oStruMod)
	oModel:SetPrimaryKey({})
	oModel:SetDescription('Monitor Integra��o Log de Sa�da')
	oModel:GetModel('MASTER'):SetDescription('Monitor Integra��o Log de Sa�da')
Return oModel

Static Function ViewDef()
	Local oModel 	:= FWLoadModel('F0700003')
	Local oStruView := FWFormStruct(2,'P20')
	Local oView		:= FWFormView():New()

	oView:SetModel(oModel)
	oView:AddField('VIEW_MASTER', oStruView, 'MASTER')
	oView:CreateHorizontalBox('SUPERIOR', 100 )
	oView:SetOwnerView('VIEW_MASTER', 'SUPERIOR')
Return oView
