#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'   

/*{Protheus.doc} F0100409()
Altera��o de Grupo de Aprova��o
@Author     Nairan Alves Silva
@Since      14/09/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Menu		*/
User Function F0100409()

Local aArea	:= GetArea()
Local aAreaP02:= P02->(GetArea())
Local aAreaSC7:= SC7->(GetArea())
Local aAreaSY1:= SY1->(GetArea())
Local xRet		:= Nil
Local cTipo	:= ""

DbSelectArea("SC7")
cTipo := SC7->C7_XTIPO

DbSelectArea("P02")
SAL->(DbSetOrder(1))

If P02->(DbSeek(xFilial('P02')+cTipo))
	dbSelectArea("SY1")
	dbSetOrder(3)
	If SY1->(DbSeek(xFilial("SY1")+P02->P02_COMPRA))
		xRet := SY1->Y1_GRAPROV
	EndIF
Endif	

RestArea(aAreaP02)
RestArea(aAreaSC7)
RestArea(aAreaSY1)
RestArea(aArea)
Return xRet