#Include 'Protheus.ch'
#Include 'TopConn.ch'
#Include 'FWMVCDef.ch'

/*
{Protheus.doc} F0200701()
Relat�rio de Lista Cr�tica em Excel
@Author     Paulo Kr�ger
@Since      01/09/2016
@Version    P12.7
@Project    MAN00000463301_EF_007      
@Return     
*/

User Function F0200701()

	Local oReport
	Private aMaiorCons	:= {}
	Private aMaxO6Q6		:= {}
	Private aMaxO6V6		:= {}
	Private cAnlCrit		:= ''
	Private cAnLsCr    	:= ''
	Private cAnPrvVenc	:= ''
	Private cCalculo   	:= ''
	Private nCobALM    	:= 0
	Private nCobFAR    	:= 0
	Private nCobUni    	:= 0
	Private cFreq      	:= ''
	Private cGrupam    	:= ''
	Private cMatSubs		:= ''
	Private cNivCritLC 	:= ''
	Private cNivCritPV 	:= ''
	Private dDTBASE		:= CTOD('  /  /  ')
	Private dDtA 			:= CTOD('  /  /  ')
	Private dDt7 			:= CTOD('  /  /  ')
	Private dDt15 		:= CTOD('  /  /  ')
	Private dDt30 		:= CTOD('  /  /  ')
	Private dDt60 		:= CTOD('  /  /  ')
	Private dDt90 		:= CTOD('  /  /  ')
	Private dDt120 		:= CTOD('  /  /  ')
	Private dDt150 		:= CTOD('  /  /  ')
	Private dDt180 		:= CTOD('  /  /  ')
	Private nCD30xCD7  	:= 0
	Private nCD90xCD30 	:= 0
	Private nCusmed		:= 0
	Private nDiasAtraz	:= 0
	Private nEstSeg    	:= 0
	Private nLeadTime  	:= ''
	Private nLenFil		:= 0
	Private nMaiorCons	:= 0
	Private nMaxO6Q6		:= 0
	Private nMaxO6V6		:= 0
	Private nQtdCD07		:= 0
	Private nQtdCD120		:= 0
	Private nQtdCD15		:= 0
	Private nQtdCD150		:= 0
	Private nQtdCD180		:= 0
	Private nQtdCD30		:= 0
	Private nQtdCD60		:= 0
	Private nQtdCD90		:= 0
	Private nQtdCDA		:= 0
	Private nSalALM    	:= 0
	Private nSalAlmTot	:= 0
	Private nSalARS    	:= 0
	Private nSalFAR    	:= 0
	Private nSArmExt   	:= 0
	Private nTamFil		:= 0
	Private nCobARS		:= 0
	Private nCobTot		:= 0
	Private cAnPreVenc 	:= ''
	Private nVlFatMin		:= 0
	Private cIndNvServ	:= ''
	Private cAnalista  	:= ''

	If TRepInUse()
		/*===========================================================================|
		|Par�metros:                                                                 |
		|============================================================================|
		|mv_par01| Tipo de Relat�rio         | 1 - Lista Cr�tica.                    |
		|        |                           | 2 - Manuten��o Lista Cr�tica.         |
		|        |                           | 3 - Previs�o Vencida.                 |
		|--------|-------------------------------------------------------------------|
		|mv_par02| Data de Refer�ncia        | Inicializador padr�o: dDataBase.      |                      
		|--------|-------------------------------------------------------------------|
       |mv_par03| Produto                   | Consulta padr�o SB1.                  |
		|--------|-------------------------------------------------------------------|
       |mv_par04| Categoria                 | Consulta padr�o 	ACU.                 |
		|--------|-------------------------------------------------------------------|
       |mv_par05| Categoria controlada pela |                                       |
       |        | equipe de planejamento    | 1 = Sim, 2 = N�o                      |
		|--------|-------------------------------------------------------------------|
       |mv_par06| Filial                    | Inicializador padr�o: filial corrente.|                
		|===========================================================================*/

		Pergunte('FS0200701',.F.)
		
		MV_PAR02 := dDataBase       

		Pergunte('FS0200701',.T.)
		
		If Empty(MV_PAR02)
			MsgAlert('N�o foi poss�vel gerar o relat�rio. Informe a Data de Refer�ncia.')
			Return
		ElseIf Empty(MV_PAR05)
			MsgAlert('N�o foi possivel gerar o relat�rio. Defina Sim ou N�o em Categoria controlada pela equipe de planejamento.')
		Else
			dDTBASE	:= MV_PAR02
			dDtA 		:= dDTBASE - 1
			dDt7 		:= dDTBASE - 7
			dDt15 		:= dDTBASE - 15
			dDt30 		:= dDTBASE - 30
			dDt60 		:= dDTBASE - 60
			dDt90 		:= dDTBASE - 90
			dDt120 	:= dDTBASE - 120
			dDt150 	:= dDTBASE - 150
			dDt180 	:= dDTBASE - 180
		
			oReport := ReportDef()
			oReport :PrintDialog()
		EndIf
	EndIf
Return

Static Function ReportDef()
	Local oReport
	Local oSection
	Local oBreak

	oReport 	:= TReport():New('FSW01007XX','Relatorio de Lista Critica','FS0200701',;
									{|oReport| PrintReport(oReport)},"Relatorio de Lista Critica")
	oSection	:= TRSection():New(oReport,'Compras',{	'cAlias01','cAlias02','cAlias03','cAlias04',;
																'cAlias05','cAlias06','cAlias07','cAlias08',;
		                                             		'cAlias09','cAlias10','cAlias11'})
	TRCell():New(oSection,'FILIAL'		,'cAlias01','Estabelecimento'			,,,,{||(FWFilialName(,cAlias01->FILIAL))},,,,,,,,,.t.)
	TRCell():New(oSection,'PRODUTO'		,'cAlias01','Material'					,,,,{||(cAlias01->PRODUTO)      			},,,,,,,,,.t.)
	TRCell():New(oSection,'DESCRICAO'	,'cAlias01','Descricao'					,,,,{||(cAlias01->DESCRICAO)    			},,,,,,,,,.t.)
	TRCell():New(oSection,'CATEGORIA'	,'cAlias01','Segmento'					,,,,{||(cAlias01->CATEGORIA)    			},,,,,,,,,.t.)
	TRCell():New(oSection,'EMISSAO'		,'cAlias01','Dt. Emissao'				,,,,{||(STOD(cAlias01->EMISSAO))			},,,,,,,,,.t.)
	TRCell():New(oSection,'NUMPCSC'		,'cAlias01','Ordem Compra'				,,,,{||(cAlias01->NUMPCSC)      			},,,,,,,,,.t.)
	TRCell():New(oSection,'FORNECE'		,'cAlias01','Fornecedor OC'				,,,,{||(cAlias01->FORNECE)      			},,,,,,,,,.t.)
	TRCell():New(oSection,'STATUS'		,'cAlias01','OC Aprovada'				,,,,{||(cAlias01->STATUS)       			},,,,,,,,,.t.)
	TRCell():New(oSection,'QUANTIDADE'	,'cAlias01','QT Pedido'					,,,,{||(Transform(cAlias01->QUANTIDADE,'@E 9,999,999.99'))		},,,,,,,,,.t.)
	TRCell():New(oSection,'ENTREGA'		,'cAlias01','Dt. Entrega'				,,,,{||(STOD(cAlias01->ENTREGA))			},,,,,,,,,.t.)
	TRCell():New(oSection,'COMPRAD'		,'cAlias01','Comprador OC'				,,,,{||(cAlias01->COMPRAD)      			},,,,,,,,,.t.)
	TRCell():New(oSection,"nQtdPedAb"	,"cAlias11","Ordens total"				,,,,{||(Transform(nQtdPedab					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
//	TRCell():New(oSection,'nSalAlmTot'	,''			,'Saldo Almox.'				,,,,{||(Transform(nSalAlmTot				,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nSalALM'		,''			,'Saldo. Almox'				,,,,{||(Transform(nSalALM					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCDA'		,'cAlias02','CDA'							,,,,{||(Transform(cAlias02->QTDCDA			,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD07'    ,'cAlias03','CD07'						,,,,{||(Transform(cAlias03->QTDCD07		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD15'		,'cAlias04','CD15'						,,,,{||(Transform(cAlias04->QTDCD15		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD30'		,'cAlias05','CD30'						,,,,{||(Transform(cAlias05->QTDCD30		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD60'		,'cAlias06','CD60'						,,,,{||(Transform(cAlias06->QTDCD60		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD90'		,'cAlias07','CD90'						,,,,{||(Transform(cAlias07->QTDCD90		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD120'	,'cAlias08','CD120'						,,,,{||(Transform(cAlias08->QTDCD120		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD150'	,'cAlias09','CD150'						,,,,{||(Transform(cAlias09->QTDCD150		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'QTDCD180'	,'cAlias10','CD180'						,,,,{||(Transform(cAlias10->QTDCD180		,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nSalFAR'		,''			,'Saldo Farmacias'			,,,,{||(Transform(nSalFAR					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nCobTot'		,''			,'Cobertura Total'			,,,,{||(If(nCobTot == 0, 'Sem Cobertura',Transform(nCobTot,'@E 9,999,999.99')))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nMaiorCons'	,''			,'Maximo'						,,,,{||(Transform(nMaxO6Q6  ,'@E 9,999,999.99')) },,,,,,,,,.t.)
	TRCell():New(oSection,'cCalculo'	,''			,'Calculo'           		,,,,{||(cCalculo)								},,,,,,,,,.t.)
	TRCell():New(oSection,'nCobARS'		,''			,'Cobertura Arsenais'		,,,,{||(If(nCobARS == 0, 'Sem Cobertura',Transform(nCobARS,'@E 9,999,999.99')))	},,,,,,,,,.t.)
	TRCell():New(oSection,'cGrupam'		,''			,'Grupamento'					,,,,{||(cAlias01->GRUPAMEN)					},,,,,,,,,.t.)
	TRCell():New(oSection,'nEstSeg'		,''			,'Est. Seguran�a'				,,,,{||(Transform(nEstSeg					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'cAnLsCr'    ,''			,'Anl. Lista Critica'		,,,,{||(cAnLsCr)								},,,,,,,,,.t.)
	TRCell():New(oSection,'cNivCritLC' ,''			,'Nivel Critic. Manut. LC'	,,,,{||(cNivCritLC)							},,,,,,,,,.t.)
	TRCell():New(oSection,'nCD30xCD7'	,''			,'CD30xCD7'					,,,,{||(Transform(nCD30xCD7					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nCD90xCD30'	,''			,'CD90xCD30'					,,,,{||(Transform(nCD90xCD30				,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nSalARS'		,''			,'Estoque Arsenal'			,,,,{||(Transform(nSalARS					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nDiasAtraz'	,''			,'Dias Atraso'				,,,,{||(Transform(nDiasAtraz				,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'nLeadTime'	,''			,'Lead Time'					,,,,{||(Transform(nLeadTime					,'@E 9,999,999.99'))	},,,,,,,,,.t.)
	TRCell():New(oSection,'cAnPreVenc'	,''			,'Analisar Previsao Vencida',,,,{||(cAnPrvVenc)							},,,,,,,,,.t.)
	TRCell():New(oSection,'cNivCritPV'	,''			,'Nivel de criticidade PV'	,,,,{||(cNivCritPV)							},,,,,,,,,.t.)
	TRCell():New(oSection,'cMatSubs'	,''			,'Materiais substitutos'		,,,,{||(cMatSubs)          					},,,,,,,,,.t.)
	TRCell():New(oSection,'cAnalista'	,'cAlias01','Analista'			   		,,,,{||cAlias01->COMPRAD 					},,,,,,,,,.t.)
	TRCell():New(oSection,'nCusMed'		,''			 ,'Custo medio'				,,,,{||nCusMed								},,,,,,,,,.t.)
	TRCell():New(oSection,'cUM'			,'cAlias01','Unidade compra'			,,,,{||cAlias01->UNIDMED 					},,,,,,,,,.t.)
	TRCell():New(oSection,'nVlFatMin'	,''			,'Faturamento Minimo'		,,,,{||(Transform(nVlFatMin					,'@E 9,999,999.99'))			},,,,,,,,,.t.)
	TRCell():New(oSection,'cIndNvServ'	,''			,'Indicador N�vel Serv'		,,,,{||(cIndNvServ)							},,,,,,,,,.t.)

Return oReport

Static Function PrintReport(oReport)
	Local oSection 	:= oReport:Section(1)
	Local aArea		:= GetArea()
	Local aAreaSA2	:= SA2->(GetArea())
	Local cFSALM		:= SuperGetMv('FS_ALM') 
	Local cFSFAR		:= SuperGetMv('FS_FAR')
	Local cFSARS		:= SuperGetMv('FS_ARS')
	Local cFSARMEX	:= SuperGetMv('FS_ARMEXT')
	Local cQuery01	:= ''
  	Local cAlias01	:= ''
	Local cAlias02	:= GetNextAlias()
	Local cAlias03	:= GetNextAlias()
	Local cAlias04	:= GetNextAlias()
	Local cAlias05	:= GetNextAlias()
	Local cAlias06	:= GetNextAlias()
	Local cAlias07	:= GetNextAlias()
	Local cAlias08	:= GetNextAlias()
	Local cAlias09	:= GetNextAlias()
	Local cAlias10	:= GetNextAlias()
	Local cAlias11	:= GetNextAlias()
	Local cNumPC		:= ''
	Local cResiduo	:= ''
	Local cEncerr		:= 'E'
	Local 	:= '1'
	Local cBloqueado	:= '2'
	Local aCompFil	:= {}
	Local aCalcEst	:= {}

	oSection:Init()
	oReport:IncMeter()
	
	/*===========================================================================|
	|Define regras de compara��o de filiais.                                     |
	|===========================================================================*/
	
	aCompFil := U_F0200702({	{'ACV','SD3'},;
                            {'ACU','SD3'},; 		
                            {'SB1','SD3'},; 		
                            {'SBZ','SD3'},; 		
                            {'SB2','SD3'},; 		
                            {'SC7','SD3'},; 		
                            {'SC1','SD3'},;
                            {'ACV','ACU'},;
                            {'SA2','SD3'}},cEmpAnt, cFilAnt)

	cQuery01 +=	"SELECT DISTINCT	SD3.D3_FILIAL		AS FILIAL		, " + CRLF 
	cQuery01 +=	"					SD3.D3_LOCAL		AS LOCAL		, " + CRLF				
	cQuery01 +=	"					SC7.C7_NUM      	AS NUMPCSC		, " + CRLF
	cQuery01 +=	"					SC7.C7_ITEM		AS ITPCSC		, " + CRLF
	cQuery01 +=	"					SC7.C7_EMISSAO	AS EMISSAO		, " + CRLF
	cQuery01 +=	"					CASE WHEN SC7.C7_ACCPROC <> '1' AND SC7.C7_CONAPRO = 'B' AND SC7.C7_QUJE < SC7.C7_QUANT THEN 'BLOQUEADO' ELSE 'LIBERADO' END AS STATUS , " + CRLF
	cQuery01 +=	"					SC7.C7_QUANT		AS QUANTIDADE	, " + CRLF
	cQuery01 +=	"					SC7.C7_QUANT - SC7.C7_QUJE AS SALDO , " + CRLF
	cQuery01 +=	"					SC7.C7_DATPRF		AS ENTREGA		, " + CRLF
	cQuery01 +=	"					SC7.C7_COMPRA		AS COMPRAD		, " + CRLF
	cQuery01 +=	"					SA2.A2_NREDUZ		AS FORNECE		, " + CRLF
	cQuery01 +=	"					SD3.D3_COD			AS PRODUTO		, " + CRLF
	cQuery01 +=	"					SB1.B1_DESC		AS DESCRICAO	, " + CRLF
	cQuery01 +=	"					SB1.B1_UM			AS UNIDMED		, " + CRLF
	cQuery01 +=	"					SB1.B1_ALTER		AS ALTERNAT	, " + CRLF
	cQuery01 +=	"					SBZ.BZ_EMAX		AS ESTQMAX		, " + CRLF
	cQuery01 +=	"					SBZ.BZ_XFREQAN	AS FREQ		, " + CRLF
	cQuery01 +=	"					SB1.B1_GRUPCOM	AS GRPCOMPR	, " + CRLF
	cQuery01 +=	"					SB1.B1_GRUPO		AS GRUPAMEN	, " + CRLF
	cQuery01 +=	"					ACU.ACU_CODPAI	AS CODPAI		, " + CRLF
	cQuery01 +=	"					ACV.ACV_CATEGO	AS CATEGORIA	, " + CRLF  
	cQuery01 +=	"					ACU.ACU_XESTSE	AS ESTSEG		, " + CRLF  
	cQuery01 +=	"					ACU.ACU_XPE		AS PE " + CRLF
	cQuery01 +=	"FROM " + RETSQLNAME('SD3') + " SD3	INNER JOIN " + RETSQLNAME('SB1') + " SB1 ON " + aCompFil[03] + CRLF
	cQuery01 +=	"															AND		SB1.B1_COD     =  SD3.D3_COD " + CRLF
	cQuery01 +=	"							INNER JOIN " + RETSQLNAME('ACV') + " ACV ON " +  aCompFil[01] + CRLF
	cQuery01 +=	"															AND		ACV.ACV_CODPRO =  SD3.D3_COD " + CRLF
	cQuery01 +=	"							INNER JOIN " + RETSQLNAME('ACU') + " ACU ON " + aCompFil[02] + CRLF
	cQuery01 +=	"															AND		ACU.ACU_COD    =  ACV.ACV_CATEGO " + CRLF
	cQuery01 +=	"      													AND 	ACU.ACU_CTRLP  =  '" + ALLTRIM(STR(mv_par05)) + "' " + CRLF
	cQuery01 +=	"      													AND 	ACU.ACU_MSBLQL =  '" + cBloqueado+ "' " + CRLF
	cQuery01 +=	"							LEFT  JOIN " + RETSQLNAME('SBZ') + " SBZ ON " + aCompFil[04] + CRLF
	cQuery01 +=	"															AND 	SBZ.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"															AND		SBZ.BZ_COD     =  SD3.D3_COD " + CRLF 
	cQuery01 +=	"							INNER  JOIN " + RETSQLNAME('SC7') + " SC7 ON " + aCompFil[06]  + CRLF
	cQuery01 +=	"															AND		SC7.C7_PRODUTO =  SD3.D3_COD " + CRLF       
	cQuery01 +=	"															AND		SC7.C7_LOCAL   =  SD3.D3_LOCAL " + CRLF    
	cQuery01 +=	"															AND		SC7.C7_QUJE    <  SC7.C7_QUANT " + CRLF     
	cQuery01 +=	"															AND		SC7.C7_RESIDUO =  '" + cResiduo + "' " + CRLF   
	cQuery01 +=	"															AND		SC7.C7_ENCER   <> '" + cEncerr  + "' " + CRLF    
	cQuery01 +=	"      													AND 	SC7.C7_NUM     <> '" + cNumPC    + "' " + CRLF
	cQuery01 +=	"															AND		SC7.D_E_L_E_T_ =  '' " + CRLF
	cQuery01 +=	"							LEFT  JOIN " + RETSQLNAME('SA2') + " SA2 ON " + aCompFil[09] + CRLF
	cQuery01 +=	"						 									AND		SA2.A2_COD		 =	SC7.C7_FORNECE " + CRLF
	cQuery01 +=	"						 									AND		SA2.A2_LOJA	 =	SC7.C7_LOJA " + CRLF			  							 
	cQuery01 +=	"															AND		SA2.D_E_L_E_T_ =  '' " + CRLF
	cQuery01 +=	"WHERE     SD3.D3_EMISSAO <= '" + DTOS(mv_par02)  + "' " + CRLF
	If !Empty(mv_par03)
		cQuery01 +=	"      AND SD3.D3_COD      =  '" + mv_par03  + "' " + CRLF
	EndIf
	If !Empty(mv_par04)
		cQuery01 +=	"      AND ACV.ACV_CATEGO  =  '" + mv_par04  + "' " + CRLF
	EndIf
	cQuery01 +=	"		AND	ACV.D_E_L_E_T_ = '' " + CRLF                                                   	
	cQuery01 +=	"		AND	ACU.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"		AND	ACU.ACU_CTRLP	 = '" + ALLTRIM(STR(mv_par05))  + "' " + CRLF
	cQuery01 +=	"      AND	SD3.D3_FILIAL  = '" + mv_par06  + "' " + CRLF
	cQuery01 +=	"      AND	SD3.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"      AND	SB1.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"UNION " + CRLF
	cQuery01 +=	"SELECT DISTINCT SD3.D3_FILIAL              AS FILIAL, " + CRLF 
	cQuery01 +=	"                SD3.D3_LOCAL               AS LOCAL, " + CRLF 
	cQuery01 +=	"                SC1.C1_NUM                 AS NUMPCSC, " + CRLF 
	cQuery01 +=	"                SC1.C1_ITEM                AS ITPCSC, " + CRLF 
	cQuery01 +=	"                SC1.C1_EMISSAO             AS EMISSAO, " + CRLF 
	cQuery01 +=	"					CASE WHEN SC1.C1_APROV = 'B' THEN 'BLOQUEADO' ELSE 'LIBERADO' END AS STATUS , " + CRLF
	cQuery01 +=	"                SC1.C1_QUANT               AS QUANTIDADE, " + CRLF 
	cQuery01 +=	"                SC1.C1_QUANT - SC1.C1_QUJE AS SALDO, " + CRLF 
	cQuery01 +=	"                SC1.C1_DATPRF              AS ENTREGA, " + CRLF 
	cQuery01 +=	"                SC1.C1_CODCOMP             AS COMPRAD, " + CRLF 
	cQuery01 +=	"                SA2.A2_NREDUZ              AS FORNECE, " + CRLF 
	cQuery01 +=	"                SD3.D3_COD                 AS PRODUTO, " + CRLF 
	cQuery01 +=	"                SB1.B1_DESC                AS DESCRICAO, " + CRLF 
	cQuery01 +=	"                SB1.B1_UM                  AS UNIDMED, " + CRLF 
	cQuery01 +=	"                SB1.B1_ALTER               AS ALTERNAT, " + CRLF 
	cQuery01 +=	"                SBZ.BZ_EMAX                AS ESTQMAX, " + CRLF 
	cQuery01 +=	"                SBZ.BZ_XFREQAN             AS FREQ, " + CRLF 
	cQuery01 +=	"                SB1.B1_GRUPCOM             AS GRPCOMPR, " + CRLF 
	cQuery01 +=	"                SB1.B1_GRUPO               AS GRUPAMEN, " + CRLF 
	cQuery01 +=	"                ACU.ACU_CODPAI             AS CODPAI, " + CRLF 
	cQuery01 +=	"                ACV.ACV_CATEGO             AS CATEGORIA, " + CRLF 
	cQuery01 +=	"                ACU.ACU_XESTSE             AS ESTSEG, " + CRLF 
	cQuery01 +=	"                ACU.ACU_XPE                AS PE " + CRLF 
	cQuery01 +=	"FROM " + RETSQLNAME('SD3') + " SD3	INNER JOIN " + RETSQLNAME('SB1') + " SB1 ON " + aCompFil[03] + CRLF
	cQuery01 +=	"															AND		SB1.B1_COD     =  SD3.D3_COD " + CRLF
	cQuery01 +=	"							INNER JOIN " + RETSQLNAME('ACV') + " ACV ON " +  aCompFil[01] + CRLF
	cQuery01 +=	"															AND		ACV.ACV_CODPRO =  SD3.D3_COD " + CRLF
	cQuery01 +=	"							INNER JOIN " + RETSQLNAME('ACU') + " ACU ON " + aCompFil[02] + CRLF
	cQuery01 +=	"															AND		ACU.ACU_COD    =  ACV.ACV_CATEGO " + CRLF
	cQuery01 +=	"      													AND 	ACU.ACU_CTRLP  =  '" + ALLTRIM(STR(mv_par05)) + "' " + CRLF
	cQuery01 +=	"      													AND 	ACU.ACU_MSBLQL =  '" + cBloqueado+ "' " + CRLF
	cQuery01 +=	"							LEFT  JOIN " + RETSQLNAME('SBZ') + " SBZ ON " + aCompFil[04] + CRLF
	cQuery01 +=	"															AND 	SBZ.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"															AND		SBZ.BZ_COD     =  SD3.D3_COD " + CRLF 
	cQuery01 +=	"							LEFT  JOIN " + RETSQLNAME('SC1') + " SC1 ON " + aCompFil[07]  + CRLF
	cQuery01 +=	"															AND		SC1.C1_PRODUTO =  SD3.D3_COD " + CRLF       
	cQuery01 +=	"															AND		SC1.C1_LOCAL   =  SD3.D3_LOCAL " + CRLF    
	cQuery01 +=	"															AND		SC1.D_E_L_E_T_ =  '' " + CRLF
	cQuery01 +=	"															AND		SC1.C1_XITMED NOT LIKE ('%GCT%') " + CRLF	
	cQuery01 +=	"							LEFT  JOIN " + RETSQLNAME('SA2') + " SA2 ON " + aCompFil[09] + CRLF
	cQuery01 +=	"						 									AND		SA2.A2_COD		 =	SC1.C1_FORNECE " + CRLF
	cQuery01 +=	"						 									AND		SA2.A2_LOJA	 =	SC1.C1_LOJA " + CRLF			  							 
	cQuery01 +=	"															AND		SA2.D_E_L_E_T_ =  '' " + CRLF
	cQuery01 +=	"WHERE     SD3.D3_EMISSAO <= '" + DTOS(mv_par02)  + "' " + CRLF
	If !Empty(mv_par03)
		cQuery01 +=	"      AND SD3.D3_COD      =  '" + mv_par03  + "' " + CRLF
	EndIf
	If !Empty(mv_par04)
		cQuery01 +=	"      AND ACV.ACV_CATEGO  =  '" + mv_par04  + "' " + CRLF
	EndIf
	cQuery01 +=	"		AND	ACV.D_E_L_E_T_ = '' " + CRLF                                                   	
	cQuery01 +=	"		AND	ACU.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"		AND	ACU.ACU_CTRLP	 = '" + ALLTRIM(STR(mv_par05))  + "' " + CRLF
	cQuery01 +=	"      AND	SD3.D3_FILIAL  = '" + mv_par06  + "' " + CRLF
	cQuery01 +=	"      AND	SD3.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"      AND	SB1.D_E_L_E_T_ = '' " + CRLF
	cQuery01 +=	"		AND SC1.C1_PEDIDO = ' ' " + CRLF	

	cQuery01 := ChangeQuery(cQuery01)
	
	If Select('cAlias01') > 0
		cAlias01->(dBCloseArea())
	EndIf
	
	dbUseArea( .T., 'TOPCONN', TcGenQry(,,cQuery01), 'cAlias01', .F., .T. )
	dBSelectArea('cAlias01')
	
	cAlias01->(dbGoTop())
	While !cAlias01->(Eof())
	
		nLenFil	:= Len(AllTrim(cAlias01->FILIAL))
		cAnalista	:= cAlias01->COMPRAD
		/*==============================================================|
		| MATERIAL SUBSTITUTO.                                          |
		|==============================================================*/
		If !Empty(cAlias01->ALTERNAT)
			cMatSubs := cAlias01->ALTERNAT + ' - ' + Posicione('SB1',01,xFilial('SB1') + cAlias01->ALTERNAT,'B1_DESC')
		EndIf
		/*==============================================================|
		| SALDO DO ARMAZEM DO TIPO ALMOXARIFADO.                        |
		|==============================================================*/
		If SB2->( dbSeek(cAlias01->FILIAL + cAlias01->PRODUTO + cFSALM) )
			aCalcEst	:= CalcEst(cAlias01->PRODUTO,cFSALM,mv_par02)
			If !Empty(aCalcEst)
				nSalALM	:= aCalcEst[01]
				nCusMed	:= aCalcEst[02]
				ASIZE(aCalcEst,0)
			EndIf
		EndIf
	
		/*==============================================================|
		| SALDO DO ARMAZEM DO TIPO FARMACIA.                            |
		|==============================================================*/
		If SB2->( dbSeek(cAlias01->FILIAL + cAlias01->PRODUTO + cFSFAR) )
			nSalFAR	:= CalcEst(cAlias01->PRODUTO,cFSFAR,mv_par02)[01]
		EndIf
		/*==============================================================|
		| SALDO DO ARMAZEM DO TIPO ARSENAL.                             |
		|==============================================================*/
		If SB2->( dbSeek(cAlias01->FILIAL + cAlias01->PRODUTO + cFSARS) )
			nSalARS	:= CalcEst(cAlias01->PRODUTO,cFSARS,mv_par02)[01]
		EndIf
		/*==============================================================|
		| SALDO DO ARMAZEM EXTERNO.                                     |
		|==============================================================*/
		If SB2->( dbSeek(cAlias01->FILIAL + cAlias01->PRODUTO + cFSARMEX) )
			nSArmExt := CalcEst(cAlias01->PRODUTO,cFSARMEX,mv_par02)[01]
		EndIf
		/*==============================================================|
		| SALDO TOTAL DE ARMAZEM.                                       |
		|==============================================================*/
		nSalAlmTot := nSalALM + nSalFAR + nSalARS + nSArmExt 
		/*==============================================================|
		| CDA  - CONSUMO DO DIA ANTERIOR.                               |
		|==============================================================*/
		If Select('cAlias02') > 0
			cAlias02->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias02'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END)  AS QTDCDA
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO =	%exp:DTOS(dDtA)%
		EndSql
		
		If !cAlias02->(Eof())
			nQtdCDA	:=	cAlias02->QTDCDA
			AADD(aMaiorCons, cAlias02->QTDCDA)
		EndIf	
		/*==============================================================|
		| CD07 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 7 DIAS.            |
		|==============================================================*/
		If Select('cAlias03') > 0
			cAlias03->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias03'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 7  AS QTDCD07
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt7)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias03->(Eof())
			nQtdCD07	:=	cAlias03->QTDCD07
			AADD(aMaiorCons , cAlias03->QTDCD07)
			AADD(aMaxO6Q6,cAlias03->QTDCD07)
			AADD(aMaxO6V6,cAlias03->QTDCD07)
		EndIf	
		/*==============================================================|
		| CD15 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 15 DIAS.           |
		|==============================================================*/
		If Select('cAlias04') > 0
			cAlias04->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias04'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 15 AS QTDCD15
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt15)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias04->(Eof())
			nQtdCD15	:=	cAlias04->QTDCD15
			AADD(aMaiorCons, cAlias04->QTDCD15)
			AADD(aMaxO6Q6,cAlias04->QTDCD15)
			AADD(aMaxO6V6,cAlias04->QTDCD15)
		EndIf	
		/*==============================================================|
		| CD30 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 30 DIAS.           |
		|==============================================================*/
		If Select('cAlias05') > 0
			cAlias05->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias05'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 30 AS QTDCD30
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt30)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias05->(Eof())
			nQtdCD30	:=	cAlias05->QTDCD30
			AADD(aMaiorCons, cAlias05->QTDCD30)
			AADD(aMaxO6Q6,cAlias05->QTDCD30)
			AADD(aMaxO6V6,cAlias05->QTDCD30)
		EndIf
		/*==============================================================|
		| MAIOR VALOR APRESENTADO NO INTERVALO CD7 / CD30               |
		|==============================================================*/
		If !Empty(aMaxO6Q6)
			ASORT(aMaxO6Q6,,, { |x, y| x > y })
			nMaxO6Q6	:= aMaxO6Q6[01]
			ASIZE(aMaxO6Q6,0)
			aMaxO6Q6 := {}
		EndIf
		/*==============================================================|
		| CD60 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 60 DIAS.           |
		|==============================================================*/
		If Select('cAlias06') > 0
			cAlias06->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias06'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 60 AS QTDCD60
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt60)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias06->(Eof())
			nQtdCD60	:=	cAlias06->QTDCD60
			AADD(aMaiorCons,cAlias06->QTDCD60)
			AADD(aMaxO6V6  ,cAlias06->QTDCD60)
		EndIf
		/*==============================================================|
		| CD90 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 90 DIAS.           |
		|==============================================================*/
		If Select('cAlias07') > 0
			cAlias07->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias07'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 90 AS QTDCD90
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt90)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias07->(Eof())
			nQtdCD90	:=	cAlias07->QTDCD90
			AADD(aMaiorCons,cAlias07->QTDCD90)
			AADD(aMaxO6V6  ,cAlias07->QTDCD90)
		EndIf
		/*==============================================================|
		| CD120 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 120 DIAS.         |
		|==============================================================*/
		If Select('cAlias08') > 0
			cAlias08->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias08'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 120 AS QTDCD120
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt120)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)%
		EndSql 
		If !cAlias08->(Eof())
			nQtdCD120	:=	cAlias08->QTDCD120
			AADD(aMaiorCons,cAlias08->QTDCD120)
			AADD(aMaxO6V6  ,cAlias08->QTDCD120)
		EndIf
		/*==============================================================|
		| CD150 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 150 DIAS.         |
		|==============================================================*/
		If Select('cAlias09') > 0
			cAlias09->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias09'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 150 AS QTDCD150
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt150)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias09->(Eof())
			nQtdCD150	:=	cAlias09->QTDCD150
			AADD(aMaiorCons,cAlias09->QTDCD150)
			AADD(aMaxO6V6  ,cAlias09->QTDCD150)
		EndIf
		/*==============================================================|
		| CD180 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 180 DIAS.         |
		|==============================================================*/
		If Select('cAlias10') > 0
			cAlias10->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias10'
		SELECT SUM(CASE WHEN SUBSTR(D3_CF,01,01) = 'D' THEN (SD3.D3_QUANT * (-1)) ELSE SD3.D3_QUANT END) / 180 AS  QTDCD180
		FROM	%table:SD3% SD3
		WHERE			SD3.%notDel%
				AND		SD3.D3_FILIAL	 =	%exp:mv_par06%
				AND		SD3.D3_COD		 =	%exp:cAlias01->PRODUTO%
				AND		SD3.D3_EMISSAO >=	%exp:DTOS(dDt180)% 
				AND		SD3.D3_EMISSAO <=	%exp:DTOS(dDTBASE)% 
		EndSql
		If !cAlias10->(Eof())
			nQtdCD180	:=	cAlias10->QTDCD180
			AADD(aMaiorCons,cAlias10->QTDCD180)
			AADD(aMaxO6V6  ,cAlias10->QTDCD180)
		EndIf
		/*==============================================================|
		| MAIOR VALOR APRESENTADO NO INTERVALO CD7 / CD180              |
		|==============================================================*/
		If !Empty(aMaxO6V6)
			ASORT(aMaxO6V6,,, { |x, y| x > y })
			nMaxO6V6	:= aMaxO6V6[01]
			ASIZE(aMaxO6V6,0)
			aMaxO6V6 := {}
		EndIf
		/*==============================================================|
		| Maior consumo 3M - MAIOR VALOR APRESENTADO NOS CD'S  APURADOS.|
		|==============================================================*/
		If !Empty(aMaiorCons)
			ASORT(aMaiorCons,,, { |x, y| x > y })
			nMaiorCons	:= aMaiorCons[01]
			ASIZE(aMaiorCons,0)
			aMaiorCons := {}
		EndIf
		/*==============================================================|
		| COBERTURA TOTAL.                                              |
		|==============================================================*/
		If nMaiorCons <= 0
			nCobTot := 0
		Else
			If nSalALM <= 0
				nCobTot := 0
			Else
				If nMaxO6Q6 > 0
					nCobTot := nSalALM / nMaxO6Q6
				Else
					nCobTot := nSalALM / nMaiorCons
				EndIf
			EndIf
		EndIf
		/*==============================================================|
		| TOTAL PED. COMPRA / SOLIC. DE COMPRA                          |
		|==============================================================*/
		If Select('cAlias11') > 0
			cAlias11->(dBCloseArea())
		EndIf
		BeginSql Alias 'cAlias11'
		SELECT SUM(C7_QUANT-C7_QUJE) QTDPEDAB
		FROM	%table:SC7% SC7
		WHERE			SC7.%notDel%
				AND		SUBSTR(SC7.C7_FILIAL,01,%exp:nLenFil%) = %exp:cAlias01->FILIAL%	
				AND		SC7.C7_RESIDUO	=	' '
				AND		SC7.C7_ENCER		<>	'E'
				AND		C7_QUANT-C7_QUJE > 0
				AND		SC7.C7_PRODUTO	=	%exp:cAlias01->PRODUTO%
		EndSql
		/*
				UNION
		SELECT SUM(C1_QUANT) QTDPEDAB
		FROM	%table:SC1% SC1
		WHERE			SC1.%notDel%
				AND		SUBSTR(SC1.C1_FILIAL,01,%exp:nLenFil%) = %exp:cAlias01->FILIAL%	
				AND		SC1.C1_RESIDUO	=	' '
				AND		SC1.C1_QUANT		>	SC1.C1_QUJE
				AND		SC1.C1_PRODUTO	=	%exp:cAlias01->PRODUTO%
				*/
		If !cAlias11->(Eof())
			nQtdPedab	:= cAlias11->QTDPEDAB
		EndIf
		/*==============================================================|
		| LEAD TIME, FREQUENCIA E ESTOQUE DE SEGURAN�A.                 |
		|==============================================================*/
		nEstSeg	:=	cAlias01->ESTSEG
		nLeadTime	:=	cAlias01->PE
		cFreq		:=	cAlias01->FREQ

		/*==============================================================|
		| ANALISE DE PAR�METROS                                         |
		|==============================================================*/
		If mv_par01 == 1
			If nCobTot >= 7
				fLImpaVar()
				cAlias01->(dBSkip())
				Loop
			EndIf
		ElseIf mv_par01 == 2
			If nCobTot >= nEstSeg
				fLImpaVar()
				cAlias01->(dBSkip())
				Loop
			EndIf
		ElseIf mv_par01 == 3
			If !(STOD(cAlias01->ENTREGA) > mv_par02 .and. cAlias01->SALDO == 0)
				fLImpaVar()
				cAlias01->(dBSkip())
				Loop
			EndIf
		EndIf

		/*==============================================================|
		| ANALISAR LISTA CR�TICA.                                       |
		|==============================================================*/
		If	nCobTot	<=	7
			cAnlCrit := 'Sim'
		Else
			If	nCobTot	<	nEstSeg
				cAnlCrit := 'Sim'
			Else
				cAnlCrit := 'N�o'
			EndIf
		EndIf
				

		/*==============================================================|
		| COBERTURAS - ALMOX, FARMACIA.                                 |
		|==============================================================*/
		If nQtdCD07 + nQtdCD30 == 0
			nCobAlm	:= 0
			nCobFAR	:= 0
//			nCobARS	:= 0
			nCobUni	:= 0
		Else
			IF nQtdCD07 == 0
				nCobAlm := nSalALM / nQtdCD30
				nCobFAR := nSalFAR / nQtdCD30
//				nCobARS := nSalARS / nQtdCD30
				nCobUni := (nSalALM + nSalARS) / nQtdCD30
			Else
				nCobAlm := nSalALM / nQtdCD07
				nCobFAR := nSalFAR / nQtdCD07
//				nCobARS := nSalARS / nQtdCD07
				nCobUni := (nSalALM + nSalARS) / nQtdCD07
			EndIf
		EndIf

		/*==============================================================|
		| COBERTURAS ARTESANAL			                                 |
		|==============================================================*/
		
		If nMaiorCons <= 0
			nCobARS := 0
		Else
			If nSalALM <= 0
				nCobARS := 0
			Else
				If nMaxO6Q6 > 0
					nCobARS := nSalARS / nMaxO6Q6
				Else
					nCobARS := nSalARS / nMaiorCons
				EndIf
			EndIf
		EndIf		

		/*==============================================================|
		| NIVEL DE CRITICIDADE MANUT. LC.                               |
		|==============================================================*/
		If	cAnlCrit	=	'N�o'
			cNivCritLC := 'Sem criticidade'
		Else
			If	 nCobTot	<=	nLeadTime
				cNivCritLC := 'Alta Criticidade'
			ElseIf nCobTot	<=	nLeadTime + 7
				cNivCritLC := 'M�dia Criticidade'
			Else
				cNivCritLC := 'Baixa Criticidade'
			EndIf
		EndIf
		/*==============================================================|
		| NIVEL DE CRITICIDADE MANUT. PV.                               |
		|==============================================================*/
		If nCobTot <= nLeadTime 
			cNivCritPV := 'Alta Criticidade'
		ElseIf nCobTot > nLeadTime .and. nCobTot <= nLeadTime + 07
			cNivCritPV := 'M�dia Criticidade'
		Else
			cNivCritPV := 'Baixa Criticidade'
		EndIf
		/*==============================================================|
		| ANALISAR LISTA CRITICA.                                       |
		|==============================================================*/
		If nCobTot <= 7 .or. nCobTot < nEstSeg
			cAnLsCr := 'Sim'
		Else
			cAnLsCr := 'N�o'
		EndIf
		/*==============================================================|
		| CD30xCD7 e CD90xCD30.                                         |
		|==============================================================*/
		nCD30xCD7	:=	(nQtdCD07 - nQtdCD30) / nQtdCD30
		nCD90xCD30	:=	(nQtdCD30 - nQtdCD90) / nQtdCD90
		/*==============================================================|
		| DIAS DE ATRASO.                                               |
		|==============================================================*/
		If	cAlias01->SALDO > 0
			If mv_par02 - STOD(cAlias01->ENTREGA) > 0
				nDiasAtraz :=	mv_par02 - STOD(cAlias01->ENTREGA)
			Else
				nDiasAtraz := 0
			EndIf
		Else
			nDiasAtraz := 0
		EndIf
		/*==============================================================|
		| ANALISAR PREVIS�O VENCIDA.                                    |
		|==============================================================*/
		If	Empty(STOD(cAlias01->ENTREGA))
			cAnPrvVenc := 'N�o'
		Else
			If STOD(cAlias01->ENTREGA) < mv_par02
				cAnPrvVenc	:= 'Sim'
			Else
				cAnPrvVenc := 'N�o'
			EndIf
		EndIf

		/*==============================================================|
		| C�LCULO.                                                      |
		|==============================================================*/
		If nMaiorCons == nQtdCDA
			cCalculo := 'CDA'
		ElseIf nMaiorCons == nQtdCD07
			cCalculo := 'CD07'
		ElseIf nMaiorCons == nQtdCD15
			cCalculo := 'CD15'
		ElseIf nMaiorCons == nQtdCD30
			cCalculo := 'CD30'
		ElseIf nMaiorCons == nQtdCD60
			cCalculo := 'CD60'
		ElseIf nMaiorCons == nQtdCD90
			cCalculo := 'CD90'
		ElseIf nMaiorCons == nQtdCD120
			cCalculo := 'CD120'
		ElseIf nMaiorCons == nQtdCD150
			cCalculo := 'CD150'
		ElseIf nMaiorCons == nQtdCD180
			cCalculo := 'CD180'
		Else
			cCalculo := 'Sem movimento'
		EndIf
		
		oSection:PrintLine()
		cAlias01->(dBSkip())
		fLImpaVar()
	
	EndDo
	

	oSection:Finish()
	
	RestArea(aArea)
	RestArea(aAreaSA2)
		
Return

Static Function fLImpaVar()

ASIZE(aMaiorCons,0)
ASIZE(aMaxO6Q6,0)
ASIZE(aMaxO6V6,0)
cAnalista 	:= ''
cAnlCrit 	:= ''
cAnLsCr 	:= ''
cAnPrvVenc	:= ''
cCalculo 	:= ''
cFreq 		:= ''
cGrupam 	:= ''
cMatSubs 	:= ''
cNivCritLC	:= ''
nCD30xCD7	:= 0
nCD90xCD30	:= 0
nCobAlm	:= 0
nCobARS	:= 0
nCobFAR	:= 0
nCobTot 	:= 0
nCobUni	:= 0
nCusMed 	:= 0
nDiasAtraz	:= 0
nEstSeg 	:= 0
nLeadTime 	:= 0
nLenFil 	:= 0
nMaiorCons	:= 0
nMaxO6Q6 	:= 0
nMaxO6V6 	:= 0
nQtdCD07 	:= 0
nQtdCD120 	:= 0
nQtdCD15 	:= 0
nQtdCD150 	:= 0
nQtdCD180 	:= 0
nQtdCD30 	:= 0
nQtdCD60 	:= 0
nQtdCD90 	:= 0
nQtdCDA 	:= 0
nQtdPedab 	:= 0
nSalALM 	:= 0
nSalAlmTot	:= 0
nSalARS 	:= 0
nSalFAR 	:= 0
nSArmExt 	:= 0
nTamFil 	:= 0

If Select('cAlias02') > 0
	cAlias02->(dBCloseArea())
EndIf
If Select('cAlias03') > 0
	cAlias03->(dBCloseArea())
EndIf
If Select('cAlias04') > 0
	cAlias04->(dBCloseArea())
EndIf
If Select('cAlias05') > 0
	cAlias05->(dBCloseArea())
EndIf
If Select('cAlias06') > 0
	cAlias06->(dBCloseArea())
EndIf
If Select('cAlias07') > 0
	cAlias07->(dBCloseArea())
EndIf
If Select('cAlias08') > 0
	cAlias08->(dBCloseArea())
EndIf
If Select('cAlias09') > 0
	cAlias09->(dBCloseArea())
EndIf
If Select('cAlias10') > 0
	cAlias10->(dBCloseArea())
EndIf
If Select('cAlias11') > 0
	cAlias11->(dBCloseArea())
EndIf

Return 