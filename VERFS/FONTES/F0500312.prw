#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*/{Protheus.doc} F0500312
Efetua a aprova��o e reprova��o enviando e-mail
@type function
@author henrique.toyada
@since 04/11/2016
@project MAN00000463301_EF_003
@version 1.0
@return cHtml, Retorna p�gina
/*/
User Function F0500312()
	
	Local cHtml    := ""
	Local oSolic   := Nil
	Local nCnt     := 0
	Local cAssApr  := "Solicita��o para Aprova��o"
	Local cBodyApr := ""
	Local cAssRpr  := "Solicita��o Reprovada"
	Local cBodyRpr := ""
	Local oAprov
	Private cMsg
	Private oParam
	Private oLista
	Private oLista2
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	HttpCTType("text/html; charset=ISO-8859-1")
	
	oSolic := WSW0500307():New()
	WsChgURL(@oSolic,"W0500307.apw")
	
	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
	
	cMsg     := "Cadastro n�o alterado, verifique amarra��o do funcion�rio logado."
	cMat     := HTTPSession->RHMat
	cCodSol  := HttpGet->cSolic
	cConfirm := HttpGet->cAceita
	cLocal   := IIF(EMPTY(HttpPost->cLocal),"",HttpPost->cLocal)
	cFilRh3  := HttpPost->cFilRh3
	
	cBodyApr := "Prezado," +CRLF + ;
		"Foi adicionado uma solicita��o " + cCodSol + " para sua aprova��o." + CRLF + ;
		"Acesse o Portal RH e efetue a analise da mesma."
	
	cBodyRpr := "Prezado," +CRLF + ;
		"A solicita��o " + cCodSol + " foi reprovada."
	
	If oSolic:InfSolVg(cCodSol,cFilRh3)
		
		oOrg:cParticipantID  := ""
		oOrg:cVision         := oSolic:oWSInfSolVgResult:cRH3VISAO
		oOrg:cEmployeeFil    := oSolic:oWSInfSolVgResult:cRH3FILINI
		oOrg:cRegistration   := oSolic:oWSInfSolVgResult:cRH3MATINI
		oOrg:cEmployeeSolFil := oSolic:oWSInfSolVgResult:cRH3FILAPR
		oOrg:cRegistSolic    := oSolic:oWSInfSolVgResult:cRH3MATAPR
		
	EndIf
	
	If oOrg:GetStructure()
		
		oAprov := WSW0500308():New()
		WsChgURL(@oAprov,"W0500308.apw")
		
		cFilSol  := oOrg:cEmployeeSolFil
		cMatSol  := oOrg:cRegistSolic
		cVOrg    := oOrg:cVision
		cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
		cDepto   := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cDepartment
		
		oAprov:VerAprvSit(cFilSol,cMatSol,cDepto,cVOrg,cEmpFunc,"1")
		
		oSolic:OWSSUPERINF:cFilMaSup   := oAprov:oWSVERAPRVSITRESULT:cFilAprov
		oSolic:OWSSUPERINF:cCodMaSup   := oAprov:oWSVERAPRVSITRESULT:cMatAprov
		oSolic:OWSSUPERINF:cNvlAprov   := cValToChar(oAprov:oWSVERAPRVSITRESULT:nNvlAprov)
		oSolic:OWSSUPERINF:cEmpAprov   := oAprov:oWSVERAPRVSITRESULT:cEmpAprov
		oSolic:OWSSUPERINF:cSolici     := cCodSol
		oSolic:OWSSUPERINF:cFilSolic   := cFilRh3
		
		oParam := WSW0200301():new()
		WsChgURL(@oParam,"W0200301.APW")
		
		If cConfirm == "1"
			If oSolic:InfPegSup()
				cEmail  := oSolic:oWSInfPegSupRESULT:cRAEMAIL
				SendEm(cAssApr, cBodyApr, cEmail)
				cMsg := "Aprovado com sucesso."
			EndIf
		Else
			If oParam:PegViInv(cMat,cCodSol,cLocal,cFilRh3)
				If Len(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL) >0
					For nCnt := 1 To Len(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL)
						cEmail  := oParam:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL[nCnt]:cRAEMAIL
						SendEm(cAssRpr, cBodyRpr, cEmail)
					Next
				EndIf
				cMsg := "Reprovado com sucesso."
			EndIf
		EndIf
	EndIf
	cHtml := ExecInPage("F0500305")
	
	WEB EXTENDED END
	
Return cHtml

/*
{Protheus.doc} SendEm()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 cAssunto
@param		 cBody
@param		 cEmail
@Return
*/
Static Function SendEm(cAssunto,cBody,cEmail)
	
	Local oWs
	
	oWs := WSW0200301():new()
	WsChgURL(@oWs,"W0200301.apw")
	oWs:PARAMENTRE(cAssunto,cBody,cEmail)
	
	
Return
