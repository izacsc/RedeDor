#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200325()
P�gina inicial do acompanhamento
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	 cHtml, p�gina em html
*/
User Function F0200325()
	Local cHtml := ""
	
	Private cMsg
	Private oLista
	Private oStatu
	Private lRet := .F.
	
	HttpCTType("text/html; charset=ISO-8859-1")
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	cMatricu := HTTPSession->RHMat
	cNome    := HttpSession->Login
	cFilMat  := HttpSession->aUser[2]
	
	cDtIni  := IIF(EMPTY(HttpGet->dAbertura),"",SUBSTR(HttpGet->dAbertura,7,4) + SUBSTR(HttpGet->dAbertura,4,2) + SUBSTR(HttpGet->dAbertura,0,2))
	cDtFim  := IIF(EMPTY(HttpGet->dFech),"",SUBSTR(HttpGet->dFech,7,4) + SUBSTR(HttpGet->dFech,4,2) + SUBSTR(HttpGet->dFech,0,2))
	cMat    := IIF(EMPTY(HttpGet->cMat),"",HttpGet->cMat)
	cStatus := IIF(EMPTY(HttpGet->cStatus),"",HttpGet->cStatus)
	
	
	oWs := WSW0200302():new()
	WsChgURL(@oWs,"W0200302.APW")
	
	If oWs:BuscaMat(cMatricu, cFilMat)
		lRet := oWs:lBuscaMatRESULT
	EndIf
	
	If oWs:BuscaSta()
		oStatu := OWS:OWSBuscaStaRESULT:OWSREGISTRO:OWSstatus
	EndIf
	
	If oWs:BuscaRH3(cFilMat, cMatricu,cDtIni, cDtFim,cMat,cStatus)
		oLista := OWS:OWSBUSCARH3RESULT:OWSREGISTRO:OWSACOMPANHA
	EndIf
	cHtml := ExecInPage("F0200325")
	
	WEB EXTENDED END
Return cHtml
