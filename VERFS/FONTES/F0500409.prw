#Include 'Protheus.ch'
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} F0500409
(long_description)
@type function
@author Cris
@since 23/11/2016
@version 1.0
@param cFilSol, character, (Filial da Solicita��o)
@param cMatFun, character, (Matricula do funcion�rio)
@param cNumSol, character, (Numero da Solicita��o)
@param cVisaoAtu, character, (C�digo do Vis�o)
@param cOpc, character, (op��o de mensagem)
@return ${lEnvEmail}, ${.T. email enviado com sucesso .F. n�o enviado}
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500409(cFilSol,cMatFun,cNumSol,cVisaoAtu,cOpc)

	Local aResp		:= {}
	Local iResp		:= 0
	Local cEmails	:= ''
	Local cBody		:= ''
	Local lEnvEmail	:= .T.
	Local cAssunto	:= ''
		
		//Retorna todos os emails de todos os aprovadores respons�veis
		U_F0500405(cVisaoAtu,,,,.T.,@aResp)
	
		For iResp := 1 to len(aResp)
		
			if !Empty(cEmails) 
				
				cEmails	:= cEmails+';'
			
			EndIf
			
			if !Alltrim(aResp[iResp][3]) $ cEmails
				
				cEmails	+= Alltrim(aResp[iResp][3])
				
			EndIf
			
		Next iResp
		
		if cOpc == '1'
			
			cAssunto := "Efetiva��o da Solicita��o de Movimenta��es de Pessoal"
			
			cBody := '<html><body><pre>'+CRLF
			cBody += '<b>Prezado,</b>'+CRLF
			cBody += "A solicita��o abaixo foi EFETIVADA pelo departamento RH com SUCESSO."+CRLF
			cBody += "N�mero da Solicita��o:"+cNumSol+CRLF
			cBody += "Matr�cula:"+cMatFun+CRLF
			cBody += '</pre></body></html>'

		Elseif  cOpc == '2'
			
			cAssunto := "Negada Efetiva��o da Solicita��o"
				
			cBody := '<html><body><pre>'+CRLF
			cBody += '<b>Prezado,</b>'+CRLF
			cBody += "CANCELADA a solicita��o abaixo pelo departamento RH."+CRLF
			cBody += "N�mero da Solicita��o:"+cNumSol+CRLF
			cBody += "Matr�cula:"+cMatFun+CRLF
			cBody += '</pre></body></html>'
		
		EndIf
		
		//FWMsgRun(,{|| lEnvEmail	:= U_F0200304("Cancelamento de Solicita��o", cBody, cEmails)},"Aguarde...","Estabelecendo conex�o com o servi�o de e-mail." ) 
		FWMsgRun(,{|| lEnvEmail:= EnvEmail("",cAssunto,cBody,cEmails,"")},"Aguarde...","Estabelecendo conex�o com o servi�o de e-mail." )
		
		if lEnvEmail
			
			Aviso("SUCESSO - Email","E-mail enviado com sucesso!" ,{'OK'},1)

		Else
		
			Aviso("INSUCESSO - Email","E-mail N�O enviado. Comunique aos envolvidos!" ,{'OK'},1)
							
		EndIf

Return lEnvEmail
//--------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ${EnvEmail}
(Envia e-mail)
@type function
@author Cris
@since 09/11/2016
@param cArquivo, character, (nome do arquivo a ser anexado)
@param cSubject, character, (Titulo do e-mail)
@param cBody, character, (Corpo do e-mail)
@param cTo, character, (endere�o de Destino do e-mail)
@param cCC, character, (endere�o copia de destino)
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function EnvEmail(cArquivo,cSubject,cBody,cTo,cCC)

	Local cServer	:= AllTrim(GetMv("MV_RELSERV"))
	Local cFrom		:= AllTrim(GetMv("MV_RELACNT")) 
	Local lAutentica:= GetMv("MV_RELAUTH") 
	Local cUserAut	:= Alltrim(GetMv("MV_RELAUSR"))
	Local cPassAut	:= Alltrim(GetMv("MV_RELAPSW"))
	
	Default cArquivo := ""
	Default cSubject := ""
	Default cBody    := ""
	Default cTo      := ""
	Default cCC      := ""
	
	If Empty(cServer)
		conout("Nome do Servidor de Envio de E-mail nao definido no 'MV_RELSERV'")
		Return .F.
	EndIF
	
	IF Empty(cFrom)
		conout("Conta para acesso ao Servidor de E-mail nao definida no 'MV_RELACNT'")
		Return .F.
	EndIf

	cTo := AvLeGrupoEMail(cTo)
	cCC := AvLeGrupoEMail(cCC)
	
	lOK	:= MailSmtpOn( cServer, cFrom, cPassAut, 60 )
	
	If !lOK
		
		conout("Falha na Conex�o com Servidor de E-Mail")
		Aviso('Email n�o enviado',"Falha na Conex�o com Servidor de E-Mail. ",{'OK'},3)
		
	Else
	
		If lAutentica
			If !MailAuth(cUserAut,cPassAut)
				Aviso('Email n�o enviado',"Falha na Autentica��o do Usu�rio. ",{'OK'},3)
				conout("Falha na Autenticacao do Usuario")
				lOk	:=	MailSmtpOff()
			EndIf
		EndIf
		
		If !Empty(cCC)
			lOK:=MailSend( cFrom, { cTo }, { cCC }, { }, cSubject, cBody, { cArquivo },.F. )
		Else
			lOK:=MailSend( cFrom, { cTo }, { }, { }, cSubject, cBody, { cArquivo },.F. )
		EndIF                                  
		If !lOK
			if !IsBlind() 
				///Aviso('Email n�o enviado',"Falha no Envio do E-Mail: "+ALLTRIM(cTo),{'OK'},3)
			Else
				conout("Falha no Envio do E-Mail: "+ALLTRIM(cTo)) 
			EndIF
		Else
			if  !IsBlind()                                                                      
				//Aviso("Envio de Email",'E-mail enviado com sucesso.', {'OK'},3)
			EndIf
		EndIF
	ENDIF
	
	MailSmtpOff()

RETURN lOK  
