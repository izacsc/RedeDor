#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200317()

@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return	 cHtml
*/
User Function F0200317()
	
	Local cHtml    := ""
	Local cSupeMat := ""
	Local cTela    := "F0200304"
	Local oParam   := Nil
	Local cHierarquia 	:= ""
	Local nPos        	:= 0
	Local aAux        	:= {}
	Local nAux        	:= 0
	Local nNivel      	:= 0
	Local oOrg
	Private oLista3
	Private oLista4
	
	WEB EXTENDED INIT cHtml START "InSite"
	Default HttpGet->Page         		:= "1"
	Default HttpGet->FilterField     	:= ""
	Default HttpGet->FilterValue	    := ""
	Default HttpGet->EmployeeFilial   	:= ""
	Default HttpGet->Registration     	:= ""
	Default HttpGet->EmployeeEmp     	:= ""
	Default HttpGet->cKeyVision		    := ""
	
	//cMat := HTTPSession->RHMat
	cNome := HttpSession->Login
	HttpSession->aStructure	   	:= {}
	HttpSession->cHierarquia		:= ""
	HttpSession->cDataIni		:= ""
	cMat      := HTTPSession->RHMat
	cNomeSup  := HttpSession->Login
	cSolic    := HttpGet->cSolic
	cStatus   := "3"
	cFilis    := ""
	cFilials  := ""
	lBtAprova := .T.
	funcodigo := IIF(HttpGet->funcodigo != Nil, HttpGet->funcodigo,cMat)
	supfuncod := IIF(HttpGet->cDepSup != NIL,HttpGet->cDepSup,cMat)
	nReg      := IIF(HttpGet->nReg != NIL,VAL(HttpGet->nReg),1)
	cFilSup	  := HttpSession->aUser[2]
	
	oParam := WSW0200301():new()
	WsChgURL(@oParam,"W0200301.APW")
	
	fGetInfRotina("U_F0200301.APW")
	GetMat()								//Pega a Matricula e a filial do participante logado
	
	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW",,,HttpGet->EmployeeEmp)
	
	
	
	If Empty(HttpGet->EmployeeFilial) .And. Empty(HttpGet->Registration)
		oOrg:cParticipantID 	    := HttpSession->cParticipantID
		
		If ValType(HttpSession->RHMat) != "U" .And. !Empty(HttpSession->RHMat)
			oOrg:cRegistration	 := HttpSession->RHMat
		EndIf
	Else
		oOrg:cEmployeeFil  	    := HttpGet->EmployeeFilial
		oOrg:cRegistration 	    := HttpGet->Registration
	EndIf
	
	oOrg:cKeyVision				:=  Alltrim(HttpGet->cKeyVision)
	
	oOrg:cVision     		    := HttpSession->aInfRotina:cVisao
	oOrg:cFilterValue 		    := HttpGet->FilterValue
	oOrg:cFilterField   		:= HttpGet->FilterField
	oOrg:cRequestType 		    := ""
	
	IF oOrg:GetStructure()
		HttpSession->aStructure  := aClone(oOrg:oWSGetStructureResult:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE)
		nPageTotal 		       := oOrg:oWSGetStructureResult:nPagesTotal
		
		// *****************************************************************
		// Inicio - Monta Hierarquia
		// *****************************************************************
		cHierarquia := '<ul style="list-style-type: none;"><li><a href="#" class="links" onclick="javascript:GoToPage(null,1,null,null,null,null,' +;
			"'" + HttpSession->aStructure[1]:cEmployeeFilial + "'," +;
			"'" + HttpSession->aStructure[1]:cRegistration + "'," +;
			"'" + HttpSession->aStructure[1]:cEmployeeEmp + "'," +;
			"'" + oOrg:cKeyVision + "'" + ')">'
		
		If Empty(HttpSession->cHierarquia) .or. (HttpSession->cParticipantID == HttpSession->aStructure[1]:cParticipantID)
			nNivel                 := 1
			HttpSession->cHierarquia := ""
		Else
			aAux := Str2Arr(HttpSession->cHierarquia, "</ul>")
			If (nPos := aScan(aAux, {|x| cHierarquia $ x })) > 0
				For nAux := len(aAux) to nPos step -1
					aDel(aAux,nAux)
					aSize(aAux,Len(aAux)-1)
				Next nAux
			EndIf
			HttpSession->cHierarquia := ""
			For nPos := 1 to Len(aAux)
				HttpSession->cHierarquia += aAux[nPos] + "</ul>"
			Next nPos
			
			nNivel := Iif(Len(aAux) > 0,Len(aAux)+1,1)
		EndIf
		
		For nPos := 1 to nNivel
			cHierarquia += '&nbsp;&nbsp;&nbsp;'
		Next nPos
		cHierarquia += Alltrim(str(nNivel)) + " . " + HttpSession->aStructure[1]:cName + '</a></li></ul>'
		
		HttpSession->cHierarquia += cHierarquia
		// Fim - Monta Hierarquia
	Else
		HttpSession->aStructure := {}
		nPageTotal 		      := 1
	EndIf
	
	If LEN(HttpSession->aStructure) > 0
		cMatricu1  := HttpSession->aStructure[nReg]:cRegistration
		cNome1     := HttpSession->aStructure[nReg]:cName
		cAdmissao1 := HttpSession->aStructure[nReg]:CADMISSIONDATE
		cDepartam1 := HttpSession->aStructure[nReg]:CDEPARTMENT
		cSituacao1 := HttpSession->aStructure[nReg]:CDESCSITUACAO
		cCenCusto1 := HttpSession->aStructure[nReg]:CCOSTID
		cCargo1    := ""//POSICIONE("SRA",1,HttpSession->aStructure[nReg]:CEMPLOYEEFILIAL + HttpSession->aStructure[nReg]:cRegistration,"RA_CARGO")
		cFilial1   := HttpSession->aStructure[nReg]:CEMPLOYEEFILIAL
		cNmCarg1   := ""//POSICIONE("SRA",1,HttpSession->aStructure[nReg]:CEMPLOYEEFILIAL + cCargo1,"Q3_DESCSUM")
		cNmCeCu1   := HttpSession->aStructure[nReg]:CCOST
		cNmDepa1   := HttpSession->aStructure[nReg]:CDESCRDEPARTMENT
		Superior1  := ""//POSICIONE("SRA",1,HttpSession->aStructure[nReg]:CSUPFILIAL + HttpSession->aStructure[nReg]:cSUPRegistration,"RA_NOME")
	EndIf
	
	If oParam:ListaSolicita(cMat)
		oLista3 :=  oParam:oWSLISTASOLICITARESULT
	EndIf
	
	If oParam:InfInsti()
		oLista4 :=  oParam:oWSInfInstiRESULT
	EndIf
	
	cHtml := ExecInPage(cTela)
	WEB EXTENDED END
	
Return cHtml

