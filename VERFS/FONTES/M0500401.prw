#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "FWBROWSE.CH"
Static lPreeRB6	:= .F.
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} M0500401
(Ponto de Entrada MVC do modelo MS050401)
@type function
@author Cris
@since 28/10/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
User Function M0500401()

	Local aParam     := PARAMIXB
	Local xRet       := .T.
	Local oObj       := aParam[1]
	Local cIdPonto   := aParam[2]
	Local cTabSal	 := ''
	Local nLinAtu	 := 1
	Local cNumRH3	 := ''

	//Sinalizado que a cada nova ativa��o da tabela uma nova consulta dever� ser efetuada na tabela Salarial
	if cIdPonto == "MODELVLDACTIVE"
	
		lPreeRB6	:= .F.
		
	EndIf

	//Simulo uma altera��o no model principal para que seja permitido a grava��o dos dados necess�rios 
	If cIdPonto == 'MODELPOS' 
	
		oObj:GetModel("F0500401"):SetValue("RH3_MAT","")   
		
	EndIf
	
	//Verifico se realmente o colaborador deseja abortar a opera��o
	if cIdPonto == 'MODELCANCEL'

        //If ( xRet := ApMsgYesNo( 'Deseja Realmente Sair ?' ) )

        Help( ,, 'Help',"CANCELAMENTO DA SOLICITA��O", 'Solicita��o abortada pelo colaborador.', 1, 0 )
		
			While .T.
				cNumRH3 := GetSX8Num("RH3","RH3_CODIGO")
				DbSelectArea("RH3")
				RH3->( DbSetOrder(1) )
				If RH3->(dbSeek(xFilial("RH3")+cNumRH3 ))
					ConfirmSX8()
					Conout("[RH3] - Realocando contador -> "+cNumRH3)
					Loop
				EndIf
				Exit
			EndDo	
		
		ConfirmSX8()
		
		if FindFunction("U_F0500201")
		
			//Requisito N005 -  Indicadores: -057-Logou se na tela - somente no cancelamento esta sendo gravado
			U_F0500201(xFilial('RH3'),cNumRH3,'057')
			
		EndIf
        //EndIf
         MtBCMod(oObj,{"SALDETAIL"},{||.T.})
         
	EndIf
	
	//Carrego a Tabela Salarial de acordo com a informada no cadastro de funcion�rio	
	If cIdPonto ==  'MODELPRE'
	  
	  	if !lPreeRB6
		  	
		  	lPreeRB6	:=  .T.
		  	
		  	oObj:GetModel("F0500401"):SetValue("RH3_MAT",SRA->RA_MAT)  
		  	
			  cTabSal	 := GetNextAlias()
			  
			  SelTabSl(@cTabSal)
	
			 if !(cTabSal)->(Eof())
				 
				 While !(cTabSal)->(Eof())
					 
					 if nLinAtu > 1
					  
				 		 oObj:GetModel("RB6DETAIL"):AddLine()
				 
				 	  EndIf
					  oObj:GetModel("RB6DETAIL"):SetValue("NIVELATU",(cTabSal)->RB6_NIVEL)	
					  oObj:GetModel("RB6DETAIL"):SetValue("FAIXAATU",(cTabSal)->RB6_FAIXA)
					  oObj:GetModel("RB6DETAIL"):SetValue("VALORATU",(cTabSal)->RB6_VALOR)

			 	(cTabSal)->(dbSkip())
			 	
			 	 nLinAtu	:= nLinAtu + 1
			 	
			 	Enddo
			 	
			 EndIf
			 
			 (cTabSal)->(dbCloseArea())
		
		//MtBCMod(oObj,{"RB6DETAIL"},{||.F.})
		
		EndIf
		  
	EndIf

Return xRet
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SelTabSl
(long_description)
@type function
@author Cris
@since 28/10/2016
@version 1.0
@param cTabSal, character, (Descri��o do par�metro)
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function SelTabSl(cTabSal)

	Local cQryRB6		:= ""
	
		cQryRB6	:= " SELECT RB6_FILIAL, RB6_TABELA, RB6_DESCTA, RB6_TIPOVL, RB6_NIVEL, RB6_FAIXA,RB6_VALOR, RB6_PTOMIN, RB6_PTOMAX,  "+CRLF
		cQryRB6	+= " RB6_DTREF, RB6_COEFIC,RB6_REGIAO, RB6_ATUAL  "+CRLF
		cQryRB6	+= " FROM "+RetSqlName("RB6")+" (NOLOCK) "+CRLF
		cQryRB6	+= " WHERE RB6_FILIAL = '"+xFilial("RB6")+"'  "+CRLF
		cQryRB6	+= "  AND RB6_TABELA = '"+SRA->RA_TABELA+"'  "+CRLF
		cQryRB6 	+= " AND RB6_NIVEL = '"+SRA->RA_TABNIVE+"'"	+ CRLF
		cQryRB6	+= "  AND D_E_L_E_T_ = ''  "+CRLF
 
 	cQryRB6 := ChangeQuery(cQryRB6)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQryRB6),cTabSal,.T.,.T.)

Return