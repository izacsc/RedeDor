#INCLUDE "TOTVS.CH"
#INCLUDE 'TBICONN.CH'

/*
{Protheus.doc} F0101004()
Envia email de  notifica��o da recusa ao respons�vel.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F0101004(cFilOri,cNumTit,cTipo,cParcela,cPrefixo,nValor,dVencrea,cFornece,cNomFor,cCodEmit,cNomEmit,cCodRecu,cNomRecu,cMotivo,dData,cOrigem,cEmailEmit)

Local cHtml		:= ''
Local cAlert	:= ''
Local cError	:= ''
Private cServer := Trim(GetMV("MV_RELSERV")) // smtp.ig.com.br ou 200.181.100.51
Private cEmail  := Trim(GetMV("MV_RELACNT")) // fulano@ig.com.br
Private cPass   := Trim(GetMV("MV_RELPSW"))  // 123abc

cHtml := '<html>'
cHtml += '<head>'
cHtml += '<title>Editor HTML Online</title>'
cHtml += '</head>'
cHtml += '<body>'
cHtml += '<div>'
cHtml += '<table align="left" border="0" cellpadding="1" cellspacing="1" style="width: 800px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<p>'
cHtml += '<strong><span style="font-family:arial,helvetica,sans-serif;">Recusa de T&iacute;tulo a Pagar</span></strong></p>'
cHtml += '</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<table border="0" cellpadding="1" cellspacing="1" style="width: 799px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">T&iacute;tulo</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;"> ' + cNumTit + '</span></span></td>'
cHtml += '<td>'
cHtml += '<strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Tipo</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;"> ' + cTipo + '</span></span></td>'
cHtml += '<td>'
cHtml += '<strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Parcela</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">' + cParcela + '</span></span></td>'
cHtml += '<td>'
cHtml += '<strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Valor</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">' + LTrim(Transform(nValor,"@E 999,999,999.99")) + '</span></span></td>'
cHtml += '<td>'
cHtml += '<strong><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Vencimento</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">' + DTOC(dVencrea) + '</span></span></td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<table border="0" cellpadding="1" cellspacing="1" style="width: 799px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<strong><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">Fornecedor</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cFornece + '</span></span></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cNomFor  + '</span></span></td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<table border="0" cellpadding="1" cellspacing="1" style="width: 799px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<strong><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">Emitente</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cCodEmit + '</span></span></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cNomEmit + '</span></span></td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<table border="0" cellpadding="1" cellspacing="1" style="width: 799px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<strong><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">Recusante</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cCodRecu + '</span></span></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">' + cNomRecu + '</span></span></td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<table align="left" border="0" cellpadding="1" cellspacing="1" style="width: 799px">'
cHtml += '<tbody>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '<strong><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">Motivo</span></span></strong></td>'
cHtml += '<td>'
cHtml += '<span style="font-family:arial,helvetica,sans-serif;"><textarea style="margin: 0px; width: 785px; height: 89px;">' + cMotivo + '</textarea></span></td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '&nbsp;</td>'
cHtml += '</tr>'
cHtml += '<tr>'
cHtml += '<td>'
cHtml += '&nbsp;</td>'
cHtml += '</tr>'
cHtml += '</tbody>'
cHtml += '</table>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</div>'
cHtml += '<div style="text-align: center;">'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</div>'
cHtml += '<p>'
cHtml += '&nbsp;</p>'
cHtml += '</body>'
cHtml += '</html>'

CONNECT SMTP SERVER cServer ACCOUNT cEmail PASSWORD cPass RESULT lResulConn
SEND MAIL FROM cEmail TO cEmailEmit SUBJECT 'Recusa de T�tulo' BODY cHtml RESULT lResulSend

If !lResulSend
	GET MAIL ERROR cError
	cAlert:= 'Falha no Envio do e-mail.' + cError
Else
	cAlert:= 'Email enviado'
Endif

MsgAlert(cAlert)

Return