#Include 'Protheus.ch'
#INCLUDE "APWIZARD.CH"
/*
{Protheus.doc} F0100602()
Montagem do e-mail a ser enviado. 
@Author     Bruno de Oliveira
@Since      01/04/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_006
@Param		lRet, l�gico, permite continuar ou n�o
@Return 	lRet, permite continuar ou n�o
*/
User Function F0100602(lRet)

Local aArea			:= GetArea()
Local aAreaSQS		:= SQS->(GetArea())
Local aAreaSRA		:= SRA->(GetArea())
Local cAssunto		:= "Admiss�o do Candidato\Funcion�rio" 
Local cEmlRespVg		:= ""
Local cConteudo		:= ""
Local cCcopia			:= ""

If lRet .AND. IsInCallStack("RSPM001")

	DbSelectArea("SQS")
	SQS->(DbSetOrder(1))
	If !SQS->(DbSeek(xFilial("SQS")+cVaga))
		Aviso("Atencao", "Vaga n�o encontrada.")
		lRet := .F.
	Else
		
		If Empty(SQS->QS_MATRESP)
			Aviso("Atencao", "Favor preencher a matricula do respons�vel da vaga.")
			lRet := .F.
		Else
			DbSelectArea("SRA")
			SRA->(DbSetOrder(1))
			If SRA->(DbSeek(SQS->QS_FILRESP + SQS->QS_MATRESP)) 
				cEmlRespVg := SRA->RA_EMAIL
			EndIf
			
			If Empty(cEmlRespVg)
				Aviso("Atencao", "Favor preencher o e-mail do respons�vel da vaga.")
				lRet := .F.
			Else
			
				cConteudo += "Prezado," + CRLF
				cConteudo += "Segue os dados do novo funcion�rio:" + CRLF
				cConteudo += "Matr�cula: " + M->RA_MAT + CRLF
				cConteudo += "Nome: " + Alltrim(M->RA_NOME) + CRLF
				cConteudo += "CPF: " + M->RA_CIC + CRLF
				cConteudo += "Dt.Admiss�o: " + DTOC(M->RA_ADMISSA)
				
				lRet := U_F0100603(cEmlRespVg,cAssunto,cConteudo,cCcopia,.F.)
				
			EndIf
		EndIf
	EndIf
EndIf

RestArea(aAreaSRA)
RestArea(aAreaSQS)
RestArea(aArea)

Return lRet