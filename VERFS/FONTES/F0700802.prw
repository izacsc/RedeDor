#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0700802 
Metodo deletar Fabricante via WS
@author Fernando Carvalho
@since 27/01/2017
@version 1.0
@param oFabriCod, objeto, (Descri��o do par�metro)
@Project MAN0000007423041_EF_008
/*/
User Function F0700802(oFabriCod)
    Local cRetorno := ""
    Local aCampos  := {}


    aCampos := {;
                    {"P13_FILIAL", oFabriCod:cFilial     },;
                    {"P13_COD"   , oFabriCod:cCOD        }; //-- fun��o pra pegar o ID
                }
    
    cChave   := u_RetChave("P13", aCampos,{1,2})
    cRetorno := u_DelMVCRg("P13", cChave, "F0700701") //-- chamar fun��o de log

    aCampos := ASize(aCampos,0)
    aCampos := Nil

Return cRetorno

