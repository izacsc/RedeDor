#Include "PROTHEUS.CH"
/*{Protheus.doc} F0400103()
Fun��o para adicionar as op��es do Banco de Conhecimento no aRotina
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
User Function F0400103()
                        
AAdd( aRotina, { 'Banco Espec�fico - Visualizar'	, 'U_F0400101(1)', 0, 0 } )
AAdd( aRotina, { 'Banco Espec�fico - Incluir'		, 'U_F0400101(3)', 0, 0 } )
AAdd( aRotina, { 'Banco Espec�fico - Excluir'		, 'U_F0400101(5)', 0, 0 } )
//AAdd( aRotina, { 'Banco Espec�fico - Download'	, 'U_F0400101(6)', 0, 0 } )

Return aRotina


/*{Protheus.doc} F0400104()
Fun��o para adicionar as op��es do Banco de Conhecimento no aRotina
@Author			Nairan Alves Silva
@Since			15/09/2016
@Version		P12.7
@Project    	MAN00000463801_EF_001
@Return		Nil	 */
User Function F0400104(aRotina)
AAdd( aRotina, { 'Banco Espec�fico - Visualizar'	, 'U_F0400101(1)', 0, 0 } )
AAdd( aRotina, { 'Banco Espec�fico - Incluir'		, 'U_F0400101(3)', 0, 0 } )
AAdd( aRotina, { 'Banco Espec�fico - Excluir'		, 'U_F0400101(5)', 0, 0 } )
//AAdd( aRotina, { 'Banco Espec�fico - Download'	, 'U_F0400101(6)', 0, 0 } )
Return aRotina
