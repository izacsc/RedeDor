#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#include 'FILEIO.ch'

Static aLogTt	:= {}

/*
{Protheus.doc} F0300607()
Importa��o carteirinha de saude
@Author     Rogerio Candisani
@Since      07/11/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300607()
    
	Local aSays			:= {}
	Local aButtons		:= {}
	Local nOpca 		:= 0
	Local aArea			:= GetArea()
	Private cTpForn		:= ''
	Private	aRet		:= {}
	Private cArqImp		:= ''
	Private cPerg		:= "FSW0300607"	// Gp. perguntas especifico RHO
	Private cProcesso	:= ""			// Variavel utilizada na funcao gpRCHFiltro() Consulta Padrao - 1 = Periodos Abertos
	Private cDrvAt	:= ''
	Private cDirect	:= ''
				    
	cCadastro := OemToAnsi("Importa��o carteira sa�de")
	AADD(aSays,OemToAnsi("Esta rotina atualizar� o n�mero das carteirinhas com: ") + CRLF )   	// "Esta rotina importa valores para os seguintes arquivos: "
	AADD(aSays,OemToAnsi("o Arquivo Carteira Sa�de OU Carteira Odontol�gica ") + CRLF )
	AADD(aSays,OemToAnsi("Conforme definido na rotina de cadastro de Layout de Importa��o.") )   			// "Conforme definido na rotina de cadastro de Layout de Importacao."
	//AADD(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T.)  } } )
	AADD(aButtons, { 5,.T.,{|| ParamImp()  } } )
	//AADD(aButtons, { 1,.T.,{|o| nOpca := 1,If(MsgYesNo(OemToAnsi("Confirma a importa��o de carteira tipo  "+Iif(MV_PAR02==1,'SA�DE','ODONTOL�GICO')+"?"),OemToAnsi("Atencao")),FechaBatch(),nOpca:=0) }} )
	AADD(aButtons, { 1,.T.,{|o| nOpca := 1,if(U_F0300608(),If(MsgYesNo(OemToAnsi("Confirma a importa��o de carteira tipo  "+Iif(cTpForn=='1','SA�DE','ODONTOL�GICO')+"?"),OemToAnsi("Atencao")),FechaBatch(),nOpca:=0),.F.) }} )
	AADD(aButtons, { 2,.T.,{|o| nOpca := 0, FechaBatch() }} )
		
	FormBatch( cCadastro, aSays, aButtons )
	If nOpca == 1
		Processa({|lEnd| F03006Proc(),"Importacao carteira de sa�de"})
	EndIf
	
	aLogTt	:= {}
	cTpForn	:= ''
	aRet	:= {}
	cArqImp	:= ''
	
	RestArea(aArea)
	
Return( Nil )
/*{Protheus.doc} ParamImp
(long_description)
@type function
@author 
@since 22/12/2016
@version 1.0
@return ${return}, ${return_description}
*/
Static Function ParamImp()

	Local aPergs		:= {}
	Local cParaFil		:= Space(TamSx3('RHK_FILIAL')[1])
	Private nTipImp		:= 1
	Private	cCamIni		:= 'C:\'+space(97)
	
		aRet	:= {}
		
	 	aAdd( aPergs ,{1,"Selecione Para Filial:",cParaFil,"@!",'.T.','SM0','.T.',50,.F.})
	 	aAdd( aPergs ,{2,'Selecione o Tipo de Plano:',nTipImp,{'1=Sa�de','2-Odontol�gico'},60,'.T.',.T.})
	 	aAdd( aPergs ,{6,'Selecione o arquivo:',cCamIni,'@!','.T.','.T.',100,.T.,"Arquivos .CSV|*.CSV",'C:\',GETF_LOCALHARD} )
	 	 	
	 	ParamBox(aPergs ,"Parametros ",aRet)      
	 	
Return 

/*{Protheus.doc} VlArq
(long_description)
@type function
@author Cris
@since 22/12/2016
@version 1.0
@return ${return}, ${return_description}
*/
Static Function VlArq()

	Local lVlArq	:= .T.
	Local cDrive	:= ''
	Local cDirect	:= ''
	Local cExtens	:= ''
	
		SplitPath(aRet[3],@cDrive,@cDirect,@cArqImp,@cExtens)	
		
		if Substring(aRet[1],1,1) == '1'
		
			if !'SAUDE' $ cArqImp
			
				Help("",1, "Help", "Tipo X Nome do Arquivo(VlArq_01)", "O tipo esta como 'SAUDE' selecione  um arquivo referente a Carteirinhas do Plano de Sa�de,contendo em seu nome a palavra 'SAUDE'!" , 3, 0)
				lVlArq	:= .F.
				
			EndIf
			
			cTpForn	:= '1'
			
		Elseif  Substring(aRet[1],1,1) == '2'
		
			if !'ODONTOLOGIC' $ cArqImp

				Help("",1, "Help", "Tipo X Nome do Arquivo(VlArq_01)", "O tipo esta como 'ODONTOLOGICO' selecione  um arquivo referente a Carteirinhas do Odontol�gico,contendo em seu nome a palavra 'ODONTOLOGIC'!" , 3, 0)			
				lVlArq	:= .F.
				
			EndIf

				cTpForn	:= '2'
									
		Else
		
			lVlArq	:= .F.
			
		EndIf
		
Return lVlArq
/*
{Protheus.doc} F03006PROC
Leitura Arquivo Texto e Gravacao no Arq. Valores Variaveis.
@author Rogerio Candisani
@since 17/10/16
@version P12.7
*/
Static Function F03006PROC()

	Local aReg		:={}
	Local cArquivo	:=	""
	Local nX		:= 	0
	Local cTXT		:= 	''
	Local aBenef	:=	{}
	
	If Len(aRet) < 3
		MsgAlert("Favor selecionar o arquivo a ser importado! Verifique os parametros.", "Aten��o!")
		Return
	Else
		cArquivo := aRet[3]
	EndIf

	//gerar importa��o dos arquivos
	oFile := FwFileReader():New(cArquivo)
	
	//Abertura do arquivo texto.
	If !(oFile:Open())
		MsgAlert("O arquivo de nome " + Alltrim(cArquivo) + " n�o pode ser aberto! Verifique os parametros.", "Aten��o!")
		Return
	EndIf

	While (oFile:hasLine())

		cTXT := oFile:GetLine()
		AADD(aReg,cTXT)
	
	End
	
	_cTotal := Len(aReg)
	ProcRegua(_cTotal)

		aItens := {}

	//Ler os aqrquivos .csv
	For nX:= 2 to len(aReg) // primeira linha cabecalho
		cBuffer := aReg[nX]

		aItens := StrTokArr2(cBuffer, ';', .T.)

		Aadd(aBenef,{  	aItens[1]  ,; //1-Filial
						aItens[2]  ,; //2-Matr�cula
						aItens[3]  ,; //3-Tipo de benef�cio
						aItens[4]  ,; //4-Sequ�ncia do Dependente
						aItens[5]  ,; //5-Certificado
						aItens[6]  ,; //6-Cart�o do plano
						aItens[7]  ,; //7-Nome do Segurado
						aItens[8]  ,; //8-N� CPF Titular
						aItens[9] })//9-N� CPF Dependente
	Next nX
	
	//inclui os CNS dos benefici�rios
	InclBenef(aBenef)

Return

/*
{Protheus.doc} InclBenef
inclui os benefici�rios
@Author     Rogerio Candisani
@Since      07/11/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Param		aBenef, array que recebe os beneficiarios a serem atualizados
*/
Static Function InclBenef(aBenef)
	
	Local nY		:= 0
	Local lExisPln	:= .F.
	Local lDtFmPln	:= .F.
	Local lAtuPln	:= .F.
	Local cMesPer	:= StrZero(Month(dDatabase),2)
	Local cAnoPer	:= StrZero(Year(dDatabase),4)
	
	//definir os campos de grava��o
	/*If MV_PAR02 == 1
		cTpForn := "1"
	Elseif MV_PAR02 == 2
		cTpForn:="2"
	Endif
	*/
		//posiciona no modelo do SRA
	For nY:= 1 to len(aBenef)

		lExisPln	:= .F.
		lDtFmPln	:= .F.
		lAtuPln		:= .F.
		
		If 	"TITULAR" $ UPPER(aBenef[nY][3]) .AND. (IIF(Empty(aRet[1]),.T.,aRet[1]==aBenef[nY][1]))
			//posicionar na RHK 
			DbSelectArea("RHK")
			RHK->(DbSetOrder(1))//RHK_FILIAL+RHK_MAT+RHK_TPFORN+RHK_CODFOR
			if RHK->(DbSeek(aBenef[nY][1]+aBenef[nY][2]))
				//Verifico se o titular possui o plano incluido 
				While !RHK->(EOF()) .AND. RHK->(RHK_FILIAL+RHK_MAT) == (aBenef[nY][1]+aBenef[nY][2])
				
					if RHK_TPFORN == cTpForn .AND. !lAtuPln
					
						//verificar se nao esta encerrado o plano
						lExisPln	:= .T.	
						If Empty(RHK->RHK_PERFIM) .OR. IIF(cAnoPer == Substring(RHK->RHK_PERFIM,3,4),cMesPer <= Substring(RHK->RHK_PERFIM,1,2),iIF(cAnoPer <= Substring(RHK->RHK_PERFIM,3,4),.T.,.F.)) //atualiza a carteira de vida
							
							if Alltrim(RHK->RHK_NCARVD) <> Alltrim(aBenef[nY][6])
							
								if	RHK->(Reclock('RHK',.F.))
									RHK->RHK_NCARVD	:= aBenef[nY][6]
									RHK->(MsUnlock())
													
									aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'Atualizado com sucesso.'})
									lAtuPln	:= .T.
									
								EndIf

							Else
												
								aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'O n�mero da carteirinha j� existe na base.'})													
								lAtuPln		:= .T.
								
							EndIf
							
							lDtFmPln	:= .F.
							
						Else
						
							lDtFmPln	:=  .T.
									
						Endif
					
					EndIf
					
					RHK->(DbSkip())
					
				Enddo
			
			EndIf
			
			//Se existi RHK mas a data final do plano for menor que a data atual
			if lExisPln .AND. lDtFmPln .AND. !lAtuPln
			
				aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'N�o Atualizado. Plano(s) n�o est�o vigente(s).'})
			
			Elseif !lExisPln .AND. !lAtuPln
			
				aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'Titular n�o cadastrado para este tipo de Plano.'})
					
			EndIf
			
		Elseif  (Empty(aRet[1]),.T.,aRet[1]==aBenef[nY][1])

			//posicionar na RHL 
			DbSelectArea("RHL")
			RHL->(DbSetOrder(2))//RHL_FILIAL+RHL_MAT+RHL_CODIGO+RHL_TPFORN+RHL_CODFOR
			if RHL->(DbSeek(aBenef[nY][1]+aBenef[nY][2]+aBenef[nY][4]))
					
				While !RHL->(EOF()) .AND.  AllTrim(RHL->(RHL_FILIAL+RHL_MAT+RHL_CODIGO)) == AllTrim((aBenef[nY][1]+aBenef[nY][2]+aBenef[nY][4]))

					If RHL_TPFORN == cTpForn .AND. !lAtuPln
					
						//verificar se nao esta encerrado o plano
						lExisPln	:= .T.	
	
						If Empty(RHL->RHL_PERFIM) .OR.  IIF(cAnoPer == Substring(RHL->RHL_PERFIM,3,4),cMesPer <= Substring(RHL->RHL_PERFIM,1,2),iIF(cAnoPer <= Substring(RHL->RHL_PERFIM,3,4),.T.,.F.))//atualiza a carteira de vida
							
							if Alltrim(RHL->RHL_NCARVD) <> aBenef[nY][6]
															
								if RHL->(Reclock('RHL',.F.))
									RHL->RHL_NCARVD	:= aBenef[nY][6]
									RHL->(MsUnlock())
								
									aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'Atualizado com sucesso.'})
									lAtuPln	:= .T.
									
								EndIf

							Else
												
								aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'O n�mero da carteirinha j� existe na base para este plano.'})
								lAtuPln	:= .T.
								
							EndIf
							
							lDtFmPln	:= .F.	
													
						Else
						
							lDtFmPln	:=  .T.
														
						Endif

					EndIf
					
					RHL->(DbSkip())
					
				Enddo
				
			Else
			
				aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'Dependente n�o cadastrado/Localizado no plano.'})
				
			EndIf
							
			//Se existi RHK mas a data final do plano for menor que a data atual
			if lExisPln .AND. lDtFmPln
			
				aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'N�o Atualizado. Plano(s) n�o est�o vigente(s).'})
			
			Elseif !lExisPln .AND. !lAtuPln
			
				aAdd(aLogTt,{aBenef[nY][2],aBenef[nY][3],aBenef[nY][7],'Dependente n�o cadastrado para este tipo de Plano.'})
				
			EndIf
				
		Endif
		
	Next nY

	if len(aLogTt) > 0
	
		//Chama rotina para gerar o excell com as informa��es de Log 
		FWMsgRun(,{|| GrvLog()},'Preenchendo o Arquivo de Log em Excel','Aguarde...' )
	
	EndIf
	
Return
/*
{Protheus.doc} GrvLog
Grava o log contendo o status da importa��o
@type function
@author 
@since 20/12/2016
@version P12.7
*/
Static Function GrvLog()

	Local oExcLog 	:= FWMSEXCEL():New()
	Local cAba1		:= 'Parametros'
	Local cTitulo1	:= 'Parametros Selecinados'
	Local cAba2		:= 'Informa��es'
	Local cTitulo2	:= 'Log de Importa��o de Carteirinha'
	Local nForLog	:= 0
	Local cNmArqEx	:= ''
	Local cCamiAux	:= Alltrim(aRet[3])
		
		//Aba - Parametros
		oExcLog:AddworkSheet(cAba1)
		oExcLog:AddTable (cAba1,cTitulo1)
		oExcLog:AddColumn(cAba1,cTitulo1,'Parametro',1,1)
		oExcLog:AddColumn(cAba1,cTitulo1,'Conte�do Informado',2,1)
		oExcLog:AddRow(cAba1,cTitulo1,{'Filial',aREt[1]})
		oExcLog:AddRow(cAba1,cTitulo1,{'Tipo ',iif(iif(Valtype(aRet[2]) =='C',Substring(aRet[2],1,1),StrZero(aRet[2],1)) =='1','1=M�dica','2=Odontol�gica')})
		oExcLog:AddRow(cAba1,cTitulo1,{'Caminho',cCamiAux})
	
		oExcLog:AddworkSheet(cAba2)
		oExcLog:AddTable (cAba2,cTitulo2)
		oExcLog:AddColumn(cAba2,cTitulo2,'Matr�cula',1,1)
		oExcLog:AddColumn(cAba2,cTitulo2,'Tipo do Benefici�rio',2,1)
		oExcLog:AddColumn(cAba2,cTitulo2,'Nome do Benefici�rio',2,1)
		oExcLog:AddColumn(cAba2,cTitulo2,'Status',2,1)
		
	For nForLog := 1 to len(aLogTt)

		oExcLog:AddRow(cAba2,cTitulo2,{aLogTt[nForLog][1],aLogTt[nForLog][2],aLogTt[nForLog][3],aLogTt[nForLog][4]})
			
	Next nForLog
				
	oExcLog:Activate()
			
	cNmArqEx	:=	cDrvAt+cDirect+dtos(dDatabase)+'_'+StrTran(time(),':','_')+"_"+cArqImp+"_log.xls"
	
	oExcLog:GetXMLFile(cNmArqEx)
	
	oExcLog:DeActivate()
				
	Aviso('LOG DE IMPORTA��O',"Verifique o log de importa��o ("+cNmArqEx+") para analisar os status de cada benefici�rio. ",{'OK'},1)
	 	
Return