#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100320()
Grava��o da solicita��o de aumento de postos\or�amento. 
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100320()

	Local cHtml   := ""
	Local oOrg
	Local oSolic
	
	Private cMsg
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	   	oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
		
		oOrg:cParticipantID   := ""
		oOrg:cVision          := HttpSession->aInfRotina:cVisao
	 	oOrg:cEmployeeFil     := HttpSession->aUser[2]
		oOrg:cRegistration    := HttpSession->RhMat
		oOrg:cEmployeeSolFil  := HttpSession->aUser[2]
		oOrg:cRegistSolic     := HttpSession->RhMat
	 
		If ValType(HttpSession->RHMat) != "U" .And. !Empty(HttpSession->RHMat)
			oOrg:cRegistration := HttpSession->RHMat
		EndIf   
				 
		If oOrg:GetStructure()
			oSolic := WSW0500308():New()
			WsChgURL(@oSolic,"W0500308.apw")
			
			oSolic:oWSSolicitQdr:cFilialQdr := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitQdr:cCodMatric := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitQdr:cFilSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitQdr:cMatSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitQdr:cVisaoOrg  := HttpSession->aInfRotina:cVisao
			oSolic:oWSSolicitQdr:cFilAprov  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cSupFilial
			oSolic:oWSSolicitQdr:cMatAprov  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cSupRegistration
			oSolic:oWSSolicitQdr:nNvlAprov  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:nLevelSup
			oSolic:oWSSolicitQdr:cEmpAprov	:= oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cSupEmpresa
			oSolic:oWSSolicitQdr:cEmpresa	:= GetEmpFun() 
	
			oSolic:oWSSolicitQdr:cCodDepto	:= HttpGet->cDptoEsc
			oSolic:oWSSolicitQdr:cCdFuncao	:= HttpPost->cFuncao
			oSolic:oWSSolicitQdr:cCdCargo	:= HttpPost->cCargo	
			oSolic:oWSSolicitQdr:cCdCentCt	:= HttpPost->cCcusto
			oSolic:oWSSolicitQdr:cSalario	:= HttpPost->cSalario
			oSolic:oWSSolicitQdr:cTpContr	:= HttpPost->cTipCon
			oSolic:oWSSolicitQdr:cQntQdro	:= HttpPost->cQtd
			oSolic:oWSSolicitQdr:cTpPosto	:= HttpPost->cTipPos
			oSolic:oWSSolicitQdr:cObserv	:= HttpPost->cObs
			
			oSolic:oWSSolicitQdr:cNvPost    := HttpGet->cNvPosto
			
			If HttpGet->cNvPosto == "2"
				oSolic:oWSSolicitQdr:cCdPost := HttpGet->cPostEsc
			EndIf
			
			If oSolic:InsSolQdro()
				If oSolic:OWSINSSOLQDRORESULT:CLRETORN == "TRUE"
					cMsg := "Solicita��o "+ oSolic:OWSINSSOLQDRORESULT:cCdSolic +" efetuada com sucesso"
					cMsg += " na data de " + oSolic:OWSINSSOLQDRORESULT:cDataSol
				Else
					cMsg := "Erro no Cadastro"
				EndIf
			EndIf
	
		EndIf
		
		cHtml := ExecInPage("F0100312")
	
	WEB EXTENDED END

Return cHtml