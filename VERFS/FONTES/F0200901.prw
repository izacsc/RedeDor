#Include "Protheus.ch"
#INCLUDE "Topconn.ch"
#INCLUDE 'parmtype.ch'
/*/{Protheus.doc} F0200901
Relat�rio de Evolu��o de Pre�os
@Project MAN00000463301_EF_009
@type function
@author queizy.nascimento
@since 10/10/2016
@version 1.0
/*/
User Function F0200901()
	Local oReport := nil
	//Define o nome da perg
	Private cPerg	  := "FSW0200901"
	//Monta as Pergs
	//U_FSW029()
	FSW0200901()
	
	//Chama a pergunta
	Pergunte(cPerg, .T.)
	
	//Monta estrutura do report
	oReport := RptDef(cPerg)
	
	//Imprimi
	oReport:PrintDialog()
	
Return

Static Function ReportPrint(oReport)
	
	Local oSection1 := oReport:Section(1)
	Local cProdIni := Space(TamSX3("B1_COD")[1])
	Local cProdFim := Space(TamSX3("B1_COD")[1])
	Local cFilIni := Space(TamSX3("C7_FILIAL")[1])
	Local cFilFim := Space(TamSX3("C7_FILIAL")[1])
	Local cGrpdIni := Space(TamSX3("B1_GRUPO")[1])
	Local cGrpdFim := Space(TamSX3("B1_GRUPO")[1])
	Local aAux		:= {}
	Local nx := na := 0	
	Private aPer1:={}
	Private aPer2:={}
	Private aRelImp:={}
	Private cProdRet,cFilRet,cGrupoProd
	Private cPer1 := DtoS(MV_PAR04)+"/"+DtoS(MV_PAR05)
	Private cPer2 := DtoS(MV_PAR06)+"/"+DtoS(MV_PAR07)
	
	//Tratativa dos parametros para passar a query
	cFilRet   := Alltrim(MV_PAR01)
	cGrupoProd:= Alltrim(MV_PAR02)
	cProdRet  := Alltrim(MV_PAR03)
	
	If at('-',cFilRet) > 0
		cFilRet := StrTran(cFilRet,";","")
		cFilIni := Substr(cFilRet,1, at('-', cFilRet) - 1)
		cFilFim := Substr(cFilRet,at('-', cFilRet) + 1,TamSX3("C7_FILIAL")[1] )
	Else
		aAux := StrtoKarr(cFilRet,';')
		cFilRet:= ""
		For nX := 1 To Len(aAux)
			If Alltrim(aAux[nX]) != ""
				cFilRet += "'"+aAux[nX]+"',"
			Endif
		Next
		If Right(AllTrim(cFilRet),1) == ','
			cFilRet := SubStr(AllTrim(cFilRet),1,Len(AllTrim(cFilRet))-1)
		Endif
	EndIf
	aAux := {}
	If at('-',cProdRet) > 0
		cProdRet:= StrTran(cProdRet,";","")
		cProdIni := Substr(cProdRet,1, at('-', cProdRet) - 1)
		cProdFim := Substr(cProdRet,at('-', cProdRet) + 1,TamSX3("B1_COD")[1] )
	Else
		aAux := StrtoKarr(cProdRet,';')
		cProdRet:= ""
		For nX := 1 To Len(aAux)
			If Alltrim(aAux[nX]) != ""
				cProdRet += "'"+aAux[nX]+"',"
			Endif
		Next
		If Right(AllTrim(cProdRet),1) == ','
			cProdRet := SubStr(AllTrim(cProdRet),1,Len(AllTrim(cProdRet))-1)
		Endif
	EndIf
	aAux := {}
	If at('-',cGrupoProd) > 0
		cGrupoProd:= StrTran(cGrupoProd,";","")
		cGrpIni := Substr(cGrupoProd,1, at('-', cGrupoProd) - 1)
		cGrpFim := Substr(cGrupoProd,at('-', cGrupoProd) + 1,TamSX3("B1_GRUPO")[1] )
	Else
		aAux := StrtoKarr(cGrupoProd,';')
		cGrupoProd:= ""
		For nX := 1 To Len(aAux)
			If Alltrim(aAux[nX]) != ""
				cGrupoProd += "'"+aAux[nX]+"',"
			Endif
		Next
		If Right(AllTrim(cGrupoProd),1) == ','
			cGrupoProd := SubStr(AllTrim(cGrupoProd),1,Len(AllTrim(cGrupoProd))-1)
		Endif
	EndIf

	//Monta a query
	cQuery:=" SELECT C7_FILIAL EMPRESA, B1_COD CODPRO, B1_GRUPO GRUPO, B1_DESC DESCRI, '1' PER, '"+DTOS(MV_PAR04)+"/"+DTOS(MV_PAR05)+ "' DEATE, "+CRLF
	cQuery+=" SUM(C7_QUANT) QUANTTOT, SUM(C7_TOTAL) VALTOT, SUM(C7_TOTAL)/SUM(C7_QUANT) MEDIA "+CRLF
	cQuery+=" FROM "+RETSQLNAME('SC7')+"  C7 "+CRLF
	cQuery+=" INNER JOIN "+RETSQLNAME('SB1')+" B1 ON B1_FILIAL = '"+xFilial("SB1")+"' AND B1.D_E_L_E_T_  = ' ' AND C7_PRODUTO = B1_COD "+CRLF
	cQuery+=" WHERE C7.D_E_L_E_T_ = '' "+CRLF
	//Se o parametro Filial n�o estiver vazio ele adiciona essa linha a query,caso o contrario
	//a query ira rodar sem filtro
	If  !Empty (MV_PAR01)
		If Empty(cFilIni)
			cQuery+= " AND C7_FILIAL IN ("+cFilRet+") "+CRLF
		Else
			cQuery+=" AND C7_FILIAL BETWEEN '"+cFiLIni+"' AND '"+cFilFim+"' "+CRLF
		EndIf
	EndIf
	cQuery+=" AND C7_EMISSAO BETWEEN '"+DTOS(MV_PAR04)+"' AND '"+DTOS(MV_PAR05)+"' "+CRLF
	If !Empty (MV_PAR02)
		If Empty(cGrpIni)
			cQuery+= " AND B1_GRUPO IN ("+cGrupoProd+") "+CRLF
		Else
			cQuery+=" AND B1_GRUPO BETWEEN '"+cGrpIni+"' AND '"+cGrpFim+"' "+CRLF
		EndIf
	EndIf
	If !Empty (MV_PAR03)
		//Se o parametro Produto n�o estiver vazio ele adiciona essa linha a query,caso o contrario
		//a query ira rodar sem filtro
		If Empty(cProdIni)
			cQuery+=" AND C7_PRODUTO IN ("+cProdRet+") "+CRLF
		Else
			cQuery+=" AND C7_PRODUTO BETWEEN '"+cProdIni+"' AND '"+cProdFim+"' "+CRLF
		EndIf
	EndIf
	cQuery+=" GROUP BY C7_FILIAL, B1_COD, B1_GRUPO,B1_DESC"+CRLF
	cQuery+=" UNION ALL"+CRLF
	cQuery+=" SELECT C7_FILIAL EMPRESA, B1_COD CODPRO, B1_GRUPO GRUPO, B1_DESC DESCRI, '2' PER, '"+DTOS(MV_PAR06)+"/"+DTOS(MV_PAR07)+"' DEATE, "+CRLF
	cQuery+=" SUM(C7_QUANT) QUANTTOT,  SUM(C7_TOTAL) VALTOT, SUM(C7_TOTAL)/SUM(C7_QUANT) MEDIA "+CRLF
	cQuery+=" FROM "+RETSQLNAME('SC7')+" C7 "+CRLF
	cQuery+=" INNER JOIN "+RETSQLNAME('SB1')+"  B1 ON B1_FILIAL = '"+xFilial("SB1")+"' AND B1.D_E_L_E_T_  = ' ' AND C7_PRODUTO = B1_COD "+CRLF
	cQuery+=" WHERE C7.D_E_L_E_T_ = '' "+CRLF
	If  !Empty (MV_PAR01)
		If Empty(cFilIni)
			cQuery+= " AND C7_FILIAL IN ("+cFilRet+") "+CRLF
		Else
			cQuery+=" AND C7_FILIAL BETWEEN '"+cFiLIni+"' AND '"+cFilFim+"' "+CRLF
		EndIf
	EndIf
	cQuery+=" AND C7_EMISSAO BETWEEN '"+DTOS(MV_PAR06)+"' AND '"+DTOS(MV_PAR07)+"' "+CRLF
	If !Empty (MV_PAR02)
		If Empty(cGrpIni)
			cQuery+= " AND B1_GRUPO IN ("+cGrupoProd+") "+CRLF
		Else
			cQuery+=" AND B1_GRUPO BETWEEN '"+cGrpIni+"' AND '"+cGrpFim+"' "+CRLF
		EndIf
	EndIf
	If !Empty (MV_PAR03)
		//Se o parametro Produto n�o estiver vazio ele adiciona essa linha a query,caso o contrario
		//a query ira rodar sem filtro
		If Empty(cProdIni)
			cQuery+=" AND C7_PRODUTO IN ("+cProdRet+") "+CRLF
		Else
			cQuery+=" AND C7_PRODUTO BETWEEN '"+cProdIni+"' AND '"+cProdFim+"' "+CRLF
		EndIf
	EndIf
	cQuery+=" GROUP BY C7_FILIAL, B1_COD, B1_GRUPO,B1_DESC "+CRLF
	cQuery := ChangeQuery(cQuery)
	If Select("QRY") > 0
		Dbselectarea("QRY")
		QRY->(DbClosearea())
	Endif
	TcQuery cQuery New ALIAS "QRY"
	dbSelectArea("QRY")
	dbGotop()
	
	oReport:SetMeter(QRY->(LastRec()))
	oSection1:Init()
	oReport:IncMeter()
	
	While QRY->(!Eof())
		If oReport:Cancel()
			Exit
		EndIf
		// Array com os dados do Periodo 1
		If QRY->PER == '1'
			aAdd(aPer1,{QRY->EMPRESA, QRY->GRUPO, QRY->CODPRO, QRY->DESCRI, QRY->DEATE, QRY->QUANTTOT, QRY->VALTOT, QRY->MEDIA})
			// Array com os dados do Periodo 2
		Else
			aAdd(aPer2,{QRY->EMPRESA, QRY->GRUPO, QRY->CODPRO, QRY->DESCRI, QRY->DEATE, QRY->QUANTTOT, QRY->VALTOT, QRY->MEDIA,.F.})
		Endif
		QRY->(dbSkip())
	Enddo
	
	//---------
	//aRelImp[nx,1] -Filial
	//aRelImp[nx,2] -Grupo de Material
	//aRelImp[nx,3] -Codigo Produto
	//aRelImp[nx,4] -Descri��o do Material
	//aRelImp[nx,5] -Periodo1
	//aRelImp[nx,6] -Quantidade Total
	//aRelImp[nx,7] -Valor Total
	//aRelImp[nx,8] -Valor Unit�rio Periodo1
	//aRelImp[nx,9] -Periodo2
	//aRelImp[nx,10] -Quantidade Total
	//aRelImp[nx,11] -Valor Total
	//aRelImp[nx,12] -Valor Unit�rio Periodo2
	//aRelImp[nx,13] -Valor de Diferen�a do Ensaio
	//aRelImp[nx,14] -% de Diferen�a
	//aRelImp[nx,15] -Observa��o
	For nx:=1 to Len (aPer1)
		If AScan(aPer2,{|x| x[1] == aPer1[nx,1] .And. x[3] == aPer1[nx,3] .And. x[5] == cPer2 }) > 0
			aAdd(aRelImp,{aPer1[nx,1], aPer1[nx,2], aPer1[nx,3], aPer1[nx,4], aPer1[nx,5], aPer1[nx,6], aPer1[nx,7],aPer1[nx,8],'','','','','','','Compra P1/P2',})
		Else
			aAdd(aRelImp,{aPer1[nx,1], aPer1[nx,2], aPer1[nx,3], aPer1[nx,4], aPer1[nx,5], aPer1[nx,6], aPer1[nx,7],aPer1[nx,8],'',0, 0,0,0,0,'Compra P1',})
		EndIf
	Next nx
	
	For nx:=1 to Len (aPer2)
		ny := AScan(aPer1,{|x| x[1] == aPer2[nx,1] .And. x[3] == aPer2[nx,3] .And. x[5] == cPer1 })
		If ny <= 0
			aAdd(aRelImp,{aPer2[nx,1],aPer2[nx,2],aPer2[nx,3],aPer2[nx,4],'', 0, 0, 0, aPer2[nx,5], aPer2[nx,6],aPer2[nx,7],aPer2[nx,8], 0, 0,'Compra P2',})
		Else
			aRelImp[ny,09] := aPer2[nx,05]
			aRelImp[ny,10] := aPer2[nx,06]
			aRelImp[ny,11] := aPer2[nx,07]
			aRelImp[ny,12] := aPer2[nx,08]
			aRelImp[ny,13] := Round(Iif( aRelImp[ny,08] == aPer2[nx,08], 0, Iif( aRelImp[ny,06] == aPer2[nx,06], aPer2[nx,07] - aRelImp[ny,07], (aRelImp[ny,06] * aPer2[nx,08]) - aRelImp[ny,07] )),2)
			aRelImp[ny,14] := Round((aPer2[nx,08] - aRelImp[ny,08]) /  aRelImp[ny,08] *100, 2)
			If aRelImp[ny,06] != aPer2[nx,06] .And. aRelImp[ny,08] != aPer2[nx,08]
				aRelImp[ny,15] := Alltrim(aRelImp[ny,15])+" - Qtd. Dif. "
			EndIf
		EndIf
	Next nx
	//----------------------------
	//IMPRESS�O DO  RELATORIO
	//
	//
	//-----------------------------
	
	//aRelImp[nx,1] -Filial
	//aRelImp[nx,2] -Grupo Material
	//aRelImp[nx,3] -Codigo do Produto
	//aRelImp[nx,4] -Descri��o do Produto
	//aRelImp[nx,5] -Periodo1
	//aRelImp[nx,6] -Quantidade Total
	//aRelImp[nx,7] -Valor Total
	//aRelImp[nx,8] -Valor Unit�rio Periodo1
	//aRelImp[nx,9] -Periodo2
	//aRelImp[nx,10] -Quantidade Total
	//aRelImp[nx,11] -Valor Total
	//aRelImp[nx,12] -Valor Unit�rio Periodo2
	//aRelImp[nx,13] -Valor de Diferen�a do Ensaio
	//aRelImp[nx,14] -% de Diferen�a
	//aRelImp[nx,15] -Observa��o
	
	For na:=1 to Len (aRelImp)
		oSection1:Cell("FILIAL"):SetValue(aRelImp[na,1])
		oSection1:Cell("GRUPO" ):SetValue(aRelImp[na,2])
		oSection1:Cell("CODIGO" ):SetValue(aRelImp[na,3])
		oSection1:Cell("DESC"):SetValue(aRelImp[na,4])
		oSection1:Cell("DEATE"):SetValue(If(Empty(aRelImp[na,5]),aRelImp[na,5],Substr(aRelImp[na,5],7,2)+"/"+Substr(aRelImp[na,5],5,2)+"/"+Substr(aRelImp[na,5],1,4)+"-"+Substr(aRelImp[na,5],16,2)+"/"+Substr(aRelImp[na,5],14,2)+"/"+Substr(aRelImp[na,5],10,4)))
		oSection1:Cell("QUANTTOT"):SetValue(aRelImp[na,6])
		oSection1:Cell("VALTOT"):SetValue(aRelImp[na,7])
		oSection1:Cell("VALUNI"):SetValue(aRelImp[na,8])
		oSection1:Cell("DEATE2"):SetValue(If(Empty(aRelImp[na,9]),aRelImp[na,9],Substr(aRelImp[na,9],7,2)+"/"+Substr(aRelImp[na,9],5,2)+"/"+Substr(aRelImp[na,9],1,4)+"-"+Substr(aRelImp[na,9],16,2)+"/"+Substr(aRelImp[na,9],14,2)+"/"+Substr(aRelImp[na,9],10,4)))
		oSection1:Cell("QUANTTOT1"):SetValue(aRelImp[na,10])
		oSection1:Cell("VALTOT1"):SetValue(aRelImp[na,11])
		oSection1:Cell("VALUNI1"):SetValue(aRelImp[na,12])
		oSection1:Cell("DIFENS"):SetValue(aRelImp[na,13])
		oSection1:Cell("PERDIF"):SetValue(aRelImp[na,14])
		oSection1:Cell("Obs"):SetValue(aRelImp[na,15])
		oSection1:Printline()
	Next na
	oSection1:Finish()
	
Return

/*/{Protheus.doc}FSW0200901
(long_description)
@type function
@author queizy.nascimento
@since 10/10/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function FSW0200901() //Monta  perguntas
	Private aHelpPor := {}
	Private aHelpPor2 := {}
	
	aAdd( aHelpPor, "Selecione a Filial.  ")
	
	PutSx1( cPerg, "01","Filial" ,"","","mv_ch1",;
		"C",20,0,0,"R","","FSSM01","","S","MV_PAR01","","","","C7_FILIAL","","","","","",;
		"","","","","","","",aHelpPor,,)
	aHelpPor := {}
	
	aAdd( aHelpPor, "Selecione o Grupo de Material.  ")
	
	PutSx1( cPerg, "02","Grupo de Material" ,"","","mv_ch2",;
		"C",60,0,0,"R","","FSSBM1"," ","S","MV_PAR02","","","","BM_GRUPO","","","","","",;
		"","","","","","","",aHelpPor,,)
	aHelpPor := {}
	
	aAdd( aHelpPor, "Selecione o Material.  ")
	PutSx1(cPerg, "03","Do Produto?"      ,"Do Produto?"      ,"Do Produto?"      ,;
		"mv_ch3","C",30,0,,"R","","FSNNR2","","S","MV_PAR03","","","","B1_COD","","","","","","","","","","","","")
	aHelpPor	:= {}
	aHelpPor2	:= {}
	
	aAdd( aHelpPor, "Selecione o Per�odo 1 De")
	aAdd( aHelpPor2, "Selecione o Per�odo 1 Ate")
	
	PutSx1( cPerg, "04","Per�odo 1 De"   ,"" ,"",;
		"mv_ch4","D",8,0,0,"G","NaoVazio()","","","S","MV_PAR04","","","","","","","","","",;
		"","","","","","","",aHelpPor,,)
	
	PutSx1( cPerg, "05","Per�odo 1 At�","",""  ,;
		"mv_ch5","D",8,0,0,"G","U_F0200902(MV_PAR04, MV_PAR05,1)","","","S","MV_PAR05","","","","","","","","","",;
		"","","","","","","",aHelpPor2,,)
	
	aHelpPor	:= {}
	aHelpPor2	:= {}
	aAdd( aHelpPor, "Selecione o Per�odo 2 De")
	aAdd( aHelpPor2, "Selecione o Per�odo 2 At�")
	
	PutSx1( cPerg, "06","Per�odo 2 De"   ,"" ,"",;
		"mv_ch6","D",8,0,0,"G","U_F0200902(MV_PAR05, MV_PAR06,2)","","","S","MV_PAR06","","","","","","","","","",;
		"","","","","","","",aHelpPor,,)
	
	PutSx1( cPerg, "07","Per�odo 2 At�","",""  ,;
		"mv_ch7","D",8,0,0,"G","U_F0200902(MV_PAR06, MV_PAR07,3)","","","S","MV_PAR07","","","","","","","","","",;
		"","","","","","","",aHelpPor2,,)
Return

/*/{Protheus.doc} RptDef
Function que monta  estrutura do relatorio
	@type function
	@author queizy.nascimento
	@since 27/09/2016
	@version 1.0
	/*/
Static Function RptDef(cNome)
	Local oReport := Nil
	Local oSection1:= Nil
	Local oBreak
	Local oFunction
	
	/*Sintaxe: TReport():New(cNome,cTitulo,cPerguntas,bBlocoCodigo,cDescricao)*/
	oReport := TReport():New(cNome,"Relat�rio Evolucao de Pre�o",cNome,{|oReport| ReportPrint(oReport)},"Descri��o do meu relat�rio")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	
	oSection1:= TRSection():New(oReport, "PROD", {"SB1"}, , .F., .T.)
	TRCell():New(oSection1,"FILIAL"	  ,"QRY","Filial"  		,"@!",30,.T.)
	TRCell():New(oSection1,"GRUPO"    ,"QRY","Grp. Material","@!",20,.T.)
	TRCell():New(oSection1,"CODIGO"	  ,"QRY","Cod. Produto ","@!",50,.T.)
	TRCell():New(oSection1,"DESC"	  ,"QRY","Descri�ao"  	,"@!",46,.T.)
	TRCell():New(oSection1,"DEATE"	  ,"QRY","Periodo 1"  	,"@!",70)
	TRCell():New(oSection1,"QUANTTOT" ,"QRY","Quantidade"  	,"@E 99999999",20)
	TRCell():New(oSection1,"VALTOT"	  ,"QRY","Val. Total"  	,"@E 999999999.99",25)
	TRCell():New(oSection1,"VALUNI"	  ,"QRY","Val. Unit."  	,"@E 99999999.99",22,.T.)
	TRCell():New(oSection1,"DEATE2"	  ,"QRY","Periodo 2"  	,"@!",70)
	TRCell():New(oSection1,"QUANTTOT1","QRY","Quantidade"  	,"@E 99999999",20)
	TRCell():New(oSection1,"VALTOT1"  ,"QRY","Val. Total"  	,"@E 999999999.99",25)
	TRCell():New(oSection1,"VALUNI1"  ,"QRY","Val. Unit."  	,"@E 99999999.99",22,.T.)
	TRCell():New(oSection1,"DIFENS"	  ,"QRY","Val. Difer."  ,"@E 999999999.99",25)
	TRCell():New(oSection1,"PERDIF"	  ,"QRY","%Diferen�a"  	,"@E 9999.99%",19)
	TRCell():New(oSection1,"Obs"      ,"QRY","Observa�ao"   ,"@!",60,.T.)
	
Return(oReport)