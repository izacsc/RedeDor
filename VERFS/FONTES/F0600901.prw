#Include "PROTHEUS.CH"

/*/{Protheus.doc}  F0600901

@Author Lucas Graglia
@Since  31/10/2016
@Sample U_F0600901(cFunc,nRecno,cAliasTrb,cChave,cObs,cDatEnv)
@Return cId,	Caso Rotina e Recno foram passados como parametro Retorna o ID

@Param cFunc		,	Nome  da (Rotina, Fun��o) que chamou a Rotina Hist�rico de Integra��o
@Param nRecno		,	Recno do Registro de Integra��o
@Param cAliasTrb	,	Alias da (Rotina, Fun��o) que chamou a Rotina Hist�rico de Integra��o
@Param cChave		,	Chave do Alias da (Rotina, Fun��o) que chamou a Rotina Hist�rico de Integra��o
@Param cDatEnv		, 	Data Envio da Integra��o
@Param cOper		, 	Opera��o
@Param cObs		,	Observa��o


@Obs Fun��o Respons�vel por Gravar Hist�rico de LOG de Integra��o

@Project MAN0000007423040_EF_009

/*/

User Function F0600901(cFunc,nRecno,cAliasTrb,cChave,cObs,dDatEnv,cOper)
	
	Local cData    := Date()
	Local cHora    := Time()
	Local cUsuario := __cUserID	
	Local cID      := FWUUIDV4(.F.)
	Local lJob		 := ISBLIND()
	
	Default cFunc			:= Funname()
	Default nRecno		:= 0
	Default cAliasTrb		:= ""
	Default cChave		:= ""
	Default cObs			:= ""	
	Default dDatEnv		:= Date()
	Default cOper 		:= ""	
		
	RecLock("PA6",.T.)
		
	PA6->PA6_FILIAL  := xFilial("PA6")		
	PA6->PA6_ID      := cID
	PA6->PA6_DATA    := cData
	PA6->PA6_HORA    := cHora
	If lJob
		PA6->PA6_USUA    := 'JOB'
	Else
		PA6->PA6_USUA    := cUsuario
	EndIf
	PA6->PA6_FUNC    := cFunc
	PA6->PA6_RECNOT  := nRecno
	PA6->PA6_ALIAS   := cAliasTrb
	PA6->PA6_CHALIAS := cChave
	PA6->PA6_DATENV  := dDatEnv
	PA6->PA6_OPERAC  := cOper
	PA6->PA6_OBS     := cObs
		
	PA6->(MsUnlock())	
	
Return(cID)