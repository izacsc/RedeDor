#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0100408
Tela de Tipos de Pagamento.
	
@author 	alexandre.arume
@since 		18/08/2016
@version 	1.0		
@Project	MAN00000462901_EF_004

@return ${return}, ${return_description}

/*/
User Function F0100408()

	Local oBrowse := FWmBrowse():New()
	
	oBrowse:SetAlias("P02")
	oBrowse:SetMenuDef("F0100408")
	oBrowse:SetDescription("Tipos de Pagamento")
	oBrowse:Activate()

Return NIL


/*/{Protheus.doc} MenuDef
Menus e bot�es da tela.
	
@author 	alexandre.arume
@since 		18/08/2016
@version 	1.0
@Project	MAN00000462901_EF_004

@return Menu padr�o.

/*/
Static Function MenuDef()

Return FWMVCMenu("F0100408")


/*/{Protheus.doc} ModelDef
Defini��o do modelo.
	
@author 	alexandre.arume
@since 		18/08/2016
@version 	1.0
@Project	MAN00000462901_EF_004

@return ${oModel}, Modelo de dados.

/*/
Static Function ModelDef()
	
	// Variavel do objeto da estrutura do modelo.
	Local oStru := FwFormStruct(1, "P02")
	
	// Remo��o do campo filial da estrutura.
	oStru:RemoveField('P02_FILIAL')
	
	oModel := MPFormModel():New('M0100408', /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/)
	//oModel := MPFormModel():New( 'COMP012MODEL', /*bPreValidacao*/, { | oMdl | COMP012POS( oMdl ) }, /*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	oModel:AddFields('P02MASTER', /*cOwner*/, oStru, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/)
	
	// Adiciona a descricao do Modelo de Dados
	oModel:SetDescription('Tipo de Pagamento')
	
	// Adiciona a descricao do Componente do Modelo de Dados
	oModel:GetModel('P02MASTER'):SetDescription('Dados do Tipo de Pagamento')
	
	// Define a chave prim�ria do modelo.
	oModel:GetModel("P02MASTER"):SetPrimaryKey({"P02_FILIAL", "P02_COD"})
	
	// Liga a valida��o da ativacao do Modelo de Dados
	//oModel:SetVldActive( { | oModel | COMP012ACT( oModel ) } )

Return oModel


/*/{Protheus.doc} ViewDef
Defini��o da tela.
	
@author 	alexandre.arume
@since 		18/08/2016
@version 	1.0
@Project	MAN00000462901_EF_004

@return ${oView}, Objeto da tela.

/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	Local oModel   	:= FWLoadModel('F0100408')
	Local oStru 	:= FwFormStruct(2, "P02")
	Local oView		:= Nil
	
	// Cria o objeto de View
	oView := FWFormView():New()
	
	// Define qual o Modelo de dados ser� utilizado
	oView:SetModel(oModel)
	
	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('VIEW_P02', oStru, 'P02MASTER')
	
	// Criar um "box" horizontal para receber algum elemento da view
	oView:CreateHorizontalBox('TELA' , 100)
	
	// Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView('VIEW_P02', 'TELA')

Return oView
