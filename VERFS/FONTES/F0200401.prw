#INCLUDE 'PROTHEUS.CH'
//-----------------------------------------------------------------------------
/*/{Protheus.doc} F0200401
Relat�rio de Rastreabilidade de SC
@type User function
@author Cristiane Thomaz Polli
@since 04/07/2016
@version P12.1.7
@Project MAN00000463301_EF_004
@return ${return}, ${n�o h�}
/*/
//-----------------------------------------------------------------------------
User Function F0200401()

	Private oReport	 := Nil
	
	    If TRepInUse()
		
			oReport := ReportDef()
			
			If oReport == Nil
		
				Aviso('Relat�rio','Relat�rio Cancelado.',{"OK"}) 
		
			Else
		
				oReport:PrintDialog()
		
			Endif
		
		EndIf

Return
//-----------------------------------------------------------------------------
/*/{Protheus.doc} ReportDef
Fun��o onde devem ser criados os componentes de impress�o, as se��es e as c�lulas, 
os totalizadores e demais componentes que o usu�rio poder� personalizar 
no relat�rio.
@type Static function
@author Cristiane Thomaz Polli
@since 05/07/2016
@version  P12.1.7
@Project MAN00000463301_EF_004
@return ${oReport}, ${objeto}
/*/
//-----------------------------------------------------------------------------
Static Function ReportDef()
	
	Local cTitulo := 'Relat�rio de Rastreabilidade de SC'
	Local cPergAtu:= 'FSW0200401'
	Local aColImpr:= {}
	Local nFor    :=0

		if pergunte(cPergAtu,.T.)
			
			//Valida o parametro Filial
			if Empty(MV_PAR01)
			
				Aviso('N�o Preenchido','Informe no m�nimo uma filial. Campo obrigat�rio!',{"OK"}) 
			
				Return oReport
				
			Else
				
				If !(U_F0200403())
	
					Return oReport
				
				EndIf			
					
				
			EndIf
			
			//Valida o parametro Setor
			If Empty(MV_PAR02)
			
				Aviso('N�o Preenchido','Informe no m�nimo uma setor. Campo obrigat�rio!',{"OK"}) 
			
				Return oReport

			Else
			
					If !(U_F0200402())
				
						Return oReport
						
					EndIf
				
			EndIf		
			
			//Valida os parametros de data
			if MV_PAR03 > MV_PAR04
			
				Aviso('Inv�lido Per�odo','Data final  informada � menor que a data inicial informada. Favor verificar as datas informadas!',{"OK"}) 
			
				Return oReport
				
			EndIf
				
			//����������������������������������������������������������������������������Ŀ
			//�funcao para trazer em array as regras de colunas conforme o tipo de pesquisa�
			//������������������������������������������������������������������������������
			aColImpr := F02004Col(aColImpr)
			
			//������������������������������������������������������������������������Ŀ
			//�Criacao do componente de impressao                                      �
			//�                                                                        �
			//�TReport():New                                                           �
			//�ExpC1 : Nome do relatorio                                               �
			//�ExpC2 : Titulo                                                          �
			//�ExpC3 : Pergunte                                                        �
			//�ExpB4 : Bloco de codigo que sera executado na confirmacao da impressao  �
			//�ExpC5 : Descricao                                                       �
			//�                                                                        �
			//��������������������������������������������������������������������������
			oReport:= TReport():New('F0200401_'+dtos(dDatabase)+'_'+StrTran(time(),':','_'),cTitulo,cPergAtu, {|oReport| ReportPrint(oReport,cPergAtu)},cTitulo)
			oReport:SetPortrait(.F.)
			oReport:SetLandscape(.T.)
									
			//������������������������������������������������������������������������Ŀ
			//�Criacao da secao utilizada pelo relatorio                               �
			//�                                                                        �
			//�TRSection():New                                                         �
			//�ExpO1 : Objeto TReport que a secao pertence                             �
			//�ExpC2 : Descricao da se�ao                                              �
			//�ExpA3 : Array com as tabelas utilizadas pela secao. A primeira tabela   �
			//�        sera considerada como principal para a se��o.                   �
			//�ExpA4 : Array com as Ordens do relat�rio                                �
			//�ExpL5 : Carrega campos do SX3 como celulas                              �
			//�        Default : False                                                 �
			//�ExpL6 : Carrega ordens do Sindex                                        �
			//�        Default : False                                                 �
			//�                                                                        �
			//��������������������������������������������������������������������������
			oSec1 := TRSection():New(oReport,'Titulos',{"SC1"},{'Origem unica'},/*Campos do SX3*/,/*Campos do SIX*/)	
			oSec1:SetTotalInLine(.F.)
			
			//������������������������������������������������������������������������Ŀ
			//�Criacao da celulas da secao do relatorio                                �
			//�TRCell():New                                                            �
			//�ExpO1 : Objeto TSection que a secao pertence                            �
			//�ExpC2 : Nome da celula do relat�rio. O SX3 ser� consultado              �
			//�ExpC3 : Nome da tabela de referencia da celula                          �
			//�ExpC4 : Titulo da celula                                                �
			//�        Default : X3Titulo()                                            �
			//�ExpC5 : Picture                                                         �
			//�        Default : X3_PICTURE                                            �
			//�ExpC6 : Tamanho                                                         �
			//�        Default : X3_TAMANHO                                            �
			//�ExpL7 : Informe se o tamanho esta em pixel                              �
			//�        Default : False                                                 �
			//�ExpB8 : Bloco de c�digo para impressao.                                 �
			//�        Default : ExpC2                                                 �
			//�                                                                        �
			//��������������������������������������������������������������������������
			
			For nFor := 1 to len(aColImpr)
				TRCell():New(oSec1,aColImpr[nFor][1],"QRY",aColImpr[nFor][2],/*Picture*/,aColImpr[nFor][3],/*lPixel*/,aColImpr[nFor][4])
			Next nFor	
									
			//������������������������������������������������������������������������Ŀ
			//� Impressao do Cabecalho no top da pagina                                �
			//��������������������������������������������������������������������������
			oReport:Section(1):SetHeaderPage()	
			
		EndIf
		
Return oReport
//-----------------------------------------------------------------------------
/*/{Protheus.doc} F02004Col
Retorna array com os titulos das colunas a serem exibidas no rel�torio.
@type Static function
@author Cristiane Thomaz Polli
@since 05/07/2016
@version 12.1.7
@param aColImpr, array, (array vazio para ser retornado com os campos a exibir)
@Project MAN00000463301_EF_004
@return ${aColImpr}, ${Retorna os campos a serem exibidos.}
/*/
//-----------------------------------------------------------------------------
Static Function F02004Col(aColImpr)

	Local aAreaSX3	:= SX3->(GetArea())
	
		aAdd(aColImpr,{'cFilC1'		,'Estabelecimento'			,TamSx3('C1_FILIAL')[1]			,Nil})
		aAdd(aColImpr,{'cSetor'		,'Cod.Setor'				,TamSx3('C1_LOCAL')[1]			,Nil})
		aAdd(aColImpr,{'cCodProd'	,'C�d.Material'				,TamSx3('C1_PRODUTO')[1]		,Nil})
		aAdd(aColImpr,{'cDesProd'	,'Descri��o'				,TamSx3('C1_DESCRI')[1]			,Nil})
		aAdd(aColImpr,{'cSolic'		,'Solicitante'				,TamSx3('C1_SOLICIT')[1]		,Nil})
		aAdd(aColImpr,{'cNumSolic'	,'N�mero da Solic.'			,TamSx3('C1_NUM')[1]			,Nil})
		aAdd(aColImpr,{'cTpSolic'	,'Tipo de Solic.'			,TamSx3('C1_TPSC')[1]			,Nil})
		aAdd(aColImpr,{'cMotSolic'	,'Motivo da Solic.'			,10/*TamSx3('C1_XMOTIVO')[1]*/	,Nil})
		aAdd(aColImpr,{'dEmisSoli'	,'Emissao da Solic.'		,TamSx3('C1_EMISSAO')[1]		,{||(StoD(dEmisSoli))}})
		aAdd(aColImpr,{'DtLib'		,'Data Libera��o'			,TamSx3('CR_DATALIB')[1]		,{||(StoD(DtLib))}})
		aAdd(aColImpr,{'cComprador'	,'Comprador'				,TamSx3('C7_USER')[1]			,Nil})
		aAdd(aColImpr,{'cNumPC'		,'Pedido de Compras'		,TamSx3('C7_NUM')[1]			,Nil})
		aAdd(aColImpr,{'dEmisPC'	,'Emiss�o PC'				,TamSx3('C7_EMISSAO')[1]		,{||(StoD(dEmisPC))}})
		aAdd(aColImpr,{'DtLib'		,'Data Aprova��o'			,TamSx3('CR_DATALIB')[1]		,{||(StoD(DtLib))}})
		aAdd(aColImpr,{'cNumMed'	,'Numero Medi��o'			,TamSx3('CND_NUMMED')[1]		,Nil})
		aAdd(aColImpr,{'dDtMed'		,'Data da Medi��o'			,TamSx3('CND_DTINIC')[1]		,{||(StoD(dDtMed))}})
		aAdd(aColImpr,{'DtLib'		,'Aprova��o Medi��o'		,TamSx3('CR_DATALIB')[1]		,{||(StoD(DtLib))}})
		aAdd(aColImpr,{'cNotaFis'	,'Nota Fiscal'				,TamSx3('F1_DOC')[1]			,Nil})
		aAdd(aColImpr,{'cSerNF'		,'S�rie da NF'				,TamSx3('F1_SERIE')[1]			,Nil})
		aAdd(aColImpr,{'dEmisNF'	,'Emiss�o da NF'			,TamSx3('F1_EMISSAO')[1]		,{||(StoD(dEmisNF))}})
		aAdd(aColImpr,{'DtEntreg'	,'Previs�o Entrega'			,TamSx3('C7_DATPRF')[1]			,{||(StoD(DtEntreg))}})
		aAdd(aColImpr,{'DtEntreg'	,'Data de Entrega'			,TamSx3('C7_DATPRF')[1]			,{||(StoD(DtEntreg))}})
		aAdd(aColImpr,{'nQtdPrev'	,'Quantidade Prevista'		,TamSx3('C7_QUANT')[1]			,Nil})
		aAdd(aColImpr,{'nQtdEntre'	,'Quantidade Entregue'		,TamSx3('C7_QUJE')[1]			,Nil})
		aAdd(aColImpr,{'nQtdPEnd'	,'Quantidade Pendente'		,TamSx3('C7_QUANT')[1]			,Nil})
		aAdd(aColImpr,{'cCGC'		,'CPF/CNPJ'					,TamSx3('A2_CGC')[1]			,Nil})
		aAdd(aColImpr,{'cNomFor'	,'Nome Fornecedor'			,TamSx3('A2_NOME')[1]			,Nil})
		
	RestArea(aAreaSX3)

Return aColImpr
//-----------------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint
Respons�vel pela sele�a� e impress�o dos registros.
@type Static function
@author Cristiane Thomaz Polli
@since 05/07/2016
@version 12.1.7
@param oReport, objeto, (Descri��o do par�metro)
@param cPergAtu, caracter, (Nome do Parametro de perguntas SX1)
@Project MAN00000463301_EF_004
@return ${return}, ${n�o existe}
/*/
//-----------------------------------------------------------------------------
Static Function ReportPrint(oReport,cPergAtu)
	
	Local cWheryC1	:= ''
	
	// Transforma parametros Range em expressao SQL
	MakeSqlExpr(cPergAtu)
		
	cWheryC1	:= MV_PAR01
	
	if !Empty(MV_PAR02)
	
			cWheryC1	+= 'AND' + MV_PAR02
	
	EndIf
	
	cWheryC1 := "%" + cWheryC1 + "%"
	
	BeginSql Alias "QRY"
		   				
		SELECT C1_FILIAL cFilC1, C1_LOCAL cSetor, C1_PRODUTO cCodProd, C1_DESCRI cDesProd, C1_SOLICIT cSolic, C1_NUM cNumSolic, C1_TPSC cTpSolic,
		C1_XMOTIVO cMotSolic, C1_EMISSAO dEmisSoli,
		C7_USER cComprador, C7_NUM cNumPC, C7_EMISSAO dEmisPC, C7_DATPRF DtEntreg, C7_QUANT nQtdPrev, C7_QUJE nQtdEntre, C7_QUANT - C7_QUJE nQtdPEnd,
		CND_NUMMED cNumMed, CND_DTINIC dDtMed, 
		CR_DATALIB DtLib,
		D1_DOC cNotaFis, D1_SERIE cSerNF, D1_EMISSAO dEmisNF,
		A2_NOME cNomFor, A2_CGC  cCGC
		FROM %table:SC1% SC1 (NOLOCK)
		LEFT JOIN %table:SC7% SC7 (NOLOCK)
		        ON  SC7.%notDel% 		   
			   AND C7_FILIAL  = C1_FILIAL
			   AND C7_NUMSC = C1_NUM
			   AND C7_ITEMSC = C1_ITEM
		LEFT JOIN %table:CND% CND  (NOLOCK)
		       ON CND_FILIAL = C7_FILIAL
		      AND CND_PEDIDO = C7_NUM
		      AND  CND.%notDel%   
		LEFT JOIN  %table:SD1% SD1 (NOLOCK)
		       ON D1_FILIAL = C1_FILIAL
		       AND SD1.%notDel%  
		       AND D1_PEDIDO = C7_NUM
		       AND D1_ITEM = C7_ITEM
		LEFT  JOIN %table:SA2% SA2 (NOLOCK)
		        ON A2_FILIAL = %exp:xFilial("SA2")%
		        AND A2_COD = C7_FORNECE
		        AND A2_LOJA = C7_LOJA
		        AND SA2.%notDel% 
		LEFT JOIN %table:SCR%  SCR
     		   ON CR_FILIAL = C1_FILIAL
			  AND CR_NUM = C1_NUM
     		  AND CR_TIPO = 'SC'
     		  AND SCR.%notDel% 
		WHERE 	%Exp:cWheryC1% 
		  AND SC1.%notDel% 
	      AND C1_EMISSAO BETWEEN %exp:Dtos(MV_PAR03)%  AND %exp:Dtos(MV_PAR04)% 
		ORDER BY C1_FILIAL, C1_EMISSAO
	   
   EndSql
   
   
   oReport:SetMeter(0)
    
    // Impress�o do cabecalho da sessao
	oReport:Section(1):Init()
	
	While  !QRY->(Eof())

		oReport:Section(1):PrintLine()//imprime a linha
		QRY->(dbSkip())

	EndDo
	
	QRY->(DbCloseArea())
		   
	oReport:Section(1):Finish()
	oReport:Section(1):SetPageBreak()
Return