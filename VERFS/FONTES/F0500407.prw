#Include 'Protheus.ch'
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} F0500407
Atualiza o campo que sinaliza se o funcion�rio possui solicita��o em aberto e envia e-mail notififa
@type User function
@author Cris
@since 22/11/2016
@version 1.0
@param cFilFun, character, (Filial do Funcion�rio)
@param cMatFun, character, (Matricula do Funcion�rio)
@param cCodSol, character, (Codigo da Solicita�ao)
@param cVisaoAt, character, (Codigo da Visao)
@param cStAtu, character, (recebe o valor do status a ser gravado)
@return ${L�gico}, ${.T. foi atualizado com sucesso .F.  n�o foi atualizado}
@Project MAN0000007423039_EF_004
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500407(cFilFun,cMatFun,cCodSol,cVisaoAt)
	
	Local aAreaAtu	:= SRA->(GetArea())
	Local lAtuSRA	:= .F.
	Local lFuncES	:= iif(IsInCallStack("U_F0500402"),.T.,.F.)
	Local lFuncCS	:= iif(IsInCallStack("PrepNot"),.T.,.F.)
	
	dbSelectArea("SRA")
	SRA->(dbSetOrder(1))
	if SRA->(dbSeek(cFilFun+cMatFun))
		
		SRA->(Reclock('SRA',.F.))
		SRA->RA_XSTMVTO := " "
		SRA->(MSunlock())
		
		lAtuSRA	:= .T.
	Else
		
		if  lFuncES .OR. lFuncCS
			
			Help("",1, "Help", "Atualiza��o de Funcion�rio(F0500407_01)", "O c�digo de funcion�rio ("+cMatFun+") n�o foi localizado para esta filial ("+cFilSol+"), portanto n�o ser� atualizado!" , 3, 0)
		EndIf
		
		lAtuSRA	:= .F.
	EndIf
	
	RestArea(aAreaAtu)
	
	if lAtuSRA .AND. IsInCallStack('U_F0100323')
		
		//Monta email notificador para enviar
		U_F0500409(cFilFun,cMatFun,cCodSol,cVisaoAt,iif(lFuncES,'1','2'))
	EndIf
	
	if lAtuSRA
		
		//Garanto que continue posicionada
		dbSelectArea("RH3")
		RH3->(dbSetOrder(1))
		if RH3->(dbSeek(cFilFun+cCodSol))
			
			//			if  !isIncallstack("FsAprovSol") .AND. FindFunction("U_F0500201")
			
			//Requisito N005 -  Indicadores: -068-Reprovada pelo CSC
			//			U_F0500201(cFilFun,cCodSol,'068')
			
			//			EndIf
			
		EndIf
	EndIf
	
Return lAtuSRA
