#include 'protheus.ch'
#include 'apwebsrv.ch'

/*/{Protheus.doc} W0700601
 WebService Server
@type function
@author queizy.nascimento
@since 26/01/2017
@version 1.0
@Project MAN0000007423041_EF_006
/*/
User Function W0700601()
Return

WSSTRUCT LocalEstoque

	WSDATA cFil 		As String
	WSDATA cCodigo		As String
	WSDATA cDescri		As String
	WSDATA cTipo  	   	As String
	WSDATA cMsblql		As String
	WSDATA cCCusto		As String 	OPTIONAL

ENDWSSTRUCT

WSSTRUCT LocalEstoqueID
	WSDATA cFil    	As String
	WSDATA cCodigo  As String
ENDWSSTRUCT
   
WSSERVICE W0700601 DESCRIPTION "Manuten��o de Locais de Estoque"
    
	WSDATA oLocalEstoque   AS LocalEstoque
	WSDATA oLocalEstoqueID AS LocalEstoqueID
	WSDATA cRetorno        AS STRING

	WSMETHOD UpsertLocalEstoque   DESCRIPTION "Realiza inclus�o/altera��o de Locais de Estoque"
	WSMETHOD DeleteLocalEstoque   DESCRIPTION "Exclus�o de Locais de Estoque"


ENDWSSERVICE

WSMETHOD UpsertLocalEstoque WSRECEIVE oLocalEstoque WSSEND cRetorno WSSERVICE W0700601
	BEGIN WSMETHOD
		::cRetorno := U_F0700602(oLocalEstoque)
	END WSMETHOD
	
Return .T.

WSMETHOD DeleteLocalEstoque WSRECEIVE oLocalEstoqueID WSSEND cRetorno WSSERVICE W0700601
	BEGIN WSMETHOD
		::cRetorno := U_F0700601(oLocalEstoqueID)
	END WSMETHOD
Return .T.