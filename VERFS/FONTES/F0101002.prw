#Include 'Protheus.ch'

/*
{Protheus.doc} F0101002()
PE F050ROT para adicionar bot�es de recusa na rotina FINA050.
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Return		aRet, Vetor aRotina
*/
User Function F0101002(aRotina)

aAdd(aRotina, {'Recusar'     ,"U_F010101A",0,3})
aAdd(aRotina, {'Hist. Recusa','U_F010101G',0,3})

Return aRotina