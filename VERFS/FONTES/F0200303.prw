#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'

/*
{Protheus.doc} F0200303()
Solicita��o de Incentivo Acad�mico
@Author     Henrique Madureira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Menu		SIGATRM - Treinamento
@Param
@Return
*/
User Function F0200303()
	
	Local oBrowse
	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('PA3')
	oBrowse:SetDescription('Solicita��o de Incentivo Academico')
	oBrowse:AddLegend( "PA3_XSTATU == '1'", "GREEN", "Aprovado"  )
	oBrowse:AddLegend( "PA3_XSTATU == '2'", "RED", "Reprovado"  )
	oBrowse:AddLegend( "PA3_XSTATU == '3'", "YELLOW", "Aguardando Aprova��o"  )
	oBrowse:Activate()
	
Return

/*
{Protheus.doc} MenuDef()
Cria��o do Menu de Incentivo Acad�mico
@Author     Henrique Madureira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param
@Return		aRotina, Op��o de Menu
*/
Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina TITLE '&Incluir'    ACTION 'VIEWDEF.F0200303' OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE '&Visualizar' ACTION 'VIEWDEF.F0200303' OPERATION 2 ACCESS 0
	
Return aRotina

/*
{Protheus.doc} ModelDef()
Modelo de Dados do Incentivo Acad�mico
@Author     Henrique Madureira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param
@Return		oModel, Modelo de Dados
*/
Static Function ModelDef()
	
	Local oStruPA3 := FWFormStruct( 1, 'PA3', /*bAvalCampo*/,.F. )
	Local oModel
	
	oStruPA3:SetProperty("PA3_XTERMI",MODEL_FIELD_VALID,{|oModel| FsVldDtFim(oModel)})
	oStruPA3:SetProperty("PA3_XSOLIC",MODEL_FIELD_INIT,{|oModel| FsBscSolic(oModel,"PA3_XSOLIC")})
	oStruPA3:SetProperty("PA3_RSPAPR",MODEL_FIELD_INIT,{|oModel| FsBscSolic(oModel,"PA3_RSPAPR")})
	oStruPA3:SetProperty("PA3_XNMSOL",MODEL_FIELD_INIT,{|oModel| FsBscSolic(oModel,"PA3_XNMSOL")})
	
	oModel := MPFormModel():New('F02003', , {|oModel|GrvSoliIn(oModel)}, /*bCommit*/, /*bCancel*/ )
	oModel:AddFields( 'PA3MASTER', /*cOwner*/, oStruPA3, , /*bPosValidacao*/, /*bCarga*/ )
	oModel:SetDescription( 'Solicita��o de Incentivo Academico' )
	oModel:SetPrimaryKey({""})
	oModel:GetModel( 'PA3MASTER' ):SetDescription( 'Incentivo Academico' )
	
	oModel:SetVldActivate( { |oModel| FsVldAct( oModel ) } )
	
Return oModel

/*
{Protheus.doc} ViewDef()
Interface do Incentivo Acad�mico
@Author     Henrique Madureira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param
@Return		oView, interface
*/
Static Function ViewDef()
	
	Local oModel   := FWLoadModel( 'F0200303' )
	Local oStruPA3 := FWFormStruct( 2, 'PA3' )
	Local oView
	
	oStruPA3:RemoveField("PA3_RSPAPR")
	
	oView := FWFormView():New()
	oView:SetModel( oModel )
	oView:AddField( 'VIEW_PA3', oStruPA3, 'PA3MASTER' )
	oView:CreateHorizontalBox( 'TELA' , 100 )
	oView:SetOwnerView( 'VIEW_PA3', 'TELA' )
	
Return oView

/*
{Protheus.doc} ValStatu()
Valida��o do Status do Incentivo Acad�mico
@Author     Henrique Madureira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param		oModel, objeto, modelo de dados
@Return		lRet, permite altera��o do campo ou n�o
*/
Static Function ValStatu(oModel)
	
	Local oModel1     := oModel:GetModel()
	Local nOperation  := oModel:GetOperation()
	Local cStatus     := oModel1:GetValue( 'PA3MASTER', "PA3_XSTATU" )
	Local oStruPA3    := FWFormStruct( 1, 'PA3', /*bAvalCampo*/,.F. )
	Local lRet        := .T.
	
	If nOperation == MODEL_OPERATION_UPDATE .AND. cStatus == "1"
		oStruPA3:SetProperty( '*' , MODEL_FIELD_WHEN,'INCLUI')
		Help( ,, 'HELP',, "N�o � poss�vel altera��o, pois a solicita��o j� foi aprovada!", 1, 0)
		lRet := .F.
	EndIf
	
	If nOperation == MODEL_OPERATION_UPDATE .AND. cStatus == "2"
		oStruPA3:SetProperty( '*' , MODEL_FIELD_WHEN,'INCLUI')
		Help( ,, 'HELP',, "N�o � poss�vel altera��o, pois a solicita��o j� foi reprovada!", 1, 0)
		lRet := .F.
	EndIf
	
Return lRet

/*
{Protheus.doc} FsVldDtFim()
Valida��o da data final do curso
@Author     Bruno de Oliveira
@Since      11/08/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param		oModel, objeto, modelo de dados
@Return		lRet, permite informar data v�lida ou n�o
*/
Static Function FsVldDtFim(oModel)
	
	Local aArea  := GetArea()
	Local dDtIni := oModel:GetValue("PA3_XINICI")
	Local dDtFim := oModel:GetValue("PA3_XTERMI")
	Local lRet   := .F.

	If !Empty(dDtIni)
		If DTOS(dDtFim) >= DTOS(dDtIni) .And. DTOS(dDtFim) >= DTOS(dDatabase)
			lRet := .T.
		EndIf
	Else
		Help(,,'FsVldDtFim',,"Necess�rio informar a data de inicio do curso primeiro",1,0)
		lRet := .F.
	EndIf

	RestArea(aArea)
	
Return lRet

/*
{Protheus.doc} FsBscSolic()
Busca solicitantes e responsaveis pela aprova��o
@Author     Bruno de Oliveira
@Since      11/08/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param		oModel, objeto, modelo de dados
@Return		cRet, retorna o conteudo do campo
*/
Static Function FsBscSolic(oModel,cCampo)
	
	Local nOper  := oModel:GetOperation()
	Local cSolic := ""
	Local cRet   := ""
	
	If nOper == MODEL_OPERATION_INSERT
		
		PswOrder(2)
		If PswSeek(CUSERNAME)
			If cCampo == "PA3_XSOLIC"
				cRet := SubStr(PswRet()[1][22],11)
			ElseIf cCampo == "PA3_RSPAPR"
				cRet := SubStr(PswRet()[1][22],11)
			EndIf
		EndIf
		
	EndIf
	
	If cCampo == "PA3_XNMSOL"
		cSolic := oModel:GetValue("PA3_XSOLIC")
		cRet   := Posicione("SRA",1,xFilial("SRA")+cSolic,"RA_NOME")
	EndIf
	
Return cRet

/*
{Protheus.doc} FsVldAct()
Valida��o antes de abrir a tela de incetivo acad�mico
@Author     Bruno de Oliveira
@Since      12/08/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_003
@Param		oModel, objeto, modelo de dados
@Return		lRet, permite ativa��o da tela ou n�o.
*/
Static Function FsVldAct(oModel)
	
	Local aArea    := GetArea()
	Local cOper    := oModel:GetOperation()
	Local cSolic   := PA3->PA3_XSOLIC
	Local cResp    := PA3->PA3_RSPAPR
	Local cStatus  := PA3->PA3_XSTATU
	Local lRet     := .T.
	
	If cOper == MODEL_OPERATION_UPDATE
		If cSolic <> cResp
			Help(,,'FsVldAct',,"Altera��o n�o permitida! Processo de solicita��o est� em andamento.",1,0)
			lRet := .F.
		EndIf
	EndIf
	
	If cOper == MODEL_OPERATION_UPDATE .AND. cStatus == "1"
		Help( ,, 'HELP',, "N�o � poss�vel altera��o, pois a solicita��o j� foi aprovada!", 1, 0)
		lRet := .F.
	EndIf
	
	If cOper == MODEL_OPERATION_UPDATE .AND. cStatus == "2"
		Help( ,, 'HELP',, "N�o � poss�vel altera��o, pois a solicita��o j� foi reprovada!", 1, 0)
		lRet := .F.
	EndIf
	
	RestArea(aArea)
	
Return lRet

Static Function GrvSoliIn(oModel)
	
	Local nReturnCode := 0
	Local nCount      := 0
	Local nItem       := 0
	Local lRet        := .F.
	Local cQuery      := ""
	Local cAliasAi8   := 'RETAI8'
	Local aAreas      := {RH4->(GetArea()),RH3->(GetArea()),GetArea()}
	Local oModel      := oModel:GetModel()
	
	cQuery := "SELECT	AI8.AI8_VISAPV "
	cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
	cQuery += " WHERE  AI8.AI8_FILIAL = '"+FwxFilial("AI8")+"' "
	cQuery += " AND	AI8.AI8_WEBSRV = 'W0200301' AND UPPER(AI8_ROTINA) = UPPER('U_F0200301.apw') "
	cQuery += " AND AI8.D_E_L_E_T_ = ' '"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasAi8)
	
	DbSelectArea(cAliasAi8)
	//Pega a vis�o cadastrada
	cVisao := (cAliasAi8)->AI8_VISAPV
	
	(cAliasAi8)->(DbCloseArea())
	
	PswOrder(1)
	If (PswSeek(__cUserId,.T.))
		aDdSolic := PswRet()
		
		cMatSoli  := Substring(aDdSolic[1][22],len(cEmpAnt)+len(cFilAnt)+1,12)
		cFilSoli  := Substring(aDdSolic[1][22],len(cEmpAnt)+1,len(cFilAnt))
		aRetSup	  := U_F0100327(cFilSoli, cMatSoli, SRA->RA_DEPTO, cVisao, cEmpAnt)
		
		If !Empty(aRetSup)
			If !Empty(cMatSoli)
				lRet := .T.
			Else
				//Aviso("Aten��o","N�o o V�nculo entre o Solicitante e o cadastro de Funcion�rios.",{"Ok"})
				Help( ,, 'HELP',, 'Erro no V�nculo entre o Solicitante e o cadastro de Funcion�rios.', 1, 0)
				lRet := .F.
				
			EndIf
		Else
			//Aviso( "Superior", "Superior para Aprova��o n�o encontrado", {"Ok"} )
			Help( ,, 'HELP',, 'Superior para Aprova��o n�o encontrado', 1, 0)
			lRet := .F.
			
		EndIf
	EndIf
	If lRet
		aRegs := {;
			{"PA3_FILIAL",M->PA3_FILIAL},;
			{"PA3_XCOD  ",M->PA3_XCOD  },;
			{"PA3_XMATRI",M->PA3_XMATRI},;
			{"PA3_XSETOR",M->PA3_XSETOR},;
			{"PA3_XCENTR",M->PA3_XCENTR},;
			{"PA3_XUNIDA",M->PA3_XUNIDA},;
			{"PA3_XCARGO",M->PA3_XCARGO},;
			{"PA3_XEMAIL",M->PA3_XEMAIL},;
			{"PA3_XTELEF",M->PA3_XTELEF},;
			{"PA3_XRAMAL",M->PA3_XRAMAL},;
			{"PA3_XTIPO ",M->PA3_XTIPO },;
			{"PA3_XINSTI",M->PA3_XINSTI},;
			{"PA3_XCURS ",M->PA3_XCURS },;
			{"PA3_XINICI",DTOS(M->PA3_XINICI)},;
			{"PA3_XTERMI",DTOS(M->PA3_XTERMI)},;
			{"PA3_XDURAC",CVALTOCHAR(M->PA3_XDURAC)},;
			{"PA3_XMENSL",CVALTOCHAR(M->PA3_XMENSL)},;
			{"PA3_XVLTAL",CVALTOCHAR(M->PA3_XVLTAL)},;
			{"PA3_XVLTCU",CVALTOCHAR(M->PA3_XVLTCU)},;
			{"PA3_XPCBEN",CVALTOCHAR(M->PA3_XPCBEN)},;
			{"PA3_XSOLIC",M->PA3_XSOLIC},;
			{"PA3_RSPAPR",aRetSup[1][2]},;
			{"PA3_XOBSER",M->PA3_XOBSER},;
			{"PA3_XSTATU",M->PA3_XSTATU};
			}
		cRH4Fil := GetSx8Num("RH3","RH3_CODIGO")
			//cRH4Fil := GetSxeNum("RH3","RH3_CODIGO","RH3_CODIGO" + xFilial("RH3"))
		RH3->(DbSetOrder(1))
		If ! RH3->(DbSeek(xFilial("RH3") + cRH4Fil))
			Reclock("RH3", .T.)
			RH3->RH3_FILIAL  := FWxFilial("RH3")
			RH3->RH3_CODIGO  := cRH4Fil
			RH3->RH3_MAT     := M->PA3_XMATRI
			RH3->RH3_TIPO    := " "
			RH3->RH3_ORIGEM  := "PORTAL"
			RH3->RH3_STATUS  := IIF(aRetSup[1][2] = cMatSoli,"4","1")
			RH3->RH3_DTSOLI  := DATE()
			RH3->RH3_NVLINI  := 0
			RH3->RH3_FILINI  := cFilSoli
			RH3->RH3_MATINI  := cMatSoli
			RH3->RH3_FILAPR  := aRetSup[1][1]
			RH3->RH3_MATAPR  := aRetSup[1][2]
			RH3->RH3_NVLAPR  := 99
			RH3->RH3_KEYINI  := "002"
			RH3->RH3_FLUIG   := 0
			RH3->RH3_EMP     := "01"
			RH3->RH3_EMPINI  := "01"
			RH3->RH3_EMPAPR  := "01"
			RH3->RH3_XTPCTM  := "007"
			RH3->(MsUnlock())
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(cFilSoli,cRH4Fil,"077")
				U_F0500201(cFilSoli,cRH4Fil,"078")
			EndIf
		EndIf
			
		RH4->(DbSetOrder(1))
		If ! RH4->(DbSeek(xFilial("RH4") + cRH4Fil))
			For nCount:= 1 To Len(aRegs)
				Reclock("RH4", .T.)
				RH4->RH4_FILIAL	:= FWxFilial("RH4")
				RH4->RH4_CODIGO	:= cRH4Fil
				RH4->RH4_ITEM		:= ++nItem
				RH4->RH4_CAMPO	:= aRegs[nCount, 1]
				RH4->RH4_VALNOV	:= IIF(EMPTY(aRegs[nCount, 2]),'',aRegs[nCount, 2])
				RH4->RH4_XOBS  	:= ""
				RH4->(MsUnlock())
			Next
		EndIf
		If (__lSX8)
			ConfirmSx8()
			lRet := .T.
		EndIf
		cNome := POSICIONE("SRA",1,cFilSoli+cMatSoli,"RA_NOME")
		U_F0200302(4, aRetSup[1][2],cNome,'','','',aRetSup[1][1])
			
	EndIf
	AEval(aAreas, {|x| RestArea(x)} )
	
Return lRet
