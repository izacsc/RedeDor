#include 'protheus.ch'
#include 'fwmvcdef.ch'

/*/{Protheus.doc} UpsMVCRg
Realiza Upsert em um Registro com modelo MVC.
@author izac.ciszevski
@since 19/01/2017
@param cTabela, character, Nome da Tabela para Busca
@param cChave, character, Chave de busca para o Registro
@param cFonte, character, Nome do fonte MVC
@param cModelo, character, Nome do Modelo a ser preenchido
@param aCampos, array, campos a serem atualizados no formato {{"CAMPO", xConteudo}}
@param nInd, numeric, Opcional | Indice de busca
@Project MAN0000007423041_EF_000 | Fun��es de Suporte
/*/
User Function UpsMVCRg(cTabela, cChave, cFonte, cModelo, aCampos, nInd)
    Local cFilReg    := Left(cChave, Len(XFilial(cTabela)))
    Local cRetorno   := ""
    Local nOperation := 0
    Local lOK        := .T.
    Local aInfo      := {}
    Local nRecLog    := 0
    Local aAreas     := {(cTabela)->(GetArea()), GetArea()}

    Default nInd := 1

    aInfo := { cTabela, cChave, cFonte, cModelo, aCampos, nInd}

    nRecLog := u_F07Log01(aCampos[len(aCampos)][2], aInfo, ProcName(1))

    If !u_F07ChkFil(cFilReg)
        Return "Erro | Filial Inv�lida"
    EndIf

    aCampos[1][2] := XFilial(cTabela) //-- ajusta a filial

    (cTabela)->(DbSetOrder(nInd))
    If (cTabela)->(DbSeek(cChave))
	    nOperation := MODEL_OPERATION_UPDATE
    Else
        nOperation := MODEL_OPERATION_INSERT
    EndIf
	
    If ( lOK := ManutReg(cFonte, cModelo, aCampos, nOperation, @cRetorno ))
        cRetorno := "Registro Inclu�do/Alterado com sucesso"
    EndIf

    u_F07Log02(nRecLog, cRetorno, lOK)

    aInfo := ASize(aInfo, 0)
    aInfo := Nil

    AEval( aAreas, {|aArea| RestArea(aArea)}) 

Return cRetorno

/*/{Protheus.doc} DelMVCRg
Realiza Delete em um Registro com modelo MVC.
@author izac.ciszevski
@since 19/01/2017
@param cTabela, character, Nome da Tabela para Busca
@param cChave, character, Chave de busca para o Registro
@param cFonte, character, Nome do fonte MVC
@param [nInd], numeric, Opcional | Indice de busca
@Project MAN0000007423041_EF_000 | Fun��es de Suporte
/*/
User Function DelMVCRg(cTabela, cChave, cFonte, nInd)
    Local cFilReg    := Left(cChave, Len(XFilial(cTabela)))
    Local cRetorno   := ""
    Local nOperation := MODEL_OPERATION_DELETE
    Local lOK        := .F.
    Local aInfo      := {}
    Local nRecLog    := 0
    Local aAreas     := {(cTabela)->(GetArea()), GetArea()}
	Local cId		 := FwUUIDv4()
    Default nInd := 1

    DbSelectArea(cTabela)
    aInfo := { cTabela, cChave, cFonte, cId, nInd }
    nRecLog := u_F07Log01(cID, aInfo, ProcName(1))

    If !u_F07ChkFil(cFilReg)
        Return "Erro | Filial Inv�lida"
    EndIf
        
    DbSetOrder(nInd)
    If !DbSeek(cChave)
        Return "Erro | Registro N�o Localizado"
    EndIf


    If (lOK := ManutReg(cFonte, , , nOperation, @cRetorno ))
        cRetorno := "Registro exclu�do com sucesso"
    EndIf
    
    u_F07Log02(nRecLog, cRetorno, lOK)
    
    aInfo := ASize(aInfo, 0)
    aInfo := Nil
    
    AEval( aAreas, {|aArea| RestArea(aArea)}) 

Return cRetorno

Static Function ManutReg(cFonte, cModelo, aCampos, nOperation, cRetorno )
    Local lOK    := .T. 
    Local nCampo := 1
    Local oModel, oModelFields
    Local aError := {}

    Default cRetorno := ""

    BEGIN SEQUENCE 

        oModel := FWLoadModel(cFonte)
        oModel:SetOperation(nOperation)
        oModel:Activate()

        If nOperation != MODEL_OPERATION_DELETE
            oModelFields := oModel:GetModel(cModelo)
            For nCampo := 1 to Len(aCampos)
                If oModelFields:CanSetValue(aCampos[nCampo][1])
                    If ! (lOK := oModelFields:SetValue(aCampos[nCampo][1], aCampos[nCampo][2]))
                        exit
                    EndIf
                EndIf
            Next
        EndIf

        If !( lOK .and. oModel:VldData() .and. oModel:CommitData())
            aError := oModel:GetErrorMessage()
            cRetorno := " Erro | " + aError[5] + " | " + aError[6] + " | " + aError[7] 
            lOK := .F.
        EndIf
        
        oModel:Destroy()

    END SEQUENCE
     
Return lOK
/*/{Protheus.doc} GetIntegID
Retorna um ID �nico para a integra��o
@author izac.ciszevski
@since 19/01/2017
@Project MAN0000007423041_EF_000 | Fun��es de Suporte
/*/
User Function GetIntegID()

Return FwUUIDv4()

/*/{Protheus.doc} RetChave
Retorna uma chave de pesquisa a partir de um array de campos,
ajustando o conte�do conforme o dicion�rio de dados.
@author izac.ciszevski
@since 19/01/2017
@version undefined
@param cTabela, character, Nome da Tabela para Busca
@param aCampos, array, campos a serem atualizados no formato {{"CAMPO", xConteudo}}
@param aPos, array, posi��o n�merica dos campos chave no array aCampos
/*/
User Function RetChave(cTabela, aCampos, aPos)
    Local cChave := ""
    Local nX     := 0
    Local nPos   := 0
   
    For nX := 1 to Len(aPos)
        nPos   := aPos[nX]
        cChave += PadR(aCampos[nPos][2], TamSX3(aCampos[nPos][1])[1])
    Next

Return cChave

User Function F07ChkFil(cFilReg)
    If !Empty(cFilReg)
        SM0->(DbSetOrder(1))
        If !SM0->(MsSeek(cEmpAnt + cFilReg))
            SM0->(MsSeek(cEmpAnt + cFilAnt))
            Return .F.
        Else
            cFilAnt := cFilReg
        EndIf
    EndIf
Return .T.

User Function F07Log01(cID, xInfo, cRotina)
    Local aCampos := {}
    Local cTrace  := GetMV("FS_WSTRACE",,"2")  //1 | Habilitado; 2 | Desabilitado
    Local nRecLog := 0
    
    Default cRotina := ProcName(1)
    Default xInfo   := ""

    If cTrace == "1"
        aCampos :=  {;
                        {"P19_FILIAL", XFilial('P19')         },;
                        {"P19_ID"    , cID                    },;
                        {"P19_DTHRI" , FwTimeStamp()          },;
                        {"P19_ROTINA", cRotina                },;
                        {"P19_STATUS", "1"                    },;
                        {"P19_INPUT" , FwJsonSerialize(xInfo) };
                    }

        ManutReg("F0700002", "MASTER", aCampos, 3)
        nRecLog := P19->(Recno())
    EndIf

    aCampos := ASize(aCampos, 0)
    aCampos := Nil

Return nRecLog

User Function F07Log02(nRecLog, cRetorno, lSucesso)
    Local aCampos := {}
    Local cIndKey := ""

    Static cTrace  := GetMV("FS_WSTRACE",,"2") //1 | Habilitado; 2 | Desabilitado
    
    If cTrace == "1"
        P19->(DbGoTo(nRecLog))
        
        AEval(StrTokArr(IndexKey(),"+"), {|cField| cIndKey += FieldGet(FieldPos(cField) + "|")})

        aCampos :=  {;
                        {"P19_INDKEY" , cIndKey              },;
                        {"P19_STATUS" , If(lSucesso,"2","3") },;
                        {"P19_DTHRF"  , FwTimeStamp()        },;
                        {"P19_OUTPUT" , cRetorno             };
                    }

        ManutReg("F0700002", "MASTER", aCampos, 4)
    EndIf

    aCampos := ASize(aCampos, 0)
    aCampos := Nil

Return