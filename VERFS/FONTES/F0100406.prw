#Include 'Protheus.ch'

/*/{Protheus.Doc} F0100406    
Altera campo customizado de status do PC quando bloqueado pelo m�dulo PCO.
Executada no PE MT120PCOK. 
@project	MAN00000462901_EF_004     
@author		Paulo Kr�ger
@version	P12
@since		15/06/2016  
/*/
User Function F0100406()

	Local aGetArea  := GetArea()
	Local nPosXStat := 0
	Local nX        := 0
	Local lAtualiz  := .T.
	
	nPosXStat := AScan(aHeader, { |x| Alltrim(Upper( x[2] )) == Alltrim(Upper('C7_XSTATUS'))})
	
	If GetMV('MV_PCOINTE') == '1'
		If nTipoPed == 1
			lAtualiz := PcoVldLan('000052','01','MATA121',/*lUsaLote*/,/*lDeleta*/, .F./*lVldLinGrade*/)
		ElseIf nTipoPed == 2
			lAtualiz := PcoVldLan('000053','01','MATA122',/*lUsaLote*/,/*lDeleta*/, .F./*lVldLinGrade*/)
		EndIf
	EndIf
	
	If !lAtualiz
	//	aCols[n][nPosXStat] := '4' //Bloqueado no Or�amento - Nairan: Alterado por solicita��o da Gisele no dia 06/01/17 �s 16:18.
	EndIf 
	
	RestArea(aGetArea)

Return