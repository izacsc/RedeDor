#include 'protheus.ch'
#include 'apwebsrv.ch'
#INCLUDE 'FWMVCDEF.CH'

WSSTRUCT Fabricante
    WSDATA cFILFab 	AS STRING
    WSDATA cCOD   	AS STRING
    WSDATA cDESCR 	AS STRING
    WSDATA cCGC 	AS STRING
    WSDATA cMSBLQL 	AS STRING
    WSDATA cTIPO 	AS STRING
    
ENDWSSTRUCT

WSSTRUCT FabricanteID
    WSDATA cFILFab AS STRING
    WSDATA cCOD    AS STRING
ENDWSSTRUCT

WSSERVICE W0700801 DESCRIPTION "Responsavel pela inclus�o/altera��o/exclus�o de registros Fabricante"
    
    WSDATA RegistroFabricante   AS Fabricante
    WSDATA RegistroFabricanteID AS FabricanteID

    WSDATA cRetorno AS STRING

    WSMETHOD UpsertFabricante   DESCRIPTION "Realiza inclus�o/altera��o de registros Fabricante"

    WSMETHOD DeleteFabricante   DESCRIPTION "Realiza exclus�o de registros Fabricante"

ENDWSSERVICE 

WSMETHOD UpsertFabricante WSRECEIVE RegistroFabricante WSSEND cRetorno WSSERVICE W0700801
    BEGIN WSMETHOD    
        ::cRetorno := U_F0700801(RegistroFabricante)
    END WSMETHOD
Return .T.  

WSMETHOD DeleteFabricante WSRECEIVE RegistroFabricanteID WSSEND cRetorno WSSERVICE W0700801
    BEGIN WSMETHOD
        ::cRetorno := U_F0700802(RegistroFabricanteID)
    END WSMETHOD
Return .T.