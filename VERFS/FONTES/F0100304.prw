#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100304()
Validação da disponibilidade de postos e orçamento.
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, página html
*/
User Function F0100304()

	Local nIndiceDepto := Val(HttpGet->nIndiceDepto)
	Local lFsUsaPco    := .F.
	Local cHtml        := ""
	Local oOrg
	
	Private cMsg       := ""
	
	HttpCTType("text/html; charset=ISO-8859-1")
	WEB EXTENDED INIT cHtml START "InSite"                     
	
	 	Default HttpSession->Postos   := {}
		Default HttpGet->FilterField  := ""
		Default HttpGet->FilterValue  := ""
	 	Default HttpGet->Page         := "1"
	 	Default HttpGet->nIndiceDepto := '1'
	 	Default HttpGet->nCurrentPage := 0
	 	Default HttpGet->nTotalPage   := '1'
	 	
	 	nCPage:= Val(HttpGet->Page)
	 		 	
		oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg, "ORGSTRUCTURE.APW",,,HttpSession->Department[nIndiceDepto]:cDepartmentEmp)
		
		nCurrentPage        := IIF(!(EMPTY(HttpGet->nCurrentPage)),VAL(HttpGet->nCurrentPage),1)
		
		                     
		//LISTA DE POSTOS	                
		oOrg:cCompanyID        := HttpSession->Department[nIndiceDepto]:cDepartmentEmp
		oOrg:cDepartmentID     := HttpSession->Department[nIndiceDepto]:cDepartment
		oOrg:cFilterField      := HttpGet->FilterField
		oOrg:cFilterValue      := HttpGet->FilterValue
		oOrg:nPage             := nCurrentPage
		oOrg:cRequestType      := HttpSession->cTypeRequest
		HttpSession->DadosFunc := WsClassNew('ORGSTRUCTURE_DATAEMPLOYEE')
		HttpSession->DadosFunc:CEMPLOYEEEMP := HttpSession->Department[nIndiceDepto]:cDepartmentEmp
		
		If oOrg:GetPostos()
			HttpSession->Postos := oOrg:oWSGETPOSTOSRESULT:oWSLISTOFPOSTS:oWSDATAPOSTOS
			nPTotal             := oOrg:oWSGETPOSTOSRESULT:nPagesTotal
			nTotalPage          := val(HttpGet->nTotalPage)
			If Len(HttpSession->Postos) == 0
				lPostV := .F.
				If lFsUsaPco 
					cMsg := "Posto indisponível!"
					Return ExecInPage("F0100308")
				Else
					cMsg := "Posto e saldo indisponíveis!" 
					Return ExecInPage("F0100308")
				EndIf
			EndIf
		EndIf
			
		cHtml := ExecInPage( "F0100304" )
			
	WEB EXTENDED END
	
Return cHtml