#Include 'Protheus.ch'
#Include 'TopConn.ch'
#INCLUDE 'FWMVCDEF.CH'

/*
{Protheus.doc} F0100109()
Browse Manual de Medi��o Automatizada
@Author     Mick William da Silva
@Since      11/07/2016
@Version    P12.7
@Project    MAN00000462901_EF_001   
@Return     Nil
*/ 

User Function F0100109()

	Local nX
	Local aStru			:= SC1->(DBSTRUCT())	//Estrutura da Tabela SC1 - Ativos
	Local aColumns		:= {}
	Private cArqTrab	:= ""
	Private oMrkBrowse
	Private aRotina 	:= MenuDef()// Desta forma est� setando pra ele sempre pegar o aRotina do MVC Atual

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.
	
	cQuery := " SELECT R_E_C_N_O_ RECNO ,C1_FILIAL,C1_NUM,C1_ITEM,C1_PRODUTO,C1_DESCRI,C1_QUANT,C1_PRECO,C1_TOTAL,C1_LOCAL,C1_EMISSAO,C1_FORNECE,C1_LOJA, "
	cQuery += " C1_CC,C1_XTPSC,C1_FLAGGCT, C1_XCODSET "
	cQuery += " FROM "+ RETSQLNAME('SC1')+ " SC1 "
	cQuery += " WHERE C1_QUJE = 0 And C1_COTACAO = '"+Space(TAMSX3("C1_COTACAO")[1]) +"' And ( C1_APROV =' ' or  C1_APROV ='L' )  " // C1_COTACAO = '      '
	cQuery += " AND SC1.D_E_L_E_T_=' ' AND SC1.C1_FLAGGCT <>'1' "
	cQuery += " ORDER BY "  + SqlOrder(SC1->(IndexKey()))
	
	cChave		:= SC1->(IndexKey())
	Aadd(aStru, {"TMP_OK", 'C',1 ,0 })									//Cria��o Campo Tempor�rio(Virtual)
	Aadd(aStru, {"RECNO" , 'N',10,0 })
	cArqTrab 	:= CriaTrab(aStru,.T.) 									// Nome do arquivo temporario
	dbUseArea(.T.,__LocalDriver,cArqTrab,cArqTrab,.F.)
	Processa({||SqlToTrb(cQuery, aStru, cArqTrab)})						// Cria arquivo temporario
	IndRegua (cArqTrab,cArqTrab,cChave,,,"Selecionando Registros...")	// "Selecionando Registros..."
	(cArqTrab)->( DbSetOrder(0) )														// Fica na ordem da query

	For nX := 1 To Len(aStru)
		If	aStru[nX][1] $ "C1_NUM|C1_ITEM|C1_PRODUTO|C1_DESCRI|C1_QUANT|C1_PRECO|C1_TOTAL|C1_LOCAL|C1_EMISSAO|C1_FORNECE|C1_LOJA|C1_CC|C1_XTPSC|C1_FLAGGCT|R_E_C_N_O_|"
			AAdd(aColumns,FWBrwColumn():New())
			aColumns[Len(aColumns)]:SetData( &("{||"+aStru[nX][1]+"}") )
			aColumns[Len(aColumns)]:SetTitle(RetTitle(aStru[nX][1]))
			aColumns[Len(aColumns)]:SetSize(aStru[nX][3])
			aColumns[Len(aColumns)]:SetDecimal(aStru[nX][4])
			aColumns[Len(aColumns)]:SetPicture(PesqPict("SC1",aStru[nX][1]))
		EndIf
	Next nX

	
	If !(cArqTrab)->(Eof())
			//------------------------------------------
			//Cria��o da MarkBrowse
			//------------------------------------------
		oMrkBrowse:= FWMarkBrowse():New()
		oMrkBrowse:SetFieldMark("TMP_OK")
		oMrkBrowse:SetOwner(Nil)
		oMrkBrowse:SetAlias(cArqTrab)
		oMrkBrowse:SetMenuDef("F0100109")
		oMrkBrowse:SetFilterDefault( "Empty(C1_FLAGGCT)" )
			//oMrkBrowse:bMark    := {|| F019Mark(cArqTrab )}
			//oMrkBrowse:bAllMark := {|| AF012Inverte(cAliasMrk) }
		oMrkBrowse:SetDescription("Medi��o Automatizada")
		oMrkBrowse:SetColumns(aColumns)
		oMrkBrowse:Activate()
	
	EndIf
	
	If !Empty (cArqTrab)
		dbSelectArea(cArqTrab)
		dbCloseArea()
		Ferase(cArqTrab+GetDBExtension())
		Ferase(cArqTrab+OrdBagExt())
		cArqTrab := ""
		dbSelectArea("SC1")
		SC1->(DbSetOrder(1))
	EndIf

Return NIL
 	
/*{Protheus.doc} MenuDEF()
Menu do Browse Manual de Medi��o Automatizada
@author 	Mick William da Silva
@since 		11/07/2016
@version 	P12.7
@Project    MAN00000462901_EF_001
@Return 	aRotina, Retorna a Rotina a ser inclusa no Menu.
*/
Static Function MenuDef()
	Local aRotina := {}
	ADD OPTION aRotina TITLE 'Processar' ACTION 'U_F019Mark(cArqTrab)' OPERATION 2 ACCESS 0
Return aRotina


/*{Protheus.doc} F019Mark()
Rotina para Marca��o de Registro
@author 	Mick William da Silva
@since 		11/07/2016
@version 	P12.7
@Project    MAN00000462901_EF_001
@Param		cAliasTMP, Alias temporario.
@Return 	lRet, Retorna Verdadeiro caso n�o exista diverg�ncia, caso contr�rio False.
*/

User Function F019Mark(cAliasTMP)

	Local aArea		:= GetArea()
	Local lRet			:= .T.
	Local lMedPar		:= .T.
	Local nCt 			:= 0
	Local aProds		:= {}
	Local cCodUser 	:= Alltrim(RetCodUsr()) 		 //Retorna o Codigo do Usuario
	Local cNamUser 	:= Alltrim(UsrRetName( cCodUser ))//Retorna o nome do usuario
	Local oMark 		:= GetMarkBrow()
	Local aProdSel	:=	{}
	Local nX			:= 0
	Local aRet			:= {}
	Local aCabs		:= {}
	Local aItens		:= {}
	Local aAreaSC1	:= SC1->(GetArea())
	Local cEstoq	:= ""

	(cAliasTMP)->(dbGoTop())

	While (cAliasTMP)->(!Eof())

		If !EMPTY((cAliasTMP)->(TMP_OK))
		
			cEstoq := Posicione("P17", 1, xFilial("P17") + (cAliasTMP)->C1_PRODUTO, "P17_ESTOQ")
	
			Aadd(aProds, (cAliasTMP)->(C1_FILIAL)	)
			Aadd(aProds, (cAliasTMP)->(C1_NUM)		)
			Aadd(aProds, (cAliasTMP)->(C1_PRODUTO)	)
			Aadd(aProds, (cAliasTMP)->(C1_LOCAL)	)
			Aadd(aProds, (cAliasTMP)->(C1_CC)		)
			Aadd(aProds,  cNamUser					)
			Aadd(aProds, (cAliasTMP)->(C1_XTPSC)	)
			Aadd(aProds, (cAliasTMP)->(C1_FORNECE)	)
			Aadd(aProds, (cAliasTMP)->(C1_LOJA)		)
			Aadd(aProds, ""							)
			Aadd(aProds, ""							)
			Aadd(aProds, ""							)
			Aadd(aProds, (cAliasTMP)->(C1_QUANT)	)
			Aadd(aProds, (cAliasTMP)->(C1_ITEM)	)
			Aadd(aProds, ""							)
			Aadd(aProds, ""							)
			Aadd(aProds, (cAliasTMP)->C1_XCODSET	)
			Aadd(aProds, cEstoq						)
			
			Aadd(aProdSel,aProds)
			aProds := {}				
			
		EndIf

		(cAliasTMP)->(DbSkip())
	EndDo
	
	Begin Transaction
	For nX := 1 To Len(aProdSel)
		aRet:= U_F0100110( aProdSel[nX] ) //Execu��o Manual de Medi��o Automatizada
		
		If ValType(aRet) == "L"
			DisarmTransaction()
			aCabs := {}
			aItens:={}
			lRet := .F.
			Exit
		ElseIf Len(aRet[1]) > 0
			aAdd(aCabs,aRet[1][1])
			aAdd(aItens,aRet[1][2])
		Endif		

	Next
	If Len(aCabs) > 0 
		lRet := U_F0100111(aCabs,aItens)
	Endif
	
	If lRet
		(cAliasTMP)->(dbGoTop())
		While (cAliasTMP)->(!Eof())
			IF !Empty((cAliasTMP)->(TMP_OK))
				SC1->(DbSetOrder(1))
				If SC1->(DbSeek((cAliasTMP)->(C1_FILIAL)+(cAliasTMP)->(C1_NUM)+(cAliasTMP)->(C1_ITEM)))
					RecLock("SC1",.F.)
					SC1->C1_FLAGGCT := '1'
					SC1->C1_XITMED	:= "Utilizado pelo GCT"
					SC1->(MsUnlock())
				EndIf
				RECLOCK((cAliasTMP), .F.)
				(cAliasTMP)->(C1_FLAGGCT) := '1'
				(cAliasTMP)->( MsUnlock() )
			EndIf
			(cAliasTMP)->(DbSkip())
		EndDo
	Else
		MsgAlert("Medi��o N�o Gerada , verifique a Tela de Log!","Medi��o Parcial")
		DisarmTransaction()
	Endif
	End Transaction
	CloseBrowse()
	
	RestArea(aAreaSC1)
	RestArea(aArea)

	U_F0100109()
Return lRet