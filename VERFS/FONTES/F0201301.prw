#Include 'Protheus.ch'
#Include 'FwCommand.ch'

/*
{Protheus.doc} F0201301()
Holerite Eletr�nico Santander 
@Author     Bruno de Oliveira
@Since      19/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Menu		SIGAGPE
@Return 	lRet
*/
User Function F0201301()

	Local aArea		:= GetArea()
	Local cPerg		:= "FSW0201301"
	Local aSays		:= {}
	Local aButtons	:= {}
	Local nOpca		:= 0
	Local cCadastro	:= OemToAnsi("Processo de Gera��o de Holerite Eletr�nico Banco Santander")

	AjustPerg(cPerg)

	Pergunte(cPerg,.F.)
			
	AADD(aSays,OemToAnsi("Este programa realiza o processo de gera��o do Holerite Eletr�nico do Banco"))
	AADD(aSays,OemToAnsi("Santander"))
	
	AADD(aButtons, { 5,.T.,{|| Pergunte(cPerg,.T.)  } } )
	AADD(aButtons, { 1,.T.,{|o| nOpca := 1,If((MsgYesNo("Confirma configura��o dos par�metros?","Aten��o")),FechaBatch(),nOpca:=0) }} )
	AADD(aButtons, { 2,.T.,{|o| FechaBatch() }} )
		
	FormBatch(cCadastro,aSays,aButtons)

	If nOpca == 1
		If VldPergnt(mv_par10,mv_par11,mv_par02,mv_par12,mv_par13)
			Processa({|| FSGerArq(mv_par01,mv_par02,mv_par03,mv_par04,mv_par05,mv_par06,mv_par07,mv_par08,mv_par09,mv_par10,mv_par11,mv_par12,mv_par13,mv_par14),"Gerando Holerite Eletr�nico"})
		EndIf
	EndIf

	RestArea(aArea)

Return

/*
{Protheus.doc} FSGerArq()
Gera��o de Holerite Eletr�nico Santander 
@Author   	Bruno de Oliveira
@Since    	19/04/2016
@Version  	P12.1.07
@Project  	MAN00000463301_EF_013
@Param		cRecibo, caracter, roteiro de c�lculo
@Param		cFil, caracter, Filial da empresa
@Param		cCCT, caracter, Centro de Custo
@Param		cMatric, caracter, Matricula
@Param		cProcesso, caracter, Processo do per�odo
@Param		cNSeman, caracter, semana do per�odo
@Param		cNome, caracter, nome do funcion�rio
@Param		cSituac, caracter, situa��o do funcion�rio
@Param		cCategor, caracter, categoria do funcion�rio
@Param		cCaminho, caracter, caminho do arquivo
@Param		dDtLibPg, date, Data da libera��o do Pagamento
@Param		cRefOpLte, caracter, referencia da opera��o do Lote		
@Param		cBncAgConv, caracter, Banco+Agencia+Convenio
@Param		cLote, caracter, n�mero do Lote
@Param		dDataGer, date, data de gera��o
@Return 	lRet
*/
Static Function FSGerArq(cRecibo,cFil,cCCT,cMatric,cProcesso,cNSeman,cNome,cSituac,cCategor,cArq,dDtLibPg,cRefOpLte,cLote,dDataGer)

	Local aArea      := GetArea()
	Local cQuery     := ""
	Local cAcessaSRA := &("{ || " + ChkRH("GPER030 ","SRA","2") + "}")
	Local cSitFol    := ""
	Local cCateg     := ""
	Local nX
	Local cAliasMov  := ""
	Local cBncAgConv := SuperGetMv("FS_CONVEN",.F.,,cFil)
	Local cCCorrEmp  := SuperGetMv("FS_CONTCC",.F.,,cFil)
	Local cCaminho   := SuperGetMv("FS_ROTARQ",.F.,,cFil)

	Private nAteLim   := 0
	Private nBaseFgts := 0
	Private nFgts     := 0
	Private nBaseIr   := 0
	Private nBaseIrFe := 0
	Private nBaseInss := 0
	Private nSeqLan   := 0
	Private cLinP12   := ""
	Private aProvent  := {}
	Private aDescont  := {}
	Private aBaseP    := {}
	Private aCodFol   := {}
	Private nTotProv  := 0
	Private nTotDesc  := 0
	Private nTotRegt  := 0
	Private lCrdArq   := .F.

	nTFil	:= TAMSX3("RA_FILIAL")[1]
	nTCC	:= TAMSX3("RA_CC")[1]
	nTMatri:= TAMSX3("RA_MAT")[1]
	nTProc	:= TAMSX3("RA_PROCES")[1]

	If !Empty(cSituac)
	
		For nX := 1 To Len(cSituac)
	
			cPosSit := SubsTr(cSituac,nX,1)
			If cPosSit != "*"
				cSitFol += "'"+ cPosSit +"',"
			EndIf
	
		Next nX

		If Empty(cSitFol)
			cSitFol := " "
		EndIf
		cSitFol := SubStr(cSitFol,1,Len(cSitFol)-1)
	EndIf

	If !Empty(cCategor)
	
		For nX := 1 To Len(cCategor)
	
			cPosCat := SubsTr(cCategor,nX,1)
			If cPosCat != "*"
				cCateg += "'"+ cPosCat +"',"
			EndIf
	
		Next nX

		If Empty(cCateg)
			cCateg := " "
		EndIf
		cCateg := SubStr(cCateg,1,Len(cCateg)-1)
	EndIf

	cAliasSRA := GetNextAlias()
	MakeSqlExpr("FSW0201301")

	cQuery := "SELECT DISTINCT"
	cQuery += " RA_FILIAL,RA_MAT,RA_CC,RA_SITFOLH,RA_PROCES,RA_NOME,RA_DEMISSA,RA_CODFUNC "
	cQuery += "FROM "
	cQuery += "	" + RetSqlName("SRA") + " SRA "

	If 	cRecibo = 'FER' .OR. cRecibo == "RES"
		cQuery += "," + RetSqlName("SRR") + " SRR "
	EndIf

//If cRecibo = '132'
//	cQuery += "," + RetSqlName("SRI") + " SRI "
//EndIf

	If cRecibo = 'FOL' .OR. cRecibo = '131' .OR. cRecibo = 'ADI' .OR. cRecibo == "PLR" .OR. cRecibo == "132"
		cQuery += "," + RetSqlName("SRC") + " SRC "
	EndIf

	cQuery += "WHERE "
	cQuery += "SRA.RA_FILIAL = " + cFil + " AND "

	If !Empty(cMatric)
		cQuery += MV_PAR04 + " AND " //Range Matricula
	EndIf

	If !Empty(cCCT)
		cQuery += MV_PAR03 + " AND " //Range Centro de Custo
	EndIf

	If !Empty(cProcesso)
		cQuery += MV_PAR05 + " AND " // Range Processo
	EndIf

	If !Empty(cNome)
		cQuery += MV_PAR07 + " AND " //Range Nome
	EndIf

	cQuery += " SRA.RA_CTDEPSA <> '            ' AND"

	If !Empty(cSitFol)
		cQuery += " SRA.RA_SITFOLH IN (" + cSitFol + ") AND"
	EndIf

	If !Empty(cCateg)
		cQuery += " SRA.RA_CATFUNC IN (" + cCateg + ") AND"
	EndIf

	If cRecibo = 'FER' .OR. cRecibo == "RES"
		cQuery += "	SRR.RR_ROTEIR = '" + cRecibo + "'"
		cQuery += "	AND SRR.RR_MAT = SRA.RA_MAT"
		cQuery += "	AND SRR.D_E_L_E_T_ = ' ' AND"
	EndIf

	If cRecibo = 'FOL' .OR. cRecibo = '131' .OR. cRecibo = 'ADI' .OR. cRecibo == "PLR" .OR. cRecibo == "132"
		cQuery += "	SRC.RC_ROTEIR = '" + cRecibo + "'"
		cQuery += "	AND SRC.RC_MAT = SRA.RA_MAT"
		cQuery += "	AND SRC.D_E_L_E_T_ = ' ' AND"
	EndIf

	cQuery += "	SRA.D_E_L_E_T_ = ' ' "

	cQuery 	   := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSRA)

	aPerAtual := {}
	cFilRCH := xFilial("RCH")
	cPerAt := ""

	If fGetPerAtual(@aPerAtual,cFilRCH,(cAliasSRA)->RA_PROCES,cRecibo)
		cPerAt := aPerAtual[1][1]
	EndIf

	If ! (cAliasSRA)->(EOF()) .OR. cPerAt != ""
		nArq := FCreate(cCaminho + cArq)
		lCrdArq := .T.
		If nArq == -1
			lCrdArq := .F.
			Help(" ",1,"FSGerArq",,"N�o foi poss�vel criar o arquivo em :"+Alltrim(cCaminho)+", verifique caminho e nome do arquivo. Opera��o cancelada.",1,0)
			RestArea(aArea)
			Return
		EndIf
		ProcRegua((cAliasSRA)->(LASTREC()))
		FsHeadEmp(cRecibo,dDtLibPg,cRefOpLte,cBncAgConv,cLote,cCCorrEmp,cPerAt,cFil,dDataGer)
	EndIf

	While ! (cAliasSRA)->(EOF()) .AND. cPerAt != ""

		FsHeadComp((cAliasSRA)->RA_FILIAL,(cAliasSRA)->RA_MAT,cBncAgConv,(cAliasSRA)->RA_CODFUNC)

		Fp_CodFol(@aCodFol,(cAliasSRA)->RA_Filial)

		IncProc((cAliasSRA)->RA_FILIAL+"-"+(cAliasSRA)->RA_MAT+"-"+(cAliasSRA)->RA_NOME)

		If cRecibo == "ADI" .OR. cRecibo == "FOL"
	
			FBscMovPer((cAliasSRA)->RA_FILIAL,(cAliasSRA)->RA_MAT,cRecibo,(cAliasSRA)->RA_PROCES,cPerAt,cNSeman,aCodFol)
		
			nVlProv := FRegDetPrv((cAliasSRA)->RA_MAT)
			nVlDesc := FRegDetDsc((cAliasSRA)->RA_MAT)
			FsTrailFun(nVlProv,nVlDesc,(cAliasSRA)->RA_MAT)
		ElseIf cRecibo == "131" .OR. cRecibo == "PLR" .OR. cRecibo == "132"

			FBscMovPer((cAliasSRA)->RA_FILIAL,(cAliasSRA)->RA_MAT,cRecibo,(cAliasSRA)->RA_PROCES,cPerAt,"",aCodFol)
		
			nVlProv := FRegDetPrv((cAliasSRA)->RA_MAT)
			nVlDesc := FRegDetDsc((cAliasSRA)->RA_MAT)
			FsTrailFun(nVlProv,nVlDesc,(cAliasSRA)->RA_MAT)
		ElseIf cRecibo == "FER" .OR. cRecibo == "RES"
	
			FBscMovFer((cAliasSRA)->RA_FILIAL,(cAliasSRA)->RA_MAT,cRecibo,(cAliasSRA)->RA_PROCES,cPerAt,"",aCodFol)
		
			nVlProv := FRegDetPrv((cAliasSRA)->RA_MAT)
			nVlDesc := FRegDetDsc((cAliasSRA)->RA_MAT)
			FsTrailFun(nVlProv,nVlDesc,(cAliasSRA)->RA_MAT)
		EndIf

		(cAliasSRA)->(DbSkip())
	End

	FsTrailEmp(cBncAgConv,cFil)

	RestArea(aArea)

Return

/*
{Protheus.doc} FsHeadEmp()
Cabe�alho da Empresa
@Author     Bruno de Oliveira
@Since      22/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cRecibo, caracter, Roteiro de c�lculo
@Param		dDtLiPg, date, Data de libera��o Pagamento
@Param		cRefOpLte, caracter, referencia da Opera��o do Lote
@Param		cBncAgConv, caracter, Banco+Agencia+Convenio
@Param		cLote, caracter, n�mero do lote
@Param		cCCorrEmp, caracter, Conta Corrente da Empresa
@Param		cPerAt, caracter, Per�odo Atual
@Param		cFil, caracter, filial do perguntes
@Param		dDataGer, date, data da gera��o
@Return 	
*/
Static Function FsHeadEmp(cRecibo,dDtLibPg,cRefOpLte,cBncAgConv,cLote,cCCorrEmp,cPerAt,cFil,dDataGer)

	Local aArea   := GetArea()
	Local nLoteMg := SuperGetMv("MQ_CONTGP2",.F.,,cFil)
	Local aDdsEmp := FWArrFilAtu(cEmpAnt,cFil)
	Local nAux    := 0

	Do case
	Case cRecibo == "ADI" // Adiantamento
		cTpPgt := "J"
	Case cRecibo == "FOL" // Pagto Mensal
		cTpPgt := "P"
	Case cRecibo == "131" // 1 Parcela 13
		cTpPgt := "A"
	Case cRecibo == "132" // 2 Parcela 13
		cTpPgt := "B"
	Case cRecibo == "FER" // Ferias
		cTpPgt := "G"
	Case cRecibo == "PLR" // PLR
		cTpPgt := "R"
	EndCase

	If cRefOpLte == 1
		cRefOper := "T"
	ElseIf cRefOpLte == 2
		cRefOper := "A"
	ElseIf cRefOpLte == 3
		cRefOper := "S"
	EndIf

	cLinP12 := PADR("0",1)														// 0001-0001 Tipo de registro = 0
	cLinP12 += "E"																// 0002-0002 Tipo de Transmiss�o "E" (Envio)
	cLinP12 += PADR(cBncAgConv,20)												// 0003-0022 N�mero do Conv�nio da Empresa ,Composto por: Banco+Ag�ncia+conv�nio
	cLinP12 += PADR(cRefOper,1)													// 0023-0023 Tipo de arquivo (T=CARGA,A=ALTERACAO,S=SUBSTIRUICAO)
	cLinP12 += StrZero(nLoteMg,6)												// 0024-0029 Sequencia de envio do arquivo
	If cRefOper == 'T'
		cLinP12 += StrZero(nLoteMg,6)											// 0030-0035 N�mero do Lote
	
		Pergunte('FSW0201301',.F.)
		MV_PAR13 := nLoteMg

	Else
		cLinP12 += StrZero(VAL(cLote),6)									// 0030-0035 N�mero do Lote
	EndIf
	cLinP12 += FwNoAccent(Alltrim(aDdsEmp[6]) + space(47 - Len(Alltrim(aDdsEmp[6]))))	// 0036-0082 Nome da Empresa
	cLinP12 += aDdsEmp[18]														// 0083-0096 CNPJ empresa=03311116000130
	cLinP12 += PadR(cPerAt,6)													// 0097-0102 Data de Referencia do Pagamento - Mes/Ano de Pagamento (AAAAMM)
	cLinP12 += PadR(DTOS(dDtLibPg),8)											// 0103-0110 Data de cr�dito (AAAAMMDD)
	cLinP12 += PadR(DTOS(dDtLibPg),8)											// 0111-0118 Data de liberacao do consulta comprovante (AAAAMMDD)
	cLinP12 += PadR(DTOS(dDataGer),8)											// 0119-0126 Data da gera��o do arquivo (AAAAMMDD)
	cLinP12 += SubStr(TIME(),1,2)+SUBSTR(TIME(),4,2)+SUBSTR(TIME(),7,2)	// 0127-0132 Hora da gera��o do arquivo
	cLinP12 += cCCorrEmp														// 0133-0152 Banco da Empresa
	cLinP12 += cTpPgt																// 0153-0153 Tipo do pagamento
	cLinP12 += Space(247)														// 0154-0400 Livre para futuras utiliza��es

	cLinP12 += Chr(13) + Chr(10)
	FWrite(nArq, cLinP12, Len(cLinP12))
	nTotRegt++
	cLinP12 := ""

	RestArea(aArea)

Return

/*
{Protheus.doc} FsHeadComp()
Cabe�alho do Funcion�rio
@Author     Bruno de Oliveira
@Since      22/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cFilQry, caracter, Filial da Empresa
@Param		cMatricFun, caracter, Matricula do Funcion�rio
@Param		cBncAgConv, caracter, Banco+Agencia+Convenio
@param		cCodFunc, caractere, Codigo da fun��o
@Return 	
*/
Static Function FsHeadComp(cFilQry,cMatricFun,cBncAgConv,cCodFunc)

	Local aArea		:= GetArea()
	Local aAreaSRA	:= SRA->(GetArea())

	Local cFunc      := ""
	Local cFilSRJ    := ""
	Local cFilSRJOri := xFilial("SRJ")
	Local nTamFilTot := Len(cFilSRJOri)
	Local nTamFilSRA := Len(AllTrim(cFilQry))
	Local nTamFilSRJ := Len(AllTrim(cFilSRJOri))
	
	If nTamFilSRJ > nTamFilSRA
		cFunc := "ER.COMPART. SRJ"
	ElseIf nTamFilSRJ == nTamFilSRA 
		cFilSRJ := cFilQry
	ElseIf nTamFilSRJ == 0 
		cFilSRJ := cFilSRJOri
	Else
		cFilSRJ := PadR(Left(cFilQry,nTamFilSRJ),nTamFilTot)
	EndIf
	
	If Empty(cFunc)
		cFunc := Posicione("SRJ",1,cFilSRJ+cCodFunc,"RJ_DESC")
	EndIf

	nSeqLan := 0

	DbSelectArea("SRA")
	SRA->(DbSetOrder(1))
	SRA->(DbSeek(cFilQry+cMatricFun))

	cLinP12 += PadR("1",1)																				// 0001-0001 Tipo de registro = 1
	cLinP12 += PadR(cBncAgConv,20)																		// 0002-0021 N�mero do Conv�nio da Empresa ,Composto por: Banco+Ag�ncia+conv�nio
	cLinP12 += Padr("00000000000000"+SubStr(cMatricFun,1,6),20)										// 0022-0041 Matricula do funcionario
	cLinP12 += Padr(SRA->RA_CIC,11)																		// 0042-0052 CPF funcionario
	cLinP12 += Padr(Alltrim(SRA->RA_NOME),47)		  													// 0053-0099 Nome funcionario
	cLinP12 += StrZero(Val(SubStr(SRA->RA_BCDEPSA,1,3)),4)											// 0100-0103 Banco do funcionario Deve ser 0008 ou 0033 ou 0353 (INCLUIR COM 3 CARACTERES)
	cLinP12 += StrZero(Val(SubStr(SRA->RA_BCDEPSA,4,4)),4)											// 0104-0107 Numero da agencia do funcionario ANGENCIA COM 4 CARACTERES
	cLinP12 += STRZERO(VAL(SUBSTR(SRA->RA_CTDEPSA, 1, LEN(ALLTRIM(SRA->RA_CTDEPSA)) )),12)	// 0108-0119 Numero da conta do funcionario
	cLinP12 += Padr("HISTORICO DE DEBITOS",35)														// 0120-0154 Cabe�alho D�bito
	cLinP12 += Padr("HISTORICO DE CREDITOS",35)														// 0155-0189 Cabe�alho Cr�dito
	cLinP12 += Padr("LIQUIDO A RECEBER",35)															// 0190-0224 Cabe�alho LIQUIDO A RECEBER
	cLinP12 += Space(1)																					// 0225-0225 Bloqueio de visualiza��o e impress�o do Holerite
	cLinP12 += Space(15)																					// 0226-0240 livre para futuras utiliza��es
	cLinP12 += "000000000000"																			// 0241-0252 livre para futuras utiliza��es
	cLinP12 += Substr(cFunc,1,15)																// 0253-0267 Descricao do cargo do funcionario
	cLinP12 += "N"																						// 0268-0268 Indicador de matr�cula 'S' MAIS DE UMA , 'N' SOMENTE UMA
	cLinP12 += Space(132)																				// 0269-0400 Livre para futuras utiliza��es

	cLinP12 += Chr(13) + Chr(10)
	FWrite(nArq, cLinP12, Len(cLinP12))
	nTotRegt++
	cLinP12 := ""

	RestArea(aAreaSRA)
	RestArea(aArea)

Return

/*
{Protheus.doc} FBscMovPer()
Busca os Movimentos do Periodo
@Author     Bruno de Oliveira
@Since      22/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cFilQry, caracter, Filial do sistema
@Param		cMat, caracter, matricula do funcion�rio
@Param		cRecibo, caracter, Roteiro de C�lculo
@Param		cProcess, caracter, Processo do per�odo
@Param		cPerAtu, caracter, Per�odo Atual
@Param		cNSeman, caracter, Numero da Semana
@Param		aCodFol, array, c�digo das verbas
@Return 	
*/
Static Function FBscMovPer(cFilQry,cMat,cRecibo,cProcess,cPerAtu,cNSeman,aCodFol)

	Local aArea   := GetArea()
	Local cQuery  := ""
	Local cFilSRV := xFilial("SRV")
	Local cIRfSem := SuperGetMv("MV_IREFSEM",,"S")
	Local cAlias1 := ''

	nAteLim	:= 0
	nBaseFgts	:= 0
	nFgts		:= 0
	nBaseIr	:= 0
	nBaseInss	:= 0
	nBaseIrFe	:= 0
	aProvent	:= {}
	aDescont	:= {}
	aBaseP		:= {}

	cAlias1 := GetNextAlias()

	cQuery := "SELECT "
	cQuery += "RC_FILIAL,RC_MAT,RC_PD,RC_VALOR,RC_SEMANA,RC_PROCES,RC_PERIODO,RC_ROTEIR,"
	cQuery += "RC_QTDSEM,RC_HORAS,RV_TIPOCOD,RV_DESC "
	cQuery += "FROM "
	cQuery += "	" + RetSqlName("SRC") + " SRC "
	cQuery += "LEFT JOIN "
	cQuery += " " + RetSqlName("SRV") + " SRV "
	cQuery += "ON (SRV.RV_COD = SRC.RC_PD "
	cQuery += "AND 	SRV.D_E_L_E_T_ = ' ') "
	cQuery += "WHERE "
	cQuery += "SRC.RC_FILIAL = " + cFilQry + " AND "
	cQuery += "SRC.RC_MAT = " + cMat + " AND "
	cQuery += "SRC.RC_PROCES = " + cProcess + " AND "
	cQuery += "SRC.RC_ROTEIR = '" + cRecibo + "' AND "
	cQuery += "SRC.RC_PERIODO = " + cPerAtu + " AND "

	If !Empty(cNSeman)
		cQuery += "SRC.RC_SEMANA = " + cNSeman + " AND "
	EndIf

	cQuery += " SRC.D_E_L_E_T_ = ' ' "

	cQuery 	   := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)

	While ! (cAlias1)->(EOF())
		nHoras := 0
		nValor := 0
		nHoras := If((cAlias1)->RC_QTDSEM > 0 .And. cIRfSem == 'S', (cAlias1)->RC_QTDSEM, (cAlias1)->RC_HORAS)
		nValor := (cAlias1)->RC_VALOR
	
		If (cAlias1)->RV_TIPOCOD == "1" //Provento
	
			nPos := Ascan(aProvent,{ |X| X[1] == (cAlias1)->RC_PD })
			If nPos == 0
				Aadd(aProvent,{(cAlias1)->RC_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aProvent[nPos,3] += nHoras
				aProvent[nPos,4] += nValor
			Endif

		ElseIf (cAlias1)->RV_TIPOCOD == "2" //Desconto
		
			nPos := Ascan(aDescont,{ |X| X[1] == (cAlias1)->RC_PD })
			If nPos == 0
				Aadd(aDescont,{(cAlias1)->RC_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aDescont[nPos,3] += nHoras
				aDescont[nPos,4] += nValor
			EndIf
	
		ElseIf (cAlias1)->RV_TIPOCOD == "3" //Base(Provento)
	
			nPos := Ascan(aBaseP,{ |X| X[1] == (cAlias1)->RC_PD })
			If nPos == 0
				Aadd(aBaseP,{(cAlias1)->RC_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aBaseP[nPos,3] += nHoras
				aBaseP[nPos,4] += nValor
			Endif
	
		EndIf

		If cRecibo = "ADI"
			If (cAlias1)->RC_PD == aCodFol[10,1]
				nBaseIr := (cAlias1)->RC_VALOR
			EndIf
		ElseIf (cAlias1)->RC_PD == aCodFol[835,1]
			nBaseIr += (cAlias1)->RC_VALOR

		ElseIf (cAlias1)->RC_PD == aCodFol[27,1]
			nBaseIr += (cAlias1)->RC_VALOR
		
		ElseIf (cAlias1)->RC_PD == aCodFol[19,1]
			nAteLim += (cAlias1)->RC_VALOR
		
		ElseIf (cAlias1)->RC_PD == aCodFol[13,1]
			nAteLim += (cAlias1)->RC_VALOR
		// BASE FGTS SAL, 13.SAL E DIF DISSIDIO E DIF DISSIDIO 13
		ElseIf (cAlias1)->RC_PD $ aCodFol[108,1]+'*'+aCodFol[17,1]+'*'+ aCodFol[337,1]+'*'+aCodFol[398,1]
			nBaseFgts += (cAlias1)->RC_VALOR
		// VALOR FGTS SAL, 13.SAL E DIF DISSIDIO E DIF.DISSIDIO 13
		ElseIf (cAlias1)->RC_PD $ aCodFol[109,1]+'*'+aCodFol[18,1]+'*'+aCodFol[339,1]+'*'+aCodFol[400,1]
			nFgts += (cAlias1)->RC_VALOR
		ElseIf (cAlias1)->RC_PD == aCodFol[15,1]
			nBaseIr += (cAlias1)->RC_VALOR
		ElseIf (cAlias1)->RC_PD == aCodFol[16,1]
			nBaseIrFe += (cAlias1)->RC_VALOR
		EndIf
	
		(cAlias1)->(DbSkip())
	End
	(cAlias1)->(DbCloseArea())
	RestArea(aArea)

Return

//===============================================================================================================================

/*
{Protheus.doc} FBscMovFer()
Busca os Movimentos do Periodo
@Author     Henrique Madureira
@Since      18/05/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cFilQry, caracter, Filial do sistema
@Param		cMat, caracter, matricula do funcion�rio
@Param		cRecibo, caracter, Roteiro de C�lculo
@Param		cProcess, caracter, Processo do per�odo
@Param		cPerAtu, caracter, Per�odo Atual
@Param		cNSeman, caracter, Numero da Semana
@Param		aCodFol, array, c�digo das verbas
@Return 	
*/
Static Function FBscMovFer(cFilQry,cMat,cRecibo,cProcess,cPerAtu,cNSeman,aCodFol)

	Local aArea   := GetArea()
	Local cQuery  := ""
	Local cFilSRV := xFilial("SRV")
	Local cIRfSem := SuperGetMv("MV_IREFSEM",,"S")
	Local cAlias1 := ''

	nAteLim	:= 0
	nBaseFgts	:= 0
	nFgts		:= 0
	nBaseIr	:= 0
	nBaseInss	:= 0
	nBaseIrFe	:= 0
	aProvent	:= {}
	aDescont	:= {}
	aBaseP		:= {}

	cAlias1 := GetNextAlias()

	cQuery := "SELECT "
	cQuery += "RR_FILIAL,RR_MAT,RR_PD,RR_VALOR,RR_SEMANA,RR_PROCES,RR_PERIODO,RR_ROTEIR,"
	cQuery += "RR_HORAS,RV_TIPOCOD,RV_DESC "
	cQuery += "FROM "
	cQuery += "	" + RetSqlName("SRR") + " SRR "
	cQuery += "LEFT JOIN "
	cQuery += " " + RetSqlName("SRV") + " SRV "
	cQuery += "ON (SRV.RV_COD = SRR.RR_PD "
	cQuery += "AND 	SRV.D_E_L_E_T_ = ' ') "
	cQuery += "WHERE "
	cQuery += "SRR.RR_FILIAL = '" + cFilQry + "' AND "
	cQuery += "SRR.RR_MAT = '" + cMat + "' AND "
	cQuery += "SRR.RR_PROCES = '" + cProcess + "' AND "
	cQuery += "SRR.RR_ROTEIR = '" + cRecibo + "' AND "
	cQuery += "SRR.RR_PERIODO = '" + cPerAtu + "' AND "

	If !Empty(cNSeman)
		cQuery += "SRR.RR_SEMANA = '" + cNSeman + "' AND "
	EndIf

	cQuery += " SRR.D_E_L_E_T_ = ' ' "

	cQuery 	   := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)

	While ! (cAlias1)->(EOF())

		nHoras := (cAlias1)->RR_HORAS
		nValor := (cAlias1)->RR_VALOR
	
		If (cAlias1)->RV_TIPOCOD == "1" //Provento
	
			nPos := Ascan(aProvent,{ |X| X[1] == (cAlias1)->RR_PD })
			If nPos == 0
				Aadd(aProvent,{(cAlias1)->RR_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aProvent[nPos,3] += nHoras
				aProvent[nPos,4] += nValor
			Endif

		ElseIf (cAlias1)->RV_TIPOCOD == "2" //Desconto
		
			nPos := Ascan(aDescont,{ |X| X[1] == (cAlias1)->RR_PD })
			If nPos == 0
				Aadd(aDescont,{(cAlias1)->RR_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aDescont[nPos,3] += nHoras
				aDescont[nPos,4] += nValor
			EndIf
	
		ElseIf (cAlias1)->RV_TIPOCOD == "3" //Base(Provento)
	
			nPos := Ascan(aBaseP,{ |X| X[1] == (cAlias1)->RR_PD })
			If nPos == 0
				Aadd(aBaseP,{(cAlias1)->RR_PD,(cAlias1)->RV_DESC,nHoras,nValor })
			Else
				aBaseP[nPos,3] += nHoras
				aBaseP[nPos,4] += nValor
			Endif
	
		EndIf

		If (cAlias1)->RR_PD == aCodFol[13,1]
			nAteLim += (cAlias1)->RR_VALOR
		// BASE FGTS SAL, 13.SAL E DIF DISSIDIO E DIF DISSIDIO 13
		ElseIf (cAlias1)->RR_PD $ aCodFol[108,1]+'*'+aCodFol[17,1]+'*'+ aCodFol[337,1]+'*'+aCodFol[398,1]
			nBaseFgts += (cAlias1)->RR_VALOR
		// VALOR FGTS SAL, 13.SAL E DIF DISSIDIO E DIF.DISSIDIO 13
		ElseIf (cAlias1)->RR_PD $ aCodFol[109,1]+'*'+aCodFol[18,1]+'*'+aCodFol[339,1]+'*'+aCodFol[400,1]
			nFgts += (cAlias1)->RR_VALOR
		ElseIf (cAlias1)->RR_PD == aCodFol[16,1]
			nBaseIr += (cAlias1)->RR_VALOR
		EndIf
	
		(cAlias1)->(DbSkip())
	End
	(cAlias1)->(dbCloseArea())
	RestArea(aArea)

Return

/*
{Protheus.doc} FRegDetPrv()
Registro Detalhe lan�amento de Provento
@Author     Bruno de Oliveira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cMatric, caracter, Matricula do Funcion�rio
@Return 	nVlProv, Valor total dos Proventos
*/
Static Function FRegDetPrv(cMatric)

	Local aArea	:= GetArea()
	Local cVlProv	:= ""
	Local nVlProv	:= 0
	Local nX		:= 0

	For nX := 1 To Len(aProvent)

		If Str(aProvent[nX,4]) $ ","
			cVlProv := StrZero(StrTran(aProvent[nX,4],",",,1,),9)
		Else
			cVlProv := StrZero(aProvent[nX,4]*100,9)
		EndIf

		nSeqLan++
	
		cLinP12 += "2"												// 0001-0001 Tipo de registro = 2
		cLinP12 += Padr("00000000000000"+SubStr(cMatric,1,6),20)	// 0002-0021 Matricula do funcionario
		cLinP12 += StrZero(nSeqLan,3)								// 0022-0024 NUmero sequencial do lan�amento
		cLinP12 += PadL(AllTrim(aProvent[nX,1]),7,"0")				// 0025-0031 Codigo de lancamento definido pela empresa ? Completado com Zeros � Esquerda.
		cLinP12 += PadR(aProvent[nX,2],23)							// 0032-0054 Descricao do lancamento
		cLinP12 += Space(3)											// 0055-0057 Livre para futuras utiliza��es
		cLinP12 += PadL(cVlProv,9)									// 0058-0066 Valor do lan�amento, valor em reais no formato 9(07)V99. Se inexistente, preencher com zeros.
		cLinP12 += PadR(aProvent[nX,2],23)+Space(57)				// 0067-0146 Repetir descri��o do lan�amento + 57 espacos direita
		cLinP12 += "C"												// 0147-0147 D = DEBITO , C = CREDITO
		cLinP12 += Space(9)											// 0148-0156 Unidade de trabalho ,Se inexistente, preencher com brancos
		cLinP12 += "00000"											// 0157-0161 Qtde trabalhadas
		cLinP12 += Space(1)											// 0162-0162 Livre para futuras utiliza��es
		cLinP12 += "00010101"										// 0163-0170 Campo de uso privativo do banco
		cLinP12 += "00010101"										// 0171-0178 Campo de uso privativo do banco
		cLinP12 += Space(222)										// 0179-0400 Livre para futuras utiliza��es
	
		cLinP12 += Chr(13) + Chr(10)
		FWrite(nArq, cLinP12, Len(cLinP12))
		nTotRegt++
		cLinP12 	:= ""

		nVlProv := nVlProv + aProvent[nX,4]
		
	Next nX

	RestArea(aArea)

Return nVlProv

/*
{Protheus.doc} FRegDetDsc()
Registro Detalhe lan�amento de Desconto
@Author     Bruno de Oliveira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cMatric, caracter, matricula do funcion�rio
@Return 	nVlDesc, Valor total dos Descontos
*/
Static Function FRegDetDsc(cMatric)

	Local aArea	:= GetArea()
	Local cVlDesc	:= ""
	Local nVlDesc	:= 0
	Local nX		:= 0

	For nX := 1 to Len(aDescont)

		If Str(aDescont[nX,4]) $ ","
			cVlDesc := StrZero(StrTran(aDescont[nX,4],",",,1,),9)
		Else
			cVlDesc := StrZero(aDescont[nX,4]*100,9)
		EndIf
	
		nSeqLan++
	
		cLinP12 += PadR("2",1)										// 0001-0001 Tipo de registro = 2
		cLinP12 += Padr("00000000000000"+SubStr(cMatric,1,6),20)	// 0002-0021 Matricula do funcionario
		cLinP12 += StrZero(nSeqLan,3)								// 0022-0024 Numero sequencial do lan�amento
		cLinP12 += PadL(AllTrim(aDescont[nX,1]),7,"0")				// 0025-0031 Codigo de lancamento definido pela empresa
		cLinP12 += PadR(aDescont[nX,2],23)							// 0032-0054 Descricao do lancamento
		cLinP12 += Space(3)											// 0055-0057 Livre para futuras utiliza��es
		cLinP12 += PadL(cVlDesc,9)									// 0058-0066 Valor do lancamento, valor em reais no formato 9(07)V99. Se inexistente, preencher com zeros.
		cLinP12 += PadR(aDescont[nX,2],23)+Space(57)				// 0067-0146 Repetir Descri��o de Lan�amento + 57 espacos direita
		cLinP12 += "D"												// 0147-0147 D = DEBITO , C = CREDITO
		cLinP12 += Space(9)											// 0148-0156 Unidade de trabalho ,Se inexistente, preencher com brancos
		cLinP12 += "00000"											// 0157-0161 Qtde trabalhadas
		cLinP12 += Space(1)											// 0162-0162 Livre para futuras utiliza��es
		cLinP12 += "00010101"										// 0163-0170 Campo de uso privativo do Banco
		cLinP12 += "00010101"										// 0171-0178 Campo de uso privatico do Banco
		cLinP12 += Space(222)										// 0179-0400 reservado
	
		cLinP12 += Chr(13) + Chr(10)
		FWrite(nArq, cLinP12, Len(cLinP12))
		nTotRegt++
		cLinP12 	:= ""

		nVlDesc := nVlDesc + aDescont[nX,4]

	Next nX

	RestArea(aArea)

Return nVlDesc

/*
{Protheus.doc} FsTrailFun()
Trailler Funcion�rio
@Author     Bruno de Oliveira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		nVlProv, numerico, valor do Provento
@Param		nVlDesc, numerico, valor do Desconto
@Param		cMatric, caracter, matricula do funcion�rio
@Return 	
*/
Static Function FsTrailFun(nVlProv,nVlDesc,cMatric)

	Local aArea	:= GetArea()
	Local nLiquid	:= 0

	cLinP12 += PadR("3",1)										// 0001-0001 Tipo de registro = 3
	cLinP12 += Padr("00000000000000"+SubStr(cMatric,1,6),20)// 0002-0021 Matricula do funcionario
	cLinP12 += StrZero(nVlDesc*100,15)							// 0022-0036 Total valor a D�bito
	cLinP12 += StrZero(nVlProv*100,15)							// 0037-0051 Total valor a Cr�dito
	cLinP12 += StrZero((nVlProv-nVlDesc)*100,15)				// 0052-0066 Total valor L�quido
	cLinP12 += StrZero(nSeqLan,10)								// 0067-0076 Total de registros lan�ados para o funcion�rio
	cLinP12 += PadR("BASE IRRF",9)								// 0077-0085 Mensagem do Comprovante
	cLinP12 += PadR(TRANSFORM(nBaseIr,"@E 999,999.99"),10)	// 0086-0095 Base IRRF Dever� conter (exemplo �999.999,99�) Alinhar � direita COM ESPA�OS A ESQ ,Verificar a picture
	cLinP12 += Space(9)											// 0096-0104 reservado
	cLinP12 += PadR("BASE INSS",9)								// 0105-0113 mensagem
	cLinP12 += PadR(TRANSFORM(nAteLim,"@E 999,999.99"),10)	// 0114-0123 valor base INSS
	cLinP12 += PadR("BASE FGTS",9)								// 0124-0132 mensagem
	cLinP12 += PadR(TRANSFORM(nBaseFgts,"@E 999,999.99"),10)// 0133-0142 Valor base FGTS verificar picture
	cLinP12 += Space(9)											// 0143-0151 RESERVADO
	cLinP12 += PadR("FGTS MES ",9)								// 0152-0160 FGTS do m�s
	cLinP12 += PadR(TRANSFORM((nFgts),"@E 999,999.99"),10)	// 0161-0170 Valor FGTS do m�s
	cLinP12 += Space(230)										// 0171-0400 reservado

	cLinP12 += Chr(13) + Chr(10)
	FWrite(nArq, cLinP12, Len(cLinP12))
	nTotRegt++
	cLinP12 := ""
    
	nTotProv := nTotProv + nVlProv
	nTotDesc := nTotDesc + nVlDesc
	//nTotRegt += nSeqLan
	

	RestArea(aArea)

Return

/*
{Protheus.doc} FsTrailEmp()
Trailler Empresa
@Author     Bruno de Oliveira
@Since      25/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cBncAgConv, caracter, Banco+Agencia+Convenio
@Param		cFIL, caracter, filial do pergunte
@Return 	
*/
Static Function FsTrailEmp(cBncAgConv,cFil)

	Local aArea 	:= GetArea()
	Local nLotP12 := SuperGetMv("MQ_CONTGP2",.F.,,cFil)

	nTotRegt++
	cLinP12 += PadR("9",1)									// 0001-0001 Tipo de registro = 9
	cLinP12 += PADR(cBncAgConv,20)							// 0002-0021 N�mero do Conv�nio da Empresa ,Composto por: Banco+Ag�ncia+conv�nio
	cLinP12 += StrZero(Val(SM0->M0_CGC),14)				// 0022-0035 CNPJ empresa=03311116000130
	cLinP12 += StrZero(nTotDesc*100,15)					// 0036-0050 Total dos d�bitos
	cLinP12 += StrZero(nTotProv*100,15)					// 0051-0065 Total dos cr�ditos
	cLinP12 += StrZero((nTotProv-nTotDesc)*100,15)		// 0066-0080 Total valor L�quido
	cLinP12 += StrZero(nTotRegt,10)							// 0081-0090 Total de Registros do Arquivo
	cLinP12 += Space(310)									// 0091-0400 Reservado

	cLinP12 += Chr(13) + Chr(10)
	If lCrdArq
		FWrite(nArq, cLinP12, Len(cLinP12))
		cLinP12 	:= ""
		FClose(nArq)
		nLotP12 := nLotP12 + 1
		DbSelectArea("SX6")
		SX6->(DbSetOrder(1))
		If SX6->(DbSeek(cFil+"MQ_CONTGP2"))
			RecLock("SX6",.F.)
			SX6->X6_CONTEUD := AllTrim(STR(nLotP12))
			SX6->(MsUnLock())
		EndIf
		MsgInfo("Arquivo gerado")
	Else
		MsgInfo("Arquivo n�o foi gerado, verificar par�metros")
	EndIf

	RestArea(aArea)

Return

/*
{Protheus.doc} VldPergnt()
Valida��o dos perguntes
@Author     Bruno de Oliveira
@Since      27/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cPerg, caracter, Pergunta a ser criada
@Param		cFIL, caracter, filial do pergunte
@Return 	
*/
Static Function VldPergnt(cArq,dDtLbPgto,cFil,nTipo,cLote)

	Local aArea 		:= GetArea()
	Local lRet 		:= .T.
	Local cCaminho	:= SuperGetMv("FS_ROTARQ",.F.,,cFil)
	Local cParaCon	:= SuperGetMv("FS_CONVEN",.F.,,cFil)
	Local cConCorr	:= SuperGetMv("FS_CONTCC",.F.,,cFil)

	If Empty(cCaminho)
		Help(" ",1,"VldPergnt",,"N�o foi informado o Caminho do arquivo, verificar par�metro FS_ROTARQ",1,0)
		lRet := .F.
	ElseIf Empty(dDtLbPgto)
		Help(" ",1,"VldPergnt",,"Data de libera��o do pagamento n�o foi informada",1,0)
		lRet := .F.
	ElseIf nTipo == 2 .AND. EMPTY(cLote)
		Help(" ",1,"VldPergnt",,"Data de libera��o do pagamento n�o foi informada",1,0)
		lRet := .F.
	ElseIf Empty(cArq)
		Help(" ",1,"VldPergnt",,"N�o foi informado nome do arquivo",1,0)
		lRet := .F.
	ElseIf Empty(cParaCon)
		Help(" ",1,"VldPergnt",,"Informe o numero do conv�nio, verificar par�metro FS_CONVEN",1,0)
		lRet := .F.
	ElseIf Empty(cConCorr)
		Help(" ",1,"VldPergnt",,"Informe o n�mero do banco, ag�ncia e conta, verificar par�metro FS_CONTCC",1,0)
		lRet := .F.
	EndIf

	If lRet
		If !Empty(cCaminho)
			If File(cCaminho + cArq)
				If MsgNoYes("J� existe um arquivo com esse nome, deseja substituir?")
					lRet := .T.
				Else
					lRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

Return lRet

/*
{Protheus.doc} AjustPerg()
Cria��o do Pergunte 
@Author     Bruno de Oliveira
@Since      19/04/2016
@Version    P12.1.07
@Project    MAN00000463301_EF_013
@Param		cPerg, caracter, Pergunta a ser criada
@Return 	
*/
Static Function AjustPerg(cPerg)

	Local aArea := GetArea()

	PutSX1(cPerg,'01','Imprimir Recibos?		','','','MV_CH1','C',03,, ,'G','NaoVazio()','FSSRY'	,'','','MV_PAR01','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'02','Filial ?					','','','MV_CH2','C',08,, ,'G','NaoVazio()','SM0'	,'','','MV_PAR02','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'03','Centro de Custo ?		','','','MV_CH3','C',09,, ,'R','          ','CTT'	,'','','MV_PAR03','','','','RA_CC','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'04','Matricula ?				','','','MV_CH4','C',99,, ,'R','          ','FS013'	,'','','MV_PAR04','','','','RA_MAT','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'05','Processo  ?				','','','MV_CH5','C',05,, ,'R','		  	','RCJ2'	,'','','MV_PAR05','','','','RA_PROCES','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'06','Numero da Semana ?		','','','MV_CH6','C',02,, ,'G','          ','RCH01'	,'','','MV_PAR06','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'07','Nome ?					','','','MV_CH7','C',99,, ,'R','          ','SRA'	,'','','MV_PAR07','','','','RA_NOME','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'08','Situa��es Func. ?  		','','','MV_CH8','C',05,, ,'G','fSituacao ','   '	,'','','MV_PAR08','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'09','Categorias Func. ?		','','','MV_CH9','C',15,, ,'G','fCategoria','   '	,'','','MV_PAR09','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'10','Nome do Arquivo ?		','','','MV_CHA','C',70,, ,'G','          ','   '	,'','','MV_PAR10','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'11','Data Libera��o Pagto?	','','','MV_CHB','D',08,, ,'G','          ','   '	,'','','MV_PAR11','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'12','Ref. Opera��o Lote ?	','','','MV_CHC','N',01,,1,'C','NaoVazio()','   '	,'','','MV_PAR12','Inclusao','','','','Alteracao','','','Substituicao','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'13','Lote a Alterar/Subs ?	','','','MV_CHD','C',06,, ,'G',''          ,'   '	,'','','MV_PAR13','','','','','','','','','','','','','','','','',{""},{""},{""})
	PutSX1(cPerg,'14','Data da Gera��o ?		','','','MV_CHE','D',08,, ,'G','NaoVazio()','   '	,'','','MV_PAR14','','','','','','','','','','','','','','','','',{""},{""},{""})

	RestArea(aArea)

Return