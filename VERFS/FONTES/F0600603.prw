#INCLUDE "TOTVS.CH"

//Layout aDados 
#Define EF_FILIAL   	1		//Filial
#Define EF_MATRIC   	2		//Matricula
#Define EF_DTDEMI   	3		//Dt Demissao
#Define EF_MOTIVO   	4		//Motivo
#Define EF_OPERACAO	5		//Operacao
#Define EF_ID			6		//ID
#Define EF_STATUS   	7		//Status
#Define EF_OBSERVA  	8		//Observa


//-----------------------------------------------------------------------
/*/{Protheus.doc} F0600603
Funcao para consumir WS ApData
 
@author Eduardo Fernandes 
@since  19/12/2016
@return Nil  

@project MAN0000007423040_EF_006
@cliente Rededor
@version P12.1.7
             
/*/
//-----------------------------------------------------------------------
User Function F0600603(aParam)
	Local nX		  := 0
	Local aFiles	  := {'RH3', 'RH4', 'P10'}
	Local dDtIni    := Date()
	Local cHora     := Time()
	Local cEmpIni   := IIF(ValType(aParam) == "A", aParam[1,1], cEmpAnt)
	Local cFilIni   := IIF(ValType(aParam) == "A", aParam[1,2], cFilAnt)
	Local aDados    := {}
	Local aValues   := {}
	Local aTblStru  := {}	
	Local cNewAlias := ""
	Local cMsgError := ""
	Local lMontaAmb := .F.
	
	
	//Verifica se j� existe um Job em execu��o com o mesmo nome.
	If !LockByName("F0600603_"+cFilIni ,.F.,.F. )
		Conout('F0600603 - J� existe um Job com mesmo nome em execu��o!')
	Else
		ConOut("**************************************************************************")
		ConOut("* F0600603: Integra��o Cadastro de Rescisao x WS ApData	 				 *")
		ConOut("* In�cio: " + Dtos(dDtIni) + " - " + cHora + "                   		 	 *")
		ConOut("* Montagem do ambiente na empresa " + cEmpIni + " - " + cFilIni + " 		 *")
		Conout("* F0600603 - Inicio Thread: '" + cValToChar(ThreadID()) 					   )
		ConOut("**************************************************************************")	  	  
	  		
		If Select("SX2") == 0 
			RpcSetType( 3 )
			If !RpcSetEnv( cEmpIni, cFilIni,,,,, aFiles )
				Conout('F0600603 - Nao foi possivel inicializar o ambiente')
				UnLockByName("F0600603_"+cFilIni,.F.,.F. )
				Return .F.
			Endif
			lMontaAmb := .T.
		Endif
				
		PtInternal(1, "F0600603: Integra��o WS ApData x Rescisao - Filial: " + cFilAnt)														
											
		// Conexao com base Externa
		cMsgError := ""
		If !U_F06001C1(@cMsgError)
			Conout(cMsgError)
			UnLockByName("F0600603_"+cFilIni,.F.,.F. )			
			Return .F.
		EndIf
																					
		//Montando estrutura da tabela
		AAdd(aTblStru, {"EF06006_ID"			,	"VARCHAR",	032})
		AAdd(aTblStru, {"EF06006_FILIAL"		,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_MATRICULA"	,	"VARCHAR",	006})
		AAdd(aTblStru, {"EF06006_DTATEND"		,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_MOTIVO"		,	"VARCHAR",	006})
		AAdd(aTblStru, {"EF06006_DTTRANSAC"	,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_HRTRANSAC"	,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_OPERACAO"		,	"VARCHAR",	006})
		AAdd(aTblStru, {"EF06006_STATUS"		,	"VARCHAR",	001})
		AAdd(aTblStru, {"EF06006_DTPROCESS"	,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_HRPROCESS"	,	"VARCHAR",	008})
		AAdd(aTblStru, {"EF06006_OBSERVA"		,	"VARCHAR",	700})		
				
		cMsgError := ""
		If !U_F06001C3("EF06006", aTblStru, @cMsgError)				
			Conout("Erro na cria��o da Tabela Interface - EF06006")
		Else
			
			//fun��o para analise dados
			LoadTRB(@aDados)
		
			If Len(aDados) == 0
				Conout("N�o existem registros para serem processados na tabela - EF06006")
			Else
				//------------------------------------------
				// Consome WS CLient do ApData				//
				//------------------------------------------
				WsApData(@aDados)				
			Endif						
		Endif				
			
		//------------------------------------------
		// Fecha Conexao com o Banco de Integracao //
		//------------------------------------------
		cMsgError := ""
		U_F06001C2(@cMsgError)
			
		If lMontaAmb
			RpcClearEnv() //Desconecta ambiente 						
		Endif		
		
		Conout('F0600603 - Final Thread: ' + cValToChar(ThreadID()) )
		UnLockByName("F0600603_"+cFilIni,.F.,.F. )			 				
	EndIf

Return NIL

//--------------------------------------------------------------------------
/*{Protheus.doc} LoadTRB
Seleciona os registros da tabela de Interface

@author     Eduardo Fernandes
@since      19/12/2016

@Param		 aDados, ARRAY
@return     NIL
@version    P12.1.7
@project    MAN0000007423040_EF_006
*/
//--------------------------------------------------------------------------
Static Function LoadTRB(aDados)
Local nError	  := 0
Local cError	  := ""
Local cQuery    := ""
Local cCommand  := ""
Local cNewAlias := GetNextAlias()
Local cMsgError := ""
Local cOwner	:= GetMV('FS_OWNER',,'')

//------------------------------------------
// Atualiza STATUS da tabela INTERFACE    //
//------------------------------------------
cCommand :=  " UPDATE "+cOwner+"EF06006 SET EF06006_STATUS = '2' WHERE EF06006_STATUS = '1' "
//cCommand +=  " COMMIT "
If !U_F06001C5(cCommand, @cMsgError)
 	Conout(cMsgError)
	Return		
Endif		 

//------------------------------------------
// Consulta Registros A PROCESSAR 			//
//------------------------------------------
cQuery := " SELECT EF06006_FILIAL, EF06006_MATRICULA, EF06006_DTATEND, EF06006_MOTIVO, EF06006_OPERACAO, EF06006_ID "
cQuery += " FROM EF06006 " 
cQuery += " WHERE EF06006_STATUS = '2' "
cQuery += " ORDER BY 1,2,3,4 "

If U_F06001C6(cQuery, cNewAlias, @cMsgError)
	If Select(cNewAlias) > 0
		//Carrega vetor com os registros selecionados
		(cNewAlias)->(dbEval({|| AAdd(aDados,{EF06006_FILIAL, EF06006_MATRICULA, EF06006_DTATEND, EF06006_MOTIVO,; 
												   EF06006_OPERACAO, EF06006_ID, "3", "" })},, {|| !EOF()}))

		//Fecha a area
		(cNewAlias)->(dbCloseArea())
	Endif	
Endif	

Return Nil	
	
//-----------------------------------------------------------------------
/*/{Protheus.doc} WsApData
Funcao para consumir WS ApData
 
@author Eduardo Fernandes 
@since  20/12/2016
@return Nil  

@project MAN0000007423040_EF_006
@cliente Rededor
@version P12.1.7
             
/*/
//-----------------------------------------------------------------------
Static Function WsApData(aDados)
	Local nX, nY		:= 0
	Local aArquivo	:= {}
	Local aMethod		:= {}
	Local cFileWsdl	:= ""
	Local cMetodo		:= ""
	
	Local cRet  		:= ""
	Local cMsg			:= ""
	Local cURL			:= GetMV("FS_URLAPDT",,"http://10.25.134.11:7800/conectador/InformarRescisaoProtheus12SA?WSDL")  
	Local cWSDL		:= "\wsdl\informarrescisaosn.wsdl"
	Local lRet			:= .T.
		
	Local oWSDL		:= Nil
	Local oXML			:= Nil
	
	//��������������������������������Ŀ
	//� Tratamento para ambiente Linux �
	//����������������������������������
	If IsSrvUnix() .And. GetRemoteType() == 1
		cWSDL := StrTran(cWSDL,"\","/")
	Endif	
	
	oWSDL	:= TWSDLManager():New()
	oXML 	:= TXMLManager():New()
	//oWSDL:SetProxy( "10.171.0.62" , 8080 ) //Configura Proxy
		
	//If !oWsdl:ParseFile( cWSDL )	
	If !oWSDL:ParseURL(cURL)	
		cMsg := "[" + FwTimeStamp(2) + "] - Arquivo WSDL informado invalido"
		cMsg += CRLF + oWsdl:cError
		lRet := .F.		
	EndIf
	
	If lRet			
		aMethod := oWSDL:ListOperations()�
		
		Conout("[" + FwTimeStamp(2) + "] - InformarRescisaoSN: procurando m�todo!")
		If AScan(aMethod, {|x| x[1] $ "Incluir/Excluir"}) == 0
			cMsg := "[" + FwTimeStamp(2) + "] - Incluir/Excluir: M�todos n�o encontrado!"			
			Conout(cMsg)
			lRet := .F.
		EndIf
	EndIf
		
	If lRet
		For nX:=1 to Len(aDados)
			Conout("[" + FwTimeStamp(2) + "] - InformarRescisaoSN: definindo m�todo a ser executado!")
		
			//Selecionando Metodo
			cMetodo := IIF(aDados[nX,05] == "UPSERT", "Incluir", "Excluir")
			If !oWSDL:SetOperation(cMetodo)
				cMsg := "[" + FwTimeStamp(2) + "] - "+cMetodo+": N�o foi poss�vel estabelecer a chamada do m�todo!"
				Conout(cMsg)
				lRet := .F.
				Exit
			EndIf
		
			//Set dos parametros
			oWSDL:SetValue(0, aDados[nX,EF_MATRIC])	//Matricula
			oWSDL:SetValue(1, aDados[nX,EF_FILIAL])	//Filial
			oWSDL:SetValue(2, aDados[nX,EF_DTDEMI])	//Dt. Desligamento
			oWSDL:SetValue(3, AllTrim(aDados[nX,EF_MOTIVO]))	//Motivo
			cMsg := oWSDL:GetSoapMsg()
		
			If oWSDL:SendSoapMsg()
				cMsg := "[" + FwTimeStamp(2) + "] - "+cMetodo+": erro ao enviar requisi��o ao servidor!"
				Conout(cMsg)
				lRet := .F.
				Exit				
			EndIf
	
			If lRet
				//Retorno WS
				cRet := oWSDL:GetSoapResponse()
		
				If !oXML:Parse(cRet) .AND. !(Empty(oXml:Error()))					
					cMsg := "[" + FwTimeStamp(2) + "] - "+cMetodo+": Erro ao tentar parsear XML!"
					Conout(cMsg)
					lRet := .F.
					Loop					
				EndIf
			EndIf
	
			If lRet
				For nY := 1 To 4 //"/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:IncluirResponse ou Excluir:Response
					If !oXML:DOMHasChildNode()
						Exit
					EndIf
					oXML:DOMChildNode()
				Next nY

				If !("/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:"+cMetodo+"Response" $ oXML:cPath)
					cMsg := "[" + FwTimeStamp(2) + "] - "+cMetodo+": Estrutura de XML n�o reconhecida!"
					Conout(cMsg)
					lRet := .F.
					Exit					
				EndIf
				
				//-------------------------------------------------------------
				// Nao sabemos como sera o Retorno do WS sem WSDL publicado, //
				// portanto o trecho abaixo sera executado se lRet == TRUE,	 //
				// para atualizar a tabela Interface. 							 //
				//-------------------------------------------------------------
				If lRet
					UpdInter(aDados[nX])
				Endif					
			EndIf						
		Next nX
	Endif		

	//Retorno
	FreeObj(oWSDL)
	FreeObj(oXML)

Return lRet
				
//--------------------------------------------------------------------------
/*{Protheus.doc} UpdInter
Atualiza STATUS final da tabela INTERFACE 

@author     Eduardo Fernandes
@since      20/12/2016

@Param		 aDados, ARRAY
@return     lRet, BOOLEAN
@version    P12.1.7
@project    MAN0000007423040_EF_004
*/
//--------------------------------------------------------------------------
Static Function UpdInter(aLinha)
Local lRet 	  := .T.
Local cCommand  := ""
Local cMsgError := ""
Local cOwner	  := GetMV('FS_OWNER',,'')

//------------------------------------------
// Atualiza STATUS da tabela INTERFACE    //
//------------------------------------------
cCommand :=  " UPDATE "+cOwner+"EF06006 SET EF06006_STATUS = '"+ aLinha[EF_STATUS]+ "', "
If !Empty(aLinha[EF_OBSERVA])
	cCommand +=  " EF06006_OBSERVA = '"+ aLinha[EF_OBSERVA]+ "', "
Endif	
cCommand +=  " EF06006_DTPROCESS = '"+ DtoS(dDatabase)+ "', "
cCommand +=  " EF06006_HRPROCESS = '"+ Time()+ "' "
cCommand +=  " WHERE EF06006_ID = '"+ AllTrim(aLinha[EF_ID])+ "' " 
cCommand +=  "   AND EF06006_STATUS = '2' "
//cCommand +=  " COMMIT "

If !U_F06001C5(cCommand, @cMsgError)
 	Conout(cMsgError)
 	lRet := .F.
Endif		
	
Return lRet

//--------------------------------------------------------------------------
/*{Protheus.doc} TsJ00606
Testa JOB iniciando pelo TDS
@author     Eduardo Fernandes
@since      20/12/2016
@return     NIL
@version    P12.1.7
@project    MAN0000007423040_EF_006
*/
//--------------------------------------------------------------------------
User Function TsJ00606()
	Local aVet := {{'01','01010002'}}
	U_F0600603(aVet)
Return
