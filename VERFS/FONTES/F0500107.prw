#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
/*
{Protheus.doc} F0500107()
Atualiza��o da tabela RCB
@Author     Roberto Souza
@Since      08/11/2016
@Version    P12.7
@Project    MAN0000007423039_EF_00101
@Return
*/
User Function F0500107()
	
	Local nFil
	Local nTamFil    := Len(cFilAnt)
	Local nTamFilRCB := Len(AllTrim(xFilial("RCB")))
	Local aTodasFil  := {}
	Local aFilRCB    := {xFilial("RCB")}
	Local cFilRCB    := cFilAnt

	aTodasFil := FWAllFilial(cEmpAnt,,,.F.)
	For nFil := 1 To Len(aTodasFil)
		cFilRCB := PadR(Left(aTodasFil[nFil],nTamFilRCB),nTamFil)
		If ( AScan(aFilRCB,{|x| x == cFilRCB }) == 0 )
			AAdd(aFilRCB,cFilRCB)
		EndIf
	Next

	For nFil := 1 To Len(aFilRCB)
		RCB->(DbSetOrder(1))
		
	
		If !RCB->(DbSeek(IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil]) + "U005"))
		
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U005'
			RCB->RCB_DESC   := 'TP RESCIS�ES VS. VIS�ES'
			RCB->RCB_ORDEM  := '01'
			RCB->RCB_CAMPOS := 'XCOD_RES'
			RCB->RCB_DESCPO := 'C�digo Rescis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 2// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_PADRAO := 'FS43BR'
			RCB->RCB_PESQ   := '1'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U005'
			RCB->RCB_DESC   := 'TP RESCIS�ES VS. VIS�ES'
			RCB->RCB_ORDEM  := '02'
			RCB->RCB_CAMPOS := 'XDES_RES'
			RCB->RCB_DESCPO := 'Descri��o Rescis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 30// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_PADRAO := ''
			RCB->RCB_PESQ   := '2'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U005'
			RCB->RCB_DESC   := 'TP RESCIS�ES VS. VIS�ES'
			RCB->RCB_ORDEM  := '03'
			RCB->RCB_CAMPOS := 'XCOD_VIS'
			RCB->RCB_DESCPO := 'C�digo Vis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 6// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_VALID  := 'EXISTCPO("RDK")'
			RCB->RCB_PADRAO := 'RDKORG'			
			RCB->RCB_PESQ   := '2'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U005'
			RCB->RCB_DESC   := 'TP RESCIS�ES VS. VIS�ES'
			RCB->RCB_ORDEM  := '04'
			RCB->RCB_CAMPOS := 'XDES_VIS'
			RCB->RCB_DESCPO := 'Descri��o Vis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 30// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_PESQ   := '2'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())
						
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U005'
			RCB->RCB_DESC   := 'TP RESCIS�ES VS. VIS�ES'
			RCB->RCB_ORDEM  := '05'
			RCB->RCB_CAMPOS := 'XTIP_RES'
			RCB->RCB_DESCPO := 'Tipo Rescis�o'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 2// N
			RCB->RCB_DECIMA := 0// N
			RCB->RCB_PICTUR := '@!'
			RCB->RCB_PADRAO := 'FSU006'			
			RCB->RCB_PESQ   := '2'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '1'
			RCB->(MsUnlock())

			
			
		EndIf
	Next
	
	// Altera��es
	DbSelectArea("RCB")
	RCB->(DbSetOrder(3))
		
	For nFil := 1 To Len(aFilRCB)
		If !RCB->(DbSeek(IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil]) + Padr("XCOD_VIS",10)+"U005"))
			RecLock("RCB", .F.)
			RCB->RCB_VALID  := 'EXISTCPO("RDK")'                                                                                                         
			RCB->(MsUnlock())
		EndIf
	Next
	
Return

