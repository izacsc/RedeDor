#include 'protheus.ch'
#include 'apwebsrv.ch'

/*/{Protheus.doc} F0700601
Fun��o para Excluir o Local de Estoque
@type function
@author queizy.nascimento
@since 26/01/2017
@version 1.0
@param LocalEstoqueID, ${param_type}, (Descri��o do par�metro)
@Project MAN0000007423041_EF_006
/*/
User Function F0700601(LocalEstoqueID)
	Local cRetorno := ""
	Local aCampos  := {}
	Local cFil:= ""

	aCampos := {;
		{"NNR_FILIAL", LocalEstoqueID:cFil},;
		{"NNR_CODIGO"   , LocalEstoqueID:cCodigo}}
	
    
	cChave := u_RetChave("NNR", aCampos,{1,2})
	cRetorno  := u_DelMVCRg ("NNR",cChave,"AGRA045")
    

	aCampos := ASize(aCampos,0)
	aCampos := Nil

Return cRetorno