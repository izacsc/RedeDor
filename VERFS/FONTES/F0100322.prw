#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100322()
Realiza o processo de aprovação ou reprovação da solicitação. 
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, página html
*/
User Function F0100322()

	Local cHtml   := ""
	Local oSolic  := Nil
	
	Private cMsg
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	   	oOrg := WSORGSTRUCTURE():New()
		WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
		
		oSolic := WSW0500308():New()
		WsChgURL(@oSolic,"W0500308.apw")
		
		oDepto := WSW0500308():New()
		WsChgURL(@oDepto,"W0500308.apw")
		
		cFililS  := httpGet->cFilil
		cCodSol  := HttpGet->cSolic
		cVisPad  := HttpSession->aInfRotina:cVisao
		cConfirm := httpget->cAceita
		cLugar   := HttpPost->cLugar
		
		If oSolic:InfSolVg(cFililS,cCodSol)
		
			oOrg:cParticipantID  := ""
			oOrg:cVision         := oSolic:oWSInfSolVgResult:cRH3VISAO
			oOrg:cEmployeeFil    := oSolic:oWSInfSolVgResult:cRH3FILINI
			oOrg:cRegistration   := oSolic:oWSInfSolVgResult:cRH3MATINI
			oOrg:cEmployeeSolFil := oSolic:oWSInfSolVgResult:cRH3FILAPR
			oOrg:cRegistSolic    := oSolic:oWSInfSolVgResult:cRH3MATAPR
		
		EndIf
		
		If oOrg:GetStructure()
		
			cFilSol  := oOrg:cEmployeeSolFil
			cMatSol  := oOrg:cRegistSolic
			cVOrg    := HttpSession->aInfRotina:cVisao			
			cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
			
			oDepto:GetDepto(oSolic:oWSInfSolVgResult:cRH3FILAPR,oSolic:oWSInfSolVgResult:cRH3MATAPR)
			
			cDepto   := oDepto:cGetDeptoRESULT
			 
			oSolic:VerAprvSit(cFilSol,cMatSol,cDepto,cVOrg,cEmpFunc,"1")
			
			oSolic:oWSSolicitQdr:cFilialQdr := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitQdr:cCodMatric := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitQdr:cFilSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			oSolic:oWSSolicitQdr:cMatSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			oSolic:oWSSolicitQdr:cVisaoOrg  := cVOrg
			oSolic:oWSSolicitQdr:cFilAprov  := oSolic:oWSVERAPRVSITRESULT:cFilAprov
			oSolic:oWSSolicitQdr:cMatAprov  := oSolic:oWSVERAPRVSITRESULT:cMatAprov
			oSolic:oWSSolicitQdr:nNvlAprov  := oSolic:oWSVERAPRVSITRESULT:nNvlAprov
			oSolic:oWSSolicitQdr:cEmpAprov  := oSolic:oWSVERAPRVSITRESULT:cEmpAprov
			oSolic:oWSSolicitQdr:cEmpresa   := GetEmpFun()
			oSolic:oWSSolicitQdr:cCodSolic  := cCodSol
		
			If cConfirm == "1" //Aprovar
				oSolic:AprSolQdro()
				cMsg := "Solicitação aprovada!"
			ElseIf cConfirm == "2" //Reprovar
				oSolic:RepSolQdro()
				cMsg := "Solicitação reprovada!"
			EndIf
	
		EndIf
		
		cHtml := ExecInPage("F0500305")

	WEB EXTENDED END

Return cHtml