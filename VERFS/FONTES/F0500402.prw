#Include 'Protheus.ch'
Static lTransf	:= .F.
Static lOutros	:= .F.
Static lTurno 	:= .F.
Static aTpsSol	:= {Nil,Nil,Nil,Nil}
Static cFilNova	:= cFilAnt
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} F0500402
Efetua a atualiza��o quando solicita��o aprovada
@type User Function
@author Cris
@since 04/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${lVlDt}, ${.T. efetivado com sucesso  .F. n�o efetivado}
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500402(oModel)
	
	Local cFilSol	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_FILIAL")
	Local cNumSol	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_CODIGO")
	Local dDtaSol	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_DTSOLI")
	Local cMatFun	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_MAT")
	Local cVisaoA	:= oModel:GetModel("RH3MASTER"):GetValue("RH3_VISAO")
	Local aCpos		:= {}
	Local cTipoAtu	:= ''
	Local lVlDt		:= .T.
	Local cBlqSlTF	:= ''
	
	dbSelectArea("RH4")
	RH4->(dbSetORder(1))
	if RH4->(dbSeek(cFilSol+cNumSol))
		
		While cFilSol+cNumSol == RH4->(RH4_FILIAL+RH4_CODIGO)
			
			if 'TMP_TIPO' $ RH4->RH4_CAMPO .AND. cTipoAtu <> RH4->RH4_VALNOV
				
				if !Empty(cTipoAtu)
					
					AcuSoli(Alltrim(cTipoAtu),@aCpos)
				EndIf
				
				cTipoAtu	:= RH4->RH4_VALNOV
			EndIf
			
			if 'RA_' $ RH4->RH4_CAMPO
				
				aAdd(aCpos,{RH4->RH4_CAMPO, RH4->RH4_VALNOV})
			EndIf
			
			RH4->(dbSkip())
		EndDo
		
		//Acumula os tipos de solicita��es nas posi��es dentro do array(aTpsSol) limitado
		AcuSoli(Alltrim(cTipoAtu),@aCpos)
		
		//Validar data da aprova��o
		if  lVlDt	:= (VlDtAprov(lVlDt,dDtaSol) .AND. VldTransf( aTpsSol[1] ) .AND. Valposto())
			
			if lTransf .AND. (lVlDt	:= MsgNoYes( "Confirma a Transfer�ncia?" + CRLF + CRLF + "ATEN��O: Ap�s a confirma��o N�O SER� POSS�VEL DESFAZER ESSA OPERA��O!" ))
				
				FWMsgRun(,{|| lVlDt		:= AtTransf(oModel,aTpsSol[1],cFilSol,cMatFun) },"Processando...","Atualizando para a transfer�ncia. Aguarde!" )
			EndIf
			
			if 	lVlDt .AND. lOutros
				
				if aTpsSol[2] <> Nil .AND. len(aTpsSol[2]) > 0
					
					FWMsgRun(,{|| lVlDt	:= ATTurHor(aTpsSol[2],cFilSol,cMatFun) },"Processando...","Atualizando Carga Hor�ria/Turno de trabalho. Aguarde!" )
				EndIf
				
				if lVlDt .AND. aTpsSol[3] <> Nil .AND. len(aTpsSol[3]) > 0
					
					FWMsgRun(,{|| lVlDt	:= AtCargo(aTpsSol[3],cFilSol,cMatFun) },"Processando...","Atualizando Cargo. Aguarde!" )
				EndIf
				
				if lVlDt .AND. aTpsSol[4] <> Nil .AND. len(aTpsSol[4]) > 0 .AND. lTransf .AND. Alltrim(GetMV('FS_BLQSLTF')) == 'S'
					
					lVlDt	:= .F.
				EndIf
				
				if lVlDt .AND. aTpsSol[4] <> Nil .AND. len(aTpsSol[4]) > 0
					
					FWMsgRun(,{|| lVlDt	:= AtSalari(aTpsSol[4],cFilSol,cMatFun) },"Processando...","Atualizando Sal�rio. Aguarde!" )
				EndIf
			EndIf
		Else
			
			lVlDt	:= .F.
		EndIf
	EndIf
	
	if lVlDt
		
		//Libera cadastro de funcion�rio para nova inclus�o de solicita��o
		U_F0500407(cFilSol,cMatFun,cNumSol,cVisaoA)
		
		//Monta e-mail para chamar rotina de envio e-mail para todos
		//U_F0500409(cFilSol,cMatFun,cNumSol,cVisaoA,'1')
		
		Aviso('SUCESSO - EFETIVA��O',"A solicita��o foi efetuada com sucesso. ",{'OK'},1)
		
		//Requisito N005 -  Indicadores: -067-Efetivada
		U_F0500201(cFilSol,cNumSol,'067')
	Else
		
		Aviso('N�O EFETIVADA - EFETIVA��O',"A solicita��o n�o foi efetuada. ",{'OK'},1)
		//Monta e-mail para chamar rotina de envio e-mail para todos
	EndIf
	
	lTransf		:= .F.
	lOutros		:= .F.
	aTpsSol		:= {Nil,Nil,Nil,Nil}
	cFilNova	:= cFilAnt
	
Return lVlDt


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AcuSoli
(long_description)
@type function
@author Cris
@since 09/11/2016
@param cTipoAtu, character, (Descri��o do par�metro)
@param aCpos, array, (Descri��o do par�metro)
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AcuSoli(cTipoAtu,aCpos)
	
	//Se for transferencia, chama rotina de transfer�ncia
	if cTipoAtu == '5'
		
		aTpsSol[1]	:= aCpos
		aCpos	:= {}
		lTransf	:= .T.
		
	Elseif cTipoAtu $ '3'//Os dados da troca de turno(4) e da carga hor�ria(3) ser�o atualizados em conjunto
		
		aTpsSol[2]	:= aCpos
		aCpos	:= {}
		lOutros	:= .T.
		lTurno	:= .T.
		
	Elseif cTipoAtu == '2'
		
		aTpsSol[3]	:= aCpos
		aCpos	:= {}
		lOutros	:= .T.
		
	Elseif cTipoAtu == '1'
		
		aTpsSol[4]	:= aCpos
		aCpos	:= {}
		lOutros	:= .T.
	EndIf
Return


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} VlDtAprov
(long_description)
@type function
@author Cris
@since 09/11/2016
@version 1.0
@param dDtaSol, data, (Descri��o do par�metro)
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function VlDtAprov(lVlDt,dDtaSol)
	
	Local lContin	:= .T.
	Local cDiaSol	:= Day(dDtaSol)
	Local cMesSol	:= Month(dDtaSol)
	Local cDiaAtu	:= Day(dDataBase)
	Local cMesAtu	:= Month(dDataBase)
	Local dDtTur	:= Ctod('16/'+Strzero(Month(dDataBase),2)+"/"+Str(Year(dDataBase)))
	
	//primeiro dia do m�s
	//data da transfer�ncia
	if cDiaSol <= 20
		
		if (cMesSol < cMesAtu) .OR.  (year(dDtaSol) < year(dDataBase))
			
			lContin	:= .T.
		Else
			
			Help("",1, "Help", "Valida��o de Data de Aprova��o(F0500402_04)", "M�s base menor que m�s da data da solicita��o. Efetiva��o n�o ser� realizada!" , 3, 0)
			lVlDt	:= .F.
		EndIf
		
	Elseif (cMesSol < cMesAtu .AND. cMesSol < cMesSol+2) .OR. (year(dDtaSol) < year(dDataBase) .AND. ((cMesSol == 12 .AND. cMesAtu >= 2) .OR. (cMesSol < 12 .AND. cMesAtu >= 1)))
		
		lContin	:= .T.
	Else
		
		Help("",1, "Help", "Valida��o de Data de Aprova��o(F0500402_05)", "Dia/M�s n�o permitido para efetiva��o para este tipo de solicita��o aprovada!" , 3, 0)
		lVlDt	:= .F.
	EndIf
	
	if lContin
		
		//Caso exista Troca de Turno
		if lTurno .AND. !lOutros .AND. cDiaAtu <> Day(dDtTur)
			
			Help("",1, "Help", "Valida��o de Data de Aprova��o(F0500402_03)", "A efetiva��o para a Troca de Turno somente poder� ocorrer dia 16 ou o 1 dia m�s posterior a data da solicita��o!" , 3, 0)
			lVlDt	:= .F.
			
		Elseif lOutros .AND. cDiaAtu <> Day(dDtTur)
			
			Help("",1, "Help", "Valida��o de Data de Aprova��o(F0500402_02)", "As efetiva��es para este tipo de solicita��o devem ser executadas somente no dia 16 do m�s posterior a data da solicita��o!" , 3, 0)
			lVlDt	:= .F.
		EndIf
	EndIf
	
Return lVlDt


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtSalari
(long_description)
@type function
@author Cris
@since 04/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AtSalari(aCpos,cFilFun,cMatFun)
	
	Local nSalAnt	:= 0
	Local aAreaSRA	:= SRA->(GetArea())
	Local lAtuSal	:= .F.
	
	nSalAnt	:= SRA->RA_SALARIO
	
	SRA->(dbSetOrder(1))
	if SRA->(dbSeek(cFilFun+cMatFun))
		SRA->(RecLock("SRA",.F.))
		SRA->RA_SALARIO	:=  Val(StrTran(StrTran(Alltrim(aCpos[1][2]),".",""),",","."))
		SRA->RA_ANTEAUM	:=  Val(StrTran(StrTran(Alltrim(aCpos[1][2]),".",""),",","."))//Conforme EF mantendo os dois iguais, para manter a integridade do calculo de Dissidio Retroativo.
		SRA->(MsUnlock())
		
		if SRA->RA_SALARIO == nSalAnt
			
			Help("",1, "Help", "Atualiza��o Salarial(AtSalari_01)", "N�o foi poss�vel efetuar a atualiza��o salarial. Efetue manualmente!" , 3, 0)
			lAtuSal	:= .F.
		Else
			
			GrvSR3R7(aCpos,cFilFun,cMatFun)
			lAtuSal	:= .T.
		EndIf
	EndIf
	
	RestArea(aAreaSRA)
	
Return lAtuSal


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtCargo
(long_description)
@type function
@author Cris
@since 04/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AtCargo(aCpos,cFilFun,cMatFun)
	
	Local aAreaSRA		:= SRA->(GetArea())
	Local lAltCarg		:= .F.
	
	//Caso tenha ocorrido transfer�ncia
	if cFilNova <> cFilAnt
		
		cFilFun	:= cFilNova
		
	EndIf
	
	dbSelectArea("SRA")
	SRA->(dbSetOrder(1))
	if SRA->(dbSeek(cFilFun+cMatFun))
		
		SRA->(RecLock("SRA",.F.))
		SRA->RA_CARGO	:= Alltrim(aCpos[1][2])
		SRA->RA_CODFUNC	:= Alltrim(aCpos[1][2])
		SRA->(MsUnlock())
		
		//Certifico que atualizou o cargo
		if (lAltCarg := (Alltrim(aCpos[1][2]) == SRA->RA_CARGO .AND. Alltrim(aCpos[1][2]) == SRA->RA_CODFUNC ))
			//Quando existir em �nica solicita��o a altera��o de Cargo com Sal�rio,�nico log nas tabelas SR3 e SR7 dever� ser gerado,
			//portanto grava-se o cargo e  depois o sal�rio.
			if aTpsSol[4] == Nil
				
				//Tabelas de hist�ricos de altera��o salarial e cargo
				GrvSR3R7(aCpos,cFilFun,cMatFun)
				
				//tabelas de hist�rico de altera��o de campo
				GrvSR9(cFilFun,cMatFun,Msdate(),'RA_CARGO',Alltrim(aCpos[1][2]))
			EndIf
			
			lAltCarg	:= .T.
		EndIf
	Else
		
		Help("",1, "Help", "INSUCESSO - Cargo(AtCargo_01)", "N�o foi localizado o funcion�rio "+cMatFun+" na filial de destino "+cFilFun+". Os campos cargo/fun��o n�o foram atualizados!" , 3, 0)
		lAltCarg	:= .F.
	EndIf
	
	RestArea(aAreaSRA)
	
Return lAltCarg


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GrvSR3R7
(long_description)
@type function
@author Cris
@since 09/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function GrvSR3R7(aCpos,cFilFun,cMatFun)
	
	Local cSeq		:= ''
	Local dDtAtu	:= MsDate()
	
	dbSelectArea("SR3")
	SR3->(dbSetOrder(1))
	if SR3->(dbSeek(cFilFun+cMatFun+Dtos(dDtAtu)+Alltrim(aCpos[2][2])))
		
		While cFilFun+cMatFun+Dtos(dDtAtu)+Alltrim(aCpos[2][2]) ==  SR3->(R3_FILIAL+R3_MAT+Dtos(R3_DATA)+R3_TIPO)
			
			cSeq	:= SR3->R3_SEQ
			
			SR3->(dbSkip())
		EndDo
		
	EndIf
	
	SR3->(RecLock("SR3",.T.))
	SR3->R3_FILIAL   := cFilFun
	SR3->R3_MAT      := cMatFun
	SR3->R3_DATA     := dDtAtu
	SR3->R3_SEQ		 := StrZero(Val(cSeq)+1,1)
	SR3->R3_PD       := "000"
	SR3->R3_DESCPD   := "SALARIO BASE"
	SR3->R3_VALOR    := SRA->RA_SALARIO
	SR3->R3_TIPO     := Alltrim(aCpos[2][2])
	SR3->R3_ANTEAUM	:= SRA->RA_SALARIO
	SR3->( MsUnLock() )
	
	if IsInCallStack("AtSalari")
		
		//Chama rotina de integra��o quando altera��o salarial
		U_F0600901("F0600301",; // cFunc
		SR3->(RECNO()),; // nRecno
		"SR3",; // cAliasTrb
		SR3->R3_FILIAL + SR3->R3_MAT + DTOS(SR3->R3_DATA) + SR3->R3_TIPO,; // cChave
		"",; // cObs
		CTOD(""),; // Data de envio
		"UPSERT") // Operacao
	EndIf
	
	dbSelectArea("SR7")
	SR7->(dbSetOrder(1))
	if SR7->(dbSeek(cFilFun+cMatFun+Dtos(dDtAtu)+Alltrim(aCpos[2][2])))
		
		While cFilFun+cMatFun+Dtos(dDtAtu)+Alltrim(aCpos[2][2]) ==  SR7->(R7_FILIAL+R7_MAT+Dtos(R7_DATA)+R7_TIPO)
			
			cSeq	:= SR7->R7_SEQ
			
			SR7->(dbSkip())
		EndDo
		
	EndIf
	
	dbSelectArea("SR7")
	IF SR7->(RecLock("SR7",.T.))
		SR7->R7_FILIAL   := cFilFun
		SR7->R7_MAT      := cMatFun
		SR7->R7_DATA     := MsDate()
		SR7->R7_TIPO     := Alltrim(aCpos[2][2])
		SR7->R7_FUNCAO   := SRA->RA_CODFUNC
		SR7->R7_DESCFUN  := POSICIONE("SRJ",1,xFilial("SRJ")+SRA->RA_CODFUNC,"RJ_DESC")
		SR7->R7_TIPOPGT  := SRA->RA_TIPOPGT
		SR7->R7_CATFUNC  := SRA->RA_CATFUNC
		SR7->R7_USUARIO  := Alltrim(cUsername)+'|'+Alltrim(LogUserName())
		If SR7->( Type("R7_CARGO") ) # "U"
			SR7->R7_CARGO   := SRA->RA_CARGO
		EndIf
		If SR7->( Type("R7_DESCCAR") ) # "U"
			SR7->R7_DESCCAR	:= POSICIONE("SQ3",1,xFilial("SQ3")+SRA->RA_CARGO,"Q3_DESCSUM")
		EndIf
		SR7->R7_SEQ		:= StrZero(Val(cSeq)+1,1)
		
		SR7->( MsUnLock() )
	EndIF
	
Return


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc}
(long_description)
@type function
@author Cris
@since 04/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function  ATTurHor(aCpos,cFilFun,cMatFun)
	
	Local aAreaSRA	:= SRA->(GetArea())
	Local aAreaSPF	:= {}
	Local cTurnAnt	:= ''
	Local cTSeqAnt	:= ''
	Local cRegrAnt	:= ''
	Local iCpoSRA	:= 0
	Local lAtuTurn	:= .T.
	Local nPosCpo	:= 0
	//Caso tenha ocorrido transfer�ncia
	if cFilNova <> cFilAnt
		
		cFilFun	:= cFilNova
	EndIf
	
	//Garanto que  realmente esta posicionado no funcionario relacionado a solicita��o
	dbSelectArea("SRA")
	SRA->(dbSetOrder(1))
	if SRA->(dbSeek(cFilFun+cMatFun))
		
		cTurnAnt	:= SRA->RA_TNOTRAB
		cTSeqAnt	:= SRA->RA_SEQTURN
		cRegrAnt	:= SRA->RA_REGRA
		
		SRA->(RecLock("SRA",.F.))
		
		For iCpoSRA	:= 1 to len(aCpos)
			
			if Posicione("SX3", 2, Alltrim(aCpos[iCpoSRA][1]), "X3_TIPO") == "N"
				&(aCpos[iCpoSRA][1])	:= Val(aCpos[iCpoSRA][2])
			Else
				&(aCpos[iCpoSRA][1])	:= aCpos[iCpoSRA][2]
			EndIf
			
		Next
		
		SRA->(MsUnlock())
		
		aAreaSPF	:= SPF->(GetArea())
		
		dbSelectArea("SPF")//PONA160.PRW
		SPF->(dbSetOrder(1))
		If SPF->( RecLock( "SPF" , !SPF->(dbSeek(cFilFun+cMatFun) ) ))
			
			SPF->PF_FILIAL	:= cFilFun
			SPF->PF_MAT		:= cMatFun
			SPF->PF_DATA	:= dDataBase
			SPF->PF_TURNODE	:= cTurnAnt
			SPF->PF_SEQUEDE := cTSeqAnt
			SPF->PF_REGRADE := cRegrAnt
			SPF->PF_TURNOPA	:= iif((nPosCpo := Ascan(aCpos,{|x,y| Alltrim(x[1]) == "RA_TNOTRAB"})) > 0, Alltrim(aCpos[nPosCpo][2]),cTurnAnt)
			SPF->PF_SEQUEPA := iif((nPosCpo := Ascan(aCpos,{|x,y| Alltrim(x[1]) == "RA_SEQTURN"})) > 0, Alltrim(aCpos[nPosCpo][2]),cTSeqAnt)
			SPF->PF_REGRAPA := iif((nPosCpo := Ascan(aCpos,{|x,y| Alltrim(x[1]) == "RA_REGRA"})) > 0, Alltrim(aCpos[nPosCpo][2]),cRegrAnt)
			SPF->(MsUnLock())
		EndIf
		
		RestArea(aAreaSPF)
		
		GrvSR9(cFilFun,cMatFun,MsDate(),'RA_HRSMES',Alltrim(aCpos[Ascan(aCpos,{|x,y| Alltrim(x[1]) == 'RA_HRSMES'})][2])	)
	EndIf
	
	RestArea(aAreaSRA)
	
Return lAtuTurn


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GrvSR9
(long_description)
@type function
@author Cris
@since 24/11/2016
@version 1.0
@param cFilFun, character, (Filial do funcion�rio)
@param cMatFun, character, (Matricula do Funcion�rio)
@param dDtReal, data, (Data real da altera��o)
@param cCpoAlt, character, (Campo alterado)
@param cCntAlt, character, (novo valor do campo alterado)
@return ${lGravou}, ${.T. gravou .F. n�o gravou}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function GrvSR9(cFilFun,cMatFun,dDtReal,cCpoAlt,cCntAlt)
	
	Local aAreaAtu	:= SR9->(GetArea())
	
	dbSelectArea("SR9")
	if SR9->(Reclock('SR9',.T.))
		SR9->R9_FILIAL	:= cFilFun
		SR9->R9_MAT		:= cMatFun
		SR9->R9_DATA	:= dDtReal
		SR9->R9_CAMPO 	:= cCpoAlt
		SR9->R9_DESC	:= cCntAlt
		SR9->(MsUnlock())
	EndIf
	
	RestArea(aAreaAtu)
Return


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} VldTransf
Valida os cadastros da transferencia na filial destino
@type function
@author Roberto Souza
@since 18/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function VldTransf( aCpos )
	Local lRet 	:= .T.
	Local nScan	:= 0
	Local nTamFil 	:= Len(cFilAnt)
	Local cMsgErro:= ""
	Local aAreaAtu	:= {}
	
	If lTransf //.And. lOutros
		nScan := aScan( aCpos, {|x|, Alltrim(x[01]) == "RA_FILIAL" })
		If nScan > 0
			cFilP := Padr(aCpos[nScan][02],nTamFil)
			cFilNova	:= cFilP
			// Verifica se mudou Depto - SQB
			If !Empty(AllTrim(xFilial("SQB")))
				nScan := aScan( aCpos, {|x|, Alltrim(x[01]) == "RA_DEPTO" })
				If nScan > 0
					cDeptoP := aCpos[nScan][02]
					
					aAreaAtu	:= SQB->(GetArea())
					
					//Verifica se o depto existe na filial destino
					DbSelectArea("SQB")
					SQB->(DbSetOrder(1))
					If !SQB->(DbSeek(cFilP+cDeptoP))
						cMsgErro +="Depto: "+AllTrim(cDeptoP) +" n�o existe na filial de destino: "+cFilP	+"."+CRLF
					EndIf
					
					RestArea(aAreaAtu)
				EndIf
			EndIf
			
			// Verifica se mudou CC - CTT
			If !Empty(AllTrim(xFilial("CTT")))
				nScan := aScan( aCpos, {|x|, Alltrim(x[01]) == "RA_CC" })
				If nScan > 0
					cCCP := aCpos[nScan][02]
					//Verifica se o CC existe na filial destino
					
					aAreaAtu	:= CTT->(GetArea())
					
					DbSelectArea("CTT")
					CTT->(DbSetOrder(1))
					If !CTT->(DbSeek(cFilP+cCCP))
						cMsgErro +="C. Custo: "+AllTrim(cCCP) +" n�o existe na filial de destino: "+cFilP	+"."+CRLF
					EndIf
					
					RestArea(aAreaAtu)
				EndIf
			EndIf
			
			// Verifica se mudou Processo - RCJ
			If !Empty(AllTrim(xFilial("RCJ")))
				nScan := aScan( aCpos, {|x|, Alltrim(x[01]) == "RA_PROCES" })
				If nScan > 0
					cProcP := aCpos[nScan][02]
					//Verifica se o Processo existe na filial destino
					
					aAreaAtu	:= RCJ->(GetArea())
					
					DbSelectArea("RCJ")
					RCJ->(DbSetOrder(1))
					If !RCJ->(DbSeek(cFilP+cProcP))
						cMsgErro +="Processo: "+AllTrim(cProcP) +" n�o existe na filial de destino: "+cFilP	+"."+CRLF
					EndIf
					
					RestArea(aAreaAtu)
				EndIf
			EndIf
			
			
			// Verifica se mudou Fun��o - SRJ
			If !Empty(AllTrim(xFilial("SRJ")))
				nScan := aScan( aCpos, {|x|, Alltrim(x[01]) == "RA_CODFUNC" })
				If nScan > 0
					cFuncP := aCpos[nScan][02]
					//Verifica se a fun��o existe na filial destino
					aAreaAtu	:= SRJ->(GetArea())
					
					DbSelectArea("SRJ")
					SRJ->(DbSetOrder(1))
					If !SRJ->(DbSeek(cFilP+cFuncP))
						cMsgErro +="Fun��o: "+AllTrim(cFuncP) +" n�o existe na filial de destino: "+cFilP	+"."+CRLF
					EndIf
					
					RestArea(aAreaAtu)
				EndIf
			EndIf
			/*
			RA_TNOTRAB	 SR6
			RA_SEQTURN SPJ
			RA_REGRA SPA
			*/
			If !Empty(cMsgErro)
				Aviso("Aten��o",cMsgErro,{"ok"},2)
				lRet := .F.
			EndIf
		EndIf
	EndIf
	
Return( lRet )


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtTransf
(long_description)
@type function
@author Cris
@since 04/11/2016
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AtTransf( oModel, aCpos, cFilFun, cMatFun )
	
	Local lRet    := .F.
	Private lLote	:= .F.
	
	DbSelectArea("SRA")
	SRA->(DbSetOrder(1))
	If SRA->(DbSeek(cFilFun+cMatFun))
		lRet := TCFA040Atende(oModel,"2")
	Else
		lRet := .F.
	EndIf
	
Return( lRet )



/*/{Protheus.doc} TCFA040Atende
Efetua a chamada de transferencia conforme o portal.
Obs . N�o alterar o nome da Static Funcion pois est� amarrado a IsCallStack dentro do padr�o
@type function
@author 		Roberto Souza
@since 			17/11/2016
@version 		P12.1.7
@Project 		MAN0000007423039_EF_004
@Param			oModel, objeto, Modelo de dados
@return 		${return}, ${n�o h�}
/*/
Static Function TCFA040Atende( oModel, cStatus )
	
	Local oModRH3   			:= oModel:GetModel("RH3MASTER") //oModel:GetModel("TCFA040_RH3")
	Local lRet				  	:= .F.
	Local nI 		 			:= 0
	
	Private cCadastro 			:= ""
	Private cTipSolicPortal		:= ""
	Private cFilFun    			:= ""
	Private cMatFun   			:= ""
	Private dFerDtIni		   	:= CToD("")
	Private dFerDtFim		   	:= CToD("")
	Private nFerDuracao			:= 0
	Private cCodSolic  			:= oModRH3:GetValue("RH3_CODIGO") // RH3->RH3_CODIGO
	Private aCPosPortal			:= {}
	Private bParamPortal		:= {|| .T.}
	Private lUseSPJ            	:= If(cPaisLoc == "BRA",.T., SuperGetMv("MV_USESPJ",NIL,"0")  == "1" )
	Private cRH3Cod				:= ''
	
	//variaveis usadas (RH3_TIPO = '2') na alteração cadastral eSocial.
	Private aCpoSRA := {}
	Private lMsErroAuto    		:= .F.
	Private lMsHelpAuto    		:= .T.
	Private lAutoErrNoFile 		:= .T.
	
	//variaveis usadas no GPEA050
	Private lIncSRA				:= .T.
	Private lInitDesc
	Private lHabAba 				:= .F.				//Variavel para habilitar a aba de Programação de férias
	Private cOrgCfg				:= SuperGetMv("MV_ORGCFG", NIL, "0" )	//-- Controlde de Postos : 0-não usa Sigaorg;1-Tem controle de postos; 2- não tem  controle de postos
	Private cGsPubl 				:= SuperGetMv("MV_GSPUBL",,"1")
	Private cContrMat				:= SuperGetMv( "MV_MATRICU", NIL, "0")
	Private lCtrAutoMat			:= .F.					//Checa se o controle Automatico de Matricula esta ativado e se havera transferencia de matricula
	
	If cGsPubl == "2" .And. GetMv("MV_VDFLOGO",,"0") <> "0"
		cGsPubl := "3"
	EndIf
	
	// **************************** Transferencia
	Private cCodTrasfPortal 	:=  oModRH3:GetValue("RH3_CODIGO") // RH3->RH3_CODIGO
	
	//-- Variaveis utilizadas pelo GPEA180
	Private cItemClVl       	:= SuperGetMv( "MV_ITMCLVL", .F., "2" )
	Private lItemClVl       	:= SuperGetMv( "MV_ITMCLVL", .F., "2" ) $ "13"
	Private aLogTransf			:= {}
	Private aTransfCols 		:= {}
	Private aTransf1Cols    	:= {}
	
	Private aNewIndexSRA  		:= {}
	Private bNewFiltroBrw 		:= {|| NIL }
	Private cMarkTransf			:= GetMark()
	Private cRaOkTransSpc		:= Space( TamSx3( "RA_OKTRANS" )[1] )
	Private lAbortPrint			:= .F.
	
	cCadastro               	:= OemToAnsi( "Transferˆncias" )  //"Transferˆncias"
	cTipSolicPortal         	:= "4"
	
	DbSelectArea( "SRA" )
	SRA->(DbSetOrder( RetOrder( "SRA", "RA_FILIAL + RA_MAT") ))
	
	If SRA->(DbSeek(RH3->RH3_FILIAL + RH3->RH3_MAT))
		
		BEGIN TRANSACTION
			
			RecLock("SRA",.F.)
			SRA->RA_OKTRANS := cMarkTransf
			SRA->(MsUnlock())
			
		END TRANSACTION
		
		RH4->(DbSeek(RH3->(RH3_FILIAL + RH3_CODIGO)))
		
		// Inicializa as variaveis com o destino do funcionário
		M->RE_EMPP 		:= cEmpAnt
		M->RE_FILIALP 	:= ""
		M->RE_MATP 		:= ""
		M->RE_DEPTOP 	:= ""
		M->RE_PROCESP 	:= ""
		M->RE_CCP       := ""
		M->RA_DESCCC	:= ""
		M->RE_POSTOP	:= ""
		
		While !RH4->(Eof()) .AND. RH4->(RH4_FILIAL + RH4_CODIGO) == RH3->(RH3_FILIAL + RH3_CODIGO)
			If AllTrim(RH4->RH4_CAMPO) == "RE_EMPP"
				M->RE_EMPP := Left(RH4->RH4_VALNOV, Len(SRE->RE_EMPP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_FILIALP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_FILIAL"
				M->RE_FILIALP := Left(RH4->RH4_VALNOV, Len(SRA->RA_FILIAL))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_MATP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_MAT"
				M->RE_MATP := Left(RH4->RH4_VALNOV, Len(SRE->RE_MATP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_DEPTOP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_DEPTO"
				M->RE_DEPTOP := Left(RH4->RH4_VALNOV, Len(SRE->RE_DEPTOP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_CCP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_CC"
				M->RE_CCP 		:= Left(RH4->RH4_VALNOV, Len(SRE->RE_CCP))
				M->RA_DESCCC	:= fDesc("CTT",Left(RH4->RH4_VALNOV, Len(SRA->RA_CC)),"CTT_DESC01",,)//SRA->RA_FILIAL))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_PROCESP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_PROCES"
				M->RE_PROCESP := Left(RH4->RH4_VALNOV, Len(SRE->RE_PROCESP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_POSTOP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_POSTO"
				M->RE_POSTOP := Left(RH4->RH4_VALNOV, Len(SRE->RE_POSTOP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_ITEMP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_ITEM"
				M->RE_ITEMP := Left(RH4->RH4_VALNOV, Len(SRE->RE_ITEMP))
			ElseIf AllTrim(RH4->RH4_CAMPO) == "RE_CLVLP" .Or. AllTrim(RH4->RH4_CAMPO) == "RA_CLVL"
				M->RE_CLVLP := Left(RH4->RH4_VALNOV, Len(SRE->RE_CLVLP))
			EndIf
			RH4->(DbSkip())
		EndDo
		
		lRet := ( Gpea180Mat("RH3",RH3->(recno()),2) == 1 )
	EndIf
	
Return( lRet )


//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ValPosto
Valida se existe disponibilidade no posto de destino
@type Static function
@author Cris
@since 23/11/2016
@version 		P12.1.7
@Project 		MAN0000007423039_EF_004
@return ${l�gico}, ${.T. v�lido  .F. n�o valido}
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function ValPosto()
	
	Local cDepto	:= ''
	Local cFilAt	:= ''
	Local cCCAt		:= ''
	Local cCodCarg	:= ''
	Local cQry		:= ''
	Local nQtdePost	:= 0
	Local nPosCpo	:= 0
	Local cTabAtu	:= ''
	Local lValPosto	:= .T.
	Local aAreaSRA	:= SRA->(GetArea())
	//Dados da Transfer�ncia
	//Lembrar que na transfer�ncia posso mudar filial, departamento,  centro de custo e/ou processo
	//Para o posto
	if aTpsSol[1] <> Nil
		
		//Busca a informa��o da filial de Destino
		if (nPosCpo := Ascan(aTpsSol[1],{|x| Alltrim(x[1]) == 'RA_FILIAL'})) > 0
			
			cFilAt	:= Alltrim(aTpsSol[1][1][nPosCpo+1])
		EndIf
		
		//Busca a informa��o do Centro de Custo de Destino
		if (nPosCpo := Ascan(aTpsSol[1],{|x| Alltrim(x[1]) == 'RA_CC'})) > 0
			
			cCCAt	:= Alltrim(aTpsSol[1][iif(len(aTpsSol[1])==1,1,2)][2])
		EndIf
		
		//Busca a informa��o do deparrtamento de Destino
		if (nPosCpo := Ascan(aTpsSol[1],{|x| Alltrim(x[1]) == 'RA_DEPTO'})) > 0
			
			cDepto	:= Alltrim(aTpsSol[1][iif(len(aTpsSol[1])==1,1,iif(len(aTpsSol[1])==2,2,3))][2])
		EndIf
	EndIf
	
	//Dados do cargo
	if aTpsSol[3] <> Nil
		
		if (nPosCpo := Ascan(aTpsSol[3],{|x| Alltrim(x[1]) == 'RA_CODFUNC'})) > 0
			
			cCodCarg	:= Alltrim(aTpsSol[3][1][2])
		EndIf
	EndIf
	
	if aTpsSol[1] <> Nil .OR. aTpsSol[3] <> Nil
		
		if Empty(cCodCarg) .OR. Empty(cFilAt) .OR. Empty(cDepto) .OR. Empty(cCCAt)
			
			dbSelectArea("SRA")
			SRA->(dbSetORder(1))
			if SRA->(dbSeek(RH3->RH3_FILIAL+RH3->RH3_MAT))
				
				cFilAt	:= iif(Empty(cFilAt),SRA->RA_FILIAL, cFilAt)
				cDepto	:= iif(Empty(cDepto),SRA->RA_DEPTO, cDepto)
				cCCAt	:= iif(Empty(cCCAt),SRA->RA_CC, cCCAt)
				cCodCarg:= iif(Empty(cCodCarg),SRA->RA_CARGO,cCodCarg)
			EndIf
		EndIf
		
		cTabAtu	:= GetNextAlias()
		
		cQry	:=  "	SELECT (RCL_NPOSTO-RCL_OPOSTO) AS QTDEDISPONIVEL "+CRLF
		cQry	+=  "		FROM "+RetSqlName("RCL")+" "+CRLF
		cQry	+=  "		WHERE RCL_FILIAL = '"+iif(!Empty(xFilial("RCL")),cFilAt,xFilial("RCL"))+"' "+CRLF
		cQry	+=  "		  AND RCL_DEPTO = '"+cDepto+"' "+CRLF
		cQry	+=  "		  AND RCL_CARGO IN ('','"+cCodCarg+"')"+CRLF
//		cQry	+=  "		  AND RCL_CC	= '"+cCCAt+"' "+CRLF
		cQry	+=  "		  AND D_E_L_E_T_ = ''"+CRLF
		cQry	+=  "		  AND RCL_DTINI <= '"+Dtos(dDataBase)+"' "+CRLF
		cQry	+=  "		  AND ("+CRLF
		cQry	+=  "		  		RCL_DTFIM >= '"+Dtos(dDataBase)+"' OR "+CRLF
		cQry	+=  "		  		RCL_DTFIM = ' '"+CRLF
		cQry	+=  "		   )"
		
		cQry := ChangeQuery(cQry)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),cTabAtu,.T.,.T.)
		
		if !(cTabAtu)->(Eof())
			
			if (cTabAtu)->QTDEDISPONIVEL == 0
				
				lValPosto	:= .F.
			Else
				
				nQtdePost	:= (cTabAtu)->QTDEDISPONIVEL
				lValPosto	:= .T.
			EndIf
		Else
			
			lValPosto	:= .F.
		EndIf
		
		if !lValPosto
			
			Help("",1, "Help", "Valida��o da Posto(ValPosto_01)", "Para o cargo "+cCodCarg+", departamento "+cDepto+" e filial "+cFilAt+" n�o existe posto dispon�vel!" , 3, 0)
		EndIf
		
		(cTabAtu)->(dbCloseArea())
	EndIf
	RestArea(aAreaSRA)

Return lValPosto
