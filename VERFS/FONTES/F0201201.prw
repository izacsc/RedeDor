#Include 'Protheus.ch'

/*
{Protheus.doc} F0201201()
Automatiza��o da Emiss�o das Notas Fiscais
@Author     Mick William da Silva
@Since      25/04/2016
@Version    P12.7
@Project    MAN00000463301_EF_012
@Param      
@Return     
*/

User Function F0201201()
	
	Local lLiber	:= .F.
	Local lLibTd	:= .T.
	Local lRet		:= .f.
	Local aItemPv	:= {}
	Local cNota		:= ''
	Local cMsg1		:= ''
	Local cMsg2		:= ''
	Local cSerie	:= SuperGetMV("MV_TPNRNFS") //Pego a s�rie informada no par�metro.
	Local aArea		:= GetArea()
	Local aAreaSB1	:= SB1->(GetArea())
	Local aAreaSB2	:= SB2->(GetArea())
	Local aAreaSC5	:= SC5->(GetArea())
	Local aAreaSC6	:= SC6->(GetArea())
	Local aAreaSC9	:= SC9->(GetArea())
	Local aAreaSE4	:= SE4->(GetArea())
	Local aAreaSF2	:= SF2->(GetArea())
	Local aAreaSF4	:= SF4->(GetArea())
	
	dbSelectArea("SC6")
	SC6->(DBSetOrder(1)) //Filial + Pedido
	SC6->(DbSeek(xFilial("SC6")+SC5->C5_NUM))
	
	SC9->( dbSetOrder(1) )
	SE4->( dbSetOrder(1) )
	SB1->( dbSetOrder(1) )
	SB2->( dbSetOrder(1) )
	SF4->( dbSetOrder(1) ) 
	
	//Analisa Antes os Itens em Rela��o a TES e a Quantidade Liberada Digitada ou sugerida
	//Alterado Por: Luiz Enrique no QA.	
	While SC6->(! Eof() .and. C6_FILIAL+C6_NUM ==xFilial("SC6")+SC5->C5_NUM)			//Percorre o Pedido de Venda	
		If (Posicione( "SF4", 1, xFilial("T04") + SC6->C6_TES, "F4_ESTOQUE" ) == 'S')
			cMsg2+= "Item: "+Alltrim(SC6->C6_ITEM) +" - "+ Alltrim(SC6->C6_PRODUTO) + " Com TES que movimenta Estoque: " + Alltrim(SC6->C6_TES) + CRLF 
		Endif		
		If 	SC9->( dbSeek(xFilial("SC9")+SC6->(C6_NUM+C6_ITEM)),.T.) .AND. SC9->C9_QTDLIB > 0
			cMsg2+= "Item: "+Alltrim(SC6->C6_ITEM) +" - "+ Alltrim(SC6->C6_PRODUTO) + " Com Quantidade Liberada Sugerida/Digitada: " + Alltrim(Str(SC9->C9_QTDLIB)) + CRLF 
		Endif		
		SC6->(DbSkip())
	EndDo
	
   	If !Empty(cMsg2)
   		cMsg1:= "A gera��o da Nota para o Pedido:" + Alltrim(SC5->C5_NUM) + CRLF
   		cMsg1+= "N�O poder� ser realizada pelos Motivos citados abaixo:" + CRLF + CRLF 
   		Aviso(":: Aten��o ::",cMsg1+cMsg2,{"Ok"})
   	Else		
		SC6->(DBSetOrder(1)) //Filial + Pedido
		SC6->(DbSeek(xFilial("SC6")+SC5->C5_NUM))				
		While SC6->(! Eof() .and. C6_FILIAL+C6_NUM == xFilial("SC6")+SC5->C5_NUM)			//Percorre o Pedido de Venda
									
			MaLibDoFat(SC6->(Recno()),;
			SC6->C6_QTDVEN,;      // -->Quantidade a ser liberada
			.T.,;                 // --> Bloqueio de Credito
			.T.,;                 // --> Bloqueio de Estoque - lBloqueia
			.T.,;                 // --> Avaliacao de Credito
			.F.,;                 // --> Avaliacao de Estoque
			.T.,;                 // --> Permite Liberacao Parcial
			.F.,;                 // --> Tranfere Locais automaticamente
			Nil,;                 // --> Empenhos ( Caso seja informado nao efetua a gravacao apenas avalia )
			Nil,;                 // --> CodBlock a ser avaliado na gravacao do SC9
			NiL,;                 // --> Array (aSaldos) com Empenhos previamente escolhidos (impede selecao dos empenhos pelas rotinas)
			Nil,Nil,Nil,,SC6->C6_QTDVEN)
			
			// Posiciono nas Tabelas para Faturamento
			SC9->( dbSeek(xFilial("SC9")+SC6->(C6_NUM+C6_ITEM)),.F.)
			SB1->( dbSeek(xFilial("SB1")+SC6->C6_PRODUTO) )
			SB2->( dbSeek(xFilial("SB2")+SC6->C6_PRODUTO) )
			SF4->( dbSeek(xFilial("SF4")+SC6->C6_TES) )
			
			aAdd(aItemPv,{SC6->C6_NUM 		,;	//[01]
			SC6->C6_ITEM 	,;	//[02]
			SC6->C6_LOCAL 	,;	//[03]
			SC6->C6_QTDVEN 	,;	//[04]
			SC6->C6_VALOR 	,;	//[05]
			SC6->C6_PRODUTO ,;	//[06]
			.F. 			,;	//[07]
			SC9->(RecNo())	,;	//[08]
			SC5->(RecNo()) 	,;	//[09]
			SC6->(RecNo()) 	,;	//[10]
			SE4->(RecNo())	,;	//[11]
			SB1->(RecNo())	,;	//[12]
			SB2->(RecNo())	,;	//[13]
			SF4->(RecNo())	}) 	//[14]
				
						
			IF Empty(SC9->C9_BLEST) .And. Empty(SC9->C9_BLCRED) .And. Empty(SC9->C9_BLOQUEI)
				lLiber := .T.
			EndIf
				
			SC6->(DbSkip())
		EndDo
		
		IF lLiber 		
			MaLiberOk({SC5->C5_NUM},.F.) //Altero a legenda do Pedido de Vendas			
			// Efetuo o faturamento do Pedido de Vendas
			If !Empty(aItemPv)
				Pergunte("MT461A",.F.)
				cNota := MaPvlNfs(aItemPv,cSerie, .F. , .F. , .T. , .T. , .F. , 0 , 0 , .T. , .F.)				
				// Posiciono no Cabe�alho das Notas Fiscais de Sa�da
				dbSelectArea("SF2")
				SF2->(DBSetOrder(2)) //F2_FILIAL+F2_CLIENTE+F2_LOJA+F2_DOC+F2_SERIE				
				IF SF2->(DbSeek(xFilial("SC5") + SC5->(C5_CLIENTE+C5_LOJACLI+C5_NOTA+C5_SERIE)))
					
					//Efetuo a Transmiss�o para a Prefeitura
					//AutoNfeEnv( Empresa ,Filial ,"0", Ambiente(1=produ��o,2=homologa��o),Seire,NF_DE,NF_ATE )
					AutoNfeEnv(cEmpAnt,SF2->F2_FILIAL,"0","1",SF2->F2_SERIE,SF2->F2_DOC,SF2->F2_DOC)
					
				EndIf
			Endif
			lRet:= .t.
		Else
			Help('',1,'Libera��o do Pedido',,'N�o foi poss�vel realizar libera��o do pedido.',1,0)
		EndIf
	Endif
	
	RestArea(aAreaSB1)
	RestArea(aAreaSB2)
	RestArea(aAreaSC5)
	RestArea(aAreaSC6)
	RestArea(aAreaSC9)
	RestArea(aAreaSE4)
	RestArea(aAreaSF2)
	RestArea(aAreaSF4)
	RestArea(aArea)
	
Return lRet