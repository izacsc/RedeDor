#Include 'Protheus.ch'
//-----------------------------------------------------------------------------
/*/{Protheus.doc} F0200403
Valida a(s) filial(is) informadas no pergunte
@type User function
@author Cristiane Thomaz Polli
@since 07/07/2016
@version P12.1.7
@Project MAN00000463301_EF_004
@return ${lValFil}, ${.T. o cont�udo informado � v�lido .F. n�o � v�lido.}
/*/
//-----------------------------------------------------------------------------
User Function F0200403()

	Local	lValFil		:= .T.
	Local	aArea		:= GetArea()
	Local	aAreaSM0	:= SM0->(GetArea())
	Local	nSM0Fil		:= 0
	Local 	cSM0Inf		:= Alltrim(MV_PAR01)
	Local 	nTamCont	:= Len(Alltrim(cSM0Inf))
	Local 	aRetfil 	:= FwEmpLoad(.T.)	
	Local 	aFilAtu		:= StrTokArr(cSM0Inf,";")
	Local 	nPosFil		:= 0
	Local   nPosAux		:= 0
	
	If !Empty(cSM0Inf)
			
		//verifica se o �ltimo caracter � ponto e v�rgula.
		If Substring(Alltrim(cSM0Inf),nTamCont,1) <> ';' 
				
			Aviso('Obrigat�rio Separador (;)','Informe o ponto  e virgula (;) ap�s cada c�digo. Obrigat�rio!',{'OK'})
			lValFil		:= .F.
								
		EndIf
		
		//V�lida informa��es digitadas		
		For nSM0Fil := 1 to len(aFilAtu)
	
			//Verifica se existe
			If Ascan(aRetFil,{|x,y| Alltrim(x[3]) == aFilAtu[nSM0Fil]}) == 0
	
				Aviso('Inv�lida Filial','A Filial '+aFilAtu[nSM0Fil]+' n�o existe ou colaborador n�o tem acesso. Informe uma filial v�lida!',{'OK'})
				lValFil		:= .F.	
			
			EndIf
			
			nPosFil	:= 	nSM0Fil+1
			//Verifica se a informa��o no  filtro n�o esta duplicado
			If lValFil	.AND. nPosFil <= len(aFilAtu) .AND. (nPosAux	:= Ascan(aFilAtu,{|x| Alltrim(x) == aFilAtu[nSM0Fil]},nPosFil)) > 0
				
				Aviso('Duplicidade','A filial '+aFilAtu[nSM0Fil]+' foi informada novamente na posi��o '+StrZero(nPosAux,3)+'. N�o duplique o filtro!',{'OK'})
				lValFil		:= .F.	
							
			EndIf
					
		Next nSM0Fil
	
	Else
		
		Aviso('Obrigat�rio Preenchimento','Informe pelo menos uma filial. Campo  obrigat�rio!',{'OK'})
		lValFil	:= .F.
			
	EndIf

	RestArea(aArea)
	RestArea(aAreaSM0)
	
Return lValFil