#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100319()
chamada da p�gina para realizar a inclus�o da solicita��o.
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, p�gina html
*/
User Function F0100319()
	
	Local cHtml := ""
	
	cCodDepto := HttpGet->cCodDepto
	cDescrDepto := HttpGet->cDescrDepto
	
	WEB EXTENDED INIT cHtml START "InSite"
		
		HttpPost->Posto := HttpSession->Postos[VAL(HttpGet->nIndicePosto)]
		
		cNPosto := HttpGet->cNPost
		cOpc := HttpGet->cOpc
					 			
		HttpCTType("text/html; charset=ISO-8859-1")		
		cHtml := ExecInPage( "F0100319" )
			
	WEB EXTENDED END
		
Return cHtml