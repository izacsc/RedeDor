#Include 'Protheus.ch'
#INCLUDE "FWMVCDEF.CH"
#Include 'TopConn.ch'

/*
{Protheus.doc} F0100105()
Rotina para a grava��o de Log da Medi��o de Contrato
@Author		Mick William da Silva
@Since		01/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001
@Param		cCod, C�digo que ir� identificar a Diverg�ncia, para apresenta��o correta da mensagam.
@Param		aItem, Array com os dados do Produto que aprensentou a Diverg�ncia.
*/
 
User Function F0100105(cCod,aItem)

	Local aArea		:= GetArea()
	//Local aAreaP07	:= P07->(GetArea())
	Local cDesLog	:= ""
	
	Do Case
		Case cCod == "01"
			cDesLog:= "Produto n�o encontrado em nenhuma Tabela de Pre�os"
		Case cCod == "02"
			cDesLog:= "Produto com a Data Vigente vencida"
		Case cCod == "03"
			cDesLog:= "N�o foi possivel realizar a Medi��o, pois a Tabela de Pre�o n�o foi Encontrado em nenhum Contrato"
		Case cCod == "04"
			cDesLog:= "Contrato com data de Vig�ncia fora da Validade"
		Case cCod == "05"
			cDesLog:= "Erro na inclusao da Medi��o"	
		Case cCod == "06"
			cDesLog:= "O Tipo de Solicita��o de Compras utilizado N�O considera a excu��o da rotina de Medi��o Automatizada"	
		Case cCod == "07"
			cDesLog:= "O Tipo de Solicita��o de Compras utilizado possui Aprova��o de Al�adas"
		Case cCod == "08"
			cDesLog:= "Produto Encontrado em Mais de Um Contrato Vigente"
	EndCase
	
	DbSelectArea("P07")
	P07->(DBSetOrder(1))
		
	RecLock("P07",.T.)
		P07_COD 	:= GetSxENum("P07","P07_COD")
		P07_FILIAL	:= aItem[1]
		P07_CONTR	:= IIF(aItem[10]<>"",aItem[10],"")
		P07_SOLCO	:= aItem[2]
		P07_TIPSOL	:= aItem[7]
		P07_SETSOL	:= aItem[4]
		P07_CCSOL	:= aItem[5]
		P07_SOLIC	:= aItem[6]
		P07_MOTSOL 	:= cDesLog
		P07_ITEMSC	:= aItem[3]
		P07_FORNEC	:= aItem[8]
		P07_LOJFOR	:= aItem[9]
		P07_DESCFO	:= Alltrim(Posicione("SA2",1,xFilial("SA2") + aItem[8] + aItem[9],"A2_NOME"))
		P07_DESCR	:= "Solicita��o em Processo de Cota��o" //C1_XSTMED 
		P07_DTMED	:= dDataBase
		P07_DTVIGI	:= IIF(!Empty(aItem[11]),aItem[11],P07_DTVIGI)
		P07_DTVIGF	:= IIF(!Empty(aItem[12]),aItem[12],P07_DTVIGF)
		
		ConfirmSX8()
	P07->(MsUnlock())
	
	//RestArea(aAreaP07)
	RestArea(aArea)
Return