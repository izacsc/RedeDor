#include "protheus.ch"

/*/{Protheus.doc} F0600701
Integra��o de Cadastro de Troca de Empresas.

@project	MAN0000007423040_EF_001
@type 		function
@author 	alexandre.arume
@since 		04/11/2016
@version 	1.0
@return 	$lRet, Integra��o com sucesso ou n�o.

/*/
User Function F0600701(cId, cOper, nRecno)
	
	Local cTbl			:= "EF06007"
	Local aTblStru		:= {{"EF06007_ID", "VARCHAR", 32},;
							{"EF06007_FILIALORI", "VARCHAR", 8},;
							{"EF06007_MATRICORI", "VARCHAR", 6},;
							{"EF06007_CCUSTOORI", "VARCHAR", 11},;
							{"EF06007_DEPTOORI", "VARCHAR", 9},;
							{"EF06007_PROCORI", "VARCHAR", 5},;
							{"EF06007_ITEMORI", "VARCHAR", 9},;
							{"EF06007_CLVLOR", "VARCHAR", 9},;
							{"EF06007_FILIALDST", "VARCHAR", 8},;
							{"EF06007_MATRICDST", "VARCHAR", 6},;
							{"EF06007_CCUSTODST", "VARCHAR", 11},;
							{"EF06007_DEPTODST", "VARCHAR", 9},;
							{"EF06007_PROCDST", "VARCHAR", 5},;
							{"EF06007_ITEMDST", "VARCHAR", 9},;
							{"EF06007_CLVLDST", "VARCHAR", 9},;
							{"EF06007_DTTRANSF", "VARCHAR", 8},;
							{"EF06007_DTTRANSAC", "VARCHAR", 8},;
							{"EF06007_HRTRANSAC", "VARCHAR", 8},;
							{"EF06007_OPERACAO", "VARCHAR", 6},;
							{"EF06007_STATUS", "VARCHAR", 1},;
							{"EF06007_DTPROCESS", "VARCHAR", 8},;
							{"EF06007_HRPROCESS", "VARCHAR", 8},;
							{"EF06007_OBSERVA", "VARCHAR", 700}}
	Local aValues		:= {}
	Local lRet			:= .T.
	
	dbSelectArea("SRE")
	dbGoTo(nRecno)
	
	aValues := {cId,;
				Left(SRE->RE_FILIALD,8),;
				SRE->RE_MATD,;
				SRE->RE_CCD,;
				SRE->RE_DEPTOD,;
				SRE->RE_PROCESD,;
				SRE->RE_ITEMD,;
				SRE->RE_CLVLD,;
				Left(SRE->RE_FILIALP,8),;
				SRE->RE_MATP,;
				SRE->RE_CCP,;
				SRE->RE_DEPTOP,;
				SRE->RE_PROCESP,;
				SRE->RE_ITEMP,;
				SRE->RE_CLVLP,;
				SRE->RE_DATA,;
				dDataBase,;
				TIME(),;
				"UPSERT",;
				"1",;
				"",;
				"",;
				""}
	
	// Grava��o na tabela de interface.
	lRet := U_F0600102(cTbl, aTblStru, aValues)
		
Return lRet
