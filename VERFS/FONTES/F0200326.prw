#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE 'FWMVCDEF.CH'

User Function F0200326()
Return
//===========================================================================================================
WsStruct acompanha
	WsData codacom As String
	WsData seracom As String
	WsData datacom As String
	WsData staacom As String
	WsData matacom As String
	WsData nomacom As String
	WsData apracom As String
EndWsStruct
//===========================================================================================================
WsStruct _acompanha
	WsData registro As Array Of acompanha
EndWsStruct
//===========================================================================================================
WsStruct STATUS
	WsData DESCRI As String
EndWsStruct
WsStruct _STATUS
	WsData registro As Array Of STATUS
EndWsStruct
//===========================================================================================================

WSSERVICE W0200302 DESCRIPTION "WebService Server responsavel pelo acompanhamento"
	
	WSDATA Assunto    AS String
	WSDATA FILIFUN    AS String
	WSDATA Matricu    AS String
	WSDATA lRet       AS Boolean
	WSDATA DtIni      AS String   OPTIONAL
	WSDATA DtFim      AS String   OPTIONAL
	WSDATA Mat        AS String   OPTIONAL
	WSDATA Status     AS String   OPTIONAL
	WSDATA Matricu    AS String
	WSDATA FilGes     AS String
	WSDATA _ACOM      AS _acompanha
	WSDATA _STAT      AS _STATUS
	
	WSMETHOD BuscaMat         DESCRIPTION "Busca matricula"
	WSMETHOD BuscaRH3         DESCRIPTION "Busca RH3"
	WSMETHOD BuscaSta         DESCRIPTION "Busca Status"
	
	
ENDWSSERVICE

//===========================================================================================================

WSMETHOD BuscaMat WSRECEIVE Matricu,FILIFUN WSSEND lRet WSSERVICE W0200302
	Local cAliasSra  := 'RETSRA'
	Local cQuery     := ''
	
	::lRet := .F.
	
	cQuery := "SELECT  distinct SRA.RA_MAT "
	cQuery += "FROM   " + RetSqlName("SRA") + " SRA, " + RetSqlName("SQB") + " SQB," + RetSqlName("RCX") + " RCX "
	cQuery += "WHERE SRA.RA_MAT = '" + Matricu + "' "
	cQuery += "       AND SRA.RA_FILIAL = '" + ::FILIFUN + "' and(SQB.qb_matresp = '" + Matricu + "' "
	cQuery += "       AND SQB.D_E_L_E_T_ = ' ') "
	cQuery += "       OR "
	cQuery += "      (RCX.rcx_matfun = '" + Matricu + "' "
	cQuery += "       AND RCX.RCX_SUBST = '2' "
	cQuery += "       AND RCX.D_E_L_E_T_ = ' ' ) "
	cQuery += "       AND SRA.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSra)
	
	DbSelectArea(cAliasSra)
	
	If !(EMPTY((cAliasSra)->RA_MAT))
		::lRet := .T.
	End
	(cAliasSra)->(DbCloseArea())
	
Return .T.

//===========================================================================================================

WSMETHOD BuscaRH3 WSRECEIVE FilGes, Matricu,DtIni, DtFim, Mat,Status WSSEND _ACOM WSSERVICE W0200302
	Local cAliasSra  := 'RETSRA'
	Local cQuery     := ''
	Local cMatFun    := ''
	Local cFilFun    := ''
	Local aAux 	   := {}
	Local aFun       := {}
	Local nX		   := 1
	Local nCnt       := 0
	Local oSolVg     := Nil
	
	aFun := U_F0500198(::FilGes, ::Matricu)
	
	For nCnt := 1 To Len(aFun)
		If nCnt < Len(aFun)
			cMatFun += aFun[nCnt][4] + " , "
			cFilFun += aFun[nCnt][3] + " , "
		Else
			cMatFun += aFun[nCnt][4]
			cFilFun += aFun[nCnt][3]
		EndIf
	Next nCnt
	
	cQuery := "SELECT	RH3_FILIAL, RH3_CODIGO, RH3_MAT, RH3_DTSOLI, RH3_STATUS, RH3_FILINI, "
	cQuery += "		RH3_MATINI, RH3_FILAPR, RH3_MATAPR, RH3_TIPO, RH3_XTPCTM, PA7_DESCR "
	cQuery += "FROM " + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("PA7") + " PA7 "
	cQuery += "ON PA7.PA7_CODIGO = RH3.RH3_XTPCTM "
	cQuery += "	AND PA7.D_E_L_E_T_ = ' ' "
	cQuery += "WHERE ((RH3.RH3_FILINI = '" + cFilFun + "' AND RH3.RH3_MATINI = '" + cMatFun + "') OR "
	cQuery += " (RH3.RH3_FILAPR = '" + ::FilGes + "' AND RH3.RH3_MATAPR = '" + ::Matricu + "')) AND "
	cQuery += " RH3.RH3_XTPCTM != ' ' AND "
	cQuery += " RH3.RH3_TIPO = ' ' AND "
	If !(empty(::DtIni))
		cQuery += "   RH3.RH3_DTSOLI BETWEEN '" + ::DtIni + "' AND '" + ::DtFim + "' AND "
	EndIf
	If !(empty(::Mat))
		cQuery += "	RH3.RH3_MAT = '" + ::Mat + "' AND "
	EndIf
	If !(empty(::Status)) .AND. ::Status != '9'
		cQuery += "	RH3.RH3_STATUS LIKE '%" + ::Status + "%' AND "
	EndIf
	cQuery += "		RH3.D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH3.RH3_CODIGO "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSra)
	
	DbSelectArea(cAliasSra)
	
	While ! (cAliasSra)->(EOF())
		AADD(aAux, {(cAliasSra)->RH3_CODIGO,;
			(cAliasSra)->PA7_DESCR,;
			STOD((cAliasSra)->RH3_DTSOLI),;
			(cAliasSra)->RH3_STATUS,;
			(cAliasSra)->RH3_MAT,;
			POSICIONE("SRA",1,(cAliasSra)->RH3_FILIAL + (cAliasSra)->RH3_MAT,"RA_NOME"),;
			POSICIONE("SRA",1,(cAliasSra)->RH3_FILAPR + (cAliasSra)->RH3_MATAPR,"RA_NOME") })
		(cAliasSra)->(DbSkip())
	End
	
	(cAliasSra)->(DbCloseArea())
	
	If Len(aAux) > 0
		
		::_ACOM := WSClassNew( "_acompanha" )
		::_ACOM:registro := {}
		
		oSolVg :=  WSClassNew( "acompanha" )
		For nX := 1 To Len(aAux)
			oSolVg:codacom := aAux[nX][1]
			oSolVg:seracom := aAux[nX][2]
			oSolVg:datacom := cValToChar(aAux[nX][3])
			oSolVg:staacom := aAux[nX][4]
			oSolVg:matacom := aAux[nX][5]
			oSolVg:nomacom := aAux[nX][6]
			oSolVg:apracom := aAux[nX][7]
			AAdd( ::_ACOM:registro, oSolVg )
			oSolVg :=  WSClassNew( "acompanha" )
		Next
	EndIf
	
Return .T.

//===========================================================================================================
WSMETHOD BuscaSta WSRECEIVE NULLPARAM WSSEND _STAT WSSERVICE W0200302
	Local cAliasSra  := 'RETRCC'
	Local cQuery     := ''
	Local aAux 	:= {}
	Local nX		:= 1
	Local oSolVg := Nil
	
	cQuery := "SELECT  RCC_CONTEU "
	cQuery += "FROM   " + RetSqlName("RCC") + " RCC"
	cQuery += "WHERE RCC.RCC_CODIGO = 'U007' "
	cQuery += "       AND RCC.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSra)
	
	DbSelectArea(cAliasSra)
	While ! (cAliasSra)->(EOF())
		AADD(aAux, (cAliasSra)->RCC_CONTEU)
		(cAliasSra)->(DbSkip())
	End
	(cAliasSra)->(DbCloseArea())
	
	If Len(aAux) > 0
		
		::_STAT := WSClassNew( "_STATUS" )
		::_STAT:registro := {}
		
		oSolVg :=  WSClassNew( "STATUS" )
		For nX := 1 To Len(aAux)
			oSolVg:DESCRI := aAux[nX]
			AAdd( ::_STAT:registro, oSolVg )
			oSolVg :=  WSClassNew( "STATUS" )
		Next
	EndIf
	
Return .T.

//===========================================================================================================