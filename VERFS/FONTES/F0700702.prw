#Include 'Protheus.ch' 
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} F0700702
Valida��o do CNPJ do fabricante
@author Fernando Carvalho
@since 20/01/2017
@Project MAN0000007423041_EF_007
@param cCGC, caracter, C�digo CGC
/*/
User Function F0700702(cCGC,cTipo)
	Local lRet 		:=	.T.
	Local aArea		:=	GetArea()
	Local cAlias01	:=	''
	Local cQuery01	:=	''
	Local cCompara	:=	''
	
	Begin Sequence
	
	If Empty(cCGC) .Or. !(cTipo $ "1|2")
		Break
	ElseIf cTipo == '1'
		cCompara	:=	" AND P13.P13_TIPO || SUBSTR(P13.P13_CGC,01,08) = '" + cTipo + SubStr(cCGC,01,08) + "'"
	ElseIf cTipo == '2'
		cCompara	:=	" AND P13.P13_TIPO || P13.P13_CGC         = '" + cTipo + cCGC + "'"
	EndIf

	If !Empty(cCompara)
		cAlias01	:=	GetNextAlias()
		cQuery01	:=	" SELECT P13_COD	CODIGO, " + CRLF
		cQuery01	+=	"        P13_DESCR  DESCRI  " + CRLF
		cQuery01	+=	" FROM " + RETSQLNAME('P13') + " P13 " + CRLF
		cQuery01	+=	" WHERE	P13.D_E_L_E_T_ = ''	" + CRLF
		cQuery01	+=	cCompara
		cQuery01	:=	ChangeQuery(cQuery01)
		
		dbUseArea(.T.,'TOPCONN',TcGenQry(,,cQuery01),cAlias01,.T.,.T.)
		If  (cAlias01)->(!Eof())
			Help('',1, 'CNPJ J� EXISTE!', 'O CNPJ informado j� foi cadastrado para o fabricante:' ,;
					'O CNPJ informado j� foi cadastrado para o fabricante:' + CHR(13) + CHR(10)+;
					'C�digo:   ' + (cAlias01)->CODIGO + CHR(13) + CHR(10)+;
					'Descri��o:' + (cAlias01)->DESCRI , 3, 0)
			lRet := .F.
		EndIf
		(cAlias01)->(DbCloseArea())
	Else
		Help('',1, 'Tipo de Fabricante indeterminado.','1-Nacional, 2-Estrangeiro.')
		lRet := .F.
	EndIf
	
	End Sequence
	
	RestArea(aArea)

Return lRet