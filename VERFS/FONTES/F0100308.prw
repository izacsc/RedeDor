#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"

#DEFINE OPERATION_INSERT  1
#DEFINE OPERATION_VIEW    2
#DEFINE OPERATION_APROVE  3
#DEFINE OPERATION_RPROVE  4

/*
{Protheus.doc} F0100308()
Webservice responsavel pelo portal e comunica��o com protheus
@Author     Bruno de Oliveira
@Since		07/10/2016
@Version    P12.1.7
@Project    MAN00000462901_EF_003
*/
User Function F0100308()
Return
//==================================================================================================================
WSSTRUCT SolicitVag
	WSDATA FilialVg		As String			OPTIONAL	//Filial
	WSDATA CodSolVg	 	As String			OPTIONAL	//Codigo da Solicitacao
	WSDATA CodDepto		As String			OPTIONAL	//Codigo do Departamento
	WSDATA FilPosto		As String			OPTIONAL	//Filial do Posto
	WSDATA CodPosto 	As String			OPTIONAL	//Codigo do Posto
	WSDATA CodMatric	As String			OPTIONAL	//Codigo da matricula
	WSDATA DataSolic 	As Date				OPTIONAL	//Data da solicita��o
	WSDATA DataRespost	As Date				OPTIONAL	//Data do atendimento
	WSDATA Status    	As String			OPTIONAL	//Status da solicitacao TRequestStatus- 1=Solicitada;2=Atendida pelo RH;3=Reprovada;4=Aguardando Efetivacao do RH
	WSDATA NvlInicial	As Integer			OPTIONAL	//Nivel Inicial de Aprovacao
	WSDATA FilSolic		As String			OPTIONAL	//Filial do Solicitante
	WSDATA MatSolic		As String			OPTIONAL	//Matricula do Solicitante
	WSData NomeSolic    As String			OPTIONAL	//Nome do Solicitante
	WSDATA ChavSolic	As String			OPTIONAL	//Chave de busca do solicitante
	WSDATA NvlAprov		As Integer			OPTIONAL	//Nivel de aprovacao atual
	WSDATA FilAprov     As String			OPTIONAL	//Filial do Aprovador
	WSDATA MatAprov		As String			OPTIONAL	//Matricula do Aprovador
	WSDATA NomeAprov	As String			OPTIONAL	//Nome do Aprovador
	WSDATA VisaoOrg		As String			OPTIONAL	//Visao para aprovacao
	WSDATA Source		As String			OPTIONAL	//Origem - Se passou pelo RH ou n�o
	WSDATA ParticRH     As String			OPTIONAL	//Participante
	WSDATA Empresa      As String           OPTIONAL	//empresa solicitante
	WSDATA EmpAprov     As String           OPTIONAL	//empresa aprovador
	WSData Vaga			As String			OPTIONAL	//Tipo da Vaga 1=Aumento de Quadro;2=Substitui��o
	WsData DscVaga		As String			OPTIONAL	//Descri��o da Vaga
	WSDATA CodCC		As String			OPTIONAL	//Codigo do Centro de Custo
	WSDATA CodFunc		As String			OPTIONAL	//Codigo da Funcao
	WSDATA CodCargo		As String			OPTIONAL	//Codigo do Cargo
	WSDATA PrazoVaga	As String			OPTIONAL	//Prazo da vaga
	WSDATA DatAbertura	As Date				OPTIONAL	//Data abertura da Vaga
	WSDATA DatFecham	As Date				OPTIONAL	//Data Fechamento da Vaga
	WSDATA CustoVaga	As String			OPTIONAL	//Salario(Custo da vaga)
	WSDATA ProcSelet	As String			OPTIONAL	//Codigo do Processo Seletivo
	WSDATA TpVaga		AS String			OPTIONAL	//Tipo da Vaga 1=Int\Ext; 2=Interna; 3=Externa

	WSDATA TurnoTab		As String			OPTIONAL	//Turno de Trabalho
	WSDATA HorMes		As String			OPTIONAL	//Hora M�s
	WSDATA HorSem		As String			OPTIONAL	//Horas Semanais
	WSDATA SalHora		As String			OPTIONAL	//Salario Hora
	WSDATA CatFun		As String			OPTIONAL	//Categoria funcional
	WSDATA TpContr		As String			OPTIONAL	//Tipo de Contrato 1=Determinado;2=Indeterminado
	WSDATA XObs			As String			OPTIONAL	//Observa��o
	
	WSDATA lRet			As String			OPTIONAL	//Retorno do webservice
	WSDATA TpRj			As String			OPTIONAL	//Tipo de Rejei��o 1=Cancelamento;2=Suspen��o
ENDWSSTRUCT

WsStruct VagDados
	WsData FilVag	 As String
	WSData Vaga	 As String
	WsData Descr	 As String
	WsData Depto	 As String
	WsData DescCc	 As String
	WsData Posto	 As String
	WsData FlPost	 As String
	WSDATA CodCC	 As String
	WSDATA Funcao	 As String
	WSDATA DescFu	 As String
	WSDATA NomeSol   As String
	WSDATA Prazo	 As String
	WSDATA DtAbt	 As String
	WSDATA DtFch	 As String
	WSDATA CustVg	 As String
	WSDATA ProcS	 As String
	WSDATA TpVaga	 AS String
	WSDATA CodSolic  As String
	WSDATA TurnoTab  As String
	WSDATA HorMes    As String
	WSDATA HorSem    As String
	WSDATA SalHora   As String
	WSDATA CatFun    As String
	WSDATA TpContr   As String
	WSDATA Ret	     As String
	WSDATA XObs      As String
	WSDATA ObserDet  AS String
	WSDATA CODCARGO  AS String
	WSDATA DESCRCARGO  AS String
EndWSStruct

WsStruct MnhVgSolic
	WsData CdSolic As String
	WsData DataSol As String
	WsData Status  As String
	WsData FilCdSl As String
EndWsStruct

WsStruct AMnhVgSl
	WsData aMnhVag As Array Of MnhVgSolic
EndWsStruct

WsStruct ARetSolict
	WsData CdSolic As String
	WsData DataSol As String
	WsData lRetorn As String
EndWsStruct

WsStruct ApRepVaga
	WsData CodApRp As String
	WsData DtSolic As String
	WsData StatSol As String
	WsData AcaoSol As String
	WsData FilSolc As String
EndWsStruct

WsStruct AApRepVag
	WsData aApRep As Array Of ApRepVaga
EndWsStruct

WSSTRUCT InfRh3
	WSDATA RH3VISAO   AS String
	WSDATA RH3FILINI  AS String
	WSDATA RH3MATINI  AS String
	WSDATA RH3FILAPR  AS String
	WSDATA RH3MATAPR  AS String
ENDWSSTRUCT

WsStruct SolicitQdr
	WSDATA FilialQdr	As String	OPTIONAL	//Filial
	WSDATA CodSolic	 	As String	OPTIONAL	//Codigo da Solicitacao
	WSDATA CodDepto		As String	OPTIONAL	//Codigo do Departamento
	WSDATA CodMatric	As String	OPTIONAL	//Codigo da matricula
	WSDATA DataSolic 	As Date		OPTIONAL	//Data da solicita��o
	WSDATA DataRespost	As Date		OPTIONAL	//Data do atendimento
	WSDATA Status    	As String	OPTIONAL	//Status da solicitacao TRequestStatus- 1=Solicitada;2=Atendida pelo RH;3=Reprovada;4=Aguardando Efetivacao do RH
	WSDATA NvlInicial	As Integer	OPTIONAL	//Nivel Inicial de Aprovacao
	WSDATA FilSolic		As String	OPTIONAL	//Filial do Solicitante
	WSDATA MatSolic		As String	OPTIONAL	//Matricula do Solicitante
	WSData NomeSolic    As String	OPTIONAL	//Nome do Solicitante
	WSDATA ChavSolic	As String	OPTIONAL	//Chave de busca do solicitante
	WSDATA NvlAprov		As Integer	OPTIONAL	//Nivel de aprovacao atual
	WSDATA FilAprov     As String	OPTIONAL	//Filial do Aprovador
	WSDATA MatAprov		As String	OPTIONAL	//Matricula do Aprovador
	WSDATA VisaoOrg		As String	OPTIONAL	//Visao para aprovacao
	WSDATA ParticRH     As String	OPTIONAL	//Participante
	WSDATA Empresa      As String   OPTIONAL	//empresa solicitante
	WSDATA EmpAprov     As String   OPTIONAL	//empresa aprovador
	WSDATA CdFuncao		As String	OPTIONAL	//C�digo da Fun��o
	WSDATA CdCargo		As String	OPTIONAL	//C�digo do cargo
	WSDATA CdCentCt		As String	OPTIONAL	//C�digo do Centro de custo
	WSDATA Salario		As String	OPTIONAL	//Salario
	WSDATA TpContr		As String	OPTIONAL	//Tipo de contrato
	WSDATA QntQdro		As String	OPTIONAL	//Quantidade do aumento de quadro
	WSDATA TpPosto		As String	OPTIONAL	//Tipo de posto
	WSDATA Observ		As String	OPTIONAL	//Observa��o
	WSDATA CdPost       AS String	OPTIONAL	//C�digo do Posto
	WSDATA NvPost       As String	OPTIONAL	//Novo Posto
EndWsStruct

WsStruct QdrDados
	WSDATA FilQdr	As String OPTIONAL //Filial do Aumento de Quadro
	WSDATA CdDept	As String OPTIONAL //C�digo do Departamento
	WSDATA DsDept	As String OPTIONAL //Descri��o do Departamento
	WSDATA CdFunc	As String OPTIONAL //C�digo da Fun��o
	WSDATA DsFunc	As String OPTIONAL //Descri��o da fun��o
	WSDATA CdCarg	As String OPTIONAL //C�digo do cargo
	WSDATA DsCarg	As String OPTIONAL //Descri��o do cargo
	WSDATA CdCCtt	As String OPTIONAL //C�digo do Centro de custo
	WSDATA DscCtt	As String OPTIONAL //Descri��o do centro de custo
	WSDATA Salari	As String OPTIONAL	//Salario
	WSDATA TpCont	As String OPTIONAL	//Tipo de contrato
	WSDATA QntQdr	As String OPTIONAL	//Quantidade do aumento de quadro
	WSDATA TpPost	As String OPTIONAL	//Tipo de posto
	WSDATA Observ	As String OPTIONAL	//Observa��o
	WSDATA CodPst	As String OPTIONAL	//C�digo do Posto
	WSDATA OrigSl	As String OPTIONAL	//Origem da Solicita��o
EndWsStruct

WsStruct FaixSal
	WsData NIVEL  As String
	WsData FAIXA  As String
	WsData VALOR  As String
EndWsStruct

WsStruct _FaixSal
	WsData Registro As Array Of FaixSal
EndWsStruct

WsStruct IdVaga
	WsData Q3FILIAL As String
	WsData Q3CARGO  As String
	WsData Q3CC     As String
EndWsStruct

WsStruct Aprovs
	WSDATA NvlAprov	 As Integer OPTIONAL	//Nivel de aprovacao atual
	WSDATA FilAprov	 As String  OPTIONAL	//Filial do Aprovador
	WSDATA MatAprov	 As String  OPTIONAL	//Matricula do Aprovador
	WSDATA NomeAprov As String  OPTIONAL	//Nome do Aprovador
	WSDATA EmpAprov	 As String  OPTIONAL	//empresa aprovador
EndWsStruct

WsService W0500308 Description "WebService Server para solicita��o de vagas"

	WsData wsNull       As String
	WsData Solicitacao	As SolicitVag
	WsData VDado        As VagDados
	WsData AumentQdro	AS SolicitQdr
	WsData QdroSolict	As QdrDados
	WsData IDSolic		As ARetSolict
	WsData Matricul		As String
	WsData MinhaVgs 	As AMnhVgSl
	WsData AprovVgs		As AApRepVag
	WsData lRet			As Boolean
	WsData FilialS      As String
	WsData CodSolic		As String
	WsData FilialAp		As String
	WsData Visao		As String
	WsData nOperac		As Integer
	WsData TpSolic		As String
	WSDATA _InfRh3      AS InfRh3
	WsData cPosto		As String
	WsData cFilP		As String
	WsData cDepto		As String
	WsData nOcup		As Integer
	WsData nQntd		As Integer
	WsData MatricAp     As String
	WsData EmpFun		As String
	WsData aAprovador   As Aprovs
	WSDATA _FxSal      AS _FaixSal
	WSDATA cRet        As String
	WSDATA CODVAGA     AS String
	WSDATA ValMenm     As String
	WSDATA TpSit	   As String
	WSDATA cFilP     AS String
	WSDATA _IdVaga   AS IdVaga
	
	WSDATA Pendentes	As boolean        	OPTIONAL
	WSDATA Aprovadas 	As boolean        	OPTIONAL
	WSDATA Reprovadas   As boolean        	OPTIONAL
	WSDATA PossuiPost   As boolean
	
	WsMethod InstSolict Description "Insere solicita��o de vaga para rh"
	WsMethod AprSolVaga Description "Aprova a solicita��o da vaga"
	WsMethod RepSolVaga Description "Reprova a solicita��o da vaga"
	WsMethod MinhSolict Description "Solicita��es realizadas"
	WsMethod SolicApRp  Description "Solicita��es da minha equipe"
	WsMethod BscSolVags Description "Busca as solicita��es realizadas"
	WsMethod InfSolVg   Description "Pega informa��o referente a solicita��o selecionada"
	WsMethod InsSolQdro Description "Insere solicita��o de aumento de quadro(posto)"
	WsMethod AprSolQdro Description "Aprova a solicita��o de aumento"
	WsMethod RepSolQdro Description "Reprova a solicita��o de aumento"
	WsMethod BscSolQdro Description "Busca as solicita��es realizadas"
	WsMethod VerPostSol Description "Verifica se o posto possui solicita��o"
	WsMethod VerAprvSit Description "Verifica a situa��o do departamento"
	WsMethod InfFxSl    Description "Retorna a tabela de faixa salarial"
	WsMethod PegMnem    Description "Retorna valor do mn�monico M_PERCTOL"
	WsMethod GetDepto   Description "Pega o departamento do solicitante"
	WsMethod VgDescDet  Description "Retorna descri��o detalhada da vaga"
	
EndWsService

WsMethod InstSolict WsReceive Solicitacao WsSend IDSolic WsService W0500308

	Local nResult := 0
	Local nSvSX8 := GetSX8Len()

	Self:Solicitacao:CodSolVg  := GetSX8Num("RH3", "RH3_CODIGO")
	Self:Solicitacao:DataSolic := dDataBase
	Self:Solicitacao:ChavSolic := fBuscaChaveFuncionario(Self:Solicitacao:FilSolic,Self:Solicitacao:MatSolic,Self:Solicitacao:VisaoOrg)
	
	If Empty(Self:Solicitacao:MatAprov)
		Self:Solicitacao:NvlInicial     := 0
		Self:Solicitacao:Status         := "4"
		Self:Solicitacao:DataRespost    := dDataBase
	Else
		Self:Solicitacao:NvlInicial     := Self:Solicitacao:NvlAprov+1
		Self:Solicitacao:Status         := "1"
		Self:Solicitacao:DataRespost    := CTod("")
	EndIf
	
	Begin Transaction
	
		nResult := GrSolVgRh(Self:Solicitacao, OPERATION_INSERT)
		If nResult > 0
			SetSoapFault("InstSolict", "Registro nao encontrado")
			Break
		EndIf
	
	End Transaction
	
	If nResult == 0
		While (GetSx8Len() > nSvSX8)
			ConfirmSX8()
		EndDo
		
		::IDSolic:lRetorn := "TRUE"
		::IDSolic:DataSol := DTOC(dDataBase)
		::IDSolic:CdSolic := Self:Solicitacao:CodSolVg
		
		If FindFunction("U_F0500201")
			U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"002") //Solicita��o de Vaga Aberta
		EndIf
		
		If !Empty(Self:Solicitacao:MatAprov)
			If FindFunction("U_F0500201")
				U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"005") //Aguardando Aprova��o
			EndIf
			PrpEnvMail(Self:Solicitacao:FilAprov,Self:Solicitacao:MatAprov,OPERATION_INSERT,"1","","","",Self:Solicitacao:XObs)
		Else
			If FindFunction("U_F0500201")
				U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"008") //Aguardando efetiva��o
			EndIf
			FsEnvRh()//Envia o email para o Rh efetivar
		EndIf
	Else
		::IDSolic:lRetorn := "FALSE"
		Return .F.
	EndIf
	
Return(.T.)

WsMethod AprSolVaga WsReceive Solicitacao WsSend wsNull WsService W0500308
	
	Local nResult := 0
	
	If Empty(Self:Solicitacao:MatAprov)
		Self:Solicitacao:Status       := "4" //verificar status no RH3
		Self:Solicitacao:DataRespost  := dDataBase
	Else
		Self:Solicitacao:Status       := "1"
	EndIf
	
	Begin Transaction
	
		If FindFunction("U_F0500201")
			U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"006") //Solicita��o de Vaga Aprovada**
		EndIf
	
		nResult := GrSolVgRh(Self:Solicitacao, OPERATION_APROVE)
		If nResult > 0
			SetSoapFault("AprSolVaga", "Registro nao encontrado")
			Break
		EndIf
	
	End Transaction
	
	If nResult > 0
		Return .F.
	Else
		If !Empty(Self:Solicitacao:MatAprov)
			If FindFunction("U_F0500201")
				U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"005") //Aguardando Aprova��o
			EndIf
			PrpEnvMail(Self:Solicitacao:FilAprov,Self:Solicitacao:MatAprov,OPERATION_APROVE,"1","","","",Self:Solicitacao:XObs)
		Else
			If FindFunction("U_F0500201")
				U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"008") //Aguardando Efetiva��o
			EndIf
			FsEnvRh()//Envia e-mail para o Rh Aprovar
		EndIf
	EndIf
	
Return .T.

WsMethod RepSolVaga WsReceive Solicitacao WsSend WsNull WsService W0500308

	Local nResult := 0
	
	Self:Solicitacao:Status := "3"
	
	Begin Transaction
	
		nResult:= GrSolVgRh(Self:Solicitacao, OPERATION_RPROVE)
		If nResult > 0
			SetSoapFault("RepSolVaga", "Registro nao encontrado")
			Break
		EndIf
		
	End Transaction
	
	If nResult > 0
		Return .F.
	Else
		If FindFunction("U_F0500201")
			U_F0500201(Self:Solicitacao:FilialVg,Self:Solicitacao:CodSolVg,"007") //Solicita��o de Vaga Reprovada
		EndIf
		PrpEnvMail(Self:Solicitacao:FilAprov,Self:Solicitacao:MatAprov,OPERATION_RPROVE,"1",Self:Solicitacao:VisaoOrg,Self:Solicitacao:cMatSolic,Self:Solicitacao:FilSolic,"","","",Self:Solicitacao:XObs)
	EndIf
	
Return .T.

Static Function GrSolVgRh(oSolicVag,nOperacao)

	Local nResult := 0
	Local nX      := 0
	Local nItem   := 0
	Local cFilRH4 := xFilial("RH4", oSolicVag:FilialVg)
	Local cMatSol := ""
	Local aDados  := {}
	
	If nOperacao == 1
		cMatSol := Alltrim(Posicione('SRA',1,oSolicVag:FilSolic + IIF(EMPTY(oSolicVag:MatSolic)   ,"",oSolicVag:MatSolic),'RA_NOME'))
		aDados  := {	{"QS_FILIAL"  , IIF(EMPTY(oSolicVag:FilialVg)   ,"",oSolicVag:FilialVg         )},;
		{"TMP_VAGA"   , IIF(EMPTY(oSolicVag:Vaga)       ,"",oSolicVag:Vaga             )},;
		{"QS_DESCRIC" , IIF(EMPTY(oSolicVag:DscVaga)    ,"",oSolicVag:DscVaga          )},;
		{"RBT_DEPTO"  , IIF(EMPTY(oSolicVag:CodDepto)   ,"",oSolicVag:CodDepto         )},;
		{"QS_POSTO"   , IIF(EMPTY(oSolicVag:CodPosto)   ,"",oSolicVag:CodPosto         )},;
		{"QS_FILPOST" , IIF(EMPTY(oSolicVag:FilPosto)   ,"",oSolicVag:FilPosto         )},;
		{"QS_CC"      , IIF(EMPTY(oSolicVag:CodCC)      ,"",oSolicVag:CodCC            )},;
		{"QS_FUNCAO"  , IIF(EMPTY(oSolicVag:CodFunc)    ,"",oSolicVag:CodFunc          )},;
		{"QS_SOLICIT" , IIF(EMPTY(cMatSol)              ,"",cMatSol                    )},;
		{"QS_PRAZO"   , IIF(EMPTY(oSolicVag:PrazoVaga)  ,"",oSolicVag:PrazoVaga        )},;
		{"QS_DTABERT" , IIF(EMPTY(oSolicVag:DatAbertura),"",DTOC(oSolicVag:DatAbertura))},;
		{"QS_DTFECH"  , IIF(EMPTY(oSolicVag:DatFecham)  ,"",DTOC(oSolicVag:DatFecham)  )},;
		{"QS_VCUSTO"  , IIF(EMPTY(oSolicVag:CustoVaga)  ,"",oSolicVag:CustoVaga        )},;
		{"QS_PROCESS" , IIF(EMPTY(oSolicVag:ProcSelet)  ,"",oSolicVag:ProcSelet        )},;
		{"QS_TIPO"    , IIF(EMPTY(oSolicVag:TpVaga)     ,"",oSolicVag:TpVaga           )},;
		{"QS_XSOLPTL" , IIF(EMPTY(oSolicVag:CodSolVg)   ,"",oSolicVag:CodSolVg         )},;
		{"QG_XTURNTB" , IIF(EMPTY(oSolicVag:TurnoTab)   ,"",oSolicVag:TurnoTab         )},;
		{"QG_XHRMES"  , IIF(EMPTY(oSolicVag:HorMes)     ,"",oSolicVag:HorMes           )},;
		{"QG_XHRSEM"  , IIF(EMPTY(oSolicVag:HorSem)     ,"",oSolicVag:HorSem           )},;
		{"QG_XSALHOR" , IIF(EMPTY(oSolicVag:SalHora)    ,"",oSolicVag:SalHora          )},;
		{"QG_XCATFUN" , IIF(EMPTY(oSolicVag:CatFun)     ,"",oSolicVag:CatFun           )},;
		{"QG_XTPCONT" , IIF(EMPTY(oSolicVag:TpContr)    ,"",oSolicVag:TpContr          )},;
		{"TMP_XOBS"   , IIF(EMPTY(oSolicVag:XObs)       ,"",oSolicVag:XObs             )};
		}
	EndIf
	
	DbSelectArea("RH3")
	RH3->(DbSetOrder(1))
	
	Begin Sequence
	 
		If nOperacao == 1
			Reclock("RH3",.T.)
			RH3->RH3_FILIAL	:= oSolicVag:FilialVg
			RH3->RH3_CODIGO	:= oSolicVag:CodSolVg
			RH3->RH3_MAT   	:= oSolicVag:CodMatric
			RH3->RH3_TIPO  	:= ""
			RH3->RH3_ORIGEM	:= "PORTAL"
			RH3->RH3_DTSOLI	:= oSolicVag:DataSolic
			RH3->RH3_KEYINI	:= oSolicVag:ChavSolic
			RH3->RH3_VISAO  := oSolicVag:VisaoOrg
			RH3->RH3_NVLINI	:= oSolicVag:NvlInicial //verificar o nivel inicial
			RH3->RH3_FILINI	:= oSolicVag:FilSolic
			RH3->RH3_MATINI	:= oSolicVag:MatSolic
			RH3->RH3_XTPCTM := "002"
			
			If RH3->(ColumnPos("RH3_EMP")) > 0 .AND. RH3->(ColumnPos("RH3_EMPINI")) > 0 .AND. RH3->(ColumnPos("RH3_EMPAPR")) > 0
				If Empty(oSolicVag:Empresa)
					oSolicVag:Empresa := cEmpAnt
				EndIf
			
				If Empty(oSolicVag:EmpAprov)
					oSolicVag:EmpAprov := cEmpAnt
				EndIf
			
				RH3->RH3_EMP	:= oSolicVag:Empresa
				RH3->RH3_EMPINI	:= cEmpAnt
				RH3->RH3_EMPAPR	:= oSolicVag:EmpAprov
			EndIf
			
			If ColumnPos('QB_KEYINI')>0
				//Atualiza Chave de busca no departamento.
				If Select("SQB")>0
					If SQB->(FieldPos("QB_KEYINI")) > 0
						If Empty(SQB->QB_KEYINI) .OR. SQB->QB_KEYINI <> oSolicVag:ChavSolic
							Reclock("SQB",.F.)
							SQB->QB_KEYINI := oSolicVag:ChavSolic
							SQB->(MsUnlock())
						EndIf
					EndIf
				EndIf
			EndIf
			
		Else
			If RH3->(DbSeek(oSolicVag:FilialVg+oSolicVag:CodSolVg))
				Reclock("RH3",.F.)
			Else
				nResult := 1
				break
			EndIf
		EndIf
		
		If nOperacao == 3 .And. AI8->AI8_APRVLV <> 0 .And. ( RH3->RH3_NVLINI - oSolicVag:NvlAprov ) > AI8->AI8_APRVLV
			RH3->RH3_STATUS := "4"
			RH3->RH3_NVLAPR := 99
			RH3->RH3_FILAPR := ""
			RH3->RH3_MATAPR := ""
			If RH3->(ColumnPos("RH3_EMPAPR")) > 0
				RH3->RH3_EMPAPR	:= ""
			EndIf
		Else
			RH3->RH3_STATUS := oSolicVag:Status
			If nOperacao != 4 //Aten��o para teste (rejei��o) //Verificar em caso de ser um notificador
				RH3->RH3_NVLAPR := oSolicVag:NvlAprov
				RH3->RH3_FILAPR := oSolicVag:FilAprov
				RH3->RH3_MATAPR := oSolicVag:MatAprov
				If RH3->(ColumnPos("RH3_EMPAPR")) > 0
					RH3->RH3_EMPAPR	:= oSolicVag:EmpAprov
				EndIf
			EndIf
		EndIf
		
		If nOperacao == 4 //Rejei��o
			If oSolicVag:TpRj == "2" //suspender
				RH3->RH3_XSUSPE := "2"
			EndIf
		EndIf
			
		RH3->(MsUnlock())
	End Sequence
	
	If nResult == 0

		DBSelectArea("RH4")
		
		If nOperacao == 1
			For nX:= 1 To Len(aDados)
				If !Empty(aDados[nX, 1])
					Reclock("RH4",.T.)
					RH4->RH4_FILIAL	:= cFilRH4
					RH4->RH4_CODIGO	:= oSolicVag:CodSolVg
					RH4->RH4_ITEM	:= ++nItem
					RH4->RH4_CAMPO	:= aDados[nX, 1]
					If aDados[nX, 1] == "TMP_XOBS"
						RH4->RH4_XOBS	:= aDados[nX, 2]
					Else
						RH4->RH4_VALNOV	:= aDados[nX, 2]
					EndIf
					RH4->(MsUnlock())
				EndIf
			Next nX
		EndIf
	
	EndIf
	
Return nResult

WsMethod MinhSolict WsReceive Matricul,TpSolic WsSend MinhaVgs WsService W0500308

	Local aAux 	:= {}
	Local nX		:= 1
	Local oSolVg := Nil
	
	aAux := BscMinhSol(::Matricul,::TpSolic)
	
	If Len(aAux) > 0
	
		::MinhaVgs := WSClassNew( "AMnhVgSl" )
		::MinhaVgs:aMnhVag := {}
		
		oSolVg :=  WSClassNew( "MnhVgSolic" )
		For nX := 1 To Len(aAux)
			oSolVg:CdSolic 	:= aAux[nX][1]
			oSolVg:DataSol	:= aAux[nX][2]
			If aAux[nX][4] == "2"
				oSolVg:Status 	:= "5"
			Else
				oSolVg:Status 	:= aAux[nX][3]
			EndIf
			oSolVg:FilCdSl := aAux[nX][5]
			AAdd( ::MinhaVgs:aMnhVag, oSolVg )
			oSolVg :=  WSClassNew( "MnhVgSolic" )
		Next
	EndIf
	
Return(.T.)

Static Function BscMinhSol(cMatric,cTpSolic)

	Local aArea 	:= GetArea()
	Local cQuery 	:= ""
	Local cAlias1	:= GetNextAlias()
	Local aAux		:= {}
	
	cQuery := "SELECT RH3_FILIAL, RH3_CODIGO, RH3_DTSOLI, RH3_STATUS, RH3_XSUSPE "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_XTPCTM = '" + cTpSolic + "' AND RH3_MATINI = '" + cMatric + "' AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	DbSelectArea(cAlias1)
	While !(cAlias1)->(EOF())
		AADD(aAux, {(cAlias1)->RH3_CODIGO,(cAlias1)->RH3_DTSOLI,(cAlias1)->RH3_STATUS,(cAlias1)->RH3_XSUSPE,(cAlias1)->RH3_FILIAL})
		(cAlias1)->(DbSkip())
	End
	
	(cAlias1)->(DbCloseArea())
	RestArea(aArea)

Return aAux

WsMethod SolicApRp WsReceive Matricul,FilialAp,Visao,TpSolic WsSend AprovVgs WsService W0500308

	Local aDados:= {}
	Local nY	:= 1
	Local oARVg := Nil
	
	aDados := BscSolVg(::Matricul,::FilialAp,::Visao,::TpSolic)
	
	If Len(aDados) > 0
	
		::AprovVgs := WSClassNew( "AApRepVag" )
		::AprovVgs:aApRep := {}
		
		oARVg :=  WSClassNew( "ApRepVaga" )
		For nY := 1 To Len(aDados)
			oARVg:CodApRp 	:= aDados[nY][1]
			oARVg:DtSolic	:= aDados[nY][2]
			oARVg:StatSol 	:= aDados[nY][3]
			oARVg:AcaoSol   := aDados[nY][4]
			oARVg:FilSolc	:= aDados[nY][5]
			AAdd( ::AprovVgs:aApRep, oARVg )
			oARVg :=  WSClassNew( "ApRepVaga" )
		Next nY
	EndIf

Return(.T.)

Static Function BscSolVg(cMatric,cFilAp,cVisao,cTpSolic)

	Local aArea 	:= GetArea()
	Local cAlias1	:= GetNextAlias()
	Local cQuery 	:= ""
	Local cAcao		:= ""
	Local aDad		:= {}
	
	cQuery := "SELECT RH3_FILIAL, RH3_VISAO, RH3_CODIGO, RH3_DTSOLI, RH3_STATUS "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_VISAO = '" + cVisao + "' "
	cQuery += "	AND RH3_XTPCTM = '" + cTpSolic + "' "
	cQuery += "	AND RH3_STATUS = '1' "
	cQuery += "	AND RH3_FILAPR = '" + cFilAp + "' "
	cQuery += "	AND RH3_MATAPR = '" + cMatric + "' "
	cQuery += "	AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	DbSelectArea(cAlias1)
	While !(cAlias1)->(EOF())
		
		If (cAlias1)->RH3_STATUS $ "14"
			cAcao := "Pendente"
		ElseIf (cAlias1)->RH3_STATUS == "2"
			cAcao := "Aprovado"
		Else
			cAcao := "Reprovado"
		EndIf
		
		AADD(aDad, {(cAlias1)->RH3_CODIGO,(cAlias1)->RH3_DTSOLI,(cAlias1)->RH3_STATUS,cAcao,(cAlias1)->RH3_FILIAL})
		
		(cAlias1)->(DbSkip())
	End
	
	(cAlias1)->(DbCloseArea())
	RestArea(aArea)

Return aDad

WsMethod BscSolVags WsReceive CodSolic,Matricul,Visao,FilialS WsSend VDado WsService W0500308

	Local aAux    := {}
	Local aAreas  := {SRJ->(GetArea()),SQB->(GetArea()),GetArea()}

	aAux := FTdsSolic(::CodSolic,::Matricul,::Visao,::FilialS)
			
	If Len(aAux) > 0
		/*
			WsData FilVag,Vaga,Descr,Depto,DescCc,Posto
			FlPost,CodCC,Funcao,DescFu,NomeSol,Prazo,DtAbt		
			DtFch,CustVg,ProcS,TpVaga,CodSolic,TurnoTab,HorMes,HorSem
			SalHora,CatFun,TpContr,Ret,XObs
		*/

		::VDado:FilVag   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_FILIAL" }))�= 0,'',aAux[var][2])
		::VDado:Vaga     := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "TMP_VAGA"  }))�= 0,'',aAux[var][2])
		::VDado:Descr    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_DESCRIC"}))�= 0,'',aAux[var][2])
		::VDado:Depto    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RBT_DEPTO" }))�= 0,'',aAux[var][2])
		::VDado:Posto    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_POSTO"  }))�= 0,'',aAux[var][2])
		::VDado:FlPost   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_FILPOST"}))�= 0,'',aAux[var][2])
		::VDado:CodCC    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_CC"     }))�= 0,'',aAux[var][2])
		::VDado:Funcao   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_FUNCAO" }))�= 0,'',aAux[var][2])
		::VDado:NomeSol  := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_SOLICIT"}))�= 0,'',aAux[var][2])
		::VDado:Prazo    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_PRAZO"  }))�= 0,'',aAux[var][2])
		::VDado:DtAbt    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_DTABERT"}))�= 0,'',aAux[var][2])
		::VDado:DtFch    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_DTFECH" }))�= 0,'',aAux[var][2])
		::VDado:CustVg   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_VCUSTO" }))�= 0,'',aAux[var][2])
		::VDado:ProcS    := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_PROCESS"}))�= 0,'',aAux[var][2])
		::VDado:TpVaga   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_TIPO"   }))�= 0,'',aAux[var][2])
		::VDado:DescCc   := POSICIONE("SQB", 1, XFILIAL("SQB") + Alltrim(IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RBT_DEPTO" }))�= 0,'',aAux[var][2])), "QB_DESCRIC")
		::VDado:DescFu   := POSICIONE("SRJ", 1, XFILIAL("SRJ") + Alltrim(IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_FUNCAO" }))�= 0,'',aAux[var][2])), "RJ_DESC")
		::VDado:CodSolic := ""
		::VDado:TurnoTab := ""
		::VDado:HorSem   := ""
		::VDado:HorMes   := IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QG_XHRMES"}))�= 0,'',aAux[var][2])
		::VDado:SalHora  := ""
		::VDado:CatFun   := ""
		::VDado:TpContr  := ""
		cChave := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao + ::VDado:DescCc,"Q3_DESCDET")
		If EMPTY(cChave)
			cChave := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao ,"Q3_DESCDET")
		EndIf
		::VDado:ObserDet := MSMM(cChave,,,,3,,,"SQ3","Q3_MEMO1")//
		cCODCARGO := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao + ::VDado:DescCc,"Q3_CARGO")
		If EMPTY(cCODCARGO)
			cCODCARGO := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao,"Q3_CARGO")
		EndIf
		::VDado:CODCARGO   := cCODCARGO
		cDESCRCARGO := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao + ::VDado:DescCc,"Q3_DESCSUM")
		If EMPTY(cDESCRCARGO)
			cDESCRCARGO := POSICIONE("SQ3",1,Fwxfilial("SQ3") + ::VDado:Funcao,"Q3_DESCSUM")
		EndIf
		::VDado:DESCRCARGO := cDESCRCARGO
		DbSelectArea("RH4")
		RH4->(DbSetOrder(1))
		If RH4->(DBSEEK(Alltrim(IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "QS_FILIAL" }))�= 0,'',aAux[var][2])) + ::CodSolic + "23"))
			::VDado:XObs := RH4->RH4_XOBS
		Else
			::VDado:XObs := ""
		EndIf
		RH4->(DbCloseArea())
		::VDado:Ret     := ".T."
				
	Else
	
		::VDado:Ret     := ".F."
	
	EndIf

	AEval(aAreas, {|x| RestArea(x)} )
	
Return(.T.)

Static Function FTdsSolic(cCodSolic,cMatricul,cVisao,cFilCSol)

	Local aDados := {}
	Local cAlias1 := GetNextAlias()
	Local cQuery := ""


	cQuery := "SELECT RH3.RH3_CODIGO,RH3.RH3_MAT,RH3.RH3_VISAO,RH4.RH4_CODIGO,RH4.RH4_CAMPO,RH4.RH4_VALNOV"
	cQuery += "FROM " + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("RH4") + " RH4 ON(RH3.RH3_CODIGO = RH4.RH4_CODIGO AND RH4.D_E_L_E_T_ = ' ') "
	cQuery += "WHERE RH3.RH3_FILIAL = '" + cFilCSol + "' AND "
	cQuery += "RH3.RH3_CODIGO = '" + cCodSolic + "' AND "
	cQuery += "RH3.RH3_VISAO = '" + cVisao + "' AND RH3.D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH4.RH4_ITEM "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	DbSelectArea(cAlias1)
	While !(cAlias1)->(EOF())
		AADD(aDados, {(cAlias1)->RH4_CAMPO,(cAlias1)->RH4_VALNOV})
		(cAlias1)->(DbSkip())
	End
	(cAlias1)->(DbCloseArea())

Return aDados

WsMethod InsSolQdro WsReceive AumentQdro WsSend IDSolic WsService W0500308

	Local nResult
	Local nSvSX8 := GetSX8Len()

	Self:AumentQdro:CodSolic := GetSX8Num("RH3", "RH3_CODIGO")
	Self:AumentQdro:DataSolic := dDataBase
	Self:AumentQdro:ChavSolic := fBuscaChaveFuncionario(Self:AumentQdro:FilSolic,Self:AumentQdro:MatSolic,Self:AumentQdro:VisaoOrg)
	
	If Empty(Self:AumentQdro:MatAprov)
		Self:AumentQdro:NvlInicial		:= 0
		Self:AumentQdro:Status			:= "4"
		Self:AumentQdro:DataRespost		:= dDataBase
	Else
		Self:AumentQdro:NvlInicial		:= Self:AumentQdro:NvlAprov+1
		Self:AumentQdro:Status			:= "1"
		Self:AumentQdro:DataRespost 	:= CTod("")
	EndIf
	
	Begin Transaction
	
		nResult := GrSolQdro(Self:AumentQdro, OPERATION_INSERT)
		If nResult > 0
			SetSoapFault("InsSolQdro", "Registro nao encontrado")
			Break
		EndIf
	
	End Transaction
	
	If nResult == 0
		While (GetSx8Len() > nSvSX8)
			ConfirmSX8()
		EndDo
		
		::IDSolic:lRetorn := "TRUE"
		::IDSolic:DataSol := DTOC(dDataBase)
		::IDSolic:CdSolic := Self:AumentQdro:CodSolic
		
		If FindFunction("U_F0500201")
			U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"016") //Solicita��o de Aumento de Quadro Aberta
		EndIf
		
		If !Empty(Self:AumentQdro:MatAprov)
			If FindFunction("U_F0500201")
				U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"017") //Aguardando Aprova��o
			EndIf
			PrpEnvMail(Self:AumentQdro:FilAprov,Self:AumentQdro:MatAprov,OPERATION_INSERT,"2","","","",Self:AumentQdro:Observ)
		Else
			If FindFunction("U_F0500201")
				U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"023")//Aguardando efetiva��o Posto
			EndIf
			FsEnvRh()
		EndIf
	Else
		::IDSolic:lRetorn := "FALSE"
		Return .F.
	EndIf

Return(.T.)

WsMethod AprSolQdro WsReceive AumentQdro WsSend wsNull WsService W0500308
	
	Local nResult := 0
	
	If Empty(Self:AumentQdro:MatAprov)
		Self:AumentQdro:Status       := "4" //verificar status no RH3
		Self:AumentQdro:DataRespost  := dDataBase
	Else
		Self:AumentQdro:Status       := "1"
	EndIf
	
	If FindFunction("U_F0500201")
		U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"018") //Solicita��o de Aumento de Quadro Aprovado
	EndIf
	nResult := GrSolQdro(Self:AumentQdro, OPERATION_APROVE)
	If nResult > 0
		SetSoapFault("AprSolQdro", "Registro nao encontrado")
		Break
	EndIf
	
	If nResult > 0
		Return .F.
	Else
		If !Empty(Self:AumentQdro:MatAprov)
			If FindFunction("U_F0500201")
				U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"017") //Aguardando Aprova��o
			EndIf
			PrpEnvMail(Self:AumentQdro:FilAprov,Self:AumentQdro:MatAprov,OPERATION_APROVE,"2","","","",Self:AumentQdro:Observ)
		Else
			If FindFunction("U_F0500201")
				U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"023")//Aguardando efetiva��o Posto
			EndIf
			FsEnvRh()
		EndIf
	EndIf
	
Return(.T.)

WsMethod RepSolQdro WsReceive AumentQdro WsSend WsNull WsService W0500308

	Local nResult := 0
	
	Self:AumentQdro:Status := "3"
	
	nResult:= GrSolQdro(Self:AumentQdro, OPERATION_RPROVE)
	If nResult > 0
		SetSoapFault("RepSolQdro", "Registro nao encontrado")
		Break
	EndIf
	
	If nResult > 0
		Return .F.
	Else
		If FindFunction("U_F0500201")
			U_F0500201(Self:AumentQdro:FilialQdr,Self:AumentQdro:CodSolic,"019") //Solicita��o de Aumento de Quadro Reprovado
		EndIf
		PrpEnvMail(Self:AumentQdro:FilAprov,Self:AumentQdro:MatAprov,OPERATION_RPROVE,"2",Self:AumentQdro:VisaoOrg,Self:AumentQdro:MatSolic,Self:AumentQdro:FilSolic,Self:AumentQdro:Observ)
	EndIf
	
Return(.T.)

Static Function GrSolQdro(oSolicQdr,nOperacao)

	Local nResult := 0
	Local nX      := 0
	Local nItem   := 0
	Local cFilRH4 := xFilial("RH4", oSolicQdr:FilialQdr)
	Local cSim    := "Novo Posto"
	Local cNao    := "Aumento de Quantidade"
	Local cDescDp := ""
	Local cDescCC := ""
	Local cDescFc := ""
	Local cDescCg := ""
	Local aDados  := {}
	
	If nOperacao == 1
		cDescDp := Alltrim(Posicione('SQB',1,xFilial("SQB")+oSolicQdr:CodDepto,'SQB->QB_DESCRIC'))
		cDescCC := Alltrim(Posicione('CTT',1,xFilial("CTT")+oSolicQdr:CdCentCt,'CTT->CTT_DESC01'))
		cDescFc := Alltrim(Posicione('SRJ',1,xFilial("SRJ")+oSolicQdr:CdFuncao,'SRJ->RJ_DESC'))
		cDescCg := Alltrim(Posicione('SQ3',1,xFilial("SQ3")+oSolicQdr:CdCargo,'SQ3->Q3_DESCSUM'))
		aDados  := {{"RCL_FILIAL", IIf(Empty(oSolicQdr:FilialQdr),"",oSolicQdr:FilialQdr)},;
		{"RCL_DEPTO" , IIf(Empty(oSolicQdr:CodDepto),"",oSolicQdr:CodDepto)},;
		{"RCL_DDEPTO", IIf(Empty(cDescDp),"",cDescDp) },;
		{"RCL_CC"    , IIf(Empty(oSolicQdr:CdCentCt),"",oSolicQdr:CdCentCt)},;
		{"RCL_DESCCC", IIf(Empty(cDescCC),"",cDescCC)},;
		{"RCL_FUNCAO", IIf(Empty(oSolicQdr:CdFuncao),"",oSolicQdr:CdFuncao)},;
		{"RCL_DFUNC" , IIf(Empty(cDescFc),"",cDescFc)},;
		{"RCL_CARGO" , IIf(Empty(oSolicQdr:CdCargo),"",oSolicQdr:CdCargo)},;
		{"RCL_DCARGO", IIf(Empty(cDescCg),"",cDescCg)},;
		{"RCL_SALAR" , IIf(Empty(oSolicQdr:Salario),"",oSolicQdr:Salario)},;
		{"RCL_TPOSTO", IIf(Empty(oSolicQdr:TpPosto),"",oSolicQdr:TpPosto)},;
		{"RCL_TPCONT", IIf(Empty(oSolicQdr:TpContr),"",oSolicQdr:TpContr)},;
		{"RCL_NPOSTO", IIf(Empty(oSolicQdr:QntQdro),"",oSolicQdr:QntQdro)},;
		{"TMP_JUSTIF", IIf(Empty(oSolicQdr:Observ),"",oSolicQdr:Observ)},;
		{"RCL_POSTO" , IIf(Empty(oSolicQdr:CdPost),"",oSolicQdr:CdPost)},;
		{"RBT_TIPOR" , IIf(Empty(oSolicQdr:NvPost),"",IIF(oSolicQdr:NvPost=="1",cSim,cNao))}}
	EndIf
	
	DbSelectArea("RH3")
	RH3->(DbSetOrder(1))
	
	Begin Sequence
	 
		If nOperacao == 1
			Reclock("RH3",.T.)
			RH3->RH3_FILIAL	:= oSolicQdr:FilialQdr
			RH3->RH3_CODIGO	:= oSolicQdr:CodSolic
			RH3->RH3_MAT   	:= oSolicQdr:CodMatric
			RH3->RH3_TIPO  	:= ""
			RH3->RH3_ORIGEM	:= "PORTAL"
			RH3->RH3_DTSOLI	:= oSolicQdr:DataSolic
			RH3->RH3_KEYINI	:= oSolicQdr:ChavSolic
			RH3->RH3_VISAO  := oSolicQdr:VisaoOrg
			RH3->RH3_NVLINI	:= oSolicQdr:NvlInicial //verificar o nivel inicial
			RH3->RH3_FILINI	:= oSolicQdr:FilSolic
			RH3->RH3_MATINI	:= oSolicQdr:MatSolic
			RH3->RH3_XTPCTM := "003"
			
			If RH3->(ColumnPos("RH3_EMP")) > 0 .AND. RH3->(ColumnPos("RH3_EMPINI")) > 0 .AND. RH3->(ColumnPos("RH3_EMPAPR")) > 0
				If Empty(oSolicQdr:Empresa)
					oSolicQdr:Empresa := cEmpAnt
				EndIf
			
				If Empty(oSolicQdr:EmpAprov)
					oSolicQdr:EmpAprov := cEmpAnt
				EndIf
			
				RH3->RH3_EMP	:= oSolicQdr:Empresa
				RH3->RH3_EMPINI	:= cEmpAnt
				RH3->RH3_EMPAPR	:= oSolicQdr:EmpAprov
			EndIf
			
			If ColumnPos('QB_KEYINI')>0
				//Atualiza Chave de busca no departamento.
				If Select("SQB")>0
					If SQB->(FieldPos("QB_KEYINI")) > 0
						If Empty(SQB->QB_KEYINI) .OR. SQB->QB_KEYINI <> oSolicQdr:ChavSolic
							Reclock("SQB",.F.)
							SQB->QB_KEYINI := oSolicQdr:ChavSolic
							SQB->(MsUnlock())
						EndIf
					EndIf
				EndIf
			EndIf
			
		Else
			If RH3->(DbSeek(oSolicQdr:FilialQdr+oSolicQdr:CodSolic))
				Reclock("RH3",.F.)
			Else
				nResult := 1
				break
			EndIf
		EndIf
		
		If nOperacao == 3 .And. AI8->AI8_APRVLV <> 0 .And. ( RH3->RH3_NVLINI - oSolicQdr:NvlAprov ) > AI8->AI8_APRVLV
			RH3->RH3_STATUS := "4"
			RH3->RH3_NVLAPR := 99
			RH3->RH3_FILAPR := ""
			RH3->RH3_MATAPR := ""
			If RH3->(ColumnPos("RH3_EMPAPR")) > 0
				RH3->RH3_EMPAPR	:= ""
			EndIf
		Else
			RH3->RH3_STATUS := oSolicQdr:Status
			RH3->RH3_NVLAPR := oSolicQdr:NvlAprov
			RH3->RH3_FILAPR := oSolicQdr:FilAprov
			RH3->RH3_MATAPR := oSolicQdr:MatAprov
			If RH3->(ColumnPos("RH3_EMPAPR")) > 0
				RH3->RH3_EMPAPR	:= oSolicQdr:EmpAprov
			EndIf
		EndIf
			
		RH3->(MsUnlock())
		
	End Sequence
	
	If nResult == 0

		DBSelectArea("RH4")
		
		If nOperacao == 1
			For nX:= 1 To Len(aDados)
				If !Empty(aDados[nX, 1])
					Reclock("RH4",.T.)
					RH4->RH4_FILIAL	:= cFilRH4
					RH4->RH4_CODIGO	:= oSolicQdr:CodSolic
					RH4->RH4_ITEM	:= ++nItem
					RH4->RH4_CAMPO	:= aDados[nX, 1]
					RH4->RH4_VALNOV	:= aDados[nX, 2]
					RH4->(MsUnlock())
				EndIf
			Next nX
		EndIf
	
	EndIf
	
Return nResult

WsMethod BscSolQdro WsReceive CodSolic,Matricul,Visao,FilialS WsSend QdroSolict WsService W0500308

	Local aAux    := {}
	Local aArea  := GetArea()

	aAux := FTdsSolic(::CodSolic,::Matricul,::Visao,::FilialS)
            
	If Len(aAux) > 0

		::QdroSolict:FilQdr	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_FILIAL" }))�= 0,'',aAux[var][2])
		::QdroSolict:CdDept	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_DEPTO"  }))�= 0,'',aAux[var][2]) 
		::QdroSolict:DsDept	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_DDEPTO" }))�= 0,'',aAux[var][2])
		::QdroSolict:CdCCtt	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_CC"     }))�= 0,'',aAux[var][2])
		::QdroSolict:DscCtt	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_DESCCC" }))�= 0,'',aAux[var][2])
		::QdroSolict:CdFunc	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_FUNCAO" }))�= 0,'',aAux[var][2])
		::QdroSolict:DsFunc	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_DFUNC"  }))�= 0,'',aAux[var][2])
		::QdroSolict:CdCarg	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_CARGO"  }))�= 0,'',aAux[var][2])
		::QdroSolict:DsCarg	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_DCARGO" }))�= 0,'',aAux[var][2])
		::QdroSolict:Salari	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_SALAR"  }))�= 0,'',aAux[var][2])
		::QdroSolict:TpPost	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_TPOSTO" }))�= 0,'',aAux[var][2])
		::QdroSolict:TpCont	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_TPCONT" }))�= 0,'',aAux[var][2])
		::QdroSolict:QntQdr	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_NPOSTO" }))�= 0,'',aAux[var][2])
		::QdroSolict:Observ	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "TMP_JUSTIF" }))�= 0,'',aAux[var][2])
		::QdroSolict:CodPst	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RCL_POSTO"  }))�= 0,'',aAux[var][2])
		::QdroSolict:OrigSl	:= IIF((var := ASCAN(aAux,{|x,y|  Alltrim(x[1]) == "RBT_TIPOR"  }))�= 0,'',aAux[var][2])
                        
	EndIf

	RestArea(aArea)

Return(.T.)

WsMethod InfSolVg WsReceive FilialS,CodSolic WsSend _InfRh3 WsService W0500308

	Local aAux := {}

	aAux := RetInfRh3(::FilialS,::CodSolic)
	
	If Len(aAux) > 0
		
		::_InfRh3:RH3VISAO   := Alltrim(aAux[1])
		::_InfRh3:RH3FILINI  := Alltrim(aAux[2])
		::_InfRh3:RH3MATINI  := Alltrim(aAux[3])
		::_InfRh3:RH3FILAPR  := Alltrim(aAux[4])
		::_InfRh3:RH3MATAPR  := Alltrim(aAux[5])
				
	EndIf
	
Return .T.

WsMethod VerPostSol WsReceive cPosto,cDepto,nOcup,nQntd,cFilP WsSend PossuiPost WsService W0500308

	Local lRet := .T.
	Local cQuery := ""
	Local cAlias1 := GetNextAlias()

	cQuery := "SELECT Count(RH3_CODIGO) QUANT"
	cQuery += "FROM " + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("RH4") + " RH4 ON (RH3.RH3_FILIAL = RH4.RH4_FILIAL AND " 
	cQuery += "RH3.RH3_CODIGO = RH4.RH4_CODIGO AND RH4.D_E_L_E_T_ = ' ' )  "
	cQuery += "INNER JOIN " + RetSqlName("RH4") + " RH4A ON (RH4.RH4_FILIAL = RH4A.RH4_FILIAL AND " 
	cQuery += "RH4.RH4_CODIGO = RH4A.RH4_CODIGO AND RH4A.D_E_L_E_T_ = ' ' )  "
	cQuery += "INNER JOIN " + RetSqlName("RH4") + " RH4B ON (RH4.RH4_FILIAL = RH4B.RH4_FILIAL AND " 
	cQuery += "RH4.RH4_CODIGO = RH4B.RH4_CODIGO AND RH4B.D_E_L_E_T_ = ' ' )  "
	cQuery += "WHERE RH4A.RH4_CAMPO = 'QS_POSTO' AND  "
	cQuery += "RH4A.RH4_VALNOV = '" + ::cPosto + "' AND "
	cQuery += "RH4.RH4_CAMPO = 'RBT_DEPTO' AND  "
	cQuery += "RH4.RH4_VALNOV = '" + ::cDepto + "' AND "
	cQuery += "RH4B.RH4_CAMPO = 'QS_FILPOST' AND  "
	cQuery += "RH4B.RH4_VALNOV = '" + ::cFilP + "' AND "
	cQuery += "RH3.RH3_STATUS IN ('1','4') AND "
	cQuery += "RH3.RH3_XTPCTM = '002' AND  "
	cQuery += "RH3.D_E_L_E_T_ = ' '  "
	
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	If !(cAlias1)->(Eof())
		If ((::nOcup + (cAlias1)->(QUANT)) < ::nQntd)//calculo: (ocupado+solic_existe) menor que quantidade_disponivel
			::PossuiPost := .F.
		Else
			::PossuiPost := .T.
		EndIf		
	Else
		::PossuiPost := .F.
	EndIf

Return .T.

WsMethod VerAprvSit WsReceive FilialAp, MatricAp, cDepto, Visao, EmpFun, TpSit WsSend aAprovador WsService W0500308

	aAprov := FsBscAprov(::FilialAp,::MatricAp,::cDepto,::Visao,::EmpFun,::TpSit)
	
	If Len(aAprov) > 0
		::aAprovador:FilAprov  := aAprov[1][1]
		::aAprovador:MatAprov  := aAprov[1][2]
		::aAprovador:NomeAprov := aAprov[1][3]
		::aAprovador:NvlAprov  := aAprov[1][4]
		::aAprovador:EmpAprov  := aAprov[1][7]
	Else
		::aAprovador:FilAprov  := ""
		::aAprovador:MatAprov  := ""
		::aAprovador:NomeAprov := ""
		::aAprovador:NvlAprov  := 99
		::aAprovador:EmpAprov  := ""
	EndIf

Return .T.

Static Function FsBscAprov(cFilFun, cMatFun, cDepto,cVisao, cEmpFun,cTpSit, cFilSol, cMatSol, cFilLstApr, cMatLstApr)
//cEmployeeFilial, Registration, Department, "2"      , Visao  ,EmployeeEmp, Registration 
//aRet[3]        , aRet[1]     , aRet[8]   , ::TypeOrg, cVision, cEmpAnt, Self:EmployeeSolFil, Self:RegistSolic,cFilLstApr ,cMatLstApr
//cFilFun, cMatFun, cDepto, aDeptos, cTipoOrg, cVisao, cEmpFun, cFilSol, cMatSol, cFilLstApr, cMatLstApr

	Local lCont        	:= .T.
	Local cChave       	:= ""
	Local cTree        	:= ""
	Local cEmpIde		:= ""
	Local cSra			:= "%"+RetFullName("SRA",cEmpFun)+"%"
	Local cSqb			:= "%"+RetFullName("SQB",cEmpFun)+"%"
	Local cRd0			:= "%"+RetFullName("RD0",cEmpFun)+"%"
	Local cRdz			:= "%"+RetFullName("RDZ",cEmpFun)+"%"
	Local cRcx			:= "%"+RetFullName("RCX",cEmpFun)+"%"
	Local cSupAlias    	:= "QSUP"
	Local cAuxAlias    	:= "QAUX"
	Local aSuperior    	:= {}
	Local aArea1 		:= ""
	Local cTOrg			:= ""
	
	DEFAULT cFilFun    	:= ""
	DEFAULT cMatFun    	:= ""
	DEFAULT cDepto     	:= ""
	DEFAULT cVisao     	:= ""
	DEFAULT cEmpFun		:= cEmpAnt
	DEFAULT cFilSol    	:= cFilFun
	DEFAULT cMatSol    	:= cMatFun
	DEFAULT cFilLstApr  := cFilSol
	DEFAULT cMatLstApr  := cMatSol

	If cTpSit == "2"
		cDepto := Alltrim(Posicione("SRA",1,cFilFun+cMatFun,"RA_DEPTO"))
	EndIf

	VerTpOrg(@cTOrg, cVisao)
	
	If cTOrg == "1" //Posto
		
		BeginSql alias cSupAlias
			
			SELECT RD4.RD4_TREE
			FROM %EXP:cRCX% RCX
			INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = RCX.RCX_POSTO AND RCX.RCX_FILIAL = RD4.RD4_FILIDE
			WHERE
			RD4.RD4_FILIAL    = %xfilial:RD4%  AND
			RD4.RD4_CODIGO    = %exp:cVisao%   AND
			RCX.RCX_MATFUN    = %exp:cMatFun%  AND
			RCX.RCX_FILFUN    = %exp:cFilFun%  AND
			RCX.RCX_SUBST     = '2'            AND
			RCX.RCX_TIPOCU    = '1'            AND
			RD4.%notDel%                       AND
			RCX.%notDel%
			
		EndSql
		
		If !(cSupAlias)->(EOF())
		
			cTree := (cSupAlias)->RD4_TREE
			
			aArea1 := GetArea()
			DbSelectArea("RD4")
			RD4->(DbSetOrder(1))
			If RD4->(DbSeek(xFilial("RD4")+cVisao+cTree))
				cEmpIde := RD4->RD4_EMPIDE
			EndIf
			
			If !Empty(cEmpIde)
				cSra	:= "%SRA"+cEmpIde+"0%"
				cRcx	:= "%RCX"+cEmpIde+"0%"
				cRdz	:= "%RDZ"+cEmpIde+"0%"
				cRd0	:= "%RD0"+cEmpIde+"0%"
			Else
				cSra	:= "%"+RetFullName("SRA",cEmpFun)+"%"
				cRcx	:= "%"+RetFullName("RCX",cEmpFun)+"%"
				cRdz	:= "%"+RetFullName("RDZ",cEmpFun)+"%"
				cRd0	:= "%"+RetFullName("RD0",cEmpFun)+"%"
			EndIf
			RestArea(aArea1)
			
			lCont := .T.
			While lCont
						
				BeginSql alias cAuxAlias
					
					SELECT RD4.RD4_CHAVE, RD4.RD4_EMPIDE, RCX.RCX_FILFUN, RCX.RCX_MATFUN, SRA.RA_NOME, SRA.RA_NOMECMP, SRA.RA_CATFUNC, RD0.RD0_NOME, RD4.RD4_TREE, RD4.RD4_CODIDE
					FROM %Exp:cRCX% RCX
					INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = RCX.RCX_POSTO AND RCX.RCX_FILIAL = RD4.RD4_FILIDE
					INNER JOIN %Exp:cSRA% SRA ON SRA.RA_FILIAL = RCX.RCX_FILFUN AND	SRA.RA_MAT = RCX.RCX_MATFUN
					INNER JOIN %Exp:cRDZ% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT
					INNER JOIN %Exp:cRD0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0
					WHERE RD0.RD0_FILIAL = %xfilial:RD0% AND
					RDZ.RDZ_EMPENT    = %exp:cEmpIde%     AND
					RDZ.RDZ_FILIAL    = %xfilial:RDZ%  AND
					SRA.RA_SITFOLH    <> 'D'           AND
					RD4.RD4_FILIAL    = %xfilial:RD4%  AND
					RD4.RD4_CODIGO    = %exp:cVisao%   AND
					RD4.RD4_ITEM      = %exp:cTree%   AND
					RCX.RCX_SUBST     = '2'            AND
					RCX.RCX_TIPOCU    = '1'            AND
					RD0.%notdel%						 AND
					RDZ.%notdel%						 AND
					SRA.%notdel%						 AND
					RD4.%notDel%                       AND
					RCX.%notDel%
					
				EndSql
				
				If (cAuxAlias)->( !Eof() )
					If FsVldNotf(cVisao,(cAuxAlias)->RD4_CHAVE)
						lCont := .T.
						cTree := (cAuxAlias)->RD4_TREE
						cFilSol := (cAuxAlias)->RCX_FILFUN
						cMatSol := (cAuxAlias)->RCX_MATFUN
						cPosto := (cAuxAlias)->RD4_CODIDE
						(cAuxAlias)->(DbCloseArea())
							//Envia a notifica��o por e-mail
						FSEnvNotf(cFilSol,cMatSol,"Posto "+cPosto)
						cAuxAlias := "QAUX"
					Else
						lCont := .F.
						aAdd( aSuperior, { (cAuxAlias)->RCX_FILFUN,;
							(cAuxAlias)->RCX_MATFUN   ,;
							if(! Empty((cAuxAlias)->RA_NOMECMP),(cAuxAlias)->RA_NOMECMP,If(! Empty((cAuxAlias)->RD0_NOME),(cAuxAlias)->RD0_NOME,(cAuxAlias)->RA_NOME)),;
							(len(Alltrim((cAuxAlias)->RD4_CHAVE))/3)-1,;
							(cAuxAlias)->RD4_CHAVE,;
							If(!Empty((cAuxAlias)->RA_NOME),(cAuxAlias)->RA_CATFUNC,""),(cAuxAlias)->RD4_EMPIDE;
							})
					EndIf
				Else
					lCont := .F.
				EndIf
			
			End
			(cAuxAlias)->( DbCloseArea() )
		EndIf
		(cSupAlias)->(dbCloseArea())
	
	ElseIf cTOrg == "2" //Departamento
	
		BeginSql alias cSupAlias
				
			SELECT SQB.QB_FILRESP, SQB.QB_MATRESP, SRA.RA_NOME, SRA.RA_NOMECMP, SRA.RA_CATFUNC, RD0.RD0_NOME, RD4.RD4_TREE, RD4.RD4_CHAVE, RD4.RD4_EMPIDE
			FROM %Exp:cSqb% SQB
			INNER JOIN %Exp:cSra% SRA ON SQB.QB_FILRESP = SRA.RA_FILIAL AND SQB.QB_MATRESP = SRA.RA_MAT
			INNER JOIN %Exp:cRdz% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT
			INNER JOIN %Exp:cRd0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0
			INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = SQB.QB_DEPTO AND RD4_EMPIDE=%exp:cEmpFun%
			WHERE RD4.RD4_CODIGO = %exp:cVisao% AND
			RD4.RD4_FILIAL = %xfilial:RD4% AND
			RD0.RD0_FILIAL = %xfilial:RD0% AND
			RDZ.RDZ_FILIAL = %xfilial:RDZ% AND
			RDZ.RDZ_EMPENT = %exp:cEmpFun% AND
			SQB.QB_DEPTO   = %exp:cDepto% AND
			SQB.QB_FILIAL  = %exp:xFilial("SQB", cFilFun)% AND
			RD4.%notDel% AND
			RD0.%notdel% AND
			RDZ.%notdel% AND
			SRA.%notDel% AND
			SQB.%notDel%
		EndSql
			
		If (cSupAlias)->( !Eof() )
			
			cTree := (cSupAlias)->RD4_TREE
					
			aArea1 := GetArea()
			dbSelectArea("RD4")
			RD4->(dbSetOrder(1))
			If RD4->(dbSeek(xFilial("RD4")+cVisao+cTree))
				cEmpIde := RD4->RD4_EMPIDE
			EndIf
				
			If !Empty(cEmpIde)
				cSra := "%SRA"+cEmpIde+"0%"
				cSqb := "%SQB"+cEmpIde+"0%"
				cRdz := "%RDZ"+cEmpIde+"0%"
				cRd0 := "%RD0"+cEmpIde+"0%"
			Else
				cSra := "%"+RetFullName("SRA",cEmpFun)+"%"
				cSqb := "%"+RetFullName("SQB",cEmpFun)+"%"
				cRdz := "%"+RetFullName("RDZ",cEmpFun)+"%"
				cRd0 := "%"+RetFullName("RD0",cEmpFun)+"%"
			EndIf
					
			RestArea(aArea1)
			lCont := .T.
			While lCont
						
				BeginSql alias cAuxAlias
						
					SELECT SRA.RA_FILIAL, SRA.RA_MAT, SRA.RA_NOME, SRA.RA_NOMECMP, SRA.RA_CATFUNC, RD0.RD0_NOME, RD4.RD4_CHAVE, RD4.RD4_TREE, RD4.RD4_EMPIDE, RD4.RD4_CODIDE
					FROM %table:RD4% RD4
					INNER JOIN %Exp:cSqb% SQB ON SQB.QB_DEPTO = RD4.RD4_CODIDE AND SQB.QB_FILIAL = RD4.RD4_FILIDE AND SQB.%notDel%
					INNER JOIN %Exp:cSra% SRA ON SQB.QB_FILRESP = SRA.RA_FILIAL AND SQB.QB_MATRESP = SRA.RA_MAT AND SRA.%notDel%
					LEFT JOIN %Exp:cRdz% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT AND RDZ.RDZ_FILIAL = %xfilial:RDZ% AND
					RDZ.RDZ_EMPENT = %exp:cEmpIde% AND RDZ.%notdel%
					LEFT JOIN %Exp:cRd0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0 AND RD0.RD0_FILIAL = %xfilial:RD0% AND RD0.%notdel%
					WHERE RD4.RD4_ITEM   = %exp:cTree%  AND
					RD4.RD4_CODIGO = %exp:cVisao% AND
					RD4.RD4_FILIAL = %xfilial:RD4% AND
					RD4.%notDel%
							
				EndSql
	
				If (cAuxAlias)->( Eof() )
					
					lCont := .F.
							
				ElseIf !( (cAuxAlias)->RA_FILIAL == cFilSol .and. (cAuxAlias)->RA_MAT == cMatSol )
					
					If FsVldNotf(cVisao,(cAuxAlias)->RD4_CHAVE)
						lCont := .T.
						cTree := (cAuxAlias)->RD4_TREE
						cFilSol := (cAuxAlias)->RA_FILIAL
						cMatSol := (cAuxAlias)->RA_MAT
						cDepto := (cAuxAlias)->RD4_CODIDE
						(cAuxAlias)->(DbCloseArea())
							//Envia a notifica��o por e-mail
						FSEnvNotf(cFilSol,cMatSol,"Departamento "+cDepto)
						cAuxAlias := "QAUX"
					Else
						lCont := .F.
						aAdd( aSuperior, { (cAuxAlias)->RA_FILIAL,;
							(cAuxAlias)->RA_MAT   ,;
							iIf(! Empty((cAuxAlias)->RA_NOMECMP),(cAuxAlias)->RA_NOMECMP,iIf(!Empty((cAuxAlias)->RD0_NOME),(cAuxAlias)->RD0_NOME,(cAuxAlias)->RA_NOME)),;
							(len(Alltrim((cAuxAlias)->RD4_CHAVE))/3)-1,;
							(cAuxAlias)->RD4_CHAVE,;
							iIf(!Empty((cAuxAlias)->RA_NOME),(cAuxAlias)->RA_CATFUNC,""),(cAuxAlias)->RD4_EMPIDE;
							})
					EndIf
							
				Else
					
					cTree := (cAuxAlias)->RD4_TREE
					lCont := .F.
					
				EndIf
			EndDo
			(cAuxAlias)->( DbCloseArea() )
		EndIf
		(cSupAlias)->( dbCloseArea() )
	EndIf

Return aSuperior

Static Function FsVldNotf(cVisao,cChave)

	Local cQuery 	:= ""
	Local cAlias1	:= GetNextAlias()
	Local lRet		:= .F.
	
	cQuery := "SELECT RD4_CODIGO, RD4_CHAVE, RD4_CODIDE, RD4_XAPNT "
	cQuery += "FROM	" + RetSqlName("RD4") + " "
	cQuery += "WHERE RD4_CODIGO = '" + cVisao + "' AND RD4_CHAVE = '" + cChave + "' AND "
	cQuery += "D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	If (cAlias1)->(!EOF())
		If (cAlias1)->RD4_XAPNT = '2'
			lRet := .T.
		EndIf
	EndIf

	(cAlias1)->( DbCloseArea() )

Return lRet

Static Function RetInfRh3(cFilSol,cCodSol)

	Local cQuery 	:= ''
	Local cAliRh3	:= 'RHPA3INF'
	Local aAux		:= {}
	
	cQuery := "SELECT RH3_VISAO, RH3_FILINI, RH3_MATINI,RH3_FILAPR, RH3_MATAPR "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_FILIAL = '" + cFilSol + "' AND "
	cQuery += "RH3_CODIGO = '" + cCodSol + "' AND D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRh3)
	
	DbSelectArea(cAliRh3)
	While !(cAliRh3)->(EOF())
		AADD(aAux, (cAliRh3)->RH3_VISAO)
		AADD(aAux, (cAliRh3)->RH3_FILINI)
		AADD(aAux, (cAliRh3)->RH3_MATINI)
		AADD(aAux, (cAliRh3)->RH3_FILAPR)
		AADD(aAux, (cAliRh3)->RH3_MATAPR)
		(cAliRh3)->(DbSkip())
	End
	(cAliRh3)->(DbCloseArea())

Return aAux

Static Function PrpEnvMail(cFilMat,cMatApr,nOper,cSol,cVisao,cMatIni,cFilIni,cObs)

	Local aArea := GetArea()
	Local cAlias1 := GetNextAlias()
	Local cQuery := ""
	Local cAssunto 	:= ""
	Local cDesSol	:= ""
	Local cBody := ""
	Local nX := 0
	Local aEmails := {}
	Local cEmail := ""
	
	Default cObs := ""

	If cSol == "1"
		cAssunto := "Solicita��o de Vagas"
		cDesSol := "vaga"
	Else
		cAssunto := "Solicita��o de Aumento de Quadro\Or�amento"
		cDesSol := "aumento de quadro\or�amento"
	EndIf

	If nOper !=4
	
		cQuery := "SELECT RA_EMAIL FROM " + RetSqlName("SRA") + " "
		cQuery += "WHERE RA_FILIAL = '" + cFilMat + "' AND RA_MAT = '" + cMatApr + "' "
		cQuery += " AND D_E_L_E_T_ = ' ' "
		
		cQuery := ChangeQuery(cQuery)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
		
		cEmail := Alltrim((cAlias1)->RA_EMAIL)
		
		If !Empty(cEmail)
		
			cBody := '<html><body><pre>'+CRLF
			cBody += '<b>Prezado,</b>'+CRLF
			cBody += "Solicita��o de " + cDesSol + " aguardando sua aprova��o/reprova��o no portal de Rh."+CRLF+CRLF
			cBody += "Obs.: " + cObs + " " + CRLF
			cBody += '</pre></body></html>'
			
			U_F0200304(cAssunto,cBody,cEmail) //Envia o e-mail
			
		EndIf
	
	Else
	
		aEmails := FsRetEmls(cFilMat,cMatApr,cVisao,cFilIni,cMatIni)

		cBody := '<html><body><pre>'+CRLF
		cBody += '<b>Prezado,</b>'+CRLF
		cBody += "Solicita��o de " + cDesSol + " foi reprovada."+CRLF
		cBody += '</pre></body></html>'
		
		For nX := 1 To Len(aEmails)
			U_F0200304(cAssunto, cBody,aEmails[nX]) //Envia o e-mail
		Next nX
	
	EndIf

	RestArea(aArea)

Return .T.

Static Function FsRetEmls(cFilMat,cMatApr,cVisao,cFilIni,cMatIni)

	Local aArea := GetArea()
	Local cQuery := ""
	Local cAlias1 := GetNextAlias()
	Local aRet := {}

	cQuery := "SELECT SQB.QB_MATRESP,SRA.RA_NOME,SRA.RA_EMAIL,RD4.RD4_TREE,RD4.RD4_CHAVE "
	cQuery += "FROM " + RetSqlName("RD4") + " RD4," + RetSqlName("SQB") + " SQB," + RetSqlName("SRA") + " SRA "
	cQuery += "WHERE RD4.D_E_L_E_T_ = ' ' "
	cQuery += " AND SQB.QB_DEPTO = RD4.RD4_CODIDE"
	cQuery += " AND SQB.QB_FILRESP = SRA.RA_FILIAL"
	cQuery += " AND SQB.QB_MATRESP = SRA.RA_MAT"
	cQuery += " AND RD4.RD4_TREE >= "
	cQuery += " (SELECT RD4.RD4_TREE FROM " + RetSqlName("RD4") + " RD4"
	cQuery += " WHERE RD4.RD4_CODIDE = (SELECT QB_DEPTO FROM " + RetSqlName("SQB") + " "
	cQuery += " WHERE QB_FILRESP = '" + cFilMat + "' AND QB_MATRESP = '" + cMatApr + "')"
	cQuery += " AND RD4.RD4_CODIGO = '" + cVisao + "' AND RD4.D_E_L_E_T_ = ' ' )"
	cQuery += " AND RD4.RD4_TREE <= "
	cQuery += " (SELECT RD4.RD4_TREE FROM " + RetSqlName("RD4") + " RD4"
	cQuery += " WHERE RD4.RD4_CODIDE = (SELECT QB_DEPTO FROM " + RetSqlName("SQB") + " "
	cQuery += " WHERE QB_MATRESP = '" + cMatIni + "') "
	cQuery += " AND RD4.RD4_CODIGO = '" + cVisao + "' AND RD4.D_E_L_E_T_ = ' ' )"
	cQuery += " AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "ORDER BY RD4_CHAVE "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	DbSelectArea(cAlias1)
	cChave := ALLTRIM((cAlias1)->(RD4_CHAVE))
	While !(cAlias1)->(EOF())
		If Len(ALLTRIM((cAlias1)->(RD4_CHAVE))) >= Len(cChave)
			AADD(aRet, {Alltrim((cAlias1)->(RA_EMAIL))})
		EndIf
		cChave := ALLTRIM((cAlias1)->(RD4_CHAVE))
		(cAlias1)->(DbSkip())
	End
	(cAlias1)->(DbCloseArea())
	
	RestArea(aArea)

Return aRet

Static Function FSEnvNotf(cFilMat,cMatApr,cTpVis)

	Local aArea := GetArea()
	Local cEmail := ""
	Local cAssunto := "Solicita��o"
	
	cEmail := Alltrim(Posicione("SRA",1,cFilMat+cMatApr,"RA_EMAIL"))

	If !Empty(cEmail)
	
		cBody := '<html><body><pre>'+CRLF
		cBody += '<b>Prezado,</b>'+CRLF
		cBody += "Para seu conhecimento, foi realizado uma solicita��o para o " + cTpVis + " no portal de Rh."+CRLF
		cBody += '</pre></body></html>'
	
		U_F0200304(cAssunto,cBody,cEmail)
	
	EndIf

	RestArea(aArea)

Return

Static Function FsEnvRh()

	Local aArea := GetArea()
	Local cAssunto := "Aguardando efetiva��o RH"
	Local cEmails := Alltrim(SuperGetMv("FS_EFTVRH"))

	cBody := '<html><body><pre>'+CRLF
	cBody += "Existem solicita��es para analise do RH, por favor acessar pelo ERP "+CRLF
	cBody += '</pre></body></html>'
	
	U_F0200304(cAssunto, cBody, cEmails)

	RestArea(aArea)

Return

WsMethod InfFxSl WsReceive CODVAGA, cFilP WsSend _FxSal WsService W0500308

	Local aDados:= {}
	Local nY    := 0
	Local oARVg
	
	aDados := BscFxSal(::CODVAGA,::cFilP)
	
	If Len(aDados) > 0
	
		::_FxSal := WSClassNew( "_FaixSal" )
		::_FxSal:Registro := {}
		
		oARVg :=  WSClassNew( "FaixSal" )
		For nY := 1 To Len(aDados)
			oARVg:NIVEL 	:= aDados[nY][1]
			oARVg:FAIXA	:= aDados[nY][2]
			oARVg:VALOR 	:= cValToChar(aDados[nY][3])
			AAdd( ::_FxSal:Registro, oARVg )
			oARVg :=  WSClassNew( "FaixSal" )
		Next nY
		
	EndIf

Return .T.

Static Function BscFxSal(cCodVg, cFilP)

	Local cQuery 	:= ''
	Local cAliRB6	:= 'INFRB6'
	Local aAux		:= {}
	
	cQuery := "SELECT	RB6_VALOR,RB6_FAIXA, RB6_NIVEL "
	cQuery += "FROM	" + RetSqlName("RB6") + " "
	cQuery += "WHERE	RB6_FILIAL = '" + cFilP + "' AND "
	cQuery += "		RB6_TABELA = (SELECT Q3_TABELA FROM " + RetSqlName("SQ3") + " "
	cQuery += "						WHERE Q3_CARGO = '" + cCodVg + "' AND D_E_L_E_T_ = ' ') AND "
	cQuery += "   	RB6_NIVEL = (SELECT Q3_TABNIVE FROM " + RetSqlName("SQ3") + " "
	cQuery += "						WHERE Q3_CARGO = '" + cCodVg + "' AND D_E_L_E_T_ = ' ') "
	cQuery += "		AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRB6)
	
	DbSelectArea(cAliRB6)
	While ! (cAliRB6)->(EOF())
		AADD(aAux, {(cAliRB6)->RB6_NIVEL,(cAliRB6)->RB6_FAIXA,(cAliRB6)->RB6_VALOR})
		(cAliRB6)->(DbSkip())
	End
	(cAliRB6)->(DbCloseArea())

Return aAux

WsMethod PegMnem WsReceive lRet WsSend cRet WsService W0500308
	
	::cRet := ALLTRIM(POSICIONE("RCA",1,XFILIAL("RCA") + 'M_PERCTOL', "RCA_CONTEU"))
	
Return .T.

WsMethod VgDescDet WsReceive _IdVaga WsSend cRet WsService W0500308
	
	Local cChave := ""
	cChave := POSICIONE("SQ3",1,Fwxfilial("SQ3") + Self:_IdVaga:Q3CARGO + Self:_IdVaga:Q3CC,"Q3_DESCDET")
	If EMPTY(cChave)
		cChave := POSICIONE("SQ3",1,Fwxfilial("SQ3") + Self:_IdVaga:Q3CARGO,"Q3_DESCDET")
	EndIf
	::cRet := MSMM(cChave,,,,3,,,"SQ3","Q3_MEMO1")
Return .T.

WsMethod GetDepto WsReceive FilialAp,MatricAp WsSend cDepto WsService W0500308

	Local cQuery 	:= ''
	Local cAlias1	:= GetNextAlias()
	
	cQuery := "SELECT QB_DEPTO "
	cQuery += "FROM	" + RetSqlName("SQB") + " "
	cQuery += "WHERE QB_FILRESP = '" + FilialAp + "' AND QB_MATRESP = '" + MatricAp + "' AND "
	cQuery += "D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	::cDepto := (cAlias1)->QB_DEPTO
	
	(cAlias1)->(DbCloseArea())

Return(.T.)

Static Function VerTpOrg(cTOrg,cVisao)

	Local aArea := GetArea()
	
	If Empty(cTOrg)
		If Empty(cVisao)
			cTOrg := "0"
		Else
			DbSelectArea("RDK")
			If RDK->(DbSeek(xFilial("RDK")+cVisao))
				
				//RDK->RDK_HIERAR -> 1=Organizacional(Departamento);2=Comunica��o(Posto)                                            
				If RDK->RDK_HIERAR == "1"
					cTOrg := "2" //departamento
				Else
					cTOrg := "1" //posto
				EndIf
			Else
				Return .F.
			EndIf
		EndIf
	EndIf
	
	RestArea(aArea)

Return