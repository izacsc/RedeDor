#INCLUDE 'protheus.ch'
#INCLUDE 'parmtype.ch'
#INCLUDE "Topconn.ch"
/*
{Protheus.doc} F0200601
Relatorio custos emergenciais
@Project MAN00000463301_EF_006
@author bruno.aferreira
@since 07/10/2016
@version 12.1.7
*/
User Function F0200601() // U_F0200601()
	Local oReport := nil
	//Private aDadRel := {}
	Private cPerg	  := "FSW0200601"
	FSW0200601()
	//Chama a pergunta
	If Pergunte(cPerg, .T.)
		//Monta estrutura do report
		oReport := RptDef(cPerg)
		//aDadRel := DADOSEF06()
		aPrintRel := DADOSEF06() //Aclone(aDadRel)
		oReport:PrintDialog()
	EndIf
Return

/*{Protheus.doc} ReportPrint
//TODO Impress�o do relatorio
@Project MAN00000463301_EF_006 001
@author bruno.aferreira
@since 07/10/2016
@version 12.1.7
@param oReport, object, estrutura criada pela funcao RPTDEF
@type function
*/
Static Function ReportPrint(oReport)
	
	Local nx:=ny:=0
	Local oSection1 := oReport:Section(1)
	
	oSection1:Init()
	For nx:=1 to Len(aPrintRel)
		oSection1:Cell("FILIAL")   :SetValue(aPrintRel[nx,01])
		oSection1:Cell("DATAEMI")  :SetValue( Substr(aPrintRel[nx,02],7,2)+"/"+Substr(aPrintRel[nx,02],5,2)+"/"+Substr(aPrintRel[nx,02],3,2) )
		oSection1:Cell("CODIGO")   :SetValue(aPrintRel[nx,04])
		oSection1:Cell("DESC")     :SetValue(aPrintRel[nx,05])
		oSection1:Cell("COMPRADOR"):SetValue(aPrintRel[nx,06])
		oSection1:Cell("SOLCOMPRA"):SetValue(aPrintRel[nx,07])
		oSection1:Cell("ORDCOMPRA"):SetValue(aPrintRel[nx,08])
		oSection1:Cell("QNTPED")   :SetValue(aPrintRel[nx,09])
		oSection1:Cell("FORNEOC")  :SetValue(aPrintRel[nx,11])
		oSection1:Cell("MODALID")  :SetValue(aPrintRel[nx,16])
		If aPrintRel[nx,16] == 'CONTRATO'
			oSection1:Cell("VALUNIT"):SetValue(aPrintRel[nx,17]) //VALOR UNITARIO  RETORNADO DO  CONTRATO
			oSection1:Cell("TOTCONTR"):SetValue(aPrintRel[nx,17]*aPrintRel[nx,9])
		Else //NESSE CASO SERA "== MERCADO"
			oSection1:Cell("VALUNIT"):SetValue( (aPrintRel[nx,18]) ) // AQUI SERA O MENOR   VALOR DA COTACAO ENCONTRADO
			oSection1:Cell("TOTCONTR"):SetValue((aPrintRel[nx,18])* aPrintRel[nx,9])
		EndIf
		oSection1:Cell("VALEMERG"):SetValue(aPrintRel[nx,14])
		oSection1:Cell("TOTEMERG"):SetValue(aPrintRel[nx,14]*aPrintRel[nx,9])
		oSection1:Cell("VARIACAO"):SetValue((oSection1:Cell("TOTEMERG"):GetValue())-(oSection1:Cell("TOTCONTR"):GetValue()))
		oSection1:Cell("JUSTIFIC"):SetValue(aPrintRel[nx,19])
		oSection1:Cell("ANALISTA"):SetValue(aPrintRel[nx,20])
		oSection1:Printline()
		oReport:ThinLine()
	next nx
	oSection1:Finish()
Return

/*{Protheus.doc} RptDef
//TODO Montagem da estrutura do relatorio
@Project MAN00000463301_EF_006 001
@author bruno.aferreira
@since 07/10/2016
@version undefined
@param cNome, characters, Nome definido para o pergunte
@type function
*/

Static Function RptDef(cNome)
	
	Local oReport := Nil
	Local oSection1:= Nil
	Local oBreak
	Local oFunction
	
	oReport := TReport():New(cNome,"Relat�rio Custo Emergencial",cNome,{|oReport| ReportPrint(oReport)},"Mostra o custo emergencial")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	
	oSection1:= TRSection():New(oReport, "COTACOES", {"SB8"}, , .F., .T.)
	TRCell():New(oSection1,"FILIAL"	  ,"QRY","Filial"  		  ,"@!",20,.T.) 
	TRCell():New(oSection1,"DATAEMI"  ,"QRY","Dt Emissao"	  ,    ,30,.T.)
	TRCell():New(oSection1,"CODIGO"	  ,"QRY","Material"  	  ,"@!",50,.T.)
	TRCell():New(oSection1,"DESC"     ,"QRY","Descri�ao"	  ,"@!",45,.F.)
	TRCell():New(oSection1,"COMPRADOR","QRY","Comprador"	  ,"@!",25,.T.)
	TRCell():New(oSection1,"SOLCOMPRA","QRY","S. Compra"      ,"@!",25,.T.)
	TRCell():New(oSection1,"ORDCOMPRA","QRY","O. Compra"	  ,"@!",25,.T.)
	TRCell():New(oSection1,"QNTPED"	  ,"QRY","Qtd. Pedido" 	  ,"@!",25,.T.)
	TRCell():New(oSection1,"MOTIVO"	  ,"QRY","Motivo"		  ,"@!",25,.T.)
	TRCell():New(oSection1,"FORNEOC"  ,"QRY","Forn. OC"		  ,"@!",45,.T.)
	TRCell():New(oSection1,"MODALID"  ,"QRY","Modalidade"  	  ,"@!",30,.T.)
	TRCell():New(oSection1,"VALUNIT"  ,"QRY","Vl. Un Cont/Mer","@E 999,999,999.99",30)
	TRCell():New(oSection1,"TOTCONTR" ,"QRY","Tot Cont/Mer"   ,"@E 999,999,999.99",30)
	TRCell():New(oSection1,"VALEMERG" ,"QRY","Val. Un Emerg." ,"@E 999,999,999.99",30)
	TRCell():New(oSection1,"TOTEMERG" ,"QRY","Tot. Emerg."    ,"@E 999,999,999.99",30)
	TRCell():New(oSection1,"VARIACAO" ,"QRY","Varia�ao"		  ,"@E 999,999,999.99",30,.T.)
	TRCell():New(oSection1,"JUSTIFIC" ,"QRY","Justific."	  ,"@!",20,.T.)
	TRCell():New(oSection1,"ANALISTA" ,"QRY","Analista"	      ,"@!",40,.T.)
	
Return(oReport)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �DADOSEF06 �Autor  � Bruno Ferreira     � Data � 30/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc.     �Busca as informacoes no banco, jogando em um array que 	  ���
���          �  e retornado pela funcao                                   ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
/* {Protheus.doc} DADOSEF06
//TODO Descri��o auto-gerada.
@Project MAN00000463301_EF_006 001
@author bruno.aferreira
@since 10/10/2016
@version undefined
@type function
*/
Static Function DADOSEF06()
	Local nCont := 0
	Local cTesteFil
	Local cBanco := If(Trim(TcGetDb()) == 'ORACLE',.T.,.F.)
	Local cProdIni := Space(TamSX3("B1_COD")[1])
	Local cProdFim := Space(TamSX3("B1_COD")[1])
	Local cFilIni := Space(TamSX3("C7_FILIAL")[1])
	Local cFilFim := Space(TamSX3("C7_FILIAL")[1])
	Local aAux		:= {}
	Local nX		:= 1
	Private aRel01:={}
	Private cProdRet,cFilRet
	
	//Tratativa dos parametros para passar para query
	cFilRet:= Alltrim(MV_PAR01)
	cProdRet:= Alltrim(MV_PAR04)
	
	If at('-',cFilRet) > 0
		cFilRet := StrTran(cFilRet,";","")
		cFilIni := Substr(cFilRet,1, at('-', cFilRet) - 1)
		cFilFim := Substr(cFilRet,at('-', cFilRet) + 1,TamSX3("C7_FILIAL")[1] )
	Else
		aAux := StrtoKarr(cFilRet,';')
		cFilRet:= ""
		For nX := 1 To Len(aAux)
			If Alltrim(aAux[nX]) != ""
				cFilRet += "'"+aAux[nX]+"',"
			Endif
		Next
		If Right(AllTrim(cFilRet),1) == ','
			cFilRet := SubStr(AllTrim(cFilRet),1,Len(AllTrim(cFilRet))-1)
		Endif
	EndIf
	aAux := {}
	If at('-',cProdRet) > 0
		cProdRet:= StrTran(cProdRet,";","")
		cProdIni := Substr(cProdRet,1, at('-', cProdRet) - 1)
		cProdFim := Substr(cProdRet,at('-', cProdRet) + 1,TamSX3("B1_COD")[1] )
	Else
		aAux := StrtoKarr(cProdRet,';')
		cProdRet:= ""
		For nX := 1 To Len(aAux)
			If Alltrim(aAux[nX]) != ""
				cProdRet += "'"+aAux[nX]+"',"
			Endif
		Next
		If Right(AllTrim(cProdRet),1) == ','
			cProdRet := SubStr(AllTrim(cProdRet),1,Len(AllTrim(cProdRet))-1)
		Endif
	EndIf
	//PEGO OS PARAMETROS INFORMADOS E ENTAO PEGO TODAS OS PEDS  RELACIONADOS AS COTACOES DO PERIODO DESEJADO
	cQuery1:= " SELECT C7_FILIAL FILIAL, C8_EMISSAO EMISSAOCOT, C8_NUM NUMCOT, C7_PRODUTO PRODUTO, C7_DESCRI DESCRICAO, "+CRLF
	cQuery1+= " C7_COMPRA COMPRADOR, C7_NUMSC NUMSC, C7_NUM NUMPED, C7_QUANT QUANTPED, C8_FORNECE FORNECE, A2_NOME NOMEFOR, "+CRLF
	cQuery1+= " C7_LOJA LOJA, C7_PRECO PRECOUNIT, C7_TOTAL TOTAL, C7_EMISSAO EMISSAOPED, C8_MOTIVO MOTIVO, ISNULL(B2_CM1 ,0) B2_CM1 "+CRLF
	cQuery1+= " FROM "+Retsqlname("SC7")+" C7 "+CRLF
	cQuery1+= " INNER JOIN "+Retsqlname("SC8")+" C8 ON C7_FILIAL = C8_FILIAL AND C8.D_E_L_E_T_ = '' AND C7_NUM = C8_NUMPED AND C7_ITEM = C8_ITEMPED "+CRLF
	cQuery1+= " INNER JOIN "+Retsqlname("SA2")+" A2 ON A2_FILIAL = '"+xFilial("SA2")+"' AND A2.D_E_L_E_T_ = '' AND A2_COD = C7_FORNECE AND A2_LOJA = C7_LOJA "+CRLF
	cQuery1+= " LEFT OUTER JOIN "+Retsqlname("SB2")+" B2 ON B2_FILIAL = C7_FILIAL AND B2.D_E_L_E_T_ = '' AND B2_COD = C7_PRODUTO AND B2_LOCAL = C7_LOCAL "+CRLF	
	cQuery1+= " WHERE C7.D_E_L_E_T_ = '' "+CRLF
	cQuery1+= " AND C7_EMISSAO BETWEEN '"+DTOS(MV_PAR02)+"' AND '"+DTOS(MV_PAR03)+"' "+CRLF
	If !Empty(cProdRet)
	    If Empty(cProdIni)
			cQuery1+="AND C7_PRODUTO IN ("+cProdRet+")"+CRLF
		Else
			cQuery1+="AND C7_PRODUTO BETWEEN '"+cProdIni+"' AND '"+cProdFim+"' "+CRLF
		EndIf
	Endif
	If !Empty(cFilRet)
	    If Empty(cFilIni)
			cQuery1+= "AND C7_FILIAL IN ("+cFilRet+")"+CRLF
		Else
			cQuery1+="AND C7_FILIAL BETWEEN '"+cFiLIni+"' AND '"+cFilFim+"' "+CRLF
		EndIf                               
	Endif
	cQuery1 := ChangeQuery(cQuery1)
	
	If Select("QRY1") > 0
		Dbselectarea("QRY1")
		QRY1->(DbClosearea())
	Endif
	
	TcQuery cQuery1 New Alias "QRY1"
	dbSelectArea("QRY1")
	dbGotop()
	
	While QRY1->(!Eof())
		//ARRAY MONTADO  COM AS INFORMACOES QUE SERAO IMPRESSAS Substr(QRY1->EMISSAOCOT,7,2)+"/"+Substr(QRY1->EMISSAOCOT,5,2)+"/"+Substr(QRY1->EMISSAOCOT,3,2),;
		aAdd(aRel01,{QRY1->FILIAL,;		//01  
			         QRY1->EMISSAOCOT,;
			         QRY1->NUMCOT,;
			         QRY1->PRODUTO,;
			         QRY1->DESCRICAO,;	//05
			         QRY1->COMPRADOR,; 
			         QRY1->NUMSC,; 
			         QRY1->NUMPED,;	
			         QRY1->QUANTPED,;
			         QRY1->FORNECE,;	//10
			         QRY1->NOMEFOR,; 
			         QRY1->LOJA,;
			         QRY1->EMISSAOPED,; //15
			         QRY1->PRECOUNIT,; 
			         QRY1->TOTAL,;
			         'MERCADO',;
			         '',; 
			         '',;
			         QRY1->MOTIVO,;
			         ''})				//20 
		nCont ++
		//POSICIONO NA SC1 PARA  PEGAR O NOME DO SOLICITANTE E ADICIONO NO ARRAY ( DESCARTADO C7_SOLICIT)
		aArea = GetArea()
		aAreaSC1 = SC1->(GetArea())
		DbSelectArea("SC1")
		SC1->(DbSetOrder(1)) // C1_FILIAL+C1_NUM
		SC1->(DbGoTop())
		
		If SC1->(DbSeek(aRel01[nCont, 1] + aRel01[nCont, 7]))
			aRel01[nCont, 20] := TRIM(SC1->C1_SOLICIT)
		EndIf
		
		DbCloseArea()
		RestArea(aArea) 
		RestArea(aAreaSC1)
		//FEITO A ANALISE DAS COTACOES QUE QUERO, IREI VERIFICAR SE TENHO CONTRATO OU NAO PARA CADA CASO, CASO SIM SELECIONO O MAIS ATUAL
		//COM O TOP1  E O DESC
		If cBanco
			cQuery2 := "SELECT * FROM (SELECT "
		Else
			cQuery2 := " SELECT TOP 1 "
		Endif
		cQuery2 += " CNA_FORNEC FORNEC, CNA_LJFORN LOJAFORN, CN9_FILIAL FILIAL, CNB_ITEM ITEM, CNB_PRODUT PRODUTO, "+CRLF
		cQuery2 += " CNB_VLUNIT VALUNIT, CNA_NUMERO PLANILHA, CNA_CONTRA CONTRATO, CN9_DTINIC DTINI, CN9_DTFIM DTFIM "+CRLF
		cQuery2 += " FROM "+Retsqlname("CN9")+" PAI "+CRLF
		cQuery2 += " INNER JOIN "+Retsqlname("CNA")+" FILHO "+CRLF
		cQuery2 += " ON PAI.CN9_FILIAL = FILHO.CNA_FILIAL "+CRLF
		cQuery2 += " AND PAI.CN9_NUMERO = FILHO.CNA_CONTRA "+CRLF
		cQuery2 += " AND CN9_REVISA = CNA_REVISA "+CRLF
		cQuery2 += " INNER JOIN "+Retsqlname("CNB")+" NETO "+CRLF
		cQuery2 += " ON FILHO.CNA_FILIAL = NETO.CNB_FILIAL "+CRLF
		cQuery2 += " AND FILHO.CNA_CONTRA = NETO.CNB_CONTRA "+CRLF
		cQuery2 += " AND FILHO.CNA_REVISA  = NETO.CNB_REVISA "+CRLF
		cQuery2 += " AND FILHO.CNA_NUMERO = NETO.CNB_NUMERO "+CRLF
		cQuery2 += " WHERE PAI.D_E_L_E_T_ = '' AND FILHO.D_E_L_E_T_ = '' AND NETO.D_E_L_E_T_ = '' "+CRLF
		cQuery2 += " AND CN9_FILIAL = '"+aRel01[ncont,1]+"'"+CRLF
		cQuery2 += " AND CNA_FORNEC = '"+aRel01[ncont,10]+"' "+CRLF
		cQuery2 += " AND CNB_PRODUT = '"+aRel01[ncont,4]+"'"+CRLF
		cQuery2 += " AND "+aRel01[ncont,2]+" BETWEEN CN9_DTINIC AND CN9_DTFIM "+CRLF //procuro contrato vigente para a data de emissao da cotacao
		cQuery2 += " ORDER BY CN9_DTFIM DESC "+CRLF
		
		If cBanco
			cQuery2 += ")WHERE ROWNUM <= 1 "
		Endif
		cQuery2 := ChangeQuery(cQuery2)
		
		If Select("QRY2") > 0
			Dbselectarea("QRY2")
			QRY2->(DbClosearea())
		Endif
	
		TcQuery cQuery2 New Alias "QRY2"
		dbSelectArea("QRY2")
		dbGotop()
		If QRY2->(!Eof()) //CASO ENCONTRE CONTRATO CONTRATO, ADD O PRECO CONTRATO E ALTERO O CAMPO MODALIDADE, CASO CONTRARIO BUSCO A MENOR COTACAO
			aRel01[nCont, 16] := 'CONTRATO' //MODALIDADE
			aRel01[nCont, 17] := QRY2->VALUNIT //VALOR DO  CONTRATO
			QRY2->(dbSkip())
		Else
			If  QRY1->B2_CM1 > 0 //VALOR DO CUSTO MEDIO
				aRel01[nCont, 18] := QRY1->B2_CM1 //VALOR DA MENOR COTACAO
			EndIf
		Endif
		QRY1->(dbSkip())
	EndDo

Return aRel01

/*/{Protheus.doc} FSW0200601
(long_description)
@type function
@author queizy.nascimento
@since 10/10/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function FSW0200601() //Monta  perguntas
	Private aHelpPor := {}
	Private aHelpPor2 := {}
	
	aAdd( aHelpPor, "Selecione a Filial.  ")
	PutSx1( cPerg, "01","Filial" ,"","","mv_ch1", "C",20,0,0,"R","","FSSM01","","S","MV_PAR01","","","","C7_FILIAL","","","","","","","","","","","","",aHelpPor,,)
	aHelpPor := {}
	
	aAdd( aHelpPor, "Selecione o Per�odo De")
	aAdd( aHelpPor2, "Selecione o Per�odo Ate")
	
	PutSx1( cPerg, "02","Per�odo De" ,"","","mv_ch2","D",8,0,0,"G","U_F0200602(MV_PAR02)","","","S","MV_PAR02","","","","","","","","","","","","","","","","",aHelpPor,,)
	PutSx1( cPerg, "03","Per�odo At�","","","mv_ch3","D",8,0,0,"G","U_F0200602(MV_PAR02, MV_PAR03)","","","S","MV_PAR03","","","","","","","","","","","","","","","","",aHelpPor2,,)
	aHelpPor := {}
	aAdd( aHelpPor, "Selecione o Material.  ")
	PutSx1(cPerg, "04","Do Produto?"       ,"","" ,"mv_ch4","C",30,0,0,"R","","FSNNR2","","S","MV_PAR04","","","","B1_COD"  ,"","","","","","","","","","","","",aHelpPor,,)
Return
