#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#include 'FILEIO.ch'
/*
{Protheus.doc} F0300604()
Exporta��o de Grupo Familiar
@Author     Rogerio Candisani
@Since      14/10/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300604()

Local ctmpTit:="" 
Local ctmpDep :=""
Local montaTxt:= ""
Local cPerg:= "FSW0300604"	// Gp. perguntas especifico
Local cEstCivi:=""
Local cGrauPar:=""
Local lRet := .F. 
Local lGerou := .F. 

//exportar os dados do grupo familiar em .CSV
//criar pergunta F0300604

/*	Exporta��o Grupo Familiar:
MV_PAR01 Filial De
MV_PAR02 Filial At�
MV_PAR03 Matr�cula De
MV_PAR04 Matr�cula At�
MV_PAR05 Admiss�o De
MV_PAR06 Admiss�o ate
MV_PAR07 Vig�ncia De
MV_PAR08 Vig�ncia At�
MV_PAR09 Tipo de Arquivo (M�dico / Odontol�gico)
MV_PAR10 Caminho de Grava��o
*/

//	As Colunas 1, 2, 3, 4, 7, 15 e 32 - Informar somente a coluna, o dado ser� vazio;
//	A Coluna 5 dever� ser verificado no par�metro de gera��o, informando o conte�do 5 para Plano de Sa�de ou 6 para Plano Odontol�gico;
// A Coluna 14 � Quando for informa��es do Titular, informar fixo "Titular", quando for dependente, buscar a informa��o no campo RB_GRAUPAR;
// As Colunas que foram indicadas para serem geradas �em branco� ter�o dados informados manualmente pela equipe de benef�cios;
// Os tamanhos das colunas s�o livres, e dever�o ser separados por ";"

Pergunte(cPerg,.T.)

If (MsgYesNo(OemToAnsi("Confirma a exporta��o do grupo familiar ?"),OemToAnsi("Atencao")))
	lRet:= .T.
Else
	lRet:= .F.
Endif

If lRet
	//montar query do grupo familiar
	ctmpTit:=GetNextAlias()
	BeginSql Alias ctmpTit
		SELECT RA_MAT, RA_NOME, RA_ADMISSA, RA_SEXO, RA_NASC, RA_CIC, RA_ESTCIVI, RA_MAE, RA_PIS, RA_RG, 
			RA_RGORG, RA_DTRGEXP, RA_CC, RA_CARGO, RA_LOGRDSC, RA_ENDEREC, RA_LOGRNUM, RA_COMPLEM, RA_BAIRRO,
			RA_MUNICIP, RA_ESTADO, RA_CEP   
		FROM %table:SRA% SRA
		Inner join %table:RHK% RHK ON 
			RHK.RHK_MAT = SRA.RA_MAT AND
			RHK.%NotDel%
		WHERE SRA.RA_FILIAL BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
			AND SRA.RA_MAT BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04%
			AND SRA.RA_ADMISSA BETWEEN %Exp:MV_PAR05% AND %Exp:MV_PAR06%
			AND RHK.RHK_PERINI >= %Exp:MV_PAR07%
			AND RHK.RHK_PERFIM <= %Exp:MV_PAR08% 
			AND RHK.RHK_TPFORN = %Exp:MV_PAR09%
			AND SRA.%NotDel%
		ORDER BY %Order:SRA%
	EndSql	
	
	DbSelectArea(ctmpTit)
	(ctmpTit)->(DbGoTop())
	cMontaTxt:= ""
	While ! (ctmpTit)->(EOF())
	    lGerou := .T. 
		//monta o txt do titular
		cMontaTxt += ";" // 1
		cMontaTxt += ";" // 2
		cMontaTxt += ";" // 3
		cMontaTxt += ";" // 4
		cMontaTxt += IIF(MV_PAR09 == 1,"Saude","Odontologico") + ";" // 5 Codigo nome plano 
		cMontaTxt += (ctmpTit)->RA_MAT + ";" // 6 Matricula
		cMontaTxt += ";" // 7
		cMontaTxt += subst((ctmpTit)->RA_ADMISSA,7,2) + "/" + subst((ctmpTit)->RA_ADMISSA,5,2) + "/" + subst((ctmpTit)->RA_ADMISSA,1,4) + ";" // 8 Data admiss�o ,Informar a data DD/MM/AAAA
		cMontaTxt += (ctmpTit)->RA_NOME + ";"//9 Nome do segurado 
		cMontaTxt += IIF(RA_SEXO == "M","Masculino","Feminino") + ";" // 10 Sexo , Masculino ou Feminino
		cMontaTxt += subst((ctmpTit)->RA_NASC,7,2) + "/" + subst((ctmpTit)->RA_NASC,5,2) + "/" + subst((ctmpTit)->RA_NASC,1,4) + ";" // 11 Data nascimento , Informar a data DD/MM/AAAA
		cMontaTxt += (ctmpTit)->RA_CIC  + ";" // 12 N. CPF , Informar o CPF beneficiario, se menos de 18 anos CPF nao deve ser preenchido
		Do Case
			 Case (ctmpTit)->RA_ESTCIVI == "C"
			 		cEstCivi:= "Casado(a)"
			 Case (ctmpTit)->RA_ESTCIVI == "D"
			 		cEstCivi:= "Divorciado(a)"
			 Case (ctmpTit)->RA_ESTCIVI == "M"
			 		cEstCivi:= "Uni�o Est�vel"
			 Case (ctmpTit)->RA_ESTCIVI == "Q"
			 		cEstCivi:= "Desquitado(a)"
			 Case (ctmpTit)->RA_ESTCIVI == "S"
			 		cEstCivi:= "Solteiro(a)"
			 Case (ctmpTit)->RA_ESTCIVI == "V"
			 		cEstCivi:= "Vi�vo(a)"										
		EndCase
		cMontaTxt += cEstCivi  + ";" // 13  Estado civil , Casado(a),Solteiro(a), Uni�o Est�vel, Separado(a),Vi�vo(a),Divorciado(a)
		cMontaTxt += "Titular" + ";" // 14 Grau de parentesco ,Quando for informa��es do Titular, informar fixo "Titular"
		cMontaTxt += ";" // 15
		cMontaTxt += (ctmpTit)->RA_MAE  + ";" // 16 Nome da m�e , informa��o nome da m�e completo
		cMontaTxt += (ctmpTit)->RA_PIS  + ";" // 17 PIS , preencher somente para o titular
		cMontaTxt += (ctmpTit)->RA_RG   + ";" // 18 RG , se tiver informa��o preencher
		cMontaTxt += (ctmpTit)->RA_RGORG + ";" // 19 Org�o emissor , se tiver informa��o preencher
		cMontaTxt += subst((ctmpTit)->RA_DTRGEXP,7,2) + "/" + subst((ctmpTit)->RA_DTRGEXP,5,2) + "/" + subst((ctmpTit)->RA_DTRGEXP,1,4) + ";" // 20 Data de expedi��o, se tiver informa��o preencher
		cMontaTxt += (ctmpTit)->RA_CC + ";" // Centro de custo, se tiver informa��o preencher
		cMontaTxt +=  ";" // Nome C. de custo , se tiver informa��o preencher
		cMontaTxt += (ctmpTit)->RA_CARGO + ";" // Cargo , se tiver informa��o preencher
		cMontaTxt += (ctmpTit)->RA_LOGRDSC + ";" // Tipo de logradouro , Rua, Avenida
		cMontaTxt += (ctmpTit)->RA_ENDEREC + ";" // Nome do logradouro 
		cMontaTxt += (ctmpTit)->RA_LOGRNUM + ";" // Numero ,numero
		cMontaTxt += (ctmpTit)->RA_COMPLEM + ";" // Compl. Numero 
		cMontaTxt += (ctmpTit)->RA_BAIRRO + ";"  // Bairro 
		cMontaTxt += (ctmpTit)->RA_MUNICIP + ";" // Cidade 
		cMontaTxt += (ctmpTit)->RA_ESTADO + ";" // Estado 
		cMontaTxt += (ctmpTit)->RA_CEP + ";"  // CEP
		cMontaTxt +=  ";"  // Banco ,informar o banco para reembolso
		cMontaTxt +=  ";"  // Agencia , informar a agencia para reembolso
		cMontaTxt +=  ";"  // Dig. Agencia , informar a digito da agencia para reembolso
		cMontaTxt +=  ";"  // Conta corrente  , informar a conta corrente
		cMontaTxt += CHR(13)+ CHR(10) 
		(ctmpTit)->(dBSkip())
	Enddo
	
	//gera dados dos dependentes de cada titular com plano ativo
	DbSelectArea(ctmpTit)
	(ctmpTit)->(DbGoTop())
	While !(ctmpTit)->(EOF())  
			//montar query do dependente
			ctmpDep:=GetNextAlias()
			BeginSql Alias ctmpDep
				SELECT RB_MAT, RB_NOME,RB_SEXO, RB_DTNASC, RB_GRAUPAR,RB_CIC,
					RB_MAE       
				FROM %table:SRB% SRB
				Inner join %table:RHL% RHL ON 
					RHL.RHL_MAT = SRB.RB_MAT AND
					RHL.RHL_CODIGO = SRB.RB_COD AND
					RHL.%NotDel%
				WHERE SRB.RB_FILIAL = %xFilial:SRB%
					AND SRB.RB_MAT = %Exp:(ctmpTit)->RA_MAT%
					AND RHL.RHL_PERINI >= %Exp:MV_PAR07% 
					AND RHL.RHL_PERINI <= %Exp:MV_PAR08% // a pedido do Trombini mudado para RHK_PERINI
					AND RHL.RHL_TPFORN = %Exp:MV_PAR09% 
					AND SRB.%NotDel%
				ORDER BY %Order:SRB%
			EndSql	
			DbSelectArea(ctmpDep)
			(ctmpDep)->(DbGoTop())
			While !(ctmpDep)->(EOF())
				//monta o txt dos dependentes
				cMontaTxt += ";" // 1
				cMontaTxt += ";" // 2
				cMontaTxt += ";" // 3
				cMontaTxt += ";" // 4
				cMontaTxt += IIF(MV_PAR09 == 1,"Saude","Odontologico") + ";" // 5 Codigo nome plano 
				cMontaTxt += (ctmpDep)->RB_MAT + ";" // 6 Matricula
				cMontaTxt += ";" // 7
				cMontaTxt += ";" // 8 Data admiss�o , so para titular
				cMontaTxt += (ctmpDep)->RB_NOME + ";"// 9 Nome do segurado 
				cMontaTxt += IIF((ctmpDep)->RB_SEXO == "M","Masculino","Feminino") + ";" // 10 Sexo , Masculino ou Feminino
				cMontaTxt += subst((ctmpDep)->RB_DTNASC,7,2) + "/" + subst((ctmpDep)->RB_DTNASC,5,2) + "/" + subst((ctmpDep)->RB_DTNASC,1,4) + ";" // 11 Data nascimento , Informar a data DD/MM/AAAA
				cMontaTxt += (ctmpDep)->RB_CIC + ";" // 12 N. CPF , Informar o CPF beneficiario, se menos de 18 anos CPF nao deve ser preenchido
				cMontaTxt +=  ";" // 13  Estado civil , Casado(a),Solteiro(a), Uni�o Est�vel, Separado(a),Vi�vo(a),Divorciado(a)
				Do Case
					 Case (ctmpDep)->RB_GRAUPAR == "C"
					 		cGrauPar:= "C�njuge/Companheiro"
					 Case (ctmpDep)->RB_GRAUPAR == "F"
					 		cGrauPar:= "Filho(a)"
					 Case (ctmpDep)->RB_GRAUPAR == "E"
					 		cGrauPar:= "Enteado(a)"		
					 Case (ctmpDep)->RB_GRAUPAR == "P"
					 		cGrauPar:= "Pai/M�e"
					 Case (ctmpDep)->RB_GRAUPAR == "O"
					 		cGrauPar:= "Agregado/Outros"		
				EndCase
				cMontaTxt += cGrauPar + ";" // 14 Grau de parentesco ,Quando for informa��es do Titular, informar fixo "Titular"
				cMontaTxt += ";" // 15
				cMontaTxt += (ctmpDep)->RB_MAE + ";" // 16 Nome da m�e , informa��o nome da m�e completo
				cMontaTxt += ";" // 17 PIS , preencher somente para o titular
				cMontaTxt += ";" // 18 RG , se tiver informa��o preencher
				cMontaTxt += ";" // 19 Org�o emissor , se tiver informa��o preencher
				cMontaTxt += ";" // 20 Data de expedi��o, se tiver informa��o preencher
				cMontaTxt += ";" // 21 Centro de custo, se tiver informa��o preencher
				cMontaTxt += ";" // 22 Nome C. de custo , se tiver informa��o preencher
				cMontaTxt += ";" // 23 Cargo , se tiver informa��o preencher
				cMontaTxt += ";" // 24 Tipo de logradouro , Rua, Avenida
				cMontaTxt += ";" // Nome do logradouro 
				cMontaTxt += ";" // Numero ,numero
				cMontaTxt += ";" // Compl. Numero 
				cMontaTxt += ";"  // Bairro 
				cMontaTxt += ";" // Cidade 
				cMontaTxt += ";" // Estado 
				cMontaTxt += ";"  // CEP
				cMontaTxt += ";"  // Banco ,informar o banco para reembolso
				cMontaTxt += ";"  // Agencia , informar a agencia para reembolso
				cMontaTxt += ";"  // Dig. Agencia , informar a digito da agencia para reembolso
				cMontaTxt += ";"  // Conta corrente  , informar a conta corrente
				cMontaTxt += CHR(13)+ CHR(10)
				(ctmpDep)->(DbSkip())
			Enddo
		(ctmpDep)->(DbCloseArea())
		DbSelectArea(ctmpTit)
		(ctmpTit)->(DbSkip())
	Enddo
	
	//fechar arquivos temporarios
	(ctmpTit)->(DbCloseArea())
	
	
	//gerar o arquivo
	If lgerou
		criaCSV()
	Else
		MsgAlert("N�o existem dados a serem gerados, verifique os parametros utilizados")
	Endif 
	

Endif
Return lRet

///////////////////////////////////////////////////////////////////////
// Exportando dados para planilha 
////////////////////////////////////////////////////////////////////////
Static Function criaCSV()

// Nome do arquivo criado, o nome � composto por umam descri��o 
//a data e a hora da cria��o, para que n�o existam nomes iguais
cNomeArq := alltrim(MV_PAR10) + ".csv"

// criar arquivo texto vazio a partir do root path no servidor
nHandle := FCREATE(cNomeArq)

FWrite(nHandle,cMontaTxt)

// encerra grava��o no arquivo
FClose(nHandle)

FOPEN(cNomeArq, FO_READWRITE)

//MsgAlert("Relatorio salvo em:" + cNomeArq )

Return