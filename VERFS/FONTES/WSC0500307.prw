#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://localhost:90/ws/W0500307.apw?WSDL
Gerado em        01/11/17 17:51:36
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _QBBETRT ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSW0500307
------------------------------------------------------------------------------- */

WSCLIENT WSW0500307

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD BUSCATRM
	WSMETHOD CHKRH3
	WSMETHOD FUNRH3
	WSMETHOD INFPEGSUP
	WSMETHOD INFRH3
	WSMETHOD INFRH4C
	WSMETHOD INFRH4D
	WSMETHOD INFRH4F
	WSMETHOD INFRH4I
	WSMETHOD INFSOLVG

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   cCODRH4                   AS string
	WSDATA   cFILRH3                   AS string
	WSDATA   oWSBUSCATRMRESULT         AS W0500307_TREINA
	WSDATA   cCODRH3                   AS string
	WSDATA   lCHKRH3RESULT             AS boolean
	WSDATA   oWSFUNRH3RESULT           AS W0500307_FUNCIO
	WSDATA   oWS_SUPERINF              AS W0500307_SUPERINF
	WSDATA   oWSINFPEGSUPRESULT        AS W0500307_SUPEML
	WSDATA   cMATRICULA                AS string
	WSDATA   cFILTRO                   AS string
	WSDATA   cCAMPO                    AS string
	WSDATA   cVALOR                    AS string
	WSDATA   oWSINFRH3RESULT           AS W0500307__ACOMPASOL
	WSDATA   oWSINFRH4CRESULT          AS W0500307_SLCARSAL
	WSDATA   oWSINFRH4DRESULT          AS W0500307_SOLDES
	WSDATA   oWSINFRH4FRESULT          AS W0500307_INFH4
	WSDATA   cINFRH4IRESULT            AS string
	WSDATA   oWSINFSOLVGRESULT         AS W0500307_INFRH3SU

	// Estruturas mantidas por compatibilidade - N�O USAR
	WSDATA   oWSSUPERINF               AS W0500307_SUPERINF

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSW0500307
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20160510 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSW0500307
	::oWSBUSCATRMRESULT  := W0500307_TREINA():New()
	::oWSFUNRH3RESULT    := W0500307_FUNCIO():New()
	::oWS_SUPERINF       := W0500307_SUPERINF():New()
	::oWSINFPEGSUPRESULT := W0500307_SUPEML():New()
	::oWSINFRH3RESULT    := W0500307__ACOMPASOL():New()
	::oWSINFRH4CRESULT   := W0500307_SLCARSAL():New()
	::oWSINFRH4DRESULT   := W0500307_SOLDES():New()
	::oWSINFRH4FRESULT   := W0500307_INFH4():New()
	::oWSINFSOLVGRESULT  := W0500307_INFRH3SU():New()

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSSUPERINF        := ::oWS_SUPERINF
Return

WSMETHOD RESET WSCLIENT WSW0500307
	::cCODRH4            := NIL 
	::cFILRH3            := NIL 
	::oWSBUSCATRMRESULT  := NIL 
	::cCODRH3            := NIL 
	::lCHKRH3RESULT      := NIL 
	::oWSFUNRH3RESULT    := NIL 
	::oWS_SUPERINF       := NIL 
	::oWSINFPEGSUPRESULT := NIL 
	::cMATRICULA         := NIL 
	::cFILTRO            := NIL 
	::cCAMPO             := NIL 
	::cVALOR             := NIL 
	::oWSINFRH3RESULT    := NIL 
	::oWSINFRH4CRESULT   := NIL 
	::oWSINFRH4DRESULT   := NIL 
	::oWSINFRH4FRESULT   := NIL 
	::cINFRH4IRESULT     := NIL 
	::oWSINFSOLVGRESULT  := NIL 

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSSUPERINF        := NIL
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSW0500307
Local oClone := WSW0500307():New()
	oClone:_URL          := ::_URL 
	oClone:cCODRH4       := ::cCODRH4
	oClone:cFILRH3       := ::cFILRH3
	oClone:oWSBUSCATRMRESULT :=  IIF(::oWSBUSCATRMRESULT = NIL , NIL ,::oWSBUSCATRMRESULT:Clone() )
	oClone:cCODRH3       := ::cCODRH3
	oClone:lCHKRH3RESULT := ::lCHKRH3RESULT
	oClone:oWSFUNRH3RESULT :=  IIF(::oWSFUNRH3RESULT = NIL , NIL ,::oWSFUNRH3RESULT:Clone() )
	oClone:oWS_SUPERINF  :=  IIF(::oWS_SUPERINF = NIL , NIL ,::oWS_SUPERINF:Clone() )
	oClone:oWSINFPEGSUPRESULT :=  IIF(::oWSINFPEGSUPRESULT = NIL , NIL ,::oWSINFPEGSUPRESULT:Clone() )
	oClone:cMATRICULA    := ::cMATRICULA
	oClone:cFILTRO       := ::cFILTRO
	oClone:cCAMPO        := ::cCAMPO
	oClone:cVALOR        := ::cVALOR
	oClone:oWSINFRH3RESULT :=  IIF(::oWSINFRH3RESULT = NIL , NIL ,::oWSINFRH3RESULT:Clone() )
	oClone:oWSINFRH4CRESULT :=  IIF(::oWSINFRH4CRESULT = NIL , NIL ,::oWSINFRH4CRESULT:Clone() )
	oClone:oWSINFRH4DRESULT :=  IIF(::oWSINFRH4DRESULT = NIL , NIL ,::oWSINFRH4DRESULT:Clone() )
	oClone:oWSINFRH4FRESULT :=  IIF(::oWSINFRH4FRESULT = NIL , NIL ,::oWSINFRH4FRESULT:Clone() )
	oClone:cINFRH4IRESULT := ::cINFRH4IRESULT
	oClone:oWSINFSOLVGRESULT :=  IIF(::oWSINFSOLVGRESULT = NIL , NIL ,::oWSINFSOLVGRESULT:Clone() )

	// Estruturas mantidas por compatibilidade - N�O USAR
	oClone:oWSSUPERINF   := oClone:oWS_SUPERINF
Return oClone

// WSDL Method BUSCATRM of Service WSW0500307

WSMETHOD BUSCATRM WSSEND cCODRH4,cFILRH3 WSRECEIVE oWSBUSCATRMRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<BUSCATRM xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH4", ::cCODRH4, cCODRH4 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</BUSCATRM>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/BUSCATRM",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSBUSCATRMRESULT:SoapRecv( WSAdvValue( oXmlRet,"_BUSCATRMRESPONSE:_BUSCATRMRESULT","TREINA",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method CHKRH3 of Service WSW0500307

WSMETHOD CHKRH3 WSSEND cCODRH3,cFILRH3 WSRECEIVE lCHKRH3RESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<CHKRH3 xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH3", ::cCODRH3, cCODRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</CHKRH3>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/CHKRH3",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::lCHKRH3RESULT      :=  WSAdvValue( oXmlRet,"_CHKRH3RESPONSE:_CHKRH3RESULT:TEXT","boolean",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method FUNRH3 of Service WSW0500307

WSMETHOD FUNRH3 WSSEND cCODRH3,cFILRH3 WSRECEIVE oWSFUNRH3RESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<FUNRH3 xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH3", ::cCODRH3, cCODRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</FUNRH3>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/FUNRH3",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSFUNRH3RESULT:SoapRecv( WSAdvValue( oXmlRet,"_FUNRH3RESPONSE:_FUNRH3RESULT","FUNCIO",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFPEGSUP of Service WSW0500307

WSMETHOD INFPEGSUP WSSEND oWS_SUPERINF WSRECEIVE oWSINFPEGSUPRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFPEGSUP xmlns="http://localhost:90/">'
cSoap += WSSoapValue("_SUPERINF", ::oWS_SUPERINF, oWS_SUPERINF , "SUPERINF", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFPEGSUP>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFPEGSUP",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFPEGSUPRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFPEGSUPRESPONSE:_INFPEGSUPRESULT","SUPEML",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFRH3 of Service WSW0500307

WSMETHOD INFRH3 WSSEND cMATRICULA,cFILTRO,cCAMPO,cVALOR WSRECEIVE oWSINFRH3RESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFRH3 xmlns="http://localhost:90/">'
cSoap += WSSoapValue("MATRICULA", ::cMATRICULA, cMATRICULA , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILTRO", ::cFILTRO, cFILTRO , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("CAMPO", ::cCAMPO, cCAMPO , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("VALOR", ::cVALOR, cVALOR , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFRH3>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFRH3",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFRH3RESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFRH3RESPONSE:_INFRH3RESULT","_ACOMPASOL",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFRH4C of Service WSW0500307

WSMETHOD INFRH4C WSSEND cCODRH4,cFILRH3 WSRECEIVE oWSINFRH4CRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFRH4C xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH4", ::cCODRH4, cCODRH4 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFRH4C>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFRH4C",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFRH4CRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFRH4CRESPONSE:_INFRH4CRESULT","SLCARSAL",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFRH4D of Service WSW0500307

WSMETHOD INFRH4D WSSEND cCODRH4,cFILRH3 WSRECEIVE oWSINFRH4DRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFRH4D xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH4", ::cCODRH4, cCODRH4 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFRH4D>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFRH4D",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFRH4DRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFRH4DRESPONSE:_INFRH4DRESULT","SOLDES",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFRH4F of Service WSW0500307

WSMETHOD INFRH4F WSSEND cCODRH4,cFILRH3 WSRECEIVE oWSINFRH4FRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFRH4F xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH4", ::cCODRH4, cCODRH4 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFRH4F>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFRH4F",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFRH4FRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFRH4FRESPONSE:_INFRH4FRESULT","INFH4",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFRH4I of Service WSW0500307

WSMETHOD INFRH4I WSSEND cCODRH4,cFILRH3 WSRECEIVE cINFRH4IRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFRH4I xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH4", ::cCODRH4, cCODRH4 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFRH4I>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFRH4I",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::cINFRH4IRESULT     :=  WSAdvValue( oXmlRet,"_INFRH4IRESPONSE:_INFRH4IRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFSOLVG of Service WSW0500307

WSMETHOD INFSOLVG WSSEND cCODRH3,cFILRH3 WSRECEIVE oWSINFSOLVGRESULT WSCLIENT WSW0500307
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFSOLVG xmlns="http://localhost:90/">'
cSoap += WSSoapValue("CODRH3", ::cCODRH3, cCODRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("FILRH3", ::cFILRH3, cFILRH3 , "string", .T. , .F., 0 , NIL, .F.) 
cSoap += "</INFSOLVG>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:90/INFSOLVG",; 
	"DOCUMENT","http://localhost:90/",,"1.031217",; 
	"http://localhost:90/ws/W0500307.apw")

::Init()
::oWSINFSOLVGRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFSOLVGRESPONSE:_INFSOLVGRESULT","INFRH3SU",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure TREINA

WSSTRUCT W0500307_TREINA
	WSDATA   cCALEND                   AS string
	WSDATA   cCURSO                    AS string
	WSDATA   cNOME                     AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_TREINA
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_TREINA
Return

WSMETHOD CLONE WSCLIENT W0500307_TREINA
	Local oClone := W0500307_TREINA():NEW()
	oClone:cCALEND              := ::cCALEND
	oClone:cCURSO               := ::cCURSO
	oClone:cNOME                := ::cNOME
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_TREINA
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCALEND            :=  WSAdvValue( oResponse,"_CALEND","string",NIL,"Property cCALEND as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCURSO             :=  WSAdvValue( oResponse,"_CURSO","string",NIL,"Property cCURSO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNOME              :=  WSAdvValue( oResponse,"_NOME","string",NIL,"Property cNOME as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure FUNCIO

WSSTRUCT W0500307_FUNCIO
	WSDATA   cCODMAFUN                 AS string
	WSDATA   cNOMMAFUN                 AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_FUNCIO
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_FUNCIO
Return

WSMETHOD CLONE WSCLIENT W0500307_FUNCIO
	Local oClone := W0500307_FUNCIO():NEW()
	oClone:cCODMAFUN            := ::cCODMAFUN
	oClone:cNOMMAFUN            := ::cNOMMAFUN
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_FUNCIO
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCODMAFUN          :=  WSAdvValue( oResponse,"_CODMAFUN","string",NIL,"Property cCODMAFUN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNOMMAFUN          :=  WSAdvValue( oResponse,"_NOMMAFUN","string",NIL,"Property cNOMMAFUN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure SUPERINF

WSSTRUCT W0500307_SUPERINF
	WSDATA   cCODMASUP                 AS string
	WSDATA   cEMPAPROV                 AS string
	WSDATA   cFILMASUP                 AS string
	WSDATA   cFILSOLIC                 AS string
	WSDATA   cNVLAPROV                 AS string
	WSDATA   cSOLICI                   AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_SUPERINF
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_SUPERINF
Return

WSMETHOD CLONE WSCLIENT W0500307_SUPERINF
	Local oClone := W0500307_SUPERINF():NEW()
	oClone:cCODMASUP            := ::cCODMASUP
	oClone:cEMPAPROV            := ::cEMPAPROV
	oClone:cFILMASUP            := ::cFILMASUP
	oClone:cFILSOLIC            := ::cFILSOLIC
	oClone:cNVLAPROV            := ::cNVLAPROV
	oClone:cSOLICI              := ::cSOLICI
Return oClone

WSMETHOD SOAPSEND WSCLIENT W0500307_SUPERINF
	Local cSoap := ""
	cSoap += WSSoapValue("CODMASUP", ::cCODMASUP, ::cCODMASUP , "string", .T. , .F., 0 , NIL, .F.) 
	cSoap += WSSoapValue("EMPAPROV", ::cEMPAPROV, ::cEMPAPROV , "string", .T. , .F., 0 , NIL, .F.) 
	cSoap += WSSoapValue("FILMASUP", ::cFILMASUP, ::cFILMASUP , "string", .T. , .F., 0 , NIL, .F.) 
	cSoap += WSSoapValue("FILSOLIC", ::cFILSOLIC, ::cFILSOLIC , "string", .T. , .F., 0 , NIL, .F.) 
	cSoap += WSSoapValue("NVLAPROV", ::cNVLAPROV, ::cNVLAPROV , "string", .T. , .F., 0 , NIL, .F.) 
	cSoap += WSSoapValue("SOLICI", ::cSOLICI, ::cSOLICI , "string", .T. , .F., 0 , NIL, .F.) 
Return cSoap

// WSDL Data Structure SUPEML

WSSTRUCT W0500307_SUPEML
	WSDATA   cQBMATRESP                AS string
	WSDATA   cRAEMAIL                  AS string
	WSDATA   cRANOME                   AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_SUPEML
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_SUPEML
Return

WSMETHOD CLONE WSCLIENT W0500307_SUPEML
	Local oClone := W0500307_SUPEML():NEW()
	oClone:cQBMATRESP           := ::cQBMATRESP
	oClone:cRAEMAIL             := ::cRAEMAIL
	oClone:cRANOME              := ::cRANOME
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_SUPEML
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cQBMATRESP         :=  WSAdvValue( oResponse,"_QBMATRESP","string",NIL,"Property cQBMATRESP as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAEMAIL           :=  WSAdvValue( oResponse,"_RAEMAIL","string",NIL,"Property cRAEMAIL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRANOME            :=  WSAdvValue( oResponse,"_RANOME","string",NIL,"Property cRANOME as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure _ACOMPASOL

WSSTRUCT W0500307__ACOMPASOL
	WSDATA   oWSACOMPANHA              AS W0500307_ARRAYOFACOMPASOL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307__ACOMPASOL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307__ACOMPASOL
Return

WSMETHOD CLONE WSCLIENT W0500307__ACOMPASOL
	Local oClone := W0500307__ACOMPASOL():NEW()
	oClone:oWSACOMPANHA         := IIF(::oWSACOMPANHA = NIL , NIL , ::oWSACOMPANHA:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307__ACOMPASOL
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_ACOMPANHA","ARRAYOFACOMPASOL",NIL,"Property oWSACOMPANHA as s0:ARRAYOFACOMPASOL on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSACOMPANHA := W0500307_ARRAYOFACOMPASOL():New()
		::oWSACOMPANHA:SoapRecv(oNode1)
	EndIf
Return

// WSDL Data Structure SLCARSAL

WSSTRUCT W0500307_SLCARSAL
	WSDATA   cRACC                     AS string
	WSDATA   cRACLVL                   AS string
	WSDATA   cRACODFUNC                AS string
	WSDATA   cRADEPTO                  AS string
	WSDATA   cRAFILIAL                 AS string
	WSDATA   cRAHRSDIA                 AS string
	WSDATA   cRAHRSEMAN                AS string
	WSDATA   cRAHRSMES                 AS string
	WSDATA   cRAITEM                   AS string
	WSDATA   cRAPOSTO                  AS string
	WSDATA   cRAPROCES                 AS string
	WSDATA   cRAREGRA                  AS string
	WSDATA   cRASALARIO                AS string
	WSDATA   cRASEQTURN                AS string
	WSDATA   cRATIPOALT                AS string
	WSDATA   cRATNOTRAB                AS string
	WSDATA   cTMPCCATU                 AS string
	WSDATA   cTMPCLVL                  AS string
	WSDATA   cTMPD_CC_D                AS string
	WSDATA   cTMPD_CCAT                AS string
	WSDATA   cTMPD_DEPD                AS string
	WSDATA   cTMPD_DEPT                AS string
	WSDATA   cTMPD_F_PP                AS string
	WSDATA   cTMPD_FILI                AS string
	WSDATA   cTMPD_FUNC                AS string
	WSDATA   cTMPD_MOT                 AS string
	WSDATA   cTMPD_TURN                AS string
	WSDATA   cTMPDEPTOA                AS string
	WSDATA   cTMPDESCTU                AS string
	WSDATA   cTMPFAIXAT                AS string
	WSDATA   cTMPFILIAL                AS string
	WSDATA   cTMPFUNCAO                AS string
	WSDATA   cTMPH_D_AT                AS string
	WSDATA   cTMPH_M_AT                AS string
	WSDATA   cTMPH_S_AT                AS string
	WSDATA   cTMPITEM                  AS string
	WSDATA   cTMPN_F_D                 AS string
	WSDATA   cTMPNIVELT                AS string
	WSDATA   cTMPPERCSA                AS string
	WSDATA   cTMPPOSTO                 AS string
	WSDATA   cTMPPROCES                AS string
	WSDATA   cTMPREGRA                 AS string
	WSDATA   cTMPS_TURN                AS string
	WSDATA   cTMPSALATU                AS string
	WSDATA   cTMPTABELA                AS string
	WSDATA   cTMPTIPO                  AS string
	WSDATA   cTMPTURNAT                AS string
	WSDATA   cTMPVLSALA                AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_SLCARSAL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_SLCARSAL
Return

WSMETHOD CLONE WSCLIENT W0500307_SLCARSAL
	Local oClone := W0500307_SLCARSAL():NEW()
	oClone:cRACC                := ::cRACC
	oClone:cRACLVL              := ::cRACLVL
	oClone:cRACODFUNC           := ::cRACODFUNC
	oClone:cRADEPTO             := ::cRADEPTO
	oClone:cRAFILIAL            := ::cRAFILIAL
	oClone:cRAHRSDIA            := ::cRAHRSDIA
	oClone:cRAHRSEMAN           := ::cRAHRSEMAN
	oClone:cRAHRSMES            := ::cRAHRSMES
	oClone:cRAITEM              := ::cRAITEM
	oClone:cRAPOSTO             := ::cRAPOSTO
	oClone:cRAPROCES            := ::cRAPROCES
	oClone:cRAREGRA             := ::cRAREGRA
	oClone:cRASALARIO           := ::cRASALARIO
	oClone:cRASEQTURN           := ::cRASEQTURN
	oClone:cRATIPOALT           := ::cRATIPOALT
	oClone:cRATNOTRAB           := ::cRATNOTRAB
	oClone:cTMPCCATU            := ::cTMPCCATU
	oClone:cTMPCLVL             := ::cTMPCLVL
	oClone:cTMPD_CC_D           := ::cTMPD_CC_D
	oClone:cTMPD_CCAT           := ::cTMPD_CCAT
	oClone:cTMPD_DEPD           := ::cTMPD_DEPD
	oClone:cTMPD_DEPT           := ::cTMPD_DEPT
	oClone:cTMPD_F_PP           := ::cTMPD_F_PP
	oClone:cTMPD_FILI           := ::cTMPD_FILI
	oClone:cTMPD_FUNC           := ::cTMPD_FUNC
	oClone:cTMPD_MOT            := ::cTMPD_MOT
	oClone:cTMPD_TURN           := ::cTMPD_TURN
	oClone:cTMPDEPTOA           := ::cTMPDEPTOA
	oClone:cTMPDESCTU           := ::cTMPDESCTU
	oClone:cTMPFAIXAT           := ::cTMPFAIXAT
	oClone:cTMPFILIAL           := ::cTMPFILIAL
	oClone:cTMPFUNCAO           := ::cTMPFUNCAO
	oClone:cTMPH_D_AT           := ::cTMPH_D_AT
	oClone:cTMPH_M_AT           := ::cTMPH_M_AT
	oClone:cTMPH_S_AT           := ::cTMPH_S_AT
	oClone:cTMPITEM             := ::cTMPITEM
	oClone:cTMPN_F_D            := ::cTMPN_F_D
	oClone:cTMPNIVELT           := ::cTMPNIVELT
	oClone:cTMPPERCSA           := ::cTMPPERCSA
	oClone:cTMPPOSTO            := ::cTMPPOSTO
	oClone:cTMPPROCES           := ::cTMPPROCES
	oClone:cTMPREGRA            := ::cTMPREGRA
	oClone:cTMPS_TURN           := ::cTMPS_TURN
	oClone:cTMPSALATU           := ::cTMPSALATU
	oClone:cTMPTABELA           := ::cTMPTABELA
	oClone:cTMPTIPO             := ::cTMPTIPO
	oClone:cTMPTURNAT           := ::cTMPTURNAT
	oClone:cTMPVLSALA           := ::cTMPVLSALA
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_SLCARSAL
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cRACC              :=  WSAdvValue( oResponse,"_RACC","string",NIL,"Property cRACC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRACLVL            :=  WSAdvValue( oResponse,"_RACLVL","string",NIL,"Property cRACLVL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRACODFUNC         :=  WSAdvValue( oResponse,"_RACODFUNC","string",NIL,"Property cRACODFUNC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRADEPTO           :=  WSAdvValue( oResponse,"_RADEPTO","string",NIL,"Property cRADEPTO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAFILIAL          :=  WSAdvValue( oResponse,"_RAFILIAL","string",NIL,"Property cRAFILIAL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAHRSDIA          :=  WSAdvValue( oResponse,"_RAHRSDIA","string",NIL,"Property cRAHRSDIA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAHRSEMAN         :=  WSAdvValue( oResponse,"_RAHRSEMAN","string",NIL,"Property cRAHRSEMAN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAHRSMES          :=  WSAdvValue( oResponse,"_RAHRSMES","string",NIL,"Property cRAHRSMES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAITEM            :=  WSAdvValue( oResponse,"_RAITEM","string",NIL,"Property cRAITEM as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAPOSTO           :=  WSAdvValue( oResponse,"_RAPOSTO","string",NIL,"Property cRAPOSTO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAPROCES          :=  WSAdvValue( oResponse,"_RAPROCES","string",NIL,"Property cRAPROCES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRAREGRA           :=  WSAdvValue( oResponse,"_RAREGRA","string",NIL,"Property cRAREGRA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRASALARIO         :=  WSAdvValue( oResponse,"_RASALARIO","string",NIL,"Property cRASALARIO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRASEQTURN         :=  WSAdvValue( oResponse,"_RASEQTURN","string",NIL,"Property cRASEQTURN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRATIPOALT         :=  WSAdvValue( oResponse,"_RATIPOALT","string",NIL,"Property cRATIPOALT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRATNOTRAB         :=  WSAdvValue( oResponse,"_RATNOTRAB","string",NIL,"Property cRATNOTRAB as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPCCATU          :=  WSAdvValue( oResponse,"_TMPCCATU","string",NIL,"Property cTMPCCATU as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPCLVL           :=  WSAdvValue( oResponse,"_TMPCLVL","string",NIL,"Property cTMPCLVL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_CC_D         :=  WSAdvValue( oResponse,"_TMPD_CC_D","string",NIL,"Property cTMPD_CC_D as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_CCAT         :=  WSAdvValue( oResponse,"_TMPD_CCAT","string",NIL,"Property cTMPD_CCAT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_DEPD         :=  WSAdvValue( oResponse,"_TMPD_DEPD","string",NIL,"Property cTMPD_DEPD as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_DEPT         :=  WSAdvValue( oResponse,"_TMPD_DEPT","string",NIL,"Property cTMPD_DEPT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_F_PP         :=  WSAdvValue( oResponse,"_TMPD_F_PP","string",NIL,"Property cTMPD_F_PP as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_FILI         :=  WSAdvValue( oResponse,"_TMPD_FILI","string",NIL,"Property cTMPD_FILI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_FUNC         :=  WSAdvValue( oResponse,"_TMPD_FUNC","string",NIL,"Property cTMPD_FUNC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_MOT          :=  WSAdvValue( oResponse,"_TMPD_MOT","string",NIL,"Property cTMPD_MOT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPD_TURN         :=  WSAdvValue( oResponse,"_TMPD_TURN","string",NIL,"Property cTMPD_TURN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPDEPTOA         :=  WSAdvValue( oResponse,"_TMPDEPTOA","string",NIL,"Property cTMPDEPTOA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPDESCTU         :=  WSAdvValue( oResponse,"_TMPDESCTU","string",NIL,"Property cTMPDESCTU as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPFAIXAT         :=  WSAdvValue( oResponse,"_TMPFAIXAT","string",NIL,"Property cTMPFAIXAT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPFILIAL         :=  WSAdvValue( oResponse,"_TMPFILIAL","string",NIL,"Property cTMPFILIAL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPFUNCAO         :=  WSAdvValue( oResponse,"_TMPFUNCAO","string",NIL,"Property cTMPFUNCAO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPH_D_AT         :=  WSAdvValue( oResponse,"_TMPH_D_AT","string",NIL,"Property cTMPH_D_AT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPH_M_AT         :=  WSAdvValue( oResponse,"_TMPH_M_AT","string",NIL,"Property cTMPH_M_AT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPH_S_AT         :=  WSAdvValue( oResponse,"_TMPH_S_AT","string",NIL,"Property cTMPH_S_AT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPITEM           :=  WSAdvValue( oResponse,"_TMPITEM","string",NIL,"Property cTMPITEM as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPN_F_D          :=  WSAdvValue( oResponse,"_TMPN_F_D","string",NIL,"Property cTMPN_F_D as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPNIVELT         :=  WSAdvValue( oResponse,"_TMPNIVELT","string",NIL,"Property cTMPNIVELT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPPERCSA         :=  WSAdvValue( oResponse,"_TMPPERCSA","string",NIL,"Property cTMPPERCSA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPPOSTO          :=  WSAdvValue( oResponse,"_TMPPOSTO","string",NIL,"Property cTMPPOSTO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPPROCES         :=  WSAdvValue( oResponse,"_TMPPROCES","string",NIL,"Property cTMPPROCES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPREGRA          :=  WSAdvValue( oResponse,"_TMPREGRA","string",NIL,"Property cTMPREGRA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPS_TURN         :=  WSAdvValue( oResponse,"_TMPS_TURN","string",NIL,"Property cTMPS_TURN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPSALATU         :=  WSAdvValue( oResponse,"_TMPSALATU","string",NIL,"Property cTMPSALATU as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPTABELA         :=  WSAdvValue( oResponse,"_TMPTABELA","string",NIL,"Property cTMPTABELA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPTIPO           :=  WSAdvValue( oResponse,"_TMPTIPO","string",NIL,"Property cTMPTIPO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPTURNAT         :=  WSAdvValue( oResponse,"_TMPTURNAT","string",NIL,"Property cTMPTURNAT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPVLSALA         :=  WSAdvValue( oResponse,"_TMPVLSALA","string",NIL,"Property cTMPVLSALA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure SOLDES

WSSTRUCT W0500307_SOLDES
	WSDATA   cP10CODRES                AS string
	WSDATA   cP10DTDEMI                AS string
	WSDATA   cP10DTSOLI                AS string
	WSDATA   cP10FILIAL                AS string
	WSDATA   cP10MATRIC                AS string
	WSDATA   cP10MOTIVO                AS string
	WSDATA   cTMPDESRES                AS string
	WSDATA   cTMPMATRIC                AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_SOLDES
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_SOLDES
Return

WSMETHOD CLONE WSCLIENT W0500307_SOLDES
	Local oClone := W0500307_SOLDES():NEW()
	oClone:cP10CODRES           := ::cP10CODRES
	oClone:cP10DTDEMI           := ::cP10DTDEMI
	oClone:cP10DTSOLI           := ::cP10DTSOLI
	oClone:cP10FILIAL           := ::cP10FILIAL
	oClone:cP10MATRIC           := ::cP10MATRIC
	oClone:cP10MOTIVO           := ::cP10MOTIVO
	oClone:cTMPDESRES           := ::cTMPDESRES
	oClone:cTMPMATRIC           := ::cTMPMATRIC
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_SOLDES
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cP10CODRES         :=  WSAdvValue( oResponse,"_P10CODRES","string",NIL,"Property cP10CODRES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cP10DTDEMI         :=  WSAdvValue( oResponse,"_P10DTDEMI","string",NIL,"Property cP10DTDEMI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cP10DTSOLI         :=  WSAdvValue( oResponse,"_P10DTSOLI","string",NIL,"Property cP10DTSOLI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cP10FILIAL         :=  WSAdvValue( oResponse,"_P10FILIAL","string",NIL,"Property cP10FILIAL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cP10MATRIC         :=  WSAdvValue( oResponse,"_P10MATRIC","string",NIL,"Property cP10MATRIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cP10MOTIVO         :=  WSAdvValue( oResponse,"_P10MOTIVO","string",NIL,"Property cP10MOTIVO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPDESRES         :=  WSAdvValue( oResponse,"_TMPDESRES","string",NIL,"Property cTMPDESRES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPMATRIC         :=  WSAdvValue( oResponse,"_TMPMATRIC","string",NIL,"Property cTMPMATRIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure INFH4

WSSTRUCT W0500307_INFH4
	WSDATA   cPA5CDCAND                AS string
	WSDATA   cPA5CDVAGA                AS string
	WSDATA   cPA5CPFCAN                AS string
	WSDATA   cPA5DTAPRV                AS string
	WSDATA   cPA5FILIAL                AS string
	WSDATA   cPA5LOGTOR                AS string
	WSDATA   cPA5NMCAND                AS string
	WSDATA   cPA5SLFECH                AS string
	WSDATA   cPA5VLVAGA                AS string
	WSDATA   cTMPNMVAGA                AS string
	WSDATA   cTMPNOMTOR                AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_INFH4
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_INFH4
Return

WSMETHOD CLONE WSCLIENT W0500307_INFH4
	Local oClone := W0500307_INFH4():NEW()
	oClone:cPA5CDCAND           := ::cPA5CDCAND
	oClone:cPA5CDVAGA           := ::cPA5CDVAGA
	oClone:cPA5CPFCAN           := ::cPA5CPFCAN
	oClone:cPA5DTAPRV           := ::cPA5DTAPRV
	oClone:cPA5FILIAL           := ::cPA5FILIAL
	oClone:cPA5LOGTOR           := ::cPA5LOGTOR
	oClone:cPA5NMCAND           := ::cPA5NMCAND
	oClone:cPA5SLFECH           := ::cPA5SLFECH
	oClone:cPA5VLVAGA           := ::cPA5VLVAGA
	oClone:cTMPNMVAGA           := ::cTMPNMVAGA
	oClone:cTMPNOMTOR           := ::cTMPNOMTOR
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_INFH4
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cPA5CDCAND         :=  WSAdvValue( oResponse,"_PA5CDCAND","string",NIL,"Property cPA5CDCAND as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5CDVAGA         :=  WSAdvValue( oResponse,"_PA5CDVAGA","string",NIL,"Property cPA5CDVAGA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5CPFCAN         :=  WSAdvValue( oResponse,"_PA5CPFCAN","string",NIL,"Property cPA5CPFCAN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5DTAPRV         :=  WSAdvValue( oResponse,"_PA5DTAPRV","string",NIL,"Property cPA5DTAPRV as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5FILIAL         :=  WSAdvValue( oResponse,"_PA5FILIAL","string",NIL,"Property cPA5FILIAL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5LOGTOR         :=  WSAdvValue( oResponse,"_PA5LOGTOR","string",NIL,"Property cPA5LOGTOR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5NMCAND         :=  WSAdvValue( oResponse,"_PA5NMCAND","string",NIL,"Property cPA5NMCAND as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5SLFECH         :=  WSAdvValue( oResponse,"_PA5SLFECH","string",NIL,"Property cPA5SLFECH as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPA5VLVAGA         :=  WSAdvValue( oResponse,"_PA5VLVAGA","string",NIL,"Property cPA5VLVAGA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPNMVAGA         :=  WSAdvValue( oResponse,"_TMPNMVAGA","string",NIL,"Property cTMPNMVAGA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTMPNOMTOR         :=  WSAdvValue( oResponse,"_TMPNOMTOR","string",NIL,"Property cTMPNOMTOR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure INFRH3SU

WSSTRUCT W0500307_INFRH3SU
	WSDATA   cRH3FILAPR                AS string
	WSDATA   cRH3FILINI                AS string
	WSDATA   cRH3MATAPR                AS string
	WSDATA   cRH3MATINI                AS string
	WSDATA   cRH3VISAO                 AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_INFRH3SU
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_INFRH3SU
Return

WSMETHOD CLONE WSCLIENT W0500307_INFRH3SU
	Local oClone := W0500307_INFRH3SU():NEW()
	oClone:cRH3FILAPR           := ::cRH3FILAPR
	oClone:cRH3FILINI           := ::cRH3FILINI
	oClone:cRH3MATAPR           := ::cRH3MATAPR
	oClone:cRH3MATINI           := ::cRH3MATINI
	oClone:cRH3VISAO            := ::cRH3VISAO
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_INFRH3SU
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cRH3FILAPR         :=  WSAdvValue( oResponse,"_RH3FILAPR","string",NIL,"Property cRH3FILAPR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3FILINI         :=  WSAdvValue( oResponse,"_RH3FILINI","string",NIL,"Property cRH3FILINI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3MATAPR         :=  WSAdvValue( oResponse,"_RH3MATAPR","string",NIL,"Property cRH3MATAPR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3MATINI         :=  WSAdvValue( oResponse,"_RH3MATINI","string",NIL,"Property cRH3MATINI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3VISAO          :=  WSAdvValue( oResponse,"_RH3VISAO","string",NIL,"Property cRH3VISAO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure ARRAYOFACOMPASOL

WSSTRUCT W0500307_ARRAYOFACOMPASOL
	WSDATA   oWSACOMPASOL              AS W0500307_ACOMPASOL OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_ARRAYOFACOMPASOL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_ARRAYOFACOMPASOL
	::oWSACOMPASOL         := {} // Array Of  W0500307_ACOMPASOL():New()
Return

WSMETHOD CLONE WSCLIENT W0500307_ARRAYOFACOMPASOL
	Local oClone := W0500307_ARRAYOFACOMPASOL():NEW()
	oClone:oWSACOMPASOL := NIL
	If ::oWSACOMPASOL <> NIL 
		oClone:oWSACOMPASOL := {}
		aEval( ::oWSACOMPASOL , { |x| aadd( oClone:oWSACOMPASOL , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_ARRAYOFACOMPASOL
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_ACOMPASOL","ACOMPASOL",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSACOMPASOL , W0500307_ACOMPASOL():New() )
			::oWSACOMPASOL[len(::oWSACOMPASOL)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ACOMPASOL

WSSTRUCT W0500307_ACOMPASOL
	WSDATA   cCODIGO                   AS string
	WSDATA   cCODSOLI                  AS string
	WSDATA   cFILRH3                   AS string
	WSDATA   cMATRIC                   AS string
	WSDATA   cSOLDATA                  AS string
	WSDATA   cSOLICITA                 AS string
	WSDATA   cSTATUS                   AS string
	WSDATA   cTPSOLIC                  AS string
	WSDATA   cVISAO                    AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500307_ACOMPASOL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500307_ACOMPASOL
Return

WSMETHOD CLONE WSCLIENT W0500307_ACOMPASOL
	Local oClone := W0500307_ACOMPASOL():NEW()
	oClone:cCODIGO              := ::cCODIGO
	oClone:cCODSOLI             := ::cCODSOLI
	oClone:cFILRH3              := ::cFILRH3
	oClone:cMATRIC              := ::cMATRIC
	oClone:cSOLDATA             := ::cSOLDATA
	oClone:cSOLICITA            := ::cSOLICITA
	oClone:cSTATUS              := ::cSTATUS
	oClone:cTPSOLIC             := ::cTPSOLIC
	oClone:cVISAO               := ::cVISAO
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500307_ACOMPASOL
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCODIGO            :=  WSAdvValue( oResponse,"_CODIGO","string",NIL,"Property cCODIGO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCODSOLI           :=  WSAdvValue( oResponse,"_CODSOLI","string",NIL,"Property cCODSOLI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFILRH3            :=  WSAdvValue( oResponse,"_FILRH3","string",NIL,"Property cFILRH3 as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cMATRIC            :=  WSAdvValue( oResponse,"_MATRIC","string",NIL,"Property cMATRIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSOLDATA           :=  WSAdvValue( oResponse,"_SOLDATA","string",NIL,"Property cSOLDATA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSOLICITA          :=  WSAdvValue( oResponse,"_SOLICITA","string",NIL,"Property cSOLICITA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSTATUS            :=  WSAdvValue( oResponse,"_STATUS","string",NIL,"Property cSTATUS as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTPSOLIC           :=  WSAdvValue( oResponse,"_TPSOLIC","string",NIL,"Property cTPSOLIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cVISAO             :=  WSAdvValue( oResponse,"_VISAO","string",NIL,"Property cVISAO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return


