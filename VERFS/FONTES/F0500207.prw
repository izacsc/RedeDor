#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
/*---------------------------------------------------------------------------------------------------------------------------
{Protheus.doc} F0500406
Atualização da tabela RCB e RCC
@Author    queizy.nascimento
@Since      01/12/2016
@Version    P12.7
@project MAN0000007423039_EF_002
@Return
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500207()

	//Inclui dados na RCB 
	AtuRCB()
	
	//Inclui dados na RCC
	AtuRCC()
	
Return
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtuRCB
(long_description)
@type function
@author queizy.nascimento
@since 01/12/2016 
@version 1.0
@return ${return}, ${return_description}
@project MAN0000007423039_EF_002
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AtuRCB()
	
	Local nFil
	Local nTamFil    := Len(cFilAnt)
	Local nTamFilRCB := Len(AllTrim(xFilial("RCB")))
	Local aTodasFil  := {}
	Local aFilRCB    := {}
	Local cFilRCB    := cFilAnt
	
	
	If Empty(xFilial("RCB"))
		aFilRCB := {cFilAnt}
	Else
		aTodasFil := FWAllFilial(cEmpAnt,,,.F.)
		For nFil := 1 To Len(aTodasFil)
			cFilRCB := PadR(Left(aTodasFil[nFil],nTamFilRCB),nTamFil)
			If ( AScan(aFilRCB,{|x| x == cFilRCB }) == 0 )
				AAdd(aFilRCB,cFilRCB)
			EndIf
		Next
	EndIf
	
	
	
	For nFil := 1 To Len(aFilRCB)
		RCB->(DbSetOrder(1))
		If !RCB->(DbSeek(IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil]) + "U007"))
			
			RecLock("RCB", .T.)
			RCB->RCB_FILIAL := IIF(Empty(xFilial("RCB")),xFilial("RCB"),aFilRCB[nFil])
			RCB->RCB_CODIGO := 'U007'
			RCB->RCB_DESC   := 'STATUS DAS SOLICITAÇÕES'
			RCB->RCB_ORDEM  := '01'
			RCB->RCB_CAMPOS := 'DESCRICAO'
			RCB->RCB_DESCPO := 'DESCRICAO'
			RCB->RCB_TIPO   := 'C'
			RCB->RCB_TAMAN  := 100
			RCB->RCB_DECIMA := 0 
			RCB->RCB_PICTUR := ''
			RCB->RCB_VALID := ''
			RCB->RCB_PESQ   := '1'
			RCB->RCB_SHOWMA := 'N'
			RCB->RCB_MODULO := '3'
			RCB->(MsUnlock())
			
	
			
		
			PutMv ("MV_PROXNUM", "U007")
			
		EndIf
	Next

Return 
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtuRCC
(long_description)
@type function
@author queizy.nascimento
@since 01/12/2016
@version 1.0
@return ${return}, ${return_description}
@project MAN0000007423039_EF_002
/*///---------------------------------------------------------------------------------------------------------------------------
Static Function AtuRCC()

	Local nFil
	Local nTamFil    := Len(cFilAnt)
	Local nTFilRCC 	 := Len(AllTrim(xFilial("RCC")))
	Local nTamSta		:= 0
	Local aTodasFil  := {}
	Local aFilRCC    := {}
	Local cFilRCC    := cFilAnt
	Local cToken		:= ","
	Local aStatus		:= StrTokArr("MONITORAMENTO DE ABERTURA DE VAGA,"+;
							"SOLICITAÇÃO DE VAGA ABERTA,"+;
							"APROVAÇÃO ORÇAMENTO,"+;
							"REPROVAÇÃO DE ORÇAMENTO,"+;
							"AGUARDANDO APROVAÇÃO,"+;
							"APROVADO SOLICITAÇÃO DE VAGAS,"+;
							"SOLICITAÇÃO DE VAGA REPROVADA,"+;
							"AGUARDANDO EFETIVAÇÃO DO RECRUTAMENTO E SELEÇÃO DE VAGAS,"+;
							"EM RECRUTAMENTO,"+;
							"CANDIDATO CONTRATADO,"+;
							"VAGA CANCELADA,"+;
							"VAGA SUSPENSA,"+;
							"VAGA REABERTA,"+;//13
							"VAGA ENCERRADA,"+;
							"MONITORAMENTO DE AUMENTO DE QUADRO,"+;
							"SOLICITAÇÃO DE AUMENTO DE QUADRO ABERTA,"+;
							"AGUARDANDO APROVAÇÃO DE AUMENTO DE QUADRO,"+;//17
							"SOLICITAÇÃO DE AUMENTO DE QUADRO APROVADO,"+;
							"SOLICITAÇÃO DE AUMENTO DE QUADRO REPROVADO,"+;
							"AGUARDANDO EFETIVAÇÃO ORÇAMENTO,"+;
							"ATENDIDO ORÇAMENTO,"+;
							"REPROVADO ORÇAMENTO,"+;//22
							"AGUARDANDO EFETIVAÇÃO POSTO,"+;
							"ATENDIDO POSTO,"+;
							"REPROVADO POSTO,"+;
							"MONITORAMENTO DE ABERTURA DE FAP,"+;
							"AGUARDANDO A  APROVAÇÃO DA FAP,"+;//27
							"EM EXAME ADMISSIONAL E DOCUMENTOS,"+;
							"SOLICITAÇÃO DE CADASTRO DA FAP REPROVADA,"+;
							"EM ADMISSÃO,"+;
							"CANDIDATO DESISTENTE,"+;
							"CANDIDATO REPROVADO DOCS. INCONSISTENTE,"+;
							"CANDIDATO REPROVADO INAPTO EXAME,"+;//
							"EM ASSINATURA DE CONTRATO,"+;
							"CANDIDATO DESISTENTE ASSINATURA,"+;
							"ATENDIDO FAP CANDIDATO EXTERNO,"+;//
							"AGUARDANDO APROVAÇÃO DO GESTOR CEDENTE,"+;
							"EM EXAME DE MUDANÇA DE FUNÇÃO,"+;
							"EM APROVAÇÃO FAP CANDIDATO INTERNO,"+;
							"AGUARDANDO EFETIVAÇÃO FAP CANDIDATO INTERNO,"+;//
							"MONITORAMENTO DE DESLIGAMENTO DE FUNCIONÁRIOS,"+;
							"SOLICITAÇÃO DE DESLIGAMENTO ABERTA,"+;/////42
							"AGUARDANDO APROVAÇÃO SOLICITAÇÃO DESLIGAMENTO,"+;
							"APROVADO SOLICITAÇÃO DESLIGAMENTO,"+;
							"REPROVADO SOLICITAÇÃO DESLIGAMENTO,"+;//
							"AGUARDANDO EFETIVAÇÃO SOLICITAÇÃO DESLIGAMENTO,"+;
							"ATENDIDO SOLICITAÇÃO DESLIGAMENTO,"+;//47
							"SOLICITAÇÃO DESLIGAMENTO REPROVADA PELA EFETIVAÇÃO,"+;
							"MONITORAMENTO DE SOLICITAÇÃO DE FÉRIAS,"+;
							"SOLICITAÇÃO DE FÉRIAS ABERTA,"+;
							"AGUARDANDO APROVAÇÃO DA SOLICITAÇÃO DE FERIAS,"+;
							"SOLICITAÇÃO DE FERIAS APROVADA,"+;
							"SOLICITAÇÃO DE FÉRIAS REPROVADA,"+;
							"AGUARDANDO EFETIVAÇÃO DO  CSC ADM. DE PESSOAL,"+;//054
							"ATENDIDO PELO CSC ADM. DE PESSOAL,"+;
							"REPROVADO PELO CSC ADM. DE PESSOAL,"+;
							"MONITORAMENTO DE MOVIMENTAÇÃO DE PESSOAL,"+;//57
							"SOLICITAÇÃO DE MOVIMENTAÇÃO DE PESSOAL ABERTA,"+;
							"APROVAÇÃO HEADCOUNT / ORÇAMENTO,"+;
							"REPROVAÇÃO HEADCOUNT,"+;
							"REPROVAÇÃO DE ORÇAMENTO,"+;
							"REPROVAÇÃO DE HEADCOUNT E ORÇAMENTO,"+;
							"AGUARDANDO APROVAÇÃO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"APROVADO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"REPROVADO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"AGUARDANDO EFETIVAÇÃO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"ATENDIDO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"REPROVADO TRANSFERÊNCIA DE FUNCIONÁRIO,"+;
							"MONITORAMENTO DE TREINAMENTO EXTERNO,"+;
							"SOLICITAÇÃO DE TREINAMENTO EXTERNO ABERTO,"+;
							"AGUARDANDO APROVAÇÃO TREINAMENTO EXTERNO,"+;
							"APROVADO TREINAMENTO EXTERNO,"+;
							"REPROVADO MONITORAMENTO DE INCENTIVO A EDUCAÇÃO,"+;
							"AGUARDANDO EFETIVAÇÃO DA SOLICITAÇÃO DE TREINAMENTO EXTERNO,"+;
							"ATENDIDO TREINAMENTO EXTERNO,"+;
							"CANCELADO TREINAMENTO EXTERNO,"+;
							"MONITORAMENTO DE INCENTIVO A EDUCAÇÃO,"+;
							"SOLICITAÇÃO DE INCENTIVO A EDUCAÇÃO ABERTA,"+;
							"AGUARDANDO APROVAÇÃO INCENTIVO A EDUCAÇÃO,"+;
							"APROVADO INCENTIVO A EDUCAÇÃO,"+;
							"REPROVADO INCENTIVO A EDUCAÇÃO,"+;
							"AGUARDANDO A EFETIVAÇÃO INCENTIVO A EDUCAÇÃO,"+;
							"ATENDIDO INCENTIVO A EDUCAÇÃO,"+;
							"CANCELADO INCENTIVO A EDUCAÇÃO"+;
							"FAP ENCERRADA E RETORNO AO RECRUTAMENTO",cToken)
	
	If Empty(xFilial("RCC"))
		aFilRCC := {cFilAnt}
	Else
		aTodasFil := FWAllFilial(cEmpAnt,,,.F.)
		For nFil := 1 To Len(aTodasFil)
			cFilRCC := PadR(Left(aTodasFil[nFil],nTFilRCC),nTamFil)
			If ( AScan(aFilRCC,{|x| x == cFilRCC }) == 0 )
				AAdd(aFilRCC,cFilRCC)
			EndIf
		Next
	EndIf
	
	For nFil := 1 To Len(aFilRCC)
		RCC->(DbSetOrder(1))
		If !RCC->(DbSeek(IIF(Empty(xFilial("RCC")),xFilial("RCC"),aFilRCC[nFil]) + "U007"))
			For nTamSta := 1 to Len(aStatus)
				RCC->(RecLock("RCC", .T.))
				
				RCC->RCC_FILIAL 	:=IIF(Empty(xFilial("RCC")),xFilial("RCC"),aFilRCC[nFil]) 
				RCC->RCC_CODIGO	:= "U007"
				RCC->RCC_FIL	   	:=''
				RCC->RCC_CHAVE	:=''
				RCC->RCC_SEQUEN	:= StrZero(nTamSta, 3)
				RCC->RCC_CONTEU	:= aStatus[nTamSta]
				RCC->(MsUnlock())				
			Next
		EndIf	
	Next
				
Return