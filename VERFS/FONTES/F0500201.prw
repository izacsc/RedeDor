#Include 'Protheus.ch'

/*/{Protheus.doc} F0500201
Fun��o para  monitoramento de solicita��es- RH
@type function
@author queizy.nascimento
@since 03/11/2016
@version 1.0
@param cNumSolicit, character, (Numero do processo RH3)
@param cCodStatus, character, (Numero Status RCC)
@project MAN0000007423039_EF_002
/*/
User Function F0500201(cFilOri,cNumSolicit,cCodStatus)
	
	Local cDescriSolicit 	:= ""   //Descri��o da Solicita��o
	Local aEvento 			:= {} 	// Descri��o do Evento
	Local aApr					:= {}
	Local cFuncao				:= ""   //Fun��o do Solicitante
	Local cMatrSolicit 		:= ""   //Matricula do Solicitante
	Local cNomeSolicit		:= "" 	//Nome do Solicitante
	Local cSetorSolicit		:= "" 	//Departamento do Solicitante
	Local cUnidSolicit		:= "" 	//Filial do Solicitante
	Local cCargoSolicit		:= "" 	//Cargo do Solicitante
	Local cMatrAprov			:= ""	//Matricula do Aprovador
	Local cNomeAprov			:= "" 	//Nome do Aprovador
	Local cSetorAprov			:= "" 	//Departamento do Aprovador
	Local cUnidAprov			:= "" 	//Filial do Aprovador
	Local cCargoAprov			:= ""   //Cargo do Aprovador
	Local cCC					:= ""   //Centro de Custo do Aprovador
	Local cContaOrcament		:= ""  	//Conta Or�ament�ria
	Local cStatus				:= ""   //Descri��o do Status
	Local cFilSol				:= ""
	Local cFilApr				:= ""
	Local nValorMov			:= 0   //Valor envolvido na movimenta��o
	Local dData 				:= Date()     //Data da Grava��o do Monitoramento
	Local cHora				:= SUBSTR(TIME(), 1, 2)+ ":" + SUBSTR(TIME(), 4, 2) //Hora da Grava��o do Monitoramento
	Local nI					:= 0
	Local cCodEvento			:= ""
	Local aAreas            := {SQB->(GetArea()),SRJ->(GetArea()),SRA->(GetArea()),RCC->(GetArea()),;
									RH4->(GetArea()),RH3->(GetArea()),PA7->(GetArea()),PA5->(GetArea()),;
									GetArea()}
	
	DbSelectArea("SRA")
	Set Filter To
	
	//Dever� posicionar na RH3 com o valor passado por par�metro (cNumSolicit) e
	//buscar as seguintes informa��es na RH3, preenchendo as seguintes vari�veis:
	DbSelectArea("RH3")
	RH3->(DbSetOrder(1))
	
	If RH3->(DbSeek(cFilOri + cNumSolicit))
		cMatrSolicit 	:= RH3->RH3_MATINI
		cFilSol		:= RH3->RH3_FILINI
		cMatrAprov 	:= RH3->RH3_MATAPR
		cFilApr		:= RH3->RH3_FILAPR
		cCodEvento 	:= RH3->RH3_XTPCTM
		
	Else
		
		AEval(aAreas, {|x| RestArea(x)} )
		Return .F.
		
	EndIF
	
	//Dever� posicionar na SRA, com a informa��o do campo RH3_MATINI e retornar os seguintes campos preenchendo
	//as seguintes vari�veis:
	
	DbSelectArea("SRA")
	//SRA->(dbGotop())
	SRA->(DbSetOrder(1))
	If SRA->(DbSeek(cFilSol + cMatrSolicit))
		cNomeSolicit 	:= SRA->RA_NOME
		cUnidSolicit 	:= cFilSol
		cCargoSolicit := Posicione("SQ3",1,IIF(Empty(xFilial("SQ3")),xFilial("SQ3"),cFilSol)+SRA->RA_CARGO,"Q3_DESCSUM")
		cFuncao		:= Posicione("SRJ",1,IIF(Empty(xFilial("SRJ")),xFilial("SRJ"),cFilSol)+SRA->RA_CODFUNC,"RJ_DESC")
		cSetorSolicit	:= Posicione("SQB",1,IIF(Empty(xFilial("SQB")),xFilial("SQB"),cFilSol)+SRA->RA_DEPTO,"QB_DESCRIC")
		cCC 			:= SRA->RA_CC
		
	Else
		
		AEval(aAreas, {|x| RestArea(x)} )
		Return .F.
		
	EndIf
	
	
	//Dever� posicionar na SRA novamente, com o campo RH3_MATAPR e retornar os seguintes campos preenchendo
	//as seguintes vari�veis:
	if cCodStatus $ "005/008/020/023/027/037/040/046"
		aApr		:= GetAprov(cFilOri,cNumSolicit)
		cFilApr	:= aApr[1]
		cMatrAprov	:= aApr[2]
	EndIf
	If SRA->(DbSeek(cFilApr+cMatrAprov))
		cNomeAprov 	:= SRA->RA_NOME
		cUnidAprov 	:= SRA->RA_FILIAL
		cCargoAprov 	:= Posicione("SQ3",1,IIF(Empty(xFilial("SQ3")),xFilial("SQ3"),cFilApr)+SRA->RA_CARGO,"Q3_DESCSUM")
		cSetorAprov	:= 	Posicione("SQB",1,IIF(Empty(xFilial("SQB")),xFilial("SQB"),cFilApr)+SRA->RA_DEPTO,"QB_DESCRIC")
		
		
	EndIf
	
	
	DbSelectArea("PA7")
	PA7->(dbGotop())
	PA7->(DbSetOrder(1))
	
	DbSelectArea("RH4")
	RH4->(DBSetOrder(1))
	RH4->(DbGoTop())
	
	If cCodEvento <> "006"
		//Caso o valor da vari�vel cCodEvento seja diferente de "006":
		//Dever� posicionar na TABELA DE ENVENTOS (PA7), com a vari�vel cCodEvento e retornar preenchendo a vari�vel:
		If PA7->(DbSeek(PA7->PA7_FILIAL + cCodEvento))
			cDescriSolicit := PA7->PA7_DESCR
			aAdd(aEvento,PA7->PA7_DESCR)
			
		EndIf
		
		//Posicionar na RH4 buscando a linha que contenha o RH4_CAMPO == �TMP_VLSALA�, e
		// retornar preenchendo a vari�vel:
		
		
		If RH4->(DbSeek(cFilOri+cNumSolicit ))
			While ! Eof() .AND.  RH4_CODIGO = cNumSolicit
				If AllTrim(RH4_CAMPO) == "RCL_SALAR"
					cValSal	:= AllTrim(RH4-> RH4_VALNOV)
					cValSal 	:= StrTran( cValSal, ".", "" )
					cValSal 	:= StrTran( cValSal, ",", "." )
					nValorMov	:= Val(cValSal)
				Elseif AllTrim(RH4_CAMPO) == "QS_VCUSTO"
					cValSal	:= AllTrim(RH4-> RH4_VALNOV)
					cValSal 	:= StrTran( cValSal, ".", "" )
					cValSal 	:= StrTran( cValSal, ",", "." )
					nValorMov	:= Val(cValSal)
				EndIf
				RH4->(Dbskip())
			EndDo
			
		EndIf
		
	Else
		If RH4->(DbSeek(RH3->RH3_FILIAL + cNumSolicit))
			If PA7->(DbSeek(PA7->PA7_FILIAL + cCodEvento))
				cDescriSolicit := PA7->PA7_DESCR
			EndIf
			While	! Eof() .AND.  RH4_CODIGO = cNumSolicit
			If AllTrim(RH4_CAMPO) == "TMP_VLSALA"
				cValSal	:= AllTrim(RH4-> RH4_VALNOV)
				cValSal 	:= StrTran( cValSal, ".", "" )
				cValSal 	:= StrTran( cValSal, ",", "." )
				nValorMov	:= Val(cValSal)
			EndIf
			If AllTrim(RH4_CAMPO) == "TMP_TIPO"
				If AllTrim(RH4->RH4_VALNOV)  == "1"
					aAdd(aEvento,"Aumento Salarial")
					
				ElseIf AllTrim(RH4->RH4_VALNOV)  == "2"
					aAdd(aEvento,"Altera��o de Cargo")
				ElseIf AllTrim(RH4->RH4_VALNOV)  == "3"
					aAdd(aEvento,"Altera��o de Carga Horaria")
					
				ElseIf AllTrim(RH4->RH4_VALNOV)  == "4"
					aAdd(aEvento,"Troca de Turno")
					
				ElseIf AllTrim(RH4->RH4_VALNOV)  == "5"
					aAdd(aEvento,"Transfer�ncia")
				EndIf
			EndIf
			
			RH4->(Dbskip())
		EndDo
	EndIf
EndIf
 

//Com a vari�vel passada por par�metro (cCodStatus), dever� posicionar na tabela auxiliar de STATUS (RCB/RCC)
// e preencher a vari�vel:
DbSelectArea("RCC")
RCC->(DbSetOrder(1))//FILIAL + CODIGO + FIL + CHAVE + SEQUENCIA
RCC->(DbGoTop())
If (RCC->(DbSeek(RCC->RCC_FILIAL +"U007"+"        "+"      " + cCodStatus)))
	cStatus := RCC->RCC_CONTEU
	
EndIf

// Por enquanto deve ficar vazio
cContaOrcament 		= ""

//Grava��o na tabela PA5
DbSelectArea("PA5")

For nI=1 To Len (aEvento)
	PA5->(RECLOCK("PA5", .T.))
	PA5->PA5_FILIAL	:= cFilOri
	PA5->PA5_XCDSTA	:= cCodStatus
	PA5->PA5_XNRSOL	:= cNumSolicit
	PA5->PA5_XDESOL	:= cDescriSolicit
	PA5->PA5_XUNIDA	:= cUnidSolicit
	PA5->PA5_XCODEV	:= cCodEvento
	PA5->PA5_XDESEV	:= aEvento[nI]
	PA5->PA5_FUNCAO	:= cFuncao
	PA5->PA5_XMAT	   	:= cMatrSolicit
	PA5->PA5_XNOME	:= cNomeSolicit
	PA5->PA5_XSETOR	:= cSetorSolicit
	PA5->PA5_XCARGO	:= cCargoSolicit
	PA5->PA5_XMATAP	:= cMatrAprov
	PA5->PA5_XAPROV	:= cNomeAprov
	PA5->PA5_XSEAPV	:= cSetorAprov
	PA5->PA5_XUNAPR	:= cUnidAprov
	PA5->PA5_XCARAP	:= cCargoAprov
	PA5->PA5_XCC		:= cCC
	PA5->PA5_XCONTA	:= cContaOrcament
	PA5->PA5_XSTAT	:= cStatus
	PA5->PA5_XEMPEN	:= nValorMov
	PA5->PA5_XDATA	:= dData
	PA5->PA5_XHORA 	:= cHora
	PA5->PA5_XUSER	:= IIF(Empty(cUserName),GetUserPort(cCodStatus,cNomeSolicit,cNomeAprov), cUserName)
	PA5->(MSUNLOCK("PA5"))
Next nI

AEval(aAreas, {|x| RestArea(x)} )

Return .T.


/*/{Protheus.doc} GetUserPort
Obt�m o usuario do portal
@author Fernando Carvalho
@since 14/12/2016
@version 1.0
@project MAN0000007423039_EF_002
/*/Static Function GetUserPort(cCodStatus,cNomeSolicit,cNomeAprov)
	Local cName	:= ""
	
	if cCodStatus $ "001/002/015/016/069/070/076/077"
		cName := cNomeSolicit
	Else	
		cName := cNomeAprov
	Endif	
Return cName

/*/{Protheus.doc} GetAprov
Obt�m o aprovador
@author Fernando Carvalho
@since 14/12/2016
@version 1.0
@project MAN0000007423039_EF_002
/*/
Static Function GetAprov(cFilOri,cNumSolicit)
	Local aArea	:= GetArea()
	Local cQuery	:= ""
	Local cFilPA5	:= ""
	Local cCodPA5 := ""
	Local cAliasPA5	:= GetNextAlias()
	cQuery	+= ""
	cQuery	+= " SELECT Max(R_E_C_N_O_),PA5_FILIAL, PA5_XNRSOL"
	cQuery	+= " FROM "+ RetSqlName("PA5")
	cQuery	+= " WHERE
	cQuery	+= " PA5_FILIAL	= '"+cFilOri +"'"
	cQuery	+= " AND PA5_XNRSOL = '"+cNumSolicit +"'"
	cQuery	+= " GROUP BY "
	cQuery	+= " PA5_FILIAL,PA5_XNRSOL"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasPA5,.T.,.T.)
	
	dbSelectArea("PA5")
	PA5->(dbSetOrder(1))
	dbSeek((cAliasPA5)->(PA5_FILIAL+PA5_XNRSOL))
	cFilPA5	:= PA5->PA5_XUNAPR
	cCodPA5	:= PA5->PA5_XMATAP
		
	RestArea(aArea)
Return {cFilPA5,cCodPA5}