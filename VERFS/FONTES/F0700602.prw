#include 'protheus.ch'
#include 'apwebsrv.ch'

/*/{Protheus.doc} F0700602
Fun��o para Cadastrar/Alterar o Local de Estoque
@type function
@author queizy.nascimento
@since 26/01/2017
@version 1.0
@param LocalEstoque, ${param_type}, (Descri��o do par�metro)
@Project MAN0000007423041_EF_006
/*/
User Function F0700602(LocalEstoque)
	Local cRetorno := ""
	Local aCampos  := {}
	Local cFil := ""

	aCampos := 	{;
				{"NNR_FILIAL"	, LocalEstoque:cFil		},;
				{"NNR_CODIGO"   , LocalEstoque:cCodigo  },;
				{"NNR_DESCRI"	, LocalEstoque:cDescri  },;
				{"NNR_TIPO"		, LocalEstoque:cTipo    },;
				{"NNR_MSBLQL"	, LocalEstoque:cMsblql  },;   
				{"NNR_XCUSTO"	, LocalEstoque:cCCusto	}}      
    
	cChave := u_RetChave("NNR", aCampos,{1,2})
    
	cRetorno := u_UpsMVCRg ("NNR", cChave, "AGRA045", "NNRMASTER", aCampos) 

    

	aCampos := ASize(aCampos,0)
    aCampos := Nil

Return cRetorno