#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'
#Include 'TopConn.ch'


/*
{Protheus.doc} F0100106()
Tela de Visualiza��o dos Logs da Medi��o de Contrato
@Author		Mick William da Silva
@Since		05/07/2016
@Version	P12.7
@Project    MAN00000462901_EF_001 
*/ 
 
User Function F0100106()

Local oModel    := Nil
Local oExecView := Nil
Local aButtons  := {{.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}, {.F., Nil}, ;
	{.F., Nil}, {.T., "Confirmar"}, {.T., "Fechar"}, {.F., Nil}, {.F., Nil}, ;
{.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}}

DbSelectArea("P07")
P07->(dbSetOrder(5))
P07->(dbSeek(cFilAnt))

P07->( dbGoTop() )

While P07->(!Eof())
	oModel:= FWLoadModel("F0100107")
	oModel:SetOperation(MODEL_OPERATION_VIEW)
	oModel:Activate()
	P07->(DbSkip())
EndDO

FWExecView( "Log de Medi��o" , "F0100107", MODEL_OPERATION_VIEW, /*oDlg*/, {|| .T. } , , /*nPercReducao*/, aButtons, /*bCancel*/ , /*cOperatId*/, /*cToolBar*/, oModel )

Return
