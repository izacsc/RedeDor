#Include 'Protheus.ch'

/*/{Protheus.doc} F0500204
Relat�rio de Solicita��es dos Indicadores
@author Fernando Carvalho
@since 07/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
User Function F0500204()
	Local oReport
	Local oSection
	Local lLandscape	:= .T. //Aponta a orienta��o de p�gina do relat�rio como paisagem       

	If Pergunte("FSW0500204",.t.)

		oReport := TReport():New("F0500204","Relatorio de Indicadores de Monitoramento","FSW0500204",{|oReport| PrintReport(oReport)},;
		"Este relatorio ira imprimir os Indicadores de Monitoramento.",lLandscape)
		
		
		oSection1 := TRSection():New(oReport,"Indicadores",{"PA5"})
		TRCell():New(oSection1,"PA5_XNRSOL"	,"PA5" 	, "N�Solic"			, PesqPict("PA5","PA5_XNRSOL")  	,05,,,,,)
		TRCell():New(oSection1,"PA5_XDESOL"	,"PA5" 	, "Descricao"			, PesqPict("PA5","PA5_XDESOL")  	,15,,,,,)
		TRCell():New(oSection1,"PA5_XCODEV"	,"PA5" 	, "Cod Evento"		, PesqPict("PA5","PA5_XCODEV")  	,05,,,,,)
		TRCell():New(oSection1,"PA5_XDESEV"	,"PA5" 	, "Evento"				, PesqPict("PA5","PA5_XDESEV")  	,15,,,,,)
		TRCell():New(oSection1,"PA5_XSTAT"		,"PA5" 	, "Status"				, PesqPict("PA5","PA5_XSTAT")  		,20,,,,,)
		TRCell():New(oSection1,"PA5_XMAT"		,"PA5" 	, "Matr Solic"		, PesqPict("PA5","PA5_XMAT")  		,05,,,,,)
		TRCell():New(oSection1,"PA5_XNOME"		,"PA5" 	, "Nome Solic"		, PesqPict("PA5","PA5_XNOME")  		,15,,,,,)
		TRCell():New(oSection1,"PA5_XSETOR"	,"PA5" 	, "Setor Solic"		, PesqPict("PA5","PA5_XSETOR")  	,20,,,,,)
		TRCell():New(oSection1,"PA5_XUNIDA"	,"PA5" 	, "Unidade Sol"		, PesqPict("PA5","PA5_XUNIDA")  	,06,,,,,)		
		TRCell():New(oSection1,"PA5_XCARGO"	,"PA5" 	, "Cargo"				, PesqPict("PA5","PA5_XCARGO")  	,20,,,,,)
		TRCell():New(oSection1,"PA5_XMATAP"	,"PA5" 	, "Matr Aprov"		, PesqPict("PA5","PA5_XMATAP")  	,05,,,,,)
		TRCell():New(oSection1,"PA5_XAPROV"	,"PA5" 	, "Nome Aprov"		, PesqPict("PA5","PA5_XAPROV")  	,20,,,,,)				
		TRCell():New(oSection1,"PA5_XSEAPV"	,"PA5" 	, "Setor Apro"		, PesqPict("PA5","PA5_XSEAPV")  	,20,,,,,)		
		TRCell():New(oSection1,"PA5_XCARAP"	,"PA5" 	, "Cargo"				, PesqPict("PA5","PA5_XCARAP")  	,20,,,,,)
		TRCell():New(oSection1,"PA5_XUNAPR"	,"PA5" 	, "Unidade Apr"		, PesqPict("PA5","PA5_XUNAPR")  	,08,,,,,)
		TRCell():New(oSection1,"PA5_XDATA"		,"PA5" 	, "Data"				, PesqPict("PA5","PA5_XDATA")  		,10,,,,,)
		TRCell():New(oSection1,"PA5_XHORA"		,"PA5" 	, "Hora"				, PesqPict("PA5","PA5_XHORA")  		,05,,,,,)
		
		
				
				
		
		oReport:SetLandscape()
		
		oReport:PrintDialog()
	EndIf
Return

/*/{Protheus.doc} F0500204
Relat�rio de Solicita��es dos Indicadores
@author Fernando Carvalho
@since 07/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function PrintReport(oReport)

	#IFDEF TOP
		cAlias := GetNextAlias()
		MakeSqlExp("REPORT")
	
		BEGIN REPORT QUERY oReport:Section(1)
	
			BeginSql alias cAlias
				SELECT
					PA5.PA5_FILIAL
					,PA5.PA5_XNRSOL
					,PA5.PA5_XDESOL
					,PA5.PA5_XUNIDA
					,PA5.PA5_XCODEV
					,PA5.PA5_XDESEV
					,PA5.PA5_FUNCAO
					,PA5.PA5_XMAT
					,PA5.PA5_XNOME
					,PA5.PA5_XSETOR
					,PA5.PA5_XCARGO
					,PA5.PA5_XMATAP
					,PA5.PA5_XAPROV
					,PA5.PA5_XSEAPV
					,PA5.PA5_XUNAPR
					,PA5.PA5_XCARAP
					,PA5.PA5_XCC
					,PA5.PA5_XCONTA
					,PA5.PA5_XSTAT
					,PA5.PA5_XDATA
					,PA5.PA5_XHORA

				FROM
					%table:PA5% PA5
				WHERE
					PA5.PA5_FILIAL 	>= %Exp:mv_par01%
				AND PA5.PA5_FILIAL 	<= %Exp:mv_par02%
				AND PA5.PA5_XNRSOL	>= %Exp:mv_par03%
				AND PA5.PA5_XNRSOL	<= %Exp:mv_par04%
				AND PA5.PA5_XDATA		>= %Exp:Dtos(mv_par05)%
				AND PA5.PA5_XDATA		<= %Exp:Dtos(mv_par06)%
				AND PA5.%notdel%
								
				ORDER BY
				PA5.PA5_XNRSOL, PA5.PA5_XDATA, PA5.PA5_XHORA
			EndSql
	
		END REPORT QUERY oReport:Section(1)
  
		oReport:Section(1):Print(.F.)
	#ENDIF	
Return
