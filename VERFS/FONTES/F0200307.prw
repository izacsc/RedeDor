#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE 'FWMVCDEF.CH'

/*
{Protheus.doc} F0200307()
Webservice responsavel pelo portal e comunica��o com protheus
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param
@Return
*/
User Function F0200307()
Return
//===========================================================================================================
WSSTRUCT Acesso
	WSDATA Matricula		As String 	OPTIONAL
	WSDATA Nome			As String 	OPTIONAL
	WSDATA Admissao		As String 	OPTIONAL
	WSDATA Departame  	As String 	OPTIONAL
	WSDATA CenCusto		As String 	OPTIONAL
	WSDATA Cargo			As String 	OPTIONAL
	WSDATA NMCENTRO      AS String 	OPTIONAL
	WSDATA NMCARGO       AS String 	OPTIONAL
	WSDATA NMDEPART      AS String 	OPTIONAL
ENDWSSTRUCT

WSSTRUCT _Subordinados
	WSDATA Registro		As ARRAY OF Acesso
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Solicitacao
	WSDATA COD				AS String
	WSDATA Matricula		As String
	WSDATA Nome			As String
	WSDATA Status			As String
	WSDATA FilialS       AS String
	WSDATA CODPA3        AS String
	WSDATA FilPa3        AS String
ENDWSSTRUCT

WSSTRUCT _Solicitacao
	WSDATA Registro		As ARRAY OF Solicitacao
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Superior
	WSDATA RANOME			AS String
	WSDATA RAEMAIL		As String
	WSDATA RD4TREE       AS String
	WSDATA SRADEPTO      AS String
	WSDATA QBMATRESP     AS String
ENDWSSTRUCT

WSSTRUCT _Superior
	WSDATA Registro		As ARRAY OF Superior
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT TreiSolici
	WSDATA CodigoTrm  AS String
	WSDATA DataTrm    AS String
	WSDATA StatusTrm  AS String
	WSDATA FilialTrm  AS String
ENDWSSTRUCT

WSSTRUCT _TreiSolici
	WSDATA TrmSL				As ARRAY OF TreiSolici
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Treinamento
	WSDATA Treinamento	AS String
	WSDATA Curso			AS String
	WSDATA Inicio			AS String
	WSDATA Termino		AS String
	WSDATA Horario		AS String
	WSDATA Vagas			AS String
	WSDATA Calendario		AS String
	WSDATA Turma			AS String
	WSDATA Reservado		AS String
	WSDATA FilTrm        AS String
ENDWSSTRUCT

WSSTRUCT _Treinamento
	WSDATA Trm				As ARRAY OF Treinamento
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Institu
	WSDATA RA0ENTIDA   As String
	WSDATA RA0DESC     As String
ENDWSSTRUCT

WSSTRUCT _Institu
	WSDATA Registro    As ARRAY OF Institu
ENDWSSTRUCT

//===========================================================================================================

WSSTRUCT ItensCurso//AcademicGrantFields
	WSDATA benefitCode						AS String				      			//C�digo do beneficio (RIS_CODE - Char - 2)
	WSDATA benefitName						AS String	      						//Nome do Benf�cio (RIS_DESC - Char - 20)
	WSDATA salaryTo							AS Float								//Sal�rio at�
ENDWSSTRUCT

WSSTRUCT IncentivoItens//AcademicGrantList
	WSDATA itens								AS Array Of ItensCurso		//Lista dos subs�dios acad�micos
	WSDATA curseName							AS String								//Nome do Curso
	WSDATA instituteName						AS String								//Nome da institui��o
	WSDATA contact							AS String								//Contato na intitui��o
	WSDATA phone								AS String								//Telefone do contato
	WSDATA ramal								AS String								//Ramal
	WSDATA startDate							AS String								//Data de inicio do curso
	WSDATA endDate							AS String								//Data do fim do curso
	WSDATA monthlyPayment					AS String								//Valor da mensalidade
	WSDATA installmentAmount					AS String								//Qtde. total de parcelas
	WSDATA benefconcedido       			AS String 								//Beneficio Concedido
	WSDATA vltotcurso           			AS String 								//Valor Total Curso
	WSDATA vlinvesanovig        			AS String 								//Valor Investimento Ano Vigente
	WSDATA duracao              			AS String 								//Dura��o
	WSDATA observation						AS String								//Obesrva��o
ENDWSSTRUCT

WSSTRUCT Incentivo//AcademicGrantList
	WSDATA curseName							AS String								//Nome do Curso
	WSDATA instituteName						AS String								//Nome da institui��o
	WSDATA contact							AS String								//Contato na intitui��o
	WSDATA phone								AS String								//Telefone do contato
	WSDATA ramal								AS String								//Ramal
	WSDATA startDate							AS String								//Data de inicio do curso
	WSDATA endDate							AS String								//Data do fim do curso
	WSDATA monthlyPayment					AS String								//Valor da mensalidade
	WSDATA installmentAmount					AS String								//Qtde. total de parcelas
	WSDATA benefconcedido       			AS String 								//Beneficio Concedido
	WSDATA vltotcurso           			AS String 								//Valor Total Curso
	WSDATA vlinvesanovig        			AS String 								//Valor Investimento Ano Vigente
	WSDATA duracao              			AS String 								//Dura��o
	WSDATA observation						AS String								//Obesrva��o
	WSDATA TIPO                           AS String
ENDWSSTRUCT

//===========================================================================================================
WSSTRUCT _RH4val
	WSDATA RA3MAT 	AS String
	WSDATA TMPNOME 	AS String
	WSDATA RA3CALEND 	AS String
	WSDATA RA3CURSO 	AS String
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT Participante
	WSDATA Email 	AS String
	WSDATA Treina 	AS String
ENDWSSTRUCT

//===========================================================================================================
WSSTRUCT SupEmail
	WSDATA QBMATRESP AS String
	WSDATA RANOME    As String
	WSDATA RAEMAIL   As String
ENDWSSTRUCT

WSSTRUCT _SupEmail
	WSDATA Registro    As ARRAY OF SupEmail
ENDWSSTRUCT
//===========================================================================================================
WSSTRUCT EstruturaSolic
	WSDATA Nome        AS String
	WSDATA Matricula   AS String
	WSDATA Setor       AS String
	WSDATA CCusto      AS String
	WSDATA Unidade     AS String
	WSDATA Cargo       AS String
	WSDATA Telefone    AS String
	WSDATA Ramal       AS String
	WSDATA Email       AS String
	WSDATA TpCurso     AS String
	WSDATA Instituicao AS String
	WSDATA NomeCurso   AS String
	WSDATA DtInicio    AS String
	WSDATA DtTermino   AS String
	WSDATA Duracao     AS String
	WSDATA Mensalidade AS String
	WSDATA InvAnoVig   AS String
	WSDATA VlTotCurso  AS String
	WSDATA BenConcedi  AS String
	WSDATA Observacao  AS String
	WSDATA Status      AS String
	WSDATA Solicitante AS String
ENDWSSTRUCT
//===========================================================================================================
WSSERVICE W0200301 DESCRIPTION "WebService Server responsavel pelas rotinas de Incentivo Academico e Treinamento/Evento"
	
	WSDATA Assunto         AS String
	WSDATA Body            AS String
	WSDATA Matricu         AS String
	WSDATA RetEmal         AS Boolean
	WSDATA Ret             AS String
	WSDATA Depto           AS String
	WSDATA _DADOS          AS Participante
	WSDATA _Acesso         AS Acesso
	WSDATA _Treinam        AS Treinamento
	WSDATA _Solici         AS _Solicitacao
	WSDATA _Superi         AS _Superior
	WSDATA RADepart        AS String
	WSDATA _Subord         AS _Subordinados
	WSDATA _Treina         AS _Treinamento
	WSDATA _Inst           AS _Institu
	WSDATA _SupInf         AS _SupEmail
	WSDATA _SupInfP        AS SupEmail
	WSDATA EmployeeFil     AS String
	WSDATA WsNull          As String	         	OPTIONAL	//NULL
	WSDATA _Ret            AS Boolean
	WSDATA _RetIn          AS Boolean
	WSDATA lProc           AS Boolean
	WSDATA CodTrm          AS String
	WSDATA CodCur          AS String
	WSDATA Nome            AS String
	WSDATA CodSol          AS String
	WSDATA Matricula       AS String
	WSDATA Setor           AS String
	WSDATA CCusto          AS String
	WSDATA Unidade         AS String
	WSDATA Cargo           AS String
	WSDATA Telefone        AS String
	WSDATA Ramal           AS String
	WSDATA Email           AS String
	WSDATA TpCurso         AS String
	WSDATA Instituicao     AS String
	WSDATA NomeCurso       AS String
	WSDATA CodRh4          AS String
	WSDATA FIRH3H4         AS String
	WSDATA DtInicio        AS String
	WSDATA DtTermino       AS String
	WSDATA Duracao         AS String
	WSDATA Mensalidade     AS String
	WSDATA InvAnoVig       AS String
	WSDATA VlTotCurso      AS String
	WSDATA BenConcedi      AS String
	WSDATA Observacao      AS String
	WSDATA Status          AS String
	WSDATA Solicitante     AS String
	WSDATA MatAprov        AS String
	WSDATA AcademicoGrant  AS IncentivoItens
	WSDATA TreinaSolicita  AS _TreiSolici
	WSDATA Academico       AS Incentivo
	WSDATA RH4val          AS _RH4val
	WSDATA EmployeeFil     AS String
	WSDATA Solici          AS String
	WSDATA Status          AS String
	WSDATA CodMatri        AS String
	
	WSDATA calendRa2       AS String
	WSDATA cursoRa2        AS String
	WSDATA turmaRa2        AS String
	// RH4
	WSDATA Matri           AS String
	WSDATA NomeSol         AS String
	WSDATA CalenSol        AS String
	WSDATA CursoSol        AS String
	WSDATA TurmaSol        AS String
	WSDATA DtSolic         AS String
	WSDATA Observ          AS String
	WSDATA FilSoliciIn     AS String
	WSDATA FilAprIn        AS String
	WSDATA MatSoliciIn     AS String
	WSDATA MatAprIn        AS String
	WSDATA lStatus         AS Boolean
	WSDATA Matricula       AS String
	WSDATA Calendario      AS String
	WSDATA Tipo            AS String
	WSDATA LocalSol        AS String
	WSDATA Superio         AS String
	WSDATA FilSuperio      AS String
	WSDATA FilMatSup       AS String
	WSDATA CodMatSup       AS String
	WSDATA FilTrm          AS String
	
	WSMETHOD ParamEntre         DESCRIPTION "Pega param�tros do protheus"
	WSMETHOD CriaEntre          DESCRIPTION "Pega as informa��es para passar o e-mail"
	WSMETHOD RetornaPa3         DESCRIPTION "Retorna Solicita��es da pessoa"
	WSMETHOD ListaSolicita      DESCRIPTION "Metodo que retorna a lista"
	WSMETHOD InserePa3          DESCRIPTION "Grava na tabela PA3"
	WSMETHOD BuscaTrm           DESCRIPTION "Pega treinamentos"
	WSMETHOD InfTrm             DESCRIPTION "Pega treinamento especifico"
	WSMETHOD InsereSoli         DESCRIPTION "Insere treinamento especifico"
	WSMETHOD InfPa3             DESCRIPTION "Retorna solicita��o"
	WSMETHOD SoliTrm            DESCRIPTION "Pega solicita��es feitas"
	WSMETHOD AnaSoli            DESCRIPTION "Pega iten da solicita��es feitas"
	WSMETHOD VisSuper           DESCRIPTION "Vis�o dos superiores"
	WSMETHOD StatuTrmIn         DESCRIPTION "Status da pessoa para com o treinamento"
	WSMETHOD ValSuper           DESCRIPTION "Superior acima"
	WSMETHOD PegSuper           DESCRIPTION "Pega superior"
	WSMETHOD InfInsti           DESCRIPTION "Pega Entidade institucionais"
	WSMETHOD PegViInv           DESCRIPTION "Pega vis�o para envio de e-mail de reprova��o"
	WSMETHOD RetInvFu           DESCRIPTION "Retorna dados do funcionario"
	
ENDWSSERVICE
//===========================================================================================================
// Metodo que envia o email
WSMETHOD RetInvFu WSRECEIVE Matricula,FilAprIn WSSEND _Acesso WSSERVICE W0200301
	
	If !(EMPTY(::Matricula)) .AND. !(EMPTY(::FilAprIn))
		cCenCusto  := POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_CC")
		cCargo     := POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_CODFUNC")
		cDepartame := POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_DEPTO")
		cData      := DTOS(POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_ADMISSA"))
		::_Acesso:Matricula  := POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_MAT")
		::_Acesso:Nome		:= POSICIONE("SRA",1,::FilAprIn + ::Matricula,"RA_NOME")
		::_Acesso:Admissao	:= SUBSTR(cData,7,2) + "/" + SUBSTR(cData,5,2) + "/" + SUBSTR(cData,0,4)
		::_Acesso:Departame  := cDepartame
		::_Acesso:CenCusto	:= cCenCusto
		::_Acesso:Cargo		:= cCargo
		::_Acesso:NMCENTRO   := POSICIONE("CTT",1,::FilAprIn + cCenCusto,"CTT_DESC01")
		::_Acesso:NMCARGO    := POSICIONE("SRJ",1,::FilAprIn + cCargo,"RJ_DESC")
		::_Acesso:NMDEPART   := POSICIONE("SQB",1,::FilAprIn + cDepartame,"QB_DESCRIC")
	EndIf
	
Return .T.
//===========================================================================================================
// Metodo que envia o email
WSMETHOD ParamEntre WSRECEIVE Assunto, Body, Matricu WSSEND RetEmal WSSERVICE W0200301
	::RetEmal := U_F0200304(::Assunto, ::Body, ::Matricu)
Return .T.
//===========================================================================================================
// Metodo que pega as informa��es(Nome/Email/Treinamento)
WSMETHOD CriaEntre WSRECEIVE FilMatSup,CodMatSup, calendRa2, cursoRa2, turmaRa2 WSSEND _DADOS WSSERVICE W0200301
	Local aAux := {}
	aAux := U_F0200305(::FilMatSup, ::CodMatSup, ::calendRa2, ::cursoRa2, ::turmaRa2)
	
	::_dados:Email 	:= aAux[1]
	::_dados:Treina 	:= aAux[2]
Return .T.
////===========================================================================================================
// Pega as informa��es de quem est� logado
WSMETHOD RetornaPa3 WSRECEIVE Matricu WSSEND _Solici WSSERVICE W0200301
	Local aAux := {}
	Local nCnt	:= 1
	Local oSolicita
	
	aAux := RetPa3(::Matricu)
	
	If Len(aAux) > 0
		::_Solici := WSClassNew( "_Solicitacao" )
		
		::_Solici:Registro := {}
		oSolicita :=  WSClassNew( "Solicitacao" )
		For nCnt := 1 To Len(aAux)
			oSolicita:COD 		:= aAux[nCnt][1]
			oSolicita:Matricula 	:= aAux[nCnt][2]
			oSolicita:Nome 		:= aAux[nCnt][3]
			oSolicita:Status 		:= aAux[nCnt][4]
			oSolicita:FilialS		:= aAux[nCnt][5]
			oSolicita:CODPA3		:= aAux[nCnt][6]
			oSolicita:FilPa3  	:= aAux[nCnt][7]
			AAdd( ::_Solici:Registro, oSolicita )
			oSolicita :=  WSClassNew( "Solicitacao" )
		Next
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} RetPa3()
Retorna todas as solicita��es de incentivo
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cMatricu, matricula
@Return     aAux
*/
Static Function RetPa3(cMatricu)
	
	Local cQuery     := ''
	Local cAliasRh3  := 'RETPA3'
	Local cAliasrH4  := 'RETRH4'
	Local cCodPa3    := ''
	Local cFilPa3    := ''
	Local nCnt       := 1
	Local aAux       := {}
	
	cQuery := "SELECT	RH3_CODIGO, RH3_MAT, RH3_DTSOLI , RH3_XTPCTM, PA7_DESCR, RH3_STATUS, RH3_MATINI, "
	cQuery += "SRA.RA_NOME, RH3_VISAO, RH3_FILIAL "
	cQuery += "FROM	" + RetSqlName("RH3") + " RH3 "
	cQuery += "INNER JOIN " + RetSqlName("SRA") + " SRA "
	cQuery += "ON SRA.RA_MAT = RH3.RH3_MATINI "
	cQuery += "AND SRA.RA_FILIAL = RH3.RH3_FILINI "
	cQuery += "	AND SRA.D_E_L_E_T_ = ' ' "
	cQuery += "INNER JOIN " + RetSqlName("PA7") + " PA7 "
	cQuery += "ON PA7.PA7_CODIGO = RH3.RH3_XTPCTM "
	cQuery += "	AND PA7.D_E_L_E_T_ = ' ' "
	cQuery += "WHERE	RH3_MATINI = '"+ cMatricu +"' "
	cQuery += "		AND RH3_TIPO = ' ' "
	cQuery += "		AND RH3_XTPCTM = '007' "
	cQuery += "		AND RH3.D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH3_CODIGO"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasRh3)
	
	DbSelectArea(cAliasRh3)
	While ! (cAliasRh3)->(EOF())
		
		AADD(aAux, {(cAliasRh3)->RH3_CODIGO,;
			(cAliasRh3)->RH3_MAT,;
			POSICIONE("SRA",1,(cAliasRh3)->RH3_FILIAL + (cAliasRh3)->RH3_MAT, "RA_NOME"),;
			(cAliasRh3)->RH3_STATUS,;
			(cAliasRh3)->RH3_FILIAL,;
			POSICIONE("RH4",1,(cAliasRh3)->RH3_FILIAL + (cAliasRh3)->RH3_CODIGO + '1', "RH4_VALNOV"),;
			POSICIONE("RH4",1,(cAliasRh3)->RH3_FILIAL + (cAliasRh3)->RH3_CODIGO + '2', "RH4_VALNOV")})
		(cAliasRh3)->(DbSkip())
	End
	(cAliasRh3)->(DbCloseArea())
	
Return aAux

//===========================================================================================================

WSMETHOD ListaSolicita WSRECEIVE EmployeeFil WSSEND AcademicoGrant WSSERVICE W0200301
	Local cMatFun		:= ::EmployeeFil
	Local lRet 		:= .T.
	Local oItem
	Local cQuery		:= GetNextAlias()
	
	BeginSql alias cQuery
		SELECT R.RIS_COD
		, R.RIS_DESC
		, R.RIS_SALATE
		FROM %table:RIS% R
		WHERE (R.RIS_FILIAL = %exp:' '% OR R.RIS_FILIAL = (SELECT RA_FILIAL FROM %table:SRA% WHERE RA_MAT = %exp:cMatFun%))
		AND R.RIS_TPBENE = %exp:'83'%
		AND R.%notDel%
		
	EndSql
	
	If (cQuery)->(Eof())
		SetSoapFault("Incentivo Academico" , "N�o h� incentivos acad�micos cadastrado ou dispon�veis.")
		lRet :=  .F.
	Else
		Self:AcademicoGrant						:= WSClassNew("IncentivoItens")
		Self:AcademicoGrant:itens 				:= {}
		Self:AcademicoGrant:curseName 			:= ''
		Self:AcademicoGrant:instituteName 		:= ''
		Self:AcademicoGrant:contact 			:= ''
		Self:AcademicoGrant:phone 				:= ''
		Self:AcademicoGrant:ramal 				:= ''
		Self:AcademicoGrant:startDate 			:= ''
		Self:AcademicoGrant:endDate 			:= ''
		Self:AcademicoGrant:monthlyPayment 	:= ''
		Self:AcademicoGrant:installmentAmount 	:= ''
		Self:AcademicoGrant:benefconcedido		:= ''
		Self:AcademicoGrant:vltotcurso			:= ''
		Self:AcademicoGrant:vlinvesanovig		:= ''
		Self:AcademicoGrant:duracao				:= ''
		Self:AcademicoGrant:observation 		:= ''
		//Self:AcademicoGrant:tipo 		       := ''
		
		While (cQuery)->(!Eof())
			oItem:= WSClassNew("ItensCurso")
			oItem:benefitCode			:= (cQuery)->RIS_COD
			oItem:benefitName			:= (cQuery)->RIS_DESC
			oItem:salaryTo			:= (cQuery)->RIS_SALATE
			AAdd(Self:AcademicoGrant:itens, oItem)
			(cQuery)->( dbSkip() )
		EndDo
	EndIf
Return lRet

//===========================================================================================================
// Grava na tabela PA3
WSMETHOD InserePa3 WSRECEIVE Nome,Matricula,Setor,CCusto,Unidade,Cargo,Telefone,Ramal,Email,TpCurso,Instituicao,NomeCurso,DtInicio,DtTermino,Duracao,Mensalidade,InvAnoVig,VlTotCurso,BenConcedi,Observacao,Status,FilSoliciIn, MatSoliciIn, FilAprIn, MatAprIn WSSEND _Ret WSSERVICE W0200301
	BEGIN TRANSACTION
		::_Ret := GrvPa3(::Nome,::Matricula,::Setor,::CCusto,::Unidade,::Cargo,::Telefone,::Ramal,::Email,::TpCurso,::Instituicao,::NomeCurso,::DtInicio,::DtTermino,::Duracao,::Mensalidade,::InvAnoVig,::VlTotCurso,::BenConcedi,::Observacao,::Status,::FilSoliciIn, ::MatSoliciIn, ::FilAprIn, ::MatAprIn)
	END TRANSACTION
Return .T.
//===========================================================================================================
/*
{Protheus.doc} GrvPa3()
Grava incentivo
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cNome, Nome
@Param      cMatricula, Matricula
@Param      cSetor, Setor
@Param      cCCusto, Centro de custo
@Param      cUnidade, Unidade
@Param      cCargo, cargo
@Param      cTelefone, telefone
@Param      cRamal, ramal
@Param      cEmail, e-mail
@Param      cTpCurso, tipo do curso
@Param      cInstituic, institui��o realizadora
@Param      cNomeCurso, nome do curso
@Param      cDtInicio, data de inicio do curso
@Param      cDtTermino, data termino do curso
@Param      cDuracao, dura��o do curso
@Param      cMensalida, mensalidade do curso
@Param      cInvAnoVig, investimento do ano vigente
@Param      cVlTotCurs, valor total do curso
@Param      cBenConced, beneficio concedido
@Param      cObservacao, observa��o
@Param      cStatus, status da solicita��o
@Param      cSolicitante, matricula do solicitante
@Return     aAux
*/
Static Function GrvPa3(cNome,cMatricula,cSetor,cCCusto,cUnidade,cCargo,cTelefone,cRamal,cEmail,cTpCurso,cInstituic,cNomeCurso,cDtInicio,cDtTermino,cDuracao,cMensalida,cInvAnoVig,cVlTotCurs,cBenConced,cObservacao,cStatus,cFilSolici, cMatSolici, cFilApr, cMatApr)
	
	Local oModel  := Nil
	Local nX		:= 0
	Local aAux		:= {}
	Local aRegs   := {}
	Local lRet		:= .F.
	
	cDtIni := substr(cdtinicio,7,4) + substr(cdtinicio,4,2) + substr(cdtinicio,0,2)
	cDtFim := substr(cDtTermino,7,4) + substr(cDtTermino,4,2) + substr(cDtTermino,0,2)
	
	cMensalida := StrTran( cMensalida, ".", "" )
	cMensalida := StrTran( cMensalida, ",", "." )
	
	cInvAnoVig := StrTran( cInvAnoVig, ".", "" )
	cInvAnoVig := StrTran( cInvAnoVig, ",", "." )
	
	cVlTotCurs := StrTran( cVlTotCurs, ".", "" )
	cVlTotCurs := StrTran( cVlTotCurs, ",", "." )
	
	cBenConced := StrTran( cBenConced, ",", "." )
	
	cFilApr    := IIF(EMPTY(cFilApr),cFilSolici,cFilApr)
	cMatApr    := IIF(EMPTY(cMatApr),cMatSolici,cMatApr)

	cCod := GetSx8Num("PA3","PA3_XCOD")
	If ! PA3->(DbSeek(cFilSolici + cCod))
		Reclock("PA3", .T.)
		PA3->PA3_FILIAL := xFilial("PA3")
		PA3->PA3_XCOD   := cCod
		PA3->PA3_XMATRI := cMatricula
		PA3->PA3_XSETOR := cSetor
		PA3->PA3_XCENTR := cCCusto
		PA3->PA3_XUNIDA := cUnidade
		PA3->PA3_XCARGO := cCargo
		PA3->PA3_XEMAIL := cEmail
		PA3->PA3_XTELEF := cTelefone
		PA3->PA3_XRAMAL := cRamal
		PA3->PA3_XTIPO  := cTpCurso
		PA3->PA3_XINSTI := cInstituic
		PA3->PA3_XCURS  := cNomeCurso
		PA3->PA3_XINICI := STOD(cDtIni)
		PA3->PA3_XTERMI := STOD(cDtFim)
		PA3->PA3_XDURAC := VAL(cDuracao)
		PA3->PA3_XMENSL := VAL(cMensalida)
		PA3->PA3_XVLTAL := VAL(cInvAnoVig)
		PA3->PA3_XVLTCU := VAL(cVlTotCurs)
		PA3->PA3_XPCBEN := VAL(cBenConced)
		PA3->PA3_XSOLIC := cMatSolici
		PA3->PA3_RSPAPR := cMatApr
		PA3->PA3_XOBSER := cObservacao
		PA3->PA3_XSTATU := cStatus
		PA3->(MsUnlock())
	EndIf
	If (__lSX8)
		ConfirmSx8()
		lRet := .T.
	EndIf
	aRegs := {;
		{"PA3_FILIAL",xFilial("PA3") },;
		{"PA3_XCOD  ",cCod    },;
		{"PA3_XMATRI",cMatricula		 },;
		{"PA3_XSETOR",cSetor			 },;
		{"PA3_XCENTR",cCCusto		 },;
		{"PA3_XUNIDA",cUnidade		 },;
		{"PA3_XCARGO",cCargo			 },;
		{"PA3_XEMAIL",cEmail			 },;
		{"PA3_XTELEF",cTelefone		 },;
		{"PA3_XRAMAL",cRamal			 },;
		{"PA3_XTIPO ",cTpCurso		 },;
		{"PA3_XINSTI",cInstituic		 },;
		{"PA3_XCURS ",cNomeCurso		 },;
		{"PA3_XINICI",cDtIni	        },;
		{"PA3_XTERMI",cDtFim	        },;
		{"PA3_XDURAC",cDuracao       },;
		{"PA3_XMENSL",cMensalida     },;
		{"PA3_XVLTAL",cInvAnoVig     },;
		{"PA3_XVLTCU",cVlTotCurs     },;
		{"PA3_XPCBEN",cBenConced     },;
		{"PA3_XSOLIC",cMatSolici  	 },;
		{"PA3_RSPAPR",cMatApr	  	 },;
		{"PA3_XOBSER",cObservacao	 },;
		{"PA3_XSTATU",cStatus		 };
		}
	
	If lRet
		GrvSoliIn(aRegs,cMatricula,cFilSolici, cMatSolici, cFilApr, cMatApr)
		lRet := .T.
	EndIf
	
Return lRet
//===========================================================================================================
// Pega informa��es dos Treinamentos
WSMETHOD BuscaTrm WSRECEIVE lProc WSSEND _Treina WSSERVICE W0200301
	Local aAux := {}
	Local nCnt := 1
	Local oTreina
	
	aAux := TrmRa2()
	
	If Len(aAux) > 0
		::_Treina := WSClassNew( "_Treinamento" )
		
		::_Treina:Trm := {}
		oTreina :=  WSClassNew( "Treinamento" )
		For nCnt := 1 To Len(aAux)
			oTreina:Calendario 	:= aAux[nCnt][1]
			oTreina:Treinamento 	:= aAux[nCnt][2]
			oTreina:Curso 		:= aAux[nCnt][3]
			oTreina:Inicio 		:= aAux[nCnt][4]
			oTreina:Termino		:= aAux[nCnt][5]
			oTreina:Horario		:= aAux[nCnt][6]
			oTreina:Vagas			:= cValToChar(aAux[nCnt][7] - aAux[nCnt][9])
			oTreina:Turma			:= aAux[nCnt][8]
			oTreina:Reservado		:= cValToChar(aAux[nCnt][9])
			oTreina:FilTrm    	:= aAux[nCnt][10]
			AAdd( ::_Treina:Trm, oTreina )
			oTreina :=  WSClassNew( "Treinamento" )
		Next
	EndIf
Return .T.
//===========================================================================================================
/*
{Protheus.doc} TrmRa2()
Pega treinamentos aptos para cadastro
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return     aAux
*/
Static Function TrmRa2()
	
	Local cQuery 	:= ''
	Local cAliTrm	:= 'TRMRA2'
	Local aAux		:= {}
	
	cQuery := "SELECT RA2_CALEND, RA2_DESC, RA2_CURSO, "
	cQuery += "       RA2_DATAIN, RA2_DATAFI, RA2_HORARI, "
	cQuery += "       RA2_VAGAS, RA2_TURMA, RA2_RESERV, RA2_FILIAL "
	cQuery += "FROM	" + RetSqlName("RA2") + " "
	cQuery += "WHERE 	RA2_VAGAS != RA2_RESERV "
	cQuery += "		AND RA2_REALIZ != 'S' "
	cQuery += "		AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliTrm)
	
	DbSelectArea(cAliTrm)
	While ! (cAliTrm)->(EOF())
		AADD(aAux, {	(cAliTrm)->RA2_CALEND ,;
			(cAliTrm)->RA2_DESC ,;
			(cAliTrm)->RA2_CURSO,;
			(cAliTrm)->RA2_DATAIN,;
			(cAliTrm)->RA2_DATAFI,;
			(cAliTrm)->RA2_HORARI,;
			(cAliTrm)->RA2_VAGAS,;
			(cAliTrm)->RA2_TURMA,;
			(cAliTrm)->RA2_RESERV,;
			(cAliTrm)->RA2_FILIAL;
			}  	)
		(cAliTrm)->(DbSkip())
	End
	(cAliTrm)->(DbCloseArea())
	
Return aAux

//===========================================================================================================
// Metodo que envia o email
WSMETHOD AnaSoli WSRECEIVE CodRh4,FIRH3H4 WSSEND RH4val WSSERVICE W0200301
	
	Local aAuxH4 := {}
	
	aAuxH4 := Rh4Inf(::CodRh4,::FIRH3H4)
	
	If Len(aAuxH4) > 0
		::RH4val:RA3MAT		:= aAuxH4[1][2]
		::RH4val:TMPNOME	  	:= aAuxH4[2][2]
		::RH4val:RA3CALEND	:= aAuxH4[3][2]
		::RH4val:RA3CURSO	  	:= aAuxH4[4][2]
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} Rh4Inf()
Retorna informa��es da solicita��o de treinamento selecionado
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      CodSol, codigo da solici��o de treinamento
@Return     aAux
*/
Static Function Rh4Inf(CodSol, FIRH3H4)
	
	Local cQuery 	:= ''
	Local cAliRh4	:= 'RH4INF'
	Local aAux		:= {}
	
	cQuery := "SELECT RH4_CAMPO, RH4_VALNOV "
	cQuery += "FROM	" + RetSqlName("RH4") + " "
	cQuery += "WHERE RH4_CODIGO = '" + CodSol + "' "
	cQuery += "		AND RH4_FILIAL = '" + FIRH3H4 + "'"
	cQuery += "      AND RH4_CAMPO IN ('RA3_CALEND','RA3_CURSO', 'RA3_MAT', 'TMP_NOME') "
	cQuery += "      AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliRh4)
	
	DbSelectArea(cAliRh4)
	While ! (cAliRh4)->(EOF())
		AADD(aAux, {(cAliRh4)->RH4_CAMPO,(cAliRh4)->RH4_VALNOV,})
		(cAliRh4)->(DbSkip())
	End
	(cAliRh4)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Pega informa��es do Treinamento especifico
WSMETHOD InfTrm WSRECEIVE CodTrm, CodCur, FilTrm WSSEND _Treinam WSSERVICE W0200301
	Local aAux := {}
	
	aAux := TrmInf(::CodTrm, ::CodCur, ::FilTrm)
	
	If Len(aAux) > 0
		::_Treinam:Calendario 	:= aAux[1]
		::_Treinam:Treinamento 	:= aAux[2]
		::_Treinam:Curso 			:= aAux[3]
		::_Treinam:Inicio 		:= aAux[4]
		::_Treinam:Termino		:= aAux[5]
		::_Treinam:Horario		:= aAux[6]
		::_Treinam:Vagas			:= cValToChar(aAux[7])
		::_Treinam:Turma			:= aAux[8]
	EndIf
Return .T.
//===========================================================================================================
/*
{Protheus.doc} TrmInf()
Pega informa��es do treinamento
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCalend, codigo do calendario
@Param      cCurso, codigo do curso
@Return     aAux
*/
Static Function TrmInf(cCalend, cCurso, cFilTrm)
	
	Local cQuery 	:= ''
	Local cAliTrm	:= 'TRMINF'
	Local aAux		:= {}
	
	cFilTrm := StaticCall( F0600401, RetFilSX2, cFilTrm, "RA2")
	
	cQuery := "SELECT RA2_CALEND, RA2_DESC, RA2_CURSO, "
	cQuery += "       RA2_DATAIN, RA2_DATAFI, RA2_HORARI, "
	cQuery += "       RA2_VAGAS, RA2_TURMA, RA2_RESERV "
	cQuery += "FROM	" + RetSqlName("RA2") + " "
	cQuery += "WHERE  RA2_FILIAL = '"+ cFilTrm +"' "
	cQuery += "  	  AND RA2_CALEND = '" + cCalend + "' "
	cQuery += "       AND RA2_CURSO = '" + cCurso + "' "
	cQuery += "       AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliTrm)
	
	DbSelectArea(cAliTrm)
	While ! (cAliTrm)->(EOF())
		AADD(aAux, (cAliTrm)->RA2_CALEND	)
		AADD(aAux, (cAliTrm)->RA2_DESC		)
		AADD(aAux, (cAliTrm)->RA2_CURSO		)
		AADD(aAux, (cAliTrm)->RA2_DATAIN	)
		AADD(aAux, (cAliTrm)->RA2_DATAFI	)
		AADD(aAux, (cAliTrm)->RA2_HORARI	)
		AADD(aAux, ((cAliTrm)->RA2_VAGAS - (cAliTrm)->RA2_RESERV ))
		AADD(aAux, (cAliTrm)->RA2_TURMA		)
		(cAliTrm)->(DbSkip())
	End
	(cAliTrm)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Solicita��es feitas
WSMETHOD SoliTrm WSRECEIVE CodMatri WSSEND TreinaSolicita WSSERVICE W0200301
	Local aAux 	:= {}
	Local nCnt		:= 1
	Local oTreina := Nil
	
	aAux := TrmInfSol(::CodMatri)
	
	If Len(aAux) > 0
		::TreinaSolicita := WSClassNew( "_TreiSolici" )
		
		::TreinaSolicita:TrmSL := {}
		oTreina :=  WSClassNew( "TreiSolici" )
		For nCnt := 1 To Len(aAux)
			oTreina:CodigoTrm 	:= aAux[nCnt][1]
			oTreina:DataTrm 		:= aAux[nCnt][2]
			oTreina:StatusTrm 	:= aAux[nCnt][3]
			oTreina:FilialTrm    := aAux[nCnt][4]
			AAdd( ::TreinaSolicita:TrmSL, oTreina )
			oTreina :=  WSClassNew( "TreiSolici" )
		Next
	EndIf
Return .T.
//===========================================================================================================
/*
{Protheus.doc} TrmInfSol()
Informa��es da solicita��o de treinamento
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, codigo da matricula do solicitante
@Return     aAux
*/
Static Function TrmInfSol(cCodMatri)
	
	Local aArea 	:= GetArea()
	Local cQuery 	:= ''
	Local cAliTrm	:= 'TRMINFSOL'
	Local aAux		:= {}
	
	cQuery := "SELECT RH3_CODIGO, RH3_DTSOLI, RH3_STATUS, RH3_FILIAL "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_FILIAL = '"+FwxFilial("RH3")+"' "
	cQuery += "		AND RH3_XTPCTM = '001' "
	cQuery += "      AND RH3_MATINI = '" + cCodMatri + "' "
	cQuery += "      AND D_E_L_E_T_ = ' ' "
	cQuery += "ORDER BY RH3_CODIGO"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliTrm)
	
	DbSelectArea(cAliTrm)
	While ! (cAliTrm)->(EOF())
		AADD(aAux, {(cAliTrm)->RH3_CODIGO,;
			(cAliTrm)->RH3_DTSOLI,;
			(cAliTrm)->RH3_STATUS,;
			(cAliTrm)->RH3_FILIAL	})
		(cAliTrm)->(DbSkip())
	End
	(cAliTrm)->(DbCloseArea())
	
	RestArea(aArea)
	
Return aAux
//===========================================================================================================
// Pega informa��es do Treinamento especifico
WSMETHOD InfPa3 WSRECEIVE CodSol,FIRH3H4 WSSEND Academico WSSERVICE W0200301
	Local aAux := {}
	
	aAux := Pa3Inf(::CodSol,::FIRH3H4)
	
	If Len(aAux) > 0
		::Academico:curseName      := aAux[6]
		::Academico:instituteName  := aAux[5]
		::Academico:contact        := aAux[3]
		::Academico:phone          := aAux[1]
		::Academico:ramal          := aAux[2]
		::Academico:startDate      := aAux[7]
		::Academico:endDate        := aAux[8]
		::Academico:monthlyPayment := cValToChar(aAux[10])
		::Academico:benefconcedido := cValToChar(aAux[13])
		::Academico:vltotcurso     := cValToChar(aAux[11])
		::Academico:vlinvesanovig  := cValToChar(aAux[12])
		::Academico:duracao        := cValToChar(aAux[9])
		::Academico:tipo           := aAux[4]
		::Academico:observation    := aAux[14]
	EndIf
Return .T.
//===========================================================================================================
/*
{Protheus.doc} Pa3Inf()
Busca informa��es da solicita��o passada
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      CodSol, codigo da solicita��o de incentivo
@Return     aAux
*/
Static Function Pa3Inf(CodSol,FIRH3H4)
	
	Local cQuery 	:= ''
	Local cAliPa3	:= 'PA3INF'
	Local aAux		:= {}
	
	FIRH3H4 := StaticCall( F0600401, RetFilSX2, FIRH3H4, "PA3")
	
	cQuery := "SELECT PA3_XTELEF, PA3_XRAMAL, PA3_XEMAIL, PA3_XTIPO, PA3_XINSTI, PA3_XCURS, PA3_XINICI, "
	cQuery += "PA3_XTERMI, PA3_XDURAC, PA3_XMENSL, PA3_XVLTAL, PA3_XVLTCU, PA3_XPCBEN "
	If TCGetDB() != 'ORACLE'
		cQuery += ", CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), PA3_XOBSER)) AS MEMO "
	Else
		cQuery += ", CAST(PA3_XOBSER AS VARCHAR(200)) AS MEMO " //Esta convers�o de CAST funciona em base Oracle
	EndIf
	cQuery += "FROM	" + RetSqlName("PA3") + " "
	cQuery += "WHERE PA3_XCOD = '" + CodSol + "' "
	cQuery += "		AND PA3_FILIAL = '" + FIRH3H4 + "'"
	cQuery += "		AND D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliPa3)
	
	DbSelectArea(cAliPa3)
	While ! (cAliPa3)->(EOF())
		AADD(aAux, (cAliPa3)->PA3_XTELEF	)
		AADD(aAux, (cAliPa3)->PA3_XRAMAL	)
		AADD(aAux, (cAliPa3)->PA3_XEMAIL	)
		AADD(aAux, POSICIONE("RIS",1,XFILIAL("RIS")+"83"+(cAliPa3)->PA3_XTIPO,"RIS_DESC"))
		AADD(aAux, POSICIONE("RA0",1,XFILIAL("RA0")+(cAliPa3)->PA3_XINSTI,"RA0_DESC"))
		AADD(aAux, (cAliPa3)->PA3_XCURS		)
		AADD(aAux, (cAliPa3)->PA3_XINICI	)
		AADD(aAux, (cAliPa3)->PA3_XTERMI	)
		AADD(aAux, (cAliPa3)->PA3_XDURAC	)
		AADD(aAux, (cAliPa3)->PA3_XMENSL	)
		AADD(aAux, (cAliPa3)->PA3_XVLTAL	)
		AADD(aAux, (cAliPa3)->PA3_XVLTCU	)
		AADD(aAux, (cAliPa3)->PA3_XPCBEN	)
		AADD(aAux, (cAliPa3)->MEMO	)
		//AADD(aAux, POSICIONE("PA3",3,XFILIAL("PA3")+CodSol,"PA3_XOBSER")	)
		(cAliPa3)->(DbSkip())
	End
	(cAliPa3)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Metodo que envia o email
WSMETHOD InsereSoli WSRECEIVE Matri, NomeSol, CalenSol, CursoSol, TurmaSol, DtSolic, Observ, FilSoliciIn, MatSoliciIn, FilAprIn, MatAprIn   WSSEND _Ret WSSERVICE W0200301
	
	Local 	aRegs :={	{"RA3_FILIAL", FWXFILIAL("RA3")  },;
		{"RA3_MAT",    ::Matri   },;
		{"TMP_NOME",   ::NomeSol },;
		{"RA3_CALEND", ::CalenSol},;
		{"RA3_CURSO",  ::CursoSol},;
		{"RA3_TURMA",  ::TurmaSol},;
		{"RA3_DATA",   ::DtSolic },;
		{"TMP_OBS",    ::Observ  }}
	BEGIN TRANSACTION
		::_Ret := GrvSolici(aRegs, ::Matri, ::FilSoliciIn, ::MatSoliciIn, ::FilAprIn, ::MatAprIn )
	END TRANSACTION
Return .T.
//===========================================================================================================
/*
{Protheus.doc} GrvSolici()
Grava solicita��o de treinamento
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cMatricula, matricula do funcionario
@Param      cNome, nome do fucnionario
@Param      cCalendario, codigo do calendario
@Param      cCurso, codigo do curso
@Param      cTurma, codigo da turma
@Param      cDtSolic, data da solicita��o
@Param      cObserv, observa��o
@Param      cMatSolici, matricula do solicitante
@Return     lRet
*/
Static Function GrvSolici(aRegs, cMatricula, cFilSolici, cMatSolici, cFilApr, cMatApr)
	
	Local nReturnCode := 0
	Local nCount      := 0
	Local nItem       := 0
	Local cAliasAi8   := "INFAI8"
	Local cVisao      := ""
	Local lRet        := .F.
	Local aAreas     := {RA2->(GetArea()),RH4->(GetArea()),RH3->(GetArea()),GetArea()}
	
	cQuery := "SELECT	AI8_VISAPV "
	cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
	cQuery += "WHERE AI8_WEBSRV = 'W0200301' AND UPPER(AI8_ROTINA) = UPPER('U_F0200306.apw') "
	cQuery += " AND D_E_L_E_T_ = ' '"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasAi8)
	
	DbSelectArea(cAliasAi8)
	//Pega a vis�o cadastrada
	cVisao := (cAliasAi8)->AI8_VISAPV
	(cAliasAi8)->(DbCloseArea())
	
	cRH4Fil := GetSx8Num("RH3","RH3_CODIGO")
	RH3->(DbSetOrder(1))
	If ! RH3->(DbSeek(xFilial("RH3") + cRH4Fil))
		Reclock("RH3", .T.)
		RH3->RH3_FILIAL  := xFilial("RH3")
		RH3->RH3_CODIGO  := cRH4Fil
		RH3->RH3_MAT     := cMatricula
		RH3->RH3_TIPO    := " "
		RH3->RH3_ORIGEM  := "PORTAL"
		RH3->RH3_STATUS  := IIF(EMPTY(cMatApr),"4","1")
		RH3->RH3_DTSOLI  := DATE()
		RH3->RH3_VISAO   := cVisao
		RH3->RH3_NVLINI  := 0
		RH3->RH3_FILINI  := cFilSolici
		RH3->RH3_MATINI  := cMatSolici
		RH3->RH3_FILAPR  := IIF(EMPTY(cMatApr),cFilSolici,cFilApr)
		RH3->RH3_MATAPR  := IIF(EMPTY(cMatApr),cMatSolici,cMatApr)
		RH3->RH3_NVLAPR  := 99
		RH3->RH3_KEYINI  := "002"
		RH3->RH3_FLUIG   := 0
		RH3->RH3_EMP     := "01"
		RH3->RH3_EMPINI  := "01"
		RH3->RH3_EMPAPR  := "01"
		RH3->RH3_XTPCTM  := "001"
		RH3->(MsUnlock())
		If LEN(GETFUNCARRAY('U_F0500201')) > 0
			U_F0500201(cFilSolici,cRH4Fil,"069")
			U_F0500201(cFilSolici,cRH4Fil,"070")
		EndIf
	EndIf
	
	RH4->(DbSetOrder(1))
	If ! RH4->(DbSeek(xFilial("RH4") + cRH4Fil))
		For nCount:= 1 To Len(aRegs)
			If !Empty(aRegs[nCount, 2])
				Reclock("RH4", .T.)
				RH4->RH4_FILIAL	:= xFilial("RH4")
				RH4->RH4_CODIGO	:= cRH4Fil
				RH4->RH4_ITEM		:= ++nItem
				RH4->RH4_CAMPO	:= aRegs[nCount, 1]
				RH4->RH4_VALNOV	:= aRegs[nCount, 2]
				RH4->RH4_XOBS  	:= ""
				RH4->(MsUnlock())
			EndIf
		Next
	EndIf
	If (__lSX8)
		ConfirmSx8()
		lRet := .T.
	EndIf
	
	AEval(aAreas, {|x| RestArea(x)} )
	
Return lRet
//===========================================================================================================
/*
{Protheus.doc} GrvSoliIn()
Grava solicita��o de incentivo academico
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cMatricula, matricula do funcionario
@Param      aRegs, Array com a informa��es para a RH4
@Param      cMatSolici, matricula do solicitante
@Return     lRet
*/
Static Function GrvSoliIn(aRegs,cMatricula,cFilSolici, cMatSolici, cFilApr, cMatApr)
	
	Local nReturnCode := 0
	Local nCount      := 0
	Local nItem       := 0
	Local cAliasAi8   := "INFAI8"
	Local cVisao      := ""
	Local lRet        := .F.
	Local aAreas     := {RH4->(GetArea()),RH3->(GetArea()),GetArea()}
	
	cQuery := "SELECT	AI8_VISAPV "
	cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
	cQuery += "WHERE AI8_WEBSRV = 'W0200301' AND UPPER(AI8_ROTINA) = UPPER('U_F0200301.apw') "
	cQuery += " AND D_E_L_E_T_ = ' '"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasAi8)
	
	DbSelectArea(cAliasAi8)
	//Pega a vis�o cadastrada
	cVisao := (cAliasAi8)->AI8_VISAPV
	(cAliasAi8)->(DbCloseArea())
	
	cRH4Fil := GetSx8Num("RH3","RH3_CODIGO")
	//cRH4Fil := GetSxeNum("RH3","RH3_CODIGO","RH3_CODIGO" + xFilial("RH3"))
	RH3->(DbSetOrder(1))
	If ! RH3->(DbSeek(xFilial("RH3") + cRH4Fil))
		Reclock("RH3", .T.)
		RH3->RH3_FILIAL  := FWxFilial("RH3")
		RH3->RH3_CODIGO  := cRH4Fil
		RH3->RH3_MAT     := cMatricula
		RH3->RH3_TIPO    := " "
		RH3->RH3_ORIGEM  := "PORTAL"
		RH3->RH3_STATUS  := IIF(cMatApr = cMatSolici,"4","1")
		RH3->RH3_DTSOLI  := DATE()
		RH3->RH3_VISAO   := cVisao
		RH3->RH3_NVLINI  := 0
		RH3->RH3_FILINI  := cFilSolici
		RH3->RH3_MATINI  := cMatSolici
		RH3->RH3_FILAPR  := cFilApr
		RH3->RH3_MATAPR  := cMatApr
		RH3->RH3_NVLAPR  := 99
		RH3->RH3_KEYINI  := "002"
		RH3->RH3_FLUIG   := 0
		RH3->RH3_EMP     := "01"
		RH3->RH3_EMPINI  := "01"
		RH3->RH3_EMPAPR  := "01"
		RH3->RH3_XTPCTM  := "007"
		RH3->(MsUnlock())
		If LEN(GETFUNCARRAY('U_F0500201')) > 0
			U_F0500201(cFilSolici,cRH4Fil,"077")
			U_F0500201(cFilSolici,cRH4Fil,"078")
		EndIf
	EndIf
	
	RH4->(DbSetOrder(1))
	If ! RH4->(DbSeek(xFilial("RH4") + cRH4Fil))
		For nCount:= 1 To Len(aRegs)
			Reclock("RH4", .T.)
			RH4->RH4_FILIAL	:= FWxFilial("RH4")
			RH4->RH4_CODIGO	:= cRH4Fil
			RH4->RH4_ITEM		:= ++nItem
			RH4->RH4_CAMPO	:= aRegs[nCount, 1]
			RH4->RH4_VALNOV	:= IIF(EMPTY(aRegs[nCount, 2]),'',aRegs[nCount, 2])
			RH4->RH4_XOBS  	:= ""
			RH4->(MsUnlock())
		Next
	EndIf
	If (__lSX8)
		ConfirmSx8()
		lRet := .T.
	EndIf
	
	AEval(aAreas, {|x| RestArea(x)} )
	
Return lRet
//===========================================================================================================
// Pega as informa��es de quem est� logado
WSMETHOD VisSuper WSRECEIVE RADepart, Matricula, Tipo WSSEND _Superi WSSERVICE W0200301
	Local aAux := {}
	Local nCnt	:= 1
	Local oSolicita
	
	aAux := RetRd4(::RADepart, ::Matricula, ::Tipo)
	
	If Len(aAux) > 0
		::_Superi := WSClassNew( "_Superior" )
		
		::_Superi:Registro := {}
		oSolicita :=  WSClassNew( "Superior" )
		For nCnt := 1 To Len(aAux)
			oSolicita:RANOME 		:= aAux[nCnt][1]
			oSolicita:RAEMAIL 	:= aAux[nCnt][2]
			oSolicita:RD4TREE    := aAux[nCnt][3]
			oSolicita:SRADEPTO   := aAux[nCnt][5]
			oSolicita:QBMATRESP  := aAux[nCnt][4]
			AAdd( ::_Superi:Registro, oSolicita )
			oSolicita :=  WSClassNew( "Superior" )
		Next
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} RetRd4()
Retorna dados do aprovador
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cDepart, departamento
@Param      cMatri, matricula
@Param      cTipo, tipo de solicita��o
@Return     aAux
*/
Static Function RetRd4(cDepart,cMatri, cTipo)
	
	Local cQuery     := ''
	Local cAliasRd4  := 'RETRD4'
	Local cAliAi8    := 'RETAI8'
	Local cAliasTRE  := 'RETTRE'
	Local cAlTRE     := 'RETTRELOG'
	Local nCnt       := 1
	Local aAux       := {}
	
	If !(EMPTY(cTipo))
		cQuery := "SELECT	AI8_VISAPV "
		cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
		If cTipo == '1'
			cQuery += "WHERE  AI8_WEBSRV = 'W0200301' "
			cQuery += "       AND UPPER(AI8_ROTINA) = UPPER('U_F0200306.apw') "
		Else
			cQuery += "WHERE  AI8_WEBSRV = 'W0200301' "
			cQuery += "       AND UPPER(AI8_ROTINA) = UPPER('U_F0200301.apw') "
		EndIf
		cQuery += " AND D_E_L_E_T_ = ' '"
	Else
		cQuery := "SELECT	RH3_VISAO "
		cQuery += "FROM	" + RetSqlName("RH3") + " "
		cQuery += "WHERE	RH3_CODIGO = '" + cSolicit + "' "
		cQuery += "		AND D_E_L_E_T_ = ' '"
	EndIf
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliAi8)
	
	DbSelectArea(cAliAi8)
	//Pega a vis�o cadastrada
	cVisao := (cAliAi8)->AI8_VISAPV
	(cAliAi8)->(DbCloseArea())
	
	cQuery := "SELECT RD4.RD4_TREE "
	cQuery += "FROM 	" + RetSqlName("RD4") + " RD4 "
	cQuery += "WHERE 	RD4.RD4_CODIDE = (SELECT QB_DEPTO FROM " + RetSqlName("SQB") + " WHERE QB_MATRESP = '" + cMatri + "') "
	cQuery += "		AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "		AND RD4.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlTRE)
	
	DbSelectArea(cAlTRE)
	//Pega a posi��o na tree da vis�o
	cTreeLog := (cAlTRE)->RD4_TREE
	//cTree := (cAliasTRE)->RD4_TREE
	
	(cAlTRE)->(DbCloseArea())
	
	cQuery := "SELECT RD4.RD4_TREE "
	cQuery += "FROM 	" + RetSqlName("RD4") + " RD4 "
	cQuery += "WHERE 	RD4.RD4_CODIDE = (SELECT QB_DEPTO FROM " + RetSqlName("SQB") + " WHERE QB_MATRESP = '" + cDepart + "') "
	cQuery += "		AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "		AND RD4.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTRE)
	
	DbSelectArea(cAliasTRE)
	//Pega a posi��o na tree da vis�o
	cTree := (cAliasTRE)->RD4_TREE
	
	(cAliasTRE)->(DbCloseArea())
	
	cQuery := "SELECT	RD4.RD4_CODIDE, RD4.RD4_TREE,SQB.QB_MATRESP, SRA.RA_NOME, SRA.RA_EMAIL, SRA.RA_DEPTO "
	cQuery += "FROM	" + RetSqlName("RD4") + " RD4, " + RetSqlName("SQB") + " SQB, " + RetSqlName("SRA") + " SRA "
	cQuery += "WHERE	RD4.D_E_L_E_T_ = ' ' "
	cQuery += "		AND SQB.QB_DEPTO = RD4.RD4_CODIDE "
	cQuery += "		AND SQB.QB_MATRESP = SRA.RA_MAT "
	cQuery += "		AND RD4.RD4_TREE >= ( SELECT RD4.RD4_TREE "
	cQuery += "					  FROM " + RetSqlName("RD4") + " RD4 "
	cQuery += "					   WHERE RD4.RD4_FILIAL = '"+FwxFilial("RD4")+"' "
	cQuery += "					    AND RD4.RD4_CODIDE = (	SELECT QB_DEPTO "
	cQuery += "					 							FROM " + RetSqlName("SQB") + " "
	cQuery += "					 							 WHERE QB_MATRESP = '" + cMatri + "'
	cQuery += "												   AND D_E_L_E_T_ = ' ' ) "
	cQuery += "					  AND RD4.RD4_CODIGO = '" + cVisao + "' AND RD4.D_E_L_E_T_ = ' ' "
	cQuery += "				    )	"
	cQuery += " 		AND RD4.RD4_CODIGO = '" + cVisao + "'"
	cQuery += " 		AND RD4.D_E_L_E_T_ = ' ' "
	cQuery += "Order by RD4.RD4_TREE "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasRd4)
	
	DbSelectArea(cAliasRd4)
	While ! (cAliasRd4)->(EOF())
		If VAL((cAliasRd4)->RD4_TREE) <= VAL(cTree)
			AADD(aAux, {(cAliasRd4)->RA_NOME,;
				ALLTRIM((cAliasRd4)->RA_EMAIL),;
				(cAliasRd4)->RD4_TREE,;
				(cAliasRd4)->QB_MATRESP,;
				(cAliasRd4)->RA_DEPTO  })
		EndIf
		(cAliasRd4)->(DbSkip())
	End
	(cAliasRd4)->(DbCloseArea())
	
Return aAux

//===========================================================================================================
// Metodo que envia o email
WSMETHOD StatuTrmIn WSRECEIVE Matricula, Calendario WSSEND lStatus WSSERVICE W0200301
	Local aAux := {}
	
	aAux := TrmStatu(::Matricula, ::Calendario)
	
	If Len(aAux) > 0
		::lStatus := .T.
	Else
		::lStatus := .F.
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} TrmStatu()
Status do treinamento
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, matricula do aprovador
@Param      cCodCalend, calendario do curso
@Return     aAux
*/
Static Function TrmStatu(cCodMatri, cCodCalend)
	
	Local aArea   := GetArea()
	Local nCont   := 1
	Local cQuery  := ''
	Local cAliTrm := 'TRMSTAT'
	Local aAux    := {}
	Local aAux1   := {}
	
	cQuery := "SELECT RH3_CODIGO, RH3_MAT, RH3_TIPO, RH3_ORIGEM, RH3_STATUS "
	cQuery += "FROM	" + RetSqlName("RH3") + " "
	cQuery += "WHERE RH3_MAT = '" + cCodMatri + "' AND RH3_XTPCTM = '001' AND RH3_STATUS IN ('1','4')AND D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliTrm)
	
	DbSelectArea(cAliTrm)
	While ! (cAliTrm)->(EOF())
		AADD(aAux, {	(cAliTrm)->RH3_CODIGO,;
			(cAliTrm)->RH3_MAT,;
			(cAliTrm)->RH3_TIPO,;
			(cAliTrm)->RH3_ORIGEM,;
			(cAliTrm)->RH3_STATUS })
		(cAliTrm)->(DbSkip())
	End
	(cAliTrm)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Metodo que envia o email
WSMETHOD ValSuper WSRECEIVE Matricula, Solici, Status, LocalSol WSSEND lStatus WSSERVICE W0200301
	Local aAux := {}
	
	aAux := SupStatu(::Matricula, ::Solici, ::Status,::LocalSol)
	
	If Len(aAux) > 0
		::lStatus := .F.
	Else
		::lStatus := .T.
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} SupStatu()
Caso tenha mais algum aprovador gravar� os dados do proximo aprovador
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, matricula do aprovador
@Param      cSolicit, calendario do curso
@Param      cStatus, calendario do curso
@Param      cLocal, tipo da solicita��o
@Return     aAux
*/
Static Function SupStatu(cCodMatri, cSolicit, cStatus, cLocal)
	
	Local nCont     := 1
	Local cQuery    := ''
	Local cAliasAi8 := 'RETAI8'
	Local cAliasTRE := 'RETTRE'
	Local cAliasSUP := 'RETSUP'
	Local aAux      := {}
	
	If !(EMPTY(cLocal))
		cQuery := "SELECT	AI8_VISAPV "
		cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
		If cLocal == '1'
			cQuery += "WHERE  AI8_WEBSRV = 'W0200301'
			cQuery += "       AND UPPER(AI8_ROTINA) = UPPER('U_F0200306.apw') "
		Else
			cQuery += "WHERE  AI8_WEBSRV = 'W0200301'
			cQuery += "       AND UPPER(AI8_ROTINA) = UPPER('U_F0200301.apw') "
		EndIf
		cQuery += " AND D_E_L_E_T_ = ' '"
	Else
		cQuery := "SELECT	RH3_VISAO "
		cQuery += "FROM	" + RetSqlName("RH3") + " "
		cQuery += "WHERE	RH3_CODIGO = '" + cSolicit + "' "
		cQuery += "		AND D_E_L_E_T_ = ' '"
	EndIf
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasAi8)
	
	DbSelectArea(cAliasAi8)
	//Pega a vis�o cadastrada
	If !(EMPTY(cLocal))
		cVisao := (cAliasAi8)->AI8_VISAPV
	Else
		cVisao := (cAliasAi8)->RH3_VISAO
	EndIf
	(cAliasAi8)->(DbCloseArea())
	
	cQuery := "SELECT RD4.RD4_TREE "
	cQuery += "FROM 	" + RetSqlName("RD4") + " RD4 "
	cQuery += "WHERE RD4.RD4_FILIAL = '"+FwxFilial("RD4")+"' "
	cQuery += "	 AND RD4.RD4_CODIDE in (SELECT QB_DEPTO "
	cQuery += "	 						 FROM " + RetSqlName("SQB") + " "
	cQuery += "	 						 WHERE QB_MATRESP = '" + cCodMatri + "' "
	cQuery += "	 							AND D_E_L_E_T_ = ''	) "
	cQuery += "		AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "		AND RD4.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTRE)
	
	DbSelectArea(cAliasTRE)
	//Pega a posi��o na tree da vis�o
	cTree := (cAliasTRE)->RD4_TREE
	
	(cAliasTRE)->(DbCloseArea())
	
	cQuery := "SELECT	SQB.QB_MATRESP, SRA.RA_NOME, SRA.RA_EMAIL, RD4.RD4_TREE "
	cQuery += "FROM	" + RetSqlName("RD4") + " RD4, " + RetSqlName("SQB") + " SQB, " + RetSqlName("SRA") + " SRA "
	cQuery += "WHERE	RD4.D_E_L_E_T_ = ' ' "
	cQuery += "		AND SQB.QB_DEPTO = RD4.RD4_CODIDE "
	cQuery += "		AND SQB.QB_MATRESP = SRA.RA_MAT "
	cQuery += "		AND RD4.RD4_TREE < ( SELECT RD4.RD4_TREE "
	cQuery += "					  FROM " + RetSqlName("RD4") + " RD4 "
	cQuery += "					  WHERE RD4.RD4_FILIAL = '"+FwxFilial("RD4")+"' "
	cQuery += "					  	AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "					  	AND RD4.RD4_CODIDE IN (SELECT QB_DEPTO "
	cQuery += "					  							FROM " + RetSqlName("SQB") + " "
	cQuery += "					  							WHERE QB_MATRESP = '" + cCodMatri + "' "
	cQuery += "					  							  AND D_E_L_E_T_ = ''	) "
	cQuery += "					  AND RD4.D_E_L_E_T_ = ' ' "
	cQuery += "				    )"
	cQuery += "		AND RD4.RD4_CODIGO = '" + cVisao + "' "
	cQuery += "		AND RD4.D_E_L_E_T_ = ' ' "
	cQuery += "		Order by RD4.RD4_TREE"

	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSUP)
	
	DbSelectArea(cAliasSUP)
	While !(cAliasSUP)->(EOF())
		If (cAliasSUP)->(RD4_TREE) != cTree
			AADD(aAux,{(cAliasSUP)->(QB_MATRESP),;
				(cAliasSUP)->(RA_NOME),;
				(cAliasSUP)->(RA_EMAIL),;
				(cAliasSUP)->(RD4_TREE)})
		EndIf
		(cAliasSUP)->(DbSkip())
	End
	(cAliasSUP)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Metodo que envia o email
WSMETHOD PegSuper WSRECEIVE Matricula, Solici, LocalSol,FilMatSup,CodMatSup, FIRH3H4 WSSEND _Ret WSSERVICE W0200301
	BEGIN TRANSACTION
		::_Ret := PegSuper(::Matricula, ::Solici, ::LocalSol,::FilMatSup, ::CodMatSup, ::FIRH3H4)
	END TRANSACTION
Return .T.
//===========================================================================================================
/*
{Protheus.doc} PegSuper()
Pega o superior do funcionario
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, matricula do aprovador
@Param      cSolicit, codigo da solicita��o
@Param      cLocal, tipo da solicita��o
@Return     lRet
*/
Static Function PegSuper(cCodMatri, cSolicit, cLocal,cFilMatSup, cCodMatSup,cFIRH3H4)
	
	Local nCont     := 1
	Local cQuery    := ''
	Local cCodPa3   := ''
	Local lRet      := .F.

	If !(EMPTY(cSolicit))
		If !(EMPTY(cCodMatSup))
			If cLocal == '1' .OR. EMPTY(cLocal)
				RH3->(DbSetOrder(1))
				If RH3->(DbSeek(cFIRH3H4 + cSolicit))
					If LEN(GETFUNCARRAY('U_F0500201')) > 0
						U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"071")
					EndIf
					Reclock("RH3", .F.)
					RH3->RH3_FILAPR := cFilMatSup
					RH3->RH3_MATAPR := cCodMatSup
					RH3->(MsUnlock())
					lRet := .T.
				EndIf
			Else
				
				cCodPa3 := POSICIONE("RH4",1,cFIRH3H4 + cSolicit + '2', "RH4_VALNOV")
				
				PA3->(DbSetOrder(3))
				If PA3->(DbSeek(xFilial("PA3") + cCodPa3))
					Reclock("PA3", .F.)
					PA3->PA3_RSPAPR := cCodMatSup
					PA3->(MsUnlock())
				EndIf
				RH3->(DbSetOrder(1))
				If RH3->(DbSeek(cFIRH3H4 + cSolicit))
					If LEN(GETFUNCARRAY('U_F0500201')) > 0
						U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"079")
					EndIf
					Reclock("RH3", .F.)
					RH3->RH3_FILAPR := cFilMatSup
					RH3->RH3_MATAPR := cCodMatSup
					RH3->(MsUnlock())
					lRet := .T.
				EndIf
			EndIf
		Else
			If cLocal == '1'  .OR. EMPTY(cLocal)
				RH3->(DbSetOrder(1))
				If RH3->(DbSeek(cFIRH3H4 + cSolicit))
					If LEN(GETFUNCARRAY('U_F0500201')) > 0
						U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"071")
					EndIf
					Reclock("RH3", .F.)
					RH3->RH3_STATUS := "4"
					RH3->(MsUnlock())
					lRet := .T.
				EndIf
			Else
				cCodPa3 := POSICIONE("RH4",1,cFIRH3H4 + cSolicit + '2', "RH4_VALNOV")
				
				PA3->(DbSetOrder(3))
				If PA3->(DbSeek(xfilial("PA3") + cCodPa3))
					Reclock("PA3", .F.)
					PA3->PA3_XSTATUS	:= "1"
					PA3->(MsUnlock())
				EndIf
				RH3->(DbSetOrder(1))
				If RH3->(DbSeek(cFIRH3H4 + cSolicit))
					If LEN(GETFUNCARRAY('U_F0500201')) > 0
						U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"079")
					EndIf
					Reclock("RH3", .F.)
					RH3->RH3_STATUS	:= "4"
					RH3->(MsUnlock())
					lRet := .T.
				EndIf
			EndIf
		EndIf
	EndIf
	
Return lRet
//===========================================================================================================
// Metodo que envia o email
WSMETHOD InfInsti WSRECEIVE NULLPARAM WSSEND _Inst WSSERVICE W0200301
	Local aAux      := {}
	Local nCnt      := 1
	Local oSolicita := nil
	aAux := InfInstit()
	
	If Len(aAux) > 0
		::_Inst := WSClassNew( "_Institu" )
		
		::_Inst:Registro := {}
		oSolicita :=  WSClassNew( "Institu" )
		For nCnt := 1 To Len(aAux)
			oSolicita:RA0ENTIDA  := aAux[nCnt][1]
			oSolicita:RA0DESC    := aAux[nCnt][2]
			AAdd( ::_Inst:Registro, oSolicita )
			oSolicita :=  WSClassNew( "Institu" )
		Next
	EndIf
	
Return .T.
//===========================================================================================================
/*
{Protheus.doc} InfInstit()
Monta combo com as intitui��es cadastradas
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return     aAux
*/
Static Function InfInstit()
	Local cQuery  := ''
	Local cAlRa0  := 'RETRA0'
	Local nCnt    := 1
	Local aAux    := {}
	
	cQuery := "SELECT RA0_ENTIDA, RA0_DESC "
	cQuery += "FROM " + RetSqlName("RA0") + " "
	cQuery += "WHERE	D_E_L_E_T_ = ' '"
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlRa0)
	DbSelectArea(cAlRa0)
	While !(cAlRa0)->(EOF())
		AADD(aAux, {(cAlRa0)->(RA0_ENTIDA),(cAlRa0)->(RA0_DESC)})
		(cAlRa0)->(DbSkip())
	End
	(cAlRa0)->(DbCloseArea())
	
Return aAux
//===========================================================================================================
// Metodo que envia o email
WSMETHOD PegViInv WSRECEIVE Matricula, Solici, LocalSol,FIRH3H4 WSSEND _SupInf WSSERVICE W0200301
	Local aAux      := {}
	Local nCnt      := 1
	Local oSolicita := NIL
	BEGIN TRANSACTION
		aAux := PegViInv(::Matricula, ::Solici, ::LocalSol, ::FIRH3H4)
	
		If Len(aAux) > 0
			::_SupInf := WSClassNew( "_SupEmail" )
		
			::_SupInf:Registro := {}
			oSolicita :=  WSClassNew( "SupEmail" )
			For nCnt := 1 To Len(aAux)
				oSolicita:QBMATRESP := aAux[nCnt][1]
				oSolicita:RANOME    := aAux[nCnt][2]
				oSolicita:RAEMAIL   := aAux[nCnt][3]
				AAdd( ::_SupInf:Registro, oSolicita )
				oSolicita :=  WSClassNew( "SupEmail" )
			Next
		EndIf
	END TRANSACTION
Return .T.
//===========================================================================================================
/*
{Protheus.doc} PegViInv()
Pega o superior do funcionario
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param      cCodMatri, matricula do aprovador
@Param      cSolicit, codigo da solicita��o
@Param      cLocal, tipo da solicita��o
@Return     aAux
*/
Static Function PegViInv(cCodMatri, cSolicit, cLocal,cFIRH3H4)
	
	Local nCont     := 1
	Local cQuery    := ''
	Local cCodPa3   := ''
	Local cTipVis	:= ''
	Local cFilSol	:= ''
	Local cConc     := "+"
	Local lNOracle	:= TCGetDB() != 'ORACLE'
	Local cAliasAi8 := 'RETAI8'
	Local cAliasTRE := 'RETTRE'
	Local cAliasSUP := 'RETSUP'
	Local cASolicit := 'RETSOL'
	Local aAux      := {}
	
	If cLocal $ '12'
		cQuery := "SELECT	AI8_VISAPV "
		cQuery += "FROM	" + RetSqlName("AI8") + " AI8 "
		cQuery += "WHERE AI8_FILIAL = '"+cFIRH3H4+"' "
		If cLocal == '1'
			cQuery += "AND	AI8_WEBSRV = 'W0200301' AND UPPER(AI8_ROTINA) = UPPER('U_F0200306.apw') "
		Else
			cQuery += "AND	AI8_WEBSRV = 'W0200301' AND UPPER(AI8_ROTINA) = UPPER('U_F0200301.apw') "
		EndIf
		cQuery += " AND D_E_L_E_T_ = ' '"
	Else
		cQuery := "SELECT	RH3_VISAO "
		cQuery += "FROM	" + RetSqlName("RH3") + " "
		cQuery += "WHERE RH3_FILIAL = '"+cFIRH3H4+"' "
		cQuery += "  AND RH3_CODIGO = '" + cSolicit + "' "
		cQuery += "		AND D_E_L_E_T_ = ' '"

	EndIf
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasAi8)
	
	DbSelectArea(cAliasAi8)
	//Pega a vis�o cadastrada
	If cLocal $ '12'
		cVisao := (cAliasAi8)->AI8_VISAPV
	Else
		cVisao := (cAliasAi8)->RH3_VISAO
	EndIf
	(cAliasAi8)->(DbCloseArea())
	
//Retorna o tipo da vis�o 
	TipoOrg(@cTipVis, cVisao)
	
	cMatFezSol	:= POSICIONE("RH3",1,cFIRH3H4 + cSolicit, "RH3_MATINI")
	cFilSol		:= POSICIONE("RH3",1,cFIRH3H4 + cSolicit, "RH3_FILINI")
	If !lNOracle
		cConc := "||"
	EndIf
	
	//Por posto 
	if cTipVis == '1'
	
		cQuery := "	SELECT RA_FILIAL,RA_MAT, RA_NOME, RA_EMAIL "+CRLF
		cQuery += "	FROM "+RetSqlName("SRA")+" (NOLOCK) "+CRLF
		cQuery += "	WHERE D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  AND RA_SITFOLH    <> 'D'  "+CRLF
		cQuery += "	  AND RA_FILIAL"+cConc+"RA_MAT IN ( SELECT RCX_FILFUN"+cConc+"RCX_MATFUN "+CRLF
		cQuery += "	  							FROM "+RetSqlName("RCX")+" (NOLOCK) "+CRLF
		cQuery += "	  							INNER JOIN "+RetSqlName("RDZ")+" (NOLOCK) "+CRLF
		
		if lNOracle
			cQuery += "	  									ON RDZ_CODENT = RCX_FILFUN"+cConc+"RCX_MATFUN "+CRLF
		Else
			cQuery += "	  									ON RDZ_CODENT = RCX_FILFUN"+cConc+"RCX_MATFUN "+CRLF
		EndIf
		
		cQuery += "	  							 	   AND "+RetSqlName("RDZ")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  							INNER JOIN "+RetSqlName("RD0")+"  (NOLOCK) "+CRLF
		cQuery += "	  							        ON RD0_FILIAL  = '' "+CRLF
		cQuery += "	  								   AND RD0_CODIGO = RDZ_CODRD0 "+CRLF
		cQuery += "	  								   AND "+RetSqlName("RD0")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  							WHERE RCX_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "	  								AND "+RetSqlName("RCX")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  								AND RCX_SUBST = '2' "+CRLF
		cQuery += "	  								AND RCX_TIPOCU = '1' "+CRLF
		cQuery += "	  								AND RCX_DTFIM = '' "+CRLF
		
		if  lNOracle
			cQuery += "	  								AND RCX_FILFUN"+cConc+"RCX_POSTO IN ( 	SELECT RD4_FILIDE"+cConc+"RD4_CODIDE "+CRLF
		Else
			cQuery += "	  								AND RCX_FILFUN"+cConc+"RCX_POSTO IN ( 	SELECT RD4_FILIDE"+cConc+"RD4_CODIDE "+CRLF
		EndIf
		
		cQuery += "	  																FROM "+RetSqlName("RD4")+" (NOLOCK) "+CRLF
		cQuery += "	  																WHERE "+RetSqlName("RD4")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  																  AND RD4_FILIAL = '' "+CRLF
		cQuery += "	  																  AND RD4_CODIGO = '"+cVisao+"' "+CRLF
		cQuery += "	  																  AND RD4_TREE >= ( SELECT RD4_TREE "+CRLF
		cQuery += "	  																					FROM  "+RetSqlName("RD4")+" (NOLOCK) "+CRLF
		cQuery += "	  																					WHERE RD4_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "	  																					 AND RD4_CODIGO = '"+cVisao+"' "+CRLF
		cQuery += "	  																					 AND D_E_L_E_T_ ='' "+CRLF
		cQuery += "	  																					 AND RD4_CODIDE = (SELECT RCX_POSTO "+CRLF
		cQuery += "	  																										FROM "+RetSqlName("RCX")+" (NOLOCK) "+CRLF
		cQuery += "	  																										WHERE RCX_FILIAL = '"+cFilSol+"'"+CRLF
		cQuery += "	  																										  AND D_E_L_E_T_ = '' "+CRLF
		cQuery += "	  																										  AND RCX_SUBST = '2'  "+CRLF
		cQuery += "	  																										  AND RCX_TIPOCU = '1' "+CRLF
		cQuery += "	  																										  AND RCX_DTFIM = '' "+CRLF
		cQuery += "	  																										  AND RCX_POSTO"+cConc+"RCX_FILFUN"+cConc+"RCX_MATFUN = (	SELECT RA_POSTO"+cConc+"RA_FILIAL"+cConc+"RA_MAT "+CRLF
		cQuery += "	  																																					FROM "+RetSqlName("SRA")+" (NOLOCK) "+CRLF
		cQuery += "	  																																					WHERE RA_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "	  																																					AND RA_MAT = '"+cMatFezSol+"' "+CRLF
		cQuery += "	  																																					AND RA_SITFOLH    <> 'D'  "+CRLF
		cQuery += "	  																																					AND D_E_L_E_T_ = '') "+CRLF
		cQuery += "	  																										) "+CRLF
		cQuery += "	  																						) "+CRLF
		cQuery += "	  																	) "+CRLF
		cQuery += "	  														) "+CRLF

	Else
		
		cQuery := "	SELECT RA_MAT, RA_NOME, RA_EMAIL "+CRLF
		cQuery += "	FROM "+RetSqlName("SQB")+" (NOLOCK) "+CRLF
		cQuery += "	INNER JOIN "+RetSqlName("RD4")+" (NOLOCK) "+CRLF
		cQuery += "			ON RD4_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "			AND RD4_CODIGO = '"+cVisao+"' "+CRLF
		cQuery += "			AND "+RetSqlName("RD4")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "			AND RD4_TREE >= ( "+CRLF
		cQuery += "								SELECT RD4_TREE "+CRLF
		cQuery += "								FROM "+RetSqlName("RD4")+" (NOLOCK) "+CRLF
		cQuery += "								WHERE RD4_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "						  		  AND RD4_CODIGO = '"+cVisao+"' "+CRLF
		cQuery += "						 		  AND "+RetSqlName("RD4")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "						  		 AND RD4_CODIDE = (	SELECT RA_DEPTO	 "+CRLF
		cQuery += "													FROM "+RetSqlName("SRA")+" (NOLOCK) "+CRLF
		cQuery += "													WHERE RA_FILIAL = '"+cFilSol+"' "+CRLF
		cQuery += "											  		  AND RA_MAT = '"+cMatFezSol+"' "+CRLF
		cQuery += "											          AND "+RetSqlName("SRA")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "											 		  AND RA_SITFOLH    <> 'D'  "+CRLF
		cQuery += "											  	   )"+CRLF
		cQuery += "							) "+CRLF
		cQuery += "			AND RD4_FILIDE = QB_FILIAL "+CRLF
		cQuery += "			AND RD4_CODIDE = QB_DEPTO "+CRLF
		cQuery += "			AND "+RetSqlName("SQB")+".D_E_L_E_T_ = '' "+CRLF
		cQuery += "	INNER JOIN "+RetSqlName("SRA")+" (NOLOCK)  "+CRLF
		cQuery += "	        ON RA_FILIAL = QB_FILRESP "+CRLF
		cQuery += "			AND RA_MAT = QB_MATRESP "+CRLF
		cQuery += "			AND RA_SITFOLH  <> 'D'  "+CRLF
		cQuery += "			AND "+RetSqlName("SRA")+".D_E_L_E_T_ = '' "+CRLF

	EndIf
			
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSUP)
	
	DbSelectArea(cAliasSUP)
	While !(cAliasSUP)->(EOF())
		AADD(aAux, {(cAliasSUP)->(RA_MAT),;
			(cAliasSUP)->(RA_NOME),;
			(cAliasSUP)->(RA_EMAIL);
			})
		(cAliasSUP)->(DbSkip())
	EndDo

	(cAliasSUP)->(DbCloseArea())
	
	If !(Len(aAux) > 0)
		AADD(aAux,{"","",""})
	EndIf
	
	If cLocal == '1' .OR. EMPTY(cLocal) // TREINAMENTO / EVENTO
		RH3->(DbSetOrder(1))
		If RH3->(DbSeek(cFIRH3H4 + cSolicit))
			If LEN(GETFUNCARRAY('U_F0500201')) > 0
				U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"076")
			EndIf
			Reclock("RH3", .F.)
			RH3->RH3_STATUS	:= "3"
			RH3->(MsUnlock())
		EndIf
	Else
		If cLocal == '2' //INCENTIVO ACADEMICO
			
			cCodPa3 := POSICIONE("RH4",1,cFIRH3H4 + cSolicit + '2', "RH4_VALNOV")
			
			PA3->(DbSetOrder(3))
			If PA3->(DbSeek(xfilial("PA3") + cCodPa3))
				Reclock("PA3", .F.)
				PA3->PA3_XSTATUS	:= "2"
				PA3->(MsUnlock())
			EndIf
			RH3->(DbSetOrder(1))
			If RH3->(DbSeek(cFIRH3H4 + cSolicit))
				If LEN(GETFUNCARRAY('U_F0500201')) > 0
					U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"081")
				EndIf
				Reclock("RH3", .F.)
				RH3->RH3_STATUS	:= "3"
				RH3->(MsUnlock())
			EndIf
		EndIf
		If cLocal == '004' //FAP
			RH3->(DbSetOrder(1))
			If RH3->(DbSeek(cFIRH3H4 + cSolicit))
				If LEN(GETFUNCARRAY('U_F0500201')) > 0
					U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"029")
				EndIf
				If FWIsInCallStack('F0500110')
					U_F0500302(RH3->RH3_FILIAL,cSolicit,"RP")
				EndIf
				Reclock("RH3", .F.)
				RH3->RH3_STATUS	:= "3"
				RH3->(MsUnlock())
			EndIf
		EndIf
		If cLocal == '005' //DESLIGAMENTO
			RH3->(DbSetOrder(1))
			If RH3->(DbSeek(cFIRH3H4 + cSolicit))
				If LEN(GETFUNCARRAY('U_F0500201')) > 0
					U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"045")
				EndIf
				If FWIsInCallStack('F0500110')
					U_F0500110(cSolicit,"3")
				EndIf
				Reclock("RH3", .F.)
				RH3->RH3_STATUS	:= "3"
				RH3->(MsUnlock())
			EndIf
		EndIf
		If cLocal == '006' // CARGOS E SALARIOS
			RH3->(DbSetOrder(1))
			If RH3->(DbSeek(cFIRH3H4 + cSolicit))
				If LEN(GETFUNCARRAY('U_F0500201')) > 0
					U_F0500201(RH3->RH3_FILIAL,RH3->RH3_CODIGO,"065")
				EndIf
				If FWIsInCallStack('F0500407')
					U_F0500407(RH3->RH3_FILIAL,RH3->RH3_MAT,cSolicit,RH3->RH3_VISAO)
				EndIf
				Reclock("RH3", .F.)
				RH3->RH3_STATUS	:= "3"
				RH3->(MsUnlock())
			EndIf
		EndIf
	EndIf
Return aAux
//===========================================================================================================
