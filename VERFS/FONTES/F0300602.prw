#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "FWMVCDEF.CH"
#include 'FILEIO.ch'
#define CRLF CHR(13)+CHR(10)

/*
{Protheus.doc} F0300602()
Importagco coparticipagco
@Author     Rogerio Candisani
@Since      14/10/2016
@Version    P12.7 
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300602()
	
	Local aSays			:= {}
	Local aButtons		:= {}
	Local nOpca 		:= 0
	Private cTpForn		:= ''
	Private cDrvAt		:= ''
	Private cDirect		:= ''
	Private	aRet		:= {}
	Private cArqImp		:= ''
	
	Private cCamIni  := 'C:\'+space(97)
	Private cNomeLOG := 'ImportPLANO'+Space(12)
	cCadastro := OemToAnsi("RHO - Co-Participa��o e Reembolso") 	// "RHO - Co-Participacao e Reembolso"
	
	AADD(aSays,OemToAnsi("Esta rotina importa valores para os seguintes arquivos: ") + CRLF )   	// "Esta rotina importa valores para os seguintes arquivos: "
	AADD(aSays,OemToAnsi("RHO - Co-Participacao e Reembolso ") + CRLF )   	// "RHO - Co-Participacao e Reembolso "
	AADD(aSays,OemToAnsi("Conforme definido na rotina de cadastro de Layout de Importa��o.") )   			// "Conforme definido na rotina de cadastro de Layout de Importacao."
	AADD(aButtons, { 5,.T.,{|| ParamImp()  } } )
	AADD(aButtons, { 1,.T.,{|o| nOpca := 1,if(U_F0300608(),If(MsgYesNo(OemToAnsi("Confirma a importa��o da Co-partici��o referente ao Plano tipo  "+Iif(cTpForn=='1','SA�DE','ODONTOL�GICO')+"?"),OemToAnsi("Atencao")),FechaBatch(),nOpca:=0),(.F.,nOpca:=0))}} )
	AADD(aButtons, { 2,.T.,{|o| FechaBatch() }} )
	
	FormBatch( cCadastro, aSays, aButtons )
	If nOpca == 1
		Processa({|lEnd| F03006Proc(),"Importa��o de Coparticipa��o"})   //"Importacao de Co-particicpagco"
	EndIf
	
Return( Nil )

/*{Protheus.doc} ParamImp
(long_description)
@type function
@author
@since 22/12/2016
@version 1.0
@return ${return}, ${return_description}
*/
Static Function ParamImp()
	
	Local aPergs     := {}
	Local cParaFil   := Space(TamSx3('RHO_FILIAL')[1])
	Local cExcel		:= '1=Sim'
	Local dData		:= date()
	Private nTipImp  := '1=Sa�de'
	
	aRet	:= {}
	
	aAdd( aPergs ,{1,"Selecione Para Filial:",cParaFil,"@!",'.T.','SM0','.T.',50,.T.})
	aAdd( aPergs ,{2,'Selecione o Plano:',nTipImp,{'1=Sa�de','2-Odontol�gico'},60,'.T.',.T.})
	aAdd( aPergs ,{6,'Arquivo a Importar:',cCamIni,'@!','.T.','.T.',100,.F.,"Arquivos .TXT|*.TXT",'C:\',GETF_LOCALHARD} )
	aAdd( aPergs ,{1,'Nome do Arquivo de LOG:',cNomeLOG,'@!','.T.',,'.T.',100,.T.})
	aAdd( aPergs ,{2,'Deseja abrir o log?',cExcel,{'1=Sim','2-N�o'},60,'.T.',.T.})	
	aAdd( aPergs ,{1,"Data da Importa��o:",dData,"",'.T.','','.T.',50,.T.})

	ParamBox(aPergs ,"Parametros ",aRet)
	
Return
/*
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
11111111111111111111111111111111111111111111111111111111111111111111111111111
11ZDDDDDDDDDDBDDDDDDDDDDDDBDDDDDDDBDDDDDDDDDDDDDDDDDDDDDBDDDDDDBDDDDDDDDDD?11
113Funcao    3 F03006PROC 3 Autor 3 Rogerio Candisani   3 Data 3 17/10/16 311
11CDDDDDDDDDDEDDDDDDDDDDDDADDDDDDDADDDDDDDDDDDDDDDDDDDDDADDDDDDADDDDDDDDDD411
113Descricao 3 Leitura Arquivo Texto e Gravacao no Arq. Valores Variaveis.311
11CDDDDDDDDDDEDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD411
113Uso       3 		                                                     311
11@DDDDDDDDDDADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDY11
11111111111111111111111111111111111111111111111111111111111111111111111111111
_____________________________________________________________________________/*/
Static Function F03006PROC()
	
	//	Local aReg:={}
	Local cArquivo:= aRet[3]
	Local lExcel	:= AllTrim(aRet[5]) == '1'
	Local dDtImp	:= aRet[6]
	Local nX:= 0
	Local cCPF:= ""
	Local ctmpMat:=""
	Local dDtOcor := ""
	Local cOrigem := ""
	Local cCodFor := ""
	Local cCodigo := ""
	Local cTplLan := ""
	Local cPD := ""
	Local nVlrFun:= 0
	Local nVlrEmp:= 0
	Local dCompPg:= ""
	Local cObserv:= ""
	Local aCop:={}
	Local cAssis  := ""
	Local cSRAMat := ""
	Local oFile
	Local cPathFile
	Local cPathF
	Local nContLin:= 0
	Local aRetLog	:= {}
	Local aRetTit	:= {}
	Local nCont	:= 0	
	Local cLog		:= ""
		
	Private aLog1 := {}
	Private aLog3 := {}
	Private aLog4 := {}
	Private aLog5 := {}
	Private aTitLog1 := {}
	Private aTitLog3 := {}
	Private aTitLog4 := {}
	Private aTitLog5 := {}
	Private aLogExcel:= {}
	//Monta o cabe�alho do CSV
	aAdd(aLogExcel,{1,"Linha;CPF;Matr�cula;Situa��o;Mensagem"})
	oFile := FWFileReader():New(cArquivo)
	//gerar importagco dos arquivos
	//mudan�a de rotina para nao dar estouro de string
	//TXT := fReadStr( nHandle,nBytes )
	
	//Abertura do arquivo texto.
	//----------------------------------------------------------------------
	
	// Nairan - Fun��o criada a pedido do Aribaldo/Andr� Magalh�es no dia 08/01/17 �s 10:30
	ExcRegitros(dDtImp,Substring(aRet[2],1,1))
	
	IF Empty(cArquivo)
		Aviso( "Finalizado", "Processo de Importa��o Finalizado", {"Ok"} )		
	EndIf
	
	If !(oFile:Open())
		MsgAlert("O arquivo de nome " + cArquivo + " n�o pode ser aberto! Verifique os parametros.", "Aten��o!")
		Return
	EndIf
	
	//Ler os aqrquivos e seus respectivos tipos
	//	For  nX:= 1 to len(aReg)
	while (oFile:hasLine())
		//layout dos arquivos
		//TIPO 0						Data de Referjncia do Movimento
		//TIPO DE REGISTRO	001	001	NN	01	-	0
		//DATA POSTERIOR COMPET	002	007	NN	06	00	Mmaaaa
		//DATA COMPETJNCIA	008	013	NN	06	00	Mmaaaa
		//TIPO MOVIMENTO	014	014	AN	01	-	S (fixo)  // D (fixo)  /; D=Dental/ S=Saude
		//FILLER	015	300	AN	286	-	Brancos
		//cAssis    := subst(aReg[x],14,1)
		cReg:= oFile:GetLine()
		nContLin ++
		If subst(cReg,1,1) == "0"
			dCompPg:= subst(cReg,8,6)
			cAssis := subst(cReg,14,1)
			
			// Data de ocorrencia deve ser o multimko dia do mes. para quem utilizar a coparticipa��o,
			// pois s� deve apresentar um unico registro para o titular e um unico registro para
			// o dependente
			
			/*Nairan - Alterado dia 08/01/17 a pedido do Aribaldo de acordo com levantamento realizado com o Cliente
			If subst(cReg,8,2)<> "02"
				dDtOcor:= "30/"+subst(cReg,8,2)+"/"+subst(cReg,10,4)
			Else
				dDtOcor:= "28/"+subst(cReg,8,2)+"/"+subst(cReg,10,4)
			Endif
			*/
			
			dDtOcor := DTOC(dDtImp)
			
		Endif
		//TIPO 1						Informagues do Titular
		//TIPO DE REGISTRO	001	001	NN	01	-	1
		//EMPRESA	002	041	AN	40	-	Nome da Empresa
		//CGC	042	059	AN	18	-	CGC da Empresa  - 99.999.999/9999-99
		//MOEDA	060	069	AN	10	-	REAL
		//TITULAR	070	109	AN	40	-	Nome do Titular
		//CPF	110	123	AN	14	-	CPF do Titular  - 999.999.999-99
		//COMPONENTE	124	129	AN	06	-	5* a 10* Posigco do Cargo/Ocupagco
		//MATRMCULA	130	149	AN	20	-	Cartco do Titular
		//DATA INMCIO	150	159	AN	10	-	dd/mm/aaaa
		//DATA FIM	160	169	AN	10	-	dd/mm/aaaa
		//DATA EMISSCO	170	179	AN	10	-	dd/mm/aaaa
		//MATRICULA ESPECIAL	180	191	AN	12	-	ID da EMPRESA
		//SUBFATURA	192	194	NN	03	-	Csdigo da subfatura
		//FILLER	195	300	AN	106	-	Brancos
		If subst(cReg,1,1) == "1"
			cCPF:= subst(cReg,110,14)
			cCPF:=strtran(cCPF,'.','')
			cCPF:=strtran(cCPF,'-','')
			//verificar se existe registro no Protheus
			ctmpMat:=GetNextAlias()
			BeginSql Alias ctmpMat
				SELECT RA_MAT, RA_SITFOLH, RA_AFASFGT
				FROM %table:SRA% SRA
				WHERE RA_CIC = %Exp:cCPF%
				AND SRA.%NotDel%
			EndSql
			DbSelectArea(ctmpMat)
			While !EOF()
				If (ctmpMat)->RA_SITFOLH != "D"
					cSRAMat := (ctmpMat)->RA_MAT
				else
					If AllTrim((ctmpMat)->RA_AFASFGT) == "N1" .Or. AllTrim((ctmpMat)->RA_AFASFGT) == "N2"
						fIncLog({"03","Linha: "+cValToChar((nContLin))+" / CPF "+Alltrim(cCPF)+" / Matricula "+Alltrim((ctmpMat)->RA_MAT)+" Transferido."})
						aAdd(aLogExcel,{2,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim((ctmpMat)->RA_MAT)+";N�o;Transferido"})
					Else
						fIncLog({"03","Linha: "+cValToChar((nContLin))+" / CPF "+Alltrim(cCPF)+" / Matricula "+Alltrim((ctmpMat)->RA_MAT)+" Demitido."})					
						aAdd(aLogExcel,{2,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim((ctmpMat)->RA_MAT)+";N�o;Demitido"})
					Endif
				EndIf
				(ctmpMat)->(DbSkip())
			Enddo
			(ctmpMat)->(DbCloseArea())
			If Empty(cSRAMat)
				fIncLog({"01","Linha: "+cValToChar(nContLin)+" / CPF "+Alltrim(cCPF)+" nao encontrado no sistema."})
				aAdd(aLogExcel,{2,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim((ctmpMat)->RA_MAT)+";N�o;N�o encontrado no sistema"})
				Loop
			Endif
		Endif
		
		//TIPO 2						Informagues sobre Utilizagues
		//TIPO DE REGISTRO	001	001	NN	01	-	2
		//DATA UTILIZAGCO	002	011	AN	10	-	dd/mm/aaaa
		//CSDIGO USUARIO	012	013	NN	02	-	Csdigo do Paciente
		//NOME USUARIO	014	053	AN	40	-	Nome do Paciente
		//NOME PRESTADOR	054	123	AN	70	-	Nome do Prestador de Servigo
		//TIPO MEIO UTILIZAGCO	124	125	AN	02	-	SR (Reembolso) / RR (Rede)
		//CGC/CPF PRESTADOR	126	143	AN	18	-	CGC/CPF do Prestador de Servigo
		//TIPO DE SERVIGO	144	168	AN	25	-	Nome do Procedimento
		//NZMERO DA SR	169	175	NN	07	-	Zeros
		//VALOR ORIGINAL	176	187	AN	12	-	9.999.999,99
		//Valor Cobrado (Extrato)
		//VALOR REEMBOLSO	188	199	NA	12	-	9.999.999,99
		//Valor Tabela B.S. (Extrato)
		//VALOR PARTICIPAGCO	200	211	NA	12	-	9.999.999,99
		//Valor Participagco (Extrato)
		//VALOR CO-PARTICIPAGCO	212	223	NA	12	-	9.999.999,99
		//Valor Excedente (Extrato)
		//DOCUMENTO	224	235	NN	12	-	Nzmero do Documento/Senha
		//PROCEDIMENTO	236	243	NN	08	-	Csdigo do Procedimento
		//SEQ. PROCEDIMENTO	244	250	NA	07	-	Sequencial Procedimento
		//CERTIFICADO	251	257	NN	07	-	Csdigo do Certificado do Segurado
		//MATRICULA ESPECIAL	258	269	AN	12	-	Matrmcula Especial do Segurado
		//SUBFATURA	270	272	NN	03	-	Csdigo da subfatura
		//FILLER	273	300	NA	28	-	Brancos
		
		If subst(cReg,1,1) == "2"
			cCodigo := subst(cReg,12,2)
			//cTpForn:= cTpForn:= IIF(MV_PAR02 == 1,"1","2")
			If cAssis = "S"
				cPD:= fTabela("U002",1,6)   //Posicione("RHK",1,xFilial("RHK")+cSRAMat,"RHK_PD")
			Else
				cPD:= fTabela("U002",1,7)
			Endif
			If cCodigo != "00"
				cCodigo := RetDep(cSRAMat,cCodigo)
			EndIf
			If Empty(cCodigo)
				fIncLog({"02","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Carteirinha "+subst(cReg,12,2)+" nao encontrada no sistema."})
				aAdd(aLogExcel,{2,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(cSRAMat)+";N�o; Carteirinha "+subst(cReg,12,2)+" nao encontrada no sistema."})
				Loop
			Endif
			cCodFor:= Posicione("RHK",1,aRet[1]+cSRAMat,"RHK_CODFOR")
			nVlrFun:= subst(cReg,200,12)
			nVlrEmp:= 0
			cObs:= subst(cReg,144,25)
			cOrigem:= IIF(cCodigo == "00","1","2")
			//cTpForn:= IIF(MV_PAR02 == 1,"1","2")
			cTpLan:= "1" // sempre sera co=participagco
		Endif
		
		//soma os valores por matricula e beneficiario
		If subst(cReg,1,1) == "2"
			//	dDtOcor:= subst(aReg[x],2,8)
			nVlrFun:=	strtran(nVlrFun,",","")
			nVlrFun:=	strtran(nVlrFun,".","")
			nVlrFun:= Val(nVlrFun)
			nVlrFun :=  nVlrFun / 100 // 2 casas decimais
			IF nVlrFun <> 0
				//verificar se ja existe este beneficiario e plano e somar
				//SetUniqueLine( { 'RHO_DTOCOR', 'RHO_TPFORN', 'RHO_CODFOR', 'RHO_ORIGEM','RHO_CODIGO','RHO_PD','RHO_COMPPG' } )
				If ValType(dDtOcor) == "C"
					If ValType(cTpForn) == "C"
						If ValType(cCodFor) == "C"
							If ValType(cOrigem) == "C"
								If ValType(cCodigo) == "C"
									If ValType(cPD) == "C"
										If ValType(dCompPg) == "C"
											//dDtOcor+cTpForn+cCodFor+cOrigem+cCodigo+cPD+dCompPg
											nPosX := aScan( aCop, { |nX| nX[1]+nX[3]+nX[5]+nX[6]+nX[4]+nX[7]+nX[9]+nX[2] == cSRAMat+dDtOcor+cTpForn+cCodFor+cOrigem+cCodigo+cPD+dCompPg } ) // compara o setUniqueLine
											If nPosX = 0 // Adiciona os itens nco encontrados no primeiro array
												Aadd(aCop,{	cSRAMat  ,; //1
												dCompPg  ,; //2
												dDtOcor  ,; //3
												cOrigem  ,; //4
												cTpForn  ,; //5
												cCodFor  ,; //6
												cCodigo  ,; //7
												cTpLan   ,; //8
												cPD      ,; //9
												nVlrFun  ,; //10
												cObs     }) //11
											Else
												aCop[nPosX][10]:= nVlrFun + aCop[nPosX][10]
											Endif
										ELse
											oFile:Close()
											fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Dado incorreto na data de compet�ncia."})
											aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Dado incorreto na data de compet�ncia"})
											Help( ,, 'HELP',, "Dado incorreto na data de compet�ncia verifique antes de prosseguir", 1, 0)
											Return
										EndIf
									Else
										oFile:Close()
										fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Dado incorreto na informa��o da verba."})
										aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Dado incorreto na informa��o da verba"})
										Help( ,, 'HELP',, "Dado incorreto na informa��o da verba verifique antes de prosseguir", 1, 0)
										Return
									EndIf
								Else
									oFile:Close()
									fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Dado da Sequ�ncia Depen/Agregado invalido."})
									aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Dado da Sequ�ncia Depen/Agregado invalido"})
									Help( ,, 'HELP',, "Dado da Sequ�ncia Depen/Agregado inv�lido verifique antes de prosseguir", 1, 0)
									Return
								EndIf
							Else
								oFile:Close()
								fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Codigo da origem do arquivo inv�lido."})
								aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Codigo da origem do arquivo inv�lido"})
								Help( ,, 'HELP',, "C�digo da origem do arquivo inv�lido verifique antes de prosseguir", 1, 0)
								Return
							EndIf
						Else
							oFile:Close()
							fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Codigo do fornecedor invalido."})
							aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Codigo do fornecedor invalido."})
							Help( ,, 'HELP',, "C�digo do fornecedor inv�lido verifique antes de prosseguir", 1, 0)
							Return
						EndIf
					Else
						oFile:Close()
						fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Tipo do fornecedor invalido."})
						aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Tipo do fornecedor invalido."})
						Help( ,, 'HELP',, "Tipo do fornecedor inv�lido verifique antes de prosseguir", 1, 0)
						Return
					EndIf
				Else
					oFile:Close()
					fIncLog({"04","Linha: "+cValToChar(nContLin)+" / Matricula "+Alltrim(cSRAMat)+" -> Data da Ocorr�ncia invalido."})
					aAdd(aLogExcel,{3,cValToChar((nContLin))+";"+Alltrim(cCPF)+";"+Alltrim(ctmpMat)+";N�o; Data da Ocorr�ncia invalido."})
					Help( ,, 'HELP',, "Data da Ocorr�ncia inv�lido verifique antes de prosseguir", 1, 0)
					Return
				EndIf
			Endif
		Endif
	EndDO
	oFile:Close()
	//inclui os registros de co-participagco
	If Len(aCop) > 0
		InclCop(aCop)
	EndIf
	
	//Criar o arquivo de Log
	/*
	fMakeLog(	aLogFile 	,;	//Array que contem os Detalhes de Ocorrencia de Log
				aLogTitle	,;	//Array que contem os Titulos de Acordo com as Ocorrencias
				cPerg		,;	//Pergunte a Ser Listado
				lShowLog	,;	//Se Havera "Display" de Tela
				cLogName	,;	//Nome Alternativo do Log
				cTitulo		,;	//Titulo Alternativo do Log
				cTamanho	,;	//Tamanho Vertical do Relatorio de Log ("P","M","G")
				cLandPort	,;	//Orientacao do Relatorio ("P" Retrato ou "L" Paisagem )
				aRet		,;	//Array com a Mesma Estrutura do aReturn
				lAddOldLog	 ;	//Se deve Manter ( Adicionar ) no Novo Log o Log Anterior
			  )
	*/
//?	fMakeLog(aLog1,aTitLog1,/*cPerg*/,.F.,cNomeLOG,/*cTitulo*/,/*cTamanho*/,/*cLandPort*/,/*aRet*/,.F.)
	
	If !Empty(aLog1)
		cLog := ""
		Aadd(aRetTit,aTitLog1[1])
		For nCont := 1 To Len(aLog1)
			cLog += aLog1[nCont][1] + CRLF
		Next
		cLog += "Total de Arquivos: "+cValtoChar(Len(aLog1))
		aAdd(aRetLog,{cLog})
	EndIf
	
	If !Empty(aLog3)
		cLog := ""
		Aadd(aRetTit,aTitLog3[1])
		For nCont := 1 To Len(aLog3)
			cLog += aLog3[nCont][1] + CRLF
		Next
		cLog += "Total de Arquivos: "+cValtoChar(Len(aLog3))
		aAdd(aRetLog,{cLog})
	EndIf

	If !Empty(aLog4)
		cLog := ""
		Aadd(aRetTit,aTitLog4[1])	
		For nCont := 1 To Len(aLog4)
			cLog += aLog4[nCont][1] + CRLF
		Next
		cLog += "Total de Arquivos: "+cValtoChar(Len(aLog4))
		aAdd(aRetLog,{cLog})
	EndIf
	
	If !Empty(aLog5)
		cLog := ""
		Aadd(aRetTit,aTitLog5[1])
		For nCont := 1 To Len(aLog5)
			cLog += aLog5[nCont][1] + CRLF
		Next
		cLog += "Total de Arquivos: "+cValtoChar(Len(aLog5))
		aAdd(aRetLog,{cLog})
	EndIf
	
//	cPathFile := fMakeLog(aRetLog,aRetTit,/*cPerg*/,.T.,cNomeLOG,,"M","P",/*aRet*/,.F.)
	If lExcel
		ExpExel(aLogExcel)
	EndIf
//	If !Empty(cPathFile)
//		cPathF := cPathFile
//	EndIf
//	cPathFile := fMakeLog(aLog3,aTitLog3,/*cPerg*/,.T.,cNomeLOG,/*cTitulo*/,/*cTamanho*/,/*cLandPort*/,/*aRet*/,.T.)
//	If !Empty(cPathFile)
//		cPathF := cPathFile
//	EndIf
//	cPathFile := fMakeLog(aLog4,aTitLog4,/*cPerg*/,.T.,cNomeLOG,/*cTitulo*/,/*cTamanho*/,/*cLandPort*/,/*aRet*/,.T.)
//	If !Empty(cPathFile)
//		cPathF := cPathFile
//	EndIf
//	If !Empty(cPathF)
////		Alert("Relatorio de LOG gerado em: " + cPathF, OemToAnsi("Aten��o!"))
//	EndIf
Return

//------------------------------------------------------------
//inclui os registros de coparticipagco
//------------------------------------------------------------
Static Function InclCop(aCop)
	
	Local ny:= 0
	//Local nz:= 0
	Local cMat:=""
	//Indice Matricula + Ocorrencia + Tipo + Cod. Forn + Origem + Verba
	//Gravar usando modelo MVC (commit do modelo)
	//"GPEA003_MSRA" - matricula do funcionario
	//"GPEA003_MRHO" - co-participagco
	//posiciona no modelo do SRA
	For ny:= 1 to len(aCop)
		If Empty(aCop[ny][1])
			Loop
		Endif
		DbSelectArea("SRA")
		SRA->(DbSetOrder(1))
		If SRA->(DbSeek(aRet[1]+aCop[ny][1]))
			cMat := aCop[ny][1]
			//Abrir modelo
			oMdlGPEA003	:= FWLoadModel( "GPEA003" )	//Cria um objeto de Modelo de dados baseado no ModelDef do fonte informado
			IF aCop[ny][7]="00"
				cCodigo := "  "
			else
				cCodigo:= aCop[ny][7]
			Endif
			dComPg:= subst(aCop[ny][2],3,4)+subst(aCop[ny][2],1,2)
			DbSelectArea("RHO")
			RHO->(DbSetOrder(3))
			oMdlGPEA003:SetOperation(MODEL_OPERATION_UPDATE) 
//				oMdlGPEA003:SetOperation(MODEL_OPERATION_INSERT)
//			EndIf
			oMdlGPEA003:Activate()
			oMdlGPEASRA:= oMdlGPEA003:GetModel("GPEA003_MSRA")
			oMdlGPEARHO:= oMdlGPEA003:GetModel("GPEA003_MRHO")
			
			//definir os campos de gravagco
			//				oMdlGPEARHO:AddLine() //campos para inclusco da linha
			//dDtOcor:= ctod(
	//		If nY > 1//oMdlGPEA003:GetOperation() == MODEL_OPERATION_INSERT
	//			oMdlGPEARHO:AddLine() //campos para inclusco da linha
			//	oMdlGPEASRA:SetValue("RHO_MAT" ,aCop[ny][1]  )
	//		EndIf
			If RHO->(!DbSeek(aRet[1]+aCop[ny][1]+DTOS(CTOD(aCop[ny][3]))+aCop[ny][5]+aCop[ny][6]+aCop[ny][4]+cCodigo+aCop[ny][9]+dComPg))// .And. !Empty(cCodigo)
				oMdlGPEARHO:AddLine()//oMdlGPEA003:SetOperation(MODEL_OPERATION_UPDATE) //operagco de langamento
			EndIf
			oMdlGPEARHO:SetValue( "RHO_DTOCOR", ctod(aCop[ny][3]) ) //data da ocorrencia
			oMdlGPEARHO:SetValue( "RHO_ORIGEM", aCop[ny][4] ) // 1-Titular,2-Dependente,3-Agregado
			oMdlGPEARHO:SetValue( "RHO_TPFORN", aCop[ny][5] ) // 1-Medico,2-Odontologico
			oMdlGPEARHO:SetValue( "RHO_CODFOR", aCop[ny][6] ) // codigo do fornecedor
			If aCop[ny][7] <> "00"
				oMdlGPEARHO:SetValue( "RHO_CODIGO", aCop[ny][7] ) // 2-  dependente, 3- agregado
			Endif
			oMdlGPEARHO:SetValue( "RHO_TPLAN" , aCop[ny][8] ) // 1-co-participagco, 2-Reembolso
			oMdlGPEARHO:SetValue( "RHO_PD" , aCop[ny][9] ) // codigo da verba
			oMdlGPEARHO:LoadValue( "RHO_VLRFUN" , aCop[ny][10] ) //R$ funcionario
			oMdlGPEARHO:LoadValue( "RHO_COMPPG" , dComPg ) //data da competencia
			oMdlGPEARHO:SetValue( "RHO_OBSERV" , aCop[ny][11] ) // obs
			If oMdlGPEA003:VldData()
				oMdlGPEA003:CommitData()
				fIncLog({"05","CPF:"+SRA->RA_CIC+" Matricula: "+Alltrim(aCop[ny][01] )+"Origem: "+aCop[ny][4]+" C�digo: "+aCop[ny][7]+"-> Dados OK."})
				aAdd(aLogExcel,{5," -  ;"+Alltrim(SRA->RA_CIC)+";"+Alltrim(aCop[ny][01] )+";Sim; Dados Ok."})
			Else
				aErro   := oMdlGPEA003:GetErrorMessage()
				fIncLog({"04","CPF:"+SRA->RA_CIC+" Matricula: "+Alltrim(aCop[ny][01] )+"Origem: "+aCop[ny][4]+" C�digo: "+aCop[ny][7]+" -> Erro na Rotina autom�tica."})
				aAdd(aLogExcel,{4," -  ;"+Alltrim(SRA->RA_CIC)+";"+Alltrim(aCop[ny][01] )+";N�o; Erro na Rotina Autom�tica."})
			//	Help( ,, 'Help',, aErro[MODEL_MSGERR_MESSAGE], 1, 0 )
			Endif
		EndIf
	Next ny
	Aviso( "Finalizado", "Processo de Importa��o Finalizado", {"Ok"} )
Return

/////////////////////////////////////////
//Busca o c�digo correto do dependente //
/////////////////////////////////////////
Static Function RetDep(cMatricula, cCodigo)
Local cQuery 		:= ""
Local cAliasTrb	:= GetNextAlias()
Local cRet			:= ""

cQuery := " SELECT RHL_CODIGO FROM "+RetSqlName("RHL") + " "
cQuery += " WHERE "
cQuery += " RHL_FILIAL = '"+aRet[1]+"' "
cQuery += " AND RHL_MAT = '"+cMatricula+"' "
cQuery += " AND RHL_TPFORN = '"+Substring(aRet[2],1,1)+"' "
cQuery += " AND SUBSTR(TRIM(RHL_NCARVD),-2,2) = '"+cCodigo+"' "
cQuery += " AND D_E_L_E_T_ = ' ' "
cQuery := ChangeQuery(cQuery )
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTrb,.T.,.T.)

If (cAliasTrb)->(!EOF())
	cRet := (cAliasTrb)->RHL_CODIGO
EndIf

(cAliasTrb)->(DbCloseArea())
Return cRet
///////////////////////////////////////////////
// Inclui um novo registro no arquivo de LOG //
///////////////////////////////////////////////
Static Function fIncLog(aReg)
/*
01 - CPF nao encontrado
02 - Codigo Dependente nao encontrado
03 - Funcionarios Demitivos
04 - Dados Invalidos
05 - Importacao OK
*/
If aReg[1] $ "01/02"
	If aScan(aTitLog1, { |x| x == "Erro no Cadastrou ou Rotina Autom�tica" }) = 0
		aAdd(aTitLog1, "Erro no Cadastrou ou Execauto ou Rotina Autom�tica")
	EndIf
	aAdd(aLog1,{"   "+aReg[2]})

ElseIf aReg[1] $ "03"
	If aScan(aTitLog3, { |x| x == "Funcionarios Demitidos" }) = 0
		aAdd(aTitLog3, "Funcionarios Demitidos")
	EndIf	
	aAdd(aLog3,{"   "+aReg[2]})

ElseIf aReg[1] $ "04"
	If aScan(aTitLog4, { |x| x == "Dados Invalidos" }) = 0
		aAdd(aTitLog4, "Dados Invalidos")
	EndIf	
	aAdd(aLog4,{"   "+aReg[2]})

ElseIf aReg[1] $ "05"
	If aScan(aTitLog5, { |x| x == "Importacao OK" }) = 0
		aAdd(aTitLog5, "Importacao OK")
	EndIf	
	aAdd(aLog5,{"   "+aReg[2]})

EndIf	
	
Return


///////////////////////
//Exporta para Excel	//
///////////////////////
Static Function ExpExel(aLogExcel)
Local cArqLoc		:= GetTempPath()
Local cNomeArq	:= DTOS(date())+StrTran(Time(),":","")+'.CSV'
Local nHandle 	:= 0
Local cTxt			:= ""
Local nX			:= 0

If Len(aLogExcel)>1
	nHandle := FCREATE(cArqLoc+cNomeArq)
	If nHandle < 0
		Alert("Erro na gera��o do Arquivo")
	Else
		aSort(aLogExcel,,,{|x,y| x[01]<y[01]})
		For nX := 1 To Len(aLogExcel)
			cTxt := aLogExcel[nX][02]+';'+CRLF
			FWrite(nHandle,cTxt)
		Next
		FClose(nHandle)
		shellExecute("Open", cArqLoc+cNomeArq,"Null" , "C:\", 1 )
	EndIf
EndIf

Return

Static Function ExcRegitros(dData,cTipo)
Local cQuery 		:= ""
Local cAliasTrb	:= GetNextAlias()
Local aAreaRHO	:= RHO ->(GetArea())

cQuery := " SELECT R_E_C_N_O_  RecRHO FROM "+RetSqlName("RHO") + " "
cQuery += " WHERE "
cQuery += " RHO_FILIAL = '"+aRet[1]+"' "
cQuery += " AND RHO_DTOCOR = '"+DTOS(dData)+"' "
cQuery += " AND RHO_TPFORN = '"+Substring(aRet[2],1,1)+"' "
cQuery += " AND D_E_L_E_T_ = ' ' "
cQuery := ChangeQuery(cQuery )
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTrb,.T.,.T.)

While (cAliasTrb)->(!EOF())
	If RHO->(DbGoTo((cAliasTrb)->RecRHO))
		RecLock("RHO",.F.)
		RHO->(dBDelete())
		RHO->(MsUnLock())
	EndIf
	(cAliasTrb)->(DbSkip())
EndDo

(cAliasTrb)->(DbCloseArea())
RestArea(aAreaRHO)
Return 