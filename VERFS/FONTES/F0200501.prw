#Include 'TOTVS.CH'

Static aTtPerid	:= {}
Static aTtPerAx	:= {}
Static aP08GL_T	:= {}
Static lBOracle	:= Trim(TcGetDb()) = 'ORACLE' 

/*{Protheus.doc} F0200501
Relat�rio de Amostra em Excell
@author		Cristiane Thomaz Polli
@since		11/07/2016
@version	12.1.7
@Menu		Compras\Relatorios\Rel. de Amostra
@Project	MAN00000463301_EF_005
*/ 
User Function F0200501()

	Local 	cTabSD3		:= ''
	Local	aAreaAtu	:= {}
	Local	aArea		:= GetArea()
	Private cTitAmos	:= 'Relat�rio de Amostra'
	Private cPergAtu	:= 'FSW0200501'
	
	Do While Pergunte(cPergAtu,.T.)
						
		//Caso um c�digo de produto seja informado, efetua a valida��o
		If !Empty(MV_PAR02)
					
			SB1->(dbSetOrder(1))
			if !SB1->(dbSeek(xFilial('SB1')+MV_PAR02))
					
				Help(" ", 1, "Help", "F0200501_01",'Informe um c�digo de produto v�lido.(SB1)', 3, 0)
				Loop
						
			EndIf
								
		EndIf
			
		//Caso um c�digo de Segmento/Categoria seja informado, efetua a valida��o
		If !Empty(MV_PAR03)
					
			ACU->(dbSetOrder(1))
			if !ACU->(dbSeek(xFilial('ACU')+MV_PAR03))
				Help(" ", 1, 'Help', 'F0200501_02','Informe um c�digo de segmento/categoria v�lido.(ACU)', 3, 0)
				Loop						
			EndIf
								
		EndIf
			
		//Seleciona os registros
		cTabSD3		:= GetNextAlias()
		FWMsgRun(,{|| SelMovto(cTabSD3) },'Selecionando Registros','Aguarde...' )
			
		//Caso existam informa��es
		if !(cTabSD3)->(Eof())
					
			//Chama rotina para gerar o excell com as informa��es carregadas
			FWMsgRun(,{|| GerExcel(cTabSD3) },'Preenchendo o Arquivo em Excel','Aguarde...' )
					
		Else
									
			Aviso('Inexistentes','Para os parametros selecionados n�o existem informa��es.',{"OK"})
				
		EndIf
		
		(cTabSD3)->(dbCloseARea())
		RestArea(aArea)
				
	EndDo
			
Return

/*{Protheus.doc} SelMovto
 Busca informa��es conforme parametros selecionados.
@author		Cristiane Thomaz Polli
@since		12/07/2016
@version	12.1.7
@Param		cTabSD3, Alias para Query
@Project	MAN00000463301_EF_005
*/ 
Static Function SelMovto(cTabSD3)
	
	Local cQrySD3	:= ''

	//Monta os periodos que ser�o considerados at� a data base.	
	aTtPerid	:= {}
	MntPeri()

	//Monta a tabela GL x T conforme informa��es incluidas na P08
	MntGLT()
		 
	cQrySD3	:= 	"	 SELECT ISNULL(D3_FILIAL,'"+xFilial('SD3')+"') AS D3_FILIAL,ACU.ACU_DESC CATEGORIA, SB1.B1_COD, SB1.B1_DESC "+CRLF
	
	// Banco Oracle
	if lBOracle
	
		cQrySD3	+= 	"	 	, SUBSTR(ISNULL(SD3.D3_EMISSAO,''),1,6) PERIODO,"+CRLF
	
	Else
		
		cQrySD3	+= 	"	 	, SUBSTRING(ISNULL(SD3.D3_EMISSAO,''),1,6) PERIODO,"+CRLF	
	
	EndIf		
	
	cQrySD3	+= 	"	 	SUM( "+CRLF
	cQrySD3	+= 	"	 		(	CASE  "+CRLF
	cQrySD3	+= 	"	 				WHEN SD3.D3_CF LIKE 'DE%' THEN SD3.D3_QUANT "+CRLF
	cQrySD3	+= 	"	 				ELSE 0 "+CRLF
	cQrySD3	+= 	"	 			END ) "+CRLF
	cQrySD3	+= 	"	 		- "+CRLF
	cQrySD3	+= 	"	 		(	CASE  "+CRLF
	cQrySD3	+= 	"	 				WHEN SD3.D3_CF LIKE 'RE%' THEN SD3.D3_QUANT "+CRLF
	cQrySD3	+= 	"	 				ELSE 0 "+CRLF
	cQrySD3	+= 	"	 			END ) "+CRLF
	cQrySD3	+= 	"	 		) AS RESULT	"+CRLF
	cQrySD3	+= 	"	 	FROM "+RetSqlName("ACU")+" ACU "+CRLF
	cQrySD3	+= 	"	 	INNER JOIN "+RetSqlName("ACV")+" ACV "+CRLF
	cQrySD3	+= 	"	 			ON ACV.ACV_FILIAL =  ACU.ACU_FILIAL "+CRLF
	cQrySD3	+= 	"	 			AND ACV.ACV_CATEGO = ACU.ACU_COD "+CRLF
	cQrySD3	+= 	"	 			AND ACV.D_E_L_E_T_ = '' "+CRLF
	cQrySD3	+= 	"	 	INNER JOIN "+RetSqlName("SB1")+" SB1 "+CRLF
	cQrySD3	+= 	"	 	       ON SB1.B1_FILIAL =  '"+xFilial("SB1")+"'"+CRLF
	cQrySD3	+= 	"	 	       AND ( "+CRLF
	cQrySD3	+= 	"	 				(ACV.ACV_CODPRO = SB1.B1_COD AND ACV.ACV_GRUPO = '') "+CRLF
	cQrySD3	+= 	"	 			 OR "+CRLF
	cQrySD3	+= 	"	 			(ACV.ACV_GRUPO = SB1.B1_GRUPO AND ACV.ACV_CODPRO = '') "+CRLF
	cQrySD3	+= 	"	 				) "+CRLF
		
	//Filtra pelo codigo de produto se informado
	if !Empty(MV_PAR02)
			
		cQrySD3	+=	"	AND SB1.B1_COD = '"+MV_PAR02+"'"+CRLF
		
	EndIf
		
	cQrySD3	+= 	"	 	      AND SB1.D_E_L_E_T_ = '' "+CRLF
	cQrySD3	+= 	"	 	LEFT JOIN "+RetSqlName("SD3")+" SD3 "+CRLF
	cQrySD3	+= 	"	 	       ON SD3.D3_FILIAL = '"+xFilial('SD3')+"'"+CRLF
	cQrySD3	+= 	"	 	      AND SD3.D3_COD = B1_COD"+CRLF
	cQrySD3	+= 	"	 	      AND SD3.D3_EMISSAO BETWEEN  '"+Dtos(FirstDate(Stod(aTtPerid[1][1])))+"' AND  '"+Dtos(LastDate(Stod(aTtPerid[24][1])))+"' "+CRLF
	cQrySD3	+= 	"	 	      AND SD3.D3_ESTORNO = '' "+CRLF
	cQrySD3	+= 	"	 	      AND SD3.D_E_L_E_T_ =  ''"+CRLF
	cQrySD3	+= 	"	 	WHERE ACU.ACU_FILIAL = '"+xFilial("ACU")+"'"+CRLF
	
		//filtra pela categoria/segmento se informado
	If !Empty(MV_PAR03)
		
		cQrySD3	+=	"     AND ACU.ACU_COD = '"+MV_PAR03+"' "+CRLF
		
	EndIf
		
		//Filtra somente por produtos controlados pelo Planejamento
	if MV_PAR04 == 1
		
		cQrySD3	+=	"     AND ACU.ACU_CTRLP = '1' "+CRLF
					
	EndIf
		
	cQrySD3	+= 	"		AND ACU.D_E_L_E_T_ = '' "+CRLF
	
	if lBOracle
	
		cQrySD3	+= 	"		GROUP BY SD3.D3_FILIAL,SB1.B1_COD,SB1.B1_DESC, SUBSTR(ISNULL(SD3.D3_EMISSAO,''),1,6),ACU.ACU_DESC "+CRLF
		cQrySD3	+=	"  		ORDER BY SD3.D3_FILIAL,ACU.ACU_DESC,SB1.B1_COD, SUBSTR(ISNULL(SD3.D3_EMISSAO,''),1,6) DESC   "+CRLF
		
	Else
	
		cQrySD3	+= 	"		GROUP BY SD3.D3_FILIAL,SB1.B1_COD,SB1.B1_DESC, SUBSTRING(ISNULL(SD3.D3_EMISSAO,''),1,6),ACU.ACU_DESC "+CRLF
		cQrySD3	+=	"  		ORDER BY SD3.D3_FILIAL,ACU.ACU_DESC,SB1.B1_COD, SUBSTRING(ISNULL(SD3.D3_EMISSAO,''),1,6) DESC   "+CRLF
	
	EndIf	
	
	cQrySD3 := ChangeQuery(cQrySD3)
	DbUseArea(.T., "TOPCONN", TcGenQry(NIL, NIL, cQrySD3), cTabSD3, .F., .T.)
		 		 
Return

/*{Protheus.doc} MntPeri
Monta os meses anteriores com base na data informada.
@author 	Cristiane Thomaz Polli
@since 		12/07/2016
@version 	12.1.7
@Project 	MAN00000463301_EF_005
*/ 
Static Function MntPeri()
 	
	Local nMeses	:= 24
	Local nM		:= 0
	Local dDatRef	:= MonthSub(MV_PAR01,1)
 		
	For nM := 1 to nMeses

		aAdd(aTtPerid,{Dtos(MonthSub(dDatRef,nMeses-nM)) ,0,'PERIOD_'+Alltrim(Str(nM))})

	Next nM
 		
Return

/*{Protheus.doc} GerExcel
Popula as informa��es nas celulas e gera  o arquivo XML
@author 	Cristiane Thomaz Polli
@since 		14/07/2016
@version 	12.1.7
@Project 	MAN00000463301_EF_005
*/ 
Static Function GerExcel(cTabSD3)
 
	Local oExcSD3 	:= FWMSEXCEL():New()
	Local cAba1		:= 'Parametros'
	Local cTitulo1	:= 'Parametros Selecinados'
	Local cAba2		:= 'Informa��es'
	Local cTitulo2	:= 'Planilha de Amostra'
	Local cNmArqEx	:= ''
	Local cCamiAux	:= ''
	Local nItemAtu	:= 1
		
	//Aba - Parametros
	oExcSD3:AddworkSheet(cAba1)
	oExcSD3:AddTable (cAba1,cTitulo1)
	oExcSD3:AddColumn(cAba1,cTitulo1,'Parametro',1,1)
	oExcSD3:AddColumn(cAba1,cTitulo1,'Conte�do Informado',2,1)
	oExcSD3:AddRow(cAba1,cTitulo1,{'Data Base',Dtoc(MV_PAR01)})
	oExcSD3:AddRow(cAba1,cTitulo1,{'C�digo do Produto',MV_PAR02})
	oExcSD3:AddRow(cAba1,cTitulo1,{'Segmento/Categoria',MV_PAR03})
	oExcSD3:AddRow(cAba1,cTitulo1,{'Somente categorias controladas pela Equipe de Planejamento',IIF(MV_PAR04==1, '1=Sim','2=N�o')})
	oExcSD3:AddRow(cAba1,cTitulo1,{'Data da Gera��o',Dtoc(dDatabase)})
	oExcSD3:AddRow(cAba1,cTitulo1,{'Hora da Gera��o',time()})
		    
	//Aba - Informa��es
	oExcSD3:AddworkSheet(cAba2)
	oExcSD3:AddTable (cAba2,cTitulo2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'Estabelecimento',1,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'C�digo do Produto',2,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'Descri��o do Produto',2,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'Segmento/Categoria',2,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'1',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'2',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'3',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'4',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'5',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'6',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'7',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'8',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'9',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'10',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'11',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'12',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'13',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'14',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'15',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'16',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'17',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'18',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'19',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'20',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'21',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'22',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'23',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'24',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'CM',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'CM90',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'CM180',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'M�dia',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'Desvio',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'%',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'GL',2,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'T',2,1)
	oExcSD3:AddColumn(cAba2,cTitulo2,'Erro',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'M�nimo',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'M�ximo',2,2)
	oExcSD3:AddColumn(cAba2,cTitulo2,'N� per�odos > 0',2,1)
		
	Do While !(cTabSD3)->(Eof())

		oExcSD3:AddRow(cAba2,cTitulo2,MntInfPd(RetNmSM0((cTabSD3)->D3_FILIAL),cTabSD3,nItemAtu))
		nItemAtu	:= nItemAtu+1
			
	EndDo
				
	oExcSD3:Activate()
				
	cNmArqEx	:=	'F0200501_'+dtos(dDatabase)+'_'+StrTran(time(),':','_')+".xls"
	oExcSD3:GetXMLFile(cNmArqEx)
		
	//Permite que o colaborador selecione uma pasta para copiar o arquivo.	
	if MsgYesNo('Deseja selecionar em qual diret�rio ser� copiado o arquivo gerado?')
			
		cCamiAux	:= cGetFile( '*.txt' , 'Selecione a Pasta', 1, 'C:\', .F., nOR( GETF_LOCALHARD, GETF_LOCALFLOPPY, GETF_RETDIRECTORY ),.F., .T. )

	EndIf
		
	//Caso n�o seja selecionado  uma pasta busca das configura��es do usu�rio
	if Empty(Alltrim(cCamiAux))
		
		cCamiAux	:= PswRet()[2][3]
			
	EndIf
		
	//Caso o usu�rio n�o possua configura��o de diretorio para impresss�o.
	if Empty(Alltrim(cCamiAux))
		
		cCamiAux	:=  'C:\TOTVS\'
		
	EndIf
		
	//Verifica se o diret�rio existe e se n�o existe for�a a cria��o
	if !ExistDir(cCamiAux)
		
		MakeDir(cCamiAux)
		
	EndIf
		
	//Copia do servidor para o caminho informado		
	If __Copyfile('\system\'+cNmArqEx,cCamiAux+cNmArqEx)
		
		Aviso('Sucesso - Gera��o do arquivo','O arquivo foi gerado com sucesso no caminho: '+cCamiAux+cNmArqEx,{"OK"})
		
	Else
			
		Aviso('N�o Gerado','Arquivo n�o foi copiado para o caminho informado.'+cCamiAux+'Contate o departamento de TI.',{'OK'})
						
	EndIf

Return

/*{Protheus.doc} MntInfPd
Monta os calculos e quantidades referente a cada linha de produto
@author Cristiane Thomaz Polli
@since 		14/07/2016
@version 	12.1.7
@Param		cNmFilial, Filial da registro (SD3)
@Param		cTabSD3, Alias da Query
@Param		nItemAtu, n�mero do item
@Return		aInfProd, Vetor (	1.Cod.Filial(SD3)  ou filial atual,
								2.Cod.Produto,
								3.Descri��o do Produto,
								4.Categoria,
								5 ao 28.Quantidades entre os periodos 1 a 24 (24 celulas),
								29.CM = Quantidade m�dio exceto dos meses zerados,
								30.CM90= Quantidade m�dio dos �ltimos 3 meses,
								31.CM180=Quantidade m�dia dos �ltimos 6 meses,
								32.M�dia= Quantidade m�dia entre  as m�dias (CM+CM90+CM180)/3,
								33.Desvio padr�o,
								34.Percentual do  Desvio padr�o com  rela��o a Quantidade M�dia(CM),
								35.GL= valor chumbado na tabela  P08 correspondente ao numero do item a ser impresso,
								36.T=  valor chumbado na tabela  P08 relacionada ao  GL impresso,
								37.Erro = parametro (FS_AMOS1*Desvio Padr�o)/Raiz quadrada FS_AMOS2,
								38.Minimo = M�dia -  Erro,
								39.M�ximo = M�dia + Erro,
								40.N� per�odos= n�mero de per�odos que tiveram movimenta��o diferente de zero(0).
							)								 
@Project 	MAN00000463301_EF_005
*/ 
Static Function MntInfPd(cNmFilial,cTabSD3,nItemAtu)
  	
	Local nFSAmos1:= SuperGetMv("FS_AMOS1")
	Local nFSAmos2:= SuperGetMv("FS_AMOS2")
	Local nPeriAtu:= 0
	Local nCMAux	:= 0
	Local nTtPAux	:= 0
	Local nCM90Aux:= 0
	Local nCM180Ax:= 0
	Local nPosGLT	:= 0
	Local nDesvAx	:= 0
	Local aInfProd	:= array(40)
		 	 
	cCodSD3		:= (cTabSD3)->B1_COD
	 	
	aInfProd[1]	:=	cNmFilial
	aInfProd[2]	:=	cCodSD3
	aInfProd[3]	:=	(cTabSD3)->B1_DESC
	aInfProd[4]	:=	(cTabSD3)->CATEGORIA
						
	While cCodSD3 == (cTabSD3)->B1_COD
			
		if (nPeriAtu	:= Ascan(aTtPerid, {|x,y|  substring(x[1],1,6) == (cTabSD3)->PERIODO})) > 0

			aInfProd[nPeriAtu+4] := (cTabSD3)->RESULT

			//Acumulador para a Media do Custo
			nCMAux	:= nCMAux + (cTabSD3)->RESULT

			//Acumulador para o calculo do CM180
			if 	nTtPAux < 6
					
				nCM180Ax	:= nCM180Ax+(cTabSD3)->RESULT
					
			EndIF
					
			//Acumulador para o calculo do CM90
			if nTtPAux < 3
					
				nCM90Aux	:= nCM90Aux+(cTabSD3)->RESULT
					
			EndIf
														
			//Acumulador para exibir meses que ocorreu o movimento
			nTtPAux	:= nTtPAux+1
					 
			//Acumulador para parte do calculo do desvio padr�o
			nDesvAx	:= nDesvAx+((cTabSD3)->RESULT^2)
										
		EndIf
								
		(cTabSD3)->(dbSkip())
								
	EndDo
			
	//Efetua-se o calculo das demais colunas
	aInfProd[29]		:= nCMAux/nTtPAux
	aInfProd[30]		:= nCM90Aux/3
	aInfProd[31]		:= nCM180Ax/6
	aInfProd[32]		:= (aInfProd[29]+aInfProd[31]+aInfProd[30])/3
//	aInfProd[33]		:= Sqrt(((24*nDesvAx)-(nCMAux^2))/(24*(24-1)))
	aInfProd[33]		:= PCO_DPAD(	aInfProd[05],;
										aInfProd[06],;
										aInfProd[07],;
										aInfProd[08],;
										aInfProd[09],;
										aInfProd[10],;
										aInfProd[11],;
										aInfProd[12],;
										aInfProd[13],;
										aInfProd[14],;
										aInfProd[15],;
										aInfProd[16],;
										aInfProd[17],;
										aInfProd[18],;
										aInfProd[19],;
										aInfProd[20],;
										aInfProd[21],;
										aInfProd[22],;
										aInfProd[23],;
										aInfProd[24],;
										aInfProd[25],;
										aInfProd[26],;
										aInfProd[27],;
										aInfProd[28])
	aInfProd[34]		:= (aInfProd[33]/aInfProd[29])*100
	aInfProd[37]		:= (nFSAmos1*aInfProd[33])/Sqrt(nFSAmos2)
	aInfProd[38]		:= aInfProd[32]-aInfProd[37]
	aInfProd[39]		:= aInfProd[32]+aInfProd[37]
	aInfProd[40]		:= nTtPAux
			
	//Preenche os valores do GL e T conforme posi��o de exibi��o
	If (nPosGLT := Ascan(aP08GL_T,{|x,y| x[1] == nItemAtu})) > 0
			
		aInfProd[35]	:= aP08GL_T[nPosGLT][2]
		aInfProd[36]	:= aP08GL_T[nPosGLT][3]
				
	Else
			
		aInfProd[35]	:= 0
		aInfProd[36]	:= 0
				
	EndIf
			
	nCM			:= 0
	nCMAux		:= 0
	nTtPAux	:= 0
	nCM90		:= 0
	nCM90Aux	:= 0
	nCM180		:= 0
	nCM180Ax	:= 0
	nMedia		:= 0
	nDesvio	:= 0
	nDesvAx	:= 0
	nCMAux		:= 0
	nPercDes	:= 0
	nErro		:= 0
	nMinimo	:= 0
	nMaximo	:= 0
	nTtPerio	:= 0
	nTtPAux	:= 0
	aTtPerAx	:= aTtPerid
	
Return aInfProd
/*{Protheus.doc} MntGLT
Monta array com as informa��es contidas na tabela P08 
@author 	Cristiane Thomaz Polli
@since 		13/07/2016
@version 	12.1.7
@Project 	MAN00000463301_EF_005
*/ 
Static Function MntGLT()
 	
	Local cQryP08	:= ''
	Local cTabP08	:= GetNextAlias()
	Local aArea	:= GetArea()
	Local nPosP08	:= 1
 	
	If lBOracle 
		cQryP08	:= "	SELECT TO_NUMBER(P08_GL) GL , P08.P08_T T "+CRLF
	Else
		cQryP08	:= "	SELECT CONVERT(INT,P08.P08_GL) GL, P08.P08_T T "+CRLF
	EndIf
	
	cQryP08	+= "	FROM "+RetSqlName('P08')+" P08 "+CRLF
	cQryP08	+= "	WHERE P08.P08_FILIAL = '"+xFilial('P08')+"' "+CRLF
	If P08->(FieldPos("P08_MSBLQL")) > 0
		cQryP08	+= "	 AND P08.P08_MSBLQL <> '1' "+CRLF
	EndIf
	cQryP08	+= "	 AND P08.D_E_L_E_T_ = ' ' "+CRLF
	cQryP08	+= "	 ORDER BY GL "+CRLF
 	
 	cQryP08 := ChangeQuery(cQryP08)
	DbUseArea(.T., "TOPCONN", TcGenQry(NIL, NIL, cQryP08), cTabP08, .F., .T.)

	If !(cTabP08)->(Eof())
 	
		While !(cTabP08)->(Eof())
 	
			AAdd(aP08GL_T,{ nPosP08, (cTabP08)->GL, (cTabP08)->T })
			(cTabP08)->(dbSkip())
			nPosP08	:= nPosP08+1
 		
		EndDo
 		
	EndIf
 	
	(cTabP08)->(dbCloseArea())
	RestArea(aArea)
 	
Return
/*{Protheus.doc} RetNmSM0
Retorna o nome da filial
@author Cristiane Thomaz Polli
@since 13/07/2016
@version 12.1.7
@return cNameAtu, Nome da filial do registro da SD3.
@Project MAN00000463301_EF_005
*/ 
Static Function RetNmSM0(cFilSD3)

	Local aAreaSM0	:= SM0->(GetArea())
	Local cNameAtu	:= ''
	
	dbSelectArea('SM0')
	if SM0->(dbSeek(cEmpAnt+cFilSD3))
		
		cNameAtu	:= Alltrim(SM0->M0_FILIAL)
		
	EndIf
		
	RestArea(aAreaSM0)
	
Return cNameAtu

/*{Protheus.doc} PCO_DPAD
Calcula o desvio padr�o das movimenta��es.
@author Paulo Kr�ger
@since 03/05/2016
@version 12.1.7
@return nlDesvPad
@Project MAN00000463301_EF_005
*/ 

Static Function PCO_DPAD(n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20,n21,n22,n23,n24)

	Local nlDesvPad	:= 0
	Local nlMedia	:= 0
	Local nlVaria	:= 0
	Local i			:= 0
	Local llRet		:= .T.
	Local alDesvPad	:= {}
	Local alNumDPad	:= {}
			
	aAdd(alNumDPad,n1) 
	aAdd(alNumDPad,n2) 
	aAdd(alNumDPad,n3) 
	aAdd(alNumDPad,n4) 
	aAdd(alNumDPad,n5) 
	aAdd(alNumDPad,n6) 
	aAdd(alNumDPad,n7) 
	aAdd(alNumDPad,n8) 
	aAdd(alNumDPad,n9) 
	aAdd(alNumDPad,n10) 
	aAdd(alNumDPad,n11) 
	aAdd(alNumDPad,n12) 
	aAdd(alNumDPad,n13) 
	aAdd(alNumDPad,n14) 
	aAdd(alNumDPad,n15) 
	aAdd(alNumDPad,n16) 
	aAdd(alNumDPad,n17) 
	aAdd(alNumDPad,n18) 
	aAdd(alNumDPad,n19) 
	aAdd(alNumDPad,n20) 
	aAdd(alNumDPad,n21) 
	aAdd(alNumDPad,n22) 
	aAdd(alNumDPad,n23) 
	aAdd(alNumDPad,n24) 

	//Adiciona no array todos os valores para o calculo do desvio padr�o.
	For i:=1 to Len(alNumDPad)
	   	If ValType(alNumDPad[i]) == "N"
			aAdd(alDesvPad,alNumDPad[i])
		Elseif ValType(alNumDPad[i]) == "C"
			llRet := .F. //Foi informado um argumento Caracter no per�odo de movimenta��es.
			Exit
		Elseif ValType(alNumDPad[i]) == "L"  //Foi informado um argumento Logico no per�odo de movimenta��es.
			llRet := .F.
			Exit
		Endif	
	Next i
	
	If llRet
		If Len(alDesvPad) == 0 //Faltam par�metros para o c�lculo do desvio padr�o.
			llRet := .F.
		Elseif Len(alDesvPad) == 1 //Faltam par�metros para o c�lculo do desvio padr�o.
			llRet := .F.
		Endif
	Endif
	
	If llRet
	
		//Faz o calculo da Media
		For i:=1 to Len(alDesvPad)
			nlMedia+=alDesvPad[i]
		Next i
		
		nlMedia := nlMedia / Len(alDesvPad)
		 
		//Faz o Calculo da Variancia
		For i:=1 to Len(alDesvPad)
			nlDesvPad += ((alDesvPad[i]-nlMedia)*(alDesvPad[i]-nlMedia))
		Next i
		
		nlVaria := nlDesvPad / (Len(alDesvPad) - 1)
		
		//Calcula o Desvio Padr�o
		nlDesvPad := SQRT(nlVaria)
	
	Endif

Return nlDesvPad