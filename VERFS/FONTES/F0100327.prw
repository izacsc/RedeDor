#Include 'Protheus.ch'

/*
{Protheus.doc} F0100327()

@Author     Bruno de Oliveira
@Since      22/12/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param      cVaga, c�digo da vaga
*/
User Function F0100327(cFilFun, cMatFun, cDepto,cVisao, cEmpFun, cFilSol, cMatSol, cFilLstApr, cMatLstApr)
	
	Local lCont        	:= .T.
	Local cChave       	:= ""
	Local cTree        	:= ""
	Local cEmpIde		:= ""
	Local cSra			:= "%"+RetFullName("SRA",cEmpFun)+"%"
	Local cSqb			:= "%"+RetFullName("SQB",cEmpFun)+"%"
	Local cRd0			:= "%"+RetFullName("RD0",cEmpFun)+"%"
	Local cRdz			:= "%"+RetFullName("RDZ",cEmpFun)+"%"
	Local cRcx			:= "%"+RetFullName("RCX",cEmpFun)+"%"
	Local cSupAlias    	:= GetNextAlias()
	Local cAuxAlias    	:= GetNextAlias()
	Local aAprov		:= {}
	Local aArea1 		:= ""
	Local cTOrg			:= ""
	Local lRetOrg
	
	DEFAULT cFilFun    	:= ""
	DEFAULT cMatFun    	:= ""
	DEFAULT cDepto     	:= ""
	DEFAULT cVisao     	:= ""
	DEFAULT cEmpFun		:= cEmpAnt
	DEFAULT cFilSol    	:= cFilFun
	DEFAULT cMatSol    	:= cMatFun
	DEFAULT cFilLstApr  := cFilSol
	DEFAULT cMatLstApr  := cMatSol
	
	lRetOrg := TipoOrg(@cTOrg, cVisao)
	
	If !lRetOrg
		lInclRH3	:= .F.
		//Help("",1, "Help", "Pesquisa Visao(PrepDds_01)", "A vis�o "+cVisao+" n�o foi encontrada, portanto n�o ser� gravada a Solicita��o!" , 3, 0)
	Else
		If cTOrg == "1" //-Posto
			
			BeginSql alias cSupAlias
				
				SELECT RD4.RD4_TREE
				FROM %EXP:cRCX% RCX
				INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = RCX.RCX_POSTO AND RCX.RCX_FILIAL = RD4.RD4_FILIDE
				WHERE
				RD4.RD4_FILIAL    = %xfilial:RD4%  AND
				RD4.RD4_CODIGO    = %exp:cVisao%   AND
				RCX.RCX_MATFUN    = %exp:cMatFun%  AND
				RCX.RCX_FILFUN    = %exp:cFilFun%  AND
				RCX.RCX_SUBST     = '2'            AND
				RCX.RCX_TIPOCU    = '1'            AND
				RD4.%notDel%                       AND
				RCX.%notDel%
				
			EndSql
			
			If !(cSupAlias)->(EOF())
				
				cTree := (cSupAlias)->RD4_TREE
				
				aArea1 := GetArea()
				DbSelectArea("RD4")
				RD4->(DbSetOrder(1))
				If RD4->(DbSeek(xFilial("RD4")+cVisao+cTree))
					cEmpIde := RD4->RD4_EMPIDE
				EndIf
				
				If !Empty(cEmpIde)
					cSra	:= "%SRA"+cEmpIde+"0%"
					cRcx	:= "%RCX"+cEmpIde+"0%"
					cRdz	:= "%RDZ"+cEmpIde+"0%"
					cRd0	:= "%RD0"+cEmpIde+"0%"
				Else
					cSra	:= "%"+RetFullName("SRA",cEmpFun)+"%"
					cRcx	:= "%"+RetFullName("RCX",cEmpFun)+"%"
					cRdz	:= "%"+RetFullName("RDZ",cEmpFun)+"%"
					cRd0	:= "%"+RetFullName("RD0",cEmpFun)+"%"
				EndIf
				RestArea(aArea1)
				
				lCont := .T.
				While lCont
					
					BeginSql alias cAuxAlias
						
						SELECT RD4_CHAVE, RD4_EMPIDE, RCX.RCX_FILFUN, RCX.RCX_MATFUN, SRA.RA_NOME, SRA.RA_NOMECMP,
						SRA.RA_CATFUNC, RD0.RD0_NOME, RD4_TREE, RD4_CODIDE
						FROM %Exp:cRCX% RCX
						INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = RCX.RCX_POSTO AND RCX.RCX_FILIAL = RD4.RD4_FILIDE
						INNER JOIN %Exp:cSRA% SRA ON SRA.RA_FILIAL = RCX.RCX_FILFUN AND	SRA.RA_MAT = RCX.RCX_MATFUN
						INNER JOIN %Exp:cRDZ% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT
						INNER JOIN %Exp:cRD0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0
						WHERE RD0.RD0_FILIAL = %xfilial:RD0% AND
						RDZ.RDZ_EMPENT    = %exp:cEmpIde%     AND
						RDZ.RDZ_FILIAL    = %xfilial:RDZ%  AND
						SRA.RA_SITFOLH    <> 'D'           AND
						RD4.RD4_FILIAL    = %xfilial:RD4%  AND
						RD4.RD4_CODIGO    = %exp:cVisao%   AND
						RD4.RD4_ITEM      = %exp:cTree%   AND
						RCX.RCX_SUBST     = '2'            AND
						RCX.RCX_TIPOCU    = '1'            AND
						RD0.%notdel%						 AND
						RDZ.%notdel%						 AND
						SRA.%notdel%						 AND
						RD4.%notDel%                       AND
						RCX.%notDel%
					EndSql
					
					If (cAuxAlias)->( !Eof() )
						If FVldNotf(cVisao,(cAuxAlias)->RD4_CHAVE)
							lCont := .T.
							cTree := (cAuxAlias)->RD4_TREE
							cFilSol := (cAuxAlias)->RCX_FILFUN
							cMatSol := (cAuxAlias)->RCX_MATFUN
							cPosto := (cAuxAlias)->RD4_CODIDE
							(cAuxAlias)->(DbCloseArea())
							//Envia a notifica��o por e-mail
							FEnvNotf(cFilSol,cMatSol,"Posto "+cPosto)
							cAuxAlias := GetNextAlias()
						Else
							lCont := .F.
							aAdd( aAprov, { (cAuxAlias)->RCX_FILFUN,;
											(cAuxAlias)->RCX_MATFUN   ,;
											if(! Empty((cAuxAlias)->RA_NOMECMP),(cAuxAlias)->RA_NOMECMP,If(! Empty((cAuxAlias)->RD0_NOME),(cAuxAlias)->RD0_NOME,(cAuxAlias)->RA_NOME)),;
											(len(Alltrim((cAuxAlias)->RD4_CHAVE))/3)-1,;
											(cAuxAlias)->RD4_CHAVE,;
											If(!Empty((cAuxAlias)->RA_NOME),(cAuxAlias)->RA_CATFUNC,""),;
											(cAuxAlias)->RD4_EMPIDE;
											})
						EndIf
					Else
						lCont := .F.
					EndIf
					
				End
				(cAuxAlias)->( DbCloseArea() )
			EndIf
			(cSupAlias)->(dbCloseArea())
			
		ElseIf cTOrg == "2" //Departamento
			
			BeginSql alias cSupAlias
				
				SELECT SQB.QB_FILRESP, SQB.QB_MATRESP, SRA.RA_NOME, SRA.RA_NOMECMP, SRA.RA_CATFUNC, RD0.RD0_NOME,
				RD4.RD4_TREE, RD4_CHAVE, RD4_EMPIDE
				FROM %Exp:cSqb% SQB
				INNER JOIN %Exp:cSra% SRA ON SQB.QB_FILRESP = SRA.RA_FILIAL AND SQB.QB_MATRESP = SRA.RA_MAT
				INNER JOIN %Exp:cRdz% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT
				INNER JOIN %Exp:cRd0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0
				INNER JOIN %table:RD4% RD4 ON RD4.RD4_CODIDE = SQB.QB_DEPTO AND RD4_EMPIDE=%exp:cEmpFun%
				WHERE RD4.RD4_CODIGO = %exp:cVisao% AND
				RD4.RD4_FILIAL = %xfilial:RD4% AND
				RD0.RD0_FILIAL = %xfilial:RD0% AND
				RDZ.RDZ_FILIAL = %xfilial:RDZ% AND
				RDZ.RDZ_EMPENT = %exp:cEmpFun% AND
				SQB.QB_DEPTO   = %exp:cDepto% AND
				SQB.QB_FILIAL  = %exp:xFilial("SQB", cFilFun)% AND
				RD4.%notDel% AND
				RD0.%notdel% AND
				RDZ.%notdel% AND
				SRA.%notDel% AND
				SQB.%notDel%
			EndSql
			
			If (cSupAlias)->( !Eof() )
				
				cTree := (cSupAlias)->RD4_TREE
				
				aArea1 := GetArea()
				dbSelectArea("RD4")
				RD4->(dbSetOrder(1))
				If RD4->(dbSeek(xFilial("RD4")+cVisao+cTree))
					cEmpIde := RD4->RD4_EMPIDE
				EndIf
				
				If !Empty(cEmpIde)
					cSra := "%SRA"+cEmpIde+"0%"
					cSqb := "%SQB"+cEmpIde+"0%"
					cRdz := "%RDZ"+cEmpIde+"0%"
					cRd0 := "%RD0"+cEmpIde+"0%"
				Else
					cSra := "%"+RetFullName("SRA",cEmpFun)+"%"
					cSqb := "%"+RetFullName("SQB",cEmpFun)+"%"
					cRdz := "%"+RetFullName("RDZ",cEmpFun)+"%"
					cRd0 := "%"+RetFullName("RD0",cEmpFun)+"%"
				EndIf
				
				RestArea(aArea1)
				lCont := .T.
				While lCont
					
					BeginSql alias cAuxAlias
						
						SELECT SRA.RA_FILIAL, SRA.RA_MAT, SRA.RA_NOME, SRA.RA_NOMECMP, SRA.RA_CATFUNC, RD0.RD0_NOME, RD4.RD4_CHAVE,
						RD4.RD4_TREE, RD4.RD4_EMPIDE, RD4.RD4_CODIDE
						FROM %table:RD4% RD4
						INNER JOIN %Exp:cSqb% SQB ON SQB.QB_DEPTO = RD4.RD4_CODIDE AND SQB.QB_FILIAL = RD4.RD4_FILIDE AND SQB.%notDel%
						INNER JOIN %Exp:cSra% SRA ON SQB.QB_FILRESP = SRA.RA_FILIAL AND SQB.QB_MATRESP = SRA.RA_MAT AND SRA.%notDel%
						LEFT JOIN %Exp:cRdz% RDZ ON RDZ.RDZ_CODENT = SRA.RA_FILIAL || SRA.RA_MAT AND RDZ.RDZ_FILIAL = %xfilial:RDZ% AND
						RDZ.RDZ_EMPENT = %exp:cEmpIde% AND RDZ.%notdel%
						LEFT JOIN %Exp:cRd0% RD0 ON RD0.RD0_CODIGO = RDZ.RDZ_CODRD0 AND RD0.RD0_FILIAL = %xfilial:RD0% AND RD0.%notdel%
						WHERE RD4.RD4_ITEM   = %exp:cTree%  AND
						RD4.RD4_CODIGO = %exp:cVisao% AND
						RD4.RD4_FILIAL = %xfilial:RD4% AND
						RD4.%notDel%
					EndSql
					
					If (cAuxAlias)->( Eof() )
						
						lCont := .F.
						
					ElseIf !( (cAuxAlias)->RA_FILIAL == cFilSol .and. (cAuxAlias)->RA_MAT == cMatSol )
						
						If FVldNotf(cVisao,(cAuxAlias)->RD4_CHAVE)
							lCont := .T.
							cTree := (cAuxAlias)->RD4_TREE
							cFilSol := (cAuxAlias)->RA_FILIAL
							cMatSol := (cAuxAlias)->RA_MAT
							cDepto := (cAuxAlias)->RD4_CODIDE
							(cAuxAlias)->(DbCloseArea())
							//Envia a notifica��o por e-mail
							FEnvNotf(cFilSol,cMatSol,"Departamento "+cDepto)
							cAuxAlias := "QAUX"
						Else
							lCont := .F.
							aAdd( aAprov, { (cAuxAlias)->RA_FILIAL,;
											(cAuxAlias)->RA_MAT   ,;
											iIf(! Empty((cAuxAlias)->RA_NOMECMP),(cAuxAlias)->RA_NOMECMP,iIf(!Empty((cAuxAlias)->RD0_NOME),(cAuxAlias)->RD0_NOME,(cAuxAlias)->RA_NOME)),;
											(len(Alltrim((cAuxAlias)->RD4_CHAVE))/3)-1,;
											(cAuxAlias)->RD4_CHAVE,;
											iIf(!Empty((cAuxAlias)->RA_NOME),(cAuxAlias)->RA_CATFUNC,""),(cAuxAlias)->RD4_EMPIDE;
											})
						EndIf
					Else
						
						cTree := (cAuxAlias)->RD4_TREE
						lCont := .F.
					EndIf
				EndDo
				(cAuxAlias)->( DbCloseArea() )
			EndIf
			(cSupAlias)->( dbCloseArea() )
		EndIf
	EndIf
	
	If Len(aAprov) == 0
		aAdd(aAprov,{"","","",99,"","",""})
	EndIf
Return aAprov



Static Function FVldNotf(cVisao,cChave)
	
	Local cQuery 	:= ""
	Local cAlias1	:= GetNextAlias()
	Local lRet		:= .F.
	
	cQuery := "SELECT RD4_CODIGO, RD4_CHAVE, RD4_CODIDE, RD4_XAPNT "
	cQuery += "FROM	" + RetSqlName("RD4") + " "
	cQuery += "WHERE RD4_CODIGO = '" + cVisao + "' AND RD4_CHAVE = '" + cChave + "' AND "
	cQuery += "D_E_L_E_T_ = ' '  "
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias1)
	
	If (cAlias1)->(!EOF())
		If (cAlias1)->RD4_XAPNT = '2'
			lRet := .T.
		EndIf
	EndIf
	
	(cAlias1)->( DbCloseArea() )
Return lRet



Static Function FEnvNotf(cFilMat,cMatApr,cTpVis)
	
	Local aArea := GetArea()
	Local cEmail := ""
	Local cAssunto := "Solicita��o"
	
	cEmail := Alltrim(Posicione("SRA",1,cFilMat+cMatApr,"RA_EMAIL"))
	
	If !Empty(cEmail)
		
		cBody := '<html><body><pre>'+CRLF
		cBody += '<b>Prezado,</b>'+CRLF
		cBody += "Para seu conhecimento, foi realizado uma solicita��o para o " + cTpVis + " no portal de Rh."+CRLF
		cBody += '</pre></body></html>'
		
		U_F0200304(cAssunto,cBody,cEmail)
	EndIf
	
	RestArea(aArea)
Return
