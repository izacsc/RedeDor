#Include 'PROTHEUS.CH'
#INCLUDE "APWEBSRV.CH"

/*/{Protheus.doc} W0700301
Fun��o de identifica��o do webservice de integra��o de setores.
@type 		function
@author 	alexandre.arume
@since 		24/01/2017
@version 	1.0
@project	MAN000007423041_EF_003

/*/
User Function W0700301()
Return

WSSTRUCT Setor

	WSDATA cFil  	AS STRING
	WSDATA cCod		AS STRING
	WSDATA cDesc	AS STRING
	WSDATA cCCusto	AS STRING	OPTIONAL
	WSDATA cMSBLQL	AS STRING

ENDWSSTRUCT

WSSTRUCT SetorID

	WSDATA cFil  	AS STRING
	WSDATA cCod		AS STRING

ENDWSSTRUCT

WSSERVICE W0700301 DESCRIPTION "WEBSERVICE PARA INTEGRA��O DE SETORES"
	
	WSDATA RegSetor		AS Setor
	WSDATA RegSetorID	AS SetorID
	WSDATA cRet			AS String
	
	WSMETHOD UpsertSetor DESCRIPTION "INCLUS�O E ALTERA��O DE SETOR"
	WSMETHOD DeleteSetor DESCRIPTION "EXCLUS�O DE SETOR"
	
ENDWSSERVICE

WSMETHOD UpsertSetor WSRECEIVE RegSetor WSSEND cRet WSSERVICE W0700301

	::cRet := U_F0700301(::RegSetor, 1)

Return .T.

WSMETHOD DeleteSetor WSRECEIVE RegSetorID WSSEND cRet WSSERVICE W0700301

	::cRet := U_F0700301(::RegSetorID, 2)

Return .T.