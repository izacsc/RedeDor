#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200320()
Aprova/Reprova
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return	 cHtml
*/
User Function F0200320()
	
	Local cHtml   := ""
	Local nCnt    := 1
	Local oParam  := Nil
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	cMat     := HTTPSession->RHMat
	cmsg     := "Registro n�o alterado."
	cSolic   := HttpGet->cSolic
	cStatus  := HttpGet->cAprova
	cNome    := HttpPost->cNome
	CFILIALS  := HttpGet->CFILIALS
	
	oParam := WSW0200301():new()
	WsChgURL(@oParam,"W0200301.APW")
	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
	
	fGetInfRotina("U_F0200301.APW")
	GetMat()								//Pega a Matricula e a filial do participante logado
	
	oOrg:cParticipantID   := ""
	oOrg:cVision          := HttpSession->aInfRotina:cVisao
	oOrg:cEmployeeFil     := HttpSession->aUser[2]
	oOrg:cRegistration    := HttpSession->RhMat
	oOrg:cEmployeeSolFil  := HttpSession->aUser[2]
	oOrg:cRegistSolic     := HttpSession->RhMat
	
	If oOrg:GetStructure()
		
		oSolic := WSW0500308():New()
		WsChgURL(@oSolic,"W0500308.apw")
		
		cFilSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
		cMatSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
		cVOrg    := HttpSession->aInfRotina:cVisao
		cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
		cDepto   := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cDepartment
		
		////cEmployeeFilial, Registration, Department,Visao  ,EmployeeEmp, Registration
		If oSolic:VerAprvSit(oOrg:cEmployeeFil,oOrg:cRegistration ,cDepto,cVOrg,cEmpFunc,'2')
			
			cFilAprov  := oSolic:oWSVERAPRVSITRESULT:cFilAprov
			cMatAprov  := oSolic:oWSVERAPRVSITRESULT:cMatAprov
			
			If cStatus == "1"
				If oParam:PegSuper(cMat, cSolic, '2',cFilAprov,cMatAprov,CFILIALS)
					If Empty(cMatAprov)
						U_F0200302(5, cMatSol,cNome,'','','',cFilSol)
					Else
						U_F0200302(4, cMatAprov,cNome,'','','',cFilAprov)
					EndIf
					cMsg := "Aprovado com sucesso."
				EndIf
			Else
				If oParam:PegViInv(cMat,cSolic,'2',CFILIALS)
					cMsg := "Reprovado com sucesso."
					For nCnt := 1 To Len(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL)
						cMat := IIF(EMPTY(OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL[nCnt]:CQBMATRESP),cMat,OPARAM:OWSPEGVIINVRESULT:OWSREGISTRO:OWSSUPEMAIL[nCnt]:CQBMATRESP)
						U_F0200302(6, cMat,cNome,HttpPost->txtCalendario,HttpPost->txtCurso,HttpPost->txtTurma,cFilSol)
					Next
					
				EndIf
			EndIf
		Else
			cMsg := "Erro no cadastro! Erro no cadastro de vis�o."
		EndIf
	EndIf
	cHtml := ExecInPage("F0500305")
	
	WEB EXTENDED END
	
Return cHtml

