#Include 'Protheus.ch'

/*/{Protheus.doc} F0200902
Fun��o de valida��o de Data 
@type function 
@author queizy.nascimento
@since 10/10/2016
@version 1.0
@param dDataDe, data, (Descri��o do par�metro)
@param dDataAte, data, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
@project MAN00000463301_EF_009
/*/
USer Function F0200902(dDataDe, dDataAte, nOpcdt)
	Local lRet := .F.
	
	If Empty (dDataAte)
		Alert ("Parametro Data at� n�o pode ser vazio")
	Elseif dDataDe >  dDataAte
		If nOpcdt == 1
			MsgInfo ('Data final informada para o Per�odo 1 � menor que a data inicial informada para o Per�odo 1.'+ Chr(13) + Chr(10) +'Favor Verificar as Datas Informadas')
		ElseIf nOpcdt == 2
			MsgInfo ('Data inicial informada para o Per�odo 2 � menor que a data final informada para o Per�odo 1.'+ Chr(13) + Chr(10) +'Favor Verificar as Datas Informadas')
		ElseIf nOpcdt == 3
			MsgInfo ('Data final informada para o Per�odo 2 � menor que a data inicial informada para o Per�odo 2.'+ Chr(13) + Chr(10) +'Favor Verificar as Datas Informadas')
		EndIf
	Else
		lRet:= .T.
	Endif
	
Return lRet