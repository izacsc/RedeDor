#Include 'Protheus.ch'

/*
{Protheus.doc} F0100326()
Ap�s admiss�o atualiza status da vaga
@Author     Bruno de Oliveira
@Since      25/11/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Param      cVaga, c�digo da vaga
*/
User Function F0100326(cVaga,cCPF)

Local aArea := GetArea()
Local aAreaSQS := SQS->(GetArea())

	If IsInCallStack( "RSPM001")
		DbSelectArea("SQS")
		SQS->(DbSetOrder(1)) //QS_FILIAL+QS_VAGA
		If SQS->(DbSeek(FwXFilial("SQS")+cVaga))
		
			RecLock("SQS",.F.)
			SQS->QS_XSTATUS := "7" //Conclu�da
			SQS->(MsUnlock())
					
			If FindFunction("U_F0500201")
				U_F0500201(SQS->QS_XSOLFIL,SQS->QS_XSOLPTL,"014") //Vaga Encerrada
			EndIf	
				
		EndIf	
		
		DbSelectArea("SQG")
		SQG->(DbSetOrder(3)) //QG_FILIAL+QG_CIC
		If SQG->(DbSeek(FwXFilial("SQG")+cCPF))
		
			If !Empty(SQG->QG_XFILFAP) .AND. !Empty(SQG->QG_XCODFAP)
				DbSelectArea("PA2")
				PA2->(DbSetOrder(6)) //PA2_FILIAL+PA2_SOL
				If PA2->(DbSeek(SQG->(QG_XFILFAP+QG_XCODFAP)))
					If PA2->PA2_SIT == "AP"
						RecLock("PA2",.F.)
						PA2->PA2_SIT := "CL" //Conclu�da
						PA2->(MsUnlock())
					EndIf
					//Alterado 26/01/2017
					If FindFunction("U_F0500201")
						U_F0500201(PA2->PA2_FILSOL,PA2->PA2_SOL,"014") //Vaga Encerrada
					EndIf
				EndIf
			EndIf
				
		EndIf
	EndIf

RestArea(aAreaSQS)
RestArea(aArea)

Return