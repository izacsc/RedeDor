#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100305()
Ao clicar no posto verifica disponibilidade de orçamento e posto
@Author     Bruno de Oliveira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, página html
*/
User Function F0100305()
	
	Local lFsUsoPco := .T.
	Local lExist	:= .F.
	Local cHtml     := ""
	Local cCodVg    := ""
	Local nOcup     := 0
	Local nQntd     := 0
	Local oOrg
	
	Private cMsg
	Private oInf
	
	WEB EXTENDED INIT cHtml START "InSite"
		
		oPost := WSW0500308():New()
		WsChgURL(@oPost,"W0500308.APW")
		
		HttpPost->Posto := HttpSession->Postos[val(HttpGet->nIndicePosto)]
		
		cPosto := HttpPost->Posto:cPosto
		cDepto := HttpPost->Posto:cCodDepto
		
		
		nOcup := HttpSession->Postos[val(HttpGet->nIndicePosto)]:nOcupado
		nQntd := HttpSession->Postos[val(HttpGet->nIndicePosto)]:nQtd
		cFilP := HttpSession->Postos[val(HttpGet->nIndicePosto)]:cPostFilial
		
		cCodVg  := HttpPost->Posto:CCODCARGO
		
		oPost:VerPostSol(cPosto,cDepto,nOcup,nQntd,cFilP)
		
		
		If !oPost:lVERPOSTSOLRESULT		
		
			lPostV := .T.
			
			If nOcup == nQntd
							
				If lFsUsoPco
					cMsg := "Posto indisponível!"
					Return ExecInPage("F0100308")
				Else
					cMsg := "Posto e saldo indisponíveis!"
					Return ExecInPage("F0100308")
				EndIf
				
			ElseIf nOcup < nQntd
				
				If !lFsUsoPco
					cMsg := "Saldo indisponível!"
					Return ExecInPage("F0100308")
				EndIf
				
			EndIf
		
			oParam := WSW0500308():New()
			WsChgURL(@oParam,"W0500308.apw")
		
			If oParam:InfFxSl(cCodVg,cFilP)
				oInf :=  oParam:oWSInfFxSlRESULT
			EndIf
	
			If oParam:PegMnem(.T.)
				nVal :=  VAL(oParam:cPEGMNEMRESULT)
			EndIf
			
			oParam:oWSIdVaga:cQ3FILIAL := cFilP
			oParam:oWSIdVaga:cQ3CARGO  := HttpPost->Posto:CCODCARGO
			oParam:oWSIdVaga:cQ3CC     := HttpPost->Posto:CCC
			
			If oParam:VgDescDet()
				cDscDetal :=  oParam:cVgDescDetRESULT
			Else
				cDscDetal := ""
			EndIf
		
		Else
		
			Return ExecInPage( "F0100311" )
		
		EndIf
		
		HttpCTType("text/html; charset=ISO-8859-1")
		cHtml := ExecInPage( "F0100305" )
	
	WEB EXTENDED END
	
Return cHtml


