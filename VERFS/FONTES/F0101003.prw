#INCLUDE "Protheus.ch"

/*
{Protheus.doc} F0101003()
Relat�rio de t�tulos recusados no sistema.
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Menu		Financeiro\Relat�rios REDEDOR\Titulos Recusados    
*/
User Function F0101003()

	Local oReport

	Private cAlQry := GetNextAlias()
	 
	oReport := ReportDef()
	oReport:PrintDialog()

Return
 
/*
{Protheus.doc} ReportDef()
Auxiliar Relat�rio de t�tulos recusados no sistema.
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Return		oReport, objeto TReport
*/
Static function ReportDef()

	Local oReport
	Local oSection1
	Local cTitulo := '[F0101003] - Impress�o de t�tulos recusados'

	If !Pergunte("FSW0101003",.T.)
		Return
	Endif

	oReport := TReport():New('F0101003', cTitulo, , {|oReport| PrintReport(oReport)},"Este relatorio ira imprimir os t�tulos recusados.")
	oReport:SetPortrait()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()
 	
	oSection1 := TRSection():New(oReport,"Filial",{"QRY"})
	oSection1:SetTotalInLine(.F.)

//	TRCell():new(oSection1, "P00_DTRECU", cAlQry, RetTitle("P00_DTRECU"),                            ,TamSX3("P00_DTRECU")[1],.F.,/*{|| code-block de impressao }*/)
//	TRCell():New(oSection1, "P00_FILIAL", cAlQry, RetTitle("P00_FILIAL"),PesqPict('P00',"P00_FILIAL"),TamSX3("P00_FILIAL")[1],.F.,/*{|| code-block de impressao }*/)
//	TRCell():new(oSection1, "P00_NUM"	, cAlQry, RetTitle("P00_NUM"   ),PesqPict('P00','P00_NUM')   ,TamSX3('P00_NUM')[1]   ,.F.,/*{|| code-block de impressao }*/)
//	TRCell():new(oSection1, "P00_PREFIX", cAlQry, RetTitle("P00_PREFIX"),PesqPict('P00',"P00_PREFIX"),TamSX3("P00_PREFIX")[1],.F.,/*{|| code-block de impressao }*/)
//	TRCell():new(oSection1, "P00_VALOR"	, cAlQry, RetTitle("P00_VALOR" ),PesqPict('P00',"P00_VALOR") ,TamSX3("P00_VALOR")[1] ,.F.,/*{|| code-block de impressao }*/)
//	TRCell():new(oSection1, "P00_USRREC", cAlQry, RetTitle("P00_USRREC"),                            ,TamSX3("P00_USRREC")[1],.F.,/*{|| code-block de impressao }*/)
//	TRCell():new(oSection1, "P00MOTIVO"	, cAlQry, RetTitle("P00_MOTIVO"),                            ,TamSX3("P00_MOTIVO")[1],.F.,/*{|| code-block de impressao }*/)

	TRCell():new(oSection1, "P00_DTRECU", cAlQry, RetTitle("P00_DTRECU"),PesqPict('P00','P00_DTRECU'),/*TamSX3("P00_DTRECU")[1]*/ 010,.F.,/*{|| code-block de impressao }*/)
	TRCell():new(oSection1, "P00_NUM"	, cAlQry, RetTitle("P00_NUM"   ),PesqPict('P00','P00_NUM')   ,/*TamSX3('f0101003333333333P00_NUM')[1]   */ 009,.F.,/*{|| code-block de impressao }*/)
	TRCell():new(oSection1, "P00_USRREC", cAlQry, RetTitle("P00_USRREC"),                            ,/*TamSX3("P00_USRREC")[1]*/ 020,.F.,/*{|| code-block de impressao }*/)
	TRCell():new(oSection1, "P00MOTIVO"	, cAlQry, RetTitle("P00_MOTIVO"),                            ,/*TamSX3("P00_MOTIVO")[1]*/ 090,.F.,/*{|| code-block de impressao }*/)
	TRCell():new(oSection1, "SE2EXCL"	, cAlQry, 'Excluido'            ,                            ,03                     ,.F.,/*{|| code-block de impressao }*/)

//	oBreak := TRBreak():New(oSection1,oSection1:Cell("P00_DTRECU"),,.F.)
//	TRFunction():New(oSection1:Cell("P00_VALOR"),"Total Dia:","SUM",oBreak,,"@E 999,999,999.99",,.F.,.F.)
	oBreak := TRBreak():New(oSection1,oSection1:Cell("P00_DTRECU"),'Total dia',.F.)//"Total dia") 
    TRFunction():New(oSection1:Cell("P00_DTRECU"),,"COUNT",oBreak,,,,.F.,.F.) 

Return oReport
 
/*
{Protheus.doc} ReportDef()
Auxiliar de Impress�o Relat�rio de t�tulos recusados no sistema.
@Author     Tiago Paulo Silva
@Since      28/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_010
@Param		oReport, objeto TReport
*/
Static Function PrintReport(oReport)

	Local oSection1	:= oReport:Section(1)
	Local cQuery		:= ""
	Local cSGBD		:= TcGetDB()

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)

	cQuery := " SELECT	P00_DTRECU , "
	cQuery += "			P00_FILIAL , "
	cQuery += "			P00_NUM    , "
	cQuery += "			P00_PREFIX , "
	cQuery += "			P00_VALOR  , "
	cQuery += "			P00_USRREC , "

	If ALLTRIM(cSGBD) == 'MSSQL'
		cQuery += " ISNULL(CONVERT(VARCHAR(8000),CONVERT(BINARY(8000), P00_MOTIVO)), '') AS P00MOTIVO "
	ElseIf ALLTRIM(cSGBD) == 'ORACLE'
		cQuery += "	utl_raw.cast_to_varchar2(P00_MOTIVO) AS P00MOTIVO,  "
	EndIf
	cQuery += "       CASE WHEN E2_NUM IS NULL THEN 'Sim' ELSE CASE WHEN E2_XSTRECU = 'C' THEN 'Sim' ELSE 'Nao' END END EXCLUIDO " + CRLF
	cQuery += " FROM " + RETSQLNAME('P00') + " P00 LEFT JOIN " + RETSQLNAME('SE2') + " SE2 ON SE2.E2_FILIAL  = P00.P00_FILIAL AND " + CRLF
	cQuery += "                                                                               SE2.E2_NUM     = P00.P00_NUM    AND " + CRLF
	cQuery += "                                                                               SE2.E2_PREFIXO = P00.P00_PREFIX AND " + CRLF
	cQuery += "                                                                               SE2.E2_FORNECE = P00.P00_FORNEC AND " + CRLF
	cQuery += "                                                                               SE2.D_E_L_E_T_ = '' " + CRLF
	cQuery += " WHERE "
	cQuery += " P00.D_E_L_E_T_ = '' AND "
	cQuery += " P00.P00_FILIAL >= '"+ALLTRIM(MV_PAR01)+"' AND "
	cQuery += " P00.P00_FILIAL <= '"+ALLTRIM(MV_PAR02)+"' AND "
	cQuery += " P00.P00_DTRECU >= '"+DTOS(MV_PAR03)+"' AND "
	cQuery += " P00.P00_DTRECU <= '"+DTOS(MV_PAR04)+"' AND "
	cQuery += " P00.P00_PREFIX >= '"+ALLTRIM(MV_PAR05)+"' AND "
	cQuery += " P00.P00_PREFIX <= '"+ALLTRIM(MV_PAR07)+"' AND "
	cQuery += " P00.P00_NUM    >= '"+ALLTRIM(MV_PAR06)+"' AND "
	cQuery += " P00.P00_NUM    <= '"+ALLTRIM(MV_PAR08)+"' "
	cQuery += " ORDER BY 1 "	

	cQuery := ChangeQuery(cQuery)
	DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAlQry, .F., .T.)
	(cAlQry)->(dbGoTop())
 
	If (cAlQry)->(EOF())
		Help('',1,'Sem Dados',,"N�o foi poss�vel localizar dados correspondentes � essa pesquisa",1,0)
	Else
		oReport:SetMeter(0)

		Do While (cAlQry)->(!Eof())
			If oReport:Cancel()
				Exit
			EndIf
	 
			oReport:IncMeter()
			oSection1:Cell("P00_DTRECU"):SetValue(STOD((cAlQry)->P00_DTRECU))
			oSection1:Cell("P00_DTRECU"):SetAlign("CENTER")
	 
//			oSection1:Cell("P00_FILIAL"):SetValue((cAlQry)->P00_FILIAL)
//			oSection1:Cell("P00_FILIAL"):SetAlign("CENTER")
	 
//			oSection1:Cell("P00_NUM"):SetValue((cAlQry)->P00_NUM)
//			oSection1:Cell("P00_NUM"):SetAlign("LEFT")
	 
//			oSection1:Cell("P00_PREFIX"):SetValue((cAlQry)->P00_PREFIX)
//			oSection1:Cell("P00_PREFIX"):SetAlign("CENTER")
	
//			oSection1:Cell("P00_VALOR"):SetValue((cAlQry)->P00_VALOR)
//			oSection1:Cell("P00_VALOR"):SetAlign("RIGHT")
	
			oSection1:Cell("P00_USRREC"):SetValue(UsrRetName((cAlQry)->P00_USRREC))
			oSection1:Cell("P00_USRREC"):SetAlign("CENTER")
	
			oSection1:Cell("P00MOTIVO"):SetValue((cAlQry)->P00MOTIVO)
			oSection1:Cell("P00MOTIVO"):SetAlign("LEFT")

			oSection1:Cell("SE2EXCL"):SetValue((cAlQry)->EXCLUIDO)
			oSection1:Cell("SE2EXCL"):SetAlign("LEFT")
	
			oSection1:PrintLine()
	 
			(cAlQry)->(dbSkip())
		EndDo
	
	EndIf
	
	oSection1:Finish()
	(cAlQry)->(DbCloseArea())

Return

