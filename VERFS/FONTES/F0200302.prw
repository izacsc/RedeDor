#Include 'Protheus.ch'
/*
{Protheus.doc} F0200302()
Fun��o de cria��o de corpo de e-mail
@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 nSolicit
@param		 cMatricu
@param		 cTreina
@param		 cCurso
@param		 cTurma
@Return
*/

User Function F0200302(nSolicit,cMatricu,cNome, cTreina, cCurso, cTurma, cFilApr)
	
	Local oWs
	Local cEmail 	:= ''
	Local aAux 	:= {}
	
	Default nSolicit := 0
	Default cMatricu := ''
	Default cNome    := ''
	Default cTreina  := ''
	Default cCurso   := ''
	Default cTurma   := ''
	
	If FWIsInCallStack("U_F0200303")
		aAux := U_F0200305(cFilApr,cMatricu, cTreina, cCurso, cTurma)
		If ValType(aAux) != 'U'
			cEmail 	:= aAux[1]
			cTreina 	:= aAux[2]
		EndIf
	Else
		oWs := WSW0200301():new()
		WsChgURL(@oWs,"W0200301.APW")
		If oWs:CRIAENTRE(cFilApr,cMatricu, cTreina, cCurso, cTurma)
			cEmail 	:= oWs:oWSCRIAENTRERESULT:cEMAIL
			cTreina	:= oWs:oWSCRIAENTRERESULT:cTreina
		EndIf
	EndIf
	If cEmail != ''
		
		DO CASE
		CASE nSolicit == 1
			SendSoct(cTreina, cNome, cEmail)
		CASE nSolicit == 2
			RetrSoct(cTreina, cNome, cEmail)
		CASE nSolicit == 3
			RetrNeg(cTreina, cNome, cEmail)
		CASE nSolicit == 4
			SendInce(cNome, cEmail)
		CASE nSolicit == 5
			InceApr( cNome, cEmail)
		CASE nSolicit == 6
			InceRpr(cNome, cEmail)
		OTHERWISE
			conout("Opera��o invalida")
		ENDCASE
	EndIf
	
Return nil

//=========================================================================================================================================
/*
{Protheus.doc} SendSoct()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 cTreina
@param		 cNome
@param		 cEmail
@Return
*/
Static Function SendSoct(cTreina, cNome, cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	Local oWs
	
	cAssunto 	:= "Solicita��o de Aprova��o para Treinamento"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "Segue solicita��o de aprova��o para treinamento '" + cTreina + "'"
	cBody += " para o colaborador '" + cNome + "'."
	cBody += "Favor efetuar a aprova��o ou reprova��o no Portal de RH."
	cBody += '</pre></body></html>'
	
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} RetrSoct()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 cTreina
@param		 cNome
@param		 cEmail
@Return
*/
Static Function RetrSoct(cTreina, cNome, cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	
	cAssunto 	:= "Retorno Solicita��o de Aprova��o para Treinamento"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "O treinamento '" + cTreina + "' "
	cBody += "solicitado para o colaborador '" + cNome + "' foi aprovado. "
	cBody += "Para maiores informa��es, procure o RH da sua Unidade"
	cBody += '</pre></body></html>'
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} RetrNeg()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 cTreina
@param		 cNome
@param		 cEmail
@Return
*/
Static Function RetrNeg(cTreina, cNome,cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	
	cAssunto 	:= "Retorno Solicita��o 'Reprovada'"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "O treinamento '" + cTreina + "' "
	cBody += "solicitado para o colaborador '" + cNome + "' foi reprovado. "
	cBody += "Para maiores informa��es, procure o RH da sua Unidade"
	cBody += '</pre></body></html>'
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} SendInce()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@param		 cNome
@param		 cEmail
@Return
*/
Static Function SendInce(cNome,cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	
	cAssunto 	:= "Solicita��o Incentivo � Educa��o"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "Segue solicita��o de aprova��o para Incentivo � "
	cBody += "Educa��o para o colaborador '" + cNome + "'."
	cBody += "Favor efetuar a aprova��o ou reprova��o no Portal de RH."
	cBody += '</pre></body></html>'
	
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} InceApr()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@param		 cNome
@param		 cEmail
@Return
*/
Static Function InceApr( cNome,cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	
	cAssunto 	:= "Incentivo � Educa��o 'Aprovado'"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "A Solicita��o de Incentivo � Educa��o "
	cBody += "solicitado para o colaborador '" + cNome + "' foi aprovado. "
	cBody += "Para maiores informa��es, procure o RH da sua Unidade"
	cBody += '</pre></body></html>'
	
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} InceRpr()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@param		 cNome
@param		 cEmail
@Return
*/
Static Function InceRpr(cNome,cEmail)
	
	Local cBody 		:= ""
	Local cAssunto 	:= ""
	
	cAssunto 	:= "Incentivo � Educa��o 'Reprovado'"
	
	cBody := '<html><body><pre>'+CRLF
	cBody += '<b>Prezado,</b>'+CRLF
	cBody += "Prezado gestor, a Solicita��o de Incentivo � Educa��o "
	cBody += "solicitado para o colaborador '" + cNome + "' foi reprovado. "
	cBody += "Para maiores informa��es, procure o RH da sua Unidade"
	cBody += '</pre></body></html>'
	// Envia o e-mail
	SendEm(cAssunto,cBody,cEmail)
	
Return

//=========================================================================================================================================
/*
{Protheus.doc} SendEm()

@Author     Henrique Madureira
@Since      30/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_003
@Param		 cAssunto
@param		 cBody
@param		 cEmail
@Return
*/
Static Function SendEm(cAssunto,cBody,cEmail)
	
	Local oWs
	
	If FWIsInCallStack("U_F0200303")
		U_F0200304(cAssunto, cBody, cEmail)
	Else
		// Envia o e-mail
		oWs := WSW0200301():new()
		WsChgURL(@oWs,"W0200301.apw")
		oWs:PARAMENTRE(cAssunto,cBody,cEmail)
	EndIf
	
Return
