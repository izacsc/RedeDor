#Include 'PROTHEUS.CH'
#INCLUDE 'TBICONN.CH'
#Include 'FWMVCDef.ch'

/*
{Protheus.doc} F010101A()
Verifica se o t�tulo se encontra Bloqueado?
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/

User Function F010101A()
	
	If !Empty(SE2->E2_DATALIB)
		U_F010101B()
		Return
	Else
		U_F010101C()
	EndIf
	
Return

/*
{Protheus.doc} F010101B()
Exibe Mensagem: �Apenas t�tulos Bloqueados poder�o ser Recusados�
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/

User Function F010101B()
	
	Alert('Apenas t�tulos Bloqueados poder�o ser Recusados')
	
Return

/*
{Protheus.doc} F010101C()
Exibe tela de justificativa e confirma��o de recusa.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/

User Function F010101C()
	
	Local oButton1
	Local oButton2
	Local oGroup1
	Local oGroup2
	Local oSay01
	Local oSay02
	Local oSay03
	Local oSay04
	Local oSay05
	Local oSay06
	Local oSay07
	Local oSay08
	Local oSay09
	Local cIdEmit		:= ''
	Local cEmailEmit	:= ''
	Local cNomeEmit		:= ''
	Local cNomeRecu		:= ''
	Local cEmailRec		:= ''
	Local cIDRecusa		:= ''
	Local cMensagem		:= ''
	Local cStatus		:= ''
	Private oDlg
	Private oGet01
	Private oGet02
	Private oGet03
	Private oGet04
	Private oGet05
	Private oGet06
	Private oGet07
	Private oGet08
	Private oGet09
	Private oGet10
	Private oGet11
	Private oGet12
	Private oMemo1
	Private cGet01 := SE2->E2_NUM
	Private cGet02 := SE2->E2_TIPO
	Private cGet03 := SE2->E2_PARCELA
	Private nGet04 := SE2->E2_VALOR
	Private cGet05 := SE2->E2_VENCREA
	Private cGet06 := SE2->E2_FORNECE + If(!Empty(SE2->E2_LOJA),'/' , ' ') + SE2->E2_LOJA
	Private cGet07 := SE2->E2_NOMFOR
	Private cGet08 := ''
	Private cGet09 := ''
	Private cGet10 := ''
	Private cGet11 := ''
	Private cGet12 := ''
	Private cMemo1 := ''
	
	If !Empty(SE2->E2_XSTRECU)
		If SE2->E2_XSTRECU == 'R'
			cStatus := ' R - Recusado.'
		ElseIf SE2->E2_XSTRECU == 'C'
			cStatus := ' C - Corrigido.'
		EndIf
		If !Empty(cStatus)
			cMensagem := 'O status do t�tulo �:' + cStatus + CRLF
			cMensagem += 'N�o ser� processada a recusa.'
			MsgAlert(cMensagem)
			cMensagem := ''
			cStatus   := ''
		EndIf
		Return
	EndIf
	
	/*==============================================================|
	|Captura emitente.                                              |
	|==============================================================*/
	cIdEmit := SUBS(Embaralha(SE2->E2_USERLGI,01),03, 06)
	PSWORDER(01)
	If (PSWSEEK(cIdEmit))
		cEmailEmit	:= Alltrim(PSWRET()[01][14])
		cNomeEmit	:= Alltrim(PSWRET()[01][04])
	EndIf
	cGet08 := cIdEmit
	cGet09 := cNomeEmit
	
	If Empty(cIdEmit)
		cMensagem := 'Emitente n�o identificado.'    + CRLF
		cMensagem += 'N�o ser� processada a recusa.'
		MsgAlert(cMensagem)
		cMensagem := ''
		Return
	EndIf
	
	/*==============================================================|
	|Captura recusante.                                             |
	|==============================================================*/
	cIDRecusa		:= RetCodUsr()
	PSWORDER(01)
	If (PSWSEEK(cIDRecusa))
		cEmailRec	:= Alltrim(PSWRET()[01][14])
		cNomeRecu	:= Alltrim(PSWRET()[01][04])
	EndIf
	cGet10 := cIDRecusa
	cGet11 := cNomeRecu
	
	If Empty(cIDRecusa)
		cMensagem := 'Recusante n�o identificado.'    + CRLF
		cMensagem += 'N�o ser� processada a recusa.'
		MsgAlert(cMensagem)
		cMensagem := ''
		Return
	EndIf
	
	DEFINE MSDIALOG oDlg TITLE 'Recusa de T�tulo' FROM 000, 000  TO 500, 700 COLORS 0, 16777215 PIXEL
	
	@ 000, 004 GROUP oGroup1 TO 211, 347 PROMPT 'Detalhes (T�tulo a Pagar)' OF oDlg COLOR 0, 16777215 PIXEL
	
	@ 021, 008 SAY	 oSay01 PROMPT	'T�tulo' 	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 032 MSGET oGet01 VAR		cGet01 	 	 SIZE 044, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 021, 088 SAY	 oSay02 PROMPT	'Tipo' 	 	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 102 MSGET oGet02 VAR		cGet02 	 	 SIZE 022, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 021, 134 SAY	 oSay03 PROMPT	'Parcela'	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 159 MSGET oGet03 VAR		cGet03 		 SIZE 016, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 021, 192 SAY	 oSay04 PROMPT	'Valor' 	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 208 MSGET oGet04 VAR		nGet04 		 SIZE 051, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F. PICTURE '@E 9,999,999,999,999.99'
	@ 021, 266 SAY	 oSay05 PROMPT	'Vencimento' SIZE 029, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 301 MSGET oGet05 VAR		cGet05 		 SIZE 041, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 047, 008 SAY	 oSay06 PROMPT	'Fornecedor' SIZE 033, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 047, 040 MSGET oGet06 VAR		cGet06 		 SIZE 048, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 047, 093 MSGET oGet07 VAR		cGet07 		 SIZE 249, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	
	@ 066, 008 SAY	 oSay07 PROMPT	'Emitente' 	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 066, 040 MSGET oGet08 VAR		cGet08 		 SIZE 048, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 066, 093 MSGET oGet09 VAR		cGet09 		 SIZE 249, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	
	@ 082, 008 SAY	 oSay08 PROMPT	'Recusante'  SIZE 028, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 082, 040 MSGET oGet10 VAR		cGet10		 SIZE 047, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	@ 082, 093 MSGET oGet11 VAR		cGet11		 SIZE 249, 010 OF oDlg COLORS 0, 16777215 PIXEL WHEN .F.
	
	@ 107, 008 SAY	 oSay09 PROMPT	'Motivo' 	 SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 107, 040 GET   oMemo1 VAR     cMemo1 MEMO  SIZE 302, 100 OF oDlg COLORS 0, 16777215 PIXEL
	
	
	
	
	@ 215, 004 GROUP oGroup2 TO 249, 347 OF oDlg COLOR 0, 16777215 PIXEL
	@ 226, 233 BUTTON oButton1 PROMPT 'Cancelar' SIZE 037, 012 OF oDlg PIXEL ACTION U_F010101F()
	@ 226, 296 BUTTON oButton2 PROMPT 'Ok' 		 SIZE 037, 012 OF oDlg PIXEL ACTION U_F010101D(	SE2->E2_FILIAL	,;
																								SE2->E2_NUM		,;
																								SE2->E2_TIPO	,;
																								SE2->E2_PARCELA ,;
																								SE2->E2_PREFIXO	,;
																								SE2->E2_VALOR	,;
																								SE2->E2_VENCREA	,;
																								SE2->E2_FORNECE	,;
																								SE2->E2_NOMFOR	,;
																								cGet08			,;
																								cGet09			,;
																								cGet10			,;
																								cGet11			,;
																								cMemo1			,;
																								dDatabase		,;
																								SE2->E2_ORIGEM	,;
																								cEmailEmit)
	ACTIVATE MSDIALOG oDlg CENTERED
	
Return

/*
{Protheus.doc} F010101D()
Grava informa��es da Recusa.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101D(cFilOri,cNumTit,cTipo,cParcela,cPrefixo,nValor,dVencrea,cFornece,cNomFor,cCodEmit,cNomEmit,cCodRecu,cNomRecu,cMotivo,dData,cOrigem,cEmailEmit)
	Begin Transaction
		RecLock('SE2',.F.)
		SE2->E2_XDTRECU := dDatabase
		SE2->E2_XSTRECU := 'R'
		SE2->(MsUnlock())
		
		RecLock('P00',.T.)
		P00->P00_FILIAL := cFilOri
		P00->P00_NUM	:= cNumTit
		P00->P00_PREFIX	:= cPrefixo
		P00->P00_VALOR	:= nValor
		P00->P00_XDTVEN	:= dVencrea
		P00->P00_FORNEC	:= cFornece
		P00->P00_USREMI	:= cCodEmit
		P00->P00_USRREC	:= cCodRecu
		P00->P00_MOTIVO	:= cMotivo
		P00->P00_DTRECU	:= dData
		P00->P00_ORIGEM	:= cOrigem
		SE2->(MsUnlock())
	End Transaction
	
	//Pesquisa o respons�vel pela inclus�o do t�tulo.
	U_F010101E(cFilOri,cNumTit,cTipo,cParcela,cPrefixo,nValor,dVencrea,cFornece,cNomFor,cCodEmit,cNomEmit,cCodRecu,cNomRecu,cMotivo,dData,cOrigem,cEmailEmit)
	
	//Fecha tela de Recusa.
	U_F010101F()
	
Return

/*
{Protheus.doc} F010101E()
Pesquisa o respons�vel pela inclus�o do t�tulo.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101E(cFilOri,cNumTit,cTipo,cParcela,cPrefixo,nValor,dVencrea,cFornece,cNomFor,cCodEmit,cNomEmit,cCodRecu,cNomRecu,cMotivo,dData,cOrigem,cEmailEmit)
	
	If Empty(cEmailEmit)
		cEmailEmit := GetMv('FS_MAILREC')
	EndIf
	
	U_F0101004(cFilOri,cNumTit,cTipo,cParcela,cPrefixo,nValor,dVencrea,cFornece,cNomFor,cCodEmit,cNomEmit,cCodRecu,cNomRecu,cMotivo,dData,cOrigem,cEmailEmit)
	
Return

/*
{Protheus.doc} F010101F()
Fecha tela de Recusa.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101F()
	cGet01 := ''
	cGet02 := ''
	cGet03 := ''
	nGet04 := 0
	cGet05 := ''
	cGet06 := ''
	cGet07 := ''
	cGet08 := ''
	cGet09 := ''
	cGet10 := ''
	cGet11 := ''
	cGet12 := ''
	
	oDlg:End()
	
Return

/*
{Protheus.doc} F010101G()
Consulta Recusas.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101G()
	
	Local oButton1
	Local oGroup1
	Local oGroup2
	Local cQuery	:= ''
	Local cAlias01	:= ''
	Local cMensagem := ''
	Local cSGBD		:= TcGetDB()
	Local aWBrowse1 := {}
	Local cNomeRecu := ''
	Static oDlg
	
	If Select('cAlias01') > 0
		cAlias01->(dbCloseArea())
	EndIf
	
	cAlias01	:= GetNextAlias()
	
	cQuery := "SELECT    P00.P00_DTRECU DATA_RECUSA, " + CRLF
	cQuery += "          P00.P00_NUM    TITULO	   , " + CRLF
	cQuery += "          P00.P00_USRREC USUARIO	   , " + CRLF
	If ALLTRIM(cSGBD) == 'MSSQL' "
		cQuery += " ISNULL(CONVERT(VARCHAR(8000),CONVERT(BINARY(8000), P00_MOTIVO)), '') MOTIVO " + CRLF
	ElseIf ALLTRIM(cSGBD) == 'ORACLE'
		cQuery += "	utl_raw.cast_to_varchar2(P00_MOTIVO)  MOTIVO  " + CRLF
	EndIf
//	cQuery += "          CASE WHEN E2_NUM IS NULL THEN 'Sim' ELSE CASE WHEN E2_XSTRECU = 'C' THEN 'Sim' ELSE 'Nao' END END EXCLUIDO " + CRLF
//	cQuery += "          CASE WHEN E2_XSTRECU = 'C' THEN 'Sim' ELSE 'Nao' END CORRIGIDO " + CRLF
	cQuery += "FROM " + RETSQLNAME('P00') + "  P00 INNER JOIN " + RETSQLNAME('SE2') + "  SE2 ON	    SE2.E2_FILIAL  = P00.P00_FILIAL " + CRLF
	cQuery += "                                                                                 AND	SE2.E2_NUM     = P00.P00_NUM    " + CRLF
	cQuery += "                                                                                 AND	SE2.E2_PREFIXO = P00.P00_PREFIX " + CRLF
	cQuery += "                                                                                 AND	SE2.E2_FORNECE = P00.P00_FORNEC " + CRLF
	cQuery += "WHERE P00.D_E_L_E_T_ = '' AND " + CRLF
	cQuery += "	     SE2.D_E_L_E_T_ = '' AND " + CRLF
	cQuery += "      SE2.E2_FILIAL  = '" + SE2->E2_FILIAL  + "' AND " + CRLF
	cQuery += "      SE2.E2_NUM     = '" + SE2->E2_NUM     + "' AND " + CRLF
	cQuery += "      SE2.E2_PREFIXO = '" + SE2->E2_PREFIXO + "' AND " + CRLF
	cQuery += "      SE2.E2_FORNECE = '" + SE2->E2_FORNECE + "'" + CRLF
	cQuery += "ORDER BY P00_DTRECU " + CRLF
	
	cQuery := ChangeQuery(cQuery)
	
	DbUseArea(.T., 'TOPCONN', TcGenQry(,,cQuery), cAlias01, .F., .T.)
	(cAlias01)->(dbGoTop())
	
	If (cAlias01)->(Eof())
		If !IsInCallStack('MSEXECAUTO')
			cMensagem := 'N�o h� recusas para o t�tulo selecionado.
			MsgAlert(cMensagem)
		EndIf
		Return
	EndIf
	
	While (cAlias01)->(!Eof())
		PSWORDER(01)
		If (PSWSEEK((cAlias01)->USUARIO))
			cNomeRecu	:= Alltrim(PSWRET()[01][04])
		EndIf
//		Aadd(aWBrowse1,{DTOC(STOD((cAlias01)->DATA_RECUSA)),(cAlias01)->TITULO,cNomeRecu,(cAlias01)->CORRIGIDO,(cAlias01)->MOTIVO})
		Aadd(aWBrowse1,{DTOC(STOD((cAlias01)->DATA_RECUSA)),(cAlias01)->TITULO,cNomeRecu,(cAlias01)->MOTIVO})
		cNomeRecu := ''
		(cAlias01)->(dBSkip())
	EndDo
	
	If Select('cAlias01') > 0
		(cAlias01)->(dbCloseArea())
	EndIf

	DEFINE MSDIALOG oDlg TITLE "Consulta Recusas do T�tulo" FROM 000, 000  TO 400, 800 COLORS 0, 16777215 PIXEL
	@ 003, 004 GROUP oGroup1 TO 165, 397 OF oDlg COLOR 0, 16777215 PIXEL
	@ 167, 004 GROUP oGroup2 TO 196, 397 OF oDlg COLOR 0, 16777215 PIXEL
	F010101H(aWBrowse1)
	@ 177, 351 BUTTON oButton1 PROMPT "Sair" SIZE 037, 012 OF oDlg PIXEL ACTION oDlg:End()
	ACTIVATE MSDIALOG oDlg CENTERED
	
Return

/*
{Protheus.doc} F010101H()
Monta grid com �tens da Consulta.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
Static Function F010101H(aWBrowse1)
	Local oWBrowse1
//	@ 011, 009 LISTBOX oWBrowse1 Fields HEADER 'Data Recusa','N�mero do T�tulo','Usu�rio','Corrigido','Motivo da Recusa' SIZE 381, 146 OF oDlg PIXEL ColSizes 50,50
	@ 011, 009 LISTBOX oWBrowse1 Fields HEADER 'Data Recusa','N�mero do T�tulo','Usu�rio','Motivo da Recusa' SIZE 381, 146 OF oDlg PIXEL ColSizes 50,50
	oWBrowse1:SetArray(aWBrowse1)
	oWBrowse1:bLine := {|| {aWBrowse1[oWBrowse1:nAt,1],;
							aWBrowse1[oWBrowse1:nAt,2],;
							aWBrowse1[oWBrowse1:nAt,3],;
							aWBrowse1[oWBrowse1:nAt,4]}}//,;
//							aWBrowse1[oWBrowse1:nAt,5]}}
	//    // DoubleClick event
	//    oWBrowse1:bLDblClick := {|| aWBrowse1[oWBrowse1:nAt,1] := !aWBrowse1[oWBrowse1:nAt,1],;
	//    oWBrowse1:DrawSelect()
Return

/*
{Protheus.doc} F010101I()
Manuten��o de Anexos.
Nas manuten��es (inclus�es ou exclus�es) de documentos
digitalizados anexos, verifica se existem T�tulos a Pagar
Recusados vinculados � cadeia de relacionamento de tabelas.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101I(cFilOri, cRotina, cCodOri) 
	
	Local cTitulo := ''
	Local cAlias01:= ''
	Local cCompara:= ''
	
	If cRotina == 'FINA050'  //Contas a Pagar
		cCompara := cFilOri + cCodOri 
		cAlias01 := GetNextAlias()
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ AS NUM_RECNO
			FROM 	%Table:SE2% SE2
			WHERE 	SE2.%notDel%
			AND SE2.E2_FILIAL || SE2.E2_NUM || SE2.E2_PREFIXO || SE2.E2_FORNECE || SE2.E2_LOJA = %Exp:cCompara%
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
	ElseIf cRotina $ 'MATA103|MATA140' //Nota/Pre Nota de Entrada
		
		cAlias01 := GetNextAlias()
		cCompara := cFilOri + cCodOri
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ AS NUM_RECNO
			FROM %Table:SE2% SE2 INNER JOIN %Table:SF1% SF1  ON		SF1.F1_FILIAL	= SE2.E2_FILIAL
			AND	SF1.F1_DOC		= SE2.E2_NUM
			AND	SF1.F1_SERIE	= SE2.E2_PREFIXO
			AND	SF1.F1_FORNECE	= SE2.E2_FORNECE
			AND	SF1.F1_LOJA		= SE2.E2_LOJA
			WHERE 		SE2.%notDel%
			AND	SF1.%notDel%
			AND	SF1.F1_FILIAL || SF1.F1_DOC || SF1.F1_SERIE || SF1.F1_FORNECE || SF1.F1_LOJA = %Exp:cCompara%
			
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
	ElseIf cRotina $ 'MATA121|F0100401' // Pedido de Compra/Solicita��o de Pagamento
		cCompara := cFilOri + cCodOri
		cAlias01 := GetNextAlias()
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ NUM_RECNO
			FROM %Table:SE2% SE2 INNER JOIN (SELECT DISTINCT	SD1.D1_FILIAL	  FILIAL	,
			SD1.D1_DOC		  DOC		,
			SD1.D1_SERIE	  SERIE		,
			SD1.D1_FORNECE	  FORNECE	,
			SD1.D1_LOJA		  LOJA
			FROM %Table:SC7% SC7 INNER JOIN %Table:SD1% SD1 ON	SD1.D1_FILIAL = SC7.C7_FILIAL
			AND	SD1.D1_PEDIDO = SC7.C7_NUM
			AND	SD1.D1_ITEMPC = SC7.C7_ITEM
			WHERE 	SC7.%notDel%		AND
			SD1.%notDel%		AND
			SC7.C7_FILIAL || SC7.C7_NUM  = %Exp:cCompara%)   SD1A ON  	SD1A.FILIAL	 =	SE2.E2_FILIAL
			AND	SD1A.DOC	 =	SE2.E2_NUM
			AND	SD1A.SERIE	 =	SE2.E2_PREFIXO
			AND	SD1A.FORNECE =	SE2.E2_FORNECE
			AND	SD1A.LOJA	 =	SE2.E2_LOJA
			WHERE SE2.%notDel%
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
		
	ElseIf cRotina  == 'MATA110' //Solicita��o de Compra
		cCompara := cFilOri + cCodOri 
		cAlias01 := GetNextAlias()
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ NUM_RECNO
			FROM %Table:SE2% SE2 INNER JOIN (SELECT DISTINCT SD1.D1_FILIAL  FILIAL  ,
			SD1.D1_DOC     DOC     ,
			SD1.D1_SERIE   SERIE   ,
			SD1.D1_FORNECE FORNECE ,
			SD1.D1_LOJA    LOJA
			FROM	%Table:SC1% SC1	INNER JOIN %Table:SC7% SC7 ON	SC7.C7_FILIAL = SC1.C1_FILIAL
			AND SC7.C7_NUM 	  = SC1.C1_PEDIDO
			AND SC7.C7_ITEM   = SC1.C1_ITEMPED
			INNER JOIN %Table:SD1% SD1 ON 	SD1.D1_FILIAL = SC7.C7_FILIAL
			AND SD1.D1_PEDIDO = SC7.C7_NUM
			AND SD1.D1_ITEMPC = SC7.C7_ITEM
			WHERE 	SC1.%notDel%    AND
			SD1.%notDel%    AND
			SC7.%notDel%    AND
			SC1.C1_FILIAL || SC1.C1_NUM = %Exp:cCompara%)  SD1A ON 	SD1A.FILIAL	 =	SE2.E2_FILIAL
			AND	SD1A.DOC	 =	SE2.E2_NUM
			AND	SD1A.SERIE	 =	SE2.E2_PREFIXO
			AND	SD1A.FORNECE =	SE2.E2_FORNECE
			AND	SD1A.LOJA	 = 	SE2.E2_LOJA
			WHERE SE2.%notDel%
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
	ElseIf cRotina $ 'GPEM660' //Manuten��o de Benef�cios
		
		cAlias01 := GetNextAlias()
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ AS NUM_RECNO
			FROM 	%Table:SE2% SE2
			WHERE 	SE2.%notDel%
			AND SE2.E2_FILIAL || SE2.E2_PREFIXO || SE2.E2_NUM || SE2.E2_TIPO || SE2.E2_FORNECE = %Exp:cCodOri%
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
	ElseIf cRotina $ 'AE_DESPV' //Presta��o de Contas
		
		cTitulo := Posicione('LHP',01,xFilial('LHQ') + cCodOri,'LHP_DOCUME')
		
		cAlias01 := GetNextAlias()
		BeginSql Alias cAlias01
			%noparser%
			SELECT SE2.R_E_C_N_O_ AS NUM_RECNO
			FROM 	%Table:SE2% SE2
			WHERE 	SE2.%notDel%
			SE2.E2_FILIAL || SE2.E2_PREFIXO || SE2.E2_NUM || SE2.E2_PARCELA || SE2.E2_TIPO || SE2.E2_FORNECE || SE2.E2_LOJA = %Exp:cTitulo%
		EndSql
		
		While (cAlias01)->(!Eof())
			dBSelectArea('SE2')
			dBGoTo((cAlias01)->NUM_RECNO)
			If SE2->E2_XSTRECU == 'R'
				Reclock('SE2',.F.)
				SE2->E2_XSTRECU := 'C'
				SE2->E2_XDTRECU := dDataBase
				SE2->(MsUnlock())
			EndIf
			(cAlias01)->(dBSkip())
		EndDo
		If Select('cAlias01') > 0
			(cAlias01)->(dbCloseArea())
		EndIf
		
	EndIf
	
Return

/*
{Protheus.doc} F010101M() 
Aciona ponto de entrada FA580LIB.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101M()
	
	Local cMensagem := ''
	Local lRet		:= .T.
	Local aArea		:= GetArea()
	
	If SE2->E2_XSTRECU  == 'R'
		If !IsInCallStack('MSEXECAUTO')
			cMensagem := 'T�tulo Recusado.' + CRLF
			cMensagem += 'N�o pode ser liberado.'
			MsgAlert(cMensagem)
		EndIf
		lRet := .F.
	EndIf
	RestArea(aArea)
Return(lRet)

/*
{Protheus.doc} F010101N()
Aciona ponto de entrada  F580LBA.
@Author     Paulo Kr�ger
@Since      18/01/2017
@Version    P12.7
@Project    MAN00000462901_EF_010
*/
User Function F010101N()
	Local cMensagem := ''
	Local lRet		:= .T.
	Local cAliasXX	:= ALIAS()
	Local aArea		:= GetArea()
	Local nRecnoSE2	:= SE2->(Recno())
	
	SE2->(dbGoTo((cAliasXX)->RECNO))
	If SE2->E2_XSTRECU  == 'R'
		If !IsInCallStack('MSEXECAUTO')
			cMensagem := 'T�tulo Recusado:' + SE2->E2_PREFIXO + If(!Empty(SE2->E2_PREFIXO),'-','') + SE2->E2_NUM + '-' + SE2->E2_NOMFOR + '.' + CRLF
			cMensagem += 'N�o pode ser liberado.'
			MsgAlert(cMensagem)
		EndIf
		lRet := .F.
	EndIf
	SE2->(dBGoTo(nRecnoSE2))
	RestArea(aArea)
Return(lRet)