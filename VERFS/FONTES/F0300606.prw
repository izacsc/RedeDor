#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "FWMVCDEF.CH"
#include 'FILEIO.ch'
/*
{Protheus.doc} F0300606()
Exporta��o de Transferencia de Colaborador
@Author     Rogerio Candisani
@Since      31/10/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300606()

Local ctmpTrf
Local ctmpDep 
Local montaTxt:= ""
Local cPerg:= "FSW0300606"	// Gp. perguntas especifico
Local lRet:= .F. // confirma a opera��o
Local Lgerou:= .F.

//exportar os dados de exclus�o do colaborador
//criar pergunta F0300606

/*	Exporta��o de Transferencia de Colaborador
MV_PAR01 Filial De
MV_PAR02 Filial At�
MV_PAR03 Transferencia De
MV_PAR04 Transferencia At�
MV_PAR05 Tipo de Arquivo (M�dico / Odontol�gico)
MV_PAR06 Caminho de Grava��o
*/

// Verificar na tabela SRE, somente para os casos onde a filial destino seja diferente da origem. N�O SE APLICA A TRANSFERENCIAS INTERNAS (CENTRO DE CUSTO, DEPARTAMENTO, ETC);
//	Verificar na tabela RHK se o colaborador transferido possui um plano ativo;
//	Ser� inclu�do no arquivo somente se satisfeita as condi��es acima.
// Dever� gerar todas as colunas, ainda que algumas sem conte�do.

// Colunas 1, 2, 6 e 14 - Informar somente a coluna, o dado ser� vazio;
//	As Colunas que foram indicadas para serem geradas �em branco� ter�o dados informados manualmente pela equipe de benef�cios;
//	A Coluna 10 dever� ser preenchida com a data da transfer�ncia;
//	Os tamanhos das colunas s�o livres, e dever�o ser separados por �; �.

Pergunte(cPerg,.T.)

If (MsgYesNo(OemToAnsi("Confirma a Exporta��o de Transferencia de Colaborador ?"),OemToAnsi("Atencao")))
	lRet:= .T.
Else
	lRet:= .F.
Endif


If lRet
	
	//montar query de transferencia do colaborador  //%xFilial:RHK% = RHK.RHK_FILIAL AND
	
	//PRESERVAR CNPJ DE TODAS AS FILIAIS DO SIGAMAT
	Private aCnpjFil := {}
	DbSelectArea("SM0")
	dbGoTop()
	While !Eof()
		Aadd(aCnpjFil, { FWGETCODFILIAL, SM0->M0_CGC,  SM0->M0_CODMUN})
		SM0->(dbSkip())
	EndDo 
	
	//mpos:=Ascan(aCnpjFil,tmpTrf->RE_FILIALD)
	
	
	CtmpTrf:=GetNextAlias()
	BeginSql Alias CtmpTrf
		SELECT RE_CCD, RE_DATA, RE_FILIAL, RE_FILIALD, RE_MATD, RE_FILIALP, RE_MATP, RE_CCP
		FROM %table:SRE% SRE
		Inner join %table:RHK% RHK ON 
			RHK.RHK_MAT = SRE.RE_MATD AND 
			RHK.RHK_TPFORN = %Exp:MV_PAR05% AND
			RHK.%NotDel% 
		WHERE SRE.RE_FILIALD BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
			AND SRE.RE_DATA BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04%
			AND SRE.RE_FILIALD <> SRE.RE_FILIALP 
			AND SRE.%NotDel%
		ORDER BY %Order:SRE%
	EndSql	
	
	DbSelectArea(CtmpTrf)
	(CtmpTrf)->(DbGoTop())
	cMontaTxt:= ""
	
	While ! (CtmpTrf)->(EOF())
		//mpos:=Ascan(aCnpjFil[1],Substr((CtmpTrf)->RE_FILIALD,1,len((CtmpTrf)->RE_FILIAL)))
		mpos := Ascan(aCnpjFil,{|X| AllTrim(X[01]) == AllTrim((CtmpTrf)->RE_FILIALD)})
		If mpos > 0
			cCNPJde:= AcNPJfIL[MPOS,2]
			//mpos:=Ascan(aCnpjFil[1],Substr((CtmpTrf)->RE_FILIALD,1,len((CtmpTrf)->RE_FILIAL)))
			mpos := Ascan(aCnpjFil,{|X| AllTrim(X[01]) == AllTrim((CtmpTrf)->RE_FILIALP)})
			
			If 	mpos > 0
				lGerou:=.T.

				cCNPJpara:= AcNPJfIL[MPOS,2]
				//monta o txt do titular
				cMontaTxt += ";" // 1 - branco
				cMontaTxt += ";" // 2 - branco
				cMontaTxt += cCNPJde + ";" // 3 - cnpj da filial origem
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(CtmpTrf)->RE_MATD,"RA_NOME") + ";" // 4 - nome do titular
				cMontaTxt += (CtmpTrf)->RE_MATD + ";" // 5 - matricula 
				cMontaTxt += ";" // 6 - branco 
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(CtmpTrf)->RE_MATD,"RA_CIC") + ";" // 7 - CPF do titular
				cMontaTxt += cCNPJpara + ";" // 8 - cnpj da filial destino
				cMontaTxt += (CtmpTrf)->RE_MATP + ";" // 9 - nova matricula
				cMontaTxt += subst((ctmpTrf)->RE_DATA,7,2) + "/" + subst((ctmpTrf)->RE_DATA,5,2) + "/" + subst((ctmpTrf)->RE_DATA,1,4) + ";" // 10 - data de inicio do beneficio DDMMAAAA
				cMontaTxt += (CtmpTrf)->RE_CCP +  ";" // 11 - centro de custo
				cMontaTxt += Posicione("CTT",1,xFilial("CTT")+ (CtmpTrf)->RE_CCP,"CTT_DESC01") + ";" // 12 - nome do centro de custo
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(CtmpTrf)->RE_MATP,"RA_CARGO") + ";" // 13 - novo cargo
				cMontaTxt += ";" // 14 - branco
				cMontaTxt += CHR(13) + CHR(10) 
			EndIf
		EndIf
		(CtmpTrf)->(dBSkip())
	Enddo
	
	//fechar arquivos temporarios
	(CtmpTrf)->(DbCloseArea())
	
	//gerar o arquivo
	If lgerou
		criaCSV()
	Else
		MsgAlert("N�o existem dados a serem gerados, verifique os parametros utilizados")
	Endif 

Endif

Return lRet

///////////////////////////////////////////////////////////////////////
// Exportando dados para planilha 
////////////////////////////////////////////////////////////////////////
Static Function criaCSV()

// Nome do arquivo criado, o nome � composto por umam descri��o 
//a data e a hora da cria��o, para que n�o existam nomes iguais
cNomeArq := alltrim(MV_PAR06) + ".csv"

// criar arquivo texto vazio a partir do root path no servidor
nHandle := FCREATE(cNomeArq)

FWrite(nHandle,cMontaTxt)

// encerra grava��o no arquivo
FClose(nHandle)

FOPEN(cNomeArq, FO_READWRITE)

//MsgAlert("Relatorio salvo em:" + cNomeArq )

Return
