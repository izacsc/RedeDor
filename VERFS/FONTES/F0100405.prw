#Include 'Protheus.ch'

/*
{Protheus.doc} F0100405()
Fun��o para a Inclus�o do Bot�o Recusa no Documento de Entrada - PE MA103OPC
@Author     Mick William da Silva
@Since      09/05/2016
@Version    P12.7
@Project    MAN00000462901_EF_004
@Return		aRet, Vetor aRotina     
*/

User Function F0100405()

	Local aRet := {}
	
	aAdd(aRet, {'Recusa', 'U_F0101001', 0, 1} )

Return aRet

