#Include 'PROTHEUS.CH'

/*/{Protheus.doc} F0700301
M�todo que insere, atualiza ou apaga o registro.
@type 		function
@author 	alexandre.arume
@since 		24/01/2017
@version 	1.0
@param 		oSetor, objeto, Objeto com os campos e valores
@param 		nOpc, num�rico, 1 = Upsert; 2 = Delete
@project	MAN000007423041_EF_003
@return 	cRetorno, Mensagem de sucesso ou erro

/*/
User Function F0700301(oSetor, nOpc)
	
	Local cRetorno := ""
    Local cChave   := ""
    Local aCampos  := {}
	
	// Upsert.
	If nOpc == 1
    
    	aCampos := {;
	                    {"P11_FILIAL", 	oSetor:cFil		},;
	                    {"P11_COD",    	oSetor:cCod		},;
	                    {"P11_DESC", 	oSetor:cDesc	},;
	                    {"P11_CCUSTO", 	oSetor:cCCusto	},;
	                    {"P11_MSBLQL", 	oSetor:cMSBLQL	},;
	                    {"P11_ID",     	U_GetIntegID()	}; //-- fun��o pra pegar o ID
					}
    
    	cChave := U_RetChave("P11", aCampos, {1, 2})
    	
    	cRetorno := U_UpsMVCRg("P11", cChave, "F0700101", "P11MASTER", aCampos)
	
	// Delete.
	Else
    	
    	aCampos := {;
	                    {"P11_FILIAL", 	oSetor:cFil		},;
	                    {"P11_COD",    	oSetor:cCod		};
					}
    
    	cChave := U_RetChave("P11", aCampos, {1, 2})
    	
    	cRetorno := U_DelMVCRg("P11", cChave, "F0700101")
    	
    EndIf

    aCampos := ASize(aCampos, 0)
    aCampos := Nil
	
Return cRetorno
