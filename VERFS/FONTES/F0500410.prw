#Include 'Protheus.ch'
//---------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} F0500410
Tela para atualizar alguns os parametros especificos 
@type function
@author Cris
@since 16/12/2016
@version 1.0
@version P12.1.7
@Project MAN0000007423039_EF_004
@return ${return}, ${n�o h�}
/*///---------------------------------------------------------------------------------------------------------------------------
User Function F0500410()

Local aPergs := {}
Local cCodRec := space(08)
Local aRet := {}
Local lRet   
Local cCodTab	:= GetMV('FS_CTABAXV')//Codigo da Tabela da Vis�o x mvto
Local cBlqSlTF	:= Alltrim(GetMV('FS_BLQSLTF'))//Indica se bloqueia a solicita��o de transf com aumento salarial
Local cDeptCan	:= GetMV('FS_DEPTCAN')//departamentos que podem ter acesso ao botar cancelar solicita��o
Local cVSalSup	:= GetMV('FS_VSALSUP')//coodiusuarioogo da vis�o diferenciada
Local pSalSup	:= GetMV('FS_PSALSUP')//percentual do atingido para acionar a vis�o diferenciada
Local aOpcSN	:= {}
Local cOpcBlq	:= ''

	if cBlqSlTF == 'S' .OR. Empty(cBlqSlTF)
	
		aOpcSN	:= {"Sim", "Nao"}
	
	Else
	
		aOpcSN	:= {"Nao","Sim"}
		
	EndIf
	
 	aAdd( aPergs ,{3,"Bloqueia Solicita��o de Transfer�ncia com altera��o de Sal�rio",1,aOpcSN , 50,'.T.',.T.})
 	//aAdd( aPergs ,{9,"FS_BLQSLTF",50,70,.T.})
 	aAdd( aPergs ,{1,"Vis�o Diferenciada",cVSalSup,"@!",'.T.','RDK','.T.',50,.F.})
 	aAdd( aPergs ,{1,"Percentual de Aumento Salarial",pSalSup,"@ 99.99",'positivo()',,'.T.',50,.F.}) 
 	aAdd( aPergs ,{1,"Tabela Vis�o X Tipos Mvto",cCodTab,"@!",'.T.','RCB','.T.',50,.F.})    
  	aAdd( aPergs ,{1,"Departamentos x bot�o Cancelar",cDeptCan+space(32),"@!",'.T.',,'.T.',50,.F.}) 
  	    
	 If ParamBox(aPergs ,"Parametros ",aRet)      
	 	
	 	dbSelectArea('SX6')
	 	cOpcBlq	:= Iif(aRet[1]==1,IIF(cBlqSlTF== 'S','S','N'),IIF(cBlqSlTF=='N','N','S'))
	 	if cOpcBlq <> cBlqSlTF
	 	   
	 	   if SX6->(dbSeek(FwxFilial('SX6')+'FS_BLQSLTF'))
	 	   
	 	   		SX6->(Reclock('SX6',.F.))
	 	   		SX6->X6_CONTEUD	:= cOpcBlq
	 	   		SX6->(MsUnlock())
	 	   		
	 	   EndIf
	 	   
 		EndIf
 		
 		if Alltrim(aRet[2]) <> Alltrim(cVSalSup)
 		
	 		 if SX6->(dbSeek(FwxFilial('SX6')+'FS_VSALSUP'))
		 	   
		 	   		SX6->(Reclock('SX6',.F.))
		 	   		SX6->X6_CONTEUD	:= Alltrim(aRet[2]) 
		 	   		SX6->(MsUnlock())
		 	   		
		 	 EndIf
	 	   
 		EndIf
 		
 		if  aRet[3] <> pSalSup
  		
	 		 if SX6->(dbSeek(FwxFilial('SX6')+'FS_PSALSUP'))
		 	   
		 	   		SX6->(Reclock('SX6',.F.))
		 	   		SX6->X6_CONTEUD	:= Transform(aRet[3],'@ 99.99')
		 	   		SX6->(MsUnlock())
		 	   		
		 	 EndIf
		 	 		
 		EndIf
 
  		if  aRet[4] <> cCodTab
   		
	 		 if SX6->(dbSeek(FwxFilial('SX6')+'FS_CTABAXV'))
		 	   
		 	   		SX6->(Reclock('SX6',.F.))
		 	   		SX6->X6_CONTEUD	:= aRet[4] 
		 	   		SX6->(MsUnlock())
		 	   		
		 	 EndIf
		 	 		
 		EndIf		
 
   		if  Alltrim(aRet[5]) <> cDeptCan		
 	 		 
 	 		 if SX6->(dbSeek(FwxFilial('SX6')+'FS_DEPTCAN'))
		 	   
		 	   		SX6->(Reclock('SX6',.F.))
		 	   		SX6->X6_CONTEUD	:= Alltrim(aRet[5])
		 	   		SX6->(MsUnlock())
		 	   		
		 	 EndIf
		 	 	
 		EndIf 
 		
 	 Else      
 	 	Alert("Pressionado Cancel")     
		lRet := .F.   
	EndIf

Return