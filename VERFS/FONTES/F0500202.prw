#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

Static cAliasPA5		

/*/{Protheus.doc} F0500202
Fun��o que exibe o Browse com os registros da Solicita��o de Vagas
@author Fernando Carvalho
@since 07/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/

User Function F0500202()
	Local oBrowse
	Local cFiltro  := ""
	cAliasPA5			:= GetNextAlias()
	cFiltro := "Empty(RH3_TIPO)"
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias("RH3")
	oBrowse:SetFilterDefault(cFiltro)
	oBrowse:SetOnlyFields({'RH3_FILIAL','RH3_CODIGO','RH3_XDSCTM','RH3_STATUS','RH3_DTSOLI'})
	oBrowse:SetDescription('Monitor das Solicita��es')
	oBrowse:Activate()
	
Return NIL

//-------------------------------------------------------------------
Static Function MenuDef()
	Local aRotina := {}
	
	ADD OPTION aRotina TITLE 'Visualizar' 		ACTION 'VIEWDEF.F0500202' 	OPERATION 2  ACCESS 0		
		
Return aRotina
//-------------------------------------------------------------------


/*/{Protheus.doc} ModelDef
Menu da rotina de Solicita��es de Vagas
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@return oModel - Objeto do modelo do MVC
@project MAN0000007423039_EF_002
/*/
Static Function ModelDef()
	// Cria a estrutura a ser usada no Modelo de Dados
	Local oStrField		:= FieldModelo()
	Local oStruPA5 		:= GridModelo()
	Local oModel
	
	
	// Cria o objeto do Modelo de Dados
	oModel := MPFormModel():New('F050203', /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )
		
	oModel:AddFields("FIELD", /*cOwner*/, oStrField, /*bPreVld*/, /*bPosVld*/,{|oModel|CargaField(oModel)}/*bLoad*/)

	oModel:AddGrid('GRID','FIELD',oStruPA5, , , , ,{|oModel|CargaGrid(oModel)})
	oModel:GetModel('FIELD'):SetPrimaryKey({})
	oModel:GetModel( 'GRID' ):SetNoInsertLine()
	oModel:SetDescription("Mudan�a de Status de Indicadores")
	// Adiciona a descricao do Componente do Modelo de Dados
	oModel:GetModel( 'FIELD' ):SetDescription( 'Consulta de Indicadores' )
	
	oModel:SetPrimaryKey({'PA5_CODIGO'})
	oModel:SetVldActivate( { |oModel| ValidMod(oModel) } )
Return oModel

/*/{Protheus.doc} FieldModelo
Retorna a estrutura da Field da Tela para o Modelo
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function FieldModelo()

	Local oStructModelField	:= FWformModelStruct():New()
	
	oStructModelField:AddTable('PA5',,"Consulta de Indicadores")
	
	oStructModelField:AddField("Filial Solic"		,"Filial Solic"	,'PA5_XUNIDA'	,'C',TamSx3("PA5_XUNIDA")[1],	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Num Solic"			,"Num Solic"		,'PA5_XNRSOL'	,'C',TamSx3("PA5_XNRSOL")[1],	0, 	{|| .T.}, 	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)	
	oStructModelField:AddField("Desc Solic"		,"Desc Solic"		,'PA5_XDESOL'	,'C',TamSx3("PA5_XDESOL")[1],	0,	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)		 		
	oStructModelField:AddField("Fun��o"			,"Fun��o"			,'PA5_FUNCAO'	,'C',TamSx3("PA5_FUNCAO")[1],	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Matr. Solici"		,"Matr. Solici"	,'PA5_XMAT'	,'C',TamSx3("PA5_XMAT")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Nome Solicit"		,"Nome Solicit"	,'PA5_XNOME'	,'C',TamSx3("PA5_XNOME")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Departamento"		,"Departamento"	,'PA5_XSETOR'	,'C',TamSx3("PA5_XSETOR")[1],	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Cargo"				,"Cargo"			,'PA5_XCARGO'	,'C',TamSx3("PA5_XCARGO")[1],	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelField:AddField("Centro Custo"		,"Centro Custo"	,'PA5_XCC'		,'C',TamSx3("PA5_XCC")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
//	oStructModelField:AddField("Conta Or�ament"	,"Conta Or�ament"	,'PA5_XCONTA'	,'C',TamSx3("PA5_XCONTA")[1],	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	
Return oStructModelField


/*/{Protheus.doc} GridModelo
Retorna a estrutura da Grid da Tela para o MOdelo
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function GridModelo()
	Local oStructModelGrid	:= FWformModelStruct():New()

	oStructModelGrid:AddTable('PA5',,"Consulta de Indicadores")
		 	
	oStructModelGrid:AddField("Matr. Aprovado"	,"Matr. Aprovado"		,'PA5_XMATAP'	,'C',TamSx3("PA5_XMATAP")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Nome Aprovador"	,"Nome Aprovador"		,'PA5_XAPROV'	,'C',TamSx3("PA5_XAPROV")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Departamento"		,"Departamento"		,'PA5_XSEAPV'	,'C',TamSx3("PA5_XSEAPV")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Filial Aprovador"	,"Filial Aprovador"	,'PA5_XUNAPR'	,'C',TamSx3("PA5_XUNAPR")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Cargo Aprovador"	,"Cargo Aprovador"	,'PA5_XCARAP'	,'C',TamSx3("PA5_XCARAP")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Cod. Evento"		,"Cod. Evento"		,'PA5_XCODEV'	,'C',TamSx3("PA5_XCODEV")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Des. Evento"		,"Des. Evento"		,'PA5_XDESEV'	,'C',TamSx3("PA5_XDESEV")[1]	,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Status"				,"Status"				,'PA5_XSTAT'	,'C',TamSx3("PA5_XSTAT")[1]		,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Valor Solicitado"	,"Valor Solicitado"	,'PA5_XEMPEN'	,'N',TamSx3("PA5_XEMPEN")[1]	,	2, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Data"				,"Data"				,'PA5_XDATA'	,'D',TamSx3("PA5_XDATA")[1]		,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)
	oStructModelGrid:AddField("Hora"				,"Hora"				,'PA5_XHORA'	,'C',TamSx3("PA5_XHORA")[1]		,	0, 	{|| .T.},	{|| .F.},	NIL,	.F.,NIL,	NIL,	NIL,.T.)	

Return oStructModelGrid


/*/{Protheus.doc} ViewDef
Retorna a View da rotina
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function ViewDef()	
	Local oModel   			
	Local oView 		:= FWFormView():New()	
	Local oStruCab	:= FieldView()
	Local oStrGrid	:= GridView()
	
	oModel   	:= FWLoadModel( 'F0500202' )
	oView:SetModel( oModel )
		
	oView:AddField( "TELA" 	, oStruCab,'FIELD') 
	oView:AddGrid("ITEM"		, oStrGrid,'GRID')
		
    
	
	oView:CreateHorizontalBox("BOXTELA", 40)
	oView:CreateHorizontalBox("BOXPA5"	, 60)
	
	// Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView("TELA","BOXTELA")
	oView:SetOwnerView("ITEM","BOXPA5")
		
	
	//oView:EnableTitleView( "ITEM", "Consulta de Indicadores" )
	
	oView:EnableControlBar( .F. )
	//oView:setUseCursor(.F.)
	//oView:SetCloseOnOk({||.T.})
	oView:SetViewCanActivate({|oView| .t. })
Return oView


/*/{Protheus.doc} FieldView
Retorna a estrutura da Field da Tela para a View
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function FieldView()
	Local oStructViewField	:= FWformViewStruct():New()
	
	oStructViewField:AddField( 'PA5_XUNIDA'	,'001','Filial Solic'	,'Filial Solic'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XNRSOL'	,'002','Num Solic'		,'Num Solic'		,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XDESOL'	,'003','Desc Solic'		,'Desc Solic'		,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_FUNCAO'	,'004','Fun��o'			,'Fun��o'			,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XMAT'		,'005','Matr. Solici'	,'Matr. Solici'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XNOME'		,'006','Nome Solicit'	,'Nome Solicit'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)	
	oStructViewField:AddField( 'PA5_XSETOR'	,'007','Departamento'	,'Departamento'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XCARGO'	,'008','Cargo'			,'Cargo'			,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	oStructViewField:AddField( 'PA5_XCC'		,'009','Centro Custo'	,'Centro Custo'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
//	oStructViewField:AddField( 'PA5_XCONTA'	,'010','Conta Or�ament'	,'Conta Or�ament'	,NIL,'GET',	'@!',	NIL,	'',	.T.,NIL, NIL,	NIL,0,'',.T.)
	
Return oStructViewField

/*/{Protheus.doc} GridView
Retorna a estrutura da Grid da Tela para a View
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function GridView()
	Local oStructViewGrid	:= FWformViewStruct():New()
	Local cP					:= '@E 99,999,999.99'	
	//Descri��o da Solicita��o
	oStructViewGrid:AddField('PA5_XSTAT' ,'001','Status'				,'Status'				,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XMATAP','002','Matr. Aprovado'	,'Matr. Aprovado'		,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XAPROV','003','Nome Aprovador'	,'Nome Aprovador'		,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XSEAPV','004','Departamento'		,'Departamento'		,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XUNAPR','005','Filial Aprovador'	,'Filial Aprovador'	,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XCARAP','006','Cargo Aprovador'	,'Cargo Aprovador'	,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XCODEV','007','Cod. Evento'		,'Cod. Evento'		,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0	,	'',.T.)
	oStructViewGrid:AddField('PA5_XDESEV','008','Des. Evento'		,'Des. Evento'		,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',.T.)	
	oStructViewGrid:AddField('PA5_XEMPEN','009','Valor Solicitado'	,'Valor Solicitado'	,NIL,'GET', cP ,NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XDATA' ,'010','Data'				,'Data'				,NIL,'GET','@D',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
	oStructViewGrid:AddField('PA5_XHORA' ,'011','Hora'				,'Hora'				,NIL,'GET','@!',NIL,'',	.T.,NIL,NIL,NIL,0,	'',	.T.)
		
Return oStructViewGrid	



/*/{Protheus.doc} ExecQuery
Cria e abre o Alias Temporario para que seja utilizado nos blocos de Load da View e do Model
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function ExecQuery()	
	Local cQuery		:= ""
	
	cQuery += " SELECT 				"	+ CRLF
	cQuery += " PA5.R_E_C_N_O_ RECNO"	+ CRLF
	cQuery += " ,PA5.PA5_XNRSOL "	+ CRLF
	cQuery += " ,PA5.PA5_XDESOL "	+ CRLF
	cQuery += " ,PA5.PA5_XUNIDA"	+ CRLF
	cQuery += " ,PA5.PA5_XCODEV"	+ CRLF
	cQuery += " ,PA5.PA5_XDESEV"	+ CRLF
	cQuery += " ,PA5.PA5_FUNCAO"	+ CRLF
	cQuery += " ,PA5.PA5_XMAT	"	+ CRLF
	cQuery += " ,PA5.PA5_XNOME	"	+ CRLF
	cQuery += " ,PA5.PA5_XSETOR	"	+ CRLF
	cQuery += " ,PA5.PA5_XCARGO	"	+ CRLF
	cQuery += " ,PA5.PA5_XMATAP	"	+ CRLF
	cQuery += " ,PA5.PA5_XAPROV	"	+ CRLF
	cQuery += " ,PA5.PA5_XSEAPV	"	+ CRLF
	cQuery += " ,PA5.PA5_XUNAPR	"	+ CRLF
	cQuery += " ,PA5.PA5_XCARAP	"	+ CRLF
	cQuery += " ,PA5.PA5_XCC	"		+ CRLF
	cQuery += " ,PA5.PA5_XCONTA	"	+ CRLF
	cQuery += " ,PA5.PA5_XSTAT	"	+ CRLF
	cQuery += " ,PA5.PA5_XEMPEN"	+ CRLF
	cQuery += " ,PA5.PA5_XDATA	"	+ CRLF
	cQuery += " ,PA5.PA5_XHORA	"	+ CRLF

	cQuery += " FROM "+RetSqlName("PA5")+" PA5"	+ CRLF
	cQuery += " INNER JOIN "+RetSqlName("RH3")+" RH3"	+ CRLF
	cQuery += " ON RH3.RH3_CODIGO = PA5.PA5_XNRSOL"	+ CRLF
	cQuery += " AND RH3.RH3_FILIAL = PA5.PA5_FILIAL"	+ CRLF
	cQuery += " AND RH3.D_E_L_E_T_ = ''"	+ CRLF
	cQuery += " WHERE"	+ CRLF
	cQuery += " PA5.PA5_FILIAL = '"+xFilial("PA5")+"'"	+ CRLF
	cQuery += " AND PA5.PA5_XNRSOL	= '"+RH3->RH3_CODIGO+"'"	+ CRLF
	cQuery += " AND PA5.D_E_L_E_T_ =''"+ CRLF
	 
	If Select(cAliasPA5)<>0
		dbSelectArea(cAliasPA5)
		dbCloseArea()
	EndIf 
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasPA5,.T.,.T.)
Return 

/*/{Protheus.doc} CargaField
Carrega e retorna os dados da Field
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function CargaField(oModel)
	Local aResult		:= {}
	ExecQuery()//Abre o arquivo tempor�rio
	dbSelectArea(cAliasPA5)
	(cAliasPA5)->(dbGoTop())
	
	aAdd(aResult,{})
	aadd(aResult,1)
	
	If (cAliasPA5)->(! EOF())		
		aResult[1] := {(cAliasPA5)->(PA5_XUNIDA),(cAliasPA5)->(PA5_XNRSOL)	,(cAliasPA5)->(PA5_XDESOL)	;
						,(cAliasPA5)->(PA5_FUNCAO),(cAliasPA5)->(PA5_XMAT)  	,(cAliasPA5)->(PA5_XNOME)	;
						,(cAliasPA5)->(PA5_XSETOR),(cAliasPA5)->(PA5_XCARGO)	,(cAliasPA5)->(PA5_XCC)  	}
	Else
		aResult[1] := { "","","","","","","","","","" }	 
	EndIf
		
Return aResult


/*/{Protheus.doc} CargaGrid
Carrega e retorna os dados do Modelo
@author Fernando Carvalho
@since 11/11/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function CargaGrid(oModel)
	Local aResult		:= {}
	Local nLoop		:= 0
	
	dbSelectArea(cAliasPA5)
	(cAliasPA5)->(dbGoTop())	
	aResult := FwLoadByAlias(oModel,cAliasPA5)	
	For nLoop := 1 To Len(aResult)
		aResult[nLoop,2,10] := StoD(aResult[nLoop,2,10])
	Next	
	
Return aResult

//-----------------------------	|
//Fun��o para encerrar a Grid	|
//-----------------------------	|
Static Function Sair()	
	Local oView := FwViewActive()				
	(cAliasPA5)->(dbCloseArea())	      
	oView:ButtonCancelAction()	//Necess�rio para encerrar a View 	
Return

/*/{Protheus.doc} Monitoramento
Fun��o chamada no MenuDef
@author Fernando Carvalho
@since 14/12/2016
@version 12.7
@project MAN0000007423039_EF_002
/*/
Static Function ValidMod()	
	Local lRet 	:= .F.
	ExecQuery()//Abre o arquivo tempor�rio
	dbSelectArea(cAliasPA5)
	(cAliasPA5)->(dbGoTop())
	
	If (cAliasPA5)->(! EOF())
		lRet 	:= .T.
	Else
		//Alert("N�o h� indicadores para ser exibido.")
		Help("",1, "Help", "Sem registro na PA5", "N�o h� indicadores para ser exibido." , 3, 0)	
	EndIf	
Return lRet 

