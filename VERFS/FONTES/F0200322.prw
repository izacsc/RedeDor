#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200322()

@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return	 cHtml
*/
User Function F0200322()
	
	Local cHtml   	:= ""
	Local oParam  	:= Nil
	Local cHierarquia 	:= ""
	Local nPos        	:= 0
	Local aAux        	:= {}
	Local nAux        	:= 0
	Local nNivel      	:= 0
	Local oOrg
	Private oLista1
	Private oLista2
	Private oLista4
	Private aAuxPag        := {}
	WEB EXTENDED INIT cHtml START "InSite"
	
	Default HttpGet->Page         		:= "1"
	Default HttpGet->FilterField     	:= ""
	Default HttpGet->FilterValue	    := ""
	Default HttpGet->EmployeeFilial   	:= ""
	Default HttpGet->Registration     	:= ""
	Default HttpGet->EmployeeEmp     	:= ""
	Default HttpGet->cKeyVision		    := ""
	
	cMat      := HTTPSession->RHMat
	codsubfun := HttpGet->codsubfun
	depsubfun := HttpGet->depsubfun
	nmsuper   := HttpGet->nmsuper
	aAuxPag   := aClone(HttpSession->aStructure)
	oParam := WSW0200301():new()
	WsChgURL(@oParam,"W0200301.APW")

	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW",,,HttpGet->EmployeeEmp)
	fGetInfRotina("U_F0200306.APW")
	GetMat()	
	If Empty(HttpGet->EmployeeFilial) .And. Empty(codsubfun)
		oOrg:cParticipantID 	    := HttpSession->cParticipantID
		
		If ValType(HttpSession->RHMat) != "U" .And. !Empty(HttpSession->RHMat)
			oOrg:cRegistration	 := HttpSession->RHMat
		EndIf
	Else
		oOrg:cEmployeeFil  	    := HttpGet->EmployeeFilial
		oOrg:cRegistration 	    := codsubfun
	EndIf
	
	oOrg:cKeyVision				:=  Alltrim(HttpGet->cKeyVision)
	
	oOrg:cVision     		    := HttpSession->aInfRotina:cVisao
	oOrg:cFilterValue 		 := HttpGet->FilterValue
	oOrg:cFilterField   		:= HttpGet->FilterField
	oOrg:cRequestType 		    := ""
	
	IF oOrg:GetStructure()
		HttpSession->aStructure  := aClone(oOrg:oWSGetStructureResult:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE)
		nPageTotal 		       := oOrg:oWSGetStructureResult:nPagesTotal
		
		// *****************************************************************
		// Inicio - Monta Hierarquia
		// *****************************************************************
		cHierarquia := '<ul style="list-style-type: none;"><li><a href="#" class="links" onclick="javascript:GoToPage(null,1,null,null,null,null,' +;
			"'" + HttpSession->aStructure[1]:cEmployeeFilial + "'," +;
			"'" + HttpSession->aStructure[1]:cRegistration + "'," +;
			"'" + HttpSession->aStructure[1]:cEmployeeEmp + "'," +;
			"'" + oOrg:cKeyVision + "'" + ')">'
		
		If Empty(HttpSession->cHierarquia) .or. (HttpSession->cParticipantID == HttpSession->aStructure[1]:cParticipantID)
			nNivel                 := 1
			HttpSession->cHierarquia := ""
		Else
			aAux := Str2Arr(HttpSession->cHierarquia, "</ul>")
			If (nPos := aScan(aAux, {|x| cHierarquia $ x })) > 0
				For nAux := len(aAux) to nPos step -1
					aDel(aAux,nAux)
					aSize(aAux,Len(aAux)-1)
				Next nAux
			EndIf
			HttpSession->cHierarquia := ""
			For nPos := 1 to Len(aAux)
				HttpSession->cHierarquia += aAux[nPos] + "</ul>"
			Next nPos
			
			nNivel := Iif(Len(aAux) > 0,Len(aAux)+1,1)
		EndIf
		
		For nPos := 1 to nNivel
			cHierarquia += '&nbsp;&nbsp;&nbsp;'
		Next nPos
		cHierarquia += Alltrim(str(nNivel)) + " . " + HttpSession->aStructure[1]:cName + '</a></li></ul>'
		
		HttpSession->cHierarquia += cHierarquia
		// Fim - Monta Hierarquia
	Else
		HttpSession->aStructure := {}
		nPageTotal 		      := 1
		cTela	:= "F0200312"
	EndIf
	
	
	If oParam:SoliTrm(cMat)
		oLista2 :=  oParam:oWSSoliTrmRESULT
	EndIf
	
	If oParam:VisSuper(codsubfun, cMat, '1')
		oLista4 :=  oParam:oWSVisSuperRESULT
	EndIf
	
	cHtml := ExecInPage("F0200311")
	WEB EXTENDED END
	
Return cHtml

