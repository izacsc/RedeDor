#Include 'Protheus.ch'

/*
{Protheus.doc} F0702001()
Inclus�o/Altera��o no Cadastro de Clientes
@Author     Bruno de Oliveira
@Since		20/01/2017
@Version    P12.1.7
@Project    MAN0000007423041_EF_020
@Param		oClients, objeto, dados do webservice
*/
User Function F0702001(oClients)

	Local aCampos := {}
	Local cRet := ""
	
    aCampos :=	{;
               		{"A1_FILIAL" , oClients:cFILREG     },;
                	{"A1_COD"    , oClients:cCOD        },;
                	{"A1_LOJA"   , oClients:cLOJA       },;
                	{"A1_NOME"   , oClients:cNOME       },;
                	{"A1_PESSOA" , oClients:cPESSOA     },;
                	{"A1_END"    , oClients:cEND        },;
                	{"A1_NREDUZ" , oClients:cNREDUZ     },;
                	{"A1_COMPLEM", oClients:cCOMPLEM    },;
                	{"A1_BAIRRO" , oClients:cBAIRRO     },;
                	{"A1_TIPO"   , oClients:cTIPO       },;
                	{"A1_EST"    , oClients:cEST        },;
                	{"A1_CEP"    , oClients:cCEP        },;
                	{"A1_COD_MUN", oClients:cCOD_MUN    },;
                	{"A1_CGC"    , oClients:cCGC        },;
                	{"A1_PFISICA", oClients:cPFISICA    },;
                	{"A1_PAIS"   , oClients:cPAIS       },;
                	{"A1_CODPAIS", oClients:cCODPAIS    },;
                	{"A1_EMAIL"  , oClients:cEMAIL      },;
                	{"A1_XOPCM"  , oClients:cXOPCM      }; 
                }	
                
	cChave := U_RetChave("SA1", aCampos,{1,2,3})
    cRet   := U_UpsMVCRg("SA1", cChave, "MATA030", "MATA030_SA1", aCampos) //-- chamar fun��o de log        
	
Return cRet