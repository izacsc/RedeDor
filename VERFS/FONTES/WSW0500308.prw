#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://localhost:81/ws/W0500308.apw?WSDL
Gerado em        02/08/17 17:29:05
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _SSBAYTM ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSW0500308
------------------------------------------------------------------------------- */

WSCLIENT WSW0500308

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD APRSOLQDRO
	WSMETHOD APRSOLVAGA
	WSMETHOD BSCSOLQDRO
	WSMETHOD BSCSOLVAGS
	WSMETHOD GETDEPTO
	WSMETHOD INFFXSL
	WSMETHOD INFSOLVG
	WSMETHOD INSSOLQDRO
	WSMETHOD INSTSOLICT
	WSMETHOD MINHSOLICT
	WSMETHOD PEGMNEM
	WSMETHOD REPSOLQDRO
	WSMETHOD REPSOLVAGA
	WSMETHOD SOLICAPRP
	WSMETHOD VERAPRVSIT
	WSMETHOD VERPOSTSOL
	WSMETHOD VGDESCDET

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   oWSAUMENTQDRO             AS W0500308_SOLICITQDR
	WSDATA   cAPRSOLQDRORESULT         AS string
	WSDATA   oWSSOLICITACAO            AS W0500308_SOLICITVAG
	WSDATA   cAPRSOLVAGARESULT         AS string
	WSDATA   cCODSOLIC                 AS string
	WSDATA   cMATRICUL                 AS string
	WSDATA   cVISAO                    AS string
	WSDATA   cFILIALS                  AS string
	WSDATA   oWSBSCSOLQDRORESULT       AS W0500308_QDRDADOS
	WSDATA   oWSBSCSOLVAGSRESULT       AS W0500308_VAGDADOS
	WSDATA   cFILIALAP                 AS string
	WSDATA   cMATRICAP                 AS string
	WSDATA   cGETDEPTORESULT           AS string
	WSDATA   cCODVAGA                  AS string
	WSDATA   cCFILP                    AS string
	WSDATA   oWSINFFXSLRESULT          AS W0500308__FAIXSAL
	WSDATA   oWSINFSOLVGRESULT         AS W0500308_INFRH3
	WSDATA   oWSINSSOLQDRORESULT       AS W0500308_ARETSOLICT
	WSDATA   oWSINSTSOLICTRESULT       AS W0500308_ARETSOLICT
	WSDATA   cTPSOLIC                  AS string
	WSDATA   oWSMINHSOLICTRESULT       AS W0500308_AMNHVGSL
	WSDATA   lLRET                     AS boolean
	WSDATA   cPEGMNEMRESULT            AS string
	WSDATA   cREPSOLQDRORESULT         AS string
	WSDATA   cREPSOLVAGARESULT         AS string
	WSDATA   oWSSOLICAPRPRESULT        AS W0500308_AAPREPVAG
	WSDATA   cCDEPTO                   AS string
	WSDATA   cEMPFUN                   AS string
	WSDATA   cTPSIT                    AS string
	WSDATA   oWSVERAPRVSITRESULT       AS W0500308_APROVS
	WSDATA   cCPOSTO                   AS string
	WSDATA   nNOCUP                    AS integer
	WSDATA   nNQNTD                    AS integer
	WSDATA   lVERPOSTSOLRESULT         AS boolean
	WSDATA   oWS_IDVAGA                AS W0500308_IDVAGA
	WSDATA   cVGDESCDETRESULT          AS string

	// Estruturas mantidas por compatibilidade - N�O USAR
	WSDATA   oWSSOLICITQDR             AS W0500308_SOLICITQDR
	WSDATA   oWSSOLICITVAG             AS W0500308_SOLICITVAG
	WSDATA   oWSIDVAGA                 AS W0500308_IDVAGA

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSW0500308
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20160114 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSW0500308
	::oWSAUMENTQDRO      := W0500308_SOLICITQDR():New()
	::oWSSOLICITACAO     := W0500308_SOLICITVAG():New()
	::oWSBSCSOLQDRORESULT := W0500308_QDRDADOS():New()
	::oWSBSCSOLVAGSRESULT := W0500308_VAGDADOS():New()
	::oWSINFFXSLRESULT   := W0500308__FAIXSAL():New()
	::oWSINFSOLVGRESULT  := W0500308_INFRH3():New()
	::oWSINSSOLQDRORESULT := W0500308_ARETSOLICT():New()
	::oWSINSTSOLICTRESULT := W0500308_ARETSOLICT():New()
	::oWSMINHSOLICTRESULT := W0500308_AMNHVGSL():New()
	::oWSSOLICAPRPRESULT := W0500308_AAPREPVAG():New()
	::oWSVERAPRVSITRESULT := W0500308_APROVS():New()
	::oWS_IDVAGA         := W0500308_IDVAGA():New()

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSSOLICITQDR      := ::oWSAUMENTQDRO
	::oWSSOLICITVAG      := ::oWSSOLICITACAO
	::oWSIDVAGA          := ::oWS_IDVAGA
Return

WSMETHOD RESET WSCLIENT WSW0500308
	::oWSAUMENTQDRO      := NIL 
	::cAPRSOLQDRORESULT  := NIL 
	::oWSSOLICITACAO     := NIL 
	::cAPRSOLVAGARESULT  := NIL 
	::cCODSOLIC          := NIL 
	::cMATRICUL          := NIL 
	::cVISAO             := NIL 
	::cFILIALS           := NIL 
	::oWSBSCSOLQDRORESULT := NIL 
	::oWSBSCSOLVAGSRESULT := NIL 
	::cFILIALAP          := NIL 
	::cMATRICAP          := NIL 
	::cGETDEPTORESULT    := NIL 
	::cCODVAGA           := NIL 
	::cCFILP             := NIL 
	::oWSINFFXSLRESULT   := NIL 
	::oWSINFSOLVGRESULT  := NIL 
	::oWSINSSOLQDRORESULT := NIL 
	::oWSINSTSOLICTRESULT := NIL 
	::cTPSOLIC           := NIL 
	::oWSMINHSOLICTRESULT := NIL 
	::lLRET              := NIL 
	::cPEGMNEMRESULT     := NIL 
	::cREPSOLQDRORESULT  := NIL 
	::cREPSOLVAGARESULT  := NIL 
	::oWSSOLICAPRPRESULT := NIL 
	::cCDEPTO            := NIL 
	::cEMPFUN            := NIL 
	::cTPSIT             := NIL 
	::oWSVERAPRVSITRESULT := NIL 
	::cCPOSTO            := NIL 
	::nNOCUP             := NIL 
	::nNQNTD             := NIL 
	::lVERPOSTSOLRESULT  := NIL 
	::oWS_IDVAGA         := NIL 
	::cVGDESCDETRESULT   := NIL 

	// Estruturas mantidas por compatibilidade - N�O USAR
	::oWSSOLICITQDR      := NIL
	::oWSSOLICITVAG      := NIL
	::oWSIDVAGA          := NIL
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSW0500308
Local oClone := WSW0500308():New()
	oClone:_URL          := ::_URL 
	oClone:oWSAUMENTQDRO :=  IIF(::oWSAUMENTQDRO = NIL , NIL ,::oWSAUMENTQDRO:Clone() )
	oClone:cAPRSOLQDRORESULT := ::cAPRSOLQDRORESULT
	oClone:oWSSOLICITACAO :=  IIF(::oWSSOLICITACAO = NIL , NIL ,::oWSSOLICITACAO:Clone() )
	oClone:cAPRSOLVAGARESULT := ::cAPRSOLVAGARESULT
	oClone:cCODSOLIC     := ::cCODSOLIC
	oClone:cMATRICUL     := ::cMATRICUL
	oClone:cVISAO        := ::cVISAO
	oClone:cFILIALS      := ::cFILIALS
	oClone:oWSBSCSOLQDRORESULT :=  IIF(::oWSBSCSOLQDRORESULT = NIL , NIL ,::oWSBSCSOLQDRORESULT:Clone() )
	oClone:oWSBSCSOLVAGSRESULT :=  IIF(::oWSBSCSOLVAGSRESULT = NIL , NIL ,::oWSBSCSOLVAGSRESULT:Clone() )
	oClone:cFILIALAP     := ::cFILIALAP
	oClone:cMATRICAP     := ::cMATRICAP
	oClone:cGETDEPTORESULT := ::cGETDEPTORESULT
	oClone:cCODVAGA      := ::cCODVAGA
	oClone:cCFILP        := ::cCFILP
	oClone:oWSINFFXSLRESULT :=  IIF(::oWSINFFXSLRESULT = NIL , NIL ,::oWSINFFXSLRESULT:Clone() )
	oClone:oWSINFSOLVGRESULT :=  IIF(::oWSINFSOLVGRESULT = NIL , NIL ,::oWSINFSOLVGRESULT:Clone() )
	oClone:oWSINSSOLQDRORESULT :=  IIF(::oWSINSSOLQDRORESULT = NIL , NIL ,::oWSINSSOLQDRORESULT:Clone() )
	oClone:oWSINSTSOLICTRESULT :=  IIF(::oWSINSTSOLICTRESULT = NIL , NIL ,::oWSINSTSOLICTRESULT:Clone() )
	oClone:cTPSOLIC      := ::cTPSOLIC
	oClone:oWSMINHSOLICTRESULT :=  IIF(::oWSMINHSOLICTRESULT = NIL , NIL ,::oWSMINHSOLICTRESULT:Clone() )
	oClone:lLRET         := ::lLRET
	oClone:cPEGMNEMRESULT := ::cPEGMNEMRESULT
	oClone:cREPSOLQDRORESULT := ::cREPSOLQDRORESULT
	oClone:cREPSOLVAGARESULT := ::cREPSOLVAGARESULT
	oClone:oWSSOLICAPRPRESULT :=  IIF(::oWSSOLICAPRPRESULT = NIL , NIL ,::oWSSOLICAPRPRESULT:Clone() )
	oClone:cCDEPTO       := ::cCDEPTO
	oClone:cEMPFUN       := ::cEMPFUN
	oClone:cTPSIT        := ::cTPSIT
	oClone:oWSVERAPRVSITRESULT :=  IIF(::oWSVERAPRVSITRESULT = NIL , NIL ,::oWSVERAPRVSITRESULT:Clone() )
	oClone:cCPOSTO       := ::cCPOSTO
	oClone:nNOCUP        := ::nNOCUP
	oClone:nNQNTD        := ::nNQNTD
	oClone:lVERPOSTSOLRESULT := ::lVERPOSTSOLRESULT
	oClone:oWS_IDVAGA    :=  IIF(::oWS_IDVAGA = NIL , NIL ,::oWS_IDVAGA:Clone() )
	oClone:cVGDESCDETRESULT := ::cVGDESCDETRESULT

	// Estruturas mantidas por compatibilidade - N�O USAR
	oClone:oWSSOLICITQDR := oClone:oWSAUMENTQDRO
	oClone:oWSSOLICITVAG := oClone:oWSSOLICITACAO
	oClone:oWSIDVAGA     := oClone:oWS_IDVAGA
Return oClone

// WSDL Method APRSOLQDRO of Service WSW0500308

WSMETHOD APRSOLQDRO WSSEND oWSAUMENTQDRO WSRECEIVE cAPRSOLQDRORESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<APRSOLQDRO xmlns="http://localhost:81/">'
cSoap += WSSoapValue("AUMENTQDRO", ::oWSAUMENTQDRO, oWSAUMENTQDRO , "SOLICITQDR", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</APRSOLQDRO>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/APRSOLQDRO",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cAPRSOLQDRORESULT  :=  WSAdvValue( oXmlRet,"_APRSOLQDRORESPONSE:_APRSOLQDRORESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method APRSOLVAGA of Service WSW0500308

WSMETHOD APRSOLVAGA WSSEND oWSSOLICITACAO WSRECEIVE cAPRSOLVAGARESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<APRSOLVAGA xmlns="http://localhost:81/">'
cSoap += WSSoapValue("SOLICITACAO", ::oWSSOLICITACAO, oWSSOLICITACAO , "SOLICITVAG", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</APRSOLVAGA>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/APRSOLVAGA",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cAPRSOLVAGARESULT  :=  WSAdvValue( oXmlRet,"_APRSOLVAGARESPONSE:_APRSOLVAGARESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method BSCSOLQDRO of Service WSW0500308

WSMETHOD BSCSOLQDRO WSSEND cCODSOLIC,cMATRICUL,cVISAO,cFILIALS WSRECEIVE oWSBSCSOLQDRORESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<BSCSOLQDRO xmlns="http://localhost:81/">'
cSoap += WSSoapValue("CODSOLIC", ::cCODSOLIC, cCODSOLIC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("MATRICUL", ::cMATRICUL, cMATRICUL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("VISAO", ::cVISAO, cVISAO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("FILIALS", ::cFILIALS, cFILIALS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</BSCSOLQDRO>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/BSCSOLQDRO",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSBSCSOLQDRORESULT:SoapRecv( WSAdvValue( oXmlRet,"_BSCSOLQDRORESPONSE:_BSCSOLQDRORESULT","QDRDADOS",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method BSCSOLVAGS of Service WSW0500308

WSMETHOD BSCSOLVAGS WSSEND cCODSOLIC,cMATRICUL,cVISAO,cFILIALS WSRECEIVE oWSBSCSOLVAGSRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<BSCSOLVAGS xmlns="http://localhost:81/">'
cSoap += WSSoapValue("CODSOLIC", ::cCODSOLIC, cCODSOLIC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("MATRICUL", ::cMATRICUL, cMATRICUL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("VISAO", ::cVISAO, cVISAO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("FILIALS", ::cFILIALS, cFILIALS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</BSCSOLVAGS>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/BSCSOLVAGS",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSBSCSOLVAGSRESULT:SoapRecv( WSAdvValue( oXmlRet,"_BSCSOLVAGSRESPONSE:_BSCSOLVAGSRESULT","VAGDADOS",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method GETDEPTO of Service WSW0500308

WSMETHOD GETDEPTO WSSEND cFILIALAP,cMATRICAP WSRECEIVE cGETDEPTORESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<GETDEPTO xmlns="http://localhost:81/">'
cSoap += WSSoapValue("FILIALAP", ::cFILIALAP, cFILIALAP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("MATRICAP", ::cMATRICAP, cMATRICAP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</GETDEPTO>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/GETDEPTO",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cGETDEPTORESULT    :=  WSAdvValue( oXmlRet,"_GETDEPTORESPONSE:_GETDEPTORESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFFXSL of Service WSW0500308

WSMETHOD INFFXSL WSSEND cCODVAGA,cCFILP WSRECEIVE oWSINFFXSLRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFFXSL xmlns="http://localhost:81/">'
cSoap += WSSoapValue("CODVAGA", ::cCODVAGA, cCODVAGA , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CFILP", ::cCFILP, cCFILP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</INFFXSL>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/INFFXSL",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSINFFXSLRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFFXSLRESPONSE:_INFFXSLRESULT","_FAIXSAL",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INFSOLVG of Service WSW0500308

WSMETHOD INFSOLVG WSSEND cFILIALS,cCODSOLIC WSRECEIVE oWSINFSOLVGRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INFSOLVG xmlns="http://localhost:81/">'
cSoap += WSSoapValue("FILIALS", ::cFILIALS, cFILIALS , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CODSOLIC", ::cCODSOLIC, cCODSOLIC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</INFSOLVG>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/INFSOLVG",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSINFSOLVGRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INFSOLVGRESPONSE:_INFSOLVGRESULT","INFRH3",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INSSOLQDRO of Service WSW0500308

WSMETHOD INSSOLQDRO WSSEND oWSAUMENTQDRO WSRECEIVE oWSINSSOLQDRORESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INSSOLQDRO xmlns="http://localhost:81/">'
cSoap += WSSoapValue("AUMENTQDRO", ::oWSAUMENTQDRO, oWSAUMENTQDRO , "SOLICITQDR", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</INSSOLQDRO>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/INSSOLQDRO",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSINSSOLQDRORESULT:SoapRecv( WSAdvValue( oXmlRet,"_INSSOLQDRORESPONSE:_INSSOLQDRORESULT","ARETSOLICT",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method INSTSOLICT of Service WSW0500308

WSMETHOD INSTSOLICT WSSEND oWSSOLICITACAO WSRECEIVE oWSINSTSOLICTRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<INSTSOLICT xmlns="http://localhost:81/">'
cSoap += WSSoapValue("SOLICITACAO", ::oWSSOLICITACAO, oWSSOLICITACAO , "SOLICITVAG", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</INSTSOLICT>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/INSTSOLICT",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSINSTSOLICTRESULT:SoapRecv( WSAdvValue( oXmlRet,"_INSTSOLICTRESPONSE:_INSTSOLICTRESULT","ARETSOLICT",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method MINHSOLICT of Service WSW0500308

WSMETHOD MINHSOLICT WSSEND cMATRICUL,cTPSOLIC WSRECEIVE oWSMINHSOLICTRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<MINHSOLICT xmlns="http://localhost:81/">'
cSoap += WSSoapValue("MATRICUL", ::cMATRICUL, cMATRICUL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("TPSOLIC", ::cTPSOLIC, cTPSOLIC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</MINHSOLICT>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/MINHSOLICT",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSMINHSOLICTRESULT:SoapRecv( WSAdvValue( oXmlRet,"_MINHSOLICTRESPONSE:_MINHSOLICTRESULT","AMNHVGSL",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method PEGMNEM of Service WSW0500308

WSMETHOD PEGMNEM WSSEND lLRET WSRECEIVE cPEGMNEMRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<PEGMNEM xmlns="http://localhost:81/">'
cSoap += WSSoapValue("LRET", ::lLRET, lLRET , "boolean", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</PEGMNEM>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/PEGMNEM",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cPEGMNEMRESULT     :=  WSAdvValue( oXmlRet,"_PEGMNEMRESPONSE:_PEGMNEMRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method REPSOLQDRO of Service WSW0500308

WSMETHOD REPSOLQDRO WSSEND oWSAUMENTQDRO WSRECEIVE cREPSOLQDRORESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<REPSOLQDRO xmlns="http://localhost:81/">'
cSoap += WSSoapValue("AUMENTQDRO", ::oWSAUMENTQDRO, oWSAUMENTQDRO , "SOLICITQDR", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</REPSOLQDRO>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/REPSOLQDRO",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cREPSOLQDRORESULT  :=  WSAdvValue( oXmlRet,"_REPSOLQDRORESPONSE:_REPSOLQDRORESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method REPSOLVAGA of Service WSW0500308

WSMETHOD REPSOLVAGA WSSEND oWSSOLICITACAO WSRECEIVE cREPSOLVAGARESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<REPSOLVAGA xmlns="http://localhost:81/">'
cSoap += WSSoapValue("SOLICITACAO", ::oWSSOLICITACAO, oWSSOLICITACAO , "SOLICITVAG", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</REPSOLVAGA>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/REPSOLVAGA",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cREPSOLVAGARESULT  :=  WSAdvValue( oXmlRet,"_REPSOLVAGARESPONSE:_REPSOLVAGARESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method SOLICAPRP of Service WSW0500308

WSMETHOD SOLICAPRP WSSEND cMATRICUL,cFILIALAP,cVISAO,cTPSOLIC WSRECEIVE oWSSOLICAPRPRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<SOLICAPRP xmlns="http://localhost:81/">'
cSoap += WSSoapValue("MATRICUL", ::cMATRICUL, cMATRICUL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("FILIALAP", ::cFILIALAP, cFILIALAP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("VISAO", ::cVISAO, cVISAO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("TPSOLIC", ::cTPSOLIC, cTPSOLIC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</SOLICAPRP>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/SOLICAPRP",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSSOLICAPRPRESULT:SoapRecv( WSAdvValue( oXmlRet,"_SOLICAPRPRESPONSE:_SOLICAPRPRESULT","AAPREPVAG",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method VERAPRVSIT of Service WSW0500308

WSMETHOD VERAPRVSIT WSSEND cFILIALAP,cMATRICAP,cCDEPTO,cVISAO,cEMPFUN,cTPSIT WSRECEIVE oWSVERAPRVSITRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<VERAPRVSIT xmlns="http://localhost:81/">'
cSoap += WSSoapValue("FILIALAP", ::cFILIALAP, cFILIALAP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("MATRICAP", ::cMATRICAP, cMATRICAP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CDEPTO", ::cCDEPTO, cCDEPTO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("VISAO", ::cVISAO, cVISAO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("EMPFUN", ::cEMPFUN, cEMPFUN , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("TPSIT", ::cTPSIT, cTPSIT , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</VERAPRVSIT>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/VERAPRVSIT",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::oWSVERAPRVSITRESULT:SoapRecv( WSAdvValue( oXmlRet,"_VERAPRVSITRESPONSE:_VERAPRVSITRESULT","APROVS",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method VERPOSTSOL of Service WSW0500308

WSMETHOD VERPOSTSOL WSSEND cCPOSTO,cCDEPTO,nNOCUP,nNQNTD,cCFILP WSRECEIVE lVERPOSTSOLRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<VERPOSTSOL xmlns="http://localhost:81/">'
cSoap += WSSoapValue("CPOSTO", ::cCPOSTO, cCPOSTO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CDEPTO", ::cCDEPTO, cCDEPTO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("NOCUP", ::nNOCUP, nNOCUP , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("NQNTD", ::nNQNTD, nNQNTD , "integer", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CFILP", ::cCFILP, cCFILP , "string", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</VERPOSTSOL>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/VERPOSTSOL",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::lVERPOSTSOLRESULT  :=  WSAdvValue( oXmlRet,"_VERPOSTSOLRESPONSE:_VERPOSTSOLRESULT:TEXT","boolean",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method VGDESCDET of Service WSW0500308

WSMETHOD VGDESCDET WSSEND oWS_IDVAGA WSRECEIVE cVGDESCDETRESULT WSCLIENT WSW0500308
Local cSoap := "" , oXmlRet

BEGIN WSMETHOD

cSoap += '<VGDESCDET xmlns="http://localhost:81/">'
cSoap += WSSoapValue("_IDVAGA", ::oWS_IDVAGA, oWS_IDVAGA , "IDVAGA", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</VGDESCDET>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://localhost:81/VGDESCDET",; 
	"DOCUMENT","http://localhost:81/",,"1.031217",; 
	"http://localhost:81/ws/W0500308.apw")

::Init()
::cVGDESCDETRESULT   :=  WSAdvValue( oXmlRet,"_VGDESCDETRESPONSE:_VGDESCDETRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure SOLICITQDR

WSSTRUCT W0500308_SOLICITQDR
	WSDATA   cCDCARGO                  AS string OPTIONAL
	WSDATA   cCDCENTCT                 AS string OPTIONAL
	WSDATA   cCDFUNCAO                 AS string OPTIONAL
	WSDATA   cCDPOST                   AS string OPTIONAL
	WSDATA   cCHAVSOLIC                AS string OPTIONAL
	WSDATA   cCODDEPTO                 AS string OPTIONAL
	WSDATA   cCODMATRIC                AS string OPTIONAL
	WSDATA   cCODSOLIC                 AS string OPTIONAL
	WSDATA   dDATARESPOST              AS date OPTIONAL
	WSDATA   dDATASOLIC                AS date OPTIONAL
	WSDATA   cEMPAPROV                 AS string OPTIONAL
	WSDATA   cEMPRESA                  AS string OPTIONAL
	WSDATA   cFILAPROV                 AS string OPTIONAL
	WSDATA   cFILIALQDR                AS string OPTIONAL
	WSDATA   cFILSOLIC                 AS string OPTIONAL
	WSDATA   cMATAPROV                 AS string OPTIONAL
	WSDATA   cMATSOLIC                 AS string OPTIONAL
	WSDATA   cNOMESOLIC                AS string OPTIONAL
	WSDATA   nNVLAPROV                 AS integer OPTIONAL
	WSDATA   nNVLINICIAL               AS integer OPTIONAL
	WSDATA   cNVPOST                   AS string OPTIONAL
	WSDATA   cOBSERV                   AS string OPTIONAL
	WSDATA   cPARTICRH                 AS string OPTIONAL
	WSDATA   cQNTQDRO                  AS string OPTIONAL
	WSDATA   cSALARIO                  AS string OPTIONAL
	WSDATA   cSTATUS                   AS string OPTIONAL
	WSDATA   cTPCONTR                  AS string OPTIONAL
	WSDATA   cTPPOSTO                  AS string OPTIONAL
	WSDATA   cVISAOORG                 AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_SOLICITQDR
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_SOLICITQDR
Return

WSMETHOD CLONE WSCLIENT W0500308_SOLICITQDR
	Local oClone := W0500308_SOLICITQDR():NEW()
	oClone:cCDCARGO             := ::cCDCARGO
	oClone:cCDCENTCT            := ::cCDCENTCT
	oClone:cCDFUNCAO            := ::cCDFUNCAO
	oClone:cCDPOST              := ::cCDPOST
	oClone:cCHAVSOLIC           := ::cCHAVSOLIC
	oClone:cCODDEPTO            := ::cCODDEPTO
	oClone:cCODMATRIC           := ::cCODMATRIC
	oClone:cCODSOLIC            := ::cCODSOLIC
	oClone:dDATARESPOST         := ::dDATARESPOST
	oClone:dDATASOLIC           := ::dDATASOLIC
	oClone:cEMPAPROV            := ::cEMPAPROV
	oClone:cEMPRESA             := ::cEMPRESA
	oClone:cFILAPROV            := ::cFILAPROV
	oClone:cFILIALQDR           := ::cFILIALQDR
	oClone:cFILSOLIC            := ::cFILSOLIC
	oClone:cMATAPROV            := ::cMATAPROV
	oClone:cMATSOLIC            := ::cMATSOLIC
	oClone:cNOMESOLIC           := ::cNOMESOLIC
	oClone:nNVLAPROV            := ::nNVLAPROV
	oClone:nNVLINICIAL          := ::nNVLINICIAL
	oClone:cNVPOST              := ::cNVPOST
	oClone:cOBSERV              := ::cOBSERV
	oClone:cPARTICRH            := ::cPARTICRH
	oClone:cQNTQDRO             := ::cQNTQDRO
	oClone:cSALARIO             := ::cSALARIO
	oClone:cSTATUS              := ::cSTATUS
	oClone:cTPCONTR             := ::cTPCONTR
	oClone:cTPPOSTO             := ::cTPPOSTO
	oClone:cVISAOORG            := ::cVISAOORG
Return oClone

WSMETHOD SOAPSEND WSCLIENT W0500308_SOLICITQDR
	Local cSoap := ""
	cSoap += WSSoapValue("CDCARGO", ::cCDCARGO, ::cCDCARGO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CDCENTCT", ::cCDCENTCT, ::cCDCENTCT , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CDFUNCAO", ::cCDFUNCAO, ::cCDFUNCAO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CDPOST", ::cCDPOST, ::cCDPOST , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CHAVSOLIC", ::cCHAVSOLIC, ::cCHAVSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODDEPTO", ::cCODDEPTO, ::cCODDEPTO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODMATRIC", ::cCODMATRIC, ::cCODMATRIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODSOLIC", ::cCODSOLIC, ::cCODSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATARESPOST", ::dDATARESPOST, ::dDATARESPOST , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATASOLIC", ::dDATASOLIC, ::dDATASOLIC , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("EMPAPROV", ::cEMPAPROV, ::cEMPAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("EMPRESA", ::cEMPRESA, ::cEMPRESA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILAPROV", ::cFILAPROV, ::cFILAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILIALQDR", ::cFILIALQDR, ::cFILIALQDR , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILSOLIC", ::cFILSOLIC, ::cFILSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("MATAPROV", ::cMATAPROV, ::cMATAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("MATSOLIC", ::cMATSOLIC, ::cMATSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NOMESOLIC", ::cNOMESOLIC, ::cNOMESOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NVLAPROV", ::nNVLAPROV, ::nNVLAPROV , "integer", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NVLINICIAL", ::nNVLINICIAL, ::nNVLINICIAL , "integer", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NVPOST", ::cNVPOST, ::cNVPOST , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("OBSERV", ::cOBSERV, ::cOBSERV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PARTICRH", ::cPARTICRH, ::cPARTICRH , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("QNTQDRO", ::cQNTQDRO, ::cQNTQDRO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("SALARIO", ::cSALARIO, ::cSALARIO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("STATUS", ::cSTATUS, ::cSTATUS , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TPCONTR", ::cTPCONTR, ::cTPCONTR , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TPPOSTO", ::cTPPOSTO, ::cTPPOSTO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("VISAOORG", ::cVISAOORG, ::cVISAOORG , "string", .F. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure SOLICITVAG

WSSTRUCT W0500308_SOLICITVAG
	WSDATA   cCATFUN                   AS string OPTIONAL
	WSDATA   cCHAVSOLIC                AS string OPTIONAL
	WSDATA   cCODCARGO                 AS string OPTIONAL
	WSDATA   cCODCC                    AS string OPTIONAL
	WSDATA   cCODDEPTO                 AS string OPTIONAL
	WSDATA   cCODFUNC                  AS string OPTIONAL
	WSDATA   cCODMATRIC                AS string OPTIONAL
	WSDATA   cCODPOSTO                 AS string OPTIONAL
	WSDATA   cCODSOLVG                 AS string OPTIONAL
	WSDATA   cCUSTOVAGA                AS string OPTIONAL
	WSDATA   dDATABERTURA              AS date OPTIONAL
	WSDATA   dDATARESPOST              AS date OPTIONAL
	WSDATA   dDATASOLIC                AS date OPTIONAL
	WSDATA   dDATFECHAM                AS date OPTIONAL
	WSDATA   cDSCVAGA                  AS string OPTIONAL
	WSDATA   cEMPAPROV                 AS string OPTIONAL
	WSDATA   cEMPRESA                  AS string OPTIONAL
	WSDATA   cFILAPROV                 AS string OPTIONAL
	WSDATA   cFILIALVG                 AS string OPTIONAL
	WSDATA   cFILPOSTO                 AS string OPTIONAL
	WSDATA   cFILSOLIC                 AS string OPTIONAL
	WSDATA   cHORMES                   AS string OPTIONAL
	WSDATA   cHORSEM                   AS string OPTIONAL
	WSDATA   cLRET                     AS string OPTIONAL
	WSDATA   cMATAPROV                 AS string OPTIONAL
	WSDATA   cMATSOLIC                 AS string OPTIONAL
	WSDATA   cNOMEAPROV                AS string OPTIONAL
	WSDATA   cNOMESOLIC                AS string OPTIONAL
	WSDATA   nNVLAPROV                 AS integer OPTIONAL
	WSDATA   nNVLINICIAL               AS integer OPTIONAL
	WSDATA   cPARTICRH                 AS string OPTIONAL
	WSDATA   cPRAZOVAGA                AS string OPTIONAL
	WSDATA   cPROCSELET                AS string OPTIONAL
	WSDATA   cSALHORA                  AS string OPTIONAL
	WSDATA   cSOURCE                   AS string OPTIONAL
	WSDATA   cSTATUS                   AS string OPTIONAL
	WSDATA   cTPCONTR                  AS string OPTIONAL
	WSDATA   cTPRJ                     AS string OPTIONAL
	WSDATA   cTPVAGA                   AS string OPTIONAL
	WSDATA   cTURNOTAB                 AS string OPTIONAL
	WSDATA   cVAGA                     AS string OPTIONAL
	WSDATA   cVISAOORG                 AS string OPTIONAL
	WSDATA   cXOBS                     AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_SOLICITVAG
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_SOLICITVAG
Return

WSMETHOD CLONE WSCLIENT W0500308_SOLICITVAG
	Local oClone := W0500308_SOLICITVAG():NEW()
	oClone:cCATFUN              := ::cCATFUN
	oClone:cCHAVSOLIC           := ::cCHAVSOLIC
	oClone:cCODCARGO            := ::cCODCARGO
	oClone:cCODCC               := ::cCODCC
	oClone:cCODDEPTO            := ::cCODDEPTO
	oClone:cCODFUNC             := ::cCODFUNC
	oClone:cCODMATRIC           := ::cCODMATRIC
	oClone:cCODPOSTO            := ::cCODPOSTO
	oClone:cCODSOLVG            := ::cCODSOLVG
	oClone:cCUSTOVAGA           := ::cCUSTOVAGA
	oClone:dDATABERTURA         := ::dDATABERTURA
	oClone:dDATARESPOST         := ::dDATARESPOST
	oClone:dDATASOLIC           := ::dDATASOLIC
	oClone:dDATFECHAM           := ::dDATFECHAM
	oClone:cDSCVAGA             := ::cDSCVAGA
	oClone:cEMPAPROV            := ::cEMPAPROV
	oClone:cEMPRESA             := ::cEMPRESA
	oClone:cFILAPROV            := ::cFILAPROV
	oClone:cFILIALVG            := ::cFILIALVG
	oClone:cFILPOSTO            := ::cFILPOSTO
	oClone:cFILSOLIC            := ::cFILSOLIC
	oClone:cHORMES              := ::cHORMES
	oClone:cHORSEM              := ::cHORSEM
	oClone:cLRET                := ::cLRET
	oClone:cMATAPROV            := ::cMATAPROV
	oClone:cMATSOLIC            := ::cMATSOLIC
	oClone:cNOMEAPROV           := ::cNOMEAPROV
	oClone:cNOMESOLIC           := ::cNOMESOLIC
	oClone:nNVLAPROV            := ::nNVLAPROV
	oClone:nNVLINICIAL          := ::nNVLINICIAL
	oClone:cPARTICRH            := ::cPARTICRH
	oClone:cPRAZOVAGA           := ::cPRAZOVAGA
	oClone:cPROCSELET           := ::cPROCSELET
	oClone:cSALHORA             := ::cSALHORA
	oClone:cSOURCE              := ::cSOURCE
	oClone:cSTATUS              := ::cSTATUS
	oClone:cTPCONTR             := ::cTPCONTR
	oClone:cTPRJ                := ::cTPRJ
	oClone:cTPVAGA              := ::cTPVAGA
	oClone:cTURNOTAB            := ::cTURNOTAB
	oClone:cVAGA                := ::cVAGA
	oClone:cVISAOORG            := ::cVISAOORG
	oClone:cXOBS                := ::cXOBS
Return oClone

WSMETHOD SOAPSEND WSCLIENT W0500308_SOLICITVAG
	Local cSoap := ""
	cSoap += WSSoapValue("CATFUN", ::cCATFUN, ::cCATFUN , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CHAVSOLIC", ::cCHAVSOLIC, ::cCHAVSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODCARGO", ::cCODCARGO, ::cCODCARGO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODCC", ::cCODCC, ::cCODCC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODDEPTO", ::cCODDEPTO, ::cCODDEPTO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODFUNC", ::cCODFUNC, ::cCODFUNC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODMATRIC", ::cCODMATRIC, ::cCODMATRIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODPOSTO", ::cCODPOSTO, ::cCODPOSTO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CODSOLVG", ::cCODSOLVG, ::cCODSOLVG , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("CUSTOVAGA", ::cCUSTOVAGA, ::cCUSTOVAGA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATABERTURA", ::dDATABERTURA, ::dDATABERTURA , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATARESPOST", ::dDATARESPOST, ::dDATARESPOST , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATASOLIC", ::dDATASOLIC, ::dDATASOLIC , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DATFECHAM", ::dDATFECHAM, ::dDATFECHAM , "date", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("DSCVAGA", ::cDSCVAGA, ::cDSCVAGA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("EMPAPROV", ::cEMPAPROV, ::cEMPAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("EMPRESA", ::cEMPRESA, ::cEMPRESA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILAPROV", ::cFILAPROV, ::cFILAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILIALVG", ::cFILIALVG, ::cFILIALVG , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILPOSTO", ::cFILPOSTO, ::cFILPOSTO , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("FILSOLIC", ::cFILSOLIC, ::cFILSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("HORMES", ::cHORMES, ::cHORMES , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("HORSEM", ::cHORSEM, ::cHORSEM , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("LRET", ::cLRET, ::cLRET , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("MATAPROV", ::cMATAPROV, ::cMATAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("MATSOLIC", ::cMATSOLIC, ::cMATSOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NOMEAPROV", ::cNOMEAPROV, ::cNOMEAPROV , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NOMESOLIC", ::cNOMESOLIC, ::cNOMESOLIC , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NVLAPROV", ::nNVLAPROV, ::nNVLAPROV , "integer", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("NVLINICIAL", ::nNVLINICIAL, ::nNVLINICIAL , "integer", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PARTICRH", ::cPARTICRH, ::cPARTICRH , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PRAZOVAGA", ::cPRAZOVAGA, ::cPRAZOVAGA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PROCSELET", ::cPROCSELET, ::cPROCSELET , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("SALHORA", ::cSALHORA, ::cSALHORA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("SOURCE", ::cSOURCE, ::cSOURCE , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("STATUS", ::cSTATUS, ::cSTATUS , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TPCONTR", ::cTPCONTR, ::cTPCONTR , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TPRJ", ::cTPRJ, ::cTPRJ , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TPVAGA", ::cTPVAGA, ::cTPVAGA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("TURNOTAB", ::cTURNOTAB, ::cTURNOTAB , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("VAGA", ::cVAGA, ::cVAGA , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("VISAOORG", ::cVISAOORG, ::cVISAOORG , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("XOBS", ::cXOBS, ::cXOBS , "string", .F. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure QDRDADOS

WSSTRUCT W0500308_QDRDADOS
	WSDATA   cCDCARG                   AS string OPTIONAL
	WSDATA   cCDCCTT                   AS string OPTIONAL
	WSDATA   cCDDEPT                   AS string OPTIONAL
	WSDATA   cCDFUNC                   AS string OPTIONAL
	WSDATA   cCODPST                   AS string OPTIONAL
	WSDATA   cDSCARG                   AS string OPTIONAL
	WSDATA   cDSCCTT                   AS string OPTIONAL
	WSDATA   cDSDEPT                   AS string OPTIONAL
	WSDATA   cDSFUNC                   AS string OPTIONAL
	WSDATA   cFILQDR                   AS string OPTIONAL
	WSDATA   cOBSERV                   AS string OPTIONAL
	WSDATA   cORIGSL                   AS string OPTIONAL
	WSDATA   cQNTQDR                   AS string OPTIONAL
	WSDATA   cSALARI                   AS string OPTIONAL
	WSDATA   cTPCONT                   AS string OPTIONAL
	WSDATA   cTPPOST                   AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_QDRDADOS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_QDRDADOS
Return

WSMETHOD CLONE WSCLIENT W0500308_QDRDADOS
	Local oClone := W0500308_QDRDADOS():NEW()
	oClone:cCDCARG              := ::cCDCARG
	oClone:cCDCCTT              := ::cCDCCTT
	oClone:cCDDEPT              := ::cCDDEPT
	oClone:cCDFUNC              := ::cCDFUNC
	oClone:cCODPST              := ::cCODPST
	oClone:cDSCARG              := ::cDSCARG
	oClone:cDSCCTT              := ::cDSCCTT
	oClone:cDSDEPT              := ::cDSDEPT
	oClone:cDSFUNC              := ::cDSFUNC
	oClone:cFILQDR              := ::cFILQDR
	oClone:cOBSERV              := ::cOBSERV
	oClone:cORIGSL              := ::cORIGSL
	oClone:cQNTQDR              := ::cQNTQDR
	oClone:cSALARI              := ::cSALARI
	oClone:cTPCONT              := ::cTPCONT
	oClone:cTPPOST              := ::cTPPOST
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_QDRDADOS
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCDCARG            :=  WSAdvValue( oResponse,"_CDCARG","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCDCCTT            :=  WSAdvValue( oResponse,"_CDCCTT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCDDEPT            :=  WSAdvValue( oResponse,"_CDDEPT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCDFUNC            :=  WSAdvValue( oResponse,"_CDFUNC","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cCODPST            :=  WSAdvValue( oResponse,"_CODPST","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDSCARG            :=  WSAdvValue( oResponse,"_DSCARG","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDSCCTT            :=  WSAdvValue( oResponse,"_DSCCTT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDSDEPT            :=  WSAdvValue( oResponse,"_DSDEPT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDSFUNC            :=  WSAdvValue( oResponse,"_DSFUNC","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cFILQDR            :=  WSAdvValue( oResponse,"_FILQDR","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cOBSERV            :=  WSAdvValue( oResponse,"_OBSERV","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cORIGSL            :=  WSAdvValue( oResponse,"_ORIGSL","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cQNTQDR            :=  WSAdvValue( oResponse,"_QNTQDR","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cSALARI            :=  WSAdvValue( oResponse,"_SALARI","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cTPCONT            :=  WSAdvValue( oResponse,"_TPCONT","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cTPPOST            :=  WSAdvValue( oResponse,"_TPPOST","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure VAGDADOS

WSSTRUCT W0500308_VAGDADOS
	WSDATA   cCATFUN                   AS string
	WSDATA   cCODCARGO                 AS string
	WSDATA   cCODCC                    AS string
	WSDATA   cCODSOLIC                 AS string
	WSDATA   cCUSTVG                   AS string
	WSDATA   cDEPTO                    AS string
	WSDATA   cDESCCC                   AS string
	WSDATA   cDESCFU                   AS string
	WSDATA   cDESCR                    AS string
	WSDATA   cDESCRCARGO               AS string
	WSDATA   cDTABT                    AS string
	WSDATA   cDTFCH                    AS string
	WSDATA   cFILVAG                   AS string
	WSDATA   cFLPOST                   AS string
	WSDATA   cFUNCAO                   AS string
	WSDATA   cHORMES                   AS string
	WSDATA   cHORSEM                   AS string
	WSDATA   cNOMESOL                  AS string
	WSDATA   cOBSERDET                 AS string
	WSDATA   cPOSTO                    AS string
	WSDATA   cPRAZO                    AS string
	WSDATA   cPROCS                    AS string
	WSDATA   cRET                      AS string
	WSDATA   cSALHORA                  AS string
	WSDATA   cTPCONTR                  AS string
	WSDATA   cTPVAGA                   AS string
	WSDATA   cTURNOTAB                 AS string
	WSDATA   cVAGA                     AS string
	WSDATA   cXOBS                     AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_VAGDADOS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_VAGDADOS
Return

WSMETHOD CLONE WSCLIENT W0500308_VAGDADOS
	Local oClone := W0500308_VAGDADOS():NEW()
	oClone:cCATFUN              := ::cCATFUN
	oClone:cCODCARGO            := ::cCODCARGO
	oClone:cCODCC               := ::cCODCC
	oClone:cCODSOLIC            := ::cCODSOLIC
	oClone:cCUSTVG              := ::cCUSTVG
	oClone:cDEPTO               := ::cDEPTO
	oClone:cDESCCC              := ::cDESCCC
	oClone:cDESCFU              := ::cDESCFU
	oClone:cDESCR               := ::cDESCR
	oClone:cDESCRCARGO          := ::cDESCRCARGO
	oClone:cDTABT               := ::cDTABT
	oClone:cDTFCH               := ::cDTFCH
	oClone:cFILVAG              := ::cFILVAG
	oClone:cFLPOST              := ::cFLPOST
	oClone:cFUNCAO              := ::cFUNCAO
	oClone:cHORMES              := ::cHORMES
	oClone:cHORSEM              := ::cHORSEM
	oClone:cNOMESOL             := ::cNOMESOL
	oClone:cOBSERDET            := ::cOBSERDET
	oClone:cPOSTO               := ::cPOSTO
	oClone:cPRAZO               := ::cPRAZO
	oClone:cPROCS               := ::cPROCS
	oClone:cRET                 := ::cRET
	oClone:cSALHORA             := ::cSALHORA
	oClone:cTPCONTR             := ::cTPCONTR
	oClone:cTPVAGA              := ::cTPVAGA
	oClone:cTURNOTAB            := ::cTURNOTAB
	oClone:cVAGA                := ::cVAGA
	oClone:cXOBS                := ::cXOBS
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_VAGDADOS
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCATFUN            :=  WSAdvValue( oResponse,"_CATFUN","string",NIL,"Property cCATFUN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCODCARGO          :=  WSAdvValue( oResponse,"_CODCARGO","string",NIL,"Property cCODCARGO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCODCC             :=  WSAdvValue( oResponse,"_CODCC","string",NIL,"Property cCODCC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCODSOLIC          :=  WSAdvValue( oResponse,"_CODSOLIC","string",NIL,"Property cCODSOLIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCUSTVG            :=  WSAdvValue( oResponse,"_CUSTVG","string",NIL,"Property cCUSTVG as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDEPTO             :=  WSAdvValue( oResponse,"_DEPTO","string",NIL,"Property cDEPTO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDESCCC            :=  WSAdvValue( oResponse,"_DESCCC","string",NIL,"Property cDESCCC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDESCFU            :=  WSAdvValue( oResponse,"_DESCFU","string",NIL,"Property cDESCFU as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDESCR             :=  WSAdvValue( oResponse,"_DESCR","string",NIL,"Property cDESCR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDESCRCARGO        :=  WSAdvValue( oResponse,"_DESCRCARGO","string",NIL,"Property cDESCRCARGO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDTABT             :=  WSAdvValue( oResponse,"_DTABT","string",NIL,"Property cDTABT as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDTFCH             :=  WSAdvValue( oResponse,"_DTFCH","string",NIL,"Property cDTFCH as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFILVAG            :=  WSAdvValue( oResponse,"_FILVAG","string",NIL,"Property cFILVAG as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFLPOST            :=  WSAdvValue( oResponse,"_FLPOST","string",NIL,"Property cFLPOST as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFUNCAO            :=  WSAdvValue( oResponse,"_FUNCAO","string",NIL,"Property cFUNCAO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cHORMES            :=  WSAdvValue( oResponse,"_HORMES","string",NIL,"Property cHORMES as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cHORSEM            :=  WSAdvValue( oResponse,"_HORSEM","string",NIL,"Property cHORSEM as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNOMESOL           :=  WSAdvValue( oResponse,"_NOMESOL","string",NIL,"Property cNOMESOL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cOBSERDET          :=  WSAdvValue( oResponse,"_OBSERDET","string",NIL,"Property cOBSERDET as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPOSTO             :=  WSAdvValue( oResponse,"_POSTO","string",NIL,"Property cPOSTO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPRAZO             :=  WSAdvValue( oResponse,"_PRAZO","string",NIL,"Property cPRAZO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cPROCS             :=  WSAdvValue( oResponse,"_PROCS","string",NIL,"Property cPROCS as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRET               :=  WSAdvValue( oResponse,"_RET","string",NIL,"Property cRET as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSALHORA           :=  WSAdvValue( oResponse,"_SALHORA","string",NIL,"Property cSALHORA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTPCONTR           :=  WSAdvValue( oResponse,"_TPCONTR","string",NIL,"Property cTPCONTR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTPVAGA            :=  WSAdvValue( oResponse,"_TPVAGA","string",NIL,"Property cTPVAGA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cTURNOTAB          :=  WSAdvValue( oResponse,"_TURNOTAB","string",NIL,"Property cTURNOTAB as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cVAGA              :=  WSAdvValue( oResponse,"_VAGA","string",NIL,"Property cVAGA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cXOBS              :=  WSAdvValue( oResponse,"_XOBS","string",NIL,"Property cXOBS as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure _FAIXSAL

WSSTRUCT W0500308__FAIXSAL
	WSDATA   oWSREGISTRO               AS W0500308_ARRAYOFFAIXSAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308__FAIXSAL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308__FAIXSAL
Return

WSMETHOD CLONE WSCLIENT W0500308__FAIXSAL
	Local oClone := W0500308__FAIXSAL():NEW()
	oClone:oWSREGISTRO          := IIF(::oWSREGISTRO = NIL , NIL , ::oWSREGISTRO:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308__FAIXSAL
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_REGISTRO","ARRAYOFFAIXSAL",NIL,"Property oWSREGISTRO as s0:ARRAYOFFAIXSAL on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSREGISTRO := W0500308_ARRAYOFFAIXSAL():New()
		::oWSREGISTRO:SoapRecv(oNode1)
	EndIf
Return

// WSDL Data Structure INFRH3

WSSTRUCT W0500308_INFRH3
	WSDATA   cRH3FILAPR                AS string
	WSDATA   cRH3FILINI                AS string
	WSDATA   cRH3MATAPR                AS string
	WSDATA   cRH3MATINI                AS string
	WSDATA   cRH3VISAO                 AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_INFRH3
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_INFRH3
Return

WSMETHOD CLONE WSCLIENT W0500308_INFRH3
	Local oClone := W0500308_INFRH3():NEW()
	oClone:cRH3FILAPR           := ::cRH3FILAPR
	oClone:cRH3FILINI           := ::cRH3FILINI
	oClone:cRH3MATAPR           := ::cRH3MATAPR
	oClone:cRH3MATINI           := ::cRH3MATINI
	oClone:cRH3VISAO            := ::cRH3VISAO
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_INFRH3
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cRH3FILAPR         :=  WSAdvValue( oResponse,"_RH3FILAPR","string",NIL,"Property cRH3FILAPR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3FILINI         :=  WSAdvValue( oResponse,"_RH3FILINI","string",NIL,"Property cRH3FILINI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3MATAPR         :=  WSAdvValue( oResponse,"_RH3MATAPR","string",NIL,"Property cRH3MATAPR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3MATINI         :=  WSAdvValue( oResponse,"_RH3MATINI","string",NIL,"Property cRH3MATINI as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cRH3VISAO          :=  WSAdvValue( oResponse,"_RH3VISAO","string",NIL,"Property cRH3VISAO as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure ARETSOLICT

WSSTRUCT W0500308_ARETSOLICT
	WSDATA   cCDSOLIC                  AS string
	WSDATA   cDATASOL                  AS string
	WSDATA   cLRETORN                  AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_ARETSOLICT
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_ARETSOLICT
Return

WSMETHOD CLONE WSCLIENT W0500308_ARETSOLICT
	Local oClone := W0500308_ARETSOLICT():NEW()
	oClone:cCDSOLIC             := ::cCDSOLIC
	oClone:cDATASOL             := ::cDATASOL
	oClone:cLRETORN             := ::cLRETORN
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_ARETSOLICT
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCDSOLIC           :=  WSAdvValue( oResponse,"_CDSOLIC","string",NIL,"Property cCDSOLIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDATASOL           :=  WSAdvValue( oResponse,"_DATASOL","string",NIL,"Property cDATASOL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cLRETORN           :=  WSAdvValue( oResponse,"_LRETORN","string",NIL,"Property cLRETORN as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure AMNHVGSL

WSSTRUCT W0500308_AMNHVGSL
	WSDATA   oWSAMNHVAG                AS W0500308_ARRAYOFMNHVGSOLIC
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_AMNHVGSL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_AMNHVGSL
Return

WSMETHOD CLONE WSCLIENT W0500308_AMNHVGSL
	Local oClone := W0500308_AMNHVGSL():NEW()
	oClone:oWSAMNHVAG           := IIF(::oWSAMNHVAG = NIL , NIL , ::oWSAMNHVAG:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_AMNHVGSL
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_AMNHVAG","ARRAYOFMNHVGSOLIC",NIL,"Property oWSAMNHVAG as s0:ARRAYOFMNHVGSOLIC on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSAMNHVAG := W0500308_ARRAYOFMNHVGSOLIC():New()
		::oWSAMNHVAG:SoapRecv(oNode1)
	EndIf
Return

// WSDL Data Structure AAPREPVAG

WSSTRUCT W0500308_AAPREPVAG
	WSDATA   oWSAAPREP                 AS W0500308_ARRAYOFAPREPVAGA
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_AAPREPVAG
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_AAPREPVAG
Return

WSMETHOD CLONE WSCLIENT W0500308_AAPREPVAG
	Local oClone := W0500308_AAPREPVAG():NEW()
	oClone:oWSAAPREP            := IIF(::oWSAAPREP = NIL , NIL , ::oWSAAPREP:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_AAPREPVAG
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_AAPREP","ARRAYOFAPREPVAGA",NIL,"Property oWSAAPREP as s0:ARRAYOFAPREPVAGA on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSAAPREP := W0500308_ARRAYOFAPREPVAGA():New()
		::oWSAAPREP:SoapRecv(oNode1)
	EndIf
Return

// WSDL Data Structure APROVS

WSSTRUCT W0500308_APROVS
	WSDATA   cEMPAPROV                 AS string OPTIONAL
	WSDATA   cFILAPROV                 AS string OPTIONAL
	WSDATA   cMATAPROV                 AS string OPTIONAL
	WSDATA   cNOMEAPROV                AS string OPTIONAL
	WSDATA   nNVLAPROV                 AS integer OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_APROVS
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_APROVS
Return

WSMETHOD CLONE WSCLIENT W0500308_APROVS
	Local oClone := W0500308_APROVS():NEW()
	oClone:cEMPAPROV            := ::cEMPAPROV
	oClone:cFILAPROV            := ::cFILAPROV
	oClone:cMATAPROV            := ::cMATAPROV
	oClone:cNOMEAPROV           := ::cNOMEAPROV
	oClone:nNVLAPROV            := ::nNVLAPROV
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_APROVS
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cEMPAPROV          :=  WSAdvValue( oResponse,"_EMPAPROV","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cFILAPROV          :=  WSAdvValue( oResponse,"_FILAPROV","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cMATAPROV          :=  WSAdvValue( oResponse,"_MATAPROV","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cNOMEAPROV         :=  WSAdvValue( oResponse,"_NOMEAPROV","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::nNVLAPROV          :=  WSAdvValue( oResponse,"_NVLAPROV","integer",NIL,NIL,NIL,"N",NIL,NIL) 
Return

// WSDL Data Structure IDVAGA

WSSTRUCT W0500308_IDVAGA
	WSDATA   cQ3CARGO                  AS string
	WSDATA   cQ3CC                     AS string
	WSDATA   cQ3FILIAL                 AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_IDVAGA
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_IDVAGA
Return

WSMETHOD CLONE WSCLIENT W0500308_IDVAGA
	Local oClone := W0500308_IDVAGA():NEW()
	oClone:cQ3CARGO             := ::cQ3CARGO
	oClone:cQ3CC                := ::cQ3CC
	oClone:cQ3FILIAL            := ::cQ3FILIAL
Return oClone

WSMETHOD SOAPSEND WSCLIENT W0500308_IDVAGA
	Local cSoap := ""
	cSoap += WSSoapValue("Q3CARGO", ::cQ3CARGO, ::cQ3CARGO , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("Q3CC", ::cQ3CC, ::cQ3CC , "string", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("Q3FILIAL", ::cQ3FILIAL, ::cQ3FILIAL , "string", .T. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure ARRAYOFFAIXSAL

WSSTRUCT W0500308_ARRAYOFFAIXSAL
	WSDATA   oWSFAIXSAL                AS W0500308_FAIXSAL OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_ARRAYOFFAIXSAL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_ARRAYOFFAIXSAL
	::oWSFAIXSAL           := {} // Array Of  W0500308_FAIXSAL():New()
Return

WSMETHOD CLONE WSCLIENT W0500308_ARRAYOFFAIXSAL
	Local oClone := W0500308_ARRAYOFFAIXSAL():NEW()
	oClone:oWSFAIXSAL := NIL
	If ::oWSFAIXSAL <> NIL 
		oClone:oWSFAIXSAL := {}
		aEval( ::oWSFAIXSAL , { |x| aadd( oClone:oWSFAIXSAL , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_ARRAYOFFAIXSAL
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_FAIXSAL","FAIXSAL",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSFAIXSAL , W0500308_FAIXSAL():New() )
			::oWSFAIXSAL[len(::oWSFAIXSAL)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ARRAYOFMNHVGSOLIC

WSSTRUCT W0500308_ARRAYOFMNHVGSOLIC
	WSDATA   oWSMNHVGSOLIC             AS W0500308_MNHVGSOLIC OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_ARRAYOFMNHVGSOLIC
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_ARRAYOFMNHVGSOLIC
	::oWSMNHVGSOLIC        := {} // Array Of  W0500308_MNHVGSOLIC():New()
Return

WSMETHOD CLONE WSCLIENT W0500308_ARRAYOFMNHVGSOLIC
	Local oClone := W0500308_ARRAYOFMNHVGSOLIC():NEW()
	oClone:oWSMNHVGSOLIC := NIL
	If ::oWSMNHVGSOLIC <> NIL 
		oClone:oWSMNHVGSOLIC := {}
		aEval( ::oWSMNHVGSOLIC , { |x| aadd( oClone:oWSMNHVGSOLIC , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_ARRAYOFMNHVGSOLIC
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_MNHVGSOLIC","MNHVGSOLIC",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSMNHVGSOLIC , W0500308_MNHVGSOLIC():New() )
			::oWSMNHVGSOLIC[len(::oWSMNHVGSOLIC)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ARRAYOFAPREPVAGA

WSSTRUCT W0500308_ARRAYOFAPREPVAGA
	WSDATA   oWSAPREPVAGA              AS W0500308_APREPVAGA OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_ARRAYOFAPREPVAGA
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_ARRAYOFAPREPVAGA
	::oWSAPREPVAGA         := {} // Array Of  W0500308_APREPVAGA():New()
Return

WSMETHOD CLONE WSCLIENT W0500308_ARRAYOFAPREPVAGA
	Local oClone := W0500308_ARRAYOFAPREPVAGA():NEW()
	oClone:oWSAPREPVAGA := NIL
	If ::oWSAPREPVAGA <> NIL 
		oClone:oWSAPREPVAGA := {}
		aEval( ::oWSAPREPVAGA , { |x| aadd( oClone:oWSAPREPVAGA , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_ARRAYOFAPREPVAGA
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_APREPVAGA","APREPVAGA",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSAPREPVAGA , W0500308_APREPVAGA():New() )
			::oWSAPREPVAGA[len(::oWSAPREPVAGA)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure FAIXSAL

WSSTRUCT W0500308_FAIXSAL
	WSDATA   cFAIXA                    AS string
	WSDATA   cNIVEL                    AS string
	WSDATA   cVALOR                    AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_FAIXSAL
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_FAIXSAL
Return

WSMETHOD CLONE WSCLIENT W0500308_FAIXSAL
	Local oClone := W0500308_FAIXSAL():NEW()
	oClone:cFAIXA               := ::cFAIXA
	oClone:cNIVEL               := ::cNIVEL
	oClone:cVALOR               := ::cVALOR
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_FAIXSAL
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cFAIXA             :=  WSAdvValue( oResponse,"_FAIXA","string",NIL,"Property cFAIXA as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cNIVEL             :=  WSAdvValue( oResponse,"_NIVEL","string",NIL,"Property cNIVEL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cVALOR             :=  WSAdvValue( oResponse,"_VALOR","string",NIL,"Property cVALOR as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure MNHVGSOLIC

WSSTRUCT W0500308_MNHVGSOLIC
	WSDATA   cCDSOLIC                  AS string
	WSDATA   cDATASOL                  AS string
	WSDATA   cFILCDSL                  AS string
	WSDATA   cSTATUS                   AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_MNHVGSOLIC
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_MNHVGSOLIC
Return

WSMETHOD CLONE WSCLIENT W0500308_MNHVGSOLIC
	Local oClone := W0500308_MNHVGSOLIC():NEW()
	oClone:cCDSOLIC             := ::cCDSOLIC
	oClone:cDATASOL             := ::cDATASOL
	oClone:cFILCDSL             := ::cFILCDSL
	oClone:cSTATUS              := ::cSTATUS
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_MNHVGSOLIC
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCDSOLIC           :=  WSAdvValue( oResponse,"_CDSOLIC","string",NIL,"Property cCDSOLIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDATASOL           :=  WSAdvValue( oResponse,"_DATASOL","string",NIL,"Property cDATASOL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFILCDSL           :=  WSAdvValue( oResponse,"_FILCDSL","string",NIL,"Property cFILCDSL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSTATUS            :=  WSAdvValue( oResponse,"_STATUS","string",NIL,"Property cSTATUS as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return

// WSDL Data Structure APREPVAGA

WSSTRUCT W0500308_APREPVAGA
	WSDATA   cACAOSOL                  AS string
	WSDATA   cCODAPRP                  AS string
	WSDATA   cDTSOLIC                  AS string
	WSDATA   cFILSOLC                  AS string
	WSDATA   cSTATSOL                  AS string
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT W0500308_APREPVAGA
	::Init()
Return Self

WSMETHOD INIT WSCLIENT W0500308_APREPVAGA
Return

WSMETHOD CLONE WSCLIENT W0500308_APREPVAGA
	Local oClone := W0500308_APREPVAGA():NEW()
	oClone:cACAOSOL             := ::cACAOSOL
	oClone:cCODAPRP             := ::cCODAPRP
	oClone:cDTSOLIC             := ::cDTSOLIC
	oClone:cFILSOLC             := ::cFILSOLC
	oClone:cSTATSOL             := ::cSTATSOL
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT W0500308_APREPVAGA
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cACAOSOL           :=  WSAdvValue( oResponse,"_ACAOSOL","string",NIL,"Property cACAOSOL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cCODAPRP           :=  WSAdvValue( oResponse,"_CODAPRP","string",NIL,"Property cCODAPRP as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cDTSOLIC           :=  WSAdvValue( oResponse,"_DTSOLIC","string",NIL,"Property cDTSOLIC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cFILSOLC           :=  WSAdvValue( oResponse,"_FILSOLC","string",NIL,"Property cFILSOLC as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::cSTATSOL           :=  WSAdvValue( oResponse,"_STATSOL","string",NIL,"Property cSTATSOL as s:string on SOAP Response not found.",NIL,"S",NIL,NIL) 
Return


