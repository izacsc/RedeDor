#Include 'Protheus.ch'

/*
{Protheus.doc} F0201001()
JOB para a execu��o da query.
@Author     Mick William da Silva
@Since      22/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_010     
@Param  	aParams, Vetor Padr�o Escheduler {Empresa,Filial,Usu�rio}
@Menu		Rotina para colocar em Scheduler
*/
User Function F0201001(aParams)

	Local cCodEmp := ""
	Local cCodFil := ""
	
	DEFAULT aParams := {"01","01",.T.}
	
	cCodEmp := aParams[1]
	cCodFil := aParams[2]
	
	//-- Verifica se j� existe um Job em execu��o com o mesmo nome.
	If LockByName("F0201001",.F.,.F. )
		Conout('','Rotina para exporta��o de dados de compras core.')
		ConOut('','[INICIANDO JOB ] - F0201001' )
		Conout('','[] ['+DtoC(Date())+' - '+Time()+']')
		
		Conout('','Inicio Thread: ' + cValToChar(ThreadID()) )
		
		RpcSetEnv(cCodEmp,cCodFil,,,'COM')
		JOB02010()
		
		//-- Fecha Ambiente
		RpcClearEnv()
		
		Conout('','Final Thread: ' + cValToChar(ThreadID()) )
		
		ConOut( '[FIM DA EXPORTA��O ] - F0201001 - F0201001 ' )
		Conout( '[F0201001] ['+DtoC(Date())+' - '+Time()+'] ')
		UnLockByName("F0201001",.F.,.F. )
	Else
		Conout('J� existe um Job com mesmo nome em execuss�o!')
	EndIf
	
Return

/*
{Protheus.doc} JOB02010()
Query para exporta��o de dados de compras core.
@Author     Mick William da Silva
@Since      22/06/2016
@Version    P12.7
@Project    MAN00000463301_EF_010   
*/
Static Function JOB02010()
	
	Local nQtdMes := SuperGetMv('FS_XMESES',,3)
	Local dDTFIM  := DtoS(MonthSub(dDataBase,nQtdMes))
	Local cQuery  := ""
	Local cDesc	  := ""
	Local cE2Dta  := ""
	Local cAlsSC7 := GetNextAlias()
	Local cAlsSD1 := GetNextAlias()
	Local cAlsSC1 := GetNextAlias()
					
	cQuery := " SELECT C7_FILIAL,C7_NUM,C7_COMPRA,C7_PRODUTO,C7_DESCRI,C7_FORNECE,C7_LOJA,C7_DATPRF, "
	cQuery += " C7_QUANT,C7_QUJE,C7_COND,C7_TOTAL,C7_NUMSC,C7_ITEM,C7_CC,C7_LOCAL,C7_CONTRA,C7_NUMCOT, "
	cQuery += " C7_XPEDBIO,C7_XPADRON, C7_XCARAC, C7_XTPROC,C7_XEMERG " 			 				
	cQuery += " FROM "+ RETSQLNAME('SC7')+ " SC7 "		
	cQuery += " WHERE SC7.D_E_L_E_T_= ' ' AND SC7.C7_EMISSAO BETWEEN '"+ dDTFIM +"' AND '"+ DtoS(dDataBase) +"' "
	cQuery += " ORDER BY SC7.C7_NUM "
	
	cQuery := ChangeQuery(cQuery)

	DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAlsSC7, .F., .T.)

	If	(cAlsSC7)->(!Eof())
		(cAlsSC7)->(dBGoTop())
		
		While (cAlsSC7)->(!Eof())
			If P99->(dBSeek((cAlsSC7)->C7_FILIAL + (cAlsSC7)->C7_NUM))
				While P99->P99_FILIAL == (cAlsSC7)->C7_FILIAL .and. P99->P99_NUMPC	= (cAlsSC7)->C7_NUM
					RecLock("P99",.F.)
					P99->(dBDelete())
					P99->(MsUnlock())
					P99->(dBSkip())
				EndDo
			EndIf
			(cAlsSC7)->(dBSkip())
		EndDo	

		(cAlsSC7)->(dBGoTop())

		Do While (cAlsSC7)->(!Eof())

			cQuery:= " SELECT D1_EMISSAO,D1_XDTENTR,F1_DTDIGIT,F1_DOC,F1_SERIE,F1_EMISSAO,F1_DTLANC,F1_COND,F1_FILIAL,F1_PREFIXO,F1_DUPL,F1_XDTVNF "		  
			cQuery+= " FROM "+ RETSQLNAME('SD1')+ " SD1 " 																			 		
			cQuery+= " LEFT JOIN "+ RETSQLNAME('SF1')+ " SF1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_SERIE = D1_SERIE "	 
			cQuery+= " AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA " 															 
			cQuery+= " WHERE SD1.D_E_L_E_T_=' ' AND SF1.D_E_L_E_T_=' '  " 															 
			cQuery+= " AND SD1.D1_FILIAL='"+ (cAlsSC7)->C7_FILIAL +"' AND SD1.D1_PEDIDO='"+ (cAlsSC7)->C7_NUM +"' AND SD1.D1_COD='"+ (cAlsSC7)->C7_PRODUTO +"' "		
			cQuery+= " AND SD1.D1_ITEM='"+ (cAlsSC7)->C7_ITEM +"' "
			
			cQuery := ChangeQuery(cQuery)
			
			DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAlsSD1, .F., .T.)

			
			TCSETFIELD(cAlsSD1,"D1_XDTENTR","D",8,0)
			TCSETFIELD(cAlsSD1,"F1_XDTVNF","D",8,0)
			
			cQuery:= " SELECT C1_DATPRF,C1_EMISSAO,C1_SOLICIT,C1_APROV,C1_TPSC,C1_EMISSAO " 									 		  
			cQuery+= " FROM "+ RETSQLNAME('SC1')+ " SC1 " 																			 		
			cQuery+= " WHERE SC1.D_E_L_E_T_=' ' " 																					 
			cQuery+= " AND SC1.C1_FILIAL='"+ (cAlsSC7)->C7_FILIAL +"' AND SC1.C1_PRODUTO='"+ (cAlsSC7)->C7_PRODUTO +"' AND SC1.C1_NUM='"+ (cAlsSC7)->C7_NUMSC +"' "	
			cQuery+= " AND SC1.C1_ITEM='"+ (cAlsSC7)->C7_ITEM +"' "			 														 
			
			cQuery := ChangeQuery(cQuery)

			DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAlsSC1, .F., .T.)
			
			cE2Dta:= Posicione("SE2",1,(cAlsSD1)->F1_FILIAL + (cAlsSD1)->F1_PREFIXO + (cAlsSD1)->F1_DUPL,"E2_BAIXA")
			
			dbSelectArea("P99")
			P99->(DBSetOrder(1))
 
 			RecLock("P99",.T.)

			P99->P99_FILIAL	:= (cAlsSC7)->C7_FILIAL
			P99->P99_DTVOC	:= IIF(!Empty((cAlsSC1)->C1_DATPRF) .And. !Empty((cAlsSD1)->D1_EMISSAO), (DateDiffDay(StoD((cAlsSC1)->C1_DATPRF), StoD((cAlsSD1)->D1_EMISSAO)) ), 0 )
			P99->P99_QTPEN	:= (cAlsSC7)->C7_QUANT  - (cAlsSC7)->C7_QUJE
			P99->P99_GRUPO 	:= Posicione("SB1",1,(cAlsSC7)->C7_FILIAL + (cAlsSC7)->C7_PRODUTO,"B1_GRUPO")
			P99->P99_APROV	:= (cAlsSC1)->C1_APROV
			P99->P99_BAIXA	:= cE2Dta 
			P99->P99_CC		:= (cAlsSC7)->C7_CC
			P99->P99_CGC	:= Posicione("SA2",1,xFilial("SA2") + (cAlsSC7)->C7_FORNECE + (cAlsSC7)->C7_LOJA ,"A2_CGC")
			P99->P99_COMPRA	:= (cAlsSC7)->C7_COMPRA
			P99->P99_COND	:= (cAlsSC7)->C7_COND
			P99->P99_CONTRA	:= (cAlsSC7)->C7_CONTRA
			P99->P99_DTSOL	:= StoD((cAlsSC1)->C1_EMISSAO)
			P99->P99_DTPED	:= Posicione("SCR",1,(cAlsSC7)->C7_FILIAL +"PC"+ (cAlsSC7)->C7_NUM,"CR_DATALIB")
			P99->P99_DTPREV	:= StoD((cAlsSC7)->C7_DATPRF)
			P99->P99_DTVSOL	:= StoD((cAlsSC1)->C1_DATPRF)
			P99->P99_DESCC 	:= Posicione("CTT",1,(cAlsSC7)->C7_FILIAL + (cAlsSC7)->C7_CC,"CTT_DESC01")
			P99->P99_DESCRI	:= (cAlsSC7)->C7_DESCRI
			P99->P99_DOC	:= (cAlsSD1)->F1_DOC
			P99->P99_DTDIGI	:= StoD((cAlsSD1)->F1_DTDIGIT)
			P99->P99_DTLANC	:= StoD((cAlsSD1)->F1_DTLANC)
			P99->P99_DTEMNF	:= StoD((cAlsSD1)->F1_EMISSAO)
			P99->P99_DTEMSC	:= StoD((cAlsSC1)->C1_EMISSAO)
			P99->P99_RSOCIA	:= Alltrim(SA2->A2_NOME)
			P99->P99_SUBGRP	:= Alltrim(SB1->B1_XGRPPRO)
			P99->P99_SETOR	:= (cAlsSC7)->C7_LOCAL
			P99->P99_FORNEC	:= (cAlsSC7)->C7_FORNECE
			P99->P99_NUMPC	:= (cAlsSC7)->C7_NUM
			P99->P99_SOLIC	:= (cAlsSC7)->C7_NUMSC
			P99->P99_ITEM	:= (cAlsSC7)->C7_ITEM
			P99->P99_PROD	:= (cAlsSC7)->C7_PRODUTO
			P99->P99_QTTOT	:= (cAlsSC7)->C7_QUANT
			P99->P99_SERIE	:= (cAlsSD1)->F1_SERIE
			P99->P99_COLICI	:= (cAlsSC1)->C1_SOLICIT
			P99->P99_TOTAL	:= (cAlsSC7)->C7_TOTAL
			P99->P99_MPVPP	:= Posicione("SC8",3,(cAlsSC7)->C7_FILIAL + (cAlsSC7)->C7_NUMCOT + (cAlsSC7)->C7_PRODUTO + (cAlsSC7)->C7_FORNECE + (cAlsSC7)->C7_LOJA + (cAlsSC7)->C7_NUM + (cAlsSC7)->C7_ITEM ,"C8_TOTAL")
			P99->P99_TPCOM	:= (cAlsSC1)->C1_TPSC
			P99->P99_USERLI	:= ALLTRIM(SCR->CR_USERLIB)
			P99->P99_CARACT	:= (cAlsSC7)->C7_XCARAC
			P99->P99_DTETT	:= (cAlsSD1)->D1_XDTENTR
			P99->P99_DTVNF	:= (cAlsSD1)->F1_XDTVNF
			P99->P99_EMERG	:= (cAlsSC7)->C7_XEMERG
			P99->P99_NEST	:= Posicione("SBM",1,(cAlsSC7)->C7_FILIAL + P99->P99_GRUPO,"BM_XESTOQ")
			P99->P99_LEADT	:= ALLTRIM(SB1->B1_XLEAD)
			P99->P99_PADRO	:= (cAlsSC7)->C7_XPADRON
			P99->P99_PEDBIO	:= (cAlsSC7)->C7_XPEDBIO
			P99->P99_TPPROC	:= (cAlsSC7)->C7_XTPROC
			P99->P99_CLASSE	:= Alltrim(SB1->B1_XSUBGRP)
			P99->P99_PENDEN	:= IIF(((cAlsSC7)->C7_QUANT  - (cAlsSC7)->C7_QUJE)>0,"1","2")
			P99->P99_FCOND	:= ALLTRIM(SA2->A2_COND)
			P99->P99_OBS	:= IIF(P99->P99_FCOND == (cAlsSC7)->C7_COND, "A condi��o de pagamento da NF confere com a OC","A condi��o de pagamento da NF n�o confere com a OC")
			P99->P99_QUJE	:= (cAlsSC7)->C7_QUJE
				
			P99->(MsUnlock())
			
			(cAlsSD1)->(dbCloseArea())
			(cAlsSC1)->(dbCloseArea())

			(cAlsSC7)->(dbSkip())
		
		EndDo

	Else
		cDesc := "N�o Existem registros a processar!"
		Conout(cDesc)
	EndIf

	(cAlsSC7)->(dbCloseArea())
	
Return

	
