#Include 'Protheus.ch'
#INCLUDE "APWEBEX.CH"
/*
{Protheus.doc} F0200318()
Insere incentivo academico
@Author     Henrique Madureira
@Since
@Version    P12.7
@Project    MAN00000463301_EF_003
@Return	 cHtml
*/
User Function F0200318()
	
	Local cHtml   := ""
	Local aAux    := {}
	Local oParam  := Nil
	Local oOrg    := Nil
	Local oSolic  := Nil
	
	cMsg := "Inconsistencia encontrada no cadastro da pessoa logada ou na amarra��o da vis�o."
	
	WEB EXTENDED INIT cHtml START "InSite"
	
	fGetInfRotina("U_F0200301.APW") //Verificar
	GetMat()							//Pega a Matricula e a filial do participante logado
	
	oOrg := WSORGSTRUCTURE():New()
	WsChgURL(@oOrg,"ORGSTRUCTURE.APW")
	
	oOrg:cParticipantID   := ""
	//oOrg:cTypeOrg         := "2"
	oOrg:cVision          := HttpSession->aInfRotina:cVisao
	oOrg:cEmployeeFil     := HttpSession->aUser[2]
	oOrg:cRegistration    := HttpSession->RhMat
	oOrg:cEmployeeSolFil  := HttpSession->aUser[2]
	oOrg:cRegistSolic     := HttpSession->RhMat
	If ValType(HttpSession->RHMat) != "U" .And. !Empty(HttpSession->RHMat)
		oOrg:cRegistration := HttpSession->RHMat
	EndIf
	If oOrg:GetStructure()
		
		oSolic := WSW0500308():New()
		WsChgURL(@oSolic,"W0500308.apw")
		
		cFilSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
		cMatSol  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
		cVOrg    := HttpSession->aInfRotina:cVisao
		cEmpFunc := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeEmp
		cDepto   := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cDepartment
		
		////cEmployeeFilial, Registration, Department,Visao  ,EmployeeEmp, Registration
		If oSolic:VerAprvSit(cFilSol,cMatSol,cDepto,cVOrg,cEmpFunc,"1")
			
			cFilSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cEmployeeFilial
			cMatSolic  := oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cRegistration
			cFilAprov  := oSolic:oWSVERAPRVSITRESULT:cFilAprov//oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cSupFilial
			cMatAprov  := oSolic:oWSVERAPRVSITRESULT:cMatAprov//oOrg:OWSGETSTRUCTURERESULT:oWSLISTOFEMPLOYEE:OWSDATAEMPLOYEE[1]:cSupRegistration
			
			cMat := HTTPSession->RHMat
			cNome := HttpPost->cNome
			
			cDtInicio := substr(HttpPost->cDtInicio,7,4) + substr(HttpPost->cDtInicio,4,2) + substr(HttpPost->cDtInicio,0,2)
			cDtFim := substr(HttpPost->cDtFim,7,4) + substr(HttpPost->cDtFim,4,2) + substr(HttpPost->cDtFim,0,2)
			
			oParam := Nil
			oParam := WSW0200301():new()
			WsChgURL(@oParam,"W0200301.APW")
			
			If !(EMPTY(cDtFim)) .AND. (STOD(cDtFim) - STOD(cDtInicio)) < 0
				cMsg := "Cadastro n�o Efetuado! A data Final � menor que a data inicial!"
			Else
				
				If oParam:InserePa3(	cNome,HttpPost->cLogin,HttpPost->cDepartamento1,HttpPost->cCenCusto1,;
						HttpPost->cSupFil,HttpPost->cCargo1,HttpPost->cPhone,HttpPost->cRamal,HttpPost->cContact,;
						HttpPost->cBenefitCode,HttpPost->cInstituteName,HttpPost->cCurseName,;
						HttpPost->cDtInicio,HttpPost->cDtFim,HttpPost->cDuracao,HttpPost->cMonthlyPayment,;
						HttpPost->cVlinvesanovig,HttpPost->cVltotcurso,HttpPost->cBenefconcedido,;
						HttpPost->txtobs,"3",cFilSolic,cMatSolic, cFilAprov,cMatAprov)
					
					If oParam:lINSEREPA3RESULT
						U_F0200302(4, cMatAprov,cNome,'','','',cFilAprov)
						cMsg := "Cadastro Efetuado com Sucesso!"
					Else
						cMsg := "Erro no Cadastro! N�o foi possivel fazer a inclus�o no banco!"
					EndIf
				Else
					cMsg := "Erro no cadastro! Verifique os dados informados!"
				EndIf
			EndIf
		Else
			cMsg := "Erro no cadastro! Erro no cadastro de vis�o."
		EndIf
	EndIf
	cHtml := ExecInPage("F0200301")
	WEB EXTENDED END
	
Return cHtml
