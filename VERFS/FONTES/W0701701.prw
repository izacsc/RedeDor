#include 'protheus.ch'
#include 'apwebsrv.ch'

/*/{Protheus.doc} W0701701
Server Simpro
@author izac.ciszevski
@since 19/01/2017
@Project MAN0000007423041_EF_017
/*/
user function W0701701()
return 

WSSTRUCT Tuss
    WSDATA cFilReg AS STRING
    WSDATA cCOD    AS STRING
    WSDATA cDESCR  AS STRING
    WSDATA cAPRES  AS STRING
    WSDATA cANVISA AS STRING
    WSDATA cREFCOD AS STRING
    WSDATA cREFER  AS STRING
    WSDATA cDTVIN  AS STRING
    WSDATA cDTVFIM AS STRING
    WSDATA cSTATUS AS STRING
ENDWSSTRUCT

WSSERVICE W0701701 DESCRIPTION "Responsavel pela inclus�o/altera��o de registros Tuss"
    
    WSDATA RegistroTuss AS Tuss

    WSDATA cRetorno AS STRING

    WSMETHOD UpsertTuss   DESCRIPTION "Realiza inclus�o/altera��o de registros Tuss"

ENDWSSERVICE 

WSMETHOD UpsertTuss WSRECEIVE RegistroTuss WSSEND cRetorno WSSERVICE W0701701
    BEGIN WSMETHOD
        ::cRetorno := U_F0701701(RegistroTuss)
    END WSMETHOD
Return .T.