#Include 'Protheus.ch'
#Include 'TopConn.ch'
#Include 'FWMVCDef.ch'

/*
{Protheus.doc} F0100901()
Relat�rio para Planejamento de Compras em Excel
@Author     Mick William da Silva
@Since      16/04/2016
@Version    P12.7
@Project    MAN00000462901_EF_009      
@Return     
*/

User Function F0100901()

	Local oReport
	Private nSalALM := "0"
	Private nSalFAR	:= "0"
	Private nSalARS	:= "0"
	Private nSArmExt	:= "0"
	Private cMod		:= ""
	Private cFornec	:= ""
	Private cFreq		:= ""
	Private cLeadTime	:= ""
	Private cEstSeg	:= ""
	Private cCompOC	:= ""
	Private cOrCO	:= ""
	Private cDtEmi	:= ""
	Private cForOC	:= ""
	Private cEnteg	:= ""
	Private cSolCo	:= ""
	Private nQTPE 	:= "0"
	Private nQUJE	:= "0"
	Private cCobALM	:= ""
	Private cCobFAR	:= ""
	Private cCobARS	:= ""
	Private cCobUni	:= ""
	Private cOCApr	:= "N�o"
	Private cGrupam	:= ""
	Private cFatMin	:= ""

	If TRepInUse()
		Pergunte("FSW0100901",.T.)
		IF Empty(Alltrim(MV_PAR03)) .And. MV_PAR05 <> 1
			MsgAlert("N�o foi possivel gerar o relat�rio. Informe o 'Produto' ou 'Produtos Controlados' igual a 'Sim'." )	
		Else
			oReport := ReportDef()
			oReport:PrintDialog()
		EndIf
	EndIf
Return

Static Function ReportDef()
	Local oReport
	Local oSection
	Local oBreak

	oReport := TReport():New("Relat�rio de Planejamento ","Relat�rio de Planejamento de Compras","FSW0100901",{|oReport| PrintReport(oReport)},"Relat�rio de Planejamento de Compras")
	oSection := TRSection():New(oReport,"Compras",{"QRYSB1","QRYSC1","QRYCDA","QCD07"})
	
	TRCell():New(oSection,"EstFil"		,	""			,"Estabelecimento"	,,,,{||(FWFilialName(,MV_PAR01))},,,,,,,,,.t.)
	TRCell():New(oSection,"B1_COD"		,"QRYSB1"		,"Material")
	TRCell():New(oSection,"B1_DESC"		,"QRYSB1"		,"Descri��o")
	TRCell():New(oSection,"ACV_CATEGO"	,"QRYSB1"		,"Categoria")
	TRCell():New(oSection,"DtEmissao"	,	""			,"Dt. Emiss�o"		,,,,{||((cDtEmi))},,,,,,,,,.t.)
	TRCell():New(oSection,"SolComp"		,	""			,"Solic. Compra"	,,,,{||(cSolCo)},,,,,,,,,.t.)
	TRCell():New(oSection,"OrdComp"		,	""			,"Ordem compra"		,,,,{||(cOrCO)},,,,,,,,,.t.)
	TRCell():New(oSection,"ForOC"		,	""			,"Fornecedor OC"	,,,,{||(cForOC)},,,,,,,,,.t.)
	TRCell():New(oSection,"OcAprov"		,	""			,"OC Aprovada"		,,,,{||(cOCApr)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdPed"		,	""			,"Qt. Pedido"		,,,,{||(nQTPE)},,,,,,,,,.t.)
	TRCell():New(oSection,"DtEnteg"		,	""			,"Dt. Entrega"		,,,,{||((cEnteg))},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdPenOC"	,	""			,"Qt. Pendente OC"	,,,,{||(nQTPE-nQUJE)},,,,,,,,,.t.)
	TRCell():New(oSection,"CompOc"		,	""			,"Comprador OC"		,,,,{||(cCompOC)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCDA"		,"QRYCDA"		,"CDA"				,,,,,,,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD07"		,"QCD07"		,"CD07"				,,,,{||((QCD07->QtdCD07)/7)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD15"		,"QCD15"		,"CD15"				,,,,{||((QCD15->QtdCD15)/15)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD30"		,"QCD30"		,"CD30"				,,,,{||((QCD30->QtdCD30)/30)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD60"		,"QCD60"		,"CD60"				,,,,{||((QCD60->QtdCD60)/60)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD90"		,"QCD90"		,"CD90"				,,,,{||((QCD90->QtdCD90)/90)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD120"	,"QCD120"		,"CD120"			,,,,{||((QCD120->QtdCD120)/120)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD150"	,"QCD150"		,"CD150"			,,,,{||((QCD150->QtdCD150)/150)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdCD180"	,"QCD180"		,"CD180"			,,,,{||((QCD180->QtdCD180)/180)},,,,,,,,,.t.)
	TRCell():New(oSection,"MaiorC3M"	,"QRYM3M"		,"Maior consumo 3M")
	TRCell():New(oSection,"SalAlm"		,	""			,"Saldo Almox."		 ,,,,{||(Val(nSalALM))},,,,,,,,,.t.) 
	TRCell():New(oSection,"SalFar"		,	""			,"Saldo Farm�cias"	 ,,,,{||(Val(nSalFAR))},,,,,,,,,.t.)
	TRCell():New(oSection,"SalArs"		,	""			,"Saldo Arsenais"	 ,,,,{||(Val(nSalARS))},,,,,,,,,.t.)
	TRCell():New(oSection,"TotUni"		,	""			,"Total unidade"	 ,,,,{||(Val(nSalALM) + Val(nSalARS) )},,,,,,,,,.t.)
	TRCell():New(oSection,"CobALM"		,	""			,"Cobertura Almox"	 ,,,,{||(cCobALM)},,,,,,,,,.t.)
	TRCell():New(oSection,"CobFAR"		,	""			,"Cobertura Farm�cia",,,,{||(cCobFAR)},,,,,,,,,.t.)
	TRCell():New(oSection,"CobARS"		,	""			,"Cobertura Arsenais",,,,{||(cCobARS)},,,,,,,,,.t.)
	TRCell():New(oSection,"CobUni"		,	""			,"Cobertura Unidade" ,,,,{||(cCobUni)},,,,,,,,,.t.)
	TRCell():New(oSection,"CusMed"		,"QRYCUM"		,"Custo m�dio"		 ,,,,{||((QRYCUM->Custo)/(QRYCUM->QtdMed))},,,,,,,,,.t.)
	TRCell():New(oSection,"EstTot"		,"QRYCUM"		,"Estoque total R$"	 ,,,,{||Round((Val(nSalALM)+Val(nSalARS)*(QRYCUM->Custo/QRYCUM->QtdMed)),2)},,,,,,,,,.t.)
	TRCell():New(oSection,"QtdPedAb"	,"QRYPEAB"		,"Ordens total")
	TRCell():New(oSection,"B1_UM"		,"QRYSB1"		,"Unidade compra")
	TRCell():New(oSection,"cLeadTime"		,""		,"Lead time",,,,{||(cLeadTime)},,,,,,,,,.t.)
	TRCell():New(oSection,"cFreq"	,""		,"Frequ�ncia",,,,{||(cFreq)},,,,,,,,,.t.)
	TRCell():New(oSection,"Polit"		,""		,"Politica"			,,,,{||((cEstSeg) + (cLeadTime) + (cFreq))},,,,,,,,,.t.)
	TRCell():New(oSection,"EstMed"		,""		,"Est. M�dio"		,,,,{||((cEstSeg) + (cFreq))},,,,,,,,,.t.)
	TRCell():New(oSection,"cEstSeg"	,""		,"Est. Seguran�a"	,,,,{||((cEstSeg))},,,,,,,,,.t.)
	TRCell():New(oSection,"B1_EMAX"		,"QRYSB1"		,"Est. M�ximo")
	TRCell():New(oSection,"","","Est. M�nimo")
	TRCell():New(oSection,"MatSub"		,"QRYSB1"		,"Materiais substitutos",,,,{||((QRYSB1->B1_ALTER)+" - "+(Posicione("SB1",1,xFilial("SB1")+QRYSB1->B1_ALTER,"B1_DESC")))},,,,,,,,,.t.)
	TRCell():New(oSection,"nSArmExt","","Saldo local externo",,,,{||(nSArmExt)},,,,,,,,,.t.)
	TRCell():New(oSection,"cMod","","Modalidade",,,,{||(cMod)},,,,,,,,,.t.)
	TRCell():New(oSection,"cFatMin","cFatMin","Valor faturamento m�nimo",,,,{||(cFatMin)},,,,,,,,,.t.)
	TRCell():New(oSection,"","","Fornecedor",,,,{||(cFornec)},,,,,,,,,.t.)
	TRCell():New(oSection,"B1_GRUPCOM","QRYSB1","Comprador")
	TRCell():New(oSection,"cGrupam","cGrupam","Grupamento",,,,{||(cGrupam)},,,,,,,,,.t.)

Return oReport

Static Function PrintReport(oReport)
	Local oSection := oReport:Section(1)
	Local cPartLocal, cFiltro   := ""
	Local cFil		:= MV_PAR01	
	Local nCDA , nCD07, nCD15, nCD30, nCD60, nCD90, nCD120, nCD150, nCD180 := 0
	Local aArea		:= GetArea()
	Local aAreaSA2	:= SA2->(GetArea())
	Local dDTBASE	:= MV_PAR02
	Local dDtA 		:= dDTBASE - 1
	Local dDt7 		:= dDTBASE - 7
	Local dDt15 	:= dDTBASE - 15
	Local dDt30 	:= dDTBASE - 30
	Local dDt60 	:= dDTBASE - 60
	Local dDt90 	:= dDTBASE - 90
	Local dDt120 	:= dDTBASE - 120
	Local dDt150 	:= dDTBASE - 150
	Local dDt180 	:= dDTBASE - 180
	Local cProd		:= Alltrim(MV_PAR03)
	Local cCat		:= Alltrim(MV_PAR04)
	Local cContro	:= MV_PAR05	
	Local cQuery	:= ""
	Local cFSALM	:= SuperGetMv("FS_ALM") 
	Local cFSFAR	:= SuperGetMv("FS_FAR")
	Local cFSARS	:= SuperGetMv("FS_ARS")
	Local cFSARMEX:= SuperGetMv("FS_ARMEXT")
	Local cFilSC7   := Iif(Empty(FWFilial("SC7")),xFilial("SC7"),cFil)   
	Local cFilSD3   := Iif(Empty(FWFilial("SD3")),xFilial("SD3"),cFil)
	Local cFilSB1   := Iif(Empty(FWFilial("SB1")),xFilial("SB1"),cFil)
	Local cFilSB2   := Iif(Empty(FWFilial("SB2")),xFilial("SB2"),cFil)
	Local cFilSC1   := Iif(Empty(FWFilial("SC1")),xFilial("SC1"),cFil)
	Local cFilSBZ   := Iif(Empty(FWFilial("SBZ")),xFilial("SBZ"),cFil)
	Local cFilACV   := Iif(Empty(FWFilial("ACV")),xFilial("ACV"),cFil)
	Local cFilACU   := Iif(Empty(FWFilial("ACU")),xFilial("ACU"),cFil)
	Local aProd		:= {}

	
	cQuery :=" SELECT DISTINCT C7_NUM AS NUMPCSC,C7_ITEM AS ITPCSC, 'SC7' AS TABELA, D3_COD,ACV_CATEGO "	
	cQuery +=" FROM "+ RETSQLNAME('SD3')+ " SD3   "	
	cQuery +=" INNER JOIN "+ RETSQLNAME('ACV')+ " ACV ON SD3.D3_COD = ACV.ACV_CODPRO "
	cQuery +=" INNER JOIN "+ RETSQLNAME('ACU')+ " ACU ON ACV.ACV_CATEGO = ACU.ACU_COD "
	cQuery +=" LEFT JOIN "+ RETSQLNAME('SC7')+ "  SC7 ON SD3.D3_COD = SC7.C7_PRODUTO AND SC7.C7_FILIAL='"+cFilSC7+"'  AND SC7.D_E_L_E_T_ = ' ' AND SC7.C7_QUJE < SC7.C7_QUANT AND SC7.C7_RESIDUO = ' ' And C7_ENCER <> 'E' "
	cQuery +=" WHERE D3_EMISSAO <= '"+ DtoS(dDTBASE) +"' "

	IF !Empty(cProd)
		cQuery +=" AND SD3.D3_COD='"+ cProd + "' "
		cQuery +=" AND ACU.ACU_CTRLP = '"+ CVALTOCHAR(cContro) +"' "
	Else
		cQuery +=" AND ACU.ACU_CTRLP = '1' "
	EndIf
	
	If !Empty(cCat)
		cQuery +=" AND ACU.ACU_COD ='" + cCat + "' "
	EndIF

	cQuery +=" AND SD3.D3_FILIAL='"+ cFilSD3 + "' "
	cQuery +=" AND ACV.ACV_FILIAL='"+ cFilACV + "' "
	cQuery +=" AND ACU.ACU_FILIAL='"+ cFilACU + "' "
	cQuery +=" AND SD3.D_E_L_E_T_=' ' AND ACV.D_E_L_E_T_=' '  AND ACU.D_E_L_E_T_=' ' AND ACU.ACU_MSBLQL='2' "

	cQuery +=" UNION "
	cQuery +=" SELECT DISTINCT C1_NUM AS NUMPCSC,C1_ITEM AS ITPCSC,'SC1' AS TABELA,D3_COD,ACV_CATEGO  "
	cQuery +=" FROM "+ RETSQLNAME('SD3')+ "  SD3   "
	cQuery +=" INNER JOIN "+ RETSQLNAME('ACV')+ "  ACV ON SD3.D3_COD = ACV.ACV_CODPRO   "
	cQuery +=" INNER JOIN "+ RETSQLNAME('ACU')+ "  ACU ON ACV.ACV_CATEGO = ACU.ACU_COD   "
	cQuery +=" INNER JOIN "+ RETSQLNAME('SC1')+ "  SC1 ON SC1.C1_PRODUTO = SD3.D3_COD AND SC1.C1_FILIAL='"+cFilSC1+"'  AND SC1.D_E_L_E_T_ = ' ' AND SC1.C1_PEDIDO = '      ' "
	cQuery +=" WHERE D3_EMISSAO <= '"+ DtoS(dDTBASE) +"'   "
	IF !Empty(cProd)
		cQuery +=" AND SD3.D3_COD='"+ cProd + "' "
		cQuery +=" AND ACU.ACU_CTRLP = '"+ CVALTOCHAR(cContro) +"' "
	Else
		cQuery +=" AND ACU.ACU_CTRLP = '1' "
	EndIf
	If !Empty(cCat)
		cQuery +=" AND ACU.ACU_COD ='" + cCat + "' "
	EndIF
	cQuery +=" AND SD3.D3_FILIAL='"+cFilSD3+"' "
	cQuery +=" AND ACV.ACV_FILIAL='"+cFilACV+"'  "
	cQuery +=" AND ACU.ACU_FILIAL='"+cFilACU+"'   "
	cQuery +=" AND SD3.D_E_L_E_T_=' '  "
	cQuery +=" AND ACV.D_E_L_E_T_=' '   "
	cQuery +=" AND ACU.D_E_L_E_T_=' '  "
	cQuery +=" AND ACU.ACU_MSBLQL='2' "
	cQuery +=" ORDER BY D3_COD,NUMPCSC "
	
	If Select("QRYPROD") > 0
		DbSelectArea("QRYPROD")
		DbCloseArea()
	Endif
	
	TCQUERY cQuery NEW ALIAS "QRYPROD"
	
	dbSelectArea("QRYPROD")
	QRYPROD->(dbGoTop())

	
	oSection:Init()
	While !QRYPROD->(EOF())
		
		If aScan(aProd,QRYPROD->D3_COD) == 0 
			Aadd(aProd,QRYPROD->D3_COD)
		Else
			If Empty(QRYPROD->NUMPCSC)
				QRYPROD->(dbSkip())
				Loop
			Endif		
		Endif
		
		cDtEmi	:= ""
		cSolCo	:= ""
		cOrCO	:= ""
		cForOC	:= ""
		nQTPE	:= 0
		nQUJE	:= 0
		cEnteg	:= 0	
		
		oReport:IncMeter()
		cProd := QRYPROD->D3_COD
		
		//Estabelecimento, Material, Descri��o e Segmento/Categoria.	
		cQuery :=" SELECT distinct (B1_COD),B1_FILIAL,B1_DESC,B1_UM,B1_ALTER,B1_EMAX,ACV_CATEGO,ACU_CODPAI,B1_GRUPCOM FROM "+ RETSQLNAME('SB1') +" SB1 "
		cQuery +=" LEFT JOIN "+ RETSQLNAME('SD3') +" SD3 ON SB1.B1_COD = SD3.D3_COD "
		cQuery +=" LEFT JOIN "+ RETSQLNAME('ACV') +" ACV ON SB1.B1_COD = ACV.ACV_CODPRO "
		cQuery +=" INNER JOIN "+ RETSQLNAME('ACU')+ " ACU ON ACV.ACV_CATEGO = ACU.ACU_COD "
		cQuery +=" WHERE SB1.B1_FILIAL ='" + cFilSB1 + "' AND SB1.B1_COD='" + cProd + "' "
		
		If Select("QRYSB1") > 0
			DbSelectArea("QRYSB1")
			DbCloseArea()
		Endif
		
		TCQUERY cQuery NEW ALIAS "QRYSB1"  
		
		If AllTrim(QRYPROD->TABELA) == 'SC7'
			SC7->(DbSetOrder(1))
			If SC7->(DbSeek(cFilSC7+QRYPROD->NUMPCSC+ QRYPROD->ITPCSC))
				IF !Empty(Alltrim(SC7->C7_XUSR))
					cCompOC := SC7->C7_XUSR
				ElseIF !Empty(Alltrim(SC7->C7_COMPRA))
					cCompOC := SC7->C7_COMPRA
				Else
					cCompOC := SC7->C7_USER
				EndIf
			
				IF Alltrim(SC7->C7_CONAPRO) == "L"
					cOCApr:= "Sim"
				Else
					cOCApr:= "N�o"
				EndIf
			
				If Empty(SC7->C7_MEDICAO) .And. Empty(SC7->C7_CONTRA)
					cMod 		:= "Mercado"
					cFornec	:= ""
				Else
					cMod := "Contrato"
					CN9->(DbSetOrder(1))
					If CN9->(DbSeek(FWXFilial("CN9")+SC7->C7_CONTRA+SC7->C7_CONTREV))
						cFatMin := Transform(CN9->CN9_XFATMI,"@E 9,999,999.99")
					EndIf
					SB1->(DbSeek(xFilial("SB1")+cProd))
					SA2->(DbSetOrder(1))
					If SA2->(DbSeek(xFilial("SA2")+SB1->B1_PROC+SB1->B1_LOJPROC))
						cFornec := SA2->A2_NREDUZ
					Else
						cFornec := ""
					Endif
				EndIf
			
				cDtEmi	:= SC7->C7_EMISSAO
				cSolCo	:= SC7->C7_NUMSC
				cOrCO	:= SC7->C7_NUM
				cForOC	:= SC7->C7_FORNECE
				nQTPE	:= SC7->C7_QUANT-SC7->C7_QUJE-SC7->C7_QTDACLA
				nQUJE	:= SC7->C7_QUJE
				cEnteg	:= SC7->C7_DATPRF
			EndIf
		Else
			SC1->(DbSetOrder(1))
			If SC1->(DbSeek(cFilSC1+QRYPROD->NUMPCSC+ QRYPROD->ITPCSC))
				cOCApr:= "N�o"
				cMod := "Contrato"
				cFornec := ""
				cCompOC := Posicione("SY1",1,xFilial("SY1")+SC1->C1_CODCOMP,"Y1_USER")
				cDtEmi	:= SC1->C1_EMISSAO
				cSolCo	:= SC1->C1_NUM
				cOrCO	:= ""
				cForOC	:= SC1->C1_FORNECE
				nQTPE	:= SC1->C1_QUANT
				cEnteg	:= SC1->C1_DATPRF
				nQUJE	:= SC1->C1_QUJE
			Endif
		Endif

		
		//CDA - CONSUMO DO DIA ANTERIOR.
		cQuery :="SELECT SUM(D3_QUANT) QtdCDA FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO ='" + dToS(dDtA) + "'  AND D_E_L_E_T_=' ' "
		
		If Select("QRYCDA") > 0
			DbSelectArea("QRYCDA")
			(QRYCDA)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QRYCDA"  
		
		//CD07 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 7 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD07 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt7) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD07") > 0
			DbSelectArea("QCD07")
			(QCD07)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD07"  
		
		//CD15 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 15 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD15 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt15) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD15") > 0
			DbSelectArea("QCD15")
			(QCD15)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD15"  
		
		//CD30 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 30 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD30 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt30) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD30") > 0
			DbSelectArea("QCD30")
			(QCD30)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD30"  
		
		//CD60 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 60 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD60 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt60) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD60") > 0
			DbSelectArea("QCD60")
			(QCD60)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD60"  
		
		//CD90 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 90 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD90 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt90) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD90") > 0
			DbSelectArea("QCD90")
			(QCD90)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD90"  
		
		//CD120 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 120 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD120 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt120) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD120") > 0
			DbSelectArea("QCD120")
			(QCD120)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD120"
		
		//CD150 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 150 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD150 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt150) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD150") > 0
			DbSelectArea("QCD150")
			(QCD150)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD150"    
		
		//CD180 - M�DIA DE CONSUMO DI�RIO DOS �LTIMOS 180 DIAS.
		cQuery :="SELECT SUM(D3_QUANT) QtdCD180 FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt180) + "' AND '"+ dtoS(dDTBASE) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QCD180") > 0
			DbSelectArea("QCD180")
			(QCD180)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QCD180"  
		
		//Maior consumo 3M - MAIOR VALOR APRESENTADO NOS CD'S  APURADOS.
		cQuery :="SELECT MAX(D3_QUANT) MaiorC3M FROM "+ RETSQLNAME('SD3')
		cQuery +=" WHERE D3_FILIAL ='"+ cFilSD3 + "' And D3_COD = '"+ cProd + "' And D3_EMISSAO BETWEEN '" + dToS(dDt90) + "' AND '"+ dtoS(dDtA) +"' AND D_E_L_E_T_=' ' "
		
		If Select("QRYM3M") > 0
			DbSelectArea("QRYM3M")
			(QRYM3M)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QRYM3M"  
		
		//Custo m�dio - CUSTO M�DIO UNIT�RIO DO ITEM.
		cQuery :="SELECT SUM(B2_QATU) QtdMed ,SUM(B2_VATU1) Custo FROM "+ RETSQLNAME('SB2')
		cQuery +=" WHERE B2_FILIAL ='"+ cFilSB2 + "' And B2_COD = '"+ cProd + "' AND D_E_L_E_T_=' ' "
		
		If Select("QRYCUM") > 0
			DbSelectArea("QRYCUM")
			(QRYCUM)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QRYCUM"  
		
		//Ordens total
		cQuery :="SELECT SUM(C7_QUANT) QtdPedAb FROM "+ RETSQLNAME('SC7')
		cQuery +=" WHERE C7_RESIDUO = ' ' And C7_ENCER <> 'E' And C7_FILIAL='"+ cFilSC7 + "' And C7_PRODUTO='"+ cProd + "' AND D_E_L_E_T_=' ' " 
		
		If Select("QRYPEAB") > 0
			DbSelectArea("QRYPEAB")
			(QRYPEAB)->(DbCloseArea())
		Endif
		
		TCQUERY cQuery NEW ALIAS "QRYPEAB"
		
		//Lead Time, Frequ�ncia e Estoque de Seguran�a
		cQuery := "SELECT BZ_PE, BZ_ESTSEG, BZ_XFREQAN FROM "+ RETSQLNAME('SBZ')
		cQuery += " WHERE BZ_FILIAL ='"+ cFilSBZ +"' AND BZ_COD='"+ cProd +"' AND D_E_L_E_T_=' ' "
		
		If Select("QRYSBZ")>0
			DbSelectArea("QRYSBZ")
			(QRYSBZ)->(DbCloseArea())
		EndIf
		
		TCQuery cQuery NEW ALIAS "QRYSBZ"
		
		IF !QRYSBZ->(EOF())
			cFreq		:= QRYSBZ->BZ_XFREQAN
			cLeadTime	:= QRYSBZ->BZ_PE
			cEstSeg	:= QRYSBZ->BZ_ESTSEG
		Else
			//Lead Time, Frequ�ncia e Estoque de Seguran�a
			cQuery := "SELECT ACU_XESTSE,ACU_XPE,ACU_XFREQ  FROM "+ RETSQLNAME('ACU') + " ACU " 
			cQuery +=" INNER JOIN "+ RETSQLNAME('ACV')+ " ACV ON ACU.ACU_COD = ACV.ACV_CATEGO "
			cQuery += " WHERE ACU_FILIAL ='"+ cFilACU +"' AND ACV.ACV_CODPRO='"+ cProd +"' AND ACU.D_E_L_E_T_=' ' "
			 
			If Select("QRYACU")>0
				DbSelectArea("QRYACU")
				(QRYACU)->(DbCloseArea())
			EndIf
			
			TCQuery cQuery NEW ALIAS "QRYACU"	
			If !QRYACU->(EOF())		
				cFreq		:= QRYACU->ACU_XFREQ 
				cLeadTime	:= QRYACU->ACU_XPE
				cEstSeg	:= QRYACU->ACU_XESTSE		
			Endif
		Endif
		//Saldo do Armazem do tipo Almoxarifado.
		dbSelectArea("SB2")
		SB2->(DBSetOrder(1))
		
		IF SB2->( dbSeek(xFilial("SB2") + cProd + cFSALM) )
			nSalALM := cValToChar(SaldoSb2())//Transform(SaldoSb2(),"@E 9,999,999.99")
		EndIf
		
		//Saldo do Armazem do tipo Farm�cia.
		IF SB2->( dbSeek(xFilial("SB2") + cProd + cFSFAR) )
			nSalFAR := cValToChar(SaldoSb2())//Transform(SaldoSb2(),"@E 9,999,999.99")
		EndIf
		
		//Saldo do Armazem do tipo Arsenal.
		IF SB2->( dbSeek(xFilial("SB2") + cProd + cFSARS) )
			nSalARS := cValToChar(SaldoSb2())//Transform(SaldoSb2(),"@E 9,999,999.99")
		EndIf
		
		//Saldo do Armazem Externo
		IF SB2->( dbSeek(xFilial("SB2") + cProd + cFSARMEX) )
			nSArmExt := cValToChar(SaldoSb2())//Transform(SaldoSb2(),"@E 9,999,999.99")
		Endif
				
		//Coberturas - Almox, Farm�cia
		IF ( (QCD07->QtdCD07) + (QCD30->QtdCD30) ) == 0
			cCobALM	:= "Sem Consumo"
			cCobFAR	:= "Sem Consumo"
			cCobARS	:= "Sem Consumo"
			cCobUni	:= "Sem Consumo"
		Else
			IF (QCD07->QtdCD07) == 0
				cCobALM := ( val(nSalALM) / (QCD30->QtdCD30) )
				cCobFAR := ( val(nSalFAR) / (QCD30->QtdCD30) )
				cCobARS := ( val(nSalARS) / (QCD30->QtdCD30) )
				cCobUni	:= ( (Val(nSalALM) + Val(nSalARS) ) / (QCD30->QtdCD30) )
			Else
				cCobALM := ( val(nSalALM) / (QCD07->QtdCD07) )
				cCobFAR := ( val(nSalFAR) / (QCD07->QtdCD07) )
				cCobARS := ( val(nSalARS) / (QCD07->QtdCD07) )
				cCobUni	:= ( (Val(nSalALM) + Val(nSalARS) ) / (QCD07->QtdCD07) )
			EndIf
		EndIf
		
		ACU->(DbSetOrder(1))
		If ACU->(DbSeek(FWXfilial("ACU") + QRYSB1->ACU_CODPAI))
			cGrupam := ACU->ACU_DESC
		Endif
		/*---- Finaliza a linha----*/	
		oSection:PrintLine()
		
		If Select("SB2") > 0
			DbSelectArea("SB2")
			SB2->(DbCloseArea())
		Endif
		If Select("QRYCOMP") > 0
			DbSelectArea("QRYCOMP")
			QRYCOMP->(DbCloseArea())
		Endif
		If Select("QRYSC1") > 0
			DbSelectArea("QRYSC1")
			QRYSC1->(DbCloseArea())
		Endif
		If Select("QRYCDA") > 0
			DbSelectArea("QRYCDA")
			QRYCDA->(DbCloseArea())
		Endif
		If Select("QCD07") > 0
			DbSelectArea("QCD07")
			QCD07->(DbCloseArea())
		Endif
		If Select("QCD15") > 0
			DbSelectArea("QCD15")
			QCD15->(DbCloseArea())
		Endif
		If Select("QCD30") > 0
			DbSelectArea("QCD30")
			QCD30->(DbCloseArea())
		Endif
		If Select("QCD60") > 0
			DbSelectArea("QCD60")
			QCD60->(DbCloseArea())
		Endif
		If Select("QCD90") > 0
			DbSelectArea("QCD90")
			QCD90->(DbCloseArea())
		Endif
		If Select("QCD120") > 0
			DbSelectArea("QCD120")
			QCD120->(DbCloseArea())
		Endif
		If Select("QCD150") > 0
			DbSelectArea("QCD150")
			QCD150->(DbCloseArea())
		Endif
		If Select("QCD180") > 0
			DbSelectArea("QCD180")
			QCD180->(DbCloseArea())
		Endif
		If Select("QRYM3M") > 0
			DbSelectArea("QRYM3M")
			QRYM3M->(DbCloseArea())
		Endif
		If Select("QRYCUM") > 0
			DbSelectArea("QRYCUM")
			QRYCUM->(DbCloseArea())
		Endif
		If Select("QRYPEAB") > 0
			DbSelectArea("QRYPEAB")
			QRYPEAB->(DbCloseArea())
		Endif
		If Select("QRYSBZ") > 0
			DbSelectArea("QRYSBZ")
			QRYSBZ->(DbCloseArea())
		Endif
		If Select("QRYACU") > 0
			DbSelectArea("QRYACU")
			QRYACU->(DbCloseArea())
		Endif

		QRYPROD->(dbSkip())
	EndDo	
	
	If Select("QRYPROD") > 0
		DbSelectArea("QRYPROD")
		QRYPROD->(DbCloseArea())
	Endif

	oSection:Finish()
	RestArea(aArea)
	RestArea(aAreaSA2)
		
Return

