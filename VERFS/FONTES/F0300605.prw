#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#include 'FILEIO.ch'

/*
{Protheus.doc} F0300605()
Exporta��o de Exclus�o de Colaborador
@Author     Rogerio Candisani
@Since      31/10/2016
@Version    P12.7
@Project    MAN00000463701_EF_006
@Return
*/
User Function F0300605()

Local ctmpExc
Local ctmpDep 
Local montaTxt:= ""
Local cPerg:= "FSW0300605"	// Gp. perguntas especifico
Local lRet:= .F.
Local lgerou:=.f.
//exportar os dados de exclus�o do colaborador
//criar pergunta F0300605

/*	Exporta��o de Exclus�o de Colaborador
MV_PAR01 Filial De
MV_PAR02 Filial At�
MV_PAR03 Demissao De
MV_PAR04 Demissao At�
MV_PAR05 Tipo de Arquivo (M�dico / Odontol�gico)
MV_PAR06 Caminho de Grava��o
*/

//	Verificar se o campo RG_DATADEM possui conte�do
//	Verificar na tabela RHK se o colaborador demitido possui um plano ativo.
//	Ser� inclu�do no arquivo somente se satisfeita as condi��es acima.
//	Dever� gerar todas as colunas, ainda que algumas sem conte�do.

// As Colunas 2, 3, 4, 6 e 13 - Informar somente a coluna, o dado ser� vazio;
// As Colunas que foram indicadas para serem geradas �em branco� ter�o dados informados manualmente pela equipe de benef�cios;
//	A Coluna 10 dever� ser preenchida com a data da demiss�o;
//	Os tamanhos das colunas s�o livres, e dever�o ser separados por �; �.

Pergunte(cPerg,.T.)

If (MsgYesNo(OemToAnsi("Confirma Exporta��o de Exclus�o de Colaborador ?"),OemToAnsi("Atencao")))
	lRet:= .T.
Else
	lRet:= .F.
Endif

If lRet
	//montar query de exclus�o do colaborador
	ctmpExc:=GetNextAlias()
	BeginSql Alias ctmpExc
		SELECT RG_FILIAL, RG_MAT, RG_DATADEM   
		FROM %table:SRG% SRG
		Inner join %table:RHK% RHK ON 
			RHK.RHK_MAT = SRG.RG_MAT AND
			RHK.RHK_TPFORN = %Exp:MV_PAR05% AND
			RHK.%NotDel%
		WHERE SRG.RG_FILIAL BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR02%
			AND SRG.RG_DATADEM BETWEEN %Exp:MV_PAR03% AND %Exp:MV_PAR04%
			AND SRG.%NotDel%
		ORDER BY %Order:SRG%
	EndSql	
	
	DbSelectArea(ctmpExc)
	(ctmpExc)->(DbGoTop())
	cMontaTxt:= ""
	While ! (ctmpExc)->(EOF())
		lgerou:=.t.
		//monta o txt do titular
		cMontaTxt += "Titular" + ";" // verificar RHK_PERFIM ou RHL_PERFIM 
		cMontaTxt += ";" // 2 - branco
		cMontaTxt += ";" // 3 - branco
		cMontaTxt += ";" // 4 - branco
		cMontaTxt += (ctmpExc)->RG_MAT + ";" // 5 - matricula do titular
		cMontaTxt += ";" // 6 - branco
		cMontaTxt += ";" // 7
		cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(ctmpExc)->RG_MAT,"RA_NOME") + ";" // 8 - nome do titular 
		cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(ctmpExc)->RG_MAT,"RA_CIC") + ";" // 9 - CPF do titular
		cMontaTxt += subst((ctmpExc)->RG_DATADEM,7,2) + "/" + subst((ctmpExc)->RG_DATADEM,5,2) + "/" + subst((ctmpExc)->RG_DATADEM,1,4) + ";" // 10 Data demissao , Informar a data DD/MM/AAAA
		cMontaTxt += ";" // 11 - nome do dependente ?
		cMontaTxt += ";" // 12 - data de cancelamento ?
		cMontaTxt += ";" // 13
		cMontaTxt += CHR(13)+ CHR(10) 
		(ctmpExc)->(dBSkip())
	Enddo
	
	//gera dados dos dependentes de cada titular excluido
	DbSelectArea(ctmpExc)
	(ctmpExc)->(DbGoTop())
	While !(ctmpExc)->(EOF())  
			//montar query do dependente
		ctmpDep:=GetNextAlias()
			BeginSql Alias ctmpDep
				SELECT RHL_MAT, RHL_PERFIM       
				FROM %table:RHL% RHL
				WHERE RHL.RHL_FILIAL = %xFilial:RHL%
					AND RHL.RHL_MAT = %Exp:(ctmpExc)->RG_MAT%
					AND RHL.RHL_TPFORN = %Exp:MV_PAR05%
					AND RHL.%NotDel%
			EndSql	
			DbSelectArea(ctmpDep)
			(ctmpDep)->(DbGoTop())
			While !(ctmpDep)->(EOF())
				//monta o txt dos dependentes a serem excluidos
				cMontaTxt += "Dependente" + ";" // verificar RHL_PERFIM 
				cMontaTxt += ";" // 2 - branco
				cMontaTxt += ";" // 3 - branco
				cMontaTxt += ";" // 4 - branco
				cMontaTxt += (ctmpDep)->RHL_MAT + ";" // 5 - matricula do titular
				cMontaTxt += ";" // 6 - branco
				cMontaTxt += ";" // 7 // campo RHL_XCARTE ?
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(ctmpDep)->RHL_MAT,"RA_NOME")+";"  // 8 - nome do titular 
				cMontaTxt += Posicione("SRA",1,xFilial("SRA")+(ctmpDep)->RHL_MAT,"RA_CIC")+";"  // 9 - CPF do titular
				cMontaTxt += subst((ctmpExc)->RG_DATADEM,7,2) + "/" + subst((ctmpExc)->RG_DATADEM,5,2) + "/" + subst((ctmpExc)->RG_DATADEM,1,4) + ";" // 10 Data demissao , Informar a data DD/MM/AAAA
				cMontaTxt += Posicione("SRB",1,xFilial("SRB")+(ctmpDep)->RHL_MAT,"RB_NOME")+";"  // 11 - nome do dependente
				cMontaTxt += subst((ctmpDep)->RHL_PERFIM,7,2) + "/" + subst((ctmpDep)->RHL_PERFIM,5,2) + "/" + subst((ctmpDep)->RHL_PERFIM,1,4) + ";" // 12 - data de cancelamento
				cMontaTxt += ";" // 13
				cMontaTxt += CHR(13)+ CHR(10) 
				(ctmpDep)->(dBSkip())
			Enddo
		DbSelectArea(ctmpExc)
		(ctmpExc)->(DbSkip())
	Enddo
	
	//fechar arquivos temporarios
	(ctmpExc)->(DbCloseArea())
	//(tmpDep)->(DbCloseArea())
	
	//gerar o arquivo
	If lgerou
		criaCSV()
	Else
		MsgAlert("N�o existem dados a serem gerados, verifique os parametros utilizados")
	Endif 

Endif

Return lRet

///////////////////////////////////////////////////////////////////////
// Exportando dados para planilha 
////////////////////////////////////////////////////////////////////////
Static Function criaCSV()

// Nome do arquivo criado, o nome � composto por umam descri��o 
//a data e a hora da cria��o, para que n�o existam nomes iguais
cNomeArq := alltrim(MV_PAR06) + ".csv"

// criar arquivo texto vazio a partir do root path no servidor
nHandle := FCREATE(cNomeArq)

FWrite(nHandle,cMontaTxt)

// encerra grava��o no arquivo
FClose(nHandle)

FOPEN(cNomeArq, FO_READWRITE)

//MsgAlert("Relatorio salvo em:" + cNomeArq )

Return