#Include "Protheus.ch"
#INCLUDE "APWEBEX.CH"

/*
{Protheus.doc} F0100310()
Busca as informações da solicitação de vaga
@Author     Bruno de Oliveira, Henrique Madureira
@Since      07/10/2016
@Version    P12.1.07
@Project    MAN00000462901_EF_003
@Return 	cHtml, página html
*/
User Function F0100310()

	Local cHtml := ""
	Local oSol  := Nil
	
	Private cMsg
	
	WEB EXTENDED INIT cHtml START "InSite"
	
		cFililS := httpget->cFilil
	    cCodSol := httpget->cSolic
	    cMatric := HttpSession->RHMat
	    cVisPad := HttpSession->aInfRotina:cVisao
	    nOperac := httpget->nGrid
	    cTVaga  := ""
	    
	  	oSol := WSW0500308():New()
		WsChgURL(@oSol,"W0500308.APW")            
	
		oSol:BscSolVags(cCodSol,cMatric,cVisPad,cFililS)
		If oSol:oWSBscSolVagsRESULT:cRet == ".T."
			cFVaga   := oSol:oWSBscSolVagsRESULT:cFilVag
			cTVaga   := IIF(oSol:oWSBscSolVagsRESULT:cVAGA == "1","Aumento de Quadro","Substituição")
			cCdDept  := oSol:oWSBscSolVagsRESULT:cDepto
			cCdPost  := oSol:oWSBscSolVagsRESULT:cPosto
			cFPost   := oSol:oWSBscSolVagsRESULT:cFlPost
			cCdCC    := oSol:oWSBscSolVagsRESULT:cCodCC
			cDESCRCC := oSol:oWSBscSolVagsRESULT:cDescCc
			cCdFun   := oSol:oWSBscSolVagsRESULT:cFuncao
			CDESCRFC := oSol:oWSBscSolVagsRESULT:cDescFu
			cNSolic  := oSol:oWSBscSolVagsRESULT:cNomeSol
			cPrzVg   := oSol:oWSBscSolVagsRESULT:cPrazo
			cDtAbt   := oSol:oWSBscSolVagsRESULT:cDtAbt
			cDtFch   := oSol:oWSBscSolVagsRESULT:cDtFch
			cCustV   := oSol:oWSBscSolVagsRESULT:cCustVg
			cPrSel   := oSol:oWSBscSolVagsRESULT:cProcS
			cTpVg    := IIF(oSol:oWSBscSolVagsRESULT:cTpVaga == "1","Int./Ext.",IIF(oSol:oWSBscSolVagsRESULT:cTpVaga == "2","Interna","Externa"))
			cJornTb  := oSol:oWSBscSolVagsRESULT:cHorMes
			cObservacao   := ALLTRIM(oSol:oWSBscSolVagsRESULT:cXObs)
			cDscDetal   := ALLTRIM(oSol:oWSBscSolVagsRESULT:cObserDet)	
			cCODCARGO  := oSol:oWSBscSolVagsRESULT:cCODCARGO
			cDESCRCARGO  := oSol:oWSBscSolVagsRESULT:cDESCRCARGO
		EndIf
		
		HttpCTType("text/html; charset=ISO-8859-1")		
		cHtml := ExecInPage( "F0100310" )
		
	WEB EXTENDED END

Return cHtml