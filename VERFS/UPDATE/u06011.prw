/*
{Protheus.doc} U06011
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 23/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423040_EF_006 CLIENTE REDEDOR
@Project MAN0000007423040_EF_004
@Obs Fontes: F0600603.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U06011(lOnlyInfo)
Local aInfo := {'06','011','WEBSERVICE RESCISAO APDATA','23/01/17','15:00','001730162010100156U1120','08/02/17','15:09',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX6,{'','FS_URLAPDT','C','URL do WS de Rescisao do APDATA','URL do WS de Rescisao do APDATA','URL do WS de Rescisao do APDATA','','','','','','','http://10.25.134.11:7800/conectador/InformarRescisaoProtheus12SA?WSDL','http://10.25.134.11:7800/conectador/InformarRescisaoProtheus12SA?WSDL','http://10.25.134.11:7800/conectador/InformarRescisaoProtheus12SA?WSDL','U','','','','','','','2017012314:59:27'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
