/*
{Protheus.doc} U01004
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 29/08/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_004
@Project NΓO IDENTIFICADO
@Obs Fontes: F0100401.PRW,F0100402.PRW,F0100403.PRW,F0100404.PRW,F0100405.PR
@Obs Fontes: F0100406.PRW,F0100408.PRW,PE_MA103OPC.PRW,PE_MT094END.PRW,PE_MT120COR.PR
@Obs Fontes: PE_MT120LEG.PRW,PE_MT120PCOK.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U01004(lOnlyInfo)
Local aInfo := {'01','004','SOLICITACAO DE PAGAMENTO','29/08/16','10:20','004690012010400801U0122','29/12/16','14:27',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P02','1','P02_FILIAL+P02_COD+P02_DESC+P02_PREF','Cod. Tipo+Desc Tipo+Prefixo','Cod. Tipo+Desc Tipo+Prefixo','Cod. Tipo+Desc Tipo+Prefixo','U','','','S',''})
aAdd(aSIX,{'SC7','N','C7_FILIAL + C7_XPREF + C7_XDOC','Prefixo + Num. NF','Prefixo + Num. NF','Prefixo + Num. NF','U','','','S',''})
aAdd(aSX2,{'P02','','P02010','TIPO DE DESPESAS','TIPO DE DESPESAS','TIPO DE DESPESAS','','E','E','E',0,'','','',0,'','','','','','',0,0,0,''})
aAdd(aSX3,{'P02','01','P02_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','A','R','','','','','','','','','033','','','','','','N','N','','','',''})
aAdd(aSX3,{'P02','02','P02_COD','C',2,0,'Cod. Tipo','Cod. Tipo','Cod. Tipo','Codigo de Tipo','Codigo de Tipo','Codigo de Tipo','@!','',' ','GETSXENUM("P02","P02_COD")','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'P02','03','P02_DESC','C',220,0,'Desc Tipo','Desc Tipo','Desc Tipo','Descriηγo do Tipo','Descriηγo do Tipo','Descriηγo do Tipo','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'P02','04','P02_PREF','C',3,0,'Prefixo','Prefixo','Prefixo','Prefixo do Tipo','Prefixo do Tipo','Prefixo do Tipo','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'P02','05','P02_COMPRA','C',6,0,'Comprador','Comprador','Comprador','Codigo do Comprador','Codigo do Comprador','Codigo do Comprador','@!','',' ','','SY1',0,'ώΐ','','','U','S','A','R','','ExistCpo("SY1",M->P02_COMPRA)','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H1','C7_XNATURE','C',10,0,'Natureza Fin','Natureza Fin','Natureza Fin','Natureza Financeira','Natureza Financeira','Natureza Financeira','@!','',' ','','SED',0,'ώΐ','','','U','N','A','R','','FinVldNat(.T.)','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H1','C7_XESPECI','C',6,0,'Especie Doc','Especie Doc','Especie Doc','Especie do Documento','Especie do Documento','Especie do Documento','@!','',' ','','42',0,'ώΐ','','','U','N','A','R','','ExistCpo("SX5", "42"+M->C7_XESPECI)','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H1','C7_XDOC','C',9,0,'Numero da NF','Numero da NF','Numero da NF','Numero da Nota Fiscal','Numero da Nota Fiscal','Numero da Nota Fiscal','@!','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H1','C7_XSERIE','C',3,0,'Serie da NF','Serie da NF','Serie da NF','Serie da Nota Fiscal','Serie da Nota Fiscal','Serie da Nota Fiscal','@!','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H1','C7_XDTEMI','D',8,0,'Dt Emis NF','Dt Emis NF','Dt Emis NF','Data de Emissao da NF','Data de Emissao da NF','Data de Emissao da NF','','',' ','','',0,'ώΐ','','','U','N','A','R','','M->C7_XDTEMI <= DDATABASE','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H3','C7_XDTVEN','D',8,0,'Dt.Prim.Venc','Dt.Prim.Venc','Dt.Prim.Venc','Data Primeiro Vencimento','Data Primeiro Vencimento','Data Primeiro Vencimento','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H4','C7_XTIPO','C',2,0,'Tipo Pag.','Tipo Pag.','Tipo Pag.','Tipo Pagamento','Tipo Pagamento','Tipo Pagamento','@!','ExistCpo("P02")',' ','','P02',0,'ώΐ','','S','U','S','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H5','C7_XPREF','C',3,0,'Prefixo','Prefixo','Prefixo','Prefixo','Prefixo','Prefixo','@!','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H6','C7_XUSR','C',6,0,'Emitente','Emitente','Emitente','Emitente','Emitente','Emitente','@!','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H7','C7_XSTATUS','C',1,0,'Status Reg.','Status Reg.','Status Reg.','Status do Registro','Status do Registro','Status do Registro','@!','',' ','','',0,'ώΐ','','','U','N','A','R','','Pertence(" 12345")','1=Sol.Atend.;2=Aguard.Aprov.;3=Aprov. Recusada;4=Bloq. Orηamento;5=Recusa Contas a Pagar','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SC7','H8','C7_XSOLPAG','C',1,0,'Solic.Pagam.','Solic.Pagam.','Solic.Pagam.','Solicitaηγo de Pagamento','Solicitaηγo de Pagamento','Solicitaηγo de Pagamento','@!','',' ','"2"','',0,'ώΐ','','','U','N','V','R','','Pertence("12")','1=Sim;2=Nao','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SD1','R1','D1_XNATURE','C',10,0,'Natureza Fin','Natureza Fin','Natureza Fin','Natureza Financeira','Natureza Financeira','Natureza Financeira','@!','',' ','','',0,'ώΐ','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SD1','R2','D1_XPRIVEN','D',8,0,'Dt.Prim.Venc','Dt.Prim.Venc','Dt.Prim.Venc','Data Primeiro Vencimento','Data Primeiro Vencimento','Data Primeiro Vencimento','','',' ','','',0,'ώΐ','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SF1','H2','F1_XUSR','C',30,0,'Emitente','Emitente','Emitente','Emitente','Emitente','Emitente','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','',''})
aAdd(aSX6,{'','FS_CONDPAG','C','Condiηγo de pagamento para a inclusγo do Pedido de','','','Compras','','','','','','001','001','001','U','','','','','','',''})
aAdd(aSXB,{'FSSB1','1','01','DB','Produto','Produto','Produto','SB1','',''})
aAdd(aSXB,{'FSSB1','2','01','01','Codigo','Codigo','Product','','',''})
aAdd(aSXB,{'FSSB1','2','02','02','Tipo + Codigo','Tipo + Codigo','Type + Product','','',''})
aAdd(aSXB,{'FSSB1','3','01','01','Cadastra Novo','Incluye Nuevo','Add New','01','',''})
aAdd(aSXB,{'FSSB1','4','01','01','Codigo','Codigo','Product','B1_COD','',''})
aAdd(aSXB,{'FSSB1','4','01','02','Descricao','Descripcion','Description','B1_DESC','',''})
aAdd(aSXB,{'FSSB1','4','01','03','Tipo','Tipo','Type','B1_TIPO','',''})
aAdd(aSXB,{'FSSB1','4','02','01','Codigo','Codigo','Product','B1_COD','',''})
aAdd(aSXB,{'FSSB1','4','02','03','Tipo','Tipo','Type','B1_TIPO','',''})
aAdd(aSXB,{'FSSB1','5','01','','','','','SB1->B1_COD','',''})
aAdd(aSXB,{'FSSB1','5','02','','','','','SB1->B1_DESC','',''})
aAdd(aSXB,{'FSSE4','1','01','DB','Cond. de Pagamento','Cond. de Pagamento','Cond. de Pagamento','SE4','',''})
aAdd(aSXB,{'FSSE4','2','01','01','Codigo','Codigo','Code','','',''})
aAdd(aSXB,{'FSSE4','3','01','01','Cadastra Novo','Incluye Nuevo','Add New','01','',''})
aAdd(aSXB,{'FSSE4','4','01','01','Codigo','Codigo','Code','E4_CODIGO','',''})
aAdd(aSXB,{'FSSE4','4','01','02','Descricao','Descripcion','Description','E4_DESCRI','',''})
aAdd(aSXB,{'FSSE4','4','01','03','Tipo','Tipo','Type','E4_TIPO','',''})
aAdd(aSXB,{'FSSE4','5','01','','','','','SE4->E4_CODIGO','',''})
aAdd(aSXB,{'FSSE4','5','02','','','','','SE4->E4_DESCRI','',''})
aAdd(aSXB,{'FSSED','1','01','DB','Natureza','Natureza','Natureza','SED','',''})
aAdd(aSXB,{'FSSED','2','01','01','Codigo','Codigo','Code','','',''})
aAdd(aSXB,{'FSSED','3','01','01','Cadastra Novo','Incluye Nuevo','Add New','01','',''})
aAdd(aSXB,{'FSSED','4','01','01','Codigo','Codigo','Code','ED_CODIGO','',''})
aAdd(aSXB,{'FSSED','4','01','02','Descricao','Descripcion','Description','ED_DESCRIC','',''})
aAdd(aSXB,{'FSSED','5','01','','','','','SED->ED_CODIGO','',''})
aAdd(aSXB,{'FSSED','5','02','','','','','SED->ED_DESCRIC','',''})
aAdd(aSXB,{'FSWP02','1','01','DB','Tipo de Despesas','Tipo de Despesas','Tipo de Despesas','P02','',''})
aAdd(aSXB,{'FSWP02','2','01','01','Cod. Tipo+desc Tipo+','Cod. Tipo+desc Tipo+','Cod. Tipo+desc Tipo+','','',''})
aAdd(aSXB,{'FSWP02','3','01','01','Cadastra Novo','Incluye Nuevo','Add New','01','',''})
aAdd(aSXB,{'FSWP02','4','01','01','Cod. Tipo','Cod. Tipo','Cod. Tipo','P02_COD','',''})
aAdd(aSXB,{'FSWP02','4','01','02','Desc Tipo','Desc Tipo','Desc Tipo','P02_DESC','',''})
aAdd(aSXB,{'FSWP02','4','01','03','Prefixo','Prefixo','Prefixo','P02_PREF','',''})
aAdd(aSXB,{'FSWP02','5','01','','','','','P02->P02_COD','',''})
aAdd(aSX3Hlp,{'P02_COMPRA','Codigo do Comprador'})
aAdd(aSX3Hlp,{'C7_XNATURE','Natureza Financeira'})
aAdd(aSX3Hlp,{'C7_XESPECI','Especie do Documento'})
aAdd(aSX3Hlp,{'C7_XDOC','Numero da Nota Fiscal'})
aAdd(aSX3Hlp,{'C7_XSERIE','Serie da Nota Fiscal'})
aAdd(aSX3Hlp,{'C7_XDTEMI','Data de Emissao da NF'})
aAdd(aSX3Hlp,{'C7_XDTVEN','Data Primeiro Vencimento'})
aAdd(aSX3Hlp,{'D1_TOTAL','Valor total da nota fiscal.'})
aAdd(aSX3Hlp,{'D1_VALDESC','Valor de Desconto do item.'})
aAdd(aSX3Hlp,{'D1_XNATURE','Natureza Financeira'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
