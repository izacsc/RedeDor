/*
{Protheus.doc} U07000
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 07/02/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423041_EF_000  FUNES DE SUPORTE
@Project NΓO IDENTIFICADO
@Obs Fontes: F0700001.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07000(lOnlyInfo)
Local aInfo := {'07','000','FUNCIONALIDADES DE SUPORTE','07/02/17','15:26','000776072010000257U0102','07/02/17','15:31','776102025201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P19','1','P19_FILIAL+P19_ID','ID Integ.','ID Integ.','ID Integ.','U','N','','','2017020715:25:23'})
aAdd(aSIX,{'P19','2','P19_FILIAL+P19_ROTINA+P19_DTHRI','Rotina+Dt.Hr.Inicio','Rotina+Dt.Hr.Inicio','Rotina+Dt.Hr.Inicio','U','N','','','2017020715:25:23'})
aAdd(aSX2,{'P19','','P19020','LOG WS DE ENTRADA','LOG WS DE ENTRADA','LOG WS DE ENTRADA','','C','C','C',0,'','','',0,'','','','','','',0,0,0,'2017020715:25:22'})
aAdd(aSX3,{'P19','01','P19_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','02','P19_ID','C',100,0,'ID Integ.','ID Integ.','ID Integ.','ID da Integracao','ID da Integracao','ID da Integracao','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','03','P19_INDKEY','C',36,0,'IndexKey','IndexKey','IndexKey','IndexKey','IndexKey','IndexKey','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','04','P19_DTHRI','C',14,0,'Dt.Hr.Inicio','Dt.Hr.Inicio','Dt.Hr.Inicio','Data/Hora Inicio','Data/Hora Inicio','Data/Hora Inicio','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','05','P19_DTHRF','C',14,0,'Dt.Hr.Fim','Dt.Hr.Fim','Dt.Hr.Fim','Dt.Hr.Fim','Dt.Hr.Fim','Dt.Hr.Fim','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','06','P19_ROTINA','C',10,0,'Rotina','Rotina','Rotina','Rotina','Rotina','Rotina','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','07','P19_STATUS','C',1,0,'Status','Status','Status','Status','Status','Status','','',' ','','',0,'ώΐ','','','U','N','A','R','','Pertence("123")','1=Gravando;2=Sucesso;3=Falha','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','08','P19_INPUT','M',10,0,'Entrada','Entrada','Entrada','Entrada','Entrada','Entrada','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX3,{'P19','09','P19_OUTPUT','M',10,0,'Saνda','Saνda','Saνda','Saida','Saida','Saida','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017020715:25:19'})
aAdd(aSX6,{'','FS_WSTRACE','C','Habilita log de WebServices','','','','','','','','','1','1','1','U','','','','','','','2017020715:25:28'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
