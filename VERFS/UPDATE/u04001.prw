/*
{Protheus.doc} U04001
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 19/09/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463801_EF_001
@Obs Fontes: F0400101.PRW,F0400103.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U04001(lOnlyInfo)
Local aInfo := {'04','001','BANCO DE CONHECIMENTO','19/09/16','11:24','001694042010100914U0112','29/12/16','14:27',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P09','1','P09_FILIAL+P09_CODDOC','Cod Document','Cod Document','Cod Document','U','','','N','2016091911:15:21'})
aAdd(aSX2,{'P09','','P09010','BANCO DE CONHECIMENTO ESP','BANCO DE CONHECIMENTO ESP','BANCO DE CONHECIMENTO ESP','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2016091911:15:19'})
aAdd(aSX3,{'P09','01','P09_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','02','P09_CODDOC','C',6,0,'Cod Document','Cod Document','Cod Document','Codigo do documento','Codigo do documento','Codigo do documento','@!','','���������������','GETSX8NUM("P09","P09_CODDOC")','',0,'��','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','03','P09_NOMDOC','C',250,0,'Nome Doc.','Nome Doc.','Nome Doc.','Nome do Documento Anexado','Nome do Documento Anexado','Nome do Documento Anexado','@!','','���������������','','',0,'��','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','04','P09_DTHORA','C',15,0,'Data/Hora','Data/Hora','Data/Hora','Data/Hora da Mov.Registro','Data/Hora da Mov.Registro','Data/Hora da Mov.Registro','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','05','P09_NIVEL','C',2,0,'N�vel Doc.','N�vel Doc.','N�vel Doc.','N�vel do Documento','N�vel do Documento','N�vel do Documento','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','06','P09_ROTINA','C',10,0,'Rotina','Rotina','Rotina','Nome da Rotina','Nome da Rotina','Nome da Rotina','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016091911:15:09'})
aAdd(aSX3,{'P09','08','P09_CODORI','C',50,0,'Cod Origem','Cod Origem','Cod Origem','C�digo Origem','C�digo Origem','C�digo Origem','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','2','2016091911:15:09'})
aAdd(aSX3Hlp,{'P09_CODDOC','C�digo sequencial do documento'})
aAdd(aSX3Hlp,{'P09_NOMDOC','Nome do Documento Anexado.'})
aAdd(aSX3Hlp,{'P09_DTHORA','Data e hora em que o registro foiinclu�do/Alterado.'})
aAdd(aSX3Hlp,{'P09_NIVEL','N�vel de Aprova�ao do Documento. Campoutilizado para definir as rotinas quevizualisar�o o ducumento'})
aAdd(aSX3Hlp,{'P09_ROTINA','Nome da Rotina em que o arquivo foiinclu�do.'})
aAdd(aSX3Hlp,{'P09_CODORI','C�digo do registro que originou o Anexo.'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
