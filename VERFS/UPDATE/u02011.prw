/*
{Protheus.doc} U02011
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 28/06/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_011
@Obs Fontes: F0201102.PRW,F0201103.PRW,F0201105.PRW,F0201106.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02011(lOnlyInfo)
Local aInfo := {'02','011','CADASTRO DE GRUPO E SUBGRUPO','28/06/16','11:38','001688122010100612U1123','29/12/16','14:29',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P03','1','P03_FILIAL+P03_GRUPO','FILIAL + GRUPO','FILIAL + GRUPO','FILIAL + GRUPO','U','','','S','2016062811:30:37'})
aAdd(aSIX,{'P04','1','P04_FILIAL+P04_SUBGRP','FILIAL+SUBGRP','FILIAL+SUBGRP','FILIAL+SUBGRP','U','','','S','2016062811:30:37'})
aAdd(aSX2,{'P03','','P03990','TABELA DE GRUPOS DE PRODUTOS','TABELA DE GRUPOS DE PRODUTOS','TABELA DE GRUPOS DE PRODUTOS','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2016062811:30:37'})
aAdd(aSX2,{'P04','','P04990','TABELA DE SUB GRUPOS DE PRODUT','TABELA DE SUB GRUPOS DE PRODUT','TABELA DE SUB GRUPOS DE PRODUT','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2016062811:30:37'})
aAdd(aSX3,{'P03','01','P03_FILIAL','C',2,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016062811:30:34'})
aAdd(aSX3,{'P03','02','P03_GRUPO','C',4,0,'Grupo','Grupo','Grupo','Grupo','Grupo','Grupo','','',' ','GETSXENUM("P03","P03_GRUPO")','',0,'ώΐ','','','U','S','V','R','','EXISTCHAV("P03",M->P03_GRUPO,1)','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P03','03','P03_DESC','C',30,0,'Desc. Grupo','Desc. Grupo','Desc. Grupo','Desc. Grupo','Desc. Grupo','Desc. Grupo','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P03','04','P03_TIPO','C',4,0,'Tipo Produto','Tipo Produto','Tipo Produto','Tipo Produto','Tipo Produto','Tipo Produto','','',' ','','SBM',0,'ώΐ','','S','U','S','A','R','','ExistCpo("SBM",M->P03_TIPO)','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P03','05','P03_DESTP','C',30,0,'Desc. Tp. Pr','Desc. Tp. Pr','Desc. Tp. Pr','Descricao do Tipo','Descricao do Tipo','Descricao do Tipo','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P04','01','P04_FILIAL','C',2,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016062811:30:34'})
aAdd(aSX3,{'P04','02','P04_SUBGRP','C',4,0,'Subgrupo','Subgrupo','Subgrupo','Subgrupo','Subgrupo','Subgrupo','','',' ','GETSXENUM("P04","P04_SUBGRP")','',0,'ώΐ','','','U','S','V','R','','EXISTCHAV("P04",M->P04_SUBGRP,1)','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P04','03','P04_DESC','C',30,0,'Descricao','Descricao','Descricao','Descricao','Descricao','Descricao','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P04','04','P04_GRUPO','C',4,0,'Grupo Prod.','Grupo Prod.','Grupo Prod.','Grupo de produto','Grupo de produto','Grupo de produto','','',' ','','FSP032',0,'ώΐ','','S','U','S','A','R','','ExistCpo("P03",M->P04_GRUPO)','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'P04','05','P04_DESGR','C',30,0,'Desc Grupo','Desc Grupo','Desc Grupo','Descricao do Grupo','Descricao do Grupo','Descricao do Grupo','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:34'})
aAdd(aSX3,{'SB1','S0','B1_XGRPPRO','C',4,0,'Grup Produto','Grup Produto','Grup Produto','Grupo do Produto','Grupo do Produto','Grupo do Produto','@!','',' ','','FSP031',0,'ώΐ','','S','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016122914:29:09'})
aAdd(aSX3,{'SB1','S1','B1_XSUBGRP','C',4,0,'Subgrup Prod','Subgrup Prod','Subgrup Prod','Subgrupo do Produto','Subgrupo do Produto','Subgrupo do Produto','@!','',' ','','FSP041',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016122914:29:09'})
aAdd(aSX3,{'SBM','27','BM_XESTOQ','C',10,0,'Est Nγo Esto','Est Nγo Esto','Est Nγo Esto','Estoque Nγo Estoque','Estoque Nγo Estoque','Estoque Nγo Estoque','','',' ','','',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:35'})
aAdd(aSX3,{'SBM','28','BM_XCCDES','C',20,0,'C. C.Despesa','C. C.Despesa','C. C.Despesa','Conta Contabil Despesa','Conta Contabil Despesa','Conta Contabil Despesa','','',' ','','FSCT1',0,'ώΐ','','S','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:35'})
aAdd(aSX3,{'SBM','29','BM_XDESCD','C',30,0,'Desc C. Desp','Desc C. Desp','Desc C. Desp','Descricao C. C. Despesa','Descricao C. C. Despesa','Descricao C. C. Despesa','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:35'})
aAdd(aSX3,{'SBM','30','BM_XCCEST','C',20,0,'C.C. Estoque','C.C. Estoque','C.C. Estoque','Conta Contabil Estoque','Conta Contabil Estoque','Conta Contabil Estoque','','',' ','','FSCT1',0,'ώΐ','','S','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:35'})
aAdd(aSX3,{'SBM','31','BM_XDESCE','C',30,0,'Desc.C.Estoq','Desc.C.Estoq','Desc.C.Estoq','Desc.C.Estoq','Desc.C.Estoq','Desc.C.Estoq','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016062811:30:35'})
aAdd(aSX7,{'B1_GRUPO','001','Space(Len(SB1->B1_XGRPPRO))','B1_XGRPPRO','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'B1_GRUPO','002','Space(Len(SB1->B1_XSUBGRP))','B1_XSUBGRP','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'B1_XGRPPRO','001','Space(Len(SB1->B1_XSUBGRP))','B1_XSUBGRP','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'BM_XCCDES','001','CT1->CT1_DESC01','BM_XDESCD','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'BM_XCCEST','001','CT1->CT1_DESC01','BM_XDESCE','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'P03_TIPO','001','SBM->BM_DESC','P03_DESTP','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSX7,{'P04_GRUPO','001','P03->P03_DESC','P04_DESGR','P','N','',0,'','','U','2016062811:30:38'})
aAdd(aSXB,{'FSP031','1','01','DB','Grupo de produtos','Grupo de produtos','Grupo de produtos','P03','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','2','01','01','Filial + Grupo','Filial + Grupo','Filial + Grupo','','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','4','01','01','Filial','Sucursal','Branch','P03_FILIAL','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','4','01','02','Grupo','Grupo','Grupo','P03_GRUPO','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','4','01','03','Desc. Grupo','Desc. Grupo','Desc. Grupo','P03_DESC','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','5','01','','','','','P03->P03_GRUPO','','2016062811:30:40'})
aAdd(aSXB,{'FSP031','6','01','','','','','P03->P03_TIPO == M->B1_GRUPO','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','1','01','DB','Grupo de produtos','Grupo de produtos','Grupo de produtos','P03','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','2','01','01','Filial + Grupo','Filial + Grupo','Filial + Grupo','','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','4','01','01','Filial','Sucursal','Branch','P03_FILIAL','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','4','01','02','Grupo','Grupo','Grupo','P03_GRUPO','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','4','01','03','Desc. Grupo','Desc. Grupo','Desc. Grupo','P03_DESC','','2016062811:30:40'})
aAdd(aSXB,{'FSP032','5','01','','','','','P03->P03_GRUPO','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','1','01','DB','SubGrupo','Subgrupo de produtos','Subgrupo de produtos','P04','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','2','01','01','Filial+subgrp','Filial+subgrp','Filial+subgrp','','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','4','01','01','Filial','Sucursal','Branch','P04_FILIAL','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','4','01','02','Subgrupo','Subgrupo','Subgrupo','P04_SUBGRP','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','4','01','03','Descricao','Descricao','Descricao','P04_DESC','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','5','01','','','','','P04->P04_SUBGRP','','2016062811:30:40'})
aAdd(aSXB,{'FSP041','6','01','','','','','P04->P04_GRUPO == M->B1_XGRPPRO','','2016062811:30:40'})
aAdd(aSX3Hlp,{'B1_XGRPPRO','Grupo do Produto'})
aAdd(aSX3Hlp,{'B1_XSUBGRP','Subgrupo do Produto'})
aAdd(aSX3Hlp,{'BM_XESTOQ','Estoque Nγo Estoque'})
aAdd(aSXB,{'FSCT1','1','01','DB','Contas Contabeis','Cuentas Contables','Ledger Accounts','CT1','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','2','01','01','Conta','Cuenta','Account','','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','2','02','06','Descricao','Descripcion','Description','','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','2','03','02','Reduzida','Reducida','Reduced','','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','3','01','01','Cadastra Nova','Registra Nuevo','Add New','01','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','01','01','Codigo','Codigo','Code','CT1_CONTA','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','01','02','Descricao','Descripcion','Description','CT1_DESC01','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','02','01','Descricao','Descripcion','Description','CT1_DESC01','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','02','02','Codigo','Codigo','Code','CT1_CONTA','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','03','01','Cod Reduz','Cod Reducido','Reduced Code','CT1_RES','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','4','03','02','Descricao','Descripcion','Description','CT1_DESC01','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','5','01','','','','','CT1->CT1_CONTA','','2016062411:42:59'})
aAdd(aSXB,{'FSCT1','5','02','','','','','CT1_DESC01','','2016062411:42:59'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
