/*
{Protheus.doc} U03006
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 24/11/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463701_EF_006
@Obs Fontes: F0300601.PRW,F0300602.PRW,F0300603.PRW,F0300604.PRW,F0300605.PR
@Obs Fontes: F0300606.PRW,F0300607.PRW
*/
#INCLUDE 'PROTHEUS.CH'
User Function U03006(lOnlyInfo)
Local aInfo := {'03','006','PLANOSAUDE','24/11/16','16:13','006643032110600163U0121','03/01/17','18:30',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'RHO','3','RHO_FILIAL+RHO_MAT+DTOS(RHO_DTOCOR)+RHO_TPFORN+RHO_CODFOR+RHO_ORIGEM+RHO_CODIGO+RHO_PD+RHO_COMPPG','Indice de importa��o coparticipa��o Rededor','Indice de importa��o coparticipa��o Rededor','Indice de importa��o coparticipa��o Rededor','U','','3','S','2016122616:01:30'})
aAdd(aSX1,{'FSW0300602','01','Filial','Filial','Filial','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300602','02','Escolha o plano','Escolha o plano','Escolha o plano','MV_CH0','C',1,0,1,'C','','MV_PAR02','Plano saude','Plano saude','Plano saude','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300602','03','Dir Arquivo','Dir Arquivo','Dir Arquivo','MV_CH0','C',30,0,0,'G','','MV_PAR03','','','','C:\AREA1\COPAR.TXT','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','01','Filial de','Filial de','Filial de','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','02','Filial ate','Filial ate','Filial ate','MV_CH0','C',8,0,0,'G','','MV_PAR02','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','03','Matricula de','Matricula de','Matricula de','MV_CH0','C',6,0,0,'G','','MV_PAR03','','','','000001','','','','','','','','','','','','','','','','','','','','','SRA','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','04','Matricula ate','Matricula ate','Matricula ate','MV_CH0','C',6,0,0,'G','','MV_PAR04','','','','999999','','','','','','','','','','','','','','','','','','','','','SRA','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','05','Admissao de','Admissao de','Admissao de','MV_CH0','D',8,0,0,'G','','MV_PAR05','','','','20120101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','06','Admissao ate','Admissao ate','Admissao ate','MV_CH0','D',8,0,0,'G','','MV_PAR06','','','','20161031','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','07','Periodo Inicial','Periodo Inicial','Periodo Inicial','MV_CH0','C',6,0,0,'G','','MV_PAR07','','','','012016','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','08','Periodo Final','Periodo Final','Periodo Final','MV_CH0','C',6,0,0,'G','','MV_PAR08','','','','102016','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','09','Tipo de arquivo','Tipo de arquivo','Tipo de arquivo','MV_CH0','N',1,0,1,'C','','MV_PAR09','Medico','Medico','Medico','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300603','10','Caminho de grava�ao','Caminho de grava�ao','Caminho de grava�ao','MV_CH1','C',20,0,0,'G','','MV_PAR10','','','','c:\area1\DEPE','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','01','Filial de','Filial de','Filial de','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','02','Filial ate','Filial ate','Filial ate','MV_CH0','C',8,0,0,'G','','MV_PAR02','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','03','Matricula de','Matricula de','Matricula de','MV_CH0','C',6,0,0,'G','','MV_PAR03','','','','000001','','','','','','','','','','','','','','','','','','','','','SRA','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','04','Matricula ate','Matricula ate','Matricula ate','MV_CH0','C',6,0,0,'G','','MV_PAR04','','','','999999','','','','','','','','','','','','','','','','','','','','','SRA','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','05','Admissao de','Admissao de','Admissao de','MV_CH0','D',8,0,0,'G','','MV_PAR05','','','','20120101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','06','Admissao ate','Admissao ate','Admissao ate','MV_CH0','D',8,0,0,'G','','MV_PAR06','','','','20161031','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','07','Vigencia de','Vigencia de','Vigencia de','MV_CH0','C',6,0,0,'G','','MV_PAR07','','','','012016','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','08','Vigencia ate','Vigencia ate','Vigencia ate','MV_CH0','C',6,0,0,'G','','MV_PAR08','','','','102016','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','09','Tipo de arquivo','Tipo de arquivo','Tipo de arquivo','MV_CH0','N',1,0,1,'C','','MV_PAR09','Medico','Medico','Medico','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300604','10','Caminho de grava�ao','Caminho de grava�ao','Caminho de grava�ao','MV_CH1','C',20,0,0,'G','','MV_PAR10','','','','c:\area1\GRUPO','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','01','Filial de','Filial de','Filial de','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','02','Filial ate','Filial ate','Filial ate','MV_CH0','C',8,0,0,'G','','MV_PAR02','','','','D MG 02','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','03','Demissao de','Demissao de','Demissao de','MV_CH0','D',8,0,0,'G','','MV_PAR03','','','','20160101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','04','Demissao ate','Demissao ate','Demissao ate','MV_CH0','D',8,0,0,'G','','MV_PAR04','','','','20161031','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','05','Tipo de Arquivo','Tipo de Arquivo','Tipo de Arquivo','MV_CH0','C',1,0,1,'C','','MV_PAR05','Medico','Medico','Medico','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300605','06','Caminho do arquivo','Caminho do arquivo','Caminho do arquivo','MV_CH0','C',40,0,0,'G','','MV_PAR06','','','','C:\AREA1\DEMIT','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','01','Filial de','Filial de','Filial de','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','02','Filial ate','Filial ate','Filial ate','MV_CH0','C',8,0,0,'G','','MV_PAR02','','','','M PR 02','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','03','Transferencia de','Transferencia de','Transferencia de','MV_CH0','D',8,0,0,'G','','MV_PAR03','','','','20160101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','04','Transferencia ate','Transferencia ate','Transferencia ate','MV_CH0','D',8,0,0,'G','','MV_PAR04','','','','20161031','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','05','Tipo de arquivo','Tipo de arquivo','Tipo de arquivo','MV_CH0','C',1,0,1,'C','','MV_PAR05','Medico','Medico','Medico','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300606','06','Caminho do arquivo','Caminho do arquivo','Caminho do arquivo','MV_CH0','C',40,0,0,'G','','MV_PAR06','','','','C:\AREA1\TRANSF','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300607','01','Filial','Filial','Filial','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','D MG 01','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300607','02','Escolha o plano','Escolha o plano','Escolha o plano','MV_CH0','C',1,0,1,'C','','MV_PAR02','Plano saude','Plano saude','Plano saude','','','Odontologico','Odontologico','Odontologico','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX1,{'FSW0300607','03','Caminho do arquivo','Caminho do arquivo','Caminho do arquivo','MV_CH0','C',40,0,0,'G','','MV_PAR03','','','','C:\AREA1\IMPCAR.CSV','','','','','','','','','','','','','','','','','','','','','','','','','','','2016122616:12:00'})
aAdd(aSX3,{'RHK','13','RHK_NCARVD','C',20,0,'N Carteira','N Carteira','N Carteira','N Carteira','N Carteira','N Carteira','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016122616:11:55'})
aAdd(aSX3,{'RHL','12','RHL_NCARVD','C',20,0,'N Carteira','N Carteira','N Carteira','N Carteira','N Carteira','N Carteira','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016122616:11:55'})
aAdd(aSX3,{'SRB','35','RB_MAE','C',60,0,'Nome Mae','Nome Mae','Nome Mae','Nome Mae','Nome Mae','Nome Mae','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016122616:11:55'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
