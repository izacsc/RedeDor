/*
{Protheus.doc} U02007
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 09/09/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_011
@Obs Fontes: F0201101.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02007(lOnlyInfo)
Local aInfo := {'02','007','PERGUNTAS EF_007','09/09/16','18:26','007696022010700982U0102','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0200701','01','Tipos de Relatorio','Tipos de Relatorio','Tipos de Relatorio','MV_CH0','N',1,0,1,'C','','MV_PAR01','Lista Critica','','','','','Manuten��o LC','','','','','Prev Vencida','','','','','','','','','','','','','','','','','','','','2016090918:24:41'})
aAdd(aSX1,{'FSW0200701','02','Data Base','Data Base','Data Base','MV_CH0','D',8,0,0,'G','','MV_PAR02','','','','20160808','','','','','','','','','','','','','','','','','','','','','','','','','','','2016090918:24:41'})
aAdd(aSX1,{'FSW0200701','03','Produto','Produto','Produto','MV_CH0','C',15,0,0,'G','','MV_PAR03','','','','000000000000002','','','','','','','','','','','','','','','','','','','','','SB1','','','','@!','','2016090918:24:41'})
aAdd(aSX1,{'FSW0200701','04','Categoria','Categoria','Categoria','MV_CH0','C',6,0,0,'G','','MV_PAR04','','','','','','','','','','','','','','','','','','','','','','','','','ACU','','','','@!','','2016090918:24:41'})
aAdd(aSX1,{'FSW0200701','05','Cont. Planejamento?','Cont. Planejamento?','Cont. Planejamento?','MV_CH0','N',1,0,1,'C','','MV_PAR05','Sim','','','','','N�o','','','','','','','','','','','','','','','','','','','','','','','','','2016090918:24:41'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
