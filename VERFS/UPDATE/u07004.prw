/*
{Protheus.doc} U07004
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 17/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07004(lOnlyInfo)
Local aInfo := {'07','004','VALIDACAO DO SETOR NO PEDIDO DE COMPRAS','17/01/17','16:13','004773072010400167U0111','27/01/17','14:45','773111016201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'SC7','HJ','C7_XCODSET','C',6,0,'Codigo Setor','Codigo Setor','Codigo Setor','Codigo do Setor','Codigo do Setor','Codigo do Setor','@!','','���������������','','P11Set',0,'��','','S','U','S','A','R','�','ExistCpo("P11")','','','','','','','','','','','','','N','N','','','','2017011716:12:07'})
aAdd(aSX3,{'SC7','HK','C7_XDESSET','C',20,0,'Desc Setor','Desc Setor','Desc Setor','Descri��o do Setor','Descri��o do Setor','Descri��o do Setor','@!','','���������������','IF(!INCLUI,POSICIONE("P11",1,XFILIAL("P11")+SC7->C7_XCODSET,"P11->P11_DESC"),"")','',0,'��','','','U','S','V','V','','','','','','','','POSICIONE("P11",1,XFILIAL("P11")+SC7->C7_XCODSET,"P11->P11_DESC")','','','','','','','N','N','','','','2017011716:12:07'})
aAdd(aSX7,{'C7_XCODSET','001','M->C7_XDESSET:=P11->P11_DESC','C7_XDESSET','P','S','P11',1,'xFilial("P11")+M->C7_XCODSET','!EMPTY(M->C7_XCODSET)','U','2017011716:12:10'})
aAdd(aSX3Hlp,{'C7_XCODSET','Codigo do Setor'})
aAdd(aSX3Hlp,{'C7_XDESSET','Descri��o do Setor'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
