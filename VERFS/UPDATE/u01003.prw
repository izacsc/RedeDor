/*
{Protheus.doc} U01003
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 06/12/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_003
@Obs Fontes: F0100301.PRW,F0100302.PRW,F0100303.PRW,F0100304.PRW,F0100305.PR
@Obs Fontes: F0100306.PRW,F0100307.PRW,F0100308.PRW,F0100309.PRW,F0100310.PR
@Obs Fontes: F0100311.PRW,F0100312.PRW,F0100313.PRW,F0100314.PRW,F0100315.PR
@Obs Fontes: F0100316.PRW,F0100317.PRW,F0100318.PRW,F0100319.PRW,F0100320.PR
@Obs Fontes: F0100321.PRW,F0100322.PRW,F0100323.PRW,F0100324.PRW,F0100325.PR
@Obs Fontes: F0100326.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U01003(lOnlyInfo)
Local aInfo := {'01','003','SOLICITA�AO DE VAGAS','06/12/16','10:21','003661012110300201U0102','09/01/17','19:21',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'RD4','12','RD4_XAPNT','C',1,0,'Aprov\Notif?','Aprov\Notif?','Aprov\Notif?','Aprova��o ou Notifica��o?','Aprova��o ou Notifica��o?','Aprova��o ou Notifica��o?','','','���������������','"1"','',0,'��','','','U','N','A','R','','PERTENCE("12")','1=Aprova��o;2=Notifica��o;','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'RH3','25','RH3_XTPCTM','C',3,0,'Tipo Solicit','Tipo Solicit','Tipo Solicit','Tipo Solicit customizada','Tipo Solicit customizada','Tipo Solicit customizada','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'RH3','26','RH3_XDSCTM','C',40,0,'Descri��o','Descri��o','Descri��o','Descri��o Tipo Solic.Cust','Descri��o tipo Customizad','Descri��o tipo Customizad','','','���������������','POSICIONE("PA7",1,XFILIAL("PA7")+RH3->RH3_XTPCTM,"PA7_DESCR")','',0,'��','','','U','S','V','V','','','','','','','','Posicione("PA7",1,xFilial("PA7")+RH3->RH3_XTPCTM,"PA7_DESCR")','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'RH3','27','RH3_XSUSPE','C',1,0,'Suspenso?','Suspenso?','Suspenso?','Solicita��o Suspensa?','Solicita��o Suspensa?','Solicita��o Suspensa?','','','���������������','"1"','',0,'��','','','U','N','A','R','','','1=Nao;2=Sim','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'RH4','08','RH4_XOBS','M',10,0,'Observacao','Observacao','Observacao','Observa�ao solicita��o','Observa�ao solicita��o','Observa�ao solicita��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','71','QG_XTURNTB','C',3,0,'Turno Trab.','Turno Trab.','Turno Trab.','Turno de trabalho','Turno de trabalho','Turno de trabalho','','','���������������','','SR6',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','72','QG_XHRMES','N',8,4,'Hora Mes','Hora Mes','Hora Mes','Hora M�s','Hora M�s','Hora M�s','@E 999.9999','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','73','QG_XHRSEM','N',8,4,'Horas Sem.','Horas Sem.','Horas Sem.','Horas semanais','Horas semanais','Horas semanais','@E 999.9999','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','74','QG_XTPCONT','C',1,0,'Tp.Contrato','Tp.Contrato','Tp.Contrato','Tipo de Contrato','Tipo de Contrato','Tipo de Contrato','','','���������������','','',0,'��','','','U','N','A','R','','','1=Determinado; 2=Indeterminado','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','78','QG_XSALHOR','N',12,2,'Salario Hora','Salario Hora','Salario Hora','Salario Hora','Salario Hora','Salario Hora','@E 999,999,999.99','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQG','80','QG_XCATFUN','C',1,0,'Cat.Func','Cat.Func','Cat.Func','Categoria do Funcionario','','','','','���������������','','28',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','27','QS_XSTATUS','C',1,0,'Status','Status','Status','Status da Vaga','Status da Vaga','Status da Vaga','','','���������������','','',0,'��','','','U','N','A','R','','','1=Aprovada;2=Em Recrutamento;3=Em Movimenta��o (RI);4=Em Admiss�o;5=Cancelada;6=Suspensa;7=Conclu�da','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','28','QS_XSOLPTL','C',5,0,'Solic.Portal','Solic.Portal','Solic.Portal','Cod.Solicita��o do Portal','Cod.Solicita��o do Portal','Cod.Solicita��o do Portal','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','30','QS_XJORN','N',6,2,'Jornada','Jornada','Jornada','Jornada de Trabalho','Jornada de Trabalho','Jornada de Trabalho','@E 999.99','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','31','QS_XOBSERV','M',10,0,'Observa��o','Observa��o','Observa��o','Observa��o da vaga','Observa��o da vaga','Observa��o da vaga','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','32','QS_XSOLFIL','C',8,0,'Filial Solic','Filial Solic','Filial Solic','Filial solicita��o portal','Filial solicita��o vaga','Filial solicita��o vaga','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','033','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SQS','36','QS_XPUBLIC','C',1,0,'Publica?','Publica?','Publica?','Publica vaga?','Publica vaga?','Publica vaga?','','','���������������','"2"','',0,'��','','','U','N','A','R','','','1=Sim;2=Nao','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX3,{'SRA','S0','RA_XVAGA','C',6,0,'C�d.Vaga','C�d.Vaga','C�d.Vaga','C�digo da Vaga','C�digo da Vaga','C�digo da Vaga','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017010918:40:00'})
aAdd(aSX6,{'','FS_DIREXE','C','Diretor executivo','','','','','','','','','','','','U','','','','','','','2017010918:40:00'})
aAdd(aSX6,{'','FS_EFTVRH','C','E-mail do Rh que realizar� a efetiva��o das solici','','','ta��es feitas no portal.','','','','','','','','','U','','','','','','','2017010918:40:00'})
aAdd(aSX6,{'','FS_RHDED','C','RH DEDICADO','','','','','','','','','','','','U','','','','','','','2017010918:40:00'})
aAdd(aSX7,{'QG_VAGA','002','U_F0100324(M->QG_VAGA,.F.)','QG_XCATFUN','P','N','',0,'','','U','2017010918:40:00'})
aAdd(aSX3Hlp,{'RD4_XAPNT','Informar se o departamento � deaprova��o ou notifica��o? (1=Aprova��o;2=Notifica��o).'})
aAdd(aSX3Hlp,{'RH3_XTPCTM','Tipo de solicita��o customizada.'})
aAdd(aSX3Hlp,{'RH3_XDSCTM','Descri��o do tipo de solicita��ocustomizada.'})
aAdd(aSX3Hlp,{'RH3_XSUSPE','Solicita��o Suspensa? (1=N�o;2=Sim)'})
aAdd(aSX3Hlp,{'QG_XTPCONT','Tipo de Contrato (1=Determinado;2=Indeterminado)'})
aAdd(aSX3Hlp,{'QS_XSTATUS','Status da Vaga (1=Aprovada,2=EmRecrutamento;3=EmMovimenta��o (RI); 4=Em Admiss�o;5=Cancelada; 6=Suspensa; 7=Conclu�da)'})
aAdd(aSX3Hlp,{'QS_XJORN','Jornada de Trabalho.'})
aAdd(aSX3Hlp,{'QS_XOBSERV','Observa��o da vaga'})
aAdd(aSX3Hlp,{'QS_XSOLFIL','Filial da solicita��o do portal'})
aAdd(aSX3Hlp,{'QS_XSOLPTL','C�digo da solicita��o realizada noportal.'})
aAdd(aSX3Hlp,{'QS_XPUBLIC','Publica vaga? (1=Sim;2=Nao).'})
aAdd(aSX3Hlp,{'RA_XVAGA','C�digo da vaga'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
