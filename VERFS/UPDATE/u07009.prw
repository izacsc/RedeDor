/*
{Protheus.doc} U07009
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 02/02/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_010
@Obs Fontes: F0101001.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07009(lOnlyInfo)
Local aInfo := {'07','009','CRIACAO DE CAMPO E GATILHO','02/02/17','14:34','009724072010900247U0103','02/02/17','16:40','724103024201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'SB1','S4','B1_XCODFAB','C',6,0,'Cod Fabrican','Cod Fabrican','Cod Fabrican','Codigo Fabricante','Codigo Fabricante','Codigo Fabricante','','','���������������','','FSWP13',0,'��','','S','U','N','A','R','','u_F0700901(M->B1_XCODFAB) .AND. ExistCpo("P13")','','','','','','','','','','','','','N','N','','','','2017020214:32:13'})
aAdd(aSX7,{'B1_XCODFAB','001','P13->P13_DESCR','B1_FABRIC','P','S','P13',1,'xFilial("P13")+M->B1_XCODFAB','','U','2017020214:32:32'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
