/*
{Protheus.doc} U02008
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 20/06/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_008
@Obs Fontes: F0200801.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02008(lOnlyInfo)
Local aInfo := {'02','008','REPORT DIRETORIA DE SUPRIMENTOS E DIRETORIA EXECUTIVA DE SERVI�OS','20/06/16','17:53','008603022010800672U0125','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0200801','01','Data Base:','Data Base:','Data Base:','MV_CH0','D',8,0,0,'G','!empty(mv_par01)','MV_PAR01','','','','20160516','','','','','','','','','','','','','','','','','','','','','','','','','','','2016062017:51:16'})
aAdd(aSX1,{'FSW0200801','02','Produto:','Produto:','Produto:','MV_CH0','C',15,0,0,'G','','MV_PAR02','','','','','','','','','','','','','','','','','','','','','','','','','SB1','','','','','','2016062017:51:16'})
aAdd(aSX1,{'FSW0200801','03','Categoria:','Categoria:','Categoria:','MV_CH0','C',6,0,0,'G','','MV_PAR03','','','','','','','','','','','','','','','','','','','','','','','','','ACU','','','','','','2016062017:51:16'})
aAdd(aSX1,{'FSW0200801','04','Unidade[Filial]:','Unidade[Filial]:','Unidade[Filial]:','MV_CH0','C',99,0,0,'R','!empty(mv_par04)','MV_PAR04','','','','SB1.B1_FILIAL','','','','','01;','','','','','','','','','','','','','','','','XM0','','','','','','2016062017:51:16'})
aAdd(aSX3,{'SB1','S2','B1_XCONSIG','C',1,0,'Consignado','Consignado','Consignado','Item Consignado','Item Consignado','Item Consignado','@!','','���������������','','',0,'��','','','U','N','A','R','','pertence(" 12")','1=Sim;2=N�o','1=Sim;2=N�o','1=Sim;2=N�o','','','','','','','','','','N','N','','','','2016062017:51:04'})
aAdd(aSX6,{'','FS_ALM','C','C�digo do Armazem do tipo Almoxarifado','C�digo do Armazem do tipo Almoxarifado','C�digo do Armazem  do tipo Almoxarifado','','','','','','','01','01','01','U','','','','','','','2016062017:51:15'})
aAdd(aSX3Hlp,{'B1_XCONSIG','Item Consignado'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
