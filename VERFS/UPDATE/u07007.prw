/*
{Protheus.doc} U07007
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 09/02/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_010
@Obs Fontes: F0101001.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07007(lOnlyInfo)
Local aInfo := {'07','007','CRIACAO DE TABELA, CAMPOS, INDICE E CONSULTA PADRAO','09/02/17','17:04','','','','794100027201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P13','1','P13_FILIAL+P13_COD','Codigo','Codigo','Codigo','U','','','S','2017020917:01:00'})
aAdd(aSIX,{'P13','2','P13_FILIAL+P13_CGC','CGC','CGC','CGC','U','','','N','2017020917:01:00'})
aAdd(aSX2,{'P13','','P13010','CADASTRO DE FABRICANTES','CADASTRO DE FABRICANTES','CADASTRO DE FABRICANTES','','C','C','C',0,'','','',0,'','','','','','',0,0,0,'2017020917:00:57'})
aAdd(aSX3,{'P13','01','P13_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','','2017020917:00:50'})
aAdd(aSX3,{'P13','02','P13_COD','C',6,0,'Codigo','Codigo','Codigo','C�digo do Fabricante','C�digo do Fabricante','C�digo do Fabricante','','','���������������','GETSXENUM("P13","P13_COD")','',0,'��','','','U','S','V','R','�','EXISTCHAV("P13")','','','','','','','','','','','','','N','N','','','','','2017020917:00:50'})
aAdd(aSX3,{'P13','03','P13_DESCR','C',30,0,'Descri��o','Descri��o','Descri��o','Descri��o do Fabricante','Descri��o do Fabricante','Descri��o do Fabricante','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','','2017020917:00:50'})
aAdd(aSX3,{'P13','04','P13_CGC','C',14,0,'CGC','CGC','CGC','CNPJ do Fabricante','CNPJ do Fabricante','CNPJ do Fabricante','@R 99.999.999/9999-99','','���������������','','',0,'��','','','U','S','A','R','�','u_F0700702(M->P13_CGC, M->P13_TIPO) .AND. ( VAZIO() .Or. CGC(M->P13_CGC) )','','','','','','','','','','','','','N','N','','','','','2017020917:00:50'})
aAdd(aSX3,{'P13','05','P13_ID','C',36,0,'ID Integra��','ID Integra��','ID Integra��','ID Integra��o','ID Integra��o','ID Integra��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','','2017020917:00:50'})
aAdd(aSX3,{'P13','07','P13_TIPO','C',1,0,'Tipo','Tipo','Tipo','Tipo','Tipo','Tipo','@!','','���������������','1','',0,'��','','','U','S','A','R','�','Pertence("12") .AND. If(!Empty(M->P13_CGC),u_F0700702(M->P13_CGC, M->P13_TIPO) .AND. ( VAZIO() .Or. CGC(M->P13_CGC) ),.T.)','1=Nacional;2=Estrangeiro','1=Nacional;2=Estrangeiro','1=Nacional;2=Estrangeiro','','','','','','','','','','N','N','','','','','2017020917:00:50'})
aAdd(aSXB,{'FSWP13','1','01','DB','Consulta Fabricante','Consulta Fabricante','Consulta Fabricante','P13','','2017020917:01:09'})
aAdd(aSXB,{'FSWP13','2','01','01','Codigo','Codigo','Codigo','','','2017020917:01:09'})
aAdd(aSXB,{'FSWP13','4','01','01','Filial','Sucursal','Branch','P13_FILIAL','','2017020917:01:09'})
aAdd(aSXB,{'FSWP13','4','01','02','Codigo','Codigo','Codigo','P13_COD','','2017020917:01:09'})
aAdd(aSXB,{'FSWP13','4','01','03','Descri��o','Descri��o','Descri��o','P13_DESCR','','2017020917:01:09'})
aAdd(aSXB,{'FSWP13','5','01','','','','','P13->P13_COD','','2017020917:01:09'})
aAdd(aSX3Hlp,{'P13_TIPO','Tipo: 1-Nacional, 2-Estrangeiro.'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
