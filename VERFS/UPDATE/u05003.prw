/*
{Protheus.doc} U05003
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 06/12/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
*/
#INCLUDE 'PROTHEUS.CH'
User Function U05003(lOnlyInfo)
Local aInfo := {'05','003','REDEDOR 003','06/12/16','09:00','003660052100300295U0100','13/01/17','17:56',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'PA2','1','PA2_FILIAL+PA2_CDVAGA+PA2_CDCAND','PA2_FILIAL+PA2_CDVAGA','PA2_FILIAL+PA2_CDVAGA','PA2_FILIAL+PA2_CDVAGA','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','2','PA2_FILIAL+PA2_LOGTOR','PA2_FILIAL+PA2_LOGTOR','PA2_FILIAL+PA2_LOGTOR','PA2_FILIAL+PA2_LOGTOR','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','3','PA2_FILIAL+PA2_LOGTOR+PA2_DTAPRV','PA2_FILIAL+PA2_LOGTOR+PA2_DTAPRV','PA2_FILIAL+PA2_LOGTOR+PA2_DTAPRV','PA2_FILIAL+PA2_LOGTOR+PA2_DTAPRV','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','4','PA2_FILIAL+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_DTAPRV+PA2_LOGTOR','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','5','PA2_FILIAL+PA2_CDVAGA+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_CDVAGA+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_CDVAGA+PA2_DTAPRV+PA2_LOGTOR','PA2_FILIAL+PA2_CDVAGA+PA2_DTAPRV+PA2_LOGTOR','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','6','PA2_FILIAL+PA2_SOL','PA2_FILIAL+PA2_SOL','PA2_FILIAL+PA2_SOL','PA2_FILIAL+PA2_SOL','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA2','7','PA2_FILIAL+PA2_FILSOL+PA2_SOL','PA2_FILIAL+PA2_FILSOL+PA2_SOL','PA2_FILIAL+PA2_FILSOL+PA2_SOL','PA2_FILIAL+PA2_FILSOL+PA2_SOL','U','','','N','2017011317:34:22'})
aAdd(aSIX,{'PA7','1','PA7_FILIAL+PA7_CODIGO','Codigo','Codigo','Codigo','U','','','N','2017011317:34:22'})
aAdd(aSX2,{'PA2','','PA2010','FORMUL�RIO DE APROVA��O PROFIS','FORMUL�RIO DE APROVA��O PROFIS','FORMUL�RIO DE APROVA��O PROFIS','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2017011317:34:22'})
aAdd(aSX2,{'PA7','','PA7010','CONTROLE SOLICITA��O PORTAL','CONTROLE SOLICITA��O PORTAL','CONTROLE SOLICITA��O PORTAL','','C','C','C',0,'','','',0,'','','','','','',0,0,0,'2017011317:34:22'})
aAdd(aSX3,{'PA2','01','PA2_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','A','R','','','','','','','','','033','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','02','PA2_CDVAGA','C',6,0,'Cod Vaga','Cod Vaga','Cod Vaga','C�digo da Vaga','C�digo da Vaga','C�digo da Vaga','','','���������������','','FSSQS2',0,'��','','S','U','S','A','R','�','ExistCpo("SQS")','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','03','PA2_FILVG','C',8,0,'Fil Vaga','Fil Vaga','Fil Vaga','Filial Vaga','Filial Vaga','Filial Vaga','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','04','PA2_VLVAGA','N',12,2,'Val. Vaga','Val. Vaga','Val. Vaga','Valor da Vaga','Valor da Vaga','Valor da Vaga','@E 999999999.99','','���������������','POSICIONE("SQS",1,XFILIAL("SQS")+M->PA2_CDVAGA,"QS_VCUSTO")','',0,'��','','','U','S','V','R','','','','','','','','POSICIONE("SQS",1,XFILIAL("SQS")+M->PA2_CDVAGA,"QS_VCUSTO")','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','05','PA2_SLFECH','N',12,2,'Fechamento','Fechamento','Fechamento','Salario Fechamento','Salario Fechamento','Salario Fechamento','@E 999999999.99','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','06','PA2_CDCAND','C',6,0,'Candidato','Candidato','Candidato','C�digo candidato','C�digo candidato','C�digo candidato','','','���������������','','SQG',0,'��','','S','U','S','A','R','�','ExistCpo("SQG")','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','07','PA2_NMCAND','C',50,0,'Nome','Nome','Nome','Nome do Candidato','Nome do Candidato','Nome do Candidato','','','���������������','POSICIONE("SQG",1,XFILIAL("SQG")+M->PA2_CDCAND,"QG_NOME")','',0,'��','','','U','S','V','V','','','','','','','','POSICIONE("SQG",1,XFILIAL("SQG")+M->PA2_CDCAND,"QG_NOME")','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','08','PA2_CPFCAN','C',14,0,'CPF','CPF','CPF','CPF do Candidato','CPF do Candidato','CPF do Candidato','','','���������������','POSICIONE("SQG",1,XFILIAL("SQG")+M->PA2_CDCAND,"QG_CIC")','',0,'��','','','U','S','V','V','','','','','','','','POSICIONE("SQG",1,XFILIAL("SQG")+M->PA2_CDCAND,"QG_CIC")','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','09','PA2_DTAPRV','D',8,0,'Data','Data','Data','Data da Aprova��o','Data da Aprova��o','Data da Aprova��o','','','���������������','','',0,'��','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','10','PA2_LOGTOR','C',6,0,'Gestor','Gestor','Gestor','Login do Gestor','Login do Gestor','Login do Gestor','','','���������������','','',0,'��','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','11','PA2_SIT','C',2,0,'Situa��o','Situa��o','Situa��o','Situa��o','Situa��o','Situa��o','','','���������������','"AG"','',0,'��','','','U','N','V','R','','','','','','','','"AG"','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','12','PA2_DTMOV','D',8,0,'Data Mov','Data Mov','Data Mov','Data Movimenta��o','Data Movimenta��o','Data Movimenta��o','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','13','PA2_FILSOL','C',8,0,'FilSolic','FilSolic','FilSolic','Filial Solicita��o','Filial Solicita��o','Filial Solicita��o','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','033','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','14','PA2_SOL','C',5,0,'Solicitacao','Solicitacao','Solicitacao','Solicitacao','Solicitacao','Solicitacao','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','','2017011317:34:22'})
aAdd(aSX3,{'PA2','15','PA2_FILCAN','C',8,0,'FilCandidato','FilCandidato','FilCandidato','Filial Candidato','Filial Candidato','Filial Candidato','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA7','01','PA7_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2017011317:34:22'})
aAdd(aSX3,{'PA7','02','PA7_CODIGO','C',3,0,'Codigo','Codigo','Codigo','Codigo do tipo','Codigo do tipo','Codigo do tipo','@!','','���������������','GETSX8NUM("PA7","PA7_CODIGO")','',0,'��','','','U','S','V','R','�','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'PA7','03','PA7_DESCR','C',30,0,'Descricao','Descricao','Descricao','Descri��o do tipo','Descri��o do tipo','Descri��o do tipo','@!','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'RH3','25','RH3_XTPCTM','C',3,0,'Tipo Solicit','Tipo Solicit','Tipo Solicit','Tipo Solicit customizada','Tipo Solicit customizada','Tipo Solicit customizada','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'SQG','71','QG_XCODFAP','C',5,0,'CodFAP','CodFAP','CodFAP','CodFAP','CodFAP','CodFAP','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX3,{'SQG','72','QG_XFILFAP','C',8,0,'FilialFAP','FilialFAP','FilialFAP','FilialFAP','FilialFAP','FilialFAP','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017011317:34:22'})
aAdd(aSX6,{'','FS_DIREXE','C','DIRETOR EXECUTIVO','','','','','','','','','','','','U','','','','','','','2017011317:34:22'})
aAdd(aSX6,{'','FS_RHDED','C','RH DEDICADO','','','','','','','','','','','','U','','','','','','','2017011317:34:22'})
aAdd(aSX6,{'','FS_SEGINF','C','SEGURANCA INFORMACAO','','','','','','','','','','','','U','','','','','','','2017011317:34:22'})
aAdd(aSX7,{'PA2_CDCAND','001','SQG->QG_NOME','PA2_NMCAND','X','N','',0,'','','U','2017011317:34:22'})
aAdd(aSX7,{'PA2_CDCAND','002','SQG->QG_CIC','PA2_CPFCAN','X','N','',0,'','','U','2017011317:34:22'})
aAdd(aSX7,{'PA2_CDVAGA','001','SQS->QS_VCUSTO','PA2_VLVAGA','X','N','',0,'','','U','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','1','01','RE','SQS','SQS','SQS','SQS','','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','2','01','01','','','','U_F0500304()','','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','5','01','','','','','SQS->QS_VAGA','','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','5','02','','','','','SQS->QS_FILIAL','','2017011317:34:22'})
aAdd(aSXB,{'FSWPA7','2','01','01','Codigo','Codigo','Codigo','','','2017011317:34:22'})
aAdd(aSXB,{'FSWPA7','4','01','01','Codigo','Codigo','Codigo','PA7_CODIGO','','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','5','01','','','','','SQS->QS_VAGA','','2017011317:34:22'})
aAdd(aSXB,{'FSSQS2','5','02','','','','','SQS->QS_FILIAL','','2017011317:34:22'})
aAdd(aSX3Hlp,{'PA2_SIT','AG = "Aguardando Aprova��o", AP  ="Aprovado", RE = "Recusado", CL = "Concluido", RI = "Recrutamento Interno"'})
aAdd(aSX3Hlp,{'PA2_DTMOV','Data Movimenta��o'})
aAdd(aSX3Hlp,{'PA7_CODIGO','Codigo do tipo de solicita��o'})
aAdd(aSX3Hlp,{'PA7_DESCR','Descri��o do tipo de solicita��o'})
aAdd(aSX3Hlp,{'RH3_XTPCTM','Tipo de solicita��o customizada portal'})
aAdd(aCarga,{'U_C050031(lReadOnly)'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
