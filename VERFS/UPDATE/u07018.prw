/*
{Protheus.doc} U07018
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 27/12/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423041_EF_18
@Obs Fontes: F0701301.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07018(lOnlyInfo)
Local aInfo := {'07','018','VALIDA��O TUSS CADASTRO DE PRODUTOS','27/12/16','15:39','008679172110800257U1123','24/01/17','18:23','679123025211'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'SB1','S5','B1_XTUSS','C',40,0,'Cod. TUSS','Cod. TUSS','Cod. TUSS','C�digo TUSS','C�digo TUSS','C�digo TUSS','','','���������������','','FSWP15',0,'��','','S','U','N','A','R','','Vazio() .or. ExistCpo("P15")','','','','','','','','','','','','','N','N','','','','2016122715:38:02'})
aAdd(aSX3,{'SB1','S6','B1_XTUSDTI','D',8,0,'Ini. Vig','Ini. Vig','Ini. Vig','In�cio Vig�ncia TUSS','In�cio Vig�ncia TUSS','In�cio Vig�ncia TUSS','','','���������������','','',0,'��','','','U','N','V','V','','','','','','','','','','','','','','','N','N','','','','2016122715:38:02'})
aAdd(aSX3,{'SB1','S7','B1_XTUSDTF','D',8,0,'Fim Vig.','Fim Vig.','Fim Vig.','Fim Vig�ncia TUSS','Fim Vig�ncia TUSS','Fim Vig�ncia TUSS','','','���������������','','',0,'��','','','U','N','V','V','','','','','','','','','','','','','','','N','N','','','','2016122715:38:02'})
aAdd(aSX3,{'SB1','S8','B1_XID','C',36,0,'ID','ID','ID','ID','ID','ID','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016122715:38:02'})
aAdd(aSX7,{'B1_XTUSS','001','P15->P15_DTVIN','B1_XTUSDTI','P','S','P15',1,'XFilial("P15") + M->B1_XTUSS','','U','2016122715:38:06'})
aAdd(aSX7,{'B1_XTUSS','002','P15->P15_DTVFIM','B1_XTUSDTF','P','S','P15',1,'XFilial("P15") + M->B1_XTUSS','','U','2016122715:38:06'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
