/*
{Protheus.doc} U06009
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 10/11/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423040_EF_009
@Obs Fontes: F0600901.PRW
*/
#INCLUDE 'PROTHEUS.CH'
User Function U06009(lOnlyInfo)
Local aInfo := {'06','009','TABELA DE LOG INTEGRA�AO RH','10/11/16','13:53','009603062110900136U0115','06/01/17','10:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'PA6','1','PA6_FILIAL+PA6_ID','Filial+ID','Filial+ID','Filial+ID','U','','','S','2016111013:53:33'})
aAdd(aSX2,{'PA6','','PA6990','LOG DE INTEGRA��ES','LOG DE INTEGRA��ES','LOG DE INTEGRA��ES','','E','E','E',0,'','','',0,'PA6_FILIAL+PA6_COD','','','1','2','',0,0,0,'2016111013:53:32'})
aAdd(aSX3,{'PA6','01','PA6_FILIAL','C',2,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','02','PA6_ID','C',36,0,'ID','ID','ID','C�digo da Empresa','C�digo da Empresa','C�digo da Empresa','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','03','PA6_DATA','D',8,0,'Data','Data','Data','Data da Grava��o','Data da Grava��o','Data da Grava��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','04','PA6_HORA','C',8,0,'Hora','Hora','Hora','Hora da Grava��o','Hora da Grava��o','Hora da Grava��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','1',,'2016111013:53:29'})
aAdd(aSX3,{'PA6','05','PA6_RECNOT','N',18,0,'RECNOTAB','RECNOTAB','RECNOTAB','Recno Registro','Recno Registro','Recno Registro','@E 999999999999999999','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','06','PA6_USUA','C',6,0,'Usuario','Usuario','Usuario','C�digo do usu�rio','C�digo do usu�rio','C�digo do usu�rio','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','07','PA6_FUNC','C',10,0,'Fun��o','Fun��o','Fun��o','Nome da Fun��o','Nome da Fun��o','Nome da Fun��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','08','PA6_ALIAS','C',3,0,'Alias','Alias','Alias','Nome da Tabela','Nome da Tabela','Nome da Tabela','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','09','PA6_CHALIA','C',100,0,'ChAlias','ChAlias','ChAlias','Chave do Alias','Chave do Alias','Chave do Alias','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','10','PA6_DATENV','D',8,0,'Data Envio','Data Envio','Data Envio','Data do Envio','Data do Envio','Data do Envio','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','11','PA6_OPERAC','C',6,0,'Opera�ao','Opera�ao','Opera�ao','Opera�ao','Opera�ao','Opera�ao','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3,{'PA6','12','PA6_OBS','C',120,0,'Observa�ao','Observa�ao','Observa�ao','Observa��o','Observa��o','Observa��o','','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016111013:53:29'})
aAdd(aSX3Hlp,{'PA6_ID','C�digo sequencial que ser� geradoautomaticamente'})
aAdd(aSX3Hlp,{'PA6_DATA','Data que o registro foi gravado'})
aAdd(aSX3Hlp,{'PA6_HORA','Hora que o Registro foi Gravado'})
aAdd(aSX3Hlp,{'PA6_RECNOT','Digite Recno do Registro Integrado'})
aAdd(aSX3Hlp,{'PA6_USUA','C�digo do usu�rio no Protheus'})
aAdd(aSX3Hlp,{'PA6_FUNC','Nome da Fun�ao que chamou a RotinaHistorico de log de integra�ao'})
aAdd(aSX3Hlp,{'PA6_ALIAS','Nome da tabela, informa�ao passada comoparametro ao chamar a rotina dehistorico de log'})
aAdd(aSX3Hlp,{'PA6_CHALIA','Digite a Chave do Alias'})
aAdd(aSX3Hlp,{'PA6_DATENV','Data do Envio da Integra�ao'})
aAdd(aSX3Hlp,{'PA6_OPERAC','Opera�ao'})
aAdd(aSX3Hlp,{'PA6_OBS','Obverva�ao'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
