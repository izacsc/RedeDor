/*
{Protheus.doc} U01010
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 26/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_010
@Obs Fontes: F0101001.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U01010(lOnlyInfo)
Local aInfo := {'01','010','CAMPOS E PARAMETRO DE RECUSA','26/01/17','19:07','000767112010000191U1120','27/01/17','17:32','767120019201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P00','1','P00_FILIAL+P00_NUM+P00_PREFIX+P00_DTRECU+P00_USRREC','FILIAL+NUM+PREFIXO+USUARIO RECUSA','FILIAL+NUM+PREFIXO+USUARIO RECUSA','FILIAL+NUM+PREFIXO+USUARIO RECUSA','U','','','S','2017012619:04:32'})
aAdd(aSX1,{'FSW0101003','01','Empresa de','','','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','','','','','','','','','','','','','','','','','','','','','','SM0','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','02','Empresa Ate','','','MV_CH0','C',8,0,0,'G','','MV_PAR02','','','','ZZZZZZZZ','','','','','','','','','','','','','','','','','','','','','SM0','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','03','Data de Recusa de:','','','MV_CH0','D',8,0,0,'G','','MV_PAR03','','','','20160501','','','','','','','','','','','','','','','','','','','','','','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','04','Data de Recusa ate:','','','MV_CH0','D',8,0,0,'G','','MV_PAR04','','','','20171231','','','','','','','','','','','','','','','','','','','','','','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','05','Prefixo de:','','','MV_CH0','C',3,0,0,'G','','MV_PAR05','','','','','','','','','','','','','','','','','','','','','','','','','FSP001','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','06','Titulo de:','','','MV_CH0','C',10,0,0,'G','','MV_PAR06','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','07','Prefixo ate:','','','MV_CH0','C',3,0,0,'G','','MV_PAR07','','','','ZZZ','','','','','','','','','','','','','','','','','','','','','FSP001','','','','','','2017012619:04:40'})
aAdd(aSX1,{'FSW0101003','08','Titulo ate:','','','MV_CH0','C',10,0,0,'G','','MV_PAR08','','','','ZZZZZZZZZZ','','','','','','','','','','','','','','','','','','','','','','','','','','','2017012619:04:40'})
aAdd(aSX2,{'P00','','P00010','TITULOS RECUSADOS','TITULOS RECUSADOS','TITULOS RECUSADOS','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2017012619:04:31'})
aAdd(aSX3,{'P00','01','P00_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','02','P00_NUM','C',9,0,'Tνtulo','Tνtulo','Tνtulo','Tνtulo Financeiro a Pagar','Tνtulo Financeiro a Pagar','Tνtulo Financeiro a Pagar','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','03','P00_PREFIX','C',3,0,'Prefixo','Prefixo','Prefixo','Prefixo Tνtulo Financeiro','Prefixo Tνtulo Financeiro','Prefixo Tνtulo Financeiro','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','04','P00_VALOR','N',16,2,'Valor','Valor','Valor','Valor do Tνtulo','Valor do Tνtulo','Valor do Tνtulo','@E 9,999,999,999,999.99','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','05','P00_XDTVEN','D',8,0,'Vencimento','Vencimento','Vencimento','Data de Vencimento','Data de Vencimento','Data de Vencimento','','',' ','','',0,'ώΐ','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','06','P00_FORNEC','C',6,0,'Fornecedor','Fornecedor','Fornecedor','Codigo do Fornecedor','Codigo do Fornecedor','Codigo do Fornecedor','@!','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','001','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','07','P00_DFORNE','C',20,0,'Nome Fornece','Nome Fornece','Nome Fornece','Nome do fornecedor','Nome do fornecedor','Nome do fornecedor','@!','',' ','POSICIONE("SA2",1,XFILIAL("SA2") + SF1->(F1_FORNECE + F1_LOJA),"A2_NREDUZ")','',0,'ώΐ','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','08','P00_USREMI','C',6,0,'Usr.Emitente','Usr.Emitente','Usr.Emitente','Usuαrio Emitente','Usuαrio Emitente','Usuαrio Emitente','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','09','P00_DUSEMI','C',20,0,'Nome Eminent','Nome Eminent','Nome Eminent','Nome Eminente','Nome Eminente','Nome Eminente','','',' ','','',0,'ώΐ','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','10','P00_USRREC','C',6,0,'Usr. Recusa','Usr. Recusa','Usr. Recusa','Usuαrio que recusou o Tit','Usuαrio que recusou o Tit','Usuαrio que recusou o Tit','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','11','P00_DUSREC','C',20,0,'Nome Recusa','Nome Recusa','Nome Recusa','Nome Recusa','Nome Recusa','Nome Recusa','','',' ','','',0,'ώΐ','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','12','P00_MOTIVO','M',10,0,'Motivo','Motivo','Motivo','Motivo da Recusa','Motivo da Recusa','Motivo da Recusa','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','13','P00_DTRECU','D',8,0,'DT. Recusa','DT. Recusa','DT. Recusa','Data Recusa','Data Recusa','Data Recusa','','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'P00','14','P00_ORIGEM','C',8,0,'Origem Tit','Origem Tit','Origem Tit','Origem Tνtulo','Origem Tνtulo','Origem Tνtulo','','',' ','','',0,'ώΐ','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:25'})
aAdd(aSX3,{'SE2','02','E2_XSTRECU','C',1,0,'Status Recus','Status Recus','Status Recus','Status da Recusa','Status da Recusa','Status da Recusa','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','Pertence(RC)','R=Recusado;C=Corrigido','','','','','','','','','','','','N','N','','','','2017012619:04:27'})
aAdd(aSX3,{'SE2','N4','E2_XDTRECU','D',8,0,'Data Recusa','Data Recusa','Data Recusa','Data da Recusa','Data da Recusa','Data da Recusa','','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017012619:04:27'})
aAdd(aSX6,{'','FS_MAILREC','C','Conta de email Recusa de Titulo','','','','','','','','','','','','U','','','','','','','2017012619:04:39'})
aAdd(aSXB,{'FSP001','1','01','DB','Tνtulos recusados','Tνtulos recusados','Tνtulos recusados','P00','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','2','01','01','Filial+num+prefixo+u','Filial+num+prefixo+u','Filial+num+prefixo+u','','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','4','01','01','Filial','Sucursal','Branch','P00_FILIAL','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','4','01','02','Tνtulo','Tνtulo','Tνtulo','P00_NUM','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','4','01','03','Valor','Valor','Valor','P00_VALOR','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','4','01','04','Fornecedor','Fornecedor','Fornecedor','P00_FORNEC','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','5','01','','','','','P00->P00_PREFIX','','2016060108:00:00'})
aAdd(aSXB,{'FSP001','5','02','','','','','P00->P00_NUM','','2016060108:00:00'})
aAdd(aSX3Hlp,{'E2_XSTRECU','Status da Recusa'})
aAdd(aSX3Hlp,{'E2_XDTRECU','Data da Recusa'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
