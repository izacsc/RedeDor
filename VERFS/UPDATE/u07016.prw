/*
{Protheus.doc} U07016
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 27/12/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423041_EF_16
@Obs Fontes: F0701601.PRW
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07016(lOnlyInfo)
Local aInfo := {'07','016','CADASTRO TUSS','27/12/16','15:37','006677172110600257U1123','01/02/17','15:15',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P15','1','P15_FILIAL+P15_COD+P15_DESCR','Cod. TUSS+Des. TUSS','Cod. TUSS+Des. TUSS','Cod. TUSS+Des. TUSS','U','','','N','2016122715:36:29'})
aAdd(aSX2,{'P15','','P15010','CADASTRO TUSS','CADASTRO TUSS','CADASTRO TUSS','','C','C','C',0,'','','',0,'','','','','','',0,0,0,'2016122715:36:28'})
aAdd(aSX3,{'P15','01','P15_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','�','','','','','','','','033','','','','','','','','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','02','P15_COD','C',40,0,'Cod. TUSS','Cod. TUSS','Cod. TUSS','C�digo TUSS','C�digo TUSS','C�digo TUSS','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','03','P15_DESCR','C',254,0,'Des. TUSS','Des. TUSS','Des. TUSS','Descri�ao TUSS','Descri�ao TUSS','Descri�ao TUSS','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','04','P15_APRES','C',254,0,'Apresent.','Apresent.','Apresent.','Apresenta��o','Apresenta��o','Apresenta��o','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','05','P15_ANVISA','C',40,0,'Cod. ANVISA','Cod. ANVISA','Cod. ANVISA','C�digo ANVISA','C�digo ANVISA','C�digo ANVISA','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','06','P15_REFCOD','C',40,0,'Ref. TUSS','Ref. TUSS','Ref. TUSS','Refer�ncia TUSS','Refer�ncia TUSS','Refer�ncia TUSS','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','07','P15_REFER','C',254,0,'Ref. Fabr.','Ref. Fabr.','Ref. Fabr.','Refer�ncia Fabricante','Refer�ncia Fabricante','Refer�ncia Fabricante','','','���������������','','',0,'��','','','U','S','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','08','P15_DTVIN','D',8,0,'Ini. Vig.','Ini. Vig.','Ini. Vig.','Data de In�cio da Vig�ncia','Data de In�cio da Vig�ncia','Data de In�cio da Vig�ncia','','','���������������','','',0,'��','','','U','N','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','09','P15_DTVFIM','D',8,0,'Fim Vig.','Fim. Vig.','Fim Vig.','Data de Fim da Vig�ncia','Data de Fim da Vig�ncia','Data de Fim da Vig�ncia','','','���������������','','',0,'��','','','U','N','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','10','P15_STATUS','C',1,0,'Status','Status','Status','Status','Status','Status','','','���������������','','',0,'��','','','U','N','A','R','�','','1=Ativo;2=Inativo','1=Ativo;2=Inativo','1=Ativo;2=Inativo','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSX3,{'P15','11','P15_ID','C',36,0,'ID','ID','ID','ID','ID','ID','','','���������������','','',0,'��','','','U','N','A','R','�','','','','','','','','','','','','','','N','N','','','','2016122715:36:24'})
aAdd(aSXB,{'FSWP15','1','01','DB','TUSS','TUSS','TUSS','P15','','2016122715:36:32'})
aAdd(aSXB,{'FSWP15','2','01','01','Cod. TUSS+des. Tuss','Cod. TUSS+des. Tuss','Cod. TUSS+des. Tuss','','','2016122715:36:32'})
aAdd(aSXB,{'FSWP15','4','01','01','Cod. TUSS','Cod. TUSS','Cod. TUSS','P15_COD','','2016122715:36:32'})
aAdd(aSXB,{'FSWP15','4','01','02','Des. TUSS','Des. TUSS','Des. TUSS','P15_DESCR','','2016122715:36:32'})
aAdd(aSXB,{'FSWP15','5','01','','','','','P15->P15_COD','','2016122715:36:32'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
