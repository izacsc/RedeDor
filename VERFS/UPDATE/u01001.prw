/*
{Protheus.doc} U01001
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 08/09/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_001
@Project MAN00000462901_EF_001 ACABCONTCABEALHO DOS CONTRATOS ENCONTRADOS APRODGCTPRODUTOS ENCONTRADOS NA SOLICITA�O DE COMPRA
@Project N�O IDENTIFICADO
@Project MAN00000462901_EF_009
@Obs Fontes: F0100101.PRW,F0100102.PRW,F0100103.PRW,F0100104.PRW,F0100105.PR
@Obs Fontes: F0100106.PRW,F0100107.PRW,F0100108.PRW,F0100109.PRW,F0100110.PR
@Obs Fontes: F0100901.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U01001(lOnlyInfo)
Local aInfo := {'01','001','MEDI�AO AUTOMATICA','08/09/16','09:43','001683012000100991U0104','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P07','1','P07_FILIAL+P07_COD','C�digo Log','C�digo Log','C�digo Log','U','','','N','2016090809:35:56'})
aAdd(aSIX,{'P07','2','P07_FILIAL+P07_CONTR','Nr. Contrato','Nr. Contrato','Nr. Contrato','U','','','N','2016090809:35:56'})
aAdd(aSIX,{'P07','3','P07_FILIAL+P07_CONTR+P07_SOLCO','Nr. Contrato+Numero da SC','Nr. Contrato+Numero da SC','Nr. Contrato+Numero da SC','U','','','N','2016090809:35:56'})
aAdd(aSIX,{'P07','4','P07_FILIAL+P07_COD+P07_CONTR','C�digo Log+Nr. Contrato','C�digo Log+Nr. Contrato','C�digo Log+Nr. Contrato','U','','','N','2016090809:35:56'})
aAdd(aSIX,{'P07','5','P07_FILIAL+P07_COD+P07_SOLCO+P07_CONTR','C�digo Log+Numero da SC+Nr. Contrato','C�digo Log+Numero da SC+Nr. Contrato','C�digo Log+Numero da SC+Nr. Contrato','U','','','N','2016090809:35:56'})
aAdd(aSX2,{'P07','','P07010','TABELA DE LOG DE MEDI��O AUTOM','TABELA DE LOG DE MEDI��O AUTOM','TABELA DE LOG DE MEDI��O AUTOM','','C','C','C',0,'','','',0,'','','','','','',0,0,0,'2016090809:35:51'})
aAdd(aSX3,{'ACU','09','ACU_XESTSE','N',12,2,'Seguranca','Seguranca','Seguranca','Estoque de Seguranca','Estoque de Seguranca','Estoque de Seguranca','@E 999,999,999.99','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016090809:35:01'})
aAdd(aSX3,{'ACU','10','ACU_XPE','N',5,0,'Lead Time','Lead Time','Lead Time','Lead Time','Lead Time','Lead Time','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016090809:35:01'})
aAdd(aSX3,{'ACU','11','ACU_XFREQ','N',5,0,'Frequ�ncia','Frequ�ncia','Frequ�ncia','Frequ�ncia de an�lise','Frequ�ncia de an�lise','Frequ�ncia de an�lise','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016090809:35:01'})
aAdd(aSX3,{'ACU','12','ACU_CTRLP','C',1,0,'Cont. Planej','Cont. Planej','Cont. Planej','Controle Planejamento','Controle Planejamento','Controle Planejamento','','','���������������','','',0,'��','','','U','N','A','R','�','Pertence(" 12")','1=Sim;2=N�o','','','','','','','','','','','','N','N','','','','2016090809:35:01'})
aAdd(aSX3,{'CNA','23','CNA_XTABPC','C',3,0,'Tab.Pre�o','Tab.Pre�o','Tab.Pre�o','Tabela de Pre�o','Tabela de Pre�o','Tabela de Pre�o','@!','','���������������','','FSCNA1',0,'��','','','U','S','A','R','','U_F0100103()','','','','','','','','','','','','','N','N','','','','2016090809:35:12'})
aAdd(aSX3,{'P07','01','P07_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','02','P07_COD','C',6,0,'C�digo Log','C�digo Log','C�digo Log','C�digo do Log','C�digo do Log','C�digo do Log','@!','','���������������','GETSXENUM("P07","P07_COD")','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','03','P07_CONTR','C',15,0,'Nr. Contrato','Nr. Contrato','Nr. Contrato','Numero do Contrato','Numero do Contrato','Numero do Contrato','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','04','P07_SOLCO','C',6,0,'Numero da SC','Numero da SC','Numero da SC','Numero da Solic.de Compra','Numero da Solic.de Compra','Numero da Solic.de Compra','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','05','P07_TIPSOL','C',2,0,'Tipo da SC','Tipo da SC','Tipo da SC','Tipo da Solic. de Compra','Tipo da Solic. de Compra','Tipo da Solic. de Compra','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','06','P07_SETSOL','C',2,0,'Setor da SC','Setor da SC','Setor da SC','Setor da Solic. de Compra','Setor da Solic. de Compra','Setor da Solic. de Compra','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','07','P07_CCSOL','C',9,0,'Centro Custo','Centro Custo','Centro Custo','Centro de Custo','Centro de Custo','Centro de Custo','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','08','P07_SOLIC','C',25,0,'Solicitante','Solicitante','Solicitante','Nome do Solicitante','Nome do Solicitante','Nome do Solicitante','@S25','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','09','P07_MOTSOL','C',110,0,'Mot. da SC','Mot. da SC','Mot. da SC','Motivo da Solicita��o','Motivo da Solicita��o','Motivo da Solicita��o','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','10','P07_ITEMSC','C',15,0,'Produto','Produto','Produto','Codigo do Produto','Codigo do Produto','Codigo do Produto','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','11','P07_FORNEC','C',6,0,'Cod.Fornec.','Cod.Fornec.','Cod.Fornec.','Codigo do Fornecedor','Codigo do Fornecedor','Codigo do Fornecedor','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','12','P07_LOJFOR','C',2,0,'Loja Fornec.','Loja Fornec.','Loja Fornec.','Loja do Fornecedor','Loja do Fornecedor','Loja do Fornecedor','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','13','P07_DESCFO','C',40,0,'Nome Fornec.','Nome Fornec.','Nome Fornec.','Nome ou Razao Social','Nome ou Razao Social','Nome ou Razao Social','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','14','P07_DESCR','C',40,0,'Desc.Status','Desc.Status','Desc.Status','Descri��o do Status','Descri��o do Status','Descri��o do Status','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','15','P07_DTMED','D',8,0,'Dt.Medi��o','Dt.Medi��o','Dt.Medi��o','Data da Medi��o','Data da Medi��o','Data da Medi��o','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','16','P07_DTVIGI','D',8,0,'Dt.Ini.Vig.','Dt.Ini.Vig.','Dt.Ini.Vig.','Data Inicio de vigencia','Data Inicio de vigencia','Data Inicio de vigencia','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'P07','17','P07_DTVIGF','D',8,0,'Dt.Fim.Vig.','Dt.Fim.Vig.','Dt.Fim.Vig.','Data Fim de Vigencia','Data Fim de Vigencia','Data Fim de Vigencia','','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:33'})
aAdd(aSX3,{'SA2','04','A2_NOME','C',40,0,'Raz�o Social','Raz�o Social','Raz�o Social','Nome ou Razao Social','Nombre o razon social','Name or Company Name','@!','','���������������','','',1,'��','','','','S','','','','texto()','','','','','','','','1','S','','','S','N','N','','','1','2016090809:35:36'})
aAdd(aSX3,{'SA2','05','A2_NREDUZ','C',20,0,'N Fantasia','Nom Fantasia','Trade Name','Nome de Fantasia','Nombre Fantasia','Trade Name','@!','','���������������','','',1,'��','','','','S','','','','texto()','','','','','','','','1','S','','','S','N','N','','','1','2016090809:35:36'})
aAdd(aSX3,{'SB1','03','B1_DESC','C',30,0,'Descricao','Descripcion','Description','Descricao do Produto','Descripcion del Producto','Description of Product','@!','A010VlStr()','���������������','','',1,'��','','S','','S','','','','texto()','','','','','','','','1','S','','','S','N','N','','','1','2016122914:26:20'})
aAdd(aSX3,{'SBZ','12','BZ_ESTSEG','N',12,2,'Seguranca','Seguridad','Safety Inv.','Estoque de Seguranca','Stock de Seguridad','Security Inventory','@E 999,999,999.99','','���?�����������','','',1,'��','','','','N','','','','Positivo()','','','','','','','','1','S','','','N','N','N','','','1','2016090809:35:37'})
aAdd(aSX3,{'SBZ','15','BZ_PE','N',5,0,'Entrega','Entrega','Deliv.Term','Prazo de Entrega','Plazo de Entrega','Delivery Term','@E 99999','Positivo()','���������������','','',1,'��','','','','N','','','','Positivo()','','','','','','','','1','S','','','N','N','N','','','1','2016090809:35:37'})
aAdd(aSX3,{'SBZ','66','BZ_XFREQAN','N',5,0,'Frequ�ncia','Frequ�ncia','Frequ�ncia','Frequ�ncia de an�lise','Frequ�ncia de an�lise','Frequ�ncia de an�lise','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX3,{'SC1','06','C1_DESCRI','C',30,0,'Descricao','Descripcion','Description','Descricao do Produto','Descripcion del Producto','Description of Product','@!','','���������������','','',1,'��','','','','N','V','R','','texto()','','','','','','','','','S','','','N','N','N','','','1','2016090809:35:37'})
aAdd(aSX3,{'SC1','95','C1_XMOTIVO','C',2,0,'Motivo','Motivo','Motivo','Motivo','Motivo','Motivo','@!','','���������������','IIF(TYPE("C") == "C" .AND. C<>"",ACOLS[N-1][GDFIELDPOS("C1_XMOTIVO")]," ")','',0,'��','','S','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016110719:40:49'})
aAdd(aSX3,{'SC1','96','C1_XTPSC','C',2,0,'Tipo SC','Tipo SC','Tipo SC','Tipo SC','Tipo SC','Tipo SC','@!','','���������������','IIF(TYPE("C") == "C" .AND. C<>"",ACOLS[N-1][GDFIELDPOS("C1_XTPSC")]," ")','',0,'��','','S','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX3,{'SC1','97','C1_XDESCTP','C',40,0,'Desc. Tipo','Desc. Tipo','Desc. Tipo','Desc. Tipo','Desc. Tipo','Desc. Tipo','@!','','���������������','IIF(!EMPTY(SC1->C1_XTPSC) .AND. !INCLUI,POSICIONE("SX5",1,XFILIAL("SX5")+"ZX" + SC1->C1_XTPSC,"X5_DESCRI"),"")','',0,'��','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX3,{'SC1','99','C1_XDESMOT','C',40,0,'Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','@!','','���������������','IIF(!EMPTY(SC1->C1_XMOTIVO) .AND. !INCLUI,POSICIONE("SX5",1,XFILIAL("SX5")+"ZZ"+ SC1->C1_XMOTIVO,"X5_DESCRI")," ")','',0,'��','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX3,{'SC1','9A','C1_XITMED','C',40,0,'Stat.Medi��o','Stat.Medi��o','Stat.Medi��o','Status Medi��o','Status Medi��o','Status Medi��o','@!','','���������������','','',0,'��','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX3,{'SC7','H6','C7_XUSR','C',6,0,'Emitente','Emitente','Emitente','Emitente','Emitente','Emitente','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016090809:35:37'})
aAdd(aSX6,{'','FS_XTPSCCA','C','Tipos de Solicita��o de Compras que N�O ter�o','','','aprova��o de Al�adas','','','','','','02','02','02','U','','','','','','','2016090809:36:18'})
aAdd(aSX6,{'','FS_XTPSCME','C','Tipos de Solicita��o de Compras que N�O ser�o','','','considerados pela rotina de Medi��o Automatizada','','','','','','01','01','01','U','','','','','','','2016090809:36:18'})
aAdd(aSX7,{'C1_XMOTIVO','001','SX5->X5_DESCRI','C1_XDESMOT','P','S','SX5',1,'XFILIAL("SX5")+"ZZ"+M->C1_XMOTIVO','','U','2016090809:36:00'})
aAdd(aSX7,{'C1_XTPSC','001','SX5->X5_DESCRI','C1_XDESCTP','P','S','SX5',1,'XFILIAL("SX5")+"ZX"+M->C1_XTPSC','','U','2016090809:36:00'})
aAdd(aSXB,{'FSCNA1','1','01','RE','Tabela de Pre�o','Tabela de Pre�o','Tabela de Pre�o','AIA','','2016090809:36:09'})
aAdd(aSXB,{'FSCNA1','2','01','01','','','','.T.','','2016090809:36:09'})
aAdd(aSXB,{'FSCNA1','5','01','','','','','U_F0100102()','','2016090809:36:09'})
aAdd(aSXB,{'FS_P07','1','01','DB','Medi��o Automatizada','Medi��o Automatizada','Medi��o Automatizada','P07','','2016090809:36:10'})
aAdd(aSXB,{'FS_P07','2','01','04','C�digo Log+nr. Contr','C�digo Log+nr. Contr','C�digo Log+nr. Contr','','','2016090809:36:10'})
aAdd(aSXB,{'FS_P07','4','01','01','C�digo Log','C�digo Log','C�digo Log','P07_COD','','2016090809:36:10'})
aAdd(aSXB,{'FS_P07','4','01','02','Desc.Status','Desc.Status','Desc.Status','P07_DESCR','','2016090809:36:10'})
aAdd(aSXB,{'FS_P07','5','01','','','','','P07->P07_COD','','2016090809:36:10'})
aAdd(aSX3Hlp,{'ACU_XESTSE','Estoque de Seguran�a. Quantidadem�nimade produto em estoque para evitarafalta do mesmo entre a solicita��odecompra ou produ��o e o seurecebimento.'})
aAdd(aSX3Hlp,{'ACU_XPE','Lead Time'})
aAdd(aSX3Hlp,{'ACU_XFREQ','Frequ�ncia de an�lise'})
aAdd(aSX3Hlp,{'ACU_CTRLP','Controle Planejamento'})
aAdd(aSX3Hlp,{'A2_NOME','Nome ou raz�o social do fornecedor.'})
aAdd(aSX3Hlp,{'A2_NREDUZ','Nome pelo qual o fornecedor � conhecido.Auxilia nas consultas e nos  relat�riosdo sistema.'})
aAdd(aSX3Hlp,{'B1_DESC','Descri��o do produto.'})
aAdd(aSX3Hlp,{'BZ_ESTSEG','Estoque de seguran�a.Quantidadem�nimadeproduto em estoquepara evitara falta domesmo entre asolicita��o decompra ou   produ��o e oseu recebimento.'})
aAdd(aSX3Hlp,{'BZ_PE','Prazo de entrega do produto. �on�merodedias, meses ou anos queofornecedor ou af�brica necessitaparaentregar o        produto, a partirdorecebimento de seu  pedido.'})
aAdd(aSX3Hlp,{'BZ_XFREQAN','Frequ�ncia de an�lise'})
aAdd(aSX3Hlp,{'C1_DESCRI','Descri��o do produto. O m�dulo decompras utiliza esta informa��o paraidentifica��o do material. Estafuncionalidade � muito ut�l para assolicita��es de material de consumo ouimobilizado, quando o usu�rio n�o desejacriar um c�digo de produto para o mesmo,desta forma, o usu�rio informa um c�digode produto gen�rico ( note que esteproduto somente deve ser utilizado paraesta finalidade ) e altera sua descri��oespecificando o produto solicitado.'})
aAdd(aSX3Hlp,{'C1_XMOTIVO','Motivo'})
aAdd(aSX3Hlp,{'C1_XTPSC','Tipo de Compra'})
aAdd(aSX3Hlp,{'C7_XUSR','Emitente'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
