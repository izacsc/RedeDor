/*
{Protheus.doc} U03003
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 30/11/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463701_EF_003
@Obs Fontes: F0300301.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U03003(lOnlyInfo)
Local aInfo := {'03','003','RELATORIO PLANO DE SUCESSAO','30/11/16','13:59','003609032110300133U0135','29/12/16','14:29',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0300301','01','Filial ?','Filial ?','Filial ?','MV_CH1','C',Len(SRA->RA_FILIAL),0,0,'G','','MV_PAR01','','','','01','','','','','','','','','','','','','','','','','','','','','SM0','','','.RHFILIAL.','','','2016113013:58:22'})
aAdd(aSX1,{'FSW0300301','02','Matricula ?','Matricula ?','Matricula ?','MV_CH2','C',60,0,0,'R','','MV_PAR02','','','','RDD_CODADO','','','','','000169;000181;000158;','','','','','','','','','','','','','','','','SRA','','','.RHMATRIC.','','','2016113013:58:22'})
aAdd(aSX1,{'FSW0300301','03','1a.Avaliacao (y)?','1a.Avaliacao ?','1a.Avaliacao ?','MV_CH3','C',6,0,0,'G','NaoVazio()','MV_PAR03','','','','000001','','','','','','','','','','','','','','','','','','','','','RD6','','','','','','2016113013:58:22'})
aAdd(aSX1,{'FSW0300301','04','2a.Avaliacao (x)?','2a.Avaliacao ?','2a.Avaliacao ?','MV_CH4','C',6,0,0,'G','NaoVazio()','MV_PAR04','','','','000003','','','','','','','','','','','','','','','','','','','','','RD6','','','','','','2016113013:58:22'})
aAdd(aSX1,{'FSW0300301','05','Tipo de Geracao ?','Tipo de Geracao ?','Tipo de Geracao ?','MV_CH5','N',1,0,1,'C','','MV_PAR05','1-Gerar Excel','1-Gerar Excel','1-Gerar Excel','','','2-Relatorio','2-Relatorio','2-Relatorio','','','','','','','','','','','','','','','','','','','','','','','2016113013:58:22'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
