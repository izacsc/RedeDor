/*
{Protheus.doc} U06010
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 17/11/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423040_EF_010
@Obs Fontes: F0601001.PRW
*/
#INCLUDE 'PROTHEUS.CH' 
User Function U06010(lOnlyInfo)
Local aInfo := {'06','010','CONSULTA DE INTEGRA��ES','17/11/16','16:55','000675162110000166U1115','29/12/16','14:30',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSWF060101','01','Data Transa��o De?','','','MV_CH0','D',10,0,0,'G','','MV_PAR01','','','','20160101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','02','Data Transa��o Ate?','','','MV_CH0','D',10,0,0,'G','','MV_PAR02','','','','20171230','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','03','Hora Transa��o De?','','','MV_CH0','C',8,0,0,'G','','MV_PAR03','','','','08:00:00','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','04','Hora Transa��o Ate?','','','MV_CH0','C',8,0,0,'G','','MV_PAR04','','','','18:00:00','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','05','Data Processamento De?','','','MV_CH0','D',10,0,0,'G','','MV_PAR05','','','','20160101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','06','Data Processamento Ate?','','','MV_CH0','D',10,0,0,'G','','MV_PAR06','','','','20171230','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','07','Hora Processamento De?','','','MV_CH0','C',8,0,0,'G','','MV_PAR07','','','','08:00:00','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','08','Hora Processamento Ate?','','','MV_CH0','C',8,0,0,'G','','MV_PAR08','','','','18:00:00','','','','','','','','','','','','','','','','','','','','','','','','','','','2016111716:54:44'})
aAdd(aSX1,{'FSWF060101','09','Status ?','','','MV_CH0','C',11,0,1,'C','','MV_PAR09','Enviado','','','','','Processando','','','','','Processado','','','','','Erro','','','','','','','','','','','','','','','2016111716:54:45'})
aAdd(aSX6,{'','MV_F6101','N','N�mero de Registros exibido','','','Na consulta na integra�ao','','','','','','20','','','U','','','','','','','2016111716:54:44'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
