/*
{Protheus.doc} U05004
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 19/12/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423039_EF_004
@Project N�O IDENTIFICADO
@Obs Fontes: F0500401.PRW,F0500402.PRW,F0500407.PRW,F0500410.PRW,F0500403.PR
@Obs Fontes: F0500404.PRW,F0500405.PRW,F0500406.PRW,F0500408.PRW,F0500409.PR
@Obs Fontes: M0500401.PRW,PE_FSUPDEND.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U05004(lOnlyInfo)
Local aInfo := {'05','004','SOLICITA��O DE MOVIMENTA��O DE PESSOAL','19/12/16','09:12','004692052100400295U0111','30/01/17','10:30',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'SRA','S1','RA_XSTMVTO','C',1,0,'Status Solic','Status Solic','Status Solic','Status Solicita�ao','Status Solicita�ao','Status Solicita�ao','@!','','���������������','','',0,'��','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2016121909:11:15'})
aAdd(aSX6,{'','FS_BLQSLTF','C','S=Bloqueia incluir uma solicita��o de transfer�nci','rio.(fonte: F0500401).','','a com altera��o de Salario. N=Permite Incluir uma','','','solicita��o de Transfer�ncia com Altera��o de Sala','','','S','','','U','','','','','','','2016121909:11:29'})
aAdd(aSX6,{'','FS_BTCONF','C','Informar as filiails+matriculas que poder�o acessa','','','o bot�o de configura��o de parametros especiificos','','','(Fonte: F0500401).','','','01010002000006','','','U','','','','','','','2016121909:11:29'})
aAdd(aSX6,{'','FS_CTABAXV','C','Codigo da tabela Movimenta��es de Pessoal X Vis�es','','','(Fonte: F0500401).','','','','','','U004','','','U','','','','','','','2016121909:11:29'})
aAdd(aSX6,{'','FS_DEPTCAN','C','Codigos dos departamentos que poder�o acessar o bo','','','t�o Cancelar Solicita��o (Fonte: F0500401).','','','','','','000000008','','','U','','','','','','','2016121909:11:29'})
aAdd(aSX6,{'','FS_PSALSUP','N','Se  n�o preenchido qualquer solicita�ao de aumento','o diferenciada que n�o esta relacionada a tabela U','','salarial n�o passar�  por uma  vis�o  diferenciada','004. (Fonte: F0500401).','','Caso esteja preenchido, ser� enviado para  um vis�','','','15','','','U','','','','','','','2016121909:11:29'})
aAdd(aSX6,{'','FS_VSALSUP','C','Codigo da Vis�o quando ultrapassar a solicita��o d','','','e aumento de salario ultrapassar os limtes  estipu','','','lados (Fonte: F0500401).','','','000009','','','U','','','','','','','2016121909:11:29'})
aAdd(aSXB,{'FSSQBM','1','01','DB','Depto. Mov.Pessoal','Depto. Mov.Pessoal','Depto. Mov.Pessoal','SQB','','2017012713:13:13'})
aAdd(aSXB,{'FSSQBM','2','01','01','Departamento','Departamento','Departamento','','','2017012713:13:13'})
aAdd(aSXB,{'FSSQBM','4','01','01','Departamento','Departamento','Departamento','QB_DEPTO','','2017012713:13:13'})
aAdd(aSXB,{'FSSQBM','4','01','02','Descricao','Descricao','Descricao','QB_DESCRIC','','2017012713:13:13'})
aAdd(aSXB,{'FSSQBM','5','01','','','','','SQB->QB_DEPTO','','2017012713:13:13'})
aAdd(aSXB,{'FSSQBM','6','01','','','','','@#U_F0500411(1)','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','1','01','DB','C.Custo Mov.Pessoal','C.Custo Mov.Pessoal','C.Custo Mov.Pessoal','CTT','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','2','01','01','C.Custo','C.Custo','C.Custo','','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','4','01','01','C.Custo','C.Custo','C.Custo','CTT_CUSTO','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','4','01','02','Desc Moeda 1','Desc Moeda 1','Desc Moeda 1','CTT_DESC01','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','5','01','','','','','CTT->CTT_CUSTO','','2017012713:13:13'})
aAdd(aSXB,{'FSCTTM','6','01','','','','','@#U_F0500411(2)','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','1','01','DB','Posto Mov.Pessoal','Posto Mov.Pessoal','Posto Mov.Pessoal','RCL','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','2','01','01','Cod.Depto+Codigo','Cod.Depto+Codigo','Cod.Depto+Codigo','','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','4','01','01','Codigo Posto','Codigo Posto','Codigo Posto','RCL_POSTO','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','4','01','02','Situacao','Situacao','Situacao','RCL_STATUS','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','5','01','','','','','RCL->RCL_POSTO','','2017012713:13:13'})
aAdd(aSXB,{'FSRCLM','6','01','','','','','@#U_F0500411(3)','','2017012713:13:13'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
