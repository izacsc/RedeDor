/*
{Protheus.doc} U07020
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 31/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN0000007423041_EF_020
@Obs Fontes: F0702001.PRW,W0702001.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07020(lOnlyInfo)
Local aInfo := {'07','020','INTEGRA��O CADASTRO CLIENTES','31/01/17','16:59','000719272010000167U2135','01/02/17','09:45','719135016201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'SA1','P5','A1_XOPCM','C',1,0,'Operadora?','Operadora?','Operadora?','Cliente � Operadora?','Cliente � Operadora?','Cliente � Operadora?','@!','','���������������','','',0,'��','','','U','N','A','R','','','1=Sim;2=N�o','','','','','','','','','','','','N','N','','','','2017013116:57:31'})
aAdd(aSX3Hlp,{'A1_XOPCM','Cliente � Operadora? (1=Sim; 2=N�o)'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
