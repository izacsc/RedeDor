/*
{Protheus.doc} U06000
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 25/11/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
*/
#INCLUDE 'PROTHEUS.CH'
User Function U06000(lOnlyInfo)
Local aInfo := {'06','000','PARAMETRO OWNER','25/11/16','16:50','000650062110000166U0125','29/12/16','14:29',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX6,{'','FS_OWNER','C','Informe o owner terminado com ponto.','','','','','','','','','','','','U','','','','','','','2016112516:49:49'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
