/*
{Protheus.doc} U00001
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 20/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_016
@Project MAN0000007423041_EF_010
@Obs Fontes: AGRA045.PRW,F0201601.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07005(lOnlyInfo)
Local aInfo := {'00','001','LOCAIS DE ESTOQUE','20/01/17','16:54','005704072010100160U0125','27/01/17','14:45','704125016201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}

DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'NNR','10','NNR_XCUSTO','C',9,0,'Centro Custo','Centro Custo','Centro Custo','Centro de Custo','Centro de Custo','Centro de Custo','!@','',' ','','CTT',0,'ώΐ','','S','U','N','A','R','','ExistCpo("CTT")','','','','','','','','','','','','','N','N','','','','2017012714:45:45'})
aAdd(aSX3,{'NNR','13','NNR_XID','C',36,0,'ID','ID','ID','ID','ID','ID','','',' ','','',0,'ώΐ','','','U','N','V','R','','','','','','','','','','','','','','','N','N','','','','2017012016:34:27'})
aAdd(aSX7,{'NNR_XCUSTO','001','CTT->CTT_DESC01','NNR_XDESC0','X','S','CTT',1,'xFilial("CTT")+M->NNR_XCUSTO','','U','2016060108:00:00'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}



