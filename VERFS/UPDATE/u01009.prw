/*
{Protheus.doc} U01009
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 15/08/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000462901_EF_009
@Obs Fontes: F0100901.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U01009(lOnlyInfo)
Local aInfo := {'01','009','RELAT�RIO PARA PLANEJAMENTO DE COMPRAS EM EXCEL','15/08/16','09:48','009658012000900891U0114','29/12/16','14:27',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0100901','01','Filial','Filial','Filial','MV_CH0','C',8,0,0,'G','','MV_PAR01','','','','01010002','','','','','','','','','','','','','','','','','','','','','XM0','','','','','','2016081509:42:10'})
aAdd(aSX1,{'FSW0100901','02','DataBase','DataBase','DataBase','MV_CH0','D',8,0,0,'G','','MV_PAR02','','','','20160810','','','','','','','','','','','','','','','','','','','','','','','','','','','2016081509:42:10'})
aAdd(aSX1,{'FSW0100901','03','Produto','Produto','Produto','MV_CH0','C',15,0,0,'G','','MV_PAR03','','','','000000000000002','','','','','','','','','','','','','','','','','','','','','SB1','','','','','','2016081509:42:10'})
aAdd(aSX1,{'FSW0100901','04','Categoria','Categoria','Categoria','MV_CH0','C',6,0,0,'G','','MV_PAR04','','','','000001','','','','','','','','','','','','','','','','','','','','','ACU','','','','','','2016081509:42:10'})
aAdd(aSX1,{'FSW0100901','05','Produtos Controlados','Produtos Controlados','Produtos Controlados','MV_CH0','C',2,0,1,'C','Pertence("1,2")','MV_PAR05','Sim','Sim','Sim','','','N�o','N�o','N�o','','','','','','','','','','','','','','','','','','','','','','','','2016081509:42:10'})
aAdd(aSX3,{'ACU','09','ACU_XESTSE','N',12,2,'Seguranca','Seguranca','Seguranca','Estoque de Seguranca','Estoque de Seguranca','Estoque de Seguranca','@E 999,999,999.99','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016081509:41:12'})
aAdd(aSX3,{'ACU','10','ACU_XPE','N',5,0,'Lead Time','Lead Time','Lead Time','Lead Time','Lead Time','Lead Time','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','','2016081509:41:12'})
aAdd(aSX3,{'ACU','11','ACU_XFREQ','N',5,0,'Frequ�ncia','Frequ�ncia','Frequ�ncia','Frequ�ncia de an�lise','Frequ�ncia de an�lise','Frequ�ncia de an�lise','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016081509:41:12'})
aAdd(aSX3,{'ACU','12','ACU_CTRLP','C',1,0,'Cont. Planej','Cont. Planej','Cont. Planej','Controle Planejamento','Controle Planejamento','Controle Planejamento','','','���������������','','',0,'��','','','U','N','A','R','�','Pertence(" 12")','1=Sim;2=N�o','','','','','','','','','','','','N','N','','','','2016081509:41:12'})
aAdd(aSX3,{'CN9','A7','CN9_XFATMI','N',15,2,'Fat. Minimo','Fat. Minimo','Fat. Minimo','Faturamento M�nimo','Faturamento M�nimo','Faturamento M�nimo','@E 999,999,999,999.99','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016081509:41:16'})
aAdd(aSX3,{'SB1','03','B1_DESC','C',30,0,'Descricao','Descripcion','Description','Descricao do Produto','Descripcion del Producto','Description of Product','@!','A010VlStr()','���������������','','',1,'��','','S','','S','','','','texto()','','','','','','','','1','S','','','S','N','N','','','1','2016122914:27:33'})
aAdd(aSX3,{'SB1','36','B1_ESTSEG','N',12,2,'Seguranca','Seguridad','Safety Inv.','Estoque de Seguranca','Stock de Seguridad','Security Inventory','@E 999,999,999.99','','���������������','','',1,'��','','','','N','','','','Positivo()','','','','','','','','3','S','','','N','N','N','','','1','2016081509:41:34'})
aAdd(aSX3,{'SB1','39','B1_PE','N',5,0,'Entrega','Entrega','Deliv.Term','Prazo de Entrega','Plazo de Entrega','Delivery Term','@E 99999','Positivo()','���������������','','',1,'��','','','','N','','','','Positivo()','','','','','','','','3','S','','','N','N','N','','','1','2016081509:41:34'})
aAdd(aSX3,{'SBZ','66','BZ_XFREQAN','N',5,0,'Frequ�ncia','Frequ�ncia','Frequ�ncia','Frequ�ncia de an�lise','Frequ�ncia de an�lise','Frequ�ncia de an�lise','@E 99999','','���������������','','',0,'��','','','U','N','A','R','','Positivo()','','','','','','','','','','','','','N','N','','','','2016081509:41:34'})
aAdd(aSX3,{'SC7','H6','C7_XUSR','C',6,0,'Emitente','Emitente','Emitente','Emitente','Emitente','Emitente','@!','','���������������','','',0,'��','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016081509:41:34'})
aAdd(aSX6,{'','FS_ALM','C','C�digo do Armazem do tipo Almoxarifado','C�digo do Armazem do tipo Almoxarifado','C�digo do Armazem  do tipo Almoxarifado','','','','','','','04','04','04','U','','','','','','','2016081509:42:07'})
aAdd(aSX6,{'','FS_ARMEXT','C','Armazem externo','Armazem externo','Armazem externo','','','','','','','01','01','01','U','','','','','','','2016081509:42:07'})
aAdd(aSX6,{'','FS_ARS','C','C�digo do Armazem do tipo Arsenal.','','','','','','','','','03','03','03','U','','','','','','','2016081509:42:07'})
aAdd(aSX6,{'','FS_FAR','C','C�digo do Armazem do tipo Farm�cia.','','','','','','','','','02','02','02','U','','','','','','','2016081509:42:07'})
aAdd(aSX3Hlp,{'ACU_XESTSE','Estoque de Seguran�a. Quantidadem�nimade produto em estoque para evitarafalta do mesmo entre a solicita��odecompra ou produ��o e o seurecebimento.'})
aAdd(aSX3Hlp,{'ACU_XPE','Lead Time'})
aAdd(aSX3Hlp,{'ACU_XFREQ','Frequ�ncia de an�lise'})
aAdd(aSX3Hlp,{'ACU_CTRLP','Controle Planejamento'})
aAdd(aSX3Hlp,{'B1_DESC','Descri��o do produto.'})
aAdd(aSX3Hlp,{'B1_ESTSEG','Estoque de seguran�a. Quantidadem�nimade produto em estoque para evitara falta do mesmo entre a solicita��o decompra ou produ��o e o seu recebimento.'})
aAdd(aSX3Hlp,{'B1_PE','Prazo de entrega do produto. � on�merode dias, meses ou anos que ofornecedor ou a f�brica necessita paraentregar o  produto, a partir dorecebimento de seu pedido.'})
aAdd(aSX3Hlp,{'BZ_XFREQAN','Frequ�ncia de an�lise'})
aAdd(aSX3Hlp,{'C7_XUSR','Emitente'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
