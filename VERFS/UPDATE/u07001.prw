/*
{Protheus.doc} U07001
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 17/01/2017
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@project	MAN000007423041_EF_001
@Obs Fontes: F0700101.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U07001(lOnlyInfo)
Local aInfo := {'07','001','CADASTRO DE SETORES','17/01/17','15:03','001773072010100157U0110','27/01/17','14:45','773110015201'}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P11','1','P11_FILIAL+P11_COD','P11_FILIAL+P11_COD','P11_FILIAL+P11_COD','P11_FILIAL+P11_COD','U','','','S','2017011715:02:30'})
aAdd(aSX2,{'P11','','P11010','SETORES','SETORES','SETORES','','E','E','E',0,'','','',0,'','','','','','',0,0,0,'2017011715:02:29'})
aAdd(aSX3,{'P11','01','P11_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','','','',1,'ώΐ','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2017011715:02:27'})
aAdd(aSX3,{'P11','02','P11_COD','C',6,0,'Cod do Setor','Cod do Setor','Cod do Setor','Codigo do Setor','Codigo do Setor','Codigo do Setor','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017011715:02:27'})
aAdd(aSX3,{'P11','03','P11_DESC','C',20,0,'Descricao','Descricao','Descricao','Descricao do Setor','Descricao do Setor','Descricao do Setor','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017011715:02:27'})
aAdd(aSX3,{'P11','04','P11_CCUSTO','C',9,0,'Centro Custo','Centro Custo','Centro Custo','Centro de Custo','Centro de Custo','Centro de Custo','@!','',' ','','',0,'ώΐ','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2017011715:02:27'})
aAdd(aSX3,{'P11','05','P11_ID','C',36,0,'ID','ID','ID','ID','ID','ID','@!','',' ','','',0,'ώΐ','','','U','S','V','R','','','','','','','','','','','','','','','N','N','','','','2017011715:02:27'})
aAdd(aSX3,{'P11','06','P11_MSBLQL','C',1,0,'Bloqueado?','Bloqueado?','Bloqueado?','Registro bloqueado','Registro bloqueado','Registro bloqueado','','',' ','"2"','',9,'ώΐ','','','U','S','A','R','','','1=Sim;2=Nγo','1=Si;2=No','1=Yes;2=No','','','','','','','','','','N','N','','','','2017011715:02:27'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
