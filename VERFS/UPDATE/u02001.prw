/*
{Protheus.doc} U02001
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 06/06/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_001
@Obs Fontes: F0200101.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02001(lOnlyInfo)
Local aInfo := {'02','001','RELATΣRIO DE CUSTO EVITADO E SAVING','06/06/16','14:39','001669022010100642U0103','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}                           
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0200101','01','Filial:','Filial:','Filial:','MV_CH0','C',99,0,0,'R','','MV_PAR01','','','','SC8.C8_FILIAL','','','','','','','','','','','','','','','','','','','','','XM0','','','','','',''})
aAdd(aSX1,{'FSW0200101','02','Data De:','Data De:','Data De:','MV_CH0','D',8,0,0,'G','!empty(mv_par02)','MV_PAR02','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''})
aAdd(aSX1,{'FSW0200101','03','Data Ate:','Data Ate:','Data Ate:','MV_CH0','D',8,0,0,'G','!empty(mv_par03) .and.(mv_par03>= mv_par02)','MV_PAR03','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''})
aAdd(aSX3,{'SB1','03','B1_DESC','C',30,0,'Descricao','Descripcion','Description','Descricao do Produto','Descripcion del Producto','Description of Product','@!','',' ','','',1,'','','S','','S','','','','texto()','','','','','','','','1','S','','','S','N','N','','','1','1'})
aAdd(aSX3,{'SB1','S0','B1_XGRPPRO','C',4,0,'Grup Produto','Grup Produto','Grup Produto','Grupo do Produto','Grupo do Produto','Grupo do Produto','@!','',' ','','SBM',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3,{'SB1','S1','B1_XSUBGRP','C',4,0,'Subgrup Prod','Subgrup Prod','Subgrup Prod','Subgrupo do Produto','Subgrupo do Produto','Subgrupo do Produto','@!','',' ','','SBM',0,'ώΐ','','','U','N','A','R','','','','','','','','','','','','','','','N','N','','','',''})
aAdd(aSX3Hlp,{'B1_DESC','Descriηγo do produto.'})
aAdd(aSX3Hlp,{'B1_XGRPPRO','Grupo do Produto'})
aAdd(aSX3Hlp,{'B1_XSUBGRP','Subgrupo do Produto'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
