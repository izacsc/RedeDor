/*
{Protheus.doc} U02005
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 19/07/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_005
@Obs Fontes: F0200501.PRW,F0200502.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02005(lOnlyInfo)
Local aInfo := {'02','005','RELATORIO DE AMOSTRA EM EXCEL','19/07/16','13:41','005691022010500732U0114','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSIX,{'P08','1','P08_FILIAL+P08_GL','GL','GL','GL','U','','GL','S','2016071913:40:26'})
aAdd(aSX1,{'FSW0200501','01','Data Base','Data Base','Data Base','MV_CH0','D',8,0,0,'G','NAOVAZIO()','MV_PAR01','','','','20110715','','','','','','','','','','','','','','','','','','','','','','','','','','','2016071913:40:30'})
aAdd(aSX1,{'FSW0200501','02','Produto','Produto','Produto','MV_CH0','C',15,0,0,'G','','MV_PAR02','','','','','','','','','','','','','','','','','','','','','','','','','SB1','','','','','','2016071913:40:30'})
aAdd(aSX1,{'FSW0200501','03','Categoria/Segmento','Categoria/Segmento','Categoria/Segmento','MV_CH0','C',6,0,0,'G','','MV_PAR03','','','','','','','','','','','','','','','','','','','','','','','','','ACU','','','','','','2016071913:40:30'})
aAdd(aSX1,{'FSW0200501','04','Somente Categorias Controladas','Somente Categorias Controladas','Somente Categorias Controladas','MV_CH0','C',1,0,1,'C','','MV_PAR04','Sim','Sim','Sim','','','N�o','N�o','N�o','','','','','','','','','','','','','','','','','','','','','','','2016071913:40:30'})
aAdd(aSX2,{'P08','','P08010','GRAU DE LIBERDADE  DA DISTRIB.','GRAU DE LIBERDADE  DA DISTRIB.','GRAU DE LIBERDADE  DA DISTRIB.','','C','C','C',0,'','P08_FILIAL+P08_GL','',0,'','','','','','',0,0,0,'2016071913:40:25'})
aAdd(aSX3,{'P08','01','P08_FILIAL','C',8,0,'Filial','Sucursal','Branch','Filial do Sistema','Sucursal','Branch of the System','@!','','���������������','','',1,'��','','','U','N','','','','','','','','','','','033','','','','','','','','','','','2016071913:40:22'})
aAdd(aSX3,{'P08','02','P08_GL','C',4,0,'GL','GL','GL','Grau de Lib. Distrib.','Grau de Lib. Distrib.','Grau de Lib. Distrib.','@ 9999','','���������������','','',0,'��','','','U','S','A','R','�','U_M02005VL()','','','','','INCLUI','','','','','','','','N','N','','','','2016071913:40:22'})
aAdd(aSX3,{'P08','03','P08_T','N',7,4,'T','T','T','Valor de Distribui��o','Valor de Distribui��o','Valor de Distribui��o','@E 99.9999','','���������������','','',0,'��','','','U','S','A','R','','','','','','','','','','','','','','','N','N','','','','2016071913:40:22'})
aAdd(aSX3,{'P08','04','P08_DTINCL','D',8,0,'Dt.Inclus�o','Dt.Inclus�o','Dt.Inclus�o','Data da Inclus�o','Data da Inclus�o','Data da Inclus�o','','','���������������','MSDATE()','',0,'��','','','U','S','V','R','','','','','','','INCLUI','','','','','','','','N','N','','','','2016071913:40:22'})
aAdd(aSX3,{'P08','05','P08_HRINCL','C',8,0,'Hora Incl.','Hora Incl.','Hora Incl.','Hora da inclusao','Hora da inclusao','Hora da inclusao','@R 99:99:99','','���������������','TIME()','',0,'��','','','U','S','V','R','','','','','','','INCLUI','','','','','','','','N','N','','','','2016071913:40:22'})
aAdd(aSX3,{'P08','06','P08_USUINC','C',40,0,'Resp. Inc.','Resp. Inc.','Resp. Inc.','Respons�vel da Inclus�o','Respons�vel da Inclus�o','Respons�vel da Inclus�o','@!','','���������������','LOGUSERNAME()','',0,'��','','','U','S','V','R','','','','','','','INCLUI','','','','','','','','N','N','','','','2016071913:40:22'})
aAdd(aSX6,{'','FS_AMOS1','N','Numero  de Amostra 1(fonte:FS0200501)','','','','','','','','','2.069','2.069','2.069','U','','','','','','','2016071913:40:30'})
aAdd(aSX6,{'','FS_AMOS2','N','Numero da Amostra 2 (FS0200501)','','','','','','','','','24','24','24','U','','','','','','','2016071913:40:30'})
aAdd(aSX3Hlp,{'P08_GL','Grau de Liberdade de Distribui��o.'})
aAdd(aSX3Hlp,{'P08_T','Valor de Distribui��o'})
PutHelp('PFSW020050101',{'Informe a partir de qual data o sistema '+CRLF+' dever� retroagir 24 meses.'+CRLF+'Campo obrigat�rio.'},{'Informe a partir de qual data o sistema '+CRLF+' dever� retroagir 24 meses.'+CRLF+'Campo obrigat�rio.'},{'Informe a partir de qual data o sistema '+CRLF+' dever� retroagir 24 meses.'+CRLF+'Campo obrigat�rio.'}) 
PutHelp('PFSW020050102',{'Informe o c�digo do produto ou clique '+CRLF+'na lupa (F3). '+CRLF+'Preenchimento de parametro opcional.'},{'Informe o c�digo do produto ou clique '+CRLF+'na lupa (F3). '+CRLF+'Preenchimento de parametro opcional.'},{'Informe o c�digo do produto ou clique '+CRLF+'na lupa (F3). '+CRLF+'Preenchimento de parametro opcional.'})
PutHelp('PFSW020050103',{'Informe o codigo da Categoria de '+CRLF+' Produto ou tecle F3 (lupa).'+CRLF+'Campo opcional.'},{'Informe o codigo da Categoria de '+CRLF+' Produto ou tecle F3 (lupa).'+CRLF+'Campo opcional.'},{'Informe o codigo da Categoria de '+CRLF+' Produto ou tecle F3 (lupa).'+CRLF+'Campo opcional.'})
PutHelp('PFSW020050104',{'Informe se somente dever�o ser'+CRLF+'consideradas as categorias controladas'+CRLF+'pela equipe de planejamento.'+CRLF+'Campo obrigat�rio.'},{'Informe se somente dever�o ser'+CRLF+'consideradas as categorias controladas'+CRLF+'pela equipe de planejamento.'+CRLF+'Campo obrigat�rio.'},{'Informe se somente dever�o ser'+CRLF+'consideradas as categorias controladas'+CRLF+'pela equipe de planejamento.'+CRLF+'Campo obrigat�rio.'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
