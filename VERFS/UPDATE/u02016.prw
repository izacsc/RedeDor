/*
{Protheus.doc} U02016
Fun��o de instala��o de dicionario especifico.
@author	FsTools V6.2.14
@since 13/05/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_016
@Obs Fontes: F0201601.PRW,PE_AGRA045.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02016(lOnlyInfo)
Local aInfo := {'02','016','CRIA��O DE ROTINA PARA A CADASTRO DE SETORES','01/06/16','08:00','006610122000600682U1100','29/12/16','14:29',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX3,{'NNR','10','NNR_XCUSTO','C',9,0,'Centro Custo','Centro Custo','Centro Custo','Centro de Custo','Centro de Custo','Centro de Custo','!@','','���������������','','CTT',0,'��','','S','U','N','A','R','�','ExistCpo("CTT")','','','','','','','','','','','','','N','N','','','','2016060108:00:00'})
aAdd(aSX3,{'NNR','11','NNR_XDESC0','C',40,0,'Desc C.Custo','Desc C.Custo','Desc C.Custo','Descri��o Centro de Custo','Descri��o Centro de Custo','Descri��o Centro de Custo','@!','','���������������','POSICIONE("CTT",1,XFILIAL("CTT")+NNR->NNR_XCUSTO,"CTT_DESC01")','',0,'��','','','U','N','V','V','','','','','','','','','','','','','','','N','N','','','','2016060108:00:00'})
aAdd(aSX3,{'NNR','12','NNR_XESTOQ','C',1,0,'Cont.Estoque','Cont.Estoque','Cont.Estoque','Controla Estoque','Controla Estoque','Controla Estoque','!' ,'','���������������','',''   ,0,'��','','','U','N','A','R','�','Pertence("12")','1=Sim;2=Nao','','','','','','','','','','','','N','N','','','','2016060108:00:00'})
aAdd(aSX7,{'NNR_XCUSTO','001','CTT->CTT_DESC01','NNR_XDESC0','X','S','CTT',1,'xFilial("CTT")+M->NNR_XCUSTO','','U','2016060108:00:00'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
