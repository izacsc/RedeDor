/*
{Protheus.doc} U02004
Funηγo de instalaηγo de dicionario especifico.
@author	FsTools V6.2.14
@since 11/07/2016
@param lOnlyInfo Indica se deve retornar so as informacoes sobre o update ou todos os ajustes a serem realizados
@Project MAN00000463301_EF_004
@Obs Fontes: F0200401.PRW,F0200402.PRW,F0200403.PRW,F0200404.PRW
@Obs manual desmark
*/
#INCLUDE 'PROTHEUS.CH'
User Function U02004(lOnlyInfo)
Local aInfo := {'02','004','RELATORIO DE RASTREABILIDADE DE SC','11/07/16','09:40','004610022000400792U0114','29/12/16','14:26',''}
Local aSIX	:= {}
Local aSX1	:= {}
Local aSX2	:= {}
Local aSX3	:= {}
Local aSX6	:= {}
Local aSX7	:= {}
Local aSXA	:= {}
Local aSXB	:= {}
Local aSX3Hlp := {}
Local aCarga  := {}
DEFAULT lOnlyInfo := .f.
If lOnlyInfo
	Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
EndIf
aAdd(aSX1,{'FSW0200401','01','Filial','Filial','Filial','MV_CH0','C',40,0,0,'R','U_F0200403()','MV_PAR01','','','','C1_FILIAL','','','','','01010005;','','','','','','','','','','','','','','','','FSSM01','','','','','','2016071109:37:16'})
aAdd(aSX1,{'FSW0200401','02','Setor','Setor','Setor','MV_CH0','C',20,0,0,'R','U_F0200402()','MV_PAR02','','','','C1_LOCAL','','','','','02;01;X;','','','','','','','','','','','','','','','','FSNNR1','','','','','','2016071109:37:16'})
aAdd(aSX1,{'FSW0200401','03','Data de:','Data de:','Data de:','MV_CH0','D',8,0,0,'G','naovazio()','MV_PAR03','','','','20100101','','','','','','','','','','','','','','','','','','','','','','','','','','','2016071109:37:16'})
aAdd(aSX1,{'FSW0200401','04','Data ate:','Data ate:','Data ate:','MV_CH0','D',8,0,0,'G','naovazio()','MV_PAR04','','','','20160707','','','','','','','','','','','','','','','','','','','','','','','','','','','2016071109:37:16'})
aAdd(aSX3,{'SC1','97','C1_XMOTIVO','C',2,0,'Motivo SC','Motivo SC','Motivo SC','Motivo SC','Motivo SC','Motivo SC','@!','',' ','IIF(TYPE("C") == "C" .AND. C<>"",ACOLS[N-1][GDFIELDPOS("C1_XMOTIVO")]," ")','',0,'ώΐ','','S','U','N','A','R','','','','','','','','','','','','','','','N','N','','','','2016072609:40:10'})
aAdd(aSX3,{'SC1','98','C1_XDESMOT','C',40,0,'Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','Desc. Motivo','@!','',' ','IIF(!EMPTY(SC1->C1_XMOTIVO) .AND. !INCLUI,POSICIONE("SX5",1,XFILIAL("SX5")+"ZZ"+ SC1->C1_XMOTIVO,"X5_DESCRI")," ")','',0,'ώΐ','','','U','S','V','V','','','','','','','','','','','','','','','N','N','','','','2016072609:40:10'})
aAdd(aSXB,{'FSNNR1','1','01','RE','Setores  Ativos','Setores  Ativos','Setores  Ativos','NNR','','2016071109:37:13'})
aAdd(aSXB,{'FSNNR1','2','01','01','','','','U_F0200404(3)','','2016071109:37:13'})
aAdd(aSXB,{'FSNNR1','5','01','','','','','U_F0200404(4)','','2016071109:37:13'})
aAdd(aSXB,{'FSSM01','1','01','RE','Filiais por Empresa','Filiais por Empresa','Filiais por Empresa','SC1','','2016071109:37:13'})
aAdd(aSXB,{'FSSM01','2','01','01','','','','U_F0200404(1)','','2016071109:37:13'})
aAdd(aSXB,{'FSSM01','5','01','','','','','U_F0200404(2)','','2016071109:37:13'})
Return {aInfo,aSIX,aSX1,aSX2,aSX3,aSX6,aSX7,aSXA,aSXB,aSX3Hlp,aCarga}
